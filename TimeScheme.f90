module TimeScheme
  use AdvectionProblem
  
  implicit none
  
contains  
  
!! fonction pour initialiser le schema en temps
subroutine SetInitialCondition(rho0, u, v, rho, rhou, rhov)
  real(pr), dimension(:,:), intent(in) :: rho0, u, v
  real(pr), dimension(:,:), intent(inout) :: rho, rhou, rhov
  alp(1) = 0._pr
  alp(2) = dt*0.5_pr
  alp(3) = dt*0.5_pr
  alp(4) = dt

  beta(1) = dt/6._pr
  beta(2) = dt/3._pr
  beta(3) = dt/3._pr
  beta(4) = dt/6._pr

  rho = rho0
  rhou = rho*u
  rhov = rho*v
  !do j=1,N
  !   do i=1,N 
  !      rhou(i,j) = rho(i,j)*u(i,j) 
  !      rhov(i,j) = rho(i,j)*v(i,j) 
  !   enddo
  !enddo
end

!! calcul de la sol approchee au temps t
subroutine AdvanceTime(nt, dt_, timeSchemeChoice, advectionSchemeChoice, interpChoice, BC, rho, u, v, rhou, rhov)
  integer, intent(in) :: nt, timeSchemeChoice, advectionSchemeChoice, interpChoice, BC
  real(pr), intent(in) :: dt_
  real(pr), dimension(:,:), intent(inout) :: rho, u, v, rhou, rhov
  real(pr),dimension(size(rho,1),size(rho,2)) :: u0, v0, rho00, rhou0, rhov0, rhoNext1, rhoNext2, un, zero
  integer :: i, j
  real(pr) :: maxLS, minLS, seuilLS
  u0 = u
  v0 = v
  un = 1._pr
  zero = 0._pr
  rho00 = rho
  rhou0 = rhou
  rhov0 = rhov

  !call computeFunction2D(dt, u, t, rhoNext1, advectionSchemeChoice, BC, un, zero)
  !call computeFunction2D(dt, v, t, rhoNext2, advectionSchemeChoice, BC, zero, un)
  !write(*,*)"DIVERGENCE ",maxval(abs(rhoNext1-rhoNext2))

  !maxLS = maxval(rho)
  !minLS = minval(rho)
  !seuilLS = (maxLS-minLS)/1.005
  !rho = rho+0.5_pr!seuilLS
  !rhou = rho*u
  !rhov = rho*v
 
  if (timeSchemeChoice == 1) then
          call AdvanceEulerExp(nt, dt_, advectionSchemeChoice, BC, rho, u, v, rhou, rhov)
  else if (timeSchemeChoice == 3) then
          call AdvanceRK3(nt, dt_, advectionSchemeChoice, BC, rho, u, v, rhou, rhov)
  else if (timeSchemeChoice == 4) then
          call AdvanceRK4(nt, dt_, advectionSchemeChoice, BC, rho, u, v, rhou, rhov)
  else if (advectionSchemeChoice == 2) then
          !call AdvanceLWdirichletH(nt, t, rho, u, v)
          !call AdvanceLWdirichletH(nt, t, rhou, u, v)
          !call AdvanceLWdirichletH(nt, t, rhov, u, v)
          !/!\ DIVERGENCE NULLE /!\!
          call AdvanceLWperiodic(nt, dt_, rho, u, v)
          call AdvanceLWperiodic(nt, dt_, rhou, u, v)
          call AdvanceLWperiodic(nt, dt_, rhov, u, v)
  else if (advectionSchemeChoice == 3) then
          !!call AdvanceParticle(nt, dt_, interpChoice, rho, u, v, rhou, rhov)
          write(*,*) "no particle"
  else if (advectionSchemeChoice == 5) then
          !call AdvanceWENO5LF(nt, t)
          write(*,*)"Lax-Friedrichs Flux Splitting NOT Implemented"
  endif

!  rhou = (rho+(maxval(rho)+minval(rho))*0.5)*u
!  rhov = (rho+(maxval(rho)+minval(rho))*0.5)*v
!  do i=1,nx
!     do j=1,ny 
!        if (rho(i,j)+(maxval(rho)+minval(rho))*0.5.gt.1.e-6_pr) then
!           u(i,j) = rhou(i,j)/(rho(i,j)+(maxval(rho)+minval(rho))*0.5) 
!           v(i,j) = rhov(i,j)/(rho(i,j)+(maxval(rho)+minval(rho))*0.5) 
!
!           !if (abs(u(i,j)-u0(i,j))>1.e-3) write(*,*) "changement U           ",i," ",j,"       ",abs(u(i,j)-u0(i,j))
!           !if (abs(v(i,j)-v0(i,j))>1.e-3) write(*,*) "changement V           ",i," ",j,"       ",abs(v(i,j)-v0(i,j))
!           !if (abs(rho(i,j)-rho00(i,j))>1.e-3) write(*,*) "changement RHO           ",i," ",j,"        ",abs(rho(i,j)-rho00(i,j))
!           !if (abs(rhou(i,j)-rhou0(i,j))>1.e-3) write(*,*) "changement RHOU           ",i," ",j,"       ",abs(rhou(i,j)-rhou0(i,j))
!           !if (abs(rhov(i,j)-rhov0(i,j))>1.e-3) write(*,*) "changement RHOV           ",i," ",j,"       ",abs(rhov(i,j)-rhov0(i,j))
!        else
!!           rho(i,j) = 0._pr !-0.5_pr!0._pr
!           rhou(i,j) = 0._pr
!           rhov(i,j) = 0._pr
!           rhouu(i,j) = 0._pr
!           rhovv(i,j) = 0._pr
!           u(i,j) = 0._pr
!           v(i,j) = 0._pr
!        endif
!     enddo
!  enddo

  !maxLS = maxval(rho)
  !minLS = minval(rho)
  !seuilLS = (maxLS-minLS)/1.005_pr
  !rho = rho - 0.5_pr!seuilLS

end

!/*************************
! * ExplicitEulerIterator *
! *************************/

!! fonction principale qui avance le schema en temps
subroutine AdvanceEulerExp(n, tn, scheme, BC, rho, u, v, rhou, rhov)
  integer, intent(in) :: n
  real(pr), intent(in) :: tn
  integer, intent(in) :: scheme, BC
  real(pr), dimension(:,:), intent(inout) :: rho, u, v, rhou, rhov
  real(pr), dimension(size(rho,1),size(rho,2)) :: rhoNext, rhouNext, rhovNext, zero, un
  real(pr), dimension(size(rho,1),size(rho,2)) :: rhoNext1, rhoNext2
  real(pr), dimension(size(rho,1),size(rho,2)) :: rhouNext1, rhovNext1
  real(pr), dimension(size(rho,1),size(rho,2)) :: rhouNext2, rhovNext2
  zero = 0._pr
  un = 1._pr

  !/* DIVERGENCE NULLE /*!
  call computeFunction2D(dt, rho, tn, rhoNext, scheme, BC, u, v)
  rho = rho + dt*rhoNext
  !call computeFunction2D(dt, rhou, tn, rhouNext, scheme, BC, u, v)
  !rhou = rhou + dt*rhouNext
  !call computeFunction2D(dt, rhov, tn, rhovNext, scheme, BC, u, v)
  !rhov = rhov + dt*rhovNext 

  !!/* DIVERGENCE NON NULLE (QUELCONQUE)/*!
  !call computeFunction2D(dt, rhou, tn, rhoNext1, scheme, BC, un, zero)
  !call computeFunction2D(dt, rhov, tn, rhoNext2, scheme, BC, zero, un)
  !rho = rho + dt*rhoNext1 + dt*rhoNext2
  !rhoNext = rhou*u
  !call computeFunction2D(dt, rhoNext, tn, rhouNext1, scheme, BC, un, zero)
  !rhoNext = rhou*v
  !call computeFunction2D(dt, rhoNext, tn, rhouNext2, scheme, BC, zero, un)
  !rhou = rhou + dt*rhouNext1 + dt*rhouNext2
  !rhoNext = rhov*u
  !call computeFunction2D(dt, rhoNext, tn, rhovNext1, scheme, BC, un, zero)
  !rhoNext = rhov*v
  !call computeFunction2D(dt, rhoNext, tn, rhovNext2, scheme, BC, zero, un)
  !rhov = rhov + dt*rhovNext1 + dt*rhovNext2

  !! Euler explicite : rho^n+1 = rho^n + dt f(t^n, rho^n)
  !! sys.AddFunction(dt, rho, tn, rho_next);
  !call AddFunction(dt, rho, tn, rhoNext, scheme)
  !call computeFunction2D(dt, rho, tn, rhoNext, scheme, BC, u, v)
  !rhoNext = rho + dt*rhoNext
  !rho = rhoNext
  !call computeFunction2D(dt, rhou, tn, rhoNext1, scheme, BC, un, zero)
  !call computeFunction2D(dt, rhov, tn, rhoNext2, scheme, BC, zero, un)
  !rho = rho + dt*rhoNext1 + dt*rhoNext2

  !call computeFunction2D(dt, rhou, tn, rhouNext, scheme, BC, u, v)
  !rhou = rhou + dt*rhouNext
  !call computeFunction2D(dt, u, tn, rhouNext1, scheme, BC, rhou, zero)
  !call computeFunction2D(dt, v, tn, rhouNext2, scheme, BC, zero, rhou)
  !rhou = rhou + dt*rhouNext + dt*rhouNext1 + dt*rhouNext2
  !rhoNext = rhou*u
  !call computeFunction2D(dt, rhoNext, tn, rhouNext1, scheme, BC, un, zero)
  !rhoNext = rhou*v
  !call computeFunction2D(dt, rhoNext, tn, rhouNext2, scheme, BC, zero, un)
  !rhou = rhou + dt*rhouNext1 + dt*rhouNext2

  !call computeFunction2D(dt, rhov, tn, rhovNext, scheme, BC, u, v)
  !rhov = rhov + dt*rhovNext 
  !call computeFunction2D(dt, u, tn, rhovNext1, scheme, BC, rhov, zero)
  !call computeFunction2D(dt, v, tn, rhovNext2, scheme, BC, zero, rhov)
  !rhov = rhov + dt*rhovNext + dt*rhovNext1 + dt*rhovNext2
  !rhoNext = rhov*u
  !call computeFunction2D(dt, rhoNext, tn, rhovNext1, scheme, BC, un, zero)
  !rhoNext = rhov*v
  !call computeFunction2D(dt, rhoNext, tn, rhovNext2, scheme, BC, zero, un)
  !rhov = rhov + dt*rhovNext1 + dt*rhovNext2

end


!/*************************
! * RK4Iterator *
! *************************/

!! fonction principale qui avance le schema en temps
subroutine AdvanceRK4(n, tn, scheme, BC, rho, u, v, rhou, rhov)
  integer, intent(in) :: n
  real(pr), intent(in) :: tn
  integer, intent(in) :: scheme, BC
  real(pr), dimension(:,:), intent(inout) :: rho, u, v, rhou, rhov
  integer :: i, j, k
  real(pr), dimension(size(rho,1),size(rho,2)) :: kRK, kRKu, kRKv, addedFunctionU,&
  addedFunctionV, addedFunction, rhoNext, rhouNext, rhovNext
  real(pr), dimension(size(rho,1),size(rho,2)) :: kRK1, kRK2, kRKu1, kRKu2, kRKv1,&
  kRKv2, rhoNext1, rhoNext2, rhouNext1, rhouNext2, rhovNext1, rhovNext2, un, zero
  
  un = 1._pr
  zero = 0._pr

  !do i=1,5
  !    rho = rho*alp(i)
  !    !! RK4 : ρ = αi ρ + Δt f(ρn+1, t + ci Δt)      ρn+1 = ρn+1 + βi ρ 
  !    !! sys.AddFunction(dt, rho_next, tn+c(i)*dt, rho);
  !    !call AddFunction(dt, rhoNext, tn+c(i)*dt, rho, scheme)
  !    call AddFunction2D(dt, rhoNext, tn+c(i)*dt, rho, scheme)
  !    !! rho = rho + dt*rhoNext
  !    rhoNext = rhoNext + beta(i)*rho
  !enddo
  !rho = rhoNext

 ! rhoNext = rho
 ! !addedFunction = rho
 ! do i=1,4
 !   addedFunction = rho + alp(i)*kRK
 !   !call AddFunction2D(alp(i), kRK, tn, addedFunction, scheme)
 !   call computeFunction2D(1._pr, addedFunction, tn, kRK, scheme, BC, u, v) !f(x,t) = f(x) d'ou tn = tn+c(i)*dt avec c(i) = 0
 !   rhoNext = rhoNext + beta(i)*kRK
 !   !call AddFunction2D(beta(i), kRk, tn, rhoNext, scheme)

 ! enddo

 ! rho = rhoNext

  !!/* DIVERGENCE NULLE/*!
  !rhoNext = rho
  !rhouNext = rhou
  !rhovNext = rhov
  !do k=1,4
  !  addedFunction = rho + alp(k)*kRK
  !  addedFunctionU = rhou + alp(k)*kRKu
  !  addedFunctionV = rhov + alp(k)*kRKv
  !  call computeFunction2D(1._pr, addedFunction, tn, kRK, scheme, BC, u, v) !f(x,t) = f(x) d'ou tn = tn+c(i)*dt avec c(i) = 0
  !  call computeFunction2D(1._pr, addedFunctionU, tn, kRKu, scheme, BC, u, v) !f(x,t) = f(x) d'ou tn = tn+c(i)*dt avec c(i) = 0
  !  call computeFunction2D(1._pr, addedFunctionV, tn, kRKv, scheme, BC, u, v) !f(x,t) = f(x) d'ou tn = tn+c(i)*dt avec c(i) = 0
  !  rhoNext = rhoNext + beta(k)*kRK
  !  rhouNext = rhouNext + beta(k)*kRKu
  !  rhovNext = rhovNext + beta(k)*kRKv
  !  do i=1,nx
  !     do j=1,ny 
  !        if (rhoNext(i,j).gt.1e-6) then
  !           u(i,j) = rhouNext(i,j)/rhoNext(i,j) 
  !           v(i,j) = rhovNext(i,j)/rhoNext(i,j) 
  !        else
  !           rhoNext(i,j) = 0._pr
  !           rhouNext(i,j) = 0._pr
  !           rhovNext(i,j) = 0._pr
  !        endif
  !     enddo
  !  enddo
  !enddo
  !rho = rhoNext
  !rhou = rhouNext
  !rhov = rhovNext

  !/* DIVERGENCE NON NULLE (QUELCONQUE)/*!
  rhoNext = rho
  rhouNext = rhou
  rhovNext = rhov
  do k=1,4
    addedFunction = rho + alp(k)*(kRK1 + kRK2)
    addedFunctionU = rhou + alp(k)*(kRKu1 + kRKu2)
    addedFunctionV = rhov + alp(k)*(kRKv1 + kRKv2)
    !rhoNext = addedFunction*u
    rhoNext = addedFunctionU
    call computeFunction2D(1._pr, rhoNext, tn, kRK1, scheme, BC, un, zero)
    !rhoNext = addedFunction*v
    rhoNext = addedFunctionV
    call computeFunction2D(1._pr, rhoNext, tn, kRK2, scheme, BC, zero, un)
    rhoNext = addedFunctionU*u
    call computeFunction2D(1._pr, rhoNext, tn, kRKu1, scheme, BC, un, zero)
    rhoNext = addedFunctionU*v
    call computeFunction2D(1._pr, rhoNext, tn, kRKu2, scheme, BC, zero, un)
    rhoNext = addedFunctionV*u
    call computeFunction2D(1._pr, rhoNext, tn, kRKv1, scheme, BC, un, zero)
    rhoNext = addedFunctionV*v
    call computeFunction2D(1._pr, rhoNext, tn, kRKv2, scheme, BC, zero, un)
    rhoNext = rhoNext + beta(k)*(kRK1 + kRK2)
    rhouNext = rhouNext + beta(k)*(kRKu1 + kRKu2)
    rhovNext = rhovNext + beta(k)*(kRKv1 + kRKv2)
    do i=1,nx
       do j=1,ny 
          if (rhoNext(i,j).gt.1e-6) then
             u(i,j) = rhouNext(i,j)/rhoNext(i,j) 
             v(i,j) = rhovNext(i,j)/rhoNext(i,j) 
          else
             rhoNext(i,j) = 0._pr
             rhouNext(i,j) = 0._pr
             rhovNext(i,j) = 0._pr
             u(i,j) = 0._pr
             v(i,j) = 0._pr
          endif
       enddo
    enddo
  enddo
  rho = rhoNext
  rhou = rhouNext
  rhov = rhovNext
end

!/*************************
! * RK3Iterator *
! *************************/

!! fonction principale qui avance le schema en temps
subroutine AdvanceRK3(n, tn, scheme, BC, rho, u, v, rhou, rhov)
  integer, intent(in) :: n
  real(pr), intent(in) :: tn
  integer, intent(in) :: scheme, BC
  real(pr), dimension(:,:), intent(inout) :: rho, u, v, rhou, rhov
  integer :: i, j
  real(pr), dimension(size(rho,1),size(rho,2)) :: addedFunction, rhostar, rhostarstar, rhoNext
  real(pr), dimension(size(rho,1),size(rho,2)) :: addedFunctionU, rhoustar, rhoustarstar, rhouNext
  real(pr), dimension(size(rho,1),size(rho,2)) :: addedFunctionV, rhovstar, rhovstarstar, rhovNext
  real(pr), dimension(size(rho,1),size(rho,2)) :: ustar, vstar, ustarstar, vstarstar, un, zero
  real(pr), dimension(size(rho,1),size(rho,2)) :: rhoNext1, rhoNext2, rhouNext1, rhouNext2, rhovNext1, rhovNext2

  un = 1._pr
  zero = 0._pr

  call computeFunction2D(1._pr, rho, tn, addedFunction, scheme, BC, u, v)
  rhostar = rho + dt*addedFunction
  call computeFunction2D(1._pr, rhostar, tn+dt, addedFunction, scheme, BC, u, v) 
  rhostarstar = 0.75_pr*rho + 0.25_pr*rhostar + 0.25_pr*dt*addedFunction
  call computeFunction2D(1._pr, rhostarstar, tn+0.5_pr*dt, addedFunction, scheme, BC, u, v)
  rhoNext = rho/3._pr + 2._pr/3._pr*rhostarstar + 2._pr/3._pr*dt*addedFunction

  rho = rhoNext

  !!/* DIVERGENCE NULLE/*!
  !call computeFunction2D(1._pr, rho, tn, addedFunction, scheme, BC, u, v)
  !rhostar = rho + dt*addedFunction
  !call computeFunction2D(1._pr, rhou, tn, addedFunctionU, scheme, BC, u, v)
  !rhoustar = rhou + dt*addedFunctionU
  !call computeFunction2D(1._pr, rhov, tn, addedFunctionV, scheme, BC, u, v)
  !rhovstar = rhov + dt*addedFunctionV
  !do i=1,nx
  !   do j=1,ny 
  !      if (rhostar(i,j).gt.1e-6) then
  !         ustar(i,j) = rhoustar(i,j)/rhostar(i,j) 
  !         vstar(i,j) = rhovstar(i,j)/rhostar(i,j) 
  !      else
  !         rhostar(i,j) = 0._pr
  !         rhoustar(i,j) = 0._pr
  !         rhovstar(i,j) = 0._pr
  !      endif
  !   enddo
  !enddo
  !call computeFunction2D(1._pr, rhostar, tn+dt, addedFunction, scheme, BC, ustar, vstar) 
  !rhostarstar = 0.75_pr*rho + 0.25_pr*rhostar + 0.25_pr*dt*addedFunction
  !call computeFunction2D(1._pr, rhoustar, tn+dt, addedFunctionU, scheme, BC, ustar, vstar) 
  !rhoustarstar = 0.75_pr*rhou + 0.25_pr*rhoustar + 0.25_pr*dt*addedFunctionU
  !call computeFunction2D(1._pr, rhovstar, tn+dt, addedFunctionV, scheme, BC, ustar, vstar) 
  !rhovstarstar = 0.75_pr*rhov + 0.25_pr*rhovstar + 0.25_pr*dt*addedFunctionV
  !do i=1,nx
  !   do j=1,ny 
  !      if (rhostarstar(i,j).gt.1e-6) then
  !         ustarstar(i,j) = rhoustarstar(i,j)/rhostarstar(i,j) 
  !         vstarstar(i,j) = rhovstarstar(i,j)/rhostarstar(i,j) 
  !      else
  !         rhostarstar(i,j) = 0._pr
  !         rhoustarstar(i,j) = 0._pr
  !         rhovstarstar(i,j) = 0._pr
  !      endif
  !   enddo
  !enddo
  !call computeFunction2D(1._pr, rhostarstar, tn+0.5_pr*dt, addedFunction, scheme, BC, ustarstar, vstarstar)
  !rhoNext = rho/3._pr + 2._pr/3._pr*rhostarstar + 2._pr/3._pr*dt*addedFunction
  !call computeFunction2D(1._pr, rhoustarstar, tn+0.5_pr*dt, addedFunctionU, scheme, BC, ustarstar, vstarstar)
  !rhouNext = rhou/3._pr + 2._pr/3._pr*rhoustarstar + 2._pr/3._pr*dt*addedFunctionU
  !call computeFunction2D(1._pr, rhovstarstar, tn+0.5_pr*dt, addedFunctionV, scheme, BC, ustarstar, vstarstar)
  !rhovNext = rhov/3._pr + 2._pr/3._pr*rhovstarstar + 2._pr/3._pr*dt*addedFunctionV
  !rho = rhoNext
  !rhou = rhouNext
  !rhov = rhovNext

  !!/* DIVERGENCE NON NULLE (QUELCONQUE)/*!
  !rhoNext = rho*u
  !call computeFunction2D(1._pr, rhoNext, tn, rhoNext1, scheme, BC, un, zero)
  !rhoNext = rho*v
  !call computeFunction2D(1._pr, rhoNext, tn, rhoNext2, scheme, BC, zero, un)
  !addedFunction = rhoNext1 + rhoNext2
  !rhostar = rho + dt*addedFunction
  !rhoNext = rhou*u
  !call computeFunction2D(1._pr, rhoNext, tn, rhouNext1, scheme, BC, un, zero)
  !rhoNext = rhou*v
  !call computeFunction2D(1._pr, rhoNext, tn, rhouNext2, scheme, BC, zero, un)
  !addedFunctionU = rhoNext1 + rhoNext2
  !rhoustar = rhou + dt*addedFunctionU
  !rhoNext = rhov*u
  !call computeFunction2D(1._pr, rhoNext, tn, rhovNext1, scheme, BC, un, zero)
  !rhoNext = rhov*v
  !call computeFunction2D(1._pr, rhoNext, tn, rhovNext2, scheme, BC, zero, un)
  !addedFunctionV = rhoNext1 + rhoNext2
  !rhovstar = rhov + dt*addedFunctionV
  !do i=1,nx
  !   do j=1,ny 
  !      if (rhostar(i,j).gt.1e-6) then
  !         ustar(i,j) = rhoustar(i,j)/rhostar(i,j) 
  !         vstar(i,j) = rhovstar(i,j)/rhostar(i,j) 
  !      else
  !         rhostar(i,j) = 0._pr
  !         rhoustar(i,j) = 0._pr
  !         rhovstar(i,j) = 0._pr
  !         ustar(i,j) = 0._pr
  !         vstar(i,j) = 0._pr
  !      endif
  !   enddo
  !enddo
  !rhoNext = rhostar*ustar !rhoustar
  !call computeFunction2D(1._pr, rhoNext, tn+dt, rhoNext1, scheme, BC, un, zero)
  !rhoNext = rhostar*vstar !rhovstar
  !call computeFunction2D(1._pr, rhoNext, tn+dt, rhoNext2, scheme, BC, zero, un)
  !addedFunction = rhoNext1 + rhoNext2
  !rhostarstar = 0.75_pr*rho + 0.25_pr*rhostar + 0.25_pr*dt*addedFunction
  !rhoNext = rhoustar*ustar
  !call computeFunction2D(1._pr, rhoNext, tn+dt, rhouNext1, scheme, BC, un, zero)
  !rhoNext = rhoustar*vstar
  !call computeFunction2D(1._pr, rhoNext, tn+dt, rhouNext2, scheme, BC, zero, un)
  !addedFunctionU = rhoNext1 + rhoNext2
  !rhoustarstar = 0.75_pr*rhou + 0.25_pr*rhoustar + 0.25_pr*dt*addedFunctionU
  !rhoNext = rhovstar*ustar
  !call computeFunction2D(1._pr, rhoNext, tn+dt, rhovNext1, scheme, BC, un, zero)
  !rhoNext = rhovstar*vstar
  !call computeFunction2D(1._pr, rhoNext, tn+dt, rhovNext2, scheme, BC, zero, un)
  !addedFunctionV = rhoNext1 + rhoNext2
  !rhovstarstar = 0.75_pr*rhov + 0.25_pr*rhovstar + 0.25_pr*dt*addedFunctionV
  !do i=1,nx
  !   do j=1,ny 
  !      if (rhostarstar(i,j).gt.1e-6) then
  !         ustarstar(i,j) = rhoustarstar(i,j)/rhostarstar(i,j) 
  !         vstarstar(i,j) = rhovstarstar(i,j)/rhostarstar(i,j) 
  !      else
  !         rhostarstar(i,j) = 0._pr
  !         rhoustarstar(i,j) = 0._pr
  !         rhovstarstar(i,j) = 0._pr
  !         ustarstar(i,j) = 0._pr
  !         vstarstar(i,j) = 0._pr
  !      endif
  !   enddo
  !enddo
  !rhoNext = rhostarstar*ustarstar !rhoustarstar
  !call computeFunction2D(1._pr, rhoNext, tn+0.5_pr*dt, rhoNext1, scheme, BC, un, zero)
  !rhoNext = rhostarstar*vstarstar !rhovstarstar
  !call computeFunction2D(1._pr, rhoNext, tn+0.5_pr*dt, rhoNext2, scheme, BC, zero, un)
  !addedFunction = rhoNext1 + rhoNext2
  !rhoNext = rho/3._pr + 2._pr/3._pr*rhostarstar + 2._pr/3._pr*dt*addedFunction
  !rhoNext = rhoustarstar*ustarstar
  !call computeFunction2D(1._pr, rhoNext, tn+0.5_pr*dt, rhouNext1, scheme, BC, un, zero)
  !rhoNext = rhoustarstar*vstarstar
  !call computeFunction2D(1._pr, rhoNext, tn+0.5_pr*dt, rhouNext2, scheme, BC, zero, un)
  !addedFunctionU = rhoNext1 + rhoNext2
  !rhouNext = rhou/3._pr + 2._pr/3._pr*rhoustarstar + 2._pr/3._pr*dt*addedFunctionU
  !rhoNext = rhovstarstar*ustarstar
  !call computeFunction2D(1._pr, rhoNext, tn+0.5_pr*dt, rhovNext1, scheme, BC, un, zero)
  !rhoNext = rhovstarstar*vstarstar
  !call computeFunction2D(1._pr, rhoNext, tn+0.5_pr*dt, rhovNext2, scheme, BC, zero, un)
  !addedFunctionV = rhoNext1 + rhoNext2
  !rhovNext = rhov/3._pr + 2._pr/3._pr*rhovstarstar + 2._pr/3._pr*dt*addedFunctionV
  !rho = rhoNext
  !rhou = rhouNext
  !rhov = rhovNext
end
end

