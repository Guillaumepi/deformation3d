program def3D
  use variables
  use interpolation
  use AdvectionProblem
  use libBezier

  implicit none

  !**
  !! Declaration of all variables (to sort)
  !**
  integer :: i, j, k, kt, idisplay, iUpdateDist, iter, picNum, l, ll, pix1, pix2, l0, pos, sizeSkel, nl,lp,bool,booltmp,boolPhi&
       ,loopbool,xskelL,yskelL,xskelR,yskelR,ii,lph,lpt,lpf,hh,ic,jc,bool1,bool2 
  real(pr) :: Tps, threshold, surf, maxv, minimL, minimL_m, minimL_p, minimR, long, tb, dtb, long2, long3, dti, dtf, dsi, dsf,xr,yr&
       ,xl,yl,long00,tp,longexp,longratio, tbb
  real(pr),dimension(:),allocatable :: longslice, longslicei, longslicef, longTh, longTheta
  !  real(pr), dimension(:,:), allocatable :: rho, uu, vv, u0, v0, u, v, rhou, rhov 
  real(pr) :: t, tPic, xi, yj, distW, distWb, xLeft, yLeft, xRight, yRight
  real(pr), dimension(:,:), allocatable :: rhoSlices2,gradPhi
  integer, dimension(:,:), allocatable :: midline,midlinebis
  real(pr), dimension(:,:), allocatable :: tmp, tmp1, tmp2, un, zero,distslice,voisin
  integer, dimension(:,:), allocatable :: dir1, dir2, dir3, dir4, Nseed, skel, skel2, tmpbool, skel3
  real(pr) :: px,py,pz,s0,sinit,tbm,tbp,s,ds,rr,pxx,pyy,pzz
  real(pr),dimension(:),allocatable :: sslice,dsslice,stheta,dstheta,sth,dsth
  real(pr),dimension(:,:),allocatable :: points_control, points_courbe, points_courbe_equal, tail_courbe, head_courbe&
  ,points_courbe_equal_ref
  real(pr),dimension(:,:,:),allocatable :: slicecontrolm, slicecontrolf, slicecontrol,slicemid,slicecontroli,slicecontroltmpf&
  ,slicecontroltmpi,slicecontroltmp
  real(pr),dimension(:,:,:),allocatable :: slice_courbe, slice_courbe_equal
  integer :: Ns,Ni,Nf,nt,errorl,errorr,itail,ihead
  real(pr) :: deltal,deltar,disttail,disthead,rhead,rtail,disttaillY,disttailrY,distheadlY,distheadrY
  real(pr),dimension(:,:,:),allocatable :: slice,slice2,vect,slicetmp
  integer,dimension(:),allocatable :: thetatab, indextheta
  integer, dimension(:,:), allocatable :: indextab
  real(pr),dimension(:),allocatable :: xTheta,yTheta
  real(pr),dimension(:,:),allocatable :: valDist,valTheta,valTh,valThtmp
  integer :: th,theta,nbtheta,boolskel
  real(pr) :: PI, sigma, sigma2 
  real(pr) :: cosPhi,sinPhi,cosTheta,sinTheta,oldS,oldC,cosAlpha,sinAlpha,cosPhil,cosPhir,sinPhil,sinPhir
  real(pr), dimension(:,:), allocatable :: cosTheta_tab,sinTheta_tab
  real(pr), dimension(:,:,:), allocatable :: rhoSlices
  real(pr) :: zslice,area,meshRatio
  real(pr) :: x1,x2,y1,y2,z1,z2,xc,yc,zc,delta,rt,xt,yt,xp,yp,x0,xg,yg,xgref,ygref
  real(pr) :: LS1,LS2,LS3,LS4,LSp,LSrr,rp,alpha,alphadef
  integer :: ip1,ip2,jp1,jp2
  real(pr),dimension(:,:,:),allocatable :: slice_courbemidLarge,slicecontrolLarge
  real(pr),dimension(:,:),allocatable :: valThLarge, valThetaLarge
  integer :: nbthetaLarge, NsLarge

  !**
  !! Main Initialisation
  !**
  meshRatio = 0.24_pr
  meshRatio = 1._pr !0.2_pr
  !meshRatio = 0.3_pr
  area = 1._pr !0.2_pr
  PI = acos(-1.0_pr)
  sigma = 1._pr
  sigma2 = 1._pr
  !N =  200 
  !nx = 200 
  !ny = 200 
  !nz = 200
  nx = 1602 
  ny = 300 
  nz = 300
  zslice = 150._pr
  x0 = 1._pr
  eepsilon = 1.e-6_pr
  dt = 1._pr
  threshold = 0.001
  !dx = 1._pr 
  !dy = 1._pr 
  !dz = 1._pr
  dx = 2.4 !24 !2.4!*0.000001_pr 
  dy = 2.25171 !24.7687 !2.25171!*0.000001_pr 
  dz = 2.25171 !24.7687 !2.25171!*0.000001_pr
  Tps = 1._pr
!  Ns = 290 
!  Ni = 22  
!  Nf = 30  
  !Ns = 180 
  Ns = 263 
  Ni = 4  
!  Nf = 54  
  !Nf = 28  
  Nf = 35  
  dtb = 1._pr/(Ns-1)
  nbtheta = 180 
  !nbtheta = 270 
  NsLarge = 900
  nbthetaLarge = 180!200 !3600

  !**
  !! Main Allocation of arrays
  !**
  allocate(cosTheta_tab(Ns+Ni+Nf-2,nbtheta),sinTheta_tab(Ns+Ni+Nf-2,nbtheta))
  allocate(slicemid(nbtheta,Ns,3))
  allocate(longslice(nbtheta),longslicei(nbtheta),longslicef(nbtheta),sslice(nbtheta),dsslice(nbtheta),stheta(Ns+Ni+Nf-2)&
       ,dstheta(Ns+Ni+Nf-2),sth(nx),dsth(nx))
  allocate(thetatab(nbtheta),indextheta(Ns+Ni+Nf-2),indextab(nbtheta,Ns+Ni+Nf-2))
  allocate(xTheta(nbtheta))
  allocate(yTheta(nbtheta))
  allocate(valTheta(Ns+Ni+Nf-2,nbtheta),longTheta(Ns+Ni+Nf-2),longTh(nx))
  allocate(valDist(Ns+Ni+Nf-2,nbTheta))
  allocate(head_courbe(Nf,3),tail_courbe(Ni,3))
  allocate(points_courbe(Ns,3))
  allocate(points_courbe_equal(Ns,3))
  allocate(points_courbe_equal_ref(Ns,3))
  allocate(rhoSlices2(nx,ny),gradPhi(nx,ny))
  !allocate(un(N,N),zero(N,N))
  allocate(tmp(nx,ny),tmp1(nx,ny),tmp2(nx,ny),tmpbool(nx,ny))
  allocate(rhoSlices(nz,nx,ny))
  allocate(xx(nx))
  allocate(yy(ny)) 
  allocate(zz(nz)) 
  !  allocate(xp(1:nx,1:ny),yp(1:nx,1:ny)) 
  !  allocate(rho0(1:nx,1:ny)) 
  !  allocate(rhou(1:nx,1:ny),rhov(1:nx,1:ny))  
  !  allocate(rhouu(1:nx,1:ny),rhovv(1:nx,1:ny)) 
  !  allocate(rhouup(1:nx,1:ny),rhovvp(1:nx,1:ny)) 
  !  allocate(rhop(1:nx,1:ny),rhoup(1:nx,1:ny),rhovp(1:nx,1:ny)) 
  !  allocate(u(1:nx,1:ny),v(1:nx,1:ny)) 
  !  allocate(rho(1:nx,1:ny))
  !  allocate(uu(1:nx,1:ny), vv(1:nx,1:ny))
  !  allocate(u0(1:nx,1:ny), v0(1:nx,1:ny))
  allocate(dir1(nx,ny),dir2(nx,ny),dir3(nx,ny),dir4(nx,ny),Nseed(nx,ny),skel(nx,ny),skel2(nx,ny),skel3(nx,ny))
  allocate(distslice(2*(Ns+Ni+Nf),3))
  allocate(slice_courbe(nbtheta,Ns+Ni+Nf-2,3),slice_courbemidLarge(nbtheta,NsLarge+Ni+Nf-2,3))
  allocate(slice_courbe_equal(nbtheta,Ns+Ni+Nf-2,3))
  allocate(slice(nbtheta,Ns+Ni+Nf-2,3),slicetmp(nbtheta,Ns+Ni+Nf-2,3))
  allocate(slice2(nbtheta,Ns+Ni+Nf-2,3),vect(nbtheta,Ns+Ni+Nf-2,3))
  allocate(valThetaLarge(Ns+Ni+Nf-2,nbthetaLarge))

  x0 = 0._pr
  do i=1,nx
     xx(i) = x0 + (float(i)-1)*dx
  enddo
  do j=1,ny
     yy(j) = x0 + (float(j)-1)*dy
  enddo
  do k=1,nz
     zz(k) = x0 + (float(k)-1)*dz
  enddo
  do l=1,size(valTheta,1)
     do theta=1,nbtheta/4+1
        t = (theta-1)/(nbtheta*0.25_pr)
        !t = ((1-exp(-sigma*t))/(1-exp(-sigma)))
        !t = ((1-exp(-sigma*t**2))/(1-exp(-sigma)))
        !valTheta(l,theta) = (nbtheta/2+1-1)*2*PI/nbtheta*t
        valTheta(l,theta) = (nbtheta/4+1-1)*2*PI/nbtheta*t
     enddo
     do theta=nbtheta/4+1,nbtheta/2+1
        t = (theta-(nbtheta/4+1))/(nbtheta*0.5_pr-nbtheta*0.25_pr)
        !t = ((1-exp(sigma*t))/(1-exp(sigma)))
        !t = ((1-exp(sigma*t**2))/(1-exp(sigma)))
        valTheta(l,theta) = (nbtheta/4+1-1)*2*PI/nbtheta*(1-t) + (nbtheta/2+1-1)*2*PI/nbtheta*t
     enddo
     do theta=nbtheta/2+1,nbtheta*3/4+1
        t = (theta-(nbtheta/2+1))/(nbtheta*0.75_pr-nbtheta*0.5_pr)
        !t = ((1-exp(-sigma2*t))/(1-exp(-sigma2)))
        !t = ((1-exp(-sigma2*t**2))/(1-exp(-sigma2)))
        valTheta(l,theta) = (nbtheta/2+1-1)*2*PI/nbtheta*(1-t) + (nbtheta*3/4+1-1)*2*PI/nbtheta*t
        !        valTheta(l,theta) = (theta-1)*2*PI/nbtheta
     enddo
     do theta=nbtheta*3/4+1,nbtheta
        t = (theta-(nbtheta*3/4+1))/(nbtheta-(nbtheta*0.75_pr+1._pr))
        !t = ((1-exp(sigma2*t))/(1-exp(sigma2)))
        !t = ((1-exp(sigma*t**2))/(1-exp(sigma)))
        valTheta(l,theta) = (nbtheta*3/4+1-1)*2*PI/nbtheta*(1-t) + (nbtheta-1)*2*PI/nbtheta*t
        !        valTheta(l,theta) = (theta-1)*2*PI/nbtheta
     enddo
     !     valTheta(l,1) = 0._pr
     !     valTheta(l,2) = PI*0.5_pr
     !     valTheta(l,3) = PI  
     !     valTheta(l,4) = PI*1.5_pr  
     !     if (l==1) then
     !             do theta=1,nbtheta-1
     !                write(*,*) "THETA  ",theta," ",valTheta(l,theta),"  dTh   ",valTheta(l,theta+1)-valTheta(l,theta)
     !                enddo
     !        endif
  enddo
  !!  do l=1,size(valTh,1)
  !!     do theta=1,nbtheta/4+1
  !!        t = (theta-1)/(nbtheta*0.25_pr)
  !!        t = ((1-exp(-sigma*t))/(1-exp(-sigma)))
  !!        !t = ((1-exp(-sigma*t**2))/(1-exp(-sigma)))
  !!        valTh(l,theta) = (nbtheta/4+1-1)*2*PI/nbtheta*t
  !!     enddo
  !!     do theta=nbtheta/4+1,nbtheta/2+1
  !!        t = (theta-(nbtheta/4+1))/(nbtheta*0.5_pr-nbtheta*0.25_pr)
  !!        t = ((1-exp(sigma*t))/(1-exp(sigma)))
  !!        !t = ((1-exp(sigma*t**2))/(1-exp(sigma)))
  !!        valTh(l,theta) = (nbtheta/4+1-1)*2*PI/nbtheta*(1-t) + (nbtheta/2+1-1)*2*PI/nbtheta*t
  !!     enddo
  !!     do theta=nbtheta/2+1,nbtheta*3/4+1
  !!        t = (theta-(nbtheta/2+1))/(nbtheta*0.75_pr-nbtheta*0.5_pr)
  !!        t = ((1-exp(-sigma2*t))/(1-exp(-sigma2)))
  !!        !t = ((1-exp(-sigma2*t**2))/(1-exp(-sigma2)))
  !!        valTh(l,theta) = (nbtheta/2+1-1)*2*PI/nbtheta*(1-t) + (nbtheta*3/4+1-1)*2*PI/nbtheta*t
  !!     enddo
  !!     do theta=nbtheta*3/4+1,nbtheta
  !!        t = (theta-(nbtheta*3/4+1))/(nbtheta-(nbtheta*0.75_pr+1._pr))
  !!        t = ((1-exp(sigma2*t))/(1-exp(sigma2)))
  !!        !t = ((1-exp(sigma2*t**2))/(1-exp(sigma2)))
  !!        valTh(l,theta) = (nbtheta*3/4+1-1)*2*PI/nbtheta*(1-t) + (nbtheta-1)*2*PI/nbtheta*t
  !!     enddo
  !!  enddo
  !  do l=1,size(valTh,1)
  !     do theta=1,nbtheta
  !        valTh(l,theta) = (theta-1)*2*PI/nbtheta
  !     enddo
  !  enddo
  !  write(*,*) "SIZE THETA ",size(valTheta,1)
  !!open(unit=78,file='/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_filt2.dat',status='unknown')
  !!!open(unit=78,file='/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_filt5.dat',status='unknown')
  !!open(unit=78,file='/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_filt4.dat',status='unknown')
  !!open(unit=78,file='/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_filt3.dat',status='unknown')
  !open(unit=78,file='/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_v2filt.dat',status='unknown')
  open(unit=78,file='/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/results_plafrim/rhoSlices_v2.dat',status='unknown')
  do k=1,nx
     do j=1,ny
        do i=1,nz
           read(78,*) rhoSlices(i,k,j)
        enddo
     enddo
  enddo
  close(78)
  do k=1,nx
    rhoSlices(:,k,:) = rhoSlices(:,k,:)/maxval(rhoSlices(:,k,:))
  enddo
  !  where (rhoSlices>=0.2) rhoSlices=1._pr !0.5
  !  where (rhoSlices<0.2) rhoSlices=0._pr !-0.5
  rhoSlices = 10*rhoSlices !- 0.5_pr
  do i=1,nz
     do k=1,nx
        do j=1,ny
           if (rhoSlices(i,k,j)>1._pr) then
              rhoSlices(i,k,j) = 1._pr
              !else
              !rhoSlices(i,k,j) = 0.
           endif
        enddo
     enddo
  enddo
  rhoSlices = rhoSlices - 0.5_pr*(maxval(rhoSlices)+minval(rhoSlices))
!  !! FILM
!  !open(unit=79,file='results/rho05.vtk',status='unknown')
!  !open(unit=79,file='results_secondTEST6/rho05.vtk',status='unknown')
!  open(unit=79,file='results_test/rho05.vtk',status='unknown')
!  write(79,'(1A26)') '# vtk DataFile Version 2.0'
!  write(79,'(a)') 'rho'
!  write(79,'(a)') 'ASCII'
!  write(79,'(a)') 'DATASET STRUCTURED_POINTS'
!  write(79,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,nz
!  write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
!  write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx, dy, dz
!  !write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 24.768700, 24.768700, 24.000000
!  write(79,'(a,I9)') 'POINT_DATA' , nx*ny*nz
!  write(79,'(a)') 'SCALARS values double'
!  write(79,'(a)') 'LOOKUP_TABLE default'
!  do k=1,nz
!     do j=1,ny
!        do i=1,nx
!           !if (abs(rhoSlices(i,k,j)).lt.1e-6) then
!           !  write(79,*) 0._pr
!           !else
!           write(79,*) rhoSlices(i,k,j)
!           !endif
!        enddo
!     enddo
!  enddo
!  close(79)    

  !**
  !! Loading the 2D silhouette
  !**

  !picNum = 140
  !! FILM
  picNum = 1 !G3
  !picnum = 80 !5dpf5
  !picnum = 220 !serie2_13
  !picnum = 160 !5dpf3
  !  pos = nx/2
  !  yLeft = ny
  !  xLeft = pos
  !  yRight = 1
  !  xRight = pos
  !  open(unit=78,file="/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_inidroit.txt",status='unknown')
  !  do k=1,nx
  !     do j=1,ny
  !        read(78,*) rhoSlices2(k,j)
  !     enddo
  !  enddo
  !  close(78)
  rhoSlices2(:,:) = rhoSlices(nint(zslice),:,:)
  rhoSlices2 = rhoSlices2 - 0.5_pr*(maxval(rhoSlices2) + minval(rhoSlices2))
  !!  open(unit=79,file='results/sol00.vtk',status='unknown')
  !!  write(79,'(1A26)') '# vtk DataFile Version 2.0'
  !!  write(79,'(a)') 'rho'
  !!  write(79,'(a)') 'ASCII'
  !!  write(79,'(a)') 'DATASET STRUCTURED_POINTS'
  !!  write(79,'(a,I4,I4,I4)') 'DIMENSIONS', N,ny,1
  !!  write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
  !!  write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,1. 
  !!  write(79,'(a,I6)') 'POINT_DATA          ' , N*ny 
  !!  write(79,'(a)') 'SCALARS values double'
  !!  write(79,'(a)') 'LOOKUP_TABLE default'
  !!
  !!  do k=1,ny
  !!     do j=1,N
  !!        write(79,*) rhoSlices2(j,k)
  !!     enddo
  !!  enddo
  !!  close(79)    
  dy = 2.4 !24 !2.4!*0.000001_pr 
  dx = 2.25171 !24.7687 !2.25171!*0.000001_pr 
  dz = 2.25171 !24.7687 !2.25171!*0.000001_pr

  !**
  !! Now, the level-set 3D is computed in rhoSlices
  !**
  call updateDistance3D(rhoSlices,gradPhi,zslice)
  !**
  !! Now, the level-set 2D is computed in rhoSlices2
  !**
  call updateDistanceINI(rhoSlices2,gradPhi)
  dx = 2.4*0.000001_pr 
  dy = 2.25171*0.000001_pr 
  dz = 2.25171*0.000001_pr
  do i=1,nx
     xx(i) = x0 + (float(i)-1)*dx
  enddo
  do j=1,ny
     yy(j) = x0 + (float(j)-1)*dy
  enddo
  do k=1,nz
     zz(k) = x0 + (float(k)-1)*dz
  enddo
  !! FILM
  !open(unit=79,file='results/sol00.vtk',status='unknown')
  !open(unit=79,file='results_secondTEST6/sol00.vtk',status='unknown')
  open(unit=79,file='results_test/sol00.vtk',status='unknown')
  write(79,'(1A26)') '# vtk DataFile Version 2.0'
  write(79,'(a)') 'rho'
  write(79,'(a)') 'ASCII'
  write(79,'(a)') 'DATASET STRUCTURED_POINTS'
  !write(79,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,nz
  write(79,'(a,I4,I4,I4)') 'DIMENSIONS', ny,nz,1
  write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
  !write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx, dy, dz
  write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 1._pr, 1._pr, 1._pr
  !write(79,'(a,I9)') 'POINT_DATA' , nx*ny*nz
  write(79,'(a,I9)') 'POINT_DATA' , ny*nz
  write(79,'(a)') 'SCALARS values double'
  write(79,'(a)') 'LOOKUP_TABLE default'
!  do k=1,nx
     do j=1,ny
        do i=1,nz
           !write(79,*) rhoSlices(i,1596,j)
           write(79,*) rhoSlices(i,6,j)
        enddo
     enddo
!  enddo
  close(79)    
!  !! FILM
!  !open(unit=79,file='results/sol00.txt',status='unknown')
!  !open(unit=79,file='results_secondTEST6/sol00.txt',status='unknown')
!  open(unit=79,file='results_test/sol00.txt',status='unknown')
!  do k=1,nz
!     do j=1,ny
!        do i=1,nx
!           !if (rhoSlices(k,i,j)>0._pr) write(79,*) xx(i)," ",yy(j)," ",zz(k)
!           if ((i==72).and.(rhoSlices(k,i,j)>0._pr)) write(79,*) xx(i)," ",yy(j)," ",zz(k)
!        enddo
!     enddo
!  enddo
!  close(79)
  !rhoSlices2(:,:) = rhoSlices(nint(zslice),:,:)
  !rhoSlices2 = (2*rhoSlices2-minval(rhoSlices2)-maxval(rhoSlices2))/(maxval(rhoSlices2)-minval(rhoSlices2))
  !rhoSlices2 = rhoSlices2/maxval(rhoSlices2)

  !**
  !! Definition of the zebrafish length
  !**

  !! FILM
  !long00 = 179.50000000000000 - 19.414213562373096
  !longexp = 152.61
  !long00 = 3.8424001526832577E-003 - 0._pr
  long00 = 3.8647606990128360E-003 + 1.3658546530224907E-005 !3.8268043996381122E-003 - 8.6686879182832310E-006
  !longexp = 152.61*0.001_pr*0.0301
  !longexp = 158*0.001_pr*0.0256
  longexp = 164*0.001_pr*0.0256
  !longexp = 167*0.001_pr*0.0256
  longratio = longexp/long00
  write(*,*) "longueurs initiales ",long00," ",longexp," ",longratio

  !**
  !! Construction of the initial midline
  !**

  dir1 = 0
  dir2 = 0
  dir3 = 0
  dir4 = 0
  Nseed = 0
  skel = 0
  do i=2,ny-1
     do j=2,nx-1
        !!        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j))) dir1(i,j) = 1
        !!        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j-1))) dir2(i,j) = 1
        !!        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j-1))) dir3(i,j) = 1
        !!        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j-1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j+1))) dir4(i,j) = 1
        !!        Nseed(i,j) = dir1(i,j) + dir2(i,j) + dir3(i,j) + dir4(i,j)
        !!        if ((Nseed(i,j)>=2).and.(rhoSlices2(i,j)>0)) skel(i,j) = 1
        !!        if ((i==146).and.(j==109)) skel(i,j) = 0 !write(*,*) "testpoint ",Nseed(i,j)
        !!        if ((i==147).and.(j==109)) skel(i,j) = 0 !write(*,*) "testpoint ",Nseed(i,j)
        !if ((gradPhi(j,i)<0.57).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
        !if ((gradPhi(j,i)<0.73).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
        if ((gradPhi(j,i)<0.74).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
     enddo
  enddo
!!  l = 1
!!  pix1 = 0
!!  pix2 = 0
!!  do while ((pix1==1).or.(pix2==1).or.(l==1))
!!     pix1 = 0
!!     pix2 = 0
!!     l = l + 1
!!     skel2 = skel
!!     do i=2,nx-1
!!        do j=2,ny-1
!!           maxv = 0
!!           if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
!!           if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
!!           if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
!!           if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
!!           if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
!!           if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
!!           if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
!!           if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
!!           Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
!!                skel(i+1,j-1)
!!           if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i+1,j)==0).or.(skel(i-1,j)==0)&
!!                .or.(skel(i,j-1)==0)).and.((skel(i,j-1)==0).or.(skel(i,j+1)==0).or.(skel(i,j+1)==0))) then
!!              skel2(i,j) = 0
!!              pix1 = 1
!!           endif
!!        enddo
!!     enddo
!!     skel = skel2
!!     skel2 = skel
!!     do i=2,nx-1
!!        do j=2,ny-1
!!           maxv = 0
!!           if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
!!           if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
!!           if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
!!           if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
!!           if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
!!           if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
!!           if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
!!           if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
!!           Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
!!                skel(i+1,j-1)
!!           if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i,j+1)==0).or.(skel(i-1,j)==0)&
!!                .or.(skel(i,j-1)==0)).and.((skel(i+1,j)==0).or.(skel(i,j+1)==0).or.(skel(i-1,j)==0))) then
!!              skel2(i,j) = 0
!!              pix2 = 1
!!           endif
!!        enddo
!!     enddo
!!     skel = skel2
!!  enddo
!!  skel2 = skel
!!  do i=2,nx-1
!!     do j=2,ny-1
!!        Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
!!             skel(i+1,j-1)
!!        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
!!        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
!!        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
!!        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i-1,j+1)==1)) skel2(i,j) = 1
!!        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i,j+1)==1)) skel2(i,j) = 1
!!        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j)==1)) skel2(i,j) = 1
!!        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
!!        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j+1)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
!!     enddo
!!  enddo
!!  skel = skel2
!!  skel2 = skel
!!  do i=2,nx-1
!!     do j=2,ny-1
!!        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
!!             skel2(i+1,j-1)
!!        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j)==1).and.(skel2(i-1,j+1)==1)) skel2(i-1,j) = 0
!!        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
!!             skel2(i+1,j-1)
!!        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j+1)==1).and.(skel2(i,j+1)==1)) skel2(i,j+1) = 0
!!        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
!!             skel2(i+1,j-1)
!!        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j+1)==1).and.(skel2(i+1,j+1)==1)) skel2(i,j+1) = 0
!!        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
!!             skel2(i+1,j-1)
!!        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j+1)==1).and.(skel2(i+1,j)==1)) skel2(i+1,j) = 0
!!        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
!!             skel2(i+1,j-1)
!!        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j)==1).and.(skel2(i+1,j-1)==1)) skel2(i+1,j) = 0
!!        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
!!             skel2(i+1,j-1)
!!        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j-1)==1).and.(skel2(i,j-1)==1)) skel2(i,j-1) = 0
!!        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
!!             skel2(i+1,j-1)
!!        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j-1)==1).and.(skel2(i-1,j-1)==1)) skel2(i,j-1) = 0
!!        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
!!             skel2(i+1,j-1)
!!        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j-1)==1).and.(skel2(i-1,j)==1)) skel2(i-1,j) = 0
!!        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
!!             skel2(i+1,j-1)
!!        if ((skel2(i,j)==1).and.(Nseed(i,j)==0)) skel2(i,j) = 0
!!     enddo
!!  enddo
!!  skel = skel2


  tmpbool = 0
  nl = sum(skel)
  sizeSkel = sum(skel)
  allocate(midlinebis(sizeSkel,3))!,xLeft_tab(sizeSkel), yLeft_tab(sizeSkel), xRight_tab(sizeSkel), yRight_tab(sizeSkel))
  l=1
  midlinebis = 0
  do j=100,nx
     do k=1,ny
        if ((skel(j,k)==1).and.(l==1)) then
!           midlinebis(l,1) = j
           midlinebis(l,2) = k
           if (j==nx/2) l0 = l
           l = l+1
        endif
     enddo
  enddo
  l=1
  do j=1,nx
     do k=1,ny
        if ((skel(j,k)==1).and.(l==1)) then
           midlinebis(l,1) = j
!           midlinebis(l,2) = k
           if (j==nx/2) l0 = l
           l = l+1
        endif
     enddo
  enddo
  midlinebis(l,3) = nint(zslice)
  !**
  !! first point of the midline (tail)
  !**
  xskelL = midlinebis(1,1)
  yskelL = midlinebis(1,2)
  lp = 1
  boolskel=0
!!  do i=2,nx-1
!!     do j=2,ny-1
!!        if (skel(i,j)==1) then
!!           Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
!!                skel(i+1,j-1)
!!           if ((Nseed(i,j)>1).and.((midlinebis(lp,1)==i).or.(midlinebis(lp,1)==i-1).or.(midlinebis(lp,1)==i+1)).and.&
!!                ((midlinebis(lp,2)==j).or.(midlinebis(lp,2)==j-1).or.(midlinebis(lp,2)==j+1)).and.&
!!                (appartient(midlinebis,i,j,lp+1).neqv..true.))&
!!                then 
!!              lp=lp+1
!!              !!midlinebis(lp,1) = i
!!              midlinebis(lp,2) = j
!!           endif
!!        endif
!!     enddo
!!   enddo

  !do i=2,nx-1
  do i=midlinebis(1,1)+1,nx-1
     !**
     !! horizontal midline
     !**
     if (lp<=1462-midlinebis(1,1)+1) then
        if (lp<20) write(*,*) "MIDLINEBIS : ",midlinebis(lp,1)," ",midlinebis(lp,2)
        lp=lp+1
        midlinebis(lp,1) = i
        midlinebis(lp,2) = midlinebis(1,2)
        !midlinebis(lp,2) = ny/2
     endif
  enddo
  midlinebis(lp,3) = nint(zslice)
  nl = lp
  !**
  !! Now we have the midline coordinates
  !**
  allocate(midline(nl,3))
  midline(1:nl,1) = midlinebis(1:nl,1)
  midline(1:nl,2) = midlinebis(1:nl,2)
  midline(1:nl,3) = midlinebis(1:nl,3)
  !!        rr = dx
  !!        x1 = xx(midline(nl-10,1))
  !!        y1 = xx(midline(nl-10,2))
  !!        x2 = xx(midline(nl,1))
  !!        y2 = xx(midline(nl,2))
  !!        sinPhi = (x2-x1)/(sqrt((x1-x2)**2+(y1-y2)**2))
  !!        cosPhi = (y2-y1)/(sqrt((x1-x2)**2+(y1-y2)**2)) 
  !!        !do while ((rhoSlices(nint(zslice),nint(x2 + rr*sinPhi),nint(y2 + rr*cosPhi))>0._pr).and.(rr<float(ny)))
  !!        do while ((rhoSlices2(nint(x2 + rr*sinPhi),nint(y2 + rr*cosPhi))>0._pr).and.(rr<float(ny)))
  !!           rr=rr+dx
  !!        enddo
  !!        xc = x2 + (rr-1.5*dx)*sinPhi !+sinPhi*rhoSlices2(nint(x2 + (rr-dx)*sinPhi),nint(y2 + (rr-dx)*cosPhi))*dy/abs(rhoSlices2(nint(x2 + rr*sinPhi),nint(y2 + rr*cosPhi)) -rhoSlices2(nint(x2 + (rr-dx)*sinPhi),nint(y2 + (rr-dx)*cosPhi)))
  !!        yc = y2 + (rr-1.5*dx)*cosPhi !+cosPhi*rhoSlices2(nint(x2 + (rr-dx)*sinPhi),nint(y2 + (rr-dx)*cosPhi))*dy/abs(rhoSlices2(nint(x2 + rr*sinPhi),nint(y2 + rr*cosPhi)) -rhoSlices2(nint(x2 + (rr-dx)*sinPhi),nint(y2 + (rr-dx)*cosPhi)))

  !**
  !! Definition of the control points of the midline
  !**
  allocate(points_control(nl,3))!,leftcontrolm(nl,2),rightcontrolm(nl,2))
  do l=1,nl
     points_control(l,1) = xx(midline(l,1))
     !points_control(l,2) = yy(midline(l,2))
     points_control(l,2) = yy(midline(1,2))!yy(midline(l,2))
     points_control(l,3) = zz(nint(zslice))
  enddo
  !! TESTNEZ
  !!  points_control(nl,1) = xc
  !!  points_control(nl,2) = yc
  !!  points_control(nl,3) = zslice

  !**
  !! Spline approximation of the midline
  !**
  points_courbe(1,1) = points_control(1,1)
  points_courbe(1,2) = points_control(1,2)
  points_courbe(1,3) = points_control(1,3)

  tb = dtb
  l = 1
  long = 0._pr
  do while ((tb<1._pr).and.(l+1<Ns+1))
     l = l+1
     !     call pointsBezierN(points_control,tb,px,py)
     call pointsBezierN3D(points_control,tb,px,py,pz)
     points_courbe(l,1) = px
     points_courbe(l,2) = py
     points_courbe(l,3) = pz
     long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))**2 + (points_courbe(l,2)-points_courbe(l-1,2))**2 +&
          (points_courbe(l,3)-points_courbe(l-1,3))**2)
     tb = tb+dtb
  enddo
  if (l==Ns-1) then
     l = l+1
     write(*,*) "ET VOILA"
     points_courbe(Ns,1) = points_control(size(points_control,1),1)
     points_courbe(Ns,2) = points_control(size(points_control,1),2)
     points_courbe(Ns,3) = points_control(size(points_control,1),3)
     long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))**2 + (points_courbe(l,2)-points_courbe(l-1,2))**2 +&
          (points_courbe(l,3)-points_courbe(l-1,3))**2)
  endif
  if (.not.(l==Ns)) write(*,*) "WARNIINNGG ",l
  write(*,*) "longueur ==  ",(points_control(size(points_control,1),1)-points_control(1,1))," ",long," ",(points_courbe(Ns,1)-&
       points_courbe(1,1))," ",points_courbe(1,1)," ",points_courbe(Ns,1)
  long2 = long
  ds = long/(Ns-1)
  !ds = 0.1*long/(0.2*Ns-1)
  !ds = area*long/(meshRatio*Ns-1)

  !**
  !! Uniform spline approximation of the midline
  !**
  points_courbe_equal(1,1) = points_control(1,1)
  points_courbe_equal(1,2) = points_control(1,2)
  points_courbe_equal(1,3) = points_control(1,3)
  l = 1
  long = 0._pr
  dtb = 1._pr/(Ns-1)
  tb = 0._pr
  !do while ((tb<1._pr).and.(l+1<meshRatio*Ns+1))
  !do while ((tb<1._pr).and.(l+1<Ns+1))
  do while ((tb<1._pr).and.(l+1<Ns))
     l = l+1
     nt = 1
     s = 0._pr
     do while ((l-1)*ds-s>0._pr) 
        nt = nt+1
        s = s + sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)-points_courbe(nt-1,2))**2 +&
             (points_courbe(nt,3)-points_courbe(nt-1,3))**2)
     enddo
     tbm = (nt-2)*dtb
     tbp = (nt-1)*dtb
     tb = tbm !(tbm + tbp)*0.5_pr
     s0 = s
     sinit = s - sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)-points_courbe(nt-1,2))**2 +&
          (points_courbe(nt,3)-points_courbe(nt-1,3))**2)
     s = sinit
     bool = 0
     !do while ((abs((l-1)*ds-s)>eepsilon*dx).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
     do while ((abs((l-1)*ds-s)/dx>eepsilon).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
        tb = (tbm + tbp)*0.5_pr
        !        call pointsBezierN(points_control,tb,px,py)
        !call pointsBezierN3D(points_control(1:nint(0.1*size(points_control,1)),:),0.1*tb,px,py,pz)
        call pointsBezierN3D(points_control,tb,px,py,pz)
        s = sinit + sqrt((px-points_courbe(nt-1,1))**2 + (py-points_courbe(nt-1,2))**2 + (pz-points_courbe(nt-1,3))**2)
        if ((l-1)*ds-s>0._pr) then
           tbm = tb
        else
           tbp = tb
           if (tbp>1._pr) tbp = 1._pr
        endif
        if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
           bool = 1
           !           write(*,*) "aie aie aie"
        endif
     enddo
     !     call pointsBezierN(points_control,tb,px,py)
     write(*,*) "TBpoints  ",l," ",tb," ",ds," ",tbm," ",tbp
     !call pointsBezierN3D(points_control(1:nint(0.1*size(points_control,1)),:),0.1*tb,px,py,pz)
     call pointsBezierN3D(points_control,tb,px,py,pz)
     points_courbe_equal(l,1) = px
     points_courbe_equal(l,2) = py
     points_courbe_equal(l,3) = pz
     long = long +&
          sqrt((points_courbe_equal(l,1)-points_courbe_equal(l-1,1))**2 + (points_courbe_equal(l,2)-points_courbe_equal(l-1,2))**2&
          + (points_courbe_equal(l,3)-points_courbe_equal(l-1,3))**2)
     write(*,*) "LLfirst  ",l,"       ",px," ",py," ",pz," ",ds
  enddo
  lp = l
!  !ds = 0.9*long2/(0.8*Ns-1)
!  !ds = 0.9*long2/(Ns-lp)
!  !ds = 0.8*long2/(0.6*Ns-1)
!  ds = (1._pr-2._pr*area)*long2/((1._pr-meshRatio)*Ns-lp)
!  !write(*,*) "new long  ",long2," ",area*long2," ",0.9*long2," ",long2-long
!  dtb = 1._pr/(Ns-1)
!  tp = tb
!  tb = 0._pr !dtb
!  l = lp !1
!  do while ((tb<1._pr).and.(l+1<(1._pr-meshRatio)*Ns+1))
!     !do while ((tb<1._pr+tp).and.(l+1<Ns+1))
!     !do while ((tb<1._pr).and.(l+1<Ns+1))
!     l = l+1
!     nt = 1
!     s = 0._pr
!     do while ((lp-1)*area*long2/(meshRatio*Ns-1)+(l-lp)*ds-s>0._pr) 
!        nt = nt+1
!        !        if ((nt-1)>lp) then
!        s = s + sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)&
!             -points_courbe(nt-1,2))**2 + (points_courbe(nt-1,3)-points_courbe(nt-1,3))**2)
!        !        else if (nt-1==lp) then
!        !        s = s + sqrt((points_courbe(nt,1)-points_courbe_equal(nt-1,1))**2 + (points_courbe(nt,2)&
!        !-points_courbe_equal(nt-1,2))**2 + (points_courbe(nt-1,3)-points_courbe_equal(nt-1,3))**2)
!        !        else
!        !        s = s + sqrt((points_courbe_equal(nt,1)-points_courbe_equal(nt-1,1))**2 + (points_courbe_equal(nt,2)&
!        !-points_courbe_equal(nt-1,2))**2 + (points_courbe_equal(nt-1,3)-points_courbe_equal(nt-1,3))**2)
!        !        endif
!        !        s = s + sqrt((points_courbe(nt+lp-1,1)-points_courbe(nt+lp-1-1,1))**2 + (points_courbe(nt+lp-1,2)&
!        !-points_courbe(nt+lp-1-1,2))**2 + (points_courbe(nt+lp-1,3)-points_courbe(nt+lp-1-1,3))**2)
!     enddo
!     !     tbm = (nt-2)*dtb!-tp+dtb
!     !     tbp = (nt-1)*dtb!-tp+dtb
!     tbm = (nt-2)*dtb!-tp+dtb
!     tbp = (nt-1)*dtb!-tp+dtb
!     !     tbm = (nt+lp-1-2)*dtb!-tp+dtb
!     !     tbp = (nt+lp-1-1)*dtb!-tp+dtb
!     !     tbm = ((nt+lp-1-2)*dtb-tp+dtb)/(1._pr+tp-dtb)
!     !     tbp = ((nt+lp-1-1)*dtb-tp+dtb)/(1._pr+tp-dtb)
!     tb = tbm !(tbm + tbp)*0.5_pr
!     s0 = s
!     !        if ((nt-1)>lp) then
!     sinit = s - sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)&
!          -points_courbe(nt-1,2))**2 + (points_courbe(nt,3)-points_courbe(nt-1,3))**2)
!     !        else if (nt-1==lp) then
!     !     sinit = s - sqrt((points_courbe(nt,1)-points_courbe_equal(nt-1,1))**2 + (points_courbe(nt,2)&
!     !-points_courbe_equal(nt-1,2))**2 + (points_courbe(nt,3)-points_courbe_equal(nt-1,3))**2)
!     !        else
!     !     sinit = s - sqrt((points_courbe_equal(nt,1)-points_courbe_equal(nt-1,1))**2 + (points_courbe_equal(nt,2)&
!     !-points_courbe_equal(nt-1,2))**2 + (points_courbe_equal(nt,3)-points_courbe_equal(nt-1,3))**2)
!     !        endif
!     !     sinit = s - sqrt((points_courbe(nt+lp-1,1)-points_courbe(nt+lp-1-1,1))**2 + (points_courbe(nt+lp-1,2)&
!     !-points_courbe(nt+lp-1-1,2))**2 + (points_courbe(nt+lp-1,3)-points_courbe(nt-1+lp-1,3))**2)
!     s = sinit
!     bool = 0
!     !do while ((abs((l-1)*ds-s)>eepsilon).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
!     do while ((abs((lp-1)*area*long2/(meshRatio*Ns-1)+(l-lp)*ds-s)>eepsilon*dx).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
!        !tb = (tbm+tp-dtb + tbp+tp-dtb)*0.5_pr
!        tb = (tbm + tbp)*0.5_pr
!        !call pointsBezierN3D(points_control,tb,px,py,pz)
!        call pointsBezierN3D(points_control,tb,px,py,pz)
!        !call pointsBezierN3D(points_control(nint(0.1*size(points_control,1)):size(points_control,1),:),tb*0.9,px,py,pz)
!        !tb = tb -tp+dtb 
!        !call pointsBezierN3D(points_control(nint(0.1*size(points_control,1)):size(points_control,1),:),tb,px,py,pz)
!        !        if ((nt-1)>lp) then
!        s = sinit + sqrt((px-points_courbe(nt-1,1))**2 + (py-points_courbe(nt-1,2))**2&
!             + (pz-points_courbe(nt-1,3))**2)
!        !        else 
!        !        s = sinit + sqrt((px-points_courbe_equal(nt-1,1))**2 + (py-points_courbe_equal(nt-1,2))**2&
!        !+ (pz-points_courbe_equal(nt-1,3))**2)
!        !        endif
!        !        s = sinit + sqrt((px-points_courbe(nt+lp-1-1,1))**2 + (py-points_courbe(nt+lp-1-1,2))**2&
!        !+ (pz-points_courbe(nt+lp-1-1,3))**2)
!        !write(*,*) "TBTB  ",tb," ",(l-1)*ds," ",s
!        !if ((l-1)*ds-s>0._pr) then
!        if ((lp-1)*area*long2/(meshRatio*Ns-1)+(l-lp)*ds-s>0._pr) then
!           tbm = tb
!        else
!           tbp = tb
!        endif
!        if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
!           bool = 1
!        endif
!     enddo
!     !        write(*,*) "TBTB  ",tb," ",(l-1)*ds," ",s
!     !tb = (tb -tp) !+dtb)!/(1._pr+tp-dtb)
!     !        write(*,*) "TBTBff  ",tb," ",(l-1)*ds," ",s
!     !tb = tb +tp-dtb
!     !call pointsBezierN3D(points_control(nint(0.1*size(points_control,1)):size(points_control,1),:),tb*0.9,px,py,pz)
!     call pointsBezierN3D(points_control,tb,px,py,pz)
!     write(*,*) "LLmid  ",l," ",lp," ",tp," ",tb," ",tbm," ",tbp," ",nt,"       ",px," ",py," ",pz," ",ds
!     points_courbe_equal(l,1) = px
!     points_courbe_equal(l,2) = py
!     points_courbe_equal(l,3) = pz
!     !     points_courbe_equal(l+lp-1,1) = px
!     !     points_courbe_equal(l+lp-1,2) = py
!     !     points_courbe_equal(l+lp-1,3) = pz
!  enddo
!  lpt = l
!  !ds = 0.1*long2/(0.2*Ns-1)
!  !ds = 0.1*long2/(Ns-lpt)
!  ds = area*long2/(Ns-lpt)
!  tp = tb
!  tb = 0._pr !dtb
!  l = lpt !1
!  do while ((tb<1._pr).and.(l+1<Ns+1))
!     l = l+1
!     nt = 1
!     s = 0._pr
!     !do while ((lp-1)*0.1*long2/(0.2*Ns-1)+(lpt-lp-1)*0.8*long2/(0.6*Ns-1)+(l-lpt-1)*ds-s>0._pr) 
!     !do while ((lp-1)*area*long2/(meshRatio*Ns-1)+(lpt-lp)*(1._pr-2._pr*area)*long2/((1._pr-meshRatio)*Ns-lp)+(l-lpt)*ds-s>0._pr) 
!     do while (((lp-1)*area*long2/(meshRatio*Ns-1)+(lpt-lp)*(1._pr-2._pr*area)*long2/((1._pr-meshRatio)*Ns-lp)+(l-lpt)*ds-s>0._pr)&
!          .and.(nt+1<size(points_courbe,1)))
!        nt = nt+1
!        s = s + sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)&
!             -points_courbe(nt-1,2))**2 + (points_courbe(nt-1,3)-points_courbe(nt-1,3))**2)
!     enddo
!     tbm = (nt-2)*dtb!-tp+dtb
!     tbp = (nt-1)*dtb!-tp+dtb
!     tb = tbm !(tbm + tbp)*0.5_pr
!     s0 = s
!     sinit = s - sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)&
!          -points_courbe(nt-1,2))**2 + (points_courbe(nt,3)-points_courbe(nt-1,3))**2)
!     s = sinit
!     bool = 0
!     !do while ((abs((lp-1)*0.1*long2/(0.2*Ns-1)+(lpt-lp-1)*0.8*long2/(0.6*Ns-1)+(l-lpt-1)*ds-s)>eepsilon).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
!     do while ((abs((lp-1)*area*long2/(meshRatio*Ns-1)+(lpt-lp)*(1._pr-2._pr*area)*long2/((1._pr-meshRatio)*Ns-lp)&
!          +(l-lpt)*ds-s)>eepsilon*dx).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
!        tb = (tbm + tbp)*0.5_pr
!        call pointsBezierN3D(points_control,tb,px,py,pz)
!        s = sinit + sqrt((px-points_courbe(nt-1,1))**2 + (py-points_courbe(nt-1,2))**2&
!             + (pz-points_courbe(nt-1,3))**2)
!        !if ((lp-1)*0.1*long2/(0.2*Ns-1)+(lpt-lp-1)*0.8*long2/(0.6*Ns-1)+(l-lpt-1)*ds-s>0._pr) then
!        if ((lp-1)*area*long2/(meshRatio*Ns-1)+(lpt-lp)*(1._pr-2._pr*area)*long2/((1._pr-meshRatio)*Ns-lp)+(l-lpt)*ds-s>0._pr) then
!           tbm = tb
!        else
!           tbp = tb
!        endif
!        if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
!           bool = 1
!        endif
!     enddo
!     call pointsBezierN3D(points_control,tb,px,py,pz)
!     write(*,*) "LLfin  ",l+lp-1," ",l," ",lp," ",tp," ",tb," ",tbm," ",tbp," ",nt,"       ",px," ",py," ",pz," ",ds
!     points_courbe_equal(l,1) = px
!     points_courbe_equal(l,2) = py
!     points_courbe_equal(l,3) = pz
!  enddo
  !! if (l+lp-1==Ns-1) then
   if (l==Ns-1) then
      l = l+1
      write(*,*) "ET VOILA1"
  points_courbe_equal(Ns,1) = points_control(size(points_control,1),1)
  points_courbe_equal(Ns,2) = points_control(size(points_control,1),2)
  points_courbe_equal(Ns,3) = points_control(size(points_control,1),3)
      long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))**2 + (points_courbe(l,2)-points_courbe(l-1,2))**2 +&
           (points_courbe(l,3)-points_courbe(l-1,3))**2)
     write(*,*) "LLfirst  ",l," ",points_courbe_equal(Ns,1)," ",points_courbe_equal(Ns,2)," "&
     ,points_courbe_equal(Ns,3)," "&
     ,sqrt((points_courbe(l,1)-points_courbe(l-1,1))**2 + (points_courbe(l,2)-points_courbe(l-1,2))**2 +&
                (points_courbe(l,3)-points_courbe(l-1,3))**2)," ",ds
   endif
  if (.not.(l==Ns)) write(*,*) "WARNIINNGG2 ",l
  !if (.not.(l+lp-1==Ns)) write(*,*) "WARNIINNGG2 ",l
  write(*,*) "longueur =  ",(points_control(size(points_control,1),1)-points_control(1,1))," ",long," ",(points_courbe_equal(Ns,1)-&
       points_courbe_equal(1,1)),"  LL  ",l
  long3 = 0._pr
  do l=1,nl-1
     long3 = long3 +sqrt((xx(midline(l+1,1))-xx(midline(l,1)))**2 + (yy(midline(l+1,2))-yy(midline(l,2)))**2)
  enddo
  write(*,*) "longueur =  ",long3

  !**
  !! Interpolation of endpoints of the real midline 
  !**
  rtail = xx(1)
  itail = floor((rtail-x0)/dx+1)
  rhead = xx(nx)
  ihead = ceiling((rhead-x0)/dx+1)
  bool1 = 0
  bool2 = 0
  if (bool1==0) write(*,*) rhoSlices2(1,midline(1,2))
  do i=2,nx-1
     if (bool1==0) write(*,*) rhoSlices2(i,midline(1,2))
     if ((rhoSlices2(i,midline(1,2))>0._pr).and.(rhoSlices2(i-1,midline(1,2))<0._pr).and.(bool1==0)) then 
        !if ((rhoSlices2(i,midline(1,2))>0._pr).and.(rhoSlices2(i+1,midline(1,2))<0._pr)) then 
        itail = i
        rtail = xx(i-1) - rhoSlices2(i,midline(1,2))*dx/(rhoSlices2(i-1,midline(1,2)) - rhoSlices2(i,midline(1,2)))
        itail = i!floor((rtail-x0)/dx+1)
        bool1 = 1
        write(*,*) "benvoila  ",rtail," ",itail," ",i
        !rtail = xx(i) + rhoSlices2(i,midline(1,2))*dx/(rhoSlices2(i+1,midline(1,2)) - rhoSlices2(i,midline(1,2)))
     endif
     if ((rhoSlices2(i,midline(1,2))>0._pr).and.(rhoSlices2(i+1,midline(1,2))<0._pr)) then
        ihead = i
        rhead = xx(i) - rhoSlices2(i,midline(1,2))*dx/(rhoSlices2(i+1,midline(1,2)) - rhoSlices2(i,midline(1,2)))
        ihead = ceiling((rhead-x0)/dx+1)
        bool2 = 1
     endif
  enddo
  rhead = rhead + 0.0000425/longratio
  ihead = ihead-1
  rtail = rtail - 0.000025/longratio
  !!  do j=2,ny-1
!!!     if ((rhoSlices2(midline(1,1),ny-j+1)>0._pr).and.(rhoSlices2(midline(1,1),ny-(j+1)+1)<0._pr)) yRight =  xx(ny-j) -&
!!!rhoSlices2(midline(1,1),ny-j+1)*dy/(rhoSlices2(midline(1,1),ny-(j+1)+1) - rhoSlices2(midline(1,1),ny-j+1)) !j+1
  !!     if ((rhoSlices2(midline(1,1),ny-j+1)>0._pr).and.(rhoSlices2(midline(1,1),ny-(j+1)+1)<0._pr)) yRight =  xx(ny-j+1) +&
  !!rhoSlices2(midline(1,1),ny-j+1)*dy/(rhoSlices2(midline(1,1),ny-(j+1)+1) - rhoSlices2(midline(1,1),ny-j+1)) !j+1
  !!     if ((rhoSlices2(midline(1,1),j)>0._pr).and.(rhoSlices2(midline(1,1),j+1)<0._pr)) yLeft =  xx(j) -&
  !!rhoSlices2(midline(1,1),j)*dy/(rhoSlices2(midline(1,1),j+1) - rhoSlices2(midline(1,1),j)) !j
  !!  enddo
  !!  !disttaillY = dist(xx(midline(1,1)),xx(midline(1,2)),xx(midline(1,1)),yLeft)
  !!  !disttailrY = dist(xx(midline(1,1)),xx(midline(1,2)),xx(midline(1,1)),yRight)
  !!  disttaillY = dist3D(xx(midline(1,1)),xx(midline(1,2)),zslice,xx(midline(1,1)),yLeft,zslice)
  !!  disttailrY = dist3D(xx(midline(1,1)),xx(midline(1,2)),zslice,xx(midline(1,1)),yRight,zslice)
  disttail = abs(rtail - points_courbe_equal(1,1))
  !!  do j=2,ny-1
!!!     if ((rhoSlices2(midline(nl,1),ny-j+1)>0._pr).and.(rhoSlices2(midline(nl,1),ny-(j+1)+1)<0._pr)) yRight =  xx(ny-j) -&
!!!rhoSlices2(midline(nl,1),ny-j+1)*dy/(rhoSlices2(midline(nl,1),ny-(j+1)+1) - rhoSlices2(midline(nl,1),ny-j+1)) !j+1
  !!     if ((rhoSlices2(midline(nl,1),ny-j+1)>0._pr).and.(rhoSlices2(midline(nl,1),ny-(j+1)+1)<0._pr)) yRight =  xx(ny-j+1) +&
  !!rhoSlices2(midline(nl,1),ny-j+1)*dy/(rhoSlices2(midline(nl,1),ny-(j+1)+1) - rhoSlices2(midline(nl,1),ny-j+1)) !j+1
  !!     if ((rhoSlices2(midline(nl,1),j)>0._pr).and.(rhoSlices2(midline(nl,1),j+1)<0._pr)) yLeft =  xx(j) -&
  !!rhoSlices2(midline(nl,1),j)*dy/(rhoSlices2(midline(nl,1),j+1) - rhoSlices2(midline(nl,1),j)) !j
  !!  enddo
  !!  !distheadlY = dist(xx(midline(nl,1)),xx(midline(nl,2)),xx(midline(nl,1)),yLeft)
  !!  !distheadrY = dist(xx(midline(nl,1)),xx(midline(nl,2)),xx(midline(nl,1)),yRight)
  !!  distheadlY = dist3D(xx(midline(nl,1)),xx(midline(nl,2)),zslice,xx(midline(nl,1)),yLeft,zslice)
  !!  distheadrY = dist3D(xx(midline(nl,1)),xx(midline(nl,2)),zslice,xx(midline(nl,1)),yRight,zslice)


  disthead = abs(rhead-points_courbe_equal(size(points_courbe_equal,1),1))
  !!  disthead = abs(rhead-points_control(size(points_control,1),1))
  write(*,*) "longueur =  ",rhead-rtail," ",rhead-rtail-disttail," ",rhead-rtail-disthead," ",rhead-rtail-disttail-disthead
  long00 = rhead-rtail-disttail-disthead
  write(*,*) "longueur ",long3," ",long2," ",long," ",long00
  write(*,*) "DIST : TAIL ",disttail," HEAD ",disthead," NINT ",nint(disttail/dx)," ",nint(disthead/dx)," ",ceiling(disthead/dx)&
       ," ",floor(disthead/dx)," ",rtail," ",rhead&
       ," ",itail," ",ihead," ",ceiling(points_courbe_equal(1,1)/dx)," ",floor(points_courbe_equal(Ns,1)/dx)&
       ," ",points_courbe_equal(1,1)," ",points_courbe_equal(1,1)/dx!," nbtheta ",nbtheta

  !**
  !! Allocation of control arrays 
  !**

  !!!  allocate(slice_control(nbtheta,ihead-itail+1+2,2))
  !!allocate(slicecontrolm(nbtheta,floor(points_courbe_equal(Ns,1))-ceiling(points_courbe_equal(1,1))+1,3))
  !allocate(slicecontrolm(nbtheta,nint(points_courbe_equal(Ns,1))-nint(points_courbe_equal(1,1))+1,3))
  !!allocate(slicecontrolf(nbtheta,floor(disthead),2))
  !allocate(slicecontrolf(nbtheta,nint(disthead)+1,3))
  !!allocate(slicecontroli(nbtheta,ceiling(disttail),3))
  !allocate(slicecontroli(nbtheta,nint(disttail)+1,3))
  allocate(slicecontrolm(nbtheta,nint(points_courbe_equal(Ns,1)/dx)-nint(points_courbe_equal(1,1)/dx)+1,3))
  allocate(slicecontrolf(nbtheta,nint(disthead/dx)+1,3))
  allocate(slicecontroltmpf(nbtheta,nint(&
  abs(rhead-0.000045/longratio-points_courbe_equal(size(points_courbe_equal,1),1))/dx)+1,3))
  !allocate(slicecontroli(nbtheta,nint(disttail/dx)+1,3))
  allocate(slicecontroli(nbtheta,nint(disttail/dx)+2,3))
  allocate(slicecontroltmpi(nbtheta,nint(abs(rtail + 0.000025/longratio - points_courbe_equal(1,1))/dx)+2,3))
  allocate(slicecontrol(nbtheta,ihead-itail+1,3),slicecontroltmp(nbtheta,ihead-itail+1,3)) 
  allocate(slicecontrolLarge(nbthetaLarge,ihead-itail+1,3)) !nbthetaLarge,nint(points_courbe_equal(Ns,1)+disthead-points_courbe_equal(1,1)-disttail),3))
  allocate(valTh(ihead-itail+1,nbtheta),valThLarge(ihead-itail+1,nbthetaLarge))
  allocate(valThtmp(nbtheta,ihead-itail+1))
  write(*,*) "SIZE CONTROLLARGE  ",size(slicecontrolLarge,2)," ",points_courbe_equal(Ns,1)+disthead-points_courbe_equal(1,1)&
       -disttail," ",rhead," ",rtail
  !  do l=1,size(valThLarge,1)
  !     do theta=1,nbthetaLarge
  !        valThLarge(l,theta) = (theta-1)*2*PI/nbthetaLarge
  !     enddo
  !  enddo
  !  longTh = 0._pr
  !  do l=itail,ihead
  !     do theta=1,nbthetaLarge
  !        rr = 0._pr
  !        bool = 0
  !        do while (((LSrr>0._pr).and.(rr<float(ny))).or.(bool==0))
  !        rp = rr
  !        if (bool==1) then
  !                LSp = LSrr
  !        else
  !        xp = rp*cos(valThLarge(l-itail+1,theta))+points_courbe_equal(1,2)
  !        yp = rp*sin(valThLarge(l-itail+1,theta))+zslice
  !
  !        ip1 = int((xp-x0)/dx)+1
  !        jp1 = int((yp-x0)/dx)+1
  !        ip2 = ip1+1
  !        jp2 = jp1+1
  !
  !        LS1 = rhoSlices(jp1,l,ip1)
  !        LS2 = rhoSlices(jp1,l,ip2)
  !        LS3 = rhoSlices(jp2,l,ip1)
  !        LS4 = rhoSlices(jp2,l,ip2)
  !
  !        x1 = xx(ip1)
  !        y1 = xx(jp1)
  !        x2 = x1 + dx
  !        y2 = y1 + dx
  !        
  !        LSp = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
  !        endif
  !
  !        bool = 1
  !        rr=rr+dx
  !        xp = rr*cos(valThLarge(l-itail+1,theta))+points_courbe_equal(1,2)
  !        yp = rr*sin(valThLarge(l-itail+1,theta))+zslice
  !
  !        ip1 = int((xp-x0)/dx)+1
  !        jp1 = int((yp-x0)/dx)+1
  !        ip2 = ip1+1
  !        jp2 = jp1+1
  !
  !        LS1 = rhoSlices(jp1,l,ip1)
  !        LS2 = rhoSlices(jp1,l,ip2)
  !        LS3 = rhoSlices(jp2,l,ip1)
  !        LS4 = rhoSlices(jp2,l,ip2)
  !
  !        x1 = xx(ip1)
  !        y1 = xx(jp1)
  !        x2 = x1 + dx
  !        y2 = y1 + dx
  !        
  !        LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
  !        enddo
  !        rr = rp
  !        LSrr = LSp
  !        do while ((LSrr>0._pr).and.(rr<float(ny)))
  !        rp = rr
  !        LSp = LSrr
  !
  !        rr=rr+0.1*dx
  !        xp = rr*cos(valThLarge(l-itail+1,theta))+points_courbe_equal(1,2)
  !        yp = rr*sin(valThLarge(l-itail+1,theta))+zslice
  !        ip1 = int((xp-x0)/dx)+1
  !        jp1 = int((yp-x0)/dx)+1
  !        ip2 = ip1+1
  !        jp2 = jp1+1
  !        LS1 = rhoSlices(jp1,l,ip1)
  !        LS2 = rhoSlices(jp1,l,ip2)
  !        LS3 = rhoSlices(jp2,l,ip1)
  !        LS4 = rhoSlices(jp2,l,ip2)
  !        x1 = xx(ip1)
  !        y1 = xx(jp1)
  !        x2 = x1 + dx
  !        y2 = y1 + dx
  !        
  !        LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
  !        enddo
  !        slicecontrolLarge(theta,l-itail+1,1) = xx(l)
  !        slicecontrolLarge(theta,l-itail+1,2) = rp*cos(valThLarge(l-itail+1,theta))+points_courbe_equal(1,2) +&
  !cos(valThLarge(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
  !        slicecontrolLarge(theta,l-itail+1,3) = rp*sin(valThLarge(l-itail+1,theta))+zslice +&
  !sin(valThLarge(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
  !        if (theta>1) longTh(l) = longTh(l) + dist(slicecontrolLarge(theta,l-itail+1,2),slicecontrolLarge(theta,l-itail+1,3)&
  !,slicecontrolLarge(theta-1,l-itail+1,2),slicecontrolLarge(theta-1,l-itail+1,3))
  !!        if (theta==nbthetaLarge) longTh(l) = longTh(l) + dist(slicecontrolLarge(theta,l-itail+1,2)&
  !!,slicecontrolLarge(theta,l-itail+1,3),slicecontrolLarge(1,l-itail+1,2),slicecontrolLarge(1,l-itail+1,3))
  !        if (theta==nbthetaLarge) write(*,*) "LongTH  ",l," ",longTh(l)
  !     enddo
  !     enddo
  do l=1,size(valTh,1)
     do theta=1,nbtheta
        valTh(l,theta) = (theta-1)*2*PI/nbtheta
        valThtmp(theta,l) = (theta-1)*2*PI/nbtheta
     enddo
  enddo
  !!  do l=1,size(valTheta,1)
  !!     do theta=1,nbtheta
  !!        valTheta(l,theta) = (theta-1)*2*PI/nbtheta
  !!     enddo
  !!  enddo

  !**
  !! Filling control arrays (based on level-set zero searching - dichotomy) 
  !**
  lp = 0
  lph = 0
  lpf = 0
  lpt = 0
  lpt = size(slicecontroli,2)-size(slicecontroltmpi,2)
  write(*,*) "SIZEC  ",size(slicecontroltmpi,2)," ",size(slicecontroli,2)," ",itail
  longTh = 0._pr
  do l=itail,ihead

     !     do j=2,ny-1
     !!        if ((rhoSlices2(l,ny-j+1)>0._pr).and.(rhoSlices2(l,ny-(j+1)+1)<0._pr)) yRight = xx(ny-j) - rhoSlices2(l,ny-j+1)*dy/&
     !!(rhoSlices2(l,ny-(j+1)+1) - rhoSlices2(l,ny-j+1)) !j+1
     !        if ((rhoSlices2(l,ny-j+1)>0._pr).and.(rhoSlices2(l,ny-(j+1)+1)<0._pr)) yRight = xx(ny-j+1) + rhoSlices2(l,ny-j+1)*dy/&
     !(rhoSlices2(l,ny-(j+1)+1) - rhoSlices2(l,ny-j+1)) !j+1
     !        if ((rhoSlices2(l,j)>0._pr).and.(rhoSlices2(l,j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(l,j)*dy/(rhoSlices2(l,j+1) -&
     ! rhoSlices2(l,j)) !j
     !!        if ((rhoSlices2(l,j)>0._pr).and.(rhoSlices2(l,j+1)<0._pr)) yRight =  xx(j) + rhoSlices2(l,j)*dy/(rhoSlices2(l,j+1) -&
     !! rhoSlices2(l,j)) !j
     !     enddo
     do theta=1,nbtheta
        !        rr = dx
        !           LSrr = rhoSlices(nint(rr*sin(valTh(l-itail+1,theta)) + zslice),l,nint(rr*cos(valTh(l-itail+1,theta))&
        !+points_courbe_equal(1,2)))
        !           rp = 0._pr
        !           LSp = rhoSlices(nint(rp*sin(valTh(l-itail+1,theta)) + zslice),l,nint(rp*cos(valTh(l-itail+1,theta))&
        !+points_courbe_equal(1,2)))
        rr = 0._pr !yy(ny-2)-yy(ny/2) !0._pr
        !do while ((rhoSlices(floor(j*sin(valTheta(l,theta)) + 100._pr),l,floor(j*cos(valTheta(l,theta))+points_courbe_equal(1,2)))>0._pr).and.(j<ny))
        !do while ((rhoSlices(floor(j*sin(valTheta(l,theta)) + 100._pr),l,floor(j*cos(valTheta(l,theta))+100._pr))>0._pr).and.(j<ny))
        !        do while ((rhoSlices(nint(rr*sin(valTh(l-itail+1,theta)) + zslice),l&
        !,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))>0._pr).and.(rr<float(ny)))
        !           LSp = rhoSlices(nint(rr*sin(valTh(l-itail+1,theta)) + zslice),l,nint(rr*cos(valTh(l-itail+1,theta))&
        !+points_courbe_equal(1,2)))
        !           rp = rr
        !           rr=rr+dx
        !           LSrr = rhoSlices(nint(rr*sin(valTh(l-itail+1,theta)) + zslice),l,nint(rr*cos(valTh(l-itail+1,theta))&
        !+points_courbe_equal(1,2)))
        !        enddo
        bool = 0
        !        do while (((LSrr>0._pr).and.(rr<float(ny))))!).or.(bool==0))
        do while (((LSrr<0._pr).and.(rr<yy(ny))).or.(bool==0))
        !do while (((LSrr<0._pr).and.(rr<float(ny))).or.(bool==0))
        !do while (((LSrr<0._pr).and.(rr<yy(ny-2)-yy(ny/2))).or.(bool==0))
        !do while (((LSrr<0._pr).and.(rr<yy(ny-2)-yy(ny/2)).and.(rr>yy(4))).or.(bool==0))
           rp = rr
           if (bool==1) then
              LSp = LSrr
           else
              ! LSp = rhoSlices(nint(zslice),l,nint(points_courbe_equal(1,2)))
              !xp = rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
              !yp = rp*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
              if (cos(valTh(l-itail+1,theta))<0._pr) then
                xp = (-yy(1)-rp+points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
              else
                xp = (yy(ny)-rp-points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
              endif
              if (sin(valTh(l-itail+1,theta))<0._pr) then
                yp = (-zz(1)-rp+zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
              else
                yp = (zz(nz)-rp-zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
              endif

              ip1 = int((xp-x0)/dy)+1
              jp1 = int((yp-x0)/dz)+1
              !ip2 = ip1+1
              !jp2 = jp1+1
              !ip2 = ip1-1
              !jp2 = jp1-1
              
              if (cos(valTh(l-itail+1,theta))<0._pr) then
                ip2 = ip1+1
              else
                ip2 = ip1-1
              endif
              if (sin(valTh(l-itail+1,theta))<0._pr) then
                jp2 = jp1+1
              else
                jp2 = jp1-1
              endif

              if (jp2==0) write(*,*) "ATTENTIONSTOPZERO  ",l," ",theta," ",jp1," "&
              ,cos(valTh(l-itail+1,theta))," ",sin(valTh(l-itail+1,theta))," ",(xp-x0)/dy," ",(yp-x0)/dz," ",rp," ",rr
              if (ip2==301) write(*,*) "ATTENTIONSTOPMAX  ",l," ",theta," ",jp1," "&
              ,cos(valTh(l-itail+1,theta))," ",sin(valTh(l-itail+1,theta))," ",(xp-x0)/dy," ",(yp-x0)/dz," ",rp," ",rr

              LS1 = rhoSlices(jp1,l,ip1)
              LS2 = rhoSlices(jp1,l,ip2)
              LS3 = rhoSlices(jp2,l,ip1)
              LS4 = rhoSlices(jp2,l,ip2)

              x1 = yy(ip1)
              y1 = zz(jp1)
              !x2 = x1 + dy
              !y2 = y1 + dz
              !x2 = x1 - dy
              !y2 = y1 - dz
              if (cos(valTh(l-itail+1,theta))<0._pr) then
                x2 = x1 + dy
              else
                x2 = x1 - dy
              endif
              if (sin(valTh(l-itail+1,theta))<0._pr) then
                y2 = y1 + dz
              else
                y2 = y1 - dz
              endif

              LSp = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
              if (l==itail+1) write(*,*) "TEST checkAVANT  ::    ",l," ",theta," ",rp," ",LSp," ",LSrr,"  ",xp," ",yp&
           ," ",(xp-x0)/dy," ",(yp-x0)/dz," ",ip1," ",jp1&
           ,"  ",cos(valTh(l-itail+1,theta))," ",sin(valTh(l-itail+1,theta)),"   ",yy(ny-2)-yy(ny/2)
              !LSp = interpLS(LS1,x2,y2,LS2,x1,y2,LS3,x2,y1,LS4,x1,y1,xp,yp) 
           endif

           bool = 1
           rr=rr+dy
           !rr=rr-dy
           !xp = rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
           !yp = rr*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
           if (cos(valTh(l-itail+1,theta))<0._pr) then
             xp = (-yy(1)-rr+points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
           else
             xp = (yy(ny)-rr-points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
           endif
           if (sin(valTh(l-itail+1,theta))<0._pr) then
             yp = (-zz(1)-rr+zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
           else
             yp = (zz(nz)-rr-zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
           endif

           ip1 = int((xp-x0)/dy)+1
           jp1 = int((yp-x0)/dz)+1
              if (l==itail+1) write(*,*) "TEST checkAVANT  ::    ",l," ",theta," ",rr," ",LSp," ",LSrr,"  ",xp," ",yp&
           ," ",(xp-x0)/dy," ",(yp-x0)/dz," ",ip1," ",jp1&
           ,"  ",cos(valTh(l-itail+1,theta))," ",sin(valTh(l-itail+1,theta)),"   ",yy(ny-2)-yy(ny/2)&
           ," ",int((points_courbe_equal(1,2)-x0)/dy+1)," ",int((zz(nint(zslice))-x0)/dz+1)
           !ip2 = ip1+1
           !jp2 = jp1+1
           !ip2 = ip1-1
           !jp2 = jp1-1
           if (cos(valTh(l-itail+1,theta))<0._pr) then
             ip2 = ip1+1
           else
             ip2 = ip1-1
           endif
           if (sin(valTh(l-itail+1,theta))<0._pr) then
             jp2 = jp1+1
           else
             jp2 = jp1-1
           endif
              if (jp2==0) write(*,*) "ATTENTIONSTOPZERO  ",l," ",theta," ",jp1," "&
              ,cos(valTh(l-itail+1,theta))," ",sin(valTh(l-itail+1,theta))," ",(xp-x0)/dy," ",(yp-x0)/dz," ",rp," ",rr

           LS1 = rhoSlices(jp1,l,ip1)
           LS2 = rhoSlices(jp1,l,ip2)
           LS3 = rhoSlices(jp2,l,ip1)
           LS4 = rhoSlices(jp2,l,ip2)

           x1 = yy(ip1)
           y1 = zz(jp1)
           !        x1 = floor(xp)
           !        y1 = floor(yp)
           !x2 = x1 + dy
           !y2 = y1 + dz
           !x2 = x1 - dy
           !y2 = y1 - dz
           if (cos(valTh(l-itail+1,theta))<0._pr) then
             x2 = x1 + dy
           else
             x2 = x1 - dy
           endif
           if (sin(valTh(l-itail+1,theta))<0._pr) then
             y2 = y1 + dz
           else
             y2 = y1 - dz
           endif

           LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
           !LSrr = interpLS(LS1,x2,y2,LS2,x1,y2,LS3,x2,y1,LS4,x1,y1,xp,yp) 
           !if ((l==itail).and.(theta==1)) write(*,*) "ITAILITAIL_1 : ",rr," ",LSrr," ",LSp,"   ",yy(ny)&
           !," -- ",LS1," ",LS2," ",LS3," ",LS4&
           !,"  --  ",cos(valTh(l-itail+1,theta))," ",sin(valTh(l-itail+1,theta))," ",rr/dy," ",xp," ",yp
           !if ((l==itail).and.(theta==nbtheta)) write(*,*) "ITAILITAIL_nbtheta : ",rr," ",LSrr," ",LSp,"   ",yy(ny)&
           !," -- ",LS1," ",LS2," ",LS3," ",LS4&
           !,"  --  ",cos(valTh(l-itail+1,theta))," ",sin(valTh(l-itail+1,theta))," ",rr/dy," ",xp," ",yp
           !        write(*,*) "TEST check  ::  ",l," ",theta," ",rr," ",LSp," ",LSrr!," ",xp," ",yp," ",rhoSlices(nint(rr*sin(valTh(l-itail+1,theta))&
           !+ zslice),l,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))," ",LS1," ",LS2," ",LS3," ",LS4 
           !if (l==72) write(*,*) "TEST check  ::    ",l," ",theta," ",rr," ",LSp," ",LSrr,"  ",xp," ",yp&
           !," ",(xp-x0)/dy," ",(yp-x0)/dz," ",ip1," ",jp1&
           !,"  ",cos(valTh(l-itail+1,theta))," ",sin(valTh(l-itail+1,theta))
        enddo
        rr = rp
        LSrr = LSp
        !do while ((LSrr<0._pr).and.(rr<float(ny)))
        do while ((LSrr<0._pr).and.(rr<yy(ny)))
           rp = rr
           LSp = LSrr

           rr=rr+0.1*dy
           !xp = rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
           !yp = rr*sin(valTh(l-itail+1,theta))+zslice
           if (cos(valTh(l-itail+1,theta))<0._pr) then
             xp = (-yy(1)-rr+points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
           else
             xp = (yy(ny)-rr-points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
           endif
           if (sin(valTh(l-itail+1,theta))<0._pr) then
             yp = (-zz(1)-rr+zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
           else
             yp = (zz(nz)-rr-zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
           endif
           ip1 = int((xp-x0)/dy)+1
           jp1 = int((yp-x0)/dz)+1
           !ip2 = ip1+1
           !jp2 = jp1+1
           if (cos(valTh(l-itail+1,theta))<0._pr) then
             ip2 = ip1+1
           else
             ip2 = ip1-1
           endif
           if (sin(valTh(l-itail+1,theta))<0._pr) then
             jp2 = jp1+1
           else
             jp2 = jp1-1
           endif

           LS1 = rhoSlices(jp1,l,ip1)
           LS2 = rhoSlices(jp1,l,ip2)
           LS3 = rhoSlices(jp2,l,ip1)
           LS4 = rhoSlices(jp2,l,ip2)
           x1 = yy(ip1)
           y1 = zz(jp1)
           !x2 = x1 + dx
           !y2 = y1 + dx
           if (cos(valTh(l-itail+1,theta))<0._pr) then
             x2 = x1 + dy
           else
             x2 = x1 - dy
           endif
           if (sin(valTh(l-itail+1,theta))<0._pr) then
             y2 = y1 + dz
           else
             y2 = y1 - dz
           endif

           LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
           !        write(*,*) "TEST check  ::  ",l," ",theta," ",rr," ",LSp," ",LSrr!," ",xp," ",yp," ",rhoSlices(nint(rr*sin(valTh(l-itail+1,theta))&
           !+ zslice),l,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))," ",LS1," ",LS2," ",LS3," ",LS4 
        enddo
        !        write(*,*) "CHECK debut ",(rr-dx)*cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2)," ",rr*&
        !cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2)," ",rhoSlices(nint((rr-dx)*sin(valTheta(l-itail+1,theta))+zslice)& 
        !,l,nint((rr-dx)*cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2)))," ",rhoSlices(nint(rr*&
        !sin(valTheta(l-itail+1,theta)) + zslice),l,nint(rr*cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2)))
        !        if (theta==1) write(*,*) "OHOHOH ",l
        !        if (theta==1) write(*,*) "TEST LEFT ",yLeft," ",(rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2) -&
        !rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2)))*dy/&
        !(rhoSlices(nint(rr*sin(valTheta(l,theta))+100._pr),l,nint(rr*cos(valTheta(l,theta))+points_courbe_equal(1,2))) -&
        !rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2))))," "&
        !,points_courbe_equal(1,2)," ",(rr-1)*cos(valTheta(l,theta))," ",rr-1," ",valTheta(l,theta)," ",theta," ",l !rr+1
        !        if (theta==1) write(*,*) "TEST LEFT ",yLeft," ",(rr-1)*cos(valTheta(l,theta))+100 - rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+100))*dy/(rhoSlices(nint(rr*sin(valTheta(l,theta))+100._pr),l,nint(rr*cos(valTheta(l,theta))+100)) - rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+100)))," ",points_courbe_equal(1,2)," ",rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+100))," ",rr-1 !rr+1
        !        if (theta==2) write(*,*) "TEST RIGHT ",yRight," ",(rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2) - rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2)))*dy/(rhoSlices(nint(rr*sin(valTheta(l,theta))+100._pr),l,nint(rr*cos(valTheta(l,theta))+points_courbe_equal(1,2))) - rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2)))) !rr+1

        !     !if (l<ceiling(points_courbe_equal(1,1))) then
        !     if (l<=nint(points_courbe_equal(1,1))) then
        !        if (lpt==0) lpt = l
        !        slicecontroli(theta,l-lpt+1,1) = xx(l)
        !        slicecontroli(theta,l-lpt+1,2) = (rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&
        !cos(valTh(l-itail+1,theta))*rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice),l,nint((rr-dx)*&
        !cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))*dy/abs(rhoSlices(nint(rr*sin(valTh(l-itail+1,theta))+zslice),l&
        !,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2))) - rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice)&
        !,l,nint((rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
        !        slicecontroli(theta,l-lpt+1,3) = (rr-dx)*sin(valTh(l-itail+1,theta))+zslice +&
        !sin(valTh(l-itail+1,theta))*rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice),l,nint((rr-dx)*&
        !cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))*dy/abs(rhoSlices(nint(rr*sin(valTh(l-itail+1,theta))+zslice),l&
        !,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2))) - rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice)&
        !,l,nint((rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
        !     endif
        !     !if ((l>=ceiling(points_courbe_equal(1,1))).and.(l<=floor(points_courbe_equal(Ns,1)))) then
        !     if ((l>=nint(points_courbe_equal(1,1))).and.(l<=nint(points_courbe_equal(Ns,1)))) then
        !        if (lp==0) lp = l
        !        slicecontrolm(theta,l-lp+1,1) = xx(l)
        !        slicecontrolm(theta,l-lp+1,2) = (rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&
        !cos(valTh(l-itail+1,theta))*rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice),l,nint((rr-dx)*&
        !cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))*dy/abs(rhoSlices(nint(rr*sin(valTh(l-itail+1,theta))+zslice),l&
        !,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2))) - rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice)&
        !,l,nint((rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
        !        slicecontrolm(theta,l-lp+1,3) = (rr-dx)*sin(valTh(l-itail+1,theta))+zslice +&
        !sin(valTh(l-itail+1,theta))*rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice),l,nint((rr-dx)*&
        !cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))*dy/abs(rhoSlices(nint(rr*sin(valTh(l-itail+1,theta))+zslice),l&
        !,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2))) - rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice)&
        !,l,nint((rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
        !     endif
        !     !if (l>floor(points_courbe_equal(Ns,1))) then
        !     if (l>=nint(points_courbe_equal(Ns,1))) then
        !        if (lph==0) lph = l
        !        slicecontrolf(theta,l-lph+1,1) = xx(l)
        !        slicecontrolf(theta,l-lph+1,2) = (rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&
        !cos(valTh(l-itail+1,theta))*rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice),l,nint((rr-dx)*&
        !cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))*dy/abs(rhoSlices(nint(rr*sin(valTh(l-itail+1,theta))+zslice),l&
        !,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2))) - rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice)&
        !,l,nint((rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
        !        slicecontrolf(theta,l-lph+1,3) = (rr-dx)*sin(valTh(l-itail+1,theta))+zslice +&
        !sin(valTh(l-itail+1,theta))*rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice),l,nint((rr-dx)*&
        !cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))*dy/abs(rhoSlices(nint(rr*sin(valTh(l-itail+1,theta))+zslice),l&
        !,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2))) - rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice)&
        !,l,nint((rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
        !     endif
        px = xx(l)
        !py = rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&
        !     cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
        !pz = rp*sin(valTh(l-itail+1,theta))+zz(nint(zslice)) +&
        !     sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp)  !rr+1
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          py = (-yy(1)-rp+points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&
               -cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
        else
          py = (yy(ny)-rp-points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&
               -cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          pz = (-zz(1)-rp+zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice)) +&
               -sin(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
        else
          pz = (zz(nz)-rp-zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))  +&
               -sin(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
        endif
        !if (l<ceiling(points_courbe_equal(1,1))) then
        !if (l==itail) write(*,*) "totototo00  ",l," ",px," ",py," ",pz," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)&
        !," ",points_courbe_equal(1,1)
        if (l<=nint((points_courbe_equal(1,1)-x0)/dx+1)) then
        !if (l<=ceiling((points_courbe_equal(1,1)-x0)/dx+1)) then
           if (lpt==size(slicecontroli,2)-size(slicecontroltmpi,2)) lpt = l-lpt
           !if (lpt==0) lpt = l-lpt
           !slicecontroli(theta,l-lpt+1,1) = xx(l-1)
           !slicecontroli(theta,l-lpt+1,2) = points_courbe_equal(1,2)
           !slicecontroli(theta,l-lpt+1,3) = zz(nint(zslice))
           slicecontroli(theta,l-lpt+2,1) = px!xx(l)
           slicecontroli(theta,l-lpt+2,2) = py!rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&                cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
           slicecontroli(theta,l-lpt+2,3) = pz !rp*sin(valTh(l-itail+1,theta))+zslice +&                sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
           !        if (theta>1) longTh(l) = longTh(l) + dist(slicecontroli(theta,l-lpt+1,2),slicecontroli(theta,l-lpt+1,3)&
           !,slicecontroli(theta-1,l-lpt+1,2),slicecontroli(theta-1,l-lpt+1,3))
           !        if (theta==nbtheta) write(*,*) "LongTH  ",l," ",longTh(l)
           if (theta==1+nbtheta/2) write(*,*) "totototo00  ",l," ",px," ",py," ",pz&
           ," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)," ",lpt
        endif
        !if ((l>=ceiling(points_courbe_equal(1,1))).and.(l<=floor(points_courbe_equal(Ns,1)))) then
        if ((l>=nint((points_courbe_equal(1,1)-x0)/dx+1)).and.(l<=nint((points_courbe_equal(Ns,1)-x0)/dx+1))) then
        !if ((l>=ceiling((points_courbe_equal(1,1)-x0)/dx+1)).and.(l<=floor((points_courbe_equal(Ns,1)-x0)/dx+1))) then
           if (lp==0) lpf = l
           if (lp==0) lp = l
           slicecontrolm(theta,l-lp+1,1) = px!xx(l)
           slicecontrolm(theta,l-lp+1,2) = py!rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&                cos(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
           slicecontrolm(theta,l-lp+1,3) = pz!rp*sin(valTh(l-itail+1,theta))+zslice +&                sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
           !        if (theta>1) longTh(l) = longTh(l) + dist(slicecontrolm(theta,l-lp+1,2),slicecontrolm(theta,l-lp+1,3)&
           !,slicecontrolm(theta-1,l-lp+1,2),slicecontrolm(theta-1,l-lp+1,3))
           !        if (theta==nbtheta) write(*,*) "LongTH  ",l," ",longTh(l)
        endif
        !if (l>floor(points_courbe_equal(Ns,1))) then
        if (l>=nint((points_courbe_equal(Ns,1)-x0)/dx+1)) then
        !if (l>=floor((points_courbe_equal(Ns,1)-x0)/dx+1)) then
           if (lph==0) lph = l
           slicecontrolf(theta,l-lph+1,1) = px!xx(l)
           slicecontrolf(theta,l-lph+1,2) = py!rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&                cos(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
           slicecontrolf(theta,l-lph+1,3) = pz!rp*sin(valTh(l-itail+1,theta))+zslice +&                sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
           !        if (theta>1) longTh(l) = longTh(l) + dist(slicecontrolf(theta,l-lph+1,2),slicecontrolf(theta,l-lph+1,3)&
           !,slicecontrolf(theta-1,l-lph+1,2),slicecontrolf(theta-1,l-lph+1,3))
           !        if (theta==nbtheta) write(*,*) "LongTH  ",l," ",longTh(l)
        endif
        slicecontrol(theta,l-itail+1,1) = px!xx(l)
        slicecontrol(theta,l-itail+1,2) = py!rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&             cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
        slicecontrol(theta,l-itail+1,3) = pz!rp*sin(valTh(l-itail+1,theta))+zslice +&             sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
        if (theta>1) longTh(l) = longTh(l) + dist(slicecontrol(theta,l-itail+1,2),slicecontrol(theta,l-itail+1,3)&
             ,slicecontrol(theta-1,l-itail+1,2),slicecontrol(theta-1,l-itail+1,3))
        if (theta==nbtheta) longTh(l) = longTh(l) + dist(slicecontrol(theta,l-itail+1,2)&
             ,slicecontrol(theta,l-itail+1,3),slicecontrol(1,l-itail+1,2),slicecontrol(1,l-itail+1,3))
                if (theta==1) write(*,*) "LongTH  ",l," ",longTh(l)," ",LSrr," ",LSp
        !if ((l<itail+5).and.(theta==1))   write(*,*) "totototo01  ",l," ",px," ",py," ",pz&
        !," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)&
        !   ," ",nint((points_courbe_equal(1,1)-x0)/dx+1)," ",lpt
        !if ((l<itail+5).and.(theta==2))   write(*,*) "totototo02  ",l," ",px," ",py," ",pz&
        !," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)&
        !   ," ",nint((points_courbe_equal(1,1)-x0)/dx+1)," ",lpt

        !if (theta==2) yRight = (rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2) - rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2)))*dy/(rhoSlices(nint(rr*sin(valTheta(l,theta))+100._pr),l,nint(rr*cos(valTheta(l,theta))+points_courbe_equal(1,2))) - rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2)))) !rr+1
        !        write(*,*) "TESTsin ",l," ",theta," ",yLeft," ",yRight,"  ",rr-1," ",(rr-1)*sin(valTheta(l,theta))+points_courbe_equal(1,2)&
        !-rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2)))*dy/&
        !(rhoSlices(nint(rr*sin(valTheta(l,theta))+100._pr),l,nint(rr*cos(valTheta(l,theta))+points_courbe_equal(1,2))) -&
        !rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2)))) !rr+1
        !        write(*,*) "TESTcos ",l," ",theta," ",yLeft," ",yRight,"  ",rr-dx," ",(rr-dx)*cos(valTheta(l-itail+1,theta))+&
        !points_courbe_equal(1,2)+cos(valTheta(l-itail+1,theta))*rhoSlices(nint((rr-dx)*sin(valTheta(l-itail+1,theta))+zslice),l&
        !,nint((rr-dx)*cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2)))*dy/abs(rhoSlices(nint(rr*sin(valTheta(l-itail+1,theta))+&
        !zslice),l,nint(rr*cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2))) -rhoSlices(nint((rr-dx)*sin(valTheta(l-itail+1,theta))&
        !+zslice),l,nint((rr-dx)*cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
        !        write(*,*) "TESTcos ",l," ",theta," ",yLeft," ",yRight,"  ",rr-dx," "&
        !,rhoSlices(nint((rr-dx)*sin(valTheta(l-itail+1,theta))+zslice),l,nint((rr-dx)*&
        !cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2)))*(-dy*cos(valTheta(l-itail+1,theta)))/(rhoSlices(nint(rr*&
        !sin(valTheta(l-itail+1,theta))+zslice),l,nint(rr*cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2)))-rhoSlices(nint((rr-dx)*& 
        !sin(valTheta(l-itail+1,theta))+zslice),l,nint((rr-dx)*cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
        !        write(*,*) "TESTcos ",l," ",theta," ",yLeft," ",yRight,"  ",rr-dx," "&
        !,rhoSlices(nint(rr*sin(valTheta(l-itail+1,theta))+zslice),l,nint(rr*&
        !cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2)))*(dy*cos(valTheta(l-itail+1,theta)))/(rhoSlices(nint(rr*&
        !sin(valTheta(l-itail+1,theta))+zslice),l,nint(rr*cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2)))-rhoSlices(nint((rr-dx)*& 
        !sin(valTheta(l-itail+1,theta))+zslice),l,nint((rr-dx)*cos(valTheta(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
        !        write(*,*) "TESTcos ",l," ",theta," ",yLeft," ",yRight,"  ",rr-1," ",(rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2)& 
        !-rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2)))*dy/&
        !(rhoSlices(nint(rr*sin(valTheta(l,theta))+100._pr),l,nint(rr*cos(valTheta(l,theta))+points_courbe_equal(1,2))) -&
        !rhoSlices(nint((rr-1)*sin(valTheta(l,theta))+100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2)))) !rr+1
        !        write(*,*) "TESTcos ",l," ",theta," ",yLeft," ",yRight,"  ",rr-dx," ",(rr-dx)*cos(valTheta(l-itail+1,theta))+&
        !points_courbe_equal(1,2)," ",points_courbe_equal(l-itail+1,2)," ",points_courbe_equal(1,2)!rr+1
        !write(*,*) "TEST RHOINI SLICE ",l," ",theta," ",yLeft," ",yRight,"        ",rr," ",rhoSlices(nint((rr-1)*sin(valTheta(l,theta)) + 100._pr),l,nint((rr-1)*cos(valTheta(l,theta))+points_courbe_equal(1,2))),"  ",rhoSlices(nint(rr*sin(valTheta(l,theta)) + 100._pr),l,nint(rr*cos(valTheta(l,theta))+points_courbe_equal(1,2)))
     enddo




     if (lp==0) then
     else
        lpf = lpf+1
     endif



     !do theta=1,nbtheta
     !   slice_control(theta,l-itail+1+1,1) = xx(l)
     !enddo
     !slice_control(1,l-itail+1+1,2) = yLeft
     !slice_control(2,l-itail+1+1,2) = yRight
     !if (l<ceiling(points_courbe_equal(1,1))) then
     !     if (l<nint(points_courbe_equal(1,1))) then
     !        if (lpt==0) lpt = l
     !        do theta=1,nbtheta
     !           slicecontroli(theta,l-lpt+1,1) = xx(l)
     !        enddo
     !        slicecontroli(1,l-lpt+1,2) = yLeft
     !        slicecontroli(2,l-lpt+1,2) = yRight
     !     endif
     !     !if ((l>=ceiling(points_courbe_equal(1,1))).and.(l<=floor(points_courbe_equal(Ns,1)))) then
     !     if ((l>=nint(points_courbe_equal(1,1))).and.(l<=nint(points_courbe_equal(Ns,1)))) then
     !        if (lp==0) lp = l
     !        do theta=1,nbtheta
     !           slicecontrolm(theta,l-lp+1,1) = xx(l)
     !        enddo
     !        slicecontrolm(1,l-lp+1,2) = yLeft
     !        slicecontrolm(2,l-lp+1,2) = yRight
     !     endif
     !     !if (l>floor(points_courbe_equal(Ns,1))) then
     !     if (l>nint(points_courbe_equal(Ns,1))) then
     !        if (lph==0) lph = l
     !        write(*,*) "CHECK LH ",l-lph+1," ",lph
     !        do theta=1,nbtheta
     !           slicecontrolf(theta,l-lph+1,1) = xx(l)
     !        enddo
     !        slicecontrolf(1,l-lph+1,2) = yLeft
     !        slicecontrolf(2,l-lph+1,2) = yRight
     !     endif
  enddo
  slicecontroltmp = slicecontrol
  do l=itail,ihead
     dsth(l) = longTh(l)/nbtheta
  enddo
  !longTh = 0._pr
  dtb = 2*PI/nbtheta
  !lpt = 0
  !lp = 0
  !lph = 0
  !  slicecontroli = 0._pr 
  !  slicecontrolm = 0._pr
  !  slicecontrolf = 0._pr
  !  slicecontrol = 0._pr
  !  valTh = 0._pr
  do l=itail,ihead
     ll = 1
     !     slicecontrol(ll,l-itail+1,1) = slicecontroltmp(ll,l-itail+1,1) !xx(l)
     !     slicecontrol(ll,l-itail+1,2) = slicecontroltmp(ll,l-itail+1,2) !rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) + cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
     !     slicecontrol(ll,l-itail+1,3) = slicecontroltmp(ll,l-itail+1,3) !rp*sin(valTh(l-itail+1,theta))+zslice + sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
     tb = 0._pr

     do while ((tb<2*PI).and.(ll+1<nbtheta+1)) !size(leftmid,1)))
        ll = ll+1
        nt = 1
        sth(l) = 0._pr
        do while (((ll-1)*dsth(l)-sth(l)>0._pr).and.(nt<size(slicecontroltmp,1)))
           nt = nt+1
           sth(l) = sth(l) +&
                sqrt((slicecontroltmp(nt,l-itail+1,1)-slicecontroltmp(nt-1,l-itail+1,1))**2 + (slicecontroltmp(nt,l-itail+1,2)-&
                slicecontroltmp(nt-1,l-itail+1,2))**2 + (slicecontroltmp(nt,l-itail+1,3)-slicecontroltmp(nt-1,l-itail+1,3))**2)
        enddo

        tbm = (nt-2)*dtb
        tbp = (nt-1)*dtb
        !        tbm = (nt-1)*dtb
        !        tbp = nt*dtb
        !!if (ll==nbtheta) tbp = nt*dtb
        tb = tbm 

        s0 = sth(l)
        if (nt>1) then
        sinit = sth(l) -&
             sqrt((slicecontroltmp(nt,l-itail+1,1)-slicecontroltmp(nt-1,l-itail+1,1))**2 + (slicecontroltmp(nt,l-itail+1,2)-&
             slicecontroltmp(nt-1,l-itail+1,2))**2 + (slicecontroltmp(nt,l-itail+1,3)-slicecontroltmp(nt-1,l-itail+1,3))**2)
        else
        sinit = sth(l)
        endif
        sth(l) = sinit
        bool = 0
        !do while ((abs((ll-1)*dsth(l)-sth(l))>eepsilon*dx).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
        do while ((abs((ll-1)*dsth(l)-sth(l))/dx>eepsilon).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
           tb = (tbm + tbp)*0.5_pr
           rr = 0._pr !yy(ny-2)-yy(ny/2) !0._pr
           booltmp = 0
           !do while (((LSrr<0._pr).and.(rr<yy(ny-2)-yy(ny/2))).or.(booltmp==0))
           do while (((LSrr<0._pr).and.(rr<yy(ny))).or.(booltmp==0))
           !do while (((LSrr>0._pr).and.(rr<yy(ny))).or.(booltmp==0))
              rp = rr
              if (booltmp==1) then
                 LSp = LSrr
              else
                 !xp = rp*cos(tb)+points_courbe_equal(1,2)
                 !yp = rp*sin(tb)+zz(nint(zslice))
                 if (cos(tb)<0._pr) then
                   xp = (-yy(1)-rp+points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2)
                 else
                   xp = (yy(ny)-rp-points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2)
                 endif
                 if (sin(tb)<0._pr) then
                   yp = (-zz(1)-rp+zz(nint(zslice)))*sin(tb)+zz(nint(zslice))
                 else
                   yp = (zz(nz)-rp-zz(nint(zslice)))*sin(tb)+zz(nint(zslice))
                 endif
                 ip1 = int((xp-x0)/dy)+1
                 jp1 = int((yp-x0)/dz)+1
                 !ip2 = ip1+1
                 !jp2 = jp1+1
                 if (cos(tb)<0._pr) then
                   ip2 = ip1+1
                 else
                   ip2 = ip1-1
                 endif
                 if (sin(tb)<0._pr) then
                   jp2 = jp1+1
                 else
                   jp2 = jp1-1
                 endif
                 LS1 = rhoSlices(jp1,l,ip1)
                 LS2 = rhoSlices(jp1,l,ip2)
                 LS3 = rhoSlices(jp2,l,ip1)
                 LS4 = rhoSlices(jp2,l,ip2)
                 x1 = yy(ip1)
                 y1 = zz(jp1)
                 !x2 = x1 + dy
                 !y2 = y1 + dz
                 if (cos(tb)<0._pr) then
                   x2 = x1 + dy
                 else
                   x2 = x1 - dy
                 endif
                 if (sin(tb)<0._pr) then
                   y2 = y1 + dz
                 else
                   y2 = y1 - dz
                 endif
                 LSp = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
              !if ((ll==4).and.(l==4))   write(*,*) "totototo00  ",l," ",px," ",py," ",pz," ",LSrr," ",LSp," ",rr," ",yy(ny)
              !if ((ll==4).and.(l==itail+1))   write(*,*) "totototo00  ",l," ",px," ",py," ",pz," ",LSrr," ",LSp&
              !," ",LS1," ",LS2," ",LS3," ",LS4,"      ",ip1," ",jp1," ",ip2," ",jp2
              endif
              booltmp = 1
              rr=rr+dy
              !rr=rr-dy
              !xp = rr*cos(tb)+points_courbe_equal(1,2)
              !yp = rr*sin(tb)+zz(nint(zslice))
              if (cos(tb)<0._pr) then
                xp = (-yy(1)-rr+points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2)
              else
                xp = (yy(ny)-rr-points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2)
              endif
              if (sin(tb)<0._pr) then
                yp = (-zz(1)-rr+zz(nint(zslice)))*sin(tb)+zz(nint(zslice))
              else
                yp = (zz(nz)-rr-zz(nint(zslice)))*sin(tb)+zz(nint(zslice))
              endif
              ip1 = int((xp-x0)/dy)+1
              jp1 = int((yp-x0)/dz)+1
              !ip2 = ip1+1
              !jp2 = jp1+1
              if (cos(tb)<0._pr) then
                ip2 = ip1+1
              else
                ip2 = ip1-1
              endif
              if (sin(tb)<0._pr) then
                jp2 = jp1+1
              else
                jp2 = jp1-1
              endif
              LS1 = rhoSlices(jp1,l,ip1)
              LS2 = rhoSlices(jp1,l,ip2)
              LS3 = rhoSlices(jp2,l,ip1)
              LS4 = rhoSlices(jp2,l,ip2)
              x1 = yy(ip1)
              y1 = zz(jp1)
              !x2 = x1 + dy
              !y2 = y1 + dz
              if (cos(tb)<0._pr) then
                x2 = x1 + dy
              else
                x2 = x1 - dy
              endif
              if (sin(tb)<0._pr) then
                y2 = y1 + dz
              else
                y2 = y1 - dz
              endif
              LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
           enddo
           rr = rp
           LSrr = LSp
           !do while ((LSrr<0._pr).and.(rr<float(ny)))
           do while ((LSrr<0._pr).and.(rr<yy(ny)))
              rp = rr
              LSp = LSrr
              rr=rr+0.1*dy
              !xp = rr*cos(tb)+points_courbe_equal(1,2)
              !yp = rr*sin(tb)+zz(nint(zslice))
              if (cos(tb)<0._pr) then
                xp = (-yy(1)-rr+points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2)
              else
                xp = (yy(ny)-rr-points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2)
              endif
              if (sin(tb)<0._pr) then
                yp = (-zz(1)-rr+zz(nint(zslice)))*sin(tb)+zz(nint(zslice))
              else
                yp = (zz(nz)-rr-zz(nint(zslice)))*sin(tb)+zz(nint(zslice))
              endif
              ip1 = int((xp-x0)/dy)+1
              jp1 = int((yp-x0)/dz)+1
              !ip2 = ip1+1
              !jp2 = jp1+1
              if (cos(tb)<0._pr) then
                ip2 = ip1+1
              else
                ip2 = ip1-1
              endif
              if (sin(tb)<0._pr) then
                jp2 = jp1+1
              else
                jp2 = jp1-1
              endif
              LS1 = rhoSlices(jp1,l,ip1)
              LS2 = rhoSlices(jp1,l,ip2)
              LS3 = rhoSlices(jp2,l,ip1)
              LS4 = rhoSlices(jp2,l,ip2)
              x1 = yy(ip1)
              y1 = zz(jp1)
              !x2 = x1 + dy
              !y2 = y1 + dz
              if (cos(tb)<0._pr) then
                x2 = x1 + dy
              else
                x2 = x1 - dy
              endif
              if (sin(tb)<0._pr) then
                y2 = y1 + dz
              else
                y2 = y1 - dz
              endif
              LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
           enddo
           px = xx(l)
           !py = rp*cos(tb)+points_courbe_equal(1,2) +&
           !     cos(tb)*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
           !pz = rp*sin(tb)+zz(nint(zslice)) +&
           !     sin(tb)*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
           if (cos(tb)<0._pr) then
             py = (-yy(1)-rp+points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2) +&
                  -cos(tb)*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
           else
             py = (yy(ny)-rp-points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2) +&
                  -cos(tb)*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
           endif
           if (sin(tb)<0._pr) then
             pz = (-zz(1)-rp+zz(nint(zslice)))*sin(tb)+zz(nint(zslice)) +&
                  -sin(tb)*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
           else
             pz = (zz(nz)-rp-zz(nint(zslice)))*sin(tb)+zz(nint(zslice))  +&
                  -sin(tb)*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
           endif

           sth(l) = sinit + sqrt((px-slicecontroltmp(nt-1,l-itail+1,1))**2 + (py-slicecontroltmp(nt-1,l-itail+1,2))**2 +&
                (pz-slicecontroltmp(nt-1,l-itail+1,3))**2)
           if ((ll-1)*dsth(l)-sth(l)>0._pr) then
              tbm = tb
           else
              tbp = tb
           endif
           if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
              bool = 1
              !            if ((ll>1).and.(l==itail+size(slicecontrol,2)/2-1)) write(*,*) "aie aie aie  ",l," ",ll,"    ",abs(tb-(tbm+tbp)*0.5_pr)
           endif
        enddo
        rr = 0._pr !yy(ny-2)-yy(ny/2)!0._pr
        booltmp = 0
        !do while (((LSrr<0._pr).and.(rr<yy(ny-2)-yy(ny/2))).or.(booltmp==0))
        do while (((LSrr<0._pr).and.(rr<yy(ny))).or.(booltmp==0))
        !do while (((LSrr>0._pr).and.(rr<yy(ny))).or.(booltmp==0))
           rp = rr
           if (booltmp==1) then
              LSp = LSrr
           else
              !xp = rp*cos(tb)+points_courbe_equal(1,2)
              !yp = rp*sin(tb)+zz(nint(zslice))
              if (cos(tb)<0._pr) then
                xp = (-yy(1)-rp+points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2)
              else
                xp = (yy(ny)-rp-points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2)
              endif
              if (sin(tb)<0._pr) then
                yp = (-zz(1)-rp+zz(nint(zslice)))*sin(tb)+zz(nint(zslice))
              else
                yp = (zz(nz)-rp-zz(nint(zslice)))*sin(tb)+zz(nint(zslice))
              endif
              ip1 = int((xp-x0)/dy)+1
              jp1 = int((yp-x0)/dz)+1
              !ip2 = ip1+1
              !jp2 = jp1+1
              if (cos(tb)<0._pr) then
                ip2 = ip1+1
              else
                ip2 = ip1-1
              endif
              if (sin(tb)<0._pr) then
                jp2 = jp1+1
              else
                jp2 = jp1-1
              endif
              LS1 = rhoSlices(jp1,l,ip1)
              LS2 = rhoSlices(jp1,l,ip2)
              LS3 = rhoSlices(jp2,l,ip1)
              LS4 = rhoSlices(jp2,l,ip2)
              x1 = yy(ip1)
              y1 = zz(jp1)
              !x2 = x1 + dy
              !y2 = y1 + dz
              if (cos(tb)<0._pr) then
                x2 = x1 + dy
              else
                x2 = x1 - dy
              endif
              if (sin(tb)<0._pr) then
                y2 = y1 + dz
              else
                y2 = y1 - dz
              endif
              LSp = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
           endif
           booltmp = 1
           rr=rr+dy
           !rr=rr-dy
           !xp = rr*cos(tb)+points_courbe_equal(1,2)
           !yp = rr*sin(tb)+zz(nint(zslice))
           if (cos(tb)<0._pr) then
             xp = (-yy(1)-rr+points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2)
           else
             xp = (yy(ny)-rr-points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2)
           endif
           if (sin(tb)<0._pr) then
             yp = (-zz(1)-rr+zz(nint(zslice)))*sin(tb)+zz(nint(zslice))
           else
             yp = (zz(nz)-rr-zz(nint(zslice)))*sin(tb)+zz(nint(zslice))
           endif
           ip1 = int((xp-x0)/dy)+1
           jp1 = int((yp-x0)/dz)+1
           !ip2 = ip1+1
           !jp2 = jp1+1
           if (cos(tb)<0._pr) then
             ip2 = ip1+1
           else
             ip2 = ip1-1
           endif
           if (sin(tb)<0._pr) then
             jp2 = jp1+1
           else
             jp2 = jp1-1
           endif
           LS1 = rhoSlices(jp1,l,ip1)
           LS2 = rhoSlices(jp1,l,ip2)
           LS3 = rhoSlices(jp2,l,ip1)
           LS4 = rhoSlices(jp2,l,ip2)
           x1 = yy(ip1)
           y1 = zz(jp1)
           !x2 = x1 + dy
           !y2 = y1 + dz
           if (cos(tb)<0._pr) then
             x2 = x1 + dy
           else
             x2 = x1 - dy
           endif
           if (sin(tb)<0._pr) then
             y2 = y1 + dz
           else
             y2 = y1 - dz
           endif
           LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
        enddo
        rr = rp
        LSrr = LSp
        !do while ((LSrr<0._pr).and.(rr<float(ny)))
        do while ((LSrr<0._pr).and.(rr<yy(ny)))
           rp = rr
           LSp = LSrr
           rr=rr+0.1*dy
           !xp = rr*cos(tb)+points_courbe_equal(1,2)
           !yp = rr*sin(tb)+zz(nint(zslice))
           if (cos(tb)<0._pr) then
             xp = (-yy(1)-rr+points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2)
           else
             xp = (yy(ny)-rr-points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2)
           endif
           if (sin(tb)<0._pr) then
             yp = (-zz(1)-rr+zz(nint(zslice)))*sin(tb)+zz(nint(zslice))
           else
             yp = (zz(nz)-rr-zz(nint(zslice)))*sin(tb)+zz(nint(zslice))
           endif
           ip1 = int((xp-x0)/dy)+1
           jp1 = int((yp-x0)/dz)+1
           !ip2 = ip1+1
           !jp2 = jp1+1
           if (cos(tb)<0._pr) then
             ip2 = ip1+1
           else
             ip2 = ip1-1
           endif
           if (sin(tb)<0._pr) then
             jp2 = jp1+1
           else
             jp2 = jp1-1
           endif
           LS1 = rhoSlices(jp1,l,ip1)
           LS2 = rhoSlices(jp1,l,ip2)
           LS3 = rhoSlices(jp2,l,ip1)
           LS4 = rhoSlices(jp2,l,ip2)
           x1 = yy(ip1)
           y1 = zz(jp1)
           !x2 = x1 + dy
           !y2 = y1 + dz
           if (cos(tb)<0._pr) then
             x2 = x1 + dy
           else
             x2 = x1 - dy
           endif
           if (sin(tb)<0._pr) then
             y2 = y1 + dz
           else
             y2 = y1 - dz
           endif
           LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
           if (ll==1+nbtheta/2)   write(*,*) "totototo00  ",l," ",px," ",py," ",pz," ",LSrr," ",LSp," ",rr," ",yy(ny)
        enddo
        px = xx(l)
        !py = rp*cos(tb)+points_courbe_equal(1,2) +&
        !     cos(tb)*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
        !pz = rp*sin(tb)+zz(nint(zslice)) +&
        !     sin(tb)*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
        if (cos(tb)<0._pr) then
          py = (-yy(1)-rp+points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2) +&
               -cos(tb)*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
        else
          py = (yy(ny)-rp-points_courbe_equal(1,2))*cos(tb)+points_courbe_equal(1,2) +&
               -cos(tb)*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
        endif
        if (sin(tb)<0._pr) then
          pz = (-zz(1)-rp+zz(nint(zslice)))*sin(tb)+zz(nint(zslice)) +&
               -sin(tb)*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
        else
          pz = (zz(nz)-rp-zz(nint(zslice)))*sin(tb)+zz(nint(zslice))  +&
               -sin(tb)*LSP*(rr-rp)/abs(LSrr - LSp)  !rr+1
        endif
        !!        call pointsBezierN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
        !if ((l-lpt+1<=size(slicecontroli,2)).and.(l-lpt+1>0)) then
        if (l<=nint((points_courbe_equal(1,1)-x0)/dx+1)) then
        !if (l<=ceiling((points_courbe_equal(1,1)-x0)/dx+1)) then
           !        if (lpt==0) lpt = l
           slicecontroli(ll,l-lpt+2,1) = px !xx(l)
           slicecontroli(ll,l-lpt+2,2) = py !rp*cos(valTh(l-itail+1,ll))+points_courbe_equal(1,2) + cos(valTh(l-itail+1,ll))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
           slicecontroli(ll,l-lpt+2,3) = pz !rp*sin(valTh(l-itail+1,ll))+zslice + sin(valTh(l-itail+1,ll))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
           if (ll==1+nbtheta/2) write(*,*) "totototo1  ",l," ",ll," ",px," ",py," ",pz&
           ," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)," ",l-lpt+1
           !if (ll==1) write(*,*) "totototo1  ",l," ",ll," ",px," ",py," ",pz&
           !," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)," ",l-lpt+1
        endif
        !if ((l-lp+1<=size(slicecontrolm,2)).and.(l-lp+1>0)) then
        if ((l>=nint((points_courbe_equal(1,1)-x0)/dx+1)).and.(l<=nint((points_courbe_equal(Ns,1)-x0)/dx+1))) then
        !if ((l>=ceiling((points_courbe_equal(1,1)-x0)/dx+1)).and.(l<=floor((points_courbe_equal(Ns,1)-x0)/dx+1))) then
           !        if (lp==0) lp = l
           slicecontrolm(ll,l-lp+1,1) = px !xx(l)
           slicecontrolm(ll,l-lp+1,2) = py !rp*cos(valTh(l-itail+1,ll))+points_courbe_equal(1,2) + cos(valTh(l-itail+1,ll))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
           slicecontrolm(ll,l-lp+1,3) = pz !rp*sin(valTh(l-itail+1,ll))+zslice + sin(valTh(l-itail+1,ll))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
           if (ll==1+nbtheta/2) write(*,*) "totototo2  ",l," ",ll," ",px," ",py," ",pz&
           ," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)," ",l-lp+1
           !if (ll==1) write(*,*) "totototo2  ",l," ",ll," ",px," ",py," ",pz&
           !," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)," ",l-lp+1
        endif
        !if ((l-lph+1<=size(slicecontrolf,2)).and.(l-lph+1>0)) then
        if (l>=nint((points_courbe_equal(Ns,1)-x0)/dx+1)) then
        !if (l>=floor((points_courbe_equal(Ns,1)-x0)/dx+1)) then
           !        if (lph==0) lph = l
           slicecontrolf(ll,l-lph+1,1) = px !xx(l)
           slicecontrolf(ll,l-lph+1,2) = py !rp*cos(valTh(l-itail+1,ll))+points_courbe_equal(1,2) + cos(valTh(l-itail+1,ll))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
           slicecontrolf(ll,l-lph+1,3) = pz !rp*sin(valTh(l-itail+1,ll))+zslice + sin(valTh(l-itail+1,ll))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
        endif


        !if ((l<itail+5).and.(ll==1))   write(*,*) "totototo11  ",l," ",px," ",py," ",pz&
        !," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)&
        !   ," ",nint((points_courbe_equal(1,1)-x0)/dx+1)
        !if ((l<itail+5).and.(ll==2))   write(*,*) "totototo22  ",l," ",px," ",py," ",pz&
        !," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)&
        !   ," ",nint((points_courbe_equal(1,1)-x0)/dx+1)
        !if ((l-lpt+1<=size(slicecontroli,2)).and.(ll==1))   write(*,*) "totototo11  ",l," ",slicecontroli(ll,l-lpt+1,1)&
        !," ",slicecontroli(ll,l-lpt+1,2)," ",slicecontroli(ll,l-lpt+1,3)&
        !," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)&
        !   ," ",nint((points_courbe_equal(1,1)-x0)/dx+1)
        !if ((l-lpt+1<=size(slicecontroli,2)).and.(ll==2))   write(*,*) "totototo22  ",l," ",slicecontroli(ll,l-lpt+1,1)&
        !," ",slicecontroli(ll,l-lpt+1,2)," ",slicecontroli(ll,l-lpt+1,3)&
        !," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)&
        !   ," ",nint((points_courbe_equal(1,1)-x0)/dx+1)
        slicecontrol(ll,l-itail+1,1) = px !xx(l)
        slicecontrol(ll,l-itail+1,2) = py !rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) + cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
        slicecontrol(ll,l-itail+1,3) = pz !rp*sin(valTh(l-itail+1,theta))+zslice + sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
        valTh(l-itail+1,ll) = tb
        valThtmp(ll,l-itail+1) = tb
        !        if ((ll>1).and.(l==itail+size(slicecontrol,2)/2-1)) write(*,*) "THETAcoupe  ",ll," ",l," ",valTh(l-itail+1,ll)&
        !,"  dTh  ",valTh(l-itail+1,ll)-valTh(l-itail+1,ll-1),"  dsth  ",sqrt((slicecontrol(ll,l-itail+1,1)-slicecontrol(ll-1,l-itail+1,1))&
        !**2 + (slicecontrol(ll,l-itail+1,2)-slicecontrol(ll-1,l-itail+1,2))**2 + (slicecontrol(ll,l-itail+1,3)-&
        !slicecontrol(ll-1,l-itail+1,3))**2)," ",dsth(l)&
        !,"     ",tb," ",tbm," ",tbp!," ",cos(tb)," ",sin(tb)," ",cos(valTh(l-itail+1,ll))," ",sin(valTh(l-itail+1,ll))
        !        if (ll==1+nbtheta/2) write(*,*) "THETAcoupe  ",ll," ",l," ",valTh(l-itail+1,ll)&
        !,"  dTh  ",valTh(l-itail+1,ll)-valTh(l-itail+1,ll-1),"  dsth  ",sqrt((slicecontrol(ll,l-itail+1,1)-slicecontrol(ll-1,l-itail+1,1))&
        !**2 + (slicecontrol(ll,l-itail+1,2)-slicecontrol(ll-1,l-itail+1,2))**2 + (slicecontrol(ll,l-itail+1,3)-&
        !slicecontrol(ll-1,l-itail+1,3))**2)," ",dsth(l)&
        !,"     ",tb," ",cos(tb)," ",sin(tb)," ",cos(valTh(l-itail+1,ll))," ",sin(valTh(l-itail+1,ll))
        !dist(slicecontrol(ll,l-itail+1,2),slicecontrol(ll,l-itail+1,3),slicecontrol(ll-1,l-itail+1,2),slicecontrol(ll-1,l-itail+1,3))
     enddo
     !  if (l==Ns-1) then
     !          l = l+1
     !  slice(theta,Ns+Ni-1,1) = slicecontrolm(theta,size(slicecontrolm,2),1)
     !  slice(theta,Ns+Ni-1,2) = slicecontrolm(theta,size(slicecontrolm,2),2)
     !  slice(theta,Ns+Ni-1,3) = slicecontrolm(theta,size(slicecontrolm,2),3)
     !     write(*,*) " ET VOILA"
     !     endif
     if (.not.(ll==nbtheta)) write(*,*) "WARNIINNGG c est moiTHETA  ",ll," ",l," ",tb
  enddo
  do theta=1,nbtheta
     l=itail+1
     rr = 0._pr
     bool = 0
     do while (((LSrr<0._pr).and.(rr<float(ny))).or.(bool==0))
     !do while (((LSrr>0._pr).and.(rr<yy(ny))).or.(bool==0))
        rp = rr
        if (bool==1) then
           LSp = LSrr
        else
           !xp = rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
           !yp = rp*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
           !xp = rp*cos(valTh(l-itail+1,theta))+(slicecontroltmp(1,2,2)+slicecontroltmp(nint(1+0.5_pr*nbtheta),2,2))*0.5_pr
           !yp = rp*sin(valTh(l-itail+1,theta))+(slicecontroltmp(nint(1+0.25_pr*nbtheta),2,3)+&
           !     slicecontroltmp(nint(1+0.75_pr*nbtheta),2,3))*0.5_pr
           if (cos(valTh(l-itail+1,theta))<0._pr) then
             xp = (-yy(1)-rp+points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
           else
             xp = (yy(ny)-rp-points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
           endif
           if (sin(valTh(l-itail+1,theta))<0._pr) then
             yp = (-zz(1)-rp+zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
           else
             yp = (zz(nz)-rp-zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
           endif
           ip1 = int((xp-x0)/dy)+1
           jp1 = int((yp-x0)/dz)+1
           !ip2 = ip1+1
           !jp2 = jp1+1
           if (cos(valTh(l-itail+1,theta))<0._pr) then
             ip2 = ip1+1
           else
             ip2 = ip1-1
           endif
           if (sin(valTh(l-itail+1,theta))<0._pr) then
             jp2 = jp1+1
           else
             jp2 = jp1-1
           endif
           LS1 = rhoSlices(jp1,l,ip1)
           LS2 = rhoSlices(jp1,l,ip2)
           LS3 = rhoSlices(jp2,l,ip1)
           LS4 = rhoSlices(jp2,l,ip2)
           x1 = yy(ip1)
           y1 = zz(jp1)
           !x2 = x1 + dy
           !y2 = y1 + dz
           if (cos(valTh(l-itail+1,theta))<0._pr) then
             x2 = x1 + dy
           else
             x2 = x1 - dy
           endif
           if (sin(valTh(l-itail+1,theta))<0._pr) then
             y2 = y1 + dz
           else
             y2 = y1 - dz
           endif
           LSp = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
           if (theta==1) write(*,*) "checknantail  ",x1," ",y1," ",LSrr," ",LSp
        endif

        bool = 1
        rr=rr+dy
        !xp = rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
        !yp = rr*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
        !xp = rr*cos(valTh(l-itail+1,theta))+(slicecontroltmp(1,2,2)+slicecontroltmp(nint(1+0.5_pr*nbtheta),2,2))*0.5_pr
        !yp = rr*sin(valTh(l-itail+1,theta))+(slicecontroltmp(nint(1+0.25_pr*nbtheta),2,3)+&
        !     slicecontroltmp(nint(1+0.75_pr*nbtheta),2,3))*0.5_pr
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          xp = (-yy(1)-rr+points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
        else
          xp = (yy(ny)-rr-points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          yp = (-zz(1)-rr+zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
        else
          yp = (zz(nz)-rr-zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
        endif
        ip1 = int((xp-x0)/dy)+1
        jp1 = int((yp-x0)/dz)+1
        !ip2 = ip1+1
        !jp2 = jp1+1
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          ip2 = ip1+1
        else
          ip2 = ip1-1
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          jp2 = jp1+1
        else
          jp2 = jp1-1
        endif
        LS1 = rhoSlices(jp1,l,ip1)
        LS2 = rhoSlices(jp1,l,ip2)
        LS3 = rhoSlices(jp2,l,ip1)
        LS4 = rhoSlices(jp2,l,ip2)
        x1 = yy(ip1)
        y1 = zz(jp1)
        !x2 = x1 + dy
        !y2 = y1 + dz
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          x2 = x1 + dy
        else
          x2 = x1 - dy
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          y2 = y1 + dz
        else
          y2 = y1 - dz
        endif
        LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
     enddo
     rr = rp
     LSrr = LSp
     !do while ((LSrr<0._pr).and.(rr<float(ny)))
     do while ((LSrr<0._pr).and.(rr<yy(ny)))
        rp = rr
        LSp = LSrr

        rr=rr+0.1*dy
        !xp = rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
        !yp = rr*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
        !xp = rr*cos(valTh(l-itail+1,theta))+(slicecontroltmp(1,2,2)+slicecontroltmp(nint(1+0.5_pr*nbtheta),2,2))*0.5_pr
        !yp = rr*sin(valTh(l-itail+1,theta))+(slicecontroltmp(nint(1+0.25_pr*nbtheta),2,3)+&
        !     slicecontroltmp(nint(1+0.75_pr*nbtheta),2,3))*0.5_pr
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          xp = (-yy(1)-rr+points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
        else
          xp = (yy(ny)-rr-points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          yp = (-zz(1)-rr+zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
        else
          yp = (zz(nz)-rr-zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
        endif
        ip1 = int((xp-x0)/dy)+1
        jp1 = int((yp-x0)/dz)+1
        !ip2 = ip1+1
        !jp2 = jp1+1
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          ip2 = ip1+1
        else
          ip2 = ip1-1
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          jp2 = jp1+1
        else
          jp2 = jp1-1
        endif
        LS1 = rhoSlices(jp1,l,ip1)
        LS2 = rhoSlices(jp1,l,ip2)
        LS3 = rhoSlices(jp2,l,ip1)
        LS4 = rhoSlices(jp2,l,ip2)
        x1 = yy(ip1)
        y1 = zz(jp1)
        !x2 = x1 + dy
        !y2 = y1 + dz
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          x2 = x1 + dy
        else
          x2 = x1 - dy
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          y2 = y1 + dz
        else
          y2 = y1 - dz
        endif
        LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
     enddo
     !     slice(theta,1,1) = rtail
     !     slice(theta,1,2) = 0.1*rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&
     !          0.1*cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
     !     slice(theta,1,3) = 0.1*rp*sin(valTh(l-itail+1,theta))+zslice +&
     !          0.1*sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
     px = rtail
     !py = rp*cos(valTh(l-itail+1,theta))+(slicecontroltmp(1,2,2) + slicecontroltmp(nint(1+0.5_pr*nbtheta),2,2))*0.5_pr&
     !     + cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
     py = 0.01_pr*rp*cos(valTh(l-itail+1,theta))+(slicecontroltmp(1,2,2) + slicecontroltmp(nint(1+0.5_pr*nbtheta),2,2))*0.5_pr&
          + 0.01_pr*cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
     !pz = 0.01_pr*rp*sin(valTh(l-itail+1,theta)) + (slicecontroltmp(nint(1+0.25_pr*nbtheta),2,3)+&
     !     slicecontroltmp(nint(1+0.75_pr*nbtheta),2,3))*0.5_pr&
     !     + 0.01_pr*sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
     pz = 0.01_pr*rp*sin(valTh(l-itail+1,theta)) + (slicecontroltmp(nint(1+0.25_pr*nbtheta),2,3)+&
          slicecontroltmp(nint(1+0.75_pr*nbtheta),2,3))*0.5_pr&
          + 0.01_pr*sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
     !py = 0.1_pr*cos(valTh(l-itail+1,theta))*abs(slicecontroltmp(theta,2,2)-points_courbe_equal(1,2))+&
     !     +     points_courbe_equal(1,2)
     !pz = 0.1_pr*sin(valTh(l-itail+1,theta))*abs(slicecontroltmp(theta,2,2)-zz(nint(zslice)))+&
     !     +     zz(nint(zslice))
     slicecontroli(theta,1,1) = px !xx(l)
     slicecontroli(theta,1,2) = py !rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) + cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
     slicecontroli(theta,1,3) = pz !rp*sin(valTh(l-itail+1,theta))+zslice + sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
     slicecontrol(theta,1,1) = px !xx(l)
     slicecontrol(theta,1,2) = py !rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) + cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
     slicecontrol(theta,1,3) = pz !rp*sin(valTh(l-itail+1,theta))+zslice + sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
     if (theta==1) write(*,*) "checknantail  ",slicecontrol(theta,1,2)," ",slicecontrol(theta,1,3)," ",LSrr," ",LSp
     l=ihead-1!+1
     rr = 0._pr
     bool = 0
     !do while (((LSrr<0._pr).and.(rr<float(ny))).or.(bool==0))
     do while (((LSrr<0._pr).and.(rr<yy(ny))).or.(bool==0))
        rp = rr
        if (bool==1) then
           LSp = LSrr
        else
           !           xp = rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
           !           yp = rp*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
           !xp = rp*cos(valTh(l-itail+1,theta))+(slicecontroltmp(1,size(slicecontroltmp,2)-1,2)+&
           !     slicecontroltmp(nint(1+0.5_pr*nbtheta),size(slicecontroltmp,2)-1,2))*0.5_pr
           !yp = rp*sin(valTh(l-itail+1,theta))+(slicecontroltmp(nint(1+0.25_pr*nbtheta),size(slicecontroltmp,2)-1,3)+&
           !     slicecontroltmp(nint(1+0.75_pr*nbtheta),size(slicecontroltmp,2)-1,3))*0.5_pr
           if (cos(valTh(l-itail+1,theta))<0._pr) then
             xp = (-yy(1)-rp+points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
           else
             xp = (yy(ny)-rp-points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
           endif
           if (sin(valTh(l-itail+1,theta))<0._pr) then
             yp = (-zz(1)-rp+zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
           else
             yp = (zz(nz)-rp-zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
           endif
           ip1 = int((xp-x0)/dy)+1
           jp1 = int((yp-x0)/dz)+1
           !ip2 = ip1+1
           !jp2 = jp1+1
           if (cos(valTh(l-itail+1,theta))<0._pr) then
             ip2 = ip1+1
           else
             ip2 = ip1-1
           endif
           if (sin(valTh(l-itail+1,theta))<0._pr) then
             jp2 = jp1+1
           else
             jp2 = jp1-1
           endif
           LS1 = rhoSlices(jp1,l,ip1)
           LS2 = rhoSlices(jp1,l,ip2)
           LS3 = rhoSlices(jp2,l,ip1)
           LS4 = rhoSlices(jp2,l,ip2)
           x1 = yy(ip1)
           y1 = zz(jp1)
           !x2 = x1 + dy
           !y2 = y1 + dz
           if (cos(valTh(l-itail+1,theta))<0._pr) then
             x2 = x1 + dy
           else
             x2 = x1 - dy
           endif
           if (sin(valTh(l-itail+1,theta))<0._pr) then
             y2 = y1 + dz
           else
             y2 = y1 - dz
           endif
           LSp = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
        endif

        bool = 1
        rr=rr+dy
        !xp = rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
        !yp = rr*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
        !xp = rr*cos(valTh(l-itail+1,theta))+(slicecontroltmp(1,size(slicecontroltmp,2)-1,2)+&
        !     slicecontroltmp(nint(1+0.5_pr*nbtheta),size(slicecontroltmp,2)-1,2))*0.5_pr
        !yp = rr*sin(valTh(l-itail+1,theta))+(slicecontroltmp(nint(1+0.25_pr*nbtheta),size(slicecontroltmp,2)-1,3)+&
        !     slicecontroltmp(nint(1+0.75_pr*nbtheta),size(slicecontroltmp,2)-1,3))*0.5_pr
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          xp = (-yy(1)-rr+points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
        else
          xp = (yy(ny)-rr-points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          yp = (-zz(1)-rr+zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
        else
          yp = (zz(nz)-rr-zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
        endif
        ip1 = int((xp-x0)/dy)+1
        jp1 = int((yp-x0)/dz)+1
        !ip2 = ip1+1
        !jp2 = jp1+1
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          ip2 = ip1+1
        else
          ip2 = ip1-1
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          jp2 = jp1+1
        else
          jp2 = jp1-1
        endif
        LS1 = rhoSlices(jp1,l,ip1)
        LS2 = rhoSlices(jp1,l,ip2)
        LS3 = rhoSlices(jp2,l,ip1)
        LS4 = rhoSlices(jp2,l,ip2)
        x1 = yy(ip1)
        y1 = zz(jp1)
        !x2 = x1 + dy
        !y2 = y1 + dz
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          x2 = x1 + dy
        else
          x2 = x1 - dy
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          y2 = y1 + dz
        else
          y2 = y1 - dz
        endif
        LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
     enddo
     rr = rp
     LSrr = LSp
     !do while ((LSrr<0._pr).and.(rr<float(ny)))
     do while ((LSrr<0._pr).and.(rr<yy(ny)))
        rp = rr
        LSp = LSrr

        rr=rr+0.1*dy
        !xp = rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
        !yp = rr*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
        !xp = rr*cos(valTh(l-itail+1,theta))+(slicecontroltmp(1,size(slicecontroltmp,2)-1,2)+&
        !     slicecontroltmp(nint(1+0.5_pr*nbtheta),size(slicecontroltmp,2)-1,2))*0.5_pr
        !yp = rr*sin(valTh(l-itail+1,theta))+(slicecontroltmp(nint(1+0.25_pr*nbtheta),size(slicecontroltmp,2)-1,3)+&
        !     slicecontroltmp(nint(1+0.75_pr*nbtheta),size(slicecontroltmp,2)-1,3))*0.5_pr
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          xp = (-yy(1)-rr+points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
        else
          xp = (yy(ny)-rr-points_courbe_equal(1,2))*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          yp = (-zz(1)-rr+zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
        else
          yp = (zz(nz)-rr-zz(nint(zslice)))*sin(valTh(l-itail+1,theta))+zz(nint(zslice))
        endif
        ip1 = int((xp-x0)/dy)+1
        jp1 = int((yp-x0)/dz)+1
        !ip2 = ip1+1
        !jp2 = jp1+1
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          ip2 = ip1+1
        else
          ip2 = ip1-1
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          jp2 = jp1+1
        else
          jp2 = jp1-1
        endif
        LS1 = rhoSlices(jp1,l,ip1)
        LS2 = rhoSlices(jp1,l,ip2)
        LS3 = rhoSlices(jp2,l,ip1)
        LS4 = rhoSlices(jp2,l,ip2)
        x1 = yy(ip1)
        y1 = zz(jp1)
        !x2 = x1 + dy
        !y2 = y1 + dz
        if (cos(valTh(l-itail+1,theta))<0._pr) then
          x2 = x1 + dy
        else
          x2 = x1 - dy
        endif
        if (sin(valTh(l-itail+1,theta))<0._pr) then
          y2 = y1 + dz
        else
          y2 = y1 - dz
        endif
        LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
     enddo
     !     slice(theta,size(slice,2),1) = rhead
     !     slice(theta,size(slice,2),2) = 0.1*rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)&
     !+ 0.1*cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
     !     slice(theta,size(slice,2),3) = 0.1*rp*sin(valTh(l-itail+1,theta))+zslice + 0.1*sin(valTh(l-itail+1,theta))*LSp*(rr-rp)&
     !/abs(LSrr - LSp) !rr+1
     px = rhead
     !py = 0.01_pr*rp*cos(valTh(l-itail+1,theta)) + (slicecontroltmp(1,size(slicecontroltmp,2)-1,2)+&
     !     slicecontroltmp(nint(1+0.5_pr*nbtheta),size(slicecontroltmp,2)-1,2))*0.5_pr&
     !     + 0.01_pr*cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
     !pz = 0.01_pr*rp*sin(valTh(l-itail+1,theta)) + (slicecontroltmp(nint(1+0.25_pr*nbtheta),size(slicecontroltmp,2)-1,3)+&
     !     slicecontroltmp(nint(1+0.75_pr*nbtheta),size(slicecontroltmp,2)-1,3))*0.5_pr&
     !     + 0.01_pr*sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
     !py = 0.1_pr*cos(valTh(l-itail+1,theta))*abs(slicecontroltmp(theta,2,2)-points_courbe_equal(1,2))+&
     !     +     points_courbe_equal(1,2)
     !pz = 0.1_pr*sin(valTh(l-itail+1,theta))*abs(slicecontroltmp(theta,2,2)-zz(nint(zslice)))+&
     !     +     zz(nint(zslice))
     py = 0.01_pr*rp*cos(valTh(l-itail+1,theta)) + (slicecontroltmp(1,size(slicecontroltmp,2)-1,2)+&
          slicecontroltmp(nint(1+0.5_pr*nbtheta),size(slicecontroltmp,2)-1,2))*0.5_pr&
          + 0.01_pr*cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
     pz = 0.01_pr*rp*sin(valTh(l-itail+1,theta)) + (slicecontroltmp(nint(1+0.25_pr*nbtheta),size(slicecontroltmp,2)-1,3)+&
          slicecontroltmp(nint(1+0.75_pr*nbtheta),size(slicecontroltmp,2)-1,3))*0.5_pr&
          + 0.01_pr*sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
     slicecontrolf(theta,size(slicecontrolf,2),1) = px !xx(l)
     slicecontrolf(theta,size(slicecontrolf,2),2) = py !rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) + cos(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
     slicecontrolf(theta,size(slicecontrolf,2),3) = pz !rp*sin(valTh(l-itail+1,theta))+zslice + sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
     slicecontrol(theta,size(slicecontrol,2),1) = px !xx(l)
     slicecontrol(theta,size(slicecontrol,2),2) = py !rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) + cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
     slicecontrol(theta,size(slicecontrol,2),3) = pz !rp*sin(valTh(l-itail+1,theta))+zslice + sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
  enddo

!  do l=ihead,nint((rhead-x0)/dx+1)
!    do theta=1,nbtheta
!      tb = (l-points_courbe_equal(Ns,1))/(ceiling((rhead-x0)/dx+1)-points_courbe_equal(Ns,1))
!      call pointsBezierN3D(slicecontrol(theta,size(slicecontrol,2)-3:size(slicecontrol,2)-2,:),0.9_pr,pxx,pyy,pzz)
!      !pxx = slicecontrol(theta,size(slicecontrol,2)-8,1)
!      !pyy = slicecontrol(theta,size(slicecontrol,2)-8,2)
!      !pzz = slicecontrol(theta,size(slicecontrol,2)-8,3)
!      tb = (l-pxx)*1._pr/(nint((rhead-x0)/dx+1)-pxx)
!      tp = (slicecontrol(theta,size(slicecontrol,2)-2,1)-pxx)/(slicecontrol(theta,size(slicecontrol,2),1)-pxx)
!      call pointsBezierN3D(slicecontrol(theta,points_courbe_equal(Ns,1)-itail+1:ihead-itail+1,:),0.9_pr,pxx,pyy,pzz)
!      call pointsBezierN3D(slicecontrol(theta,points_courbe_equal(Ns,1)-itail+1:ihead-itail+1,:),0.9_pr,pxx,pyy,pzz)
!      call pointsBezierN3D(slicecontrol(theta,points_courbe_equal(Ns,1)-itail+1:ihead-itail+1,:),0.9_pr,pxx,pyy,pzz)
!      !call poly2(pxx,slicecontrol(theta,size(slicecontrol,2)-2,1),slicecontrol(theta,size(slicecontrol,2),1),tb,tp,px) 
!      !call poly2(pyy,slicecontrol(theta,size(slicecontrol,2)-2,2),slicecontrol(theta,size(slicecontrol,2),2),tb,tp,py) 
!      !call poly2(pzz,slicecontrol(theta,size(slicecontrol,2)-2,3),slicecontrol(theta,size(slicecontrol,2),3),tb,tp,pz) 
!      slicecontrolf(theta,l-lph+1,1) = px !xx(l)
!      slicecontrolf(theta,l-lph+1,2) = py !rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) + cos(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
!      slicecontrolf(theta,l-lph+1,3) = pz !rp*sin(valTh(l-itail+1,theta))+zslice + sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
!      if (theta==1) write(*,*) "AH      ",floor((points_courbe_equal(Ns,1)-x0)/dx+1)," "&
!      ,ceiling((rhead-x0)/dx+1)," ",floor((points_courbe_equal(Ns,1)-x0)/dx+1)
!      if (theta==1) write(*,*) "AH      ",nint((points_courbe_equal(Ns,1)-x0)/dx+1)," "&
!      ,nint((rhead-x0)/dx+1)," ",nint((points_courbe_equal(Ns,1)-x0)/dx+1)
!      if (theta==1) write(*,*) "TBsliceFIN  ",l," ",tb," ",tb*tp," ",tp," ",px," ",py," ",pz," ",pxx&
!      ," ",slicecontrol(theta,size(slicecontrol,2)-2,1)," ",slicecontrol(theta,size(slicecontrol,2),1)
!      if (theta==1) write(*,*) "TBsliceFIN  ",l," ",tb," ",tb*tp," ",tp," ",px," ",py," ",pz," ",pyy&
!      ," ",slicecontrol(theta,size(slicecontrol,2)-2,2)," ",slicecontrol(theta,size(slicecontrol,2),2)
!      if (theta==1) write(*,*) "TBsliceFIN  ",l," ",tb," ",tb*tp," ",tp," ",px," ",py," ",pz," ",pzz&
!      ," ",slicecontrol(theta,size(slicecontrol,2)-2,3)," ",slicecontrol(theta,size(slicecontrol,2),3)
!    enddo
!  enddo
!  do l=nint((rtail-x0)/dx+1),itail-1
!    do theta=1,nbtheta
!      tb = (l-nint((rtail-x0)/dx+1))/(points_courbe_equal(1,1)-nint((rtail-x0)/dx+1))
!      call pointsBezierN3D(slicecontrol(theta,2:3,:),0.1_pr,pxx,pyy,pzz)
!      !pxx = slicecontrol(theta,8,1)
!      !pyy = slicecontrol(theta,8,2)
!      !pzz = slicecontrol(theta,8,3)
!      tb = (l-nint((rtail-x0)/dx+1))*1._pr/(pxx-nint((rtail-x0)/dx+1))
!      tp = (slicecontrol(theta,2,1)-slicecontrol(theta,1,1))/(pxx-slicecontrol(theta,1,1))
!      call poly2(slicecontrol(theta,1,1),slicecontrol(theta,2,1),pxx,tb,tp,px) 
!      call poly2(slicecontrol(theta,1,2),slicecontrol(theta,2,2),pyy,tb,tp,py) 
!      call poly2(slicecontrol(theta,1,3),slicecontrol(theta,2,3),pzz,tb,tp,pz) 
!      slicecontroli(theta,lpt-l+1,1) = px !xx(l)
!      slicecontroli(theta,lpt-l+1,2) = py !rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) + cos(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
!      slicecontroli(theta,lpt-l+1,3) = pz !rp*sin(valTh(l-itail+1,theta))+zslice + sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
!      if (theta==1) write(*,*) "AH      ",floor((points_courbe_equal(1,1)-x0)/dx+1)," "&
!      ,floor((rtail-x0)/dx+1)," ",floor((points_courbe_equal(1,1)-x0)/dx+1)," ",lpt
!      if (theta==1) write(*,*) "AH      ",nint((points_courbe_equal(1,1)-x0)/dx+1)," "&
!      ,nint((rtail-x0)/dx+1)," ",nint((points_courbe_equal(1,1)-x0)/dx+1)
!      if (theta==1) write(*,*) "TBsliceINI  ",l," ",tb," ",tb*tp," ",tp," ",px," ",py," ",pz," ",pxx&
!      ," ",slicecontrol(theta,1,1)," ",slicecontrol(theta,2,1)
!      if (theta==1) write(*,*) "TBsliceINI  ",l," ",tb," ",tb*tp," ",tp," ",px," ",py," ",pz," ",pyy&
!      ," ",slicecontrol(theta,1,2)," ",slicecontrol(theta,2,2)
!      if (theta==1) write(*,*) "TBsliceINI  ",l," ",tb," ",tb*tp," ",tp," ",px," ",py," ",pz," ",pzz&
!      ," ",slicecontrol(theta,1,3)," ",slicecontrol(theta,2,3)
!    enddo
!  enddo


  !**
  !! Spline approximation of contour/surface control points
  !**
  dtb = 1._pr/(Ni-1)
  tb = dtb
  do theta=1,nbtheta
     !     slicecontroli(theta,1,1) = rtail
     !     slicecontroli(theta,1,2) = points_courbe_equal(1,2)
     !     slicecontroli(theta,1,3) = zslice
     !     slicecontrol(theta,1,1) = rtail
     !     slicecontrol(theta,1,2) = points_courbe_equal(1,2)
     !     slicecontrol(theta,1,3) = zslice
     !     slicecontrol(theta,size(slicecontrol,2),1) = rhead
     !     slicecontrol(theta,size(slicecontrol,2),2) = points_courbe_equal(1,2)
     !     slicecontrol(theta,size(slicecontrol,2),3) = zslice
     slice_courbe(theta,1,1) = slicecontroli(theta,1,1)
     slice_courbe(theta,1,2) = slicecontroli(theta,1,2)
     slice_courbe(theta,1,3) = slicecontroli(theta,1,3)
     valTheta(1,theta) = valTh(1,theta)
  enddo
  !!  l = 1
  !!  longslicei = 0._pr
  !!  do while ((tb<1._pr).and.(l+1<Ni+1))
  !!     l = l+1
  !!     !!     call pointsBezierN(slicecontroli(1,:,:),tb,px,py)
  !!     !!     slice_courbe(1,l,1) = px
  !!     !!     slice_courbe(1,l,2) = py
  !!     !!     call pointsBezierN(slicecontroli(2,:,:),tb,px,py)
  !!     !!     slice_courbe(2,l,1) = px
  !!     !!     slice_courbe(2,l,2) = py
  !!     do theta=1,nbtheta
  !!        !call pointsBezierN1D(valThtmp(theta,:),tb*float(Ni)/(Ni+Ns+Nf-2),alpha)
  !!        !call pointsBezierN1D(valThtmp(theta,:),tb*float(Ni)/(Ni+Ns+Nf-2-1),alpha)
  !!        call pointsBezierN1D(valThtmp(theta,lpt-itail+1:lp-itail+1)&
  !!             ,tb,alpha)
  !!        valTheta(l,theta) = alpha
  !!        call pointsBezierN3D(slicecontroli(theta,:,:),tb,px,py,pz)
  !!        slice_courbe(theta,l,1) = px
  !!        slice_courbe(theta,l,2) = py
  !!        slice_courbe(theta,l,3) = pz
  !!     enddo
  !!     do theta=1,nbtheta
  !!        longslicei(theta) = longslicei(theta) +&
  !!             sqrt((slice_courbe(theta,l,1)-slice_courbe(theta,l-1,1))**2 + (slice_courbe(theta,l,2)-slice_courbe(theta,l-1,2))**2 +&
  !!             (slice_courbe(theta,l,3)-slice_courbe(theta,l-1,3))**2)
  !!     enddo
  !!     tb = tb+dtb
  !!  enddo
  dsi = disttail/(Ni-1)
  dsf = disthead/(Nf-1)
  do theta=1,nbtheta
     tb = 0._pr
     if (theta==1) write(*,*) "XX  ",nint((points_courbe_equal(1,1)-x0)/dx+1)&
     ," ",floor((points_courbe_equal(1,1)-x0)/dx+1)&
     ," ",ceiling((points_courbe_equal(1,1)-x0)/dx+1)
     if (theta==1) write(*,*) "XX  ",size(slicecontroltmpi,2)&
     ," ",size(slicecontroli,2)&
     ," ",itail-lpt+2&
     ," ",nint((points_courbe_equal(1,1)-x0)/dx+1)-lpt+2
     slicecontroltmpi(theta,2:size(slicecontroltmpi,2),:) = slicecontroli(theta,itail-lpt+2:&
     nint((points_courbe_equal(1,1)-x0)/dx+1)-lpt+2,:)
     !nint((points_courbe_equal(1,1)-x0)/dx+1)-lpt+2,:)
     slicecontroltmpi(theta,1,:) = slicecontroli(theta,1,:)
     if (theta==1) write(*,*) "XX  ",slicecontroli(theta,:,1)
     if (theta==1) write(*,*) "XXTMP  ",slicecontroltmpi(theta,:,1)
     if (theta==1) write(*,*) "YY  ",slicecontroli(theta,:,2)
     if (theta==1) write(*,*) "YYTMP  ",slicecontroltmpi(theta,:,2)
     do l=Ni,1,-1
        nt = 1
        tbm = 0._pr
        tbp = 1._pr
        tb = tbm 
        bool = 0
        !do while (((abs(sslice(theta))>eepsilon*dx*dx).and.(bool==0)).or.(nt==1))
        do while (((abs(sslice(theta))/(dx*dx)>eepsilon).and.(bool==0)).or.(nt==1))
           tb = (tbm + tbp)*0.5_pr
           nt = nt+1
           !call pointsBezierN3D(slicecontroli(theta,:,:),tb,px,py,pz)
           call pointsBezierN3D(slicecontroltmpi(theta,:,:),tb,px,py,pz)
           !call splineN3D(slicecontroli(theta,:,:),tb,px,py,pz)
           sslice(theta) = dotProd3D(points_courbe_equal(1,1)-(Ni-l)*dsi,points_courbe_equal(1,2),points_courbe_equal(1,3)&
                ,points_courbe_equal(1,1)-(Ni-l-1)*dsi,points_courbe_equal(1,2),points_courbe_equal(1,3),px,py,pz)

           if (sslice(theta)<0._pr) then
              tbm = tb
           else
              tbp = tb
           endif
           if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
              bool = 1
           endif
        enddo
        call pointsBezierN3D(slicecontroltmpi(theta,:,:),tb,px,py,pz)
        slice_courbe(theta,l,1) = px
        slice_courbe(theta,l,2) = py
        slice_courbe(theta,l,3) = pz
        !tb = (l-1)*1._pr/(Ni-1)
        !call pointsBezierN3D(slicecontroli(theta,:,:),tb,px,py,pz)
        !if (theta==1) write(*,*) "TBsliceI  ",l," ",tb
        if (l<=Ni) then
           !call pointsBezierN3D(slicecontrol(theta,2:3,:),0.1_pr,pxx,pyy,pzz)
           !call splineN3D(slicecontrol(theta,2:3,:),0.1_pr,pxx,pyy,pzz)
           call pointsBezierN3D(slicecontrolm(theta,:,:),5.0258636474609375E-003_pr,pxx,pyy,pzz)
           !pxx = slice_courbe(theta,Ni-1,1)
           !pyy = slice_courbe(theta,Ni-1,2)
           !pzz = slice_courbe(theta,Ni-1,3)
           tbb = (px-rtail)*1._pr/(pxx-rtail)
           !tp = (slicecontrol(theta,2,1)-slicecontrol(theta,1,1))/(slicecontrol(theta,3,1)-slicecontrol(theta,1,1))
           !tp = (slicecontroli(theta,2,1)-slicecontroli(theta,1,1))/(pxx-slicecontroli(theta,1,1))
           tp = (slice_courbe(theta,Ni-1,1)-slicecontroli(theta,1,1))/(pxx-slicecontroli(theta,1,1))
           !call poly2(slicecontrol(theta,1,1),slicecontrol(theta,2,1),slicecontrol(theta,3,1),tb*tp,tp,px) 
           !call poly2(slicecontrol(theta,1,2),slicecontrol(theta,2,2),slicecontrol(theta,3,2),tb*tp,tp,py) 
           !call poly2(slicecontrol(theta,1,3),slicecontrol(theta,2,3),slicecontrol(theta,3,3),tb*tp,tp,pz) 
           call poly2(slicecontroli(theta,1,1),slice_courbe(theta,Ni-1,1),pxx,tbb,tp,px) 
           call poly2(slicecontroli(theta,1,2),slice_courbe(theta,Ni-1,2),pyy,tbb,tp,py) 
           call poly2(slicecontroli(theta,1,3),slice_courbe(theta,Ni-1,3),pzz,tbb,tp,pz) 
           slice_courbe(theta,l,1) = px
           slice_courbe(theta,l,2) = py
           slice_courbe(theta,l,3) = pz
           !if (theta==1) write(*,*) "TBsliceII  ",l," ",tb*tp," ",tp," ",px," ",py," ",pz," ",slicecontrol(theta,1,1)&
           !     ," ",slicecontrol(theta,2,1)," ",pxx
           !if (theta==1) write(*,*) "TBsliceII  ",l," ",tb*tp," ",tp," ",px," ",py," ",pz," ",slicecontrol(theta,1,2)&
           !     ," ",slicecontrol(theta,2,2)," ",pyy
           !if (theta==1) write(*,*) "TBsliceII  ",l," ",tb*tp," ",tp," ",px," ",py," ",pz," ",slicecontroli(theta,1,2)&
           !     ," ",slicecontroli(theta,2,2)," ",pyy
        !else
        !   call pointsBezierN3D(slicecontroltmpi(theta,:,:),tb,px,py,pz)
        !   !call splineN3D(slicecontroli(theta,:,:),tb,px,py,pz)
        endif
     enddo
     nt = 1
     tbm = 0._pr
     tbp = 1._pr
     tb = tbm 
     bool = 0
     !do while (((abs(sslice(theta))>eepsilon*dx*dx).and.(bool==0)).or.(nt==1))
     do while (((abs(sslice(theta))/(dx*dx)>eepsilon).and.(bool==0)).or.(nt==1))
        tb = (tbm + tbp)*0.5_pr
        nt = nt+1
        call pointsBezierN3D(slicecontroltmpi(theta,:,:),tb,px,py,pz)
        !call splineN3D(slicecontroli(theta,:,:),tb,px,py,pz)
        !sslice(theta) = dotProd3D(points_courbe_equal(1,1),points_courbe_equal(1,2),points_courbe_equal(1,3)&
        !     !,points_courbe_equal(2,1),points_courbe_equal(2,2),points_courbe_equal(2,3),px,py,pz)
        !     ,points_courbe_equal(2,1),points_courbe_equal(2,2),points_courbe_equal(2,3),px,py,pz)

        sslice(theta) = dotProd3D(points_courbe_equal(1,1),points_courbe_equal(1,2),points_courbe_equal(1,3)&
             ,points_courbe_equal(1,1)+dsi,points_courbe_equal(1,2),points_courbe_equal(1,3),px,py,pz)
        if (theta==1) write(*,*) "SSLICE  ",sslice(theta),"   ",slicecontroltmpi(1,:,1)

        if (sslice(theta)<0._pr) then
           tbm = tb
        else
           tbp = tb
        endif
        if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
           bool = 1
        endif
     enddo
     call pointsBezierN3D(slicecontroltmpi(theta,:,:),tb,px,py,pz)
     if (theta==1) write(*,*) "TBsliceI  ",Ni," ",tb," ",px," ",py," ",pz
     !call splineN3D(slicecontroli(theta,:,:),tb,px,py,pz)
           call pointsBezierN3D(slicecontrolm(theta,:,:),5.0258636474609375E-003_pr,pxx,pyy,pzz)
           tbb = (px-rtail)*1._pr/(pxx-rtail)
           tp = (slice_courbe(theta,Ni-1,1)-slicecontroli(theta,1,1))/(pxx-slicecontroli(theta,1,1))
           call poly2(slicecontroli(theta,1,1),slice_courbe(theta,Ni-1,1),pxx,tbb,tp,px) 
           call poly2(slicecontroli(theta,1,2),slice_courbe(theta,Ni-1,2),pyy,tbb,tp,py) 
           call poly2(slicecontroli(theta,1,3),slice_courbe(theta,Ni-1,3),pzz,tbb,tp,pz) 
     slice_courbe(theta,Ni,1) = px
     slice_courbe(theta,Ni,2) = py
     slice_courbe(theta,Ni,3) = pz
  enddo


  dtb = 1._pr/(Ns-1)
  tb = dtb
  !!  slicecontrolm(1,1,1) = points_courbe_equal(1,1)
  !!  slicecontrolm(1,1,2) = points_courbe_equal(1,2) + disttaillY
  !!  slicecontrolm(1,size(slicecontrolm,2),1) = points_courbe_equal(size(points_courbe_equal,1),1)
  !!  slicecontrolm(1,size(slicecontrolm,2),2) = points_courbe_equal(size(points_courbe_equal,1),2) + distheadlY
  !!  slicecontrolm(2,1,1) = points_courbe_equal(1,1)
  !!  slicecontrolm(2,1,2) = points_courbe_equal(1,2) - disttailrY
  !!  slicecontrolm(2,size(slicecontrolm,2),1) = points_courbe_equal(size(points_courbe_equal,1),1)
  !!  slicecontrolm(2,size(slicecontrolm,2),2) = points_courbe_equal(size(points_courbe_equal,1),2) - distheadrY
  !!  do theta=1,nbtheta
  !!     slicecontrolm(theta,1,1) = points_courbe_equal(1,1)
  !!     slicecontrolm(theta,1,2) = points_courbe_equal(1,2) + cos(valTheta(1,theta))*disttaillY
  !!     slicecontrolm(theta,1,3) = zslice + sin(valTheta(1,theta))*disttaillY
  !!     slicecontrolm(theta,size(slicecontrolm,2),1) = points_courbe_equal(size(points_courbe_equal,1),1)
  !!     slicecontrolm(theta,size(slicecontrolm,2),2) = points_courbe_equal(size(points_courbe_equal,1),2) + cos(valTheta(size(valTheta,1),theta))*distheadlY
  !!     slicecontrolm(theta,size(slicecontrolm,2),3) = zslice + sin(valTheta(size(valTheta,1),theta))*distheadlY
  !!  enddo
  !!if (l==Ni-1) then
  !!   l = l+1
  !!   write(*,*) "ET VOILA2"
  !!   do theta=1,nbtheta
  !!      slice_courbe(theta,l,1) = slicecontrolm(theta,1,1)
  !!      slice_courbe(theta,l,2) = slicecontrolm(theta,1,2)
  !!      slice_courbe(theta,l,3) = slicecontrolm(theta,1,3)
  !!      !call pointsBezierN1D(valThtmp(theta,:),1._pr*float(Ni)/(Ni+Ns+Nf-2),alpha)
  !!      !call pointsBezierN1D(valThtmp(theta,:),1._pr*float(Ni)/(Ni+Ns+Nf-2-1),alpha)
  !!      call pointsBezierN1D(valThtmp(theta,lpt-itail+1:lp-itail+1)&
  !!           ,1._pr,alpha)
  !!      valTheta(l,theta) = alpha
  !!      !        call pointsBezierN1D(valThtmp(theta,lp-itail+1:lph-itail+1)&
  !!      !             ,tb,alpha)
  !!      !     if (theta==34) write(*,*) "alpha  ",alpha," ",l
  !!      !        valTheta(l,theta) = alpha
  !!   enddo
  !!   do theta=1,nbtheta
  !!      longslicei(theta) = longslicei(theta) +&
  !!           sqrt((slice_courbe(theta,l,1)-slice_courbe(theta,l-1,1))**2 + (slice_courbe(theta,l,2)-slice_courbe(theta,l-1,2))**2 +&
  !!           (slice_courbe(theta,l,3)-slice_courbe(theta,l-1,3))**2) 
  !!   enddo
  !!endif
  !!if (.not.(l==Ni)) write(*,*) "WARNIINNGG  I ",l
  !  longslice = 0._pr
  !  do while ((tb<1._pr).and.(l+1<Ni-1+NsLarge+1))
  !     l = l+1
  !     do theta=1,nbtheta
  !        call pointsBezierN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
  !        slice_courbemidLarge(theta,l,1) = px
  !        slice_courbemidLarge(theta,l,2) = py
  !        slice_courbemidLarge(theta,l,3) = pz
  !     enddo
  !     do theta=1,nbtheta
  !        longslice(theta) = longslice(theta) +&
  !sqrt((slice_courbemidLarge(theta,l,1)-slice_courbemidLarge(theta,l-1,1))**2 + (slice_courbemidLarge(theta,l,2)-&
  !slice_courbemidLarge(theta,l-1,2))**2 + (slice_courbemidLarge(theta,l,3)-slice_courbemidLarge(theta,l-1,3))**2)
  !        if (theta==1) write(*,*) "SLICE  ",l," ",longslice(theta)," ",sqrt((slice_courbemidLarge(theta,l,1)-&
  !slice_courbemidLarge(theta,l-1,1))**2 + (slice_courbemidLarge(theta,l,2)-slice_courbemidLarge(theta,l-1,2))**2 +&
  !(slice_courbemidLarge(theta,l,3)-slice_courbemidLarge(theta,l-1,3))**2)
  !     enddo
  !     tb = tb+dtb
  !  enddo
  !  if (l==Ni-1+NsLarge-1) then
  !          l = l+1
  !  do theta=1,nbtheta
  !     slice_courbemidLarge(theta,l,1) = slicecontrolm(theta,size(slicecontrolm,2),1)
  !     slice_courbemidLarge(theta,l,2) = slicecontrolm(theta,size(slicecontrolm,2),2)
  !     slice_courbemidLarge(theta,l,3) = slicecontrolm(theta,size(slicecontrolm,2),3)
  !  enddo
  !  do theta=1,nbtheta
  !     longslice(theta) = longslice(theta) +&
  !sqrt((slice_courbemidLarge(theta,l,1)-slice_courbemidLarge(theta,l-1,1))**2 + (slice_courbemidLarge(theta,l,2)-&
  !slice_courbemidLarge(theta,l-1,2))**2 + (slice_courbemidLarge(theta,l,3)-slice_courbemidLarge(theta,l-1,3))**2)
  !    write(*,*) "MY slice LONG ::  ",theta," ",longslice(theta)
  !  enddo
  !  endif
  longslice = 0._pr
  !dtb = 0.1_pr/0.2*(Ns-1)
  !tb = dtb
  !do while ((tb<1._pr).and.(l+1<Ni-1+0.2*Ns+1))
  !!  do while ((tb<1._pr).and.(l+1<Ni-1+Ns+1))
  !!     l = l+1
  !!     !     call pointsBezierN(slicecontrolm(1,:,:),tb,px,py)
  !!     !     slice_courbe(1,l,1) = px
  !!     !     slice_courbe(1,l,2) = py
  !!     !     call pointsBezierN(slicecontrolm(2,:,:),tb,px,py)
  !!     !     slice_courbe(2,l,1) = px
  !!     !     slice_courbe(2,l,2) = py
  !!     do theta=1,nbtheta
  !!        !call pointsBezierN1D(valThtmp(theta,:),tb*float(Ns)/(Ni-1+Ns-1+Nf)+float(Ni-1)/(Ni-1+Ns-1+Nf),alpha)
  !!        !call pointsBezierN1D(valThtmp(theta,nint(points_courbe_equal(1,1))-itail+1:nint(points_courbe_equal(Ns,1)-itail+1))&
  !!        !     ,tb,alpha)
  !!        !call pointsBezierN1D(valThtmp(theta,ceiling(points_courbe_equal(1,1))-itail+1:floor(points_courbe_equal(Ns,1)-itail+1))&
  !!        !     ,tb,alpha)
  !!        !call pointsBezierN1D(valThtmp(theta,floor(points_courbe_equal(1,1))-itail+1:ceiling(points_courbe_equal(Ns,1)-itail+1))&
  !!        !     ,tb,alpha)
  !!        call pointsBezierN1D(valThtmp(theta,lp-itail+1:lph-itail+1)&
  !!             ,tb,alpha)
  !!
  !!        !call pointsBezierN1D(valThtmp(theta,:),tb*float(Ns)/(Ni-1+Ns-1+Nf-1)+float(Ni-1)/(Ni-1+Ns-1+Nf-1),alpha)
  !!        !call pointsBezierN1D(valThtmp(theta,:),tb*float(Ns-1)/(Ni-1+Ns-1+Nf-1)+float(Ni)/(Ni-1+Ns-1+Nf-1),alpha)
  !!        !call pointsBezierN1D(valThtmp(theta,:),tb*float(Ns)/(Ni-1+Ns-1+Nf-1)+float(Ni)/(Ni-1+Ns-1+Nf-1),alpha)
  !!        !call pointsBezierN1D(valThtmp(theta,:),tb*float(Ns-1)/(Ni-1+Ns-1+Nf-1)+float(Ni-1)/(Ni-1+Ns-1+Nf-1),alpha)
  !!        valTheta(l,theta) = alpha
  !!        call pointsBezierN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
  !!        slice_courbe(theta,l,1) = px
  !!        slice_courbe(theta,l,2) = py
  !!        slice_courbe(theta,l,3) = pz
  !!     enddo
  !!     write(*,*) "TBslice  ",l," ",tb
  !!     do theta=1,nbtheta
  !!        longslice(theta) = longslice(theta) +&
  !!             sqrt((slice_courbe(theta,l,1)-slice_courbe(theta,l-1,1))**2 + (slice_courbe(theta,l,2)-slice_courbe(theta,l-1,2))**2 +&
  !!             (slice_courbe(theta,l,3)-slice_courbe(theta,l-1,3))**2)
  !!     enddo
  !!
  !!     tb = tb+dtb
  !!  enddo
  do theta=1,nbtheta
     !    l = 1
     tb = 0._pr
     !    do while ((tb<1._pr).and.(l+1<Ns+1)) !size(leftmid,1)))
     !do l=2,Ns-1
     do l=2,Ns-1
        !       l = l+1
        nt = 1
        tbm = 0._pr
        tbp = 1._pr
        tb = tbm 
        bool = 0
        !do while (((abs(sslice(theta))>eepsilon*dx*dx).and.(bool==0)).or.(nt==1))
        do while (((abs(sslice(theta))/(dx*dx)>eepsilon).and.(bool==0)).or.(nt==1))
           tb = (tbm + tbp)*0.5_pr
           nt = nt+1
           call pointsBezierN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
           !if (theta==1+nbtheta/2) write(*,*) "TBslicetheta  ",l," ",tb," ",sslice(theta)," ",tbm," ",tbp
           !if (theta==1) write(*,*) "TBslicetheta  ",l," ",tb," ",sslice(theta)," ",tbm," ",tbp
           if (theta==1) write(*,*) "TBslicem  ",l," ",tb," ",px," ",py," ",pz
           !call splineN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
           sslice(theta) = dotProd3D(points_courbe_equal(l,1),points_courbe_equal(l,2),points_courbe_equal(l,3)&
                ,points_courbe_equal(l+1,1),points_courbe_equal(l+1,2),points_courbe_equal(l+1,3),px,py,pz)

           if (sslice(theta)<0._pr) then
              tbm = tb
           else
              tbp = tb
           endif
           if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
              bool = 1
           endif
        enddo
        call pointsBezierN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
        !if (theta==1+nbtheta/2) write(*,*) "TBslicem  ",l," ",tb," ",px," ",py," ",pz
        if (theta==1) write(*,*) "TBslicem  ",l," ",tb," ",px," ",py," ",pz
        !call splineN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
        slice_courbe(theta,l+Ni-1,1) = px
        slice_courbe(theta,l+Ni-1,2) = py
        slice_courbe(theta,l+Ni-1,3) = pz
     enddo
     nt = 1
     tbm = 0._pr
     tbp = 1._pr
     tb = tbm 
     bool = 0
     !do while (((abs(sslice(theta))>eepsilon*dx*dx).and.(bool==0)).or.(nt==1))
     do while (((abs(sslice(theta))/(dx*dx)>eepsilon).and.(bool==0)).or.(nt==1))
        tb = (tbm + tbp)*0.5_pr
        nt = nt+1
        call pointsBezierN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
        !call splineN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
        sslice(theta) = dotProd3D(points_courbe_equal(Ns,1),points_courbe_equal(Ns,2),points_courbe_equal(Ns,3)&
             ,points_courbe_equal(Ns,1)+dsf,points_courbe_equal(1,2),points_courbe_equal(1,3),px,py,pz)

        if (sslice(theta)<0._pr) then
           tbm = tb
        else
           tbp = tb
        endif
        if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
           bool = 1
        endif
     enddo
     call pointsBezierN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
     !if (theta==1+nbtheta/2) write(*,*) "TBslice  ",Ns," ",tb," ",px," ",py," ",pz
     if (theta==1) write(*,*) "TBslice  ",Ns," ",tb," ",px," ",py," ",pz
     !call splineN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
     slice_courbe(theta,Ns+Ni-1,1) = px
     slice_courbe(theta,Ns+Ni-1,2) = py
     slice_courbe(theta,Ns+Ni-1,3) = pz
  enddo
  !!  lp = l
  !!  tp = tb
  !!  dtb = 0.9_pr/(0.8*Ns-1)
  !!  do while ((tb<1._pr).and.(l+1<Ni-1+0.8*Ns+1))
  !!     l = l+1
  !!     tb = tb -tp+dtb
  !!     do theta=1,nbtheta
  !!        call pointsBezierN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
  !!        slice_courbe(theta,l+lp-1,1) = px
  !!        slice_courbe(theta,l+lp-1,2) = py
  !!        slice_courbe(theta,l+lp-1,3) = pz
  !!     enddo
  !!
  !!     tb = tb+dtb
  !!  enddo
  !!  if (l==Ni-1+Ns-1) then
  !!     l = l+1
  !!     write(*,*) "ET VOILA3"
  !!     do theta=1,nbtheta
  !!        slice_courbe(theta,l,1) = slicecontrolm(theta,size(slicecontrolm,2),1)
  !!        slice_courbe(theta,l,2) = slicecontrolm(theta,size(slicecontrolm,2),2)
  !!        slice_courbe(theta,l,3) = slicecontrolm(theta,size(slicecontrolm,2),3)
  !!        !call pointsBezierN1D(valThtmp(theta,:),1._pr*float(Ns)/(Ni-1+Ns-1+Nf)+float(Ni-1)/(Ni-1+Ns-1+Nf),alpha)
  !!        !call pointsBezierN1D(valThtmp(theta,:),1._pr*float(Ns)/(Ni-1+Ns-1+Nf-1)+float(Ni-1)/(Ni-1+Ns-1+Nf-1),alpha)
  !!        call pointsBezierN1D(valThtmp(theta,lp-itail+1:lph-itail+1)&
  !!             ,tb,alpha)
  !!        valTheta(l,theta) = alpha
  !!     enddo
  !!     do theta=1,nbtheta
  !!        longslice(theta) = longslice(theta) +&
  !!             sqrt((slice_courbe(theta,l,1)-slice_courbe(theta,l-1,1))**2 + (slice_courbe(theta,l,2)-slice_courbe(theta,l-1,2))**2 +&
  !!             (slice_courbe(theta,l,3)-slice_courbe(theta,l-1,3))**2)
  !!        write(*,*) "MY slice LONG ::  ",theta," ",longslice(theta)
  !!     enddo
  !!  endif
  !!  if (.not.(l==Ni-1+Ns)) write(*,*) "WARNIINNGG  mid ",l

  dtb = 1._pr/(Nf-1)
  tb = dtb
  longslicef = 0._pr
  !!  slicecontrolf(1,1,1) = points_courbe_equal(size(points_courbe_equal,1),1)
  !!  slicecontrolf(1,1,2) = points_courbe_equal(size(points_courbe_equal,1),2) + distheadlY
  !!  slicecontrolf(1,size(slicecontrolf,2),1) = rhead
  !!  slicecontrolf(1,size(slicecontrolf,2),2) = points_courbe_equal(1,2) 
  !!  slicecontrolf(1,size(slicecontrolf,2),3) = zslice 
  !!  slicecontrolf(2,1,1) = points_courbe_equal(size(points_courbe_equal,1),1)
  !!  slicecontrolf(2,1,2) = points_courbe_equal(size(points_courbe_equal,1),2) - distheadrY
  !!  slicecontrolf(2,size(slicecontrolf,2),1) = rhead
  !!  slicecontrolf(2,size(slicecontrolf,2),2) = points_courbe_equal(1,2) 
  !!  slicecontrolf(2,size(slicecontrolf,2),3) = zslice 
  do theta=1,nbtheta
     !     slicecontrolf(theta,size(slicecontrolf,2),1) = rhead
     !     slicecontrolf(theta,size(slicecontrolf,2),2) = points_courbe_equal(1,2) 
     !     slicecontrolf(theta,size(slicecontrolf,2),3) = zslice 
     slice_courbe(theta,size(slice_courbe,2),1) = slicecontrolf(theta,size(slicecontrolf,2),1)
     slice_courbe(theta,size(slice_courbe,2),2) = slicecontrolf(theta,size(slicecontrolf,2),2)
     slice_courbe(theta,size(slice_courbe,2),3) = slicecontrolf(theta,size(slicecontrolf,2),3)
  enddo
  !!  do while ((tb<1._pr).and.(l+1<Ni-1+Ns+Nf-1+1)) !size(left_courbe,1)))
  !!     l = l+1
  !!     !     call pointsBezierN(slicecontrolf(1,:,:),tb,px,py)
  !!     !     slice_courbe(1,l,1) = px
  !!     !     slice_courbe(1,l,2) = py
  !!     !     call pointsBezierN(slicecontrolf(2,:,:),tb,px,py)
  !!     !     slice_courbe(2,l,1) = px
  !!     !     slice_courbe(2,l,2) = py
  !!     do theta=1,nbtheta
  !!        !call pointsBezierN1D(valThtmp(theta,:),tb*float(Nf)/(Ni-1+Ns-1+Nf)+float(Ni-1+Ns-1)/(Ni-1+Ns-1+Nf),alpha)
  !!        !call pointsBezierN1D(valThtmp(theta,:),tb*float(Nf)/(Ni-1+Ns-1+Nf-1)+float(Ni-1+Ns-1)/(Ni-1+Ns-1+Nf-1),alpha)
  !!        call pointsBezierN1D(valThtmp(theta,lph-itail+1:ihead-itail+1)&
  !!             ,tb,alpha)
  !!        valTheta(l,theta) = alpha
  !!        call pointsBezierN3D(slicecontrolf(theta,:,:),tb,px,py,pz)
  !!        slice_courbe(theta,l,1) = px
  !!        slice_courbe(theta,l,2) = py
  !!        slice_courbe(theta,l,3) = pz
  !!     enddo
  !!     do theta=1,nbtheta
  !!        longslicef(theta) = longslicef(theta) +&
  !!             sqrt((slice_courbe(theta,l,1)-slice_courbe(theta,l-1,1))**2 + (slice_courbe(theta,l,2)-slice_courbe(theta,l-1,2))**2 +&
  !!             (slice_courbe(theta,l,3)-slice_courbe(theta,l-1,3))**2)
  !!     enddo
  !!     tb = tb+dtb
  !!  enddo
  do theta=1,nbtheta
     tb = 0._pr
     slicecontroltmpf(theta,1:ihead-lph,:) = slicecontrolf(theta,1:ihead-lph,:)
     slicecontroltmpf(theta,size(slicecontroltmpf,2),:) = slicecontrolf(theta,size(slicecontrolf,2),:)
     !if (theta==1) write(*,*) "XX  ",slicecontrolf(theta,:,1)
     !if (theta==1) write(*,*) "XXTMP  ",slicecontroltmpf(theta,:,1)
     !if (theta==1) write(*,*) "YY  ",slicecontrolf(theta,:,2)
     !if (theta==1) write(*,*) "YYTMP  ",slicecontroltmpf(theta,:,2)
     do l=1,Nf-1
        nt = 1
        tbm = 0._pr
        tbp = 1._pr
        tb = tbm 
        bool = 0
        !do while (((abs(sslice(theta))>eepsilon*dx*dx).and.(bool==0)).or.(nt==1))
        do while (((abs(sslice(theta))/(dx*dx)>eepsilon).and.(bool==0)).or.(nt==1))
           tb = (tbm + tbp)*0.5_pr
           nt = nt+1
           !call pointsBezierN3D(slicecontrolf(theta,:,:),tb,px,py,pz)
           call pointsBezierN3D(slicecontroltmpf(theta,:,:),tb,px,py,pz)
           !call pointsBezierN3D(slicecontrolf(theta,:,:),tb*(rhead-0.0005/longratio-points_courbe_equal(Ns,1))&
           !/(rhead-points_courbe_equal(Ns,1)),px,py,pz)
           if (theta==1) write(*,*) "TBsliceF  ",l," ",tb," ",px," ",py," ",pz


           sslice(theta) = dotProd3D(points_courbe_equal(Ns,1)+(l-1)*dsf,points_courbe_equal(1,2),points_courbe_equal(1,3)&
                ,points_courbe_equal(Ns,1)+l*dsf,points_courbe_equal(1,2),points_courbe_equal(1,3),px,py,pz)

           if (sslice(theta)<0._pr) then
              tbm = tb
           else
              tbp = tb
           endif
           if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
              bool = 1
           endif
        enddo
        if (theta==1) write(*,*) "TBsliceF  ",l," ",tb
        !call pointsBezierN3D(slicecontrolf(theta,:,:),tb,px,py,pz)
        call pointsBezierN3D(slicecontroltmpf(theta,:,:),tb,px,py,pz)
        !call pointsBezierN3D(slicecontrolf(theta,:,:),tb*(rhead-0.0005/longratio-points_courbe_equal(Ns,1))&
        !/(rhead-points_courbe_equal(Ns,1)),px,py,pz)
        !call splineN3D(slicecontrolf(theta,:,:),tb,px,py,pz)
        slice_courbe(theta,l+Ni-1+Ns-1,1) = px
        slice_courbe(theta,l+Ni-1+Ns-1,2) = py
        slice_courbe(theta,l+Ni-1+Ns-1,3) = pz
        !if (l>Nf-8) then
        if (l>Nf-5) then
          !call pointsBezierN3D(slice_courbe(theta,Nf-8-1+Ni-1+Ns-1:Nf-2*ceiling(0.1*Nf)+Ni-1+Ns-1,:),0.9_pr,pxx,pyy,pzz)
          !tbb = (px-pxx)*1._pr/(nint((rhead-x0)/dx+1)-pxx)
          pxx = slice_courbe(theta,Nf-7-1+Ni-1+Ns-1,1)
          pyy = slice_courbe(theta,Nf-7-1+Ni-1+Ns-1,2)
          pzz = slice_courbe(theta,Nf-7-1+Ni-1+Ns-1,3)
          tbb = (px-pxx)*1._pr/(rhead-pxx)
          !tp = (slice_courbe(theta,Nf-8+Ni-1+Ns-1,1)-pxx)/(slicecontrolf(theta,size(slicecontrolf,2),1)-pxx)
          tp = (slice_courbe(theta,Nf-4+Ni-1+Ns-1,1)-pxx)/(slicecontrolf(theta,size(slicecontrolf,2),1)-pxx)
          !call poly2(pxx,slice_courbe(theta,Nf-8+Ni-1+Ns-1,1),slicecontrolf(theta,size(slicecontrolf,2),1),tbb,tp,px) 
          !call poly2(pyy,slice_courbe(theta,Nf-8+Ni-1+Ns-1,2),slicecontrolf(theta,size(slicecontrolf,2),2),tbb,tp,py) 
          !call poly2(pzz,slice_courbe(theta,Nf-8+Ni-1+Ns-1,3),slicecontrolf(theta,size(slicecontrolf,2),3),tbb,tp,pz) 
          call poly2(pxx,slice_courbe(theta,Nf-4+Ni-1+Ns-1,1),slicecontrolf(theta,size(slicecontrolf,2),1),tbb,tp,px) 
          call poly2(pyy,slice_courbe(theta,Nf-4+Ni-1+Ns-1,2),slicecontrolf(theta,size(slicecontrolf,2),2),tbb,tp,py) 
          call poly2(pzz,slice_courbe(theta,Nf-4+Ni-1+Ns-1,3),slicecontrolf(theta,size(slicecontrolf,2),3),tbb,tp,pz) 
          slice_courbe(theta,l+Ni-1+Ns-1,1) = px !xx(l)
          slice_courbe(theta,l+Ni-1+Ns-1,2) = py !rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) + cos(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
          slice_courbe(theta,l+Ni-1+Ns-1,3) = pz !rp*sin(valTh(l-itail+1,theta))+zslice + sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
      if (theta==1) write(*,*) "AH      ",floor((points_courbe_equal(Ns,1)-x0)/dx+1)," "&
      ,ceiling((rhead-x0)/dx+1)," ",floor((points_courbe_equal(Ns,1)-x0)/dx+1)
      if (theta==1) write(*,*) "AH      ",nint((points_courbe_equal(Ns,1)-x0)/dx+1)," "&
      ,nint((rhead-x0)/dx+1)," ",nint((points_courbe_equal(Ns,1)-x0)/dx+1)
      if (theta==1) write(*,*) "TBsliceFIN  ",l," ",tbb," ",tbb*tp," ",tp," ",px," ",py," ",pz," ",pxx&
      ," ",slice_courbe(theta,Nf-10+Ni-1+Ns-1,1)," ",slicecontrolf(theta,size(slicecontrolf,2),1)
      if (theta==1) write(*,*) "TBsliceFIN  ",l," ",tbb," ",tbb*tp," ",tp," ",px," ",py," ",pz," ",pyy&
      ," ",slice_courbe(theta,Nf-10+Ni-1+Ns-1,2)," ",slicecontrolf(theta,size(slicecontrolf,2),2)
      if (theta==1) write(*,*) "TBsliceFIN  ",l," ",tbb," ",tbb*tp," ",tp," ",px," ",py," ",pz," ",pzz&
      ," ",slice_courbe(theta,Nf-10+Ni-1+Ns-1,3)," ",slicecontrolf(theta,size(slicecontrolf,2),3)
        endif
     enddo
  enddo
  l = Nf-1+Ns-1+Ni-1
  if (l==Ni-1+Ns-1+Nf-1) then
     l = l+1
     do theta=1,nbtheta
        !     slicecontrolf(theta,size(slicecontrolf,2),1) = rhead
        !     slicecontrolf(theta,size(slicecontrolf,2),2) = points_courbe_equal(1,2) 
        !     slicecontrolf(theta,size(slicecontrolf,2),3) = zslice 
        slice_courbe(theta,l,1) = slicecontrolf(theta,size(slicecontrolf,2),1)
        slice_courbe(theta,l,2) = slicecontrolf(theta,size(slicecontrolf,2),2)
        slice_courbe(theta,l,3) = slicecontrolf(theta,size(slicecontrolf,2),3)
        !call pointsBezierN1D(valThtmp(theta,:),1._pr*float(Nf)/(Ni-1+Ns-1+Nf)+float(Ni-1+Ns-1)/(Ni-1+Ns-1+Nf),alpha)
        !call pointsBezierN1D(valThtmp(theta,:),1._pr*float(Nf)/(Ni-1+Ns-1+Nf-1)+float(Ni-1+Ns-1)/(Ni-1+Ns-1+Nf-1),alpha)
        call pointsBezierN1D(valThtmp(theta,lph-itail+1:ihead-itail+1)&
             ,tb,alpha)
        valTheta(l,theta) = alpha
        longslicef(theta) = longslicef(theta) +&
             sqrt((slice_courbe(theta,l,1)-slice_courbe(theta,l-1,1))**2 + (slice_courbe(theta,l,2)-slice_courbe(theta,l-1,2))**2 +&
             (slice_courbe(theta,l,3)-slice_courbe(theta,l-1,3))**2)
     enddo
     write(*,*) "ET VOILA F "!,l," ",slice_courbe(1,l,1)," ",slice_courbe(1,l,2)," ",slice_courbe(1,l,3)
  endif
  if (.not.(l==Ni-1+Ns-1+Nf)) write(*,*) "WARNIINNGG  F ",l

  !**
  !! Scaling the contour/surface Lagrangian markers
  !**
  do l=1,Ni-1+Ns-1+Nf
    do theta=1,nbtheta
      slice(theta,l,1) = (slice_courbe(theta,l,1)-x0)*longratio+x0
      slice(theta,l,2) = (slice_courbe(theta,l,2)-x0)*longratio+x0
      slice(theta,l,3) = (slice_courbe(theta,l,3)-x0)*longratio+x0
    enddo
  enddo
  do l=1,size(points_courbe_equal,1)
    points_courbe_equal(l,1) = (points_courbe_equal(l,1)-x0)*longratio+x0
    points_courbe_equal(l,2) = (points_courbe_equal(l,2)-x0)*longratio+x0
    points_courbe_equal(l,3) = (points_courbe_equal(l,3)-x0)*longratio+x0
  enddo
  !points_courbe_equal_ref = points_courbe_equal
  !call body_masscenter(points_courbe_equal_ref,xgref,ygref)
  do l=1,size(slicecontrol,2)
    do theta=1,nbtheta
      slicecontrol(theta,l,1) = (slicecontrol(theta,l,1)-x0)*longratio+x0
      slicecontrol(theta,l,2) = (slicecontrol(theta,l,2)-x0)*longratio+x0
      slicecontrol(theta,l,3) = (slicecontrol(theta,l,3)-x0)*longratio+x0
    enddo
  enddo
  !do theta=1,nbtheta
  !  slice(theta,1,1) = (rtail-x0)*longratio+x0
  !  slice(theta,Ni-1+Ns-1+Nf,1) = (rhead-x0)*longratio+x0
  !enddo
  long00 = long00*longratio
  disthead = disthead*longratio
  disttail = disttail*longratio
  dsi = disttail/(Ni-1)
  dsf = disthead/(Nf-1)
  !slice = slice_courbe
  dtb = 1._pr/(Ns-1)
  !  do theta=1,nbtheta
  !     dsslice(theta) = longslice(theta)/(Ns-1)
  !  enddo
  !  dtb = 1._pr/(Ns-1)
  !  longslice = 0._pr
  !  do theta=1,nbtheta
  !  l = 1
  !  tb = 0._pr
  !     slice(theta,1:Ni,1) = slice_courbe(theta,1:Ni,1)
  !     slice(theta,1:Ni,2) = slice_courbe(theta,1:Ni,2)
  !     slice(theta,1:Ni,3) = slice_courbe(theta,1:Ni,3)
  !     slice(theta,Ni-1+Ns:size(slice_courbe_equal,2),1) = slice_courbe(theta,Ni-1+Ns:size(slice_courbe_equal,2),1)
  !     slice(theta,Ni-1+Ns:size(slice_courbe_equal,2),2) = slice_courbe(theta,Ni-1+Ns:size(slice_courbe_equal,2),2)
  !     slice(theta,Ni-1+Ns:size(slice_courbe_equal,2),3) = slice_courbe(theta,Ni-1+Ns:size(slice_courbe_equal,2),3)
  !     slicemid(theta,1:Ns,1) = slice_courbe(theta,Ni:Ni-1+Ns,1)
  !     slicemid(theta,1:Ns,2) = slice_courbe(theta,Ni:Ni-1+Ns,2)
  !     slicemid(theta,1:Ns,3) = slice_courbe(theta,Ni:Ni-1+Ns,3)
  !  do while ((tb<1._pr).and.(l+1<Ns+1)) !size(leftmid,1)))
  !     l = l+1
  !     nt = 1
  !        sslice(theta) = 0._pr
  !        do while ((l-1)*dsslice(theta)-sslice(theta)>0._pr) 
  !           nt = nt+1
  !           sslice(theta) = sslice(theta) +&
  !                sqrt((slicemid(theta,nt,1)-slicemid(theta,nt-1,1))**2 + (slicemid(theta,nt,2)-slicemid(theta,nt-1,2))**2 +&
  !                (slicemid(theta,nt,3)-slicemid(theta,nt-1,3))**2)
  !        enddo
  !        tbm = (nt-2)*dtb
  !        tbp = (nt-1)*dtb
  !        tb = tbm 
  !        s0 = sslice(theta)
  !        sinit = sslice(theta) -&
  !             sqrt((slicemid(theta,nt,1)-slicemid(theta,nt-1,1))**2 + (slicemid(theta,nt,2)-slicemid(theta,nt-1,2))**2 +&
  !             (slicemid(theta,nt,3)-slicemid(theta,nt-1,3))**2)
  !        sslice(theta) = sinit
  !        bool = 0
  !        do while ((abs((l-1)*dsslice(theta)-sslice(theta))>eepsilon).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
  !           tb = (tbm + tbp)*0.5_pr
  !           !           call pointsBezierN(slicecontrolm(theta,:,:),tb,px,py)
  !           call pointsBezierN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
  !           sslice(theta) = sinit + sqrt((px-slicemid(theta,nt-1,1))**2 + (py-slicemid(theta,nt-1,2))**2 +&
  !                (pz-slicemid(theta,nt-1,3))**2)
  !           if ((l-1)*dsslice(theta)-sslice(theta)>0._pr) then
  !              tbm = tb
  !           else
  !              tbp = tb
  !           endif
  !           if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
  !              bool = 1
  !           endif
  !        enddo
  !        !        call pointsBezierN(slicecontrolm(theta,:,:),tb,px,py)
  !        call pointsBezierN1D(valThtmp(theta,:),tb*float(Ns)/(Ni-1+Ns-1+Nf)+float(Ni-1)/(Ni-1+Ns-1+Nf),alpha)
  !        if ((theta>1).and.(l==size(slice,2)/2)) write(*,*) "ValTheta  ",theta," ",l," ",tb," ",alpha*180/PI&
  !             ," ",valTheta(l,theta)*180/PI
  !        valTheta(l,theta) = alpha
  !        call pointsBezierN3D(slicecontrolm(theta,:,:),tb,px,py,pz)
  !        slice_courbe_equal(theta,l+Ni-1,1) = px
  !        slice_courbe_equal(theta,l+Ni-1,2) = py
  !        slice_courbe_equal(theta,l+Ni-1,3) = pz
  !        slice(theta,l+Ni-1,1) = slice_courbe_equal(theta,l+Ni-1,1)
  !        slice(theta,l+Ni-1,2) = slice_courbe_equal(theta,l+Ni-1,2)
  !        slice(theta,l+Ni-1,3) = slice_courbe_equal(theta,l+Ni-1,3)
  !        longslice(1) = longslice(1) +&
  !             sqrt((slice(theta,l+Ni-1,1)-slice(theta,l+Ni-1-1,1))**2 + (slice(theta,l+Ni-1,2)-slice(theta,l+Ni-1-1,2))**2 +&
  !             (slice(theta,l+Ni-1,3)-slice(theta,l+Ni-1-1,3))**2)
  !        if (theta==nbtheta) write(*,*) "SLICEEQUAL  ",l," ",longslice(1)&
  !             ," ",slice(theta,l+Ni-1,1)," ",slice(theta,l+Ni-1,2)," ",slice(theta,l+Ni-1,3)&
  !             ," ",sqrt((slice(theta,l+Ni-1,1)-slice(theta,l+Ni-1-1,1))**2 +&
  !             (slice(theta,l+Ni-1,2)-slice(theta,l+Ni-1-1,2))**2 + (slice(theta,l+Ni-1,3)-slice(theta,l+Ni-1-1,3))**2)," ",dsslice(1)
  !             if (l==2) write(*,*) "SLICEequal2  ",l," ",theta," ",slice(theta,l,2)," ",slice(theta,l,3)
  !             if (l==size(slice,2)/2) write(*,*) "SLICEequal  ",l," ",theta," ",slice(theta,l,2)," ",slice(theta,l,3)
  !     enddo
  !     if (l==Ns-1) then
  !        l = l+1
  !        slice(theta,Ns+Ni-1,1) = slicecontrolm(theta,size(slicecontrolm,2),1)
  !        slice(theta,Ns+Ni-1,2) = slicecontrolm(theta,size(slicecontrolm,2),2)
  !        slice(theta,Ns+Ni-1,3) = slicecontrolm(theta,size(slicecontrolm,2),3)
  !        call pointsBezierN1D(valThtmp(theta,:),1._pr*float(Ns)/(Ni-1+Ns-1+Nf)+float(Ni-1)/(Ni-1+Ns-1+Nf),alpha)
  !        valTheta(l,theta) = alpha
  !        write(*,*) " ET VOILA"
  !     endif
  !     if (.not.(l==Ns)) write(*,*) "WARNIINNGG c est moi  ",l," ",theta," ",tb
  !  enddo


  !  tb = 0._pr
  !  dtb = 1._pr/(Ni+Ns+Nf-2-1)
  do l =1,Ni+Ns+Nf-2
     do theta=1,nbtheta
        !        call pointsBezierN1D(valThtmp(theta,:),tb,alpha)
        !        valTheta(l,theta) = alpha
        if (theta==nbtheta/2) write(*,*) "FINAL THETA  ",l," ",theta," ",valTheta(l,theta)
     enddo
     !     tb = tb + dtb
  enddo
  slicetmp = slice

  !!  distslice(1+Ni,1) = dist(slice(1,1+Ni-1,1),slice(1,1+Ni-1,2),points_courbe_equal(1,1),points_courbe_equal(1,2))
  !!  distslice(1+Ni,2) = dist(slice(1,1+Ni-1,1),slice(1,1+Ni-1,2),points_courbe_equal(2,1),points_courbe_equal(2,2))
  !!  do theta=1,nbtheta
  !!     valDist(1+Ni-1,theta) = dist(slice(theta,1+Ni-1,1),slice(theta,1+Ni-1,2),points_courbe_equal(1,1),points_courbe_equal(1,2))
  !!  enddo
  !!  do l=2,Ns-1
  !!     distslice(l+Ni,1) = dist(slice(1,l+Ni-1,1),slice(1,l+Ni-1,2),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2))
  !!     distslice(l+Ni,2) = dist(slice(1,l+Ni-1,1),slice(1,l+Ni-1,2),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2))
  !!     do theta=1,nbtheta
  !!        valDist(l+Ni-1,theta) = dist(slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2))
  !!     enddo
  !!  enddo
  !!  distslice(Ns+Ni,1) = dist(slice(1,Ns+Ni-1,1),slice(1,Ns+Ni-1,2),points_courbe_equal(Ns-1,1),points_courbe_equal(Ns-1,2))
  !!  distslice(Ns+Ni,2) = dist(slice(1,Ns+Ni-1,1),slice(1,Ns+Ni-1,2),points_courbe_equal(Ns,1),points_courbe_equal(Ns,2))
  !!  do theta=1,nbtheta
  !!     valDist(Ns+Ni-1,theta) =&
  !!dist(slice(theta,Ns+Ni-1,1),slice(theta,Ns+Ni-1,2),points_courbe_equal(Ns,1),points_courbe_equal(Ns,2))
  !!  enddo
  !!
  !!  dsi = disttail/(Ni-1)
  !!  distslice(1,1) = dist(slice(1,1,1),slice(1,1,2),points_courbe_equal(1,1)-(Ni-1)*dsi,points_courbe_equal(1,2))
  !!  distslice(1,2) = dist(slice(1,1,1),slice(1,1,2),points_courbe_equal(1,1)-(Ni-2)*dsi,points_courbe_equal(1,2))
  !!  do theta=1,nbtheta
  !!     valDist(1,theta) = dist(slice(theta,1,1),slice(theta,1,2),points_courbe_equal(1,1)-(Ni-1)*dsi,points_courbe_equal(1,2))
  !!  enddo
  !!  do l=2,Ni-1
  !!     distslice(l,1) = dist(slice(1,l,1),slice(1,l,2),points_courbe_equal(1,1)-(Ni-l+1)*dsi,points_courbe_equal(1,2))
  !!     distslice(l,2) = dist(slice(1,l,1),slice(1,l,2),points_courbe_equal(1,1)-(Ni-l-1)*dsi,points_courbe_equal(1,2))
  !!     do theta=1,nbtheta
  !!        valDist(l,theta) = dist(slice(theta,l,1),slice(theta,l,2),points_courbe_equal(1,1)-(Ni-l)*dsi,points_courbe_equal(1,2))
  !!     enddo
  !!  enddo
  !!  distslice(Ni,1) = dist(slice(1,Ni,1),slice(1,Ni,2),points_courbe_equal(1,1)-dsi,points_courbe_equal(1,2))
  !!  distslice(Ni,2) = dist(slice(1,Ni,1),slice(1,Ni,2),points_courbe_equal(1,1),points_courbe_equal(1,2))
  !!  do theta=1,nbtheta
  !!     valDist(Ni,theta) = dist(slice(theta,Ni,1),slice(theta,Ni,2),points_courbe_equal(1,1),points_courbe_equal(1,2))
  !!  enddo
  !!
  !!  dsf = disthead/(Nf-1)
  !!  distslice(1+Ni+Ns,1) = dist(slice(1,1+Ni-1+Ns-1,1),slice(1,1+Ni-1+Ns-1,2),points_courbe_equal(Ns,1),points_courbe_equal(1,2))
  !!  distslice(1+Ni+Ns,2) = dist(slice(1,1+Ni-1+Ns-1,1),slice(1,1+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+dsf,points_courbe_equal(1,2))
  !!  do theta=1,nbtheta
  !!     valDist(1+Ni-1+Ns-1,theta) =&
  !!dist(slice(theta,1+Ni-1+Ns-1,1),slice(theta,1+Ni-1+Ns-1,2),points_courbe_equal(Ns,1),points_courbe_equal(1,2))
  !!  enddo
  !!  do l=2,Nf-1
  !!     distslice(l+Ni+Ns,1) =&
  !!dist(slice(1,l+Ni-1+Ns-1,1),slice(1,l+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(l-2)*dsf,points_courbe_equal(1,2))
  !!     distslice(l+Ni+Ns,2) =&
  !!dist(slice(1,l+Ni-1+Ns-1,1),slice(1,l+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+l*dsf,points_courbe_equal(1,2))
  !!     do theta=1,nbtheta
  !!        valDist(l+Ni-1+Ns-1,theta) =&
  !!dist(slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(l-1)*dsf,points_courbe_equal(1,2))
  !!     enddo
  !!  enddo
  !!  distslice(Nf+Ni+Ns,1) =&
  !!dist(slice(1,Nf+Ni-1+Ns-1,1),slice(1,Nf+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(Nf-2)*dsf,points_courbe_equal(1,2))
  !!  distslice(Nf+Ni+Ns,2) =&
  !!dist(slice(1,Nf+Ni-1+Ns-1,1),slice(1,Nf+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(Nf-1)*dsf,points_courbe_equal(1,2))
  !!  do theta=1,nbtheta
  !!     valDist(Nf+Ni-1+Ns-1,theta) =&
  !!dist(slice(theta,Nf+Ni-1+Ns-1,1),slice(theta,Nf+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(Nf-1)*dsf,points_courbe_equal(1,2))
  !!  enddo
  !write(*,*) "AVANT  ",slice(1,1,1)," ",slice(1,1,2)," ",slice(1,1,3),"         ",slice(1,size(slice,2),1)&
  !     ," ",slice(1,size(slice,2),2)," ",slice(1,size(slice,2),3)
  !  do theta=1,nbtheta
  !     l=itail+1
  !     rr = 0._pr
  !     bool = 0
  !     do while (((LSrr>0._pr).and.(rr<float(ny))).or.(bool==0))
  !        rp = rr
  !        if (bool==1) then
  !           LSp = LSrr
  !        else
  !           xp = rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
  !           yp = rp*sin(valTh(l-itail+1,theta))+zslice
  !           ip1 = int((xp-x0)/dx)+1
  !           jp1 = int((yp-x0)/dx)+1
  !           ip2 = ip1+1
  !           jp2 = jp1+1
  !           LS1 = rhoSlices(jp1,l,ip1)
  !           LS2 = rhoSlices(jp1,l,ip2)
  !           LS3 = rhoSlices(jp2,l,ip1)
  !           LS4 = rhoSlices(jp2,l,ip2)
  !           x1 = xx(ip1)
  !           y1 = xx(jp1)
  !           x2 = x1 + dx
  !           y2 = y1 + dx
  !           LSp = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
  !           if (theta==1) write(*,*) "checknantail  ",x1," ",y1," ",LSrr," ",LSp
  !        endif
  !
  !        bool = 1
  !        rr=rr+dx
  !        xp = rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
  !        yp = rr*sin(valTh(l-itail+1,theta))+zslice
  !        ip1 = int((xp-x0)/dx)+1
  !        jp1 = int((yp-x0)/dx)+1
  !        ip2 = ip1+1
  !        jp2 = jp1+1
  !        LS1 = rhoSlices(jp1,l,ip1)
  !        LS2 = rhoSlices(jp1,l,ip2)
  !        LS3 = rhoSlices(jp2,l,ip1)
  !        LS4 = rhoSlices(jp2,l,ip2)
  !        x1 = xx(ip1)
  !        y1 = xx(jp1)
  !        x2 = x1 + dx
  !        y2 = y1 + dx
  !        LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
  !     enddo
  !     rr = rp
  !     LSrr = LSp
  !     do while ((LSrr>0._pr).and.(rr<float(ny)))
  !        rp = rr
  !        LSp = LSrr
  !
  !        rr=rr+0.1*dx
  !        xp = rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
  !        yp = rr*sin(valTh(l-itail+1,theta))+zslice
  !        ip1 = int((xp-x0)/dx)+1
  !        jp1 = int((yp-x0)/dx)+1
  !        ip2 = ip1+1
  !        jp2 = jp1+1
  !        LS1 = rhoSlices(jp1,l,ip1)
  !        LS2 = rhoSlices(jp1,l,ip2)
  !        LS3 = rhoSlices(jp2,l,ip1)
  !        LS4 = rhoSlices(jp2,l,ip2)
  !        x1 = xx(ip1)
  !        y1 = xx(jp1)
  !        x2 = x1 + dx
  !        y2 = y1 + dx
  !        LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
  !     enddo
  !     slice(theta,1,1) = rtail
  !     slice(theta,1,2) = 0.1*rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&
  !          0.1*cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
  !     slice(theta,1,3) = 0.1*rp*sin(valTh(l-itail+1,theta))+zslice +&
  !          0.1*sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
  !     if (theta==1) write(*,*) "checknantail  ",slice(theta,1,2)," ",slice(theta,1,3)," ",LSrr," ",LSp
!!!!        slice(theta,1,2) = 0.1*rp*cos(alph)+points_courbe_equal(1,2) +&
!!!!0.1*cos(alph)*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
!!!!        slice(theta,1,3) = 0.1*rp*sin(alph)+zslice +&
!!!!0.1*sin(alph)*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
!!!!        rr = dx
!!!!        do while ((rhoSlices(nint(rr*sin(valTh(l-itail+1,theta)) + zslice),l&
!!!!,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))>0._pr).and.(rr<float(ny)))
!!!!           rr=rr+dx
!!!!        enddo
!!!!!slice(theta,1,2) = points_courbe_equal(1,2) +
!!!!!((slice(theta,2,2)-points_courbe_equal,2)*eepsilon
!!!!!slice(theta,size(slice,2),2) = slice(theta,size(slice,2),2)*eepsilon
!!!!        slice(theta,1,1) = rtail
!!!!        slice(theta,1,2) = 0.1*(rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&
!!!!0.1*cos(valTh(l-itail+1,theta))*rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice),l,nint((rr-dx)*&
!!!!cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))*dy/abs(rhoSlices(nint(rr*sin(valTh(l-itail+1,theta))+zslice),l&
!!!!,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2))) - rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice)&
!!!!,l,nint((rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
!!!!        slice(theta,1,3) = 0.1*(rr-dx)*sin(valTh(l-itail+1,theta))+zslice +&
!!!!0.1*sin(valTh(l-itail+1,theta))*rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice),l,nint((rr-dx)*&
!!!!cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))*dy/abs(rhoSlices(nint(rr*sin(valTh(l-itail+1,theta))+zslice),l&
!!!!,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2))) - rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice)&
!!!!,l,nint((rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
  !     l=ihead-1!+1
  !     rr = 0._pr
  !     bool = 0
  !     do while (((LSrr>0._pr).and.(rr<float(ny))).or.(bool==0))
  !        rp = rr
  !        if (bool==1) then
  !           LSp = LSrr
  !        else
  !           xp = rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
  !           yp = rp*sin(valTh(l-itail+1,theta))+zslice
  !           ip1 = int((xp-x0)/dx)+1
  !           jp1 = int((yp-x0)/dx)+1
  !           ip2 = ip1+1
  !           jp2 = jp1+1
  !           LS1 = rhoSlices(jp1,l,ip1)
  !           LS2 = rhoSlices(jp1,l,ip2)
  !           LS3 = rhoSlices(jp2,l,ip1)
  !           LS4 = rhoSlices(jp2,l,ip2)
  !           x1 = xx(ip1)
  !           y1 = xx(jp1)
  !           x2 = x1 + dx
  !           y2 = y1 + dx
  !           LSp = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
  !        endif
  !
  !        bool = 1
  !        rr=rr+dx
  !        xp = rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
  !        yp = rr*sin(valTh(l-itail+1,theta))+zslice
  !        ip1 = int((xp-x0)/dx)+1
  !        jp1 = int((yp-x0)/dx)+1
  !        ip2 = ip1+1
  !        jp2 = jp1+1
  !        LS1 = rhoSlices(jp1,l,ip1)
  !        LS2 = rhoSlices(jp1,l,ip2)
  !        LS3 = rhoSlices(jp2,l,ip1)
  !        LS4 = rhoSlices(jp2,l,ip2)
  !        x1 = xx(ip1)
  !        y1 = xx(jp1)
  !        x2 = x1 + dx
  !        y2 = y1 + dx
  !        LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
  !     enddo
  !     rr = rp
  !     LSrr = LSp
  !     do while ((LSrr>0._pr).and.(rr<float(ny)))
  !        rp = rr
  !        LSp = LSrr
  !
  !        rr=rr+0.1*dx
  !        xp = rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)
  !        yp = rr*sin(valTh(l-itail+1,theta))+zslice
  !        ip1 = int((xp-x0)/dx)+1
  !        jp1 = int((yp-x0)/dx)+1
  !        ip2 = ip1+1
  !        jp2 = jp1+1
  !        LS1 = rhoSlices(jp1,l,ip1)
  !        LS2 = rhoSlices(jp1,l,ip2)
  !        LS3 = rhoSlices(jp2,l,ip1)
  !        LS4 = rhoSlices(jp2,l,ip2)
  !        x1 = xx(ip1)
  !        y1 = xx(jp1)
  !        x2 = x1 + dx
  !        y2 = y1 + dx
  !        LSrr = interpLS(LS1,x1,y1,LS2,x2,y1,LS3,x1,y2,LS4,x2,y2,xp,yp) 
  !     enddo
  !     slice(theta,size(slice,2),1) = rhead
  !     slice(theta,size(slice,2),2) = 0.1*rp*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&
  !          0.1*cos(valTh(l-itail+1,theta))*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
  !     slice(theta,size(slice,2),3) = 0.1*rp*sin(valTh(l-itail+1,theta))+zslice +&
  !          0.1*sin(valTh(l-itail+1,theta))*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
!!!!        slice(theta,size(slice,2),2) = 0.1*rp*cos(alph)+points_courbe_equal(1,2) +&
!!!!0.1*cos(alph)*LSP*(rr-rp)/abs(LSrr - LSp) !rr+1
!!!!        slice(theta,size(slice,2),3) = 0.1*rp*sin(alph)+zslice +&
!!!!0.1*sin(alph)*LSp*(rr-rp)/abs(LSrr - LSp) !rr+1
!!!!        rr = dx
!!!!        do while ((rhoSlices(nint(rr*sin(valTh(l-itail+1,theta)) + zslice),l&
!!!!,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))>0._pr).and.(rr<float(ny)))
!!!!           rr=rr+dx
!!!!        enddo
!!!!        slice(theta,size(slice,2),1) = rhead
!!!!        slice(theta,size(slice,2),2) = 0.1*(rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2) +&
!!!!0.1*cos(valTh(l-itail+1,theta))*rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice),l,nint((rr-dx)*&
!!!!cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))*dy/abs(rhoSlices(nint(rr*sin(valTh(l-itail+1,theta))+zslice),l&
!!!!,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2))) - rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice)&
!!!!,l,nint((rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
!!!!        slice(theta,size(slice,2),3) = 0.1*(rr-dx)*sin(valTh(l-itail+1,theta))+zslice +&
!!!!0.1*sin(valTh(l-itail+1,theta))*rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice),l,nint((rr-dx)*&
!!!!cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))*dy/abs(rhoSlices(nint(rr*sin(valTh(l-itail+1,theta))+zslice),l&
!!!!,nint(rr*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2))) - rhoSlices(nint((rr-dx)*sin(valTh(l-itail+1,theta))+zslice)&
!!!!,l,nint((rr-dx)*cos(valTh(l-itail+1,theta))+points_courbe_equal(1,2)))) !rr+1
  !  enddo
  !write(*,*) "APRES  ",slice(1,1,1)," ",slice(1,1,2)," ",slice(1,1,3),"         ",slice(1,size(slice,2),1)&
  !     ," ",slice(1,size(slice,2),2)," ",slice(1,size(slice,2),3)
  !write(*,*) "APRES  ",slice(1,2,1)," ",slice(1,2,2)," ",slice(1,2,3),"         ",slice(1,size(slice,2)-1,1)&
  !     ," ",slice(1,size(slice,2)-1,2)," ",slice(1,size(slice,2)-1,3),"  ",sin(valTh(l-itail+1,1))


  !**
  !! Computing the distance and angles between each Lagrangian markers and associated midline points
  !**
  distslice(1+Ni,1) = dist3D(slice(1,1+Ni-1,1),slice(1,1+Ni-1,2),slice(1,1+Ni-1,3)&
       ,points_courbe_equal(1,1),points_courbe_equal(1,2),points_courbe_equal(1,3))
  distslice(1+Ni,2) = dist3D(slice(1,1+Ni-1,1),slice(1,1+Ni-1,2),slice(1,1+Ni-1,3)&
       ,points_courbe_equal(2,1),points_courbe_equal(2,2),points_courbe_equal(1,3))
  do theta=1,nbtheta
     valDist(1+Ni-1,theta) = dist3D(slice(theta,1+Ni-1,1),slice(theta,1+Ni-1,2),slice(theta,1+Ni-1,3)&
          ,points_courbe_equal(1,1),points_courbe_equal(1,2),points_courbe_equal(1,3))
     sinTheta_tab(1+Ni-1,theta) = (slice(theta,1+Ni-1,3)-points_courbe_equal(1,3))/valDist(1+Ni-1,theta) !(sqrt((slice(theta,1+Ni-1,2)-points_courbe_equal(1,2))**2+(slice(theta,1+Ni-1,3)-points_courbe_equal(1,3))**2))
     cosTheta_tab(1+Ni-1,theta) = (slice(theta,1+Ni-1,2)-points_courbe_equal(1,2))/valDist(1+Ni-1,theta) !(sqrt((slice(theta,1+Ni-1,2)-points_courbe_equal(1,2))**2+(slice(theta,1+Ni-1,3)-points_courbe_equal(1,3))**2)) 
  enddo
  do l=2,Ns-1
     distslice(l+Ni,1) = dist3D(slice(1,l+Ni-1,1),slice(1,l+Ni-1,2),slice(1,l+Ni-1,3)&
          ,points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),points_courbe_equal(l-1,3))
     distslice(l+Ni,2) = dist3D(slice(1,l+Ni-1,1),slice(1,l+Ni-1,2),slice(1,l+Ni-1,3)&
          ,points_courbe_equal(l+1,1),points_courbe_equal(l+1,2),points_courbe_equal(l+1,3))
     write(*,*) "DOTPROD  ",l," ", dotProd(points_courbe_equal(l,1),points_courbe_equal(l,2),slice(1,l+Ni-1,1)&
          ,slice(1,l+Ni-1,2),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2))
     do theta=1,nbtheta
        valDist(l+Ni-1,theta) = dist3D(slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2),slice(theta,l+Ni-1,3)&
             ,points_courbe_equal(l,1),points_courbe_equal(l,2),points_courbe_equal(l,3))
        sinTheta_tab(l+Ni-1,theta) = (slice(theta,l+Ni-1,3)-points_courbe_equal(l,3))/valDist(l+Ni-1,theta) !(sqrt((slice(theta,l+Ni-1,2)-points_courbe_equal(l,2))**2+(slice(theta,l+Ni-1,3)-points_courbe_equal(l,3))**2))
        cosTheta_tab(l+Ni-1,theta) = (slice(theta,l+Ni-1,2)-points_courbe_equal(l,2))/valDist(l+Ni-1,theta) !(sqrt((slice(theta,l+Ni-1,2)-points_courbe_equal(l,2))**2+(slice(theta,l+Ni-1,3)-points_courbe_equal(l,3))**2)) 
     enddo
  enddo
  distslice(Ns+Ni,1) = dist3D(slice(1,Ns+Ni-1,1),slice(1,Ns+Ni-1,2),slice(1,Ns+Ni-1,3)&
       ,points_courbe_equal(Ns-1,1),points_courbe_equal(Ns-1,2),points_courbe_equal(Ns-1,3))
  distslice(Ns+Ni,2) = dist3D(slice(1,Ns+Ni-1,1),slice(1,Ns+Ni-1,2),slice(1,Ns+Ni-1,3)&
       ,points_courbe_equal(Ns,1),points_courbe_equal(Ns,2),points_courbe_equal(Ns,3))
  do theta=1,nbtheta
     valDist(Ns+Ni-1,theta) =&
          dist3D(slice(theta,Ns+Ni-1,1),slice(theta,Ns+Ni-1,2),slice(theta,Ns+Ni-1,3)&
          ,points_courbe_equal(Ns,1),points_courbe_equal(Ns,2),points_courbe_equal(Ns,3))
     sinTheta_tab(Ns+Ni-1,theta) = (slice(theta,Ns+Ni-1,3)-points_courbe_equal(Ns,3))/valDist(Ns+Ni-1,theta) !(sqrt((slice(theta,Ns+Ni-1,2)-points_courbe_equal(Ns,2))**2+(slice(theta,Ns+Ni-1,3)-points_courbe_equal(Ns,3))**2))
     cosTheta_tab(Ns+Ni-1,theta) = (slice(theta,Ns+Ni-1,2)-points_courbe_equal(Ns,2))/valDist(Ns+Ni-1,theta) !(sqrt((slice(theta,Ns+Ni-1,2)-points_courbe_equal(Ns,2))**2+(slice(theta,Ns+Ni-1,3)-points_courbe_equal(Ns,3))**2)) 
  enddo

  dsi = disttail/(Ni-1)
  distslice(1,1) = dist3D(slice(1,1,1),slice(1,1,2),slice(1,1,3)&
       ,points_courbe_equal(1,1)-(Ni-1)*dsi,points_courbe_equal(1,2),points_courbe_equal(1,3))
  distslice(1,2) = dist3D(slice(1,1,1),slice(1,1,2),slice(1,1,3)&
       ,points_courbe_equal(1,1)-(Ni-2)*dsi,points_courbe_equal(1,2),points_courbe_equal(1,3))
  do theta=1,nbtheta 
     valDist(1,theta) = dist3D(slice(theta,1,1),slice(theta,1,2),slice(theta,1,3)&
          ,points_courbe_equal(1,1)-(Ni-1)*dsi,points_courbe_equal(1,2),points_courbe_equal(1,3))
     sinTheta_tab(1,theta) = (slice(theta,1,3)-points_courbe_equal(1,3))/valDist(1,theta) !(sqrt((slice(theta,1,2)-points_courbe_equal(1,2))**2+(slice(theta,1,3)-points_courbe_equal(1,3))**2))
     cosTheta_tab(1,theta) = (slice(theta,1,2)-points_courbe_equal(1,2))/valDist(1,theta) !(sqrt((slice(theta,l+Ni-1,2)-points_courbe_equal(l,2))**2+(slice(theta,l+Ni-1,3)-points_courbe_equal(l,3))**2)) 
  enddo
  do l=2,Ni-1
     distslice(l,1) = dist3D(slice(1,l,1),slice(1,l,2),slice(1,l,3)&
          ,points_courbe_equal(1,1)-(Ni-l+1)*dsi,points_courbe_equal(1,2),points_courbe_equal(1,3))
     distslice(l,2) = dist3D(slice(1,l,1),slice(1,l,2),slice(1,l,3)&
          ,points_courbe_equal(1,1)-(Ni-l-1)*dsi,points_courbe_equal(1,2),points_courbe_equal(1,3))
     write(*,*) "DOTPRODi  ",l," ", dotProd(points_courbe_equal(1,1)-(Ni-l)*dsi,points_courbe_equal(1,2),slice(1,l,1)&
          ,slice(1,l,2),points_courbe_equal(1,1)-(Ni-l-1)*dsi,points_courbe_equal(1,2))
     do theta=1,nbtheta
        valDist(l,theta) = dist3D(slice(theta,l,1),slice(theta,l,2),slice(theta,l,3)&
             ,points_courbe_equal(1,1)-(Ni-l)*dsi,points_courbe_equal(1,2),points_courbe_equal(1,3))
        sinTheta_tab(l,theta) = (slice(theta,l,3)-points_courbe_equal(1,3))/valDist(l,theta) 
        cosTheta_tab(l,theta) = (slice(theta,l,2)-points_courbe_equal(1,2))/valDist(l,theta)  
     enddo
  enddo
  distslice(Ni,1) = dist3D(slice(1,Ni,1),slice(1,Ni,2),slice(1,Ni,3)&
       ,points_courbe_equal(1,1)-dsi,points_courbe_equal(1,2),points_courbe_equal(1,3))
  distslice(Ni,2) = dist3D(slice(1,Ni,1),slice(1,Ni,2),slice(1,Ni,3)&
       ,points_courbe_equal(1,1),points_courbe_equal(1,2),points_courbe_equal(1,3))
  do theta=1,nbtheta
     valDist(Ni,theta) = dist3D(slice(theta,Ni,1),slice(theta,Ni,2),slice(theta,Ni,3)&
          ,points_courbe_equal(1,1),points_courbe_equal(1,2),points_courbe_equal(1,3))
     sinTheta_tab(Ni,theta) = (slice(theta,Ni,3)-points_courbe_equal(1,3))/valDist(Ni,theta) 
     cosTheta_tab(Ni,theta) = (slice(theta,Ni,2)-points_courbe_equal(1,2))/valDist(Ni,theta)  
  enddo

  dsf = disthead/(Nf-1)
  distslice(1+Ni+Ns,1) = dist3D(slice(1,1+Ni-1+Ns-1,1),slice(1,1+Ni-1+Ns-1,2),slice(1,1+Ni-1+Ns-1,3)&
       ,points_courbe_equal(Ns,1),points_courbe_equal(1,2),points_courbe_equal(1,3))
  distslice(1+Ni+Ns,2) = dist3D(slice(1,1+Ni-1+Ns-1,1),slice(1,1+Ni-1+Ns-1,2),slice(1,1+Ni-1+Ns-1,3)&
       ,points_courbe_equal(Ns,1)+dsf,points_courbe_equal(1,2),points_courbe_equal(1,3))
  do theta=1,nbtheta
     valDist(1+Ni-1+Ns-1,theta) =&
          dist3D(slice(theta,1+Ni-1+Ns-1,1),slice(theta,1+Ni-1+Ns-1,2),slice(theta,1+Ni-1+Ns-1,3)&
          ,points_courbe_equal(Ns,1),points_courbe_equal(1,2),points_courbe_equal(1,3))
     sinTheta_tab(1+Ni-1+Ns-1,theta) = (slice(theta,1+Ni-1+Ns-1,3)-points_courbe_equal(1,3))/valDist(1+Ni-1+Ns-1,theta) 
     cosTheta_tab(1+Ni-1+Ns-1,theta) = (slice(theta,1+Ni-1+Ns-1,2)-points_courbe_equal(1,2))/valDist(1+Ni-1+Ns-1,theta)  
  enddo
  do l=2,Nf-1
     distslice(l+Ni+Ns,1) =&
          dist3D(slice(1,l+Ni-1+Ns-1,1),slice(1,l+Ni-1+Ns-1,2),slice(1,l+Ni-1+Ns-1,3)&
          ,points_courbe_equal(Ns,1)+(l-2)*dsf,points_courbe_equal(1,2),points_courbe_equal(1,3))
     distslice(l+Ni+Ns,2) =&
          dist3D(slice(1,l+Ni-1+Ns-1,1),slice(1,l+Ni-1+Ns-1,2),slice(1,l+Ni-1+Ns-1,3)&
          ,points_courbe_equal(Ns,1)+l*dsf,points_courbe_equal(1,2),points_courbe_equal(1,3))
     write(*,*) "DOTPRODf  ",l," ", dotProd(points_courbe_equal(Ns,1)+(l-1)*dsf,points_courbe_equal(1,2),slice(1,l+Ni-1+Ns-1,1)&
          ,slice(1,l+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+l*dsf,points_courbe_equal(1,2))
     do theta=1,nbtheta
        valDist(l+Ni-1+Ns-1,theta) =&
             dist3D(slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2),slice(theta,l+Ni-1+Ns-1,3)&
             ,points_courbe_equal(Ns,1)+(l-1)*dsf,points_courbe_equal(1,2),points_courbe_equal(1,3))
        sinTheta_tab(l+Ni-1+Ns-1,theta) = (slice(theta,l+Ni-1+Ns-1,3)-points_courbe_equal(1,3))/valDist(l+Ni-1+Ns-1,theta) 
        cosTheta_tab(l+Ni-1+Ns-1,theta) = (slice(theta,l+Ni-1+Ns-1,2)-points_courbe_equal(1,2))/valDist(l+Ni-1+Ns-1,theta)  
     enddo
  enddo
  distslice(Nf+Ni+Ns,1) =&
       dist3D(slice(1,Nf+Ni-1+Ns-1,1),slice(1,Nf+Ni-1+Ns-1,2),slice(1,Nf+NI-1+Ns-1,3)&
       ,points_courbe_equal(Ns,1)+(Nf-2)*dsf,points_courbe_equal(1,2),points_courbe_equal(1,3))
  distslice(Nf+Ni+Ns,2) =&
       dist3D(slice(1,Nf+Ni-1+Ns-1,1),slice(1,Nf+Ni-1+Ns-1,2),slice(1,Nf+Ni-1+Ns-1,3)&
       ,points_courbe_equal(Ns,1)+(Nf-1)*dsf,points_courbe_equal(1,2),points_courbe_equal(1,3))
  do theta=1,nbtheta
     valDist(Nf+Ni-1+Ns-1,theta) =&
          dist3D(slice(theta,Nf+Ni-1+Ns-1,1),slice(theta,Nf+Ni-1+Ns-1,2),slice(theta,Nf+Ni-1+Ns-1,3)&
          ,points_courbe_equal(Ns,1)+(Nf-1)*dsf,points_courbe_equal(1,2),points_courbe_equal(1,3))
     sinTheta_tab(Nf+Ni-1+Ns-1,theta) = (slice(theta,Nf+Ni-1+Ns-1,3)-points_courbe_equal(1,3))/valDist(Nf+Ni-1+Ns-1,theta) 
     cosTheta_tab(Nf+Ni-1+Ns-1,theta) = (slice(theta,Nf+Ni-1+Ns-1,2)-points_courbe_equal(1,2))/valDist(Nf+Ni-1+Ns-1,theta)  
  enddo
  !  distslice(1+Ni,1) = dist(slice(1,1+Ni-1,2),slice(1,1+Ni-1,3)&
  !       ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !  distslice(1+Ni,2) = dist(slice(1,1+Ni-1,2),slice(1,1+Ni-1,3)&
  !       ,points_courbe_equal(2,2),points_courbe_equal(1,3))
  !  do theta=1,nbtheta
  !     valDist(1+Ni-1,theta) = dist(slice(theta,1+Ni-1,2),slice(theta,1+Ni-1,3)&
  !          ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !  enddo
  !  do l=2,Ns-1
  !     distslice(l+Ni,1) = dist(slice(1,l+Ni-1,2),slice(1,l+Ni-1,3)&
  !          ,points_courbe_equal(l-1,2),points_courbe_equal(l-1,3))
  !     distslice(l+Ni,2) = dist(slice(1,l+Ni-1,2),slice(1,l+Ni-1,3)&
  !          ,points_courbe_equal(l+1,2),points_courbe_equal(l+1,3))
  !     do theta=1,nbtheta
  !        valDist(l+Ni-1,theta) = dist(slice(theta,l+Ni-1,2),slice(theta,l+Ni-1,3)&
  !             ,points_courbe_equal(l,2),points_courbe_equal(l,3))
  !     enddo
  !  enddo
  !  distslice(Ns+Ni,1) = dist(slice(1,Ns+Ni-1,2),slice(1,Ns+Ni-1,3)&
  !       ,points_courbe_equal(Ns-1,2),points_courbe_equal(Ns-1,3))
  !  distslice(Ns+Ni,2) = dist(slice(1,Ns+Ni-1,2),slice(1,Ns+Ni-1,3)&
  !       ,points_courbe_equal(Ns,2),points_courbe_equal(Ns,3))
  !  do theta=1,nbtheta
  !     valDist(Ns+Ni-1,theta) =&
  !          dist(slice(theta,Ns+Ni-1,2),slice(theta,Ns+Ni-1,3)&
  !          ,points_courbe_equal(Ns,2),points_courbe_equal(Ns,3))
  !  enddo

  !    dsi = disttail/(Ni-1)
  !    distslice(1,1) = dist(slice(1,1,2),slice(1,1,3)&
  !  ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !    distslice(1,2) = dist(slice(1,1,2),slice(1,1,3)&
  !  ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !    do theta=1,nbtheta
  !       valDist(1,theta) = dist(slice(theta,1,2),slice(theta,1,3)&
  !  ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !    enddo
  !    do l=2,Ni-1
  !       distslice(l,1) = dist(slice(1,l,2),slice(1,l,3)&
  !  ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !       distslice(l,2) = dist(slice(1,l,2),slice(1,l,3)&
  !  ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !       do theta=1,nbtheta
  !          valDist(l,theta) = dist(slice(theta,l,2),slice(theta,l,3)&
  !  ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !       enddo
  !    enddo
  !    distslice(Ni,1) = dist(slice(1,Ni,2),slice(1,Ni,3)&
  !  ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !    distslice(Ni,2) = dist(slice(1,Ni,2),slice(1,Ni,3)&
  !  ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !    do theta=1,nbtheta
  !       valDist(Ni,theta) = dist(slice(theta,Ni,2),slice(theta,Ni,3)&
  !  ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !    enddo

  !  dsf = disthead/(Nf-1)
  !  distslice(1+Ni+Ns,1) = dist(slice(1,1+Ni-1+Ns-1,2),slice(1,1+Ni-1+Ns-1,3)&
  !       ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !  distslice(1+Ni+Ns,2) = dist(slice(1,1+Ni-1+Ns-1,2),slice(1,1+Ni-1+Ns-1,3)&
  !       ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !  do theta=1,nbtheta
  !     valDist(1+Ni-1+Ns-1,theta) =&
  !          dist(slice(theta,1+Ni-1+Ns-1,2),slice(theta,1+Ni-1+Ns-1,3)&
  !          ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !  enddo
  !  do l=2,Nf-1
  !     distslice(l+Ni+Ns,1) =&
  !          dist(slice(1,l+Ni-1+Ns-1,2),slice(1,l+Ni-1+Ns-1,3)&
  !          ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !     distslice(l+Ni+Ns,2) =&
  !          dist(slice(1,l+Ni-1+Ns-1,2),slice(1,l+Ni-1+Ns-1,3)&
  !          ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !     do theta=1,nbtheta
  !        valDist(l+Ni-1+Ns-1,theta) =&
  !             dist(slice(theta,l+Ni-1+Ns-1,2),slice(theta,l+Ni-1+Ns-1,3)&
  !             ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !     enddo
  !  enddo
  !  distslice(Nf+Ni+Ns,1) =&
  !       dist(slice(1,Nf+Ni-1+Ns-1,2),slice(1,Nf+NI-1+Ns-1,3)&
  !       ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !  distslice(Nf+Ni+Ns,2) =&
  !       dist(slice(1,Nf+Ni-1+Ns-1,2),slice(1,Nf+Ni-1+Ns-1,3)&
  !       ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !  do theta=1,nbtheta
  !     valDist(Nf+Ni-1+Ns-1,theta) =&
  !          dist(slice(theta,Nf+Ni-1+Ns-1,2),slice(theta,Nf+Ni-1+Ns-1,3)&
  !          ,points_courbe_equal(1,2),points_courbe_equal(1,3))
  !  enddo
  !!  do l=2,Ns-1
  !!     call intersection(points_courbe_equal(l-1,1),points_courbe_equal(l+1,1),points_courbe_equal(l-1,2),&
  !!          points_courbe_equal(l+1,2),points_courbe_equal(l,1),points_courbe_equal(l,2),distslice(l+Ni,1),distslice(l+Ni,2),&
  !!          distslice(Ns+Ni+Nf+l+Ni,1),distslice(Ns+Ni+Nf+l+Ni,2),xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
  !!     sinPhi = (xTheta(1)-points_courbe_equal(l,1))/(&
  !!          sqrt((xTheta(1)-points_courbe_equal(l,1))**2+(yTheta(1)-points_courbe_equal(l,2))**2))
  !!     cosPhi = (yTheta(1)-points_courbe_equal(l,2))/(&
  !!          sqrt((xTheta(1)-points_courbe_equal(l,1))**2+(yTheta(1)-points_courbe_equal(l,2))**2)) 
  !!
  !!     do theta=1,nbtheta
  !!        !             slice(theta,l+Ni-1,1) = points_courbe_equal(l,1) + valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhi
  !!        !             slice(theta,l+Ni-1,2) = points_courbe_equal(l,2) + valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhi
  !!        !             slice(theta,l+Ni-1,3) = zslice + valDist(l+Ni-1,theta)*sin(valTheta(l+Ni-1,theta))
  !!        !             slice(theta,l+Ni-1,1) = points_courbe_equal(l,1) 
  !!        !             slice(theta,l+Ni-1,2) = points_courbe_equal(l,2) + valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))
  !!        !             slice(theta,l+Ni-1,3) = zslice + valDist(l+Ni-1,theta)*sin(valTheta(l+Ni-1,theta))
  !!
  !!        !          sinPhi = (slicetmp(theta,l+Ni-1,3)-points_courbe_equal(l,3))/(&
  !!        !  sqrt((slicetmp(theta,l+Ni-1,2)-points_courbe_equal(l,2))**2+(slicetmp(theta,l+Ni-1,3)-points_courbe_equal(l,3))**2))
  !!        !          cosPhi = (slicetmp(theta,l+Ni-1,2)-points_courbe_equal(l,2))/(&
  !!        !  sqrt((slicetmp(theta,l+Ni-1,2)-points_courbe_equal(l,2))**2+(slicetmp(theta,l+Ni-1,3)-points_courbe_equal(l,3))**2)) 
  !!        slice(theta,l+Ni-1,1) = points_courbe_equal(l,1) 
  !!        slice(theta,l+Ni-1,2) = points_courbe_equal(l,2) + valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)
  !!        slice(theta,l+Ni-1,3) = zslice + valDist(l+Ni-1,theta)*sinTheta_tab(l+Ni-1,theta)
  !!     enddo
  !!     if (errorl==1) write(*,*) "WARNING : ",errorl," ",l
  !!  enddo


  !**
  !! Now, we can write in output the initial shape of the zebrafish (Lagrangian markers / midline)
  !**

  !! TEST
  !open(unit=91,file='results/theta00.txt',status='unknown')
  !open(unit=90,file='results/controltheta00.txt',status='unknown')
  !open(unit=88,file='results/coupeLarge00.txt',status='unknown')
  !open(unit=89,file='results/coupetmp00.txt',status='unknown')
  !open(unit=87,file='results/coupe00.txt',status='unknown')
  !open(unit=84,file='results/right00.txt',status='unknown')
  !open(unit=83,file='results/left00.txt',status='unknown')
  !open(unit=86,file='results/controlright00.txt',status='unknown')
  !open(unit=85,file='results/controlleft00.txt',status='unknown')
  !open(unit=79,file='results/skelet00.vtk',status='unknown')
  !open(unit=81,file='results/skelett00.txt',status='unknown')
  !open(unit=82,file='results/skeletteq00.txt',status='unknown')
  !open(unit=91,file='results_secondTEST6/theta00.txt',status='unknown')
  !open(unit=90,file='results_secondTEST6/controltheta00.txt',status='unknown')
  !open(unit=88,file='results_secondTEST6/coupeLarge00.txt',status='unknown')
  !open(unit=89,file='results_secondTEST6/coupetmp00.txt',status='unknown')
  !open(unit=87,file='results_secondTEST6/coupe00.txt',status='unknown')
  !open(unit=84,file='results_secondTEST6/right00.txt',status='unknown')
  !open(unit=83,file='results_secondTEST6/left00.txt',status='unknown')
  !open(unit=86,file='results_secondTEST6/controlright00.txt',status='unknown')
  !open(unit=85,file='results_secondTEST6/controlleft00.txt',status='unknown')
  !open(unit=79,file='results_secondTEST6/skelet00.vtk',status='unknown')
  !open(unit=81,file='results_secondTEST6/skelett00.txt',status='unknown')
  !open(unit=82,file='results_secondTEST6/skeletteq00.txt',status='unknown')
  open(unit=91,file='results_test/theta00.txt',status='unknown')
  open(unit=90,file='results_test/controltheta00.txt',status='unknown')
  open(unit=88,file='results_test/coupeLarge00.txt',status='unknown')
  open(unit=89,file='results_test/coupetmp00.txt',status='unknown')
  open(unit=87,file='results_test/coupe00.txt',status='unknown')
  open(unit=84,file='results_test/right00.txt',status='unknown')
  open(unit=83,file='results_test/left00.txt',status='unknown')
  open(unit=86,file='results_test/controlright00.txt',status='unknown')
  open(unit=85,file='results_test/controlleft00.txt',status='unknown')
  open(unit=79,file='results_test/skelet00.vtk',status='unknown')
  open(unit=81,file='results_test/skelett00.txt',status='unknown')
  open(unit=82,file='results_test/skeletteq00.txt',status='unknown')
  write(79,'(1A26)') '# vtk DataFile Version 2.0'
  write(79,'(a)') 'rho'
  write(79,'(a)') 'ASCII'
  write(79,'(a)') 'DATASET STRUCTURED_POINTS'
  write(79,'(a,I4,I4,I4)') 'DIMENSIONS ', nx,ny,1
  write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
  !write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,1. 
  write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 1.,1.,1. 
  write(79,'(a,I6)') 'POINT_DATA          ' , nx*ny 
  write(79,'(a)') 'SCALARS values double'
  write(79,'(a)') 'LOOKUP_TABLE default'
  !! FILM
  !open(unit=78,file='results/surf00.vts',status='unknown')
  !open(unit=78,file='results_secondTEST6/surf00.vts',status='unknown')
  open(unit=78,file='results_test/surf00.vts',status='unknown')
  !open(unit=92,file='results_secondTEST6/surf00.dat',status='unknown')
  open(unit=92,file='results_test/surf00.dat',status='unknown')
  write(78,'(a)') "<?xml version=""1.0""?>"
  write(78,'(a)') "<VTKFile type=""StructuredGrid"" version=""0.1"" byte_order=""LittleEndian"" compressor=&
       ""vtkZLibDataCompressor"">"
  write(78,'(a,I3,a,I3,a)') "<StructuredGrid WholeExtent=""0 ",nbtheta," 0 ",Ni-1+Ns+Nf-1-1," 0 0"">"
  write(78,'(a,I3,a,I3,a)') "<Piece Extent=""0 ",nbtheta," 0 ",Ni-1+Ns+Nf-1-1," 0 0"">"
  !write(78,'(a,I3,a,I3,a)') "<StructuredGrid WholeExtent=""0 ",nbtheta-1," 0 ",Ns-1-1-1," 0 0"">"
  !write(78,'(a,I3,a,I3,a)') "<Piece Extent=""0 ",nbtheta-1," 0 ",Ns-1-1-1," 0 0"">"
  write(78,'(a)') "<PointData >"
  write(78,'(a)') "</PointData>"
  write(78,'(a)') "<CellData>"
  write(78,'(a)') "</CellData>"
  write(78,'(a)') "<Points>"
  write(78,'(a)') "<DataArray NumberOfComponents=""3"" type=""Float64"" format=""ascii"" >"  
  do l=1,Ni+Ns+Nf-2
     !do l=Ni+1,Ni-1+Ns-1
     do theta=1,nbtheta !indextheta(l)
        write(78,*) slice(theta,l,1)," ",slice(theta,l,2)," ",slice(theta,l,3)
        write(92,*) slice(theta,l,1)," ",slice(theta,l,2)," ",slice(theta,l,3)
        !write(78,*) slicetmp(theta,l,1)," ",slicetmp(theta,l,2)," ",slicetmp(theta,l,3)
        if (theta==88) write(*,*) "cmptheta75  ",l," ",dist3D(slice(theta,l,1),slice(theta,l,2),slice(theta,l,3)&
             ,slicetmp(theta,l,1),slicetmp(theta,l,2),slicetmp(theta,l,3))
        if (l==Ni+1+10) write(*,*) "cmpslice10  ",theta," ", dist3D(slice(theta,l,1),slice(theta,l,2),slice(theta,l,3)&
             ,slicetmp(theta,l,1),slicetmp(theta,l,2),slicetmp(theta,l,3))
        if (l==Ni+1+5) write(*,*) "cmpslice5  ",theta," ", dist3D(slice(theta,l,1),slice(theta,l,2),slice(theta,l,3)&
             ,slicetmp(theta,l,1),slicetmp(theta,l,2),slicetmp(theta,l,3))
        if (l==Ni+1+2) write(*,*) "cmpslice2  ",theta," ", dist3D(slice(theta,l,1),slice(theta,l,2),slice(theta,l,3)&
             ,slicetmp(theta,l,1),slicetmp(theta,l,2),slicetmp(theta,l,3))
        if (l==Ni+1) write(*,*) "cmpsliceNI  ",theta," ", dist3D(slice(theta,l,1),slice(theta,l,2),slice(theta,l,3)&
             ,slicetmp(theta,l,1),slicetmp(theta,l,2),slicetmp(theta,l,3))
     enddo
        write(78,*) slice(1,l,1)," ",slice(1,l,2)," ",slice(1,l,3)
        write(92,*) slice(1,l,1)," ",slice(1,l,2)," ",slice(1,l,3)
  enddo
  write(78,'(a)') "</DataArray>"
  write(78,'(a)') "</Points>"
  write(78,'(a)') "</Piece>"
  write(78,'(a)') "</StructuredGrid>"
  write(78,'(a)') "</VTKFile>"
  close(78)    
  close(92)    

  do k=1,ny
     do j=1,nx
        !write(79,*) rhoSlices2(j,k)*(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k)
        write(79,*) rhoSlices2(j,k)
     enddo
  enddo
  !  do l=1,Ns
  !     write(81,*) points_courbe(l,1)," ",points_courbe(l,2)
  !     write(82,*) points_courbe_equal(l,1)," ",points_courbe_equal(l,2)
  !  enddo
  !  do l=1,size(slice,2) !size(left_courbe_equal,1)
  !     write(83,*) slice(1,l,1)," ",slice(1,l,2)
  !  enddo
  !  do l=1,size(slice,2) !size(right_courbe_equal,1)
  !     write(84,*) slice(2,l,1)," ",slice(2,l,2)
  !  enddo
  !  write(*,*) " long ==   ",nl," ",long
  do l=1,Ns
     write(81,*) points_courbe(l,1)," ",points_courbe(l,2)," ",points_courbe(l,3)
     write(82,*) points_courbe_equal(l,1)," ",points_courbe_equal(l,2)," ",points_courbe_equal(l,3)
  enddo
  !  do l=1,size(slice,2) !size(left_courbe_equal,1)
  !     do theta=1,nbtheta
  !        write(83,*) slice(theta,l,1)," ",slice(theta,l,2)," ",slice(theta,l,3)
  !     enddo
  !  enddo
  do l=1,size(slice,2) !size(left_courbe_equal,1)
     !     do theta=1,nbtheta
     theta = 1
     write(83,*) slice(theta,l,1)," ",slice(theta,l,2)," ",slice(theta,l,3)
     theta = 1+nbtheta/2
     write(84,*) slice(theta,l,1)," ",slice(theta,l,2)," ",slice(theta,l,3)
     !     enddo
  enddo
  !  do l=1,size(slicecontroli,2) 
  !      theta = 1
  !        write(85,*) slicecontroli(theta,l,1)," ",slicecontroli(theta,l,2)," ",slicecontroli(theta,l,3)
  !      theta = 1+nbtheta/2
  !        write(86,*) slicecontroli(theta,l,1)," ",slicecontroli(theta,l,2)," ",slicecontroli(theta,l,3)
  !  enddo
  !  do l=1,size(slicecontrolm,2) 
  !      theta = 1
  !        write(85,*) slicecontrolm(theta,l,1)," ",slicecontrolm(theta,l,2)," ",slicecontrolm(theta,l,3)
  !      theta = 1+nbtheta/2
  !        write(86,*) slicecontrolm(theta,l,1)," ",slicecontrolm(theta,l,2)," ",slicecontrolm(theta,l,3)
  !  enddo
  !  do l=1,size(slicecontrolf,2) 
  !      theta = 1
  !        write(85,*) slicecontrolf(theta,l,1)," ",slicecontrolf(theta,l,2)," ",slicecontrolf(theta,l,3)
  !      theta = 1+nbtheta/2
  !        write(86,*) slicecontrolf(theta,l,1)," ",slicecontrolf(theta,l,2)," ",slicecontrolf(theta,l,3)
  !  enddo
  do l=1,size(slicecontrol,2) 
     theta = 1
     write(85,*) slicecontrol(theta,l,1)," ",slicecontrol(theta,l,2)," ",slicecontrol(theta,l,3)
     theta = 1+nbtheta/2
     write(86,*) slicecontrol(theta,l,1)," ",slicecontrol(theta,l,2)," ",slicecontrol(theta,l,3)
  enddo
  l = 120 !size(slicecontrol,2)/2
  do theta = 1,nbtheta
     write(87,*) slicecontrol(theta,l,2)," ",slicecontrol(theta,l,3)
     !     write(87,*) points_courbe_equal(l,2)," ",points_courbe_equal(l,3)
     !write(89,*) slicecontroltmp(theta,l,2)," ",slicecontroltmp(theta,l,3)
  enddo
  l = 120 !size(slice,2)/2 
  write(89,*) points_courbe_equal(l,2)," ",points_courbe_equal(l,3)
  do theta = 1,nbtheta
     write(89,*) slice(theta,l,2)," ",slice(theta,l,3)
  enddo
  l = 120 !size(slicecontrolLarge,2)/2
  write(88,*) points_courbe_equal(l,2)," ",points_courbe_equal(l,3)
  do theta = 1,nbthetaLarge
     !write(88,*) slicecontrolLarge(theta,l-itail+1,2)," ",slicecontrolLarge(theta,l-itail+1,3)
     !write(88,*) slicecontroltmp(theta,l,2)," ",slicecontroltmp(theta,l,3)
     write(88,*) slicetmp(theta,l,2)," ",slicetmp(theta,l,3)
  enddo
  theta = 34
  do l = itail,ihead
     write(90,*) l," ",valTh(l-itail+1,theta)
  enddo
  do l=1,size(valTheta,1)
     if (l<=size(points_courbe_equal,1)) then
        write(91,*) points_courbe_equal(l,1)," ",valTheta(l,theta)
     else
        write(91,*) points_courbe_equal(Ns,1)+(l-Ns)*dsf," ",valTheta(l,theta)
     endif
  enddo
  close(90)
  close(91)
  close(89)
  close(88)
  close(87)
  close(79)    
  close(85)
  close(86)
  close(81)
  close(82)
  close(83)
  close(84)
  deallocate(midline,midlinebis)
  deallocate(points_control)
  deallocate(slicecontrolm,slicecontrolf,slicecontroli,slicecontroltmpf,slicecontroltmpi)
  deallocate(slicecontrolLarge)
  deallocate(slicecontrol,slicecontroltmp)
  deallocate(valTh,valThLarge,valThtmp)

  deallocate(rhoSlices2,rhoSlices,gradPhi,xx,yy,zz,Nseed,skel,skel2,skel3,longTh)
  
  !! FILM
  N =  200 
  nx = 200 
  ny = 200 
  nz = 200
  !dx = 0.001_pr*0.0301
  !dy = 0.001_pr*0.0301
  !dz = 0.001_pr*0.0301
  dx = 0.001_pr*0.0256
  dy = 0.001_pr*0.0256
  dz = 0.001_pr*0.0256

  allocate(rhoSlices2(nx,ny),gradPhi(nx,ny))
  allocate(xx(nx),yy(ny),zz(nz))
  allocate(Nseed(nx,ny),skel(nx,ny),skel2(nx,ny),skel3(nx,ny),longTh(nx))

  do i=1,nx
     xx(i) = x0 + (float(i)-1)*dx
  enddo
  do j=1,ny
     yy(j) = x0 + (float(j)-1)*dy
  enddo
  do k=1,nz
     zz(k) = x0 + (float(k)-1)*dz
  enddo

  !! FILM
  !open(unit=80,file='skeleton.txt',status='unknown')
  !open(unit=80,file='results_secondTEST6/skeleton.txt',status='unknown')
  open(unit=80,file='results_test/skeleton.txt',status='unknown')
  !open(unit=80,file='results/skeleton.txt',status='unknown')
  write(80,*) kt,"    ",nl," ",long3," ",long2," ",long

  !**
  !! Loop over each experimental image to construct the deformed zebrafish shape (Lagrangian markers) based on the pre-built shape
  !**
  t = 0
  iter = 0
  idisplay = 10
  iUpdateDist = 5
  do kt = 1,580 !750 !710 !670 !625 !685 !775 !620 !845 !795 !725 !650 !770 !795 !785 !520 !705!720 !815 !780 !720 !760 !630 !550 !770 !700 !810 !570 !800 !696 !566!671!480!301!671!566!671!521!566 !,idisplay !755,idisplay !566,idisplay !566,615,idisplay
     write(*,*)"time iteration : ",kt
     !! FILM
     !open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/mask200/Image_"//str(kt+picNum-1)//".txt",&
     !     status='unknown')
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/secondTEST6/IMAGES2/Image_"//str(kt+picNum-1)//".dat",&
     !!open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISH_G3/IMAGES2/Image_"//str(kt+picNum-1)//".dat",&
     open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie5_18/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISH_5dpf5/IMAGES2/Image_"//str(kt+picNum-1)//".dat",&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISH_5dpf3/IMAGES2/Image_"//str(kt+picNum-1)//".dat",&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie2_13/IMAGES2/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie2_19/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie8_P19_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie8_P19_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie8_P65_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie7_P19_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie7_P35_2/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie9_DMSO_p3_2/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie8_P27_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie10_DMSO_p4P1_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie5_12/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie13_DMSO_p1P1_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie13_CPO100_p12P2_2/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie10_CPO_p12P1_2/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie10_CPO_p10P2_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie11_P13_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie12_P43_3/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie12_P33_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie11_P31_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie12_P101_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie12_P81_2/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie10_CPO_p5P1_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie10_CPO_p5P2_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie13_CPO150_p8P1_2/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie10_CPO_p7P3_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie7_P47_1/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/myFISHserie8_P17_2/IMAGES4/Image_"&
     !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/test/IMAGES2/Image_"&
     //str(kt+picNum-1)//".dat",&
          status='unknown')
  !**
  !! Computation of the level-set 
  !**
     do k=1,nx
        do j=1,ny
           read(78,*) rhoSlices2(k,j)
           !read(78,*) rhoSlices2(nx-k+1,j)
        enddo
     enddo
     close(78)
     rhoSlices2 = rhoSlices2 - 0.5_pr*(maxval(rhoSlices2) + minval(rhoSlices2))
     !rhoSlices2 = (2*rhoSlices2-minval(rhoSlices2)-maxval(rhoSlices2))/(maxval(rhoSlices2)-minval(rhoSlices2))
     !dx = 0.001_pr*0.0301*1E6
     !dy = 0.001_pr*0.0301*1E6
     !dz = 0.001_pr*0.0301*1E6
     dx = 0.001_pr*0.0256*1E6
     dy = 0.001_pr*0.0256*1E6
     dz = 0.001_pr*0.0256*1E6
     call updateDistance(rhoSlices2,gradPhi)
     !rhoSlices2 = (2*rhoSlices2-minval(rhoSlices2)-maxval(rhoSlices2))/(maxval(rhoSlices2)-minval(rhoSlices2))
     !rhoSlices2 = rhoSlices2/maxval(rhoSlices2)
     !dx = 0.001_pr*0.0301
     !dy = 0.001_pr*0.0301
     !dz = 0.001_pr*0.0301
     dx = 0.001_pr*0.0256
     dy = 0.001_pr*0.0256
     dz = 0.001_pr*0.0256
     do i=1,nx
        xx(i) = x0 + (float(i)-1)*dx
     enddo
     do j=1,ny
        yy(j) = x0 + (float(j)-1)*dy
     enddo
     do k=1,nz
        zz(k) = x0 + (float(k)-1)*dz
     enddo

  !**
  !! Computation of the midline (skel: skeleton based one the level-set, skel2: gradient of the level-set)
  !**
     dir1 = 0
     dir2 = 0
     dir3 = 0
     dir4 = 0
     Nseed = 0
     skel = 0
     skel2 = 0
     do i=2,nx-1
        do j=2,ny-1
           !! FILM
           !if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j))) dir1(i,j) = 1
           !if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j-1))) dir2(i,j) = 1
           !if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j-1))) dir3(i,j) = 1
           !if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j-1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j+1))) dir4(i,j) = 1
           !Nseed(i,j) = dir1(i,j) + dir2(i,j) + dir3(i,j) + dir4(i,j)
           !if ((Nseed(i,j)>=2).and.(rhoSlices2(i,j)>0)) then 
           !   skel(i,j) = 1
           !endif
           !if ((gradPhi(j,i)<0.57).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!!           if ((gradPhi(j,i)<0.58).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!!           if ((gradPhi(j,i)<0.62).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!!           if ((gradPhi(j,i)<0.66).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((gradPhi(j,i)<0.62).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           !if ((gradPhi(j,i)<0.62).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((gradPhi(j,i)<0.95).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           !if ((gradPhi(j,i)<0.97).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           !if (rhoSlices2(j,i)>0._pr) skel(j,i) = 1
!
           !if ((gradPhi(j,i)<0.64).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
           !if ((gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!           !!5dpf5
!           !if ((gradPhi(j,i)<0.66).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           !if ((gradPhi(j,i)<0.97).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           !serie2_13
!           !if ((gradPhi(j,i)<0.66).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           !if ((gradPhi(j,i)<0.96).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           !if ((gradPhi(j,i)<0.68).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((gradPhi(j,i)<0.62).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           !if ((gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!!           !!serie2_19
!!           if ((gradPhi(j,i)<0.58).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!!           if (kt==274) then
!!             if((gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr))skel2(j,i)=1
!!           else
!!             if((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr))skel2(j,i)=1
!!           endif
!            if ((kt==537).or.(kt==538).or.(kt==568)) then
!                   if ((gradPhi(j,i)<0.7).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           else
!                   if ((gradPhi(j,i)<0.55).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           endif
!           if ((gradPhi(j,i)<0.96).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!           !serie_5_18 (bis -> test)
!           if (kt==406) then
!             if ((gradPhi(j,i)<0.68).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           else
!             if ((gradPhi(j,i)<0.66).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           endif
!           if ((gradPhi(j,i)<0.96).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1


           !!serie2_5dpf3
           !if ((gradPhi(j,i)<0.62).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
           !if ((gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!!           !!serie8_P19_1
!!           if ((gradPhi(j,i)<0.62).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!!           if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!!           if ((kt==217).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!!           if ((kt==471).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!!           if ((kt==445).and.(gradPhi(j,i)<0.8).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!            if ((gradPhi(j,i)<0.53).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!            if ((kt>240).and.(gradPhi(j,i)<0.54).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!            if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1


!!           !!serie5_18
!           if ((gradPhi(j,i)<0.66).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!!           if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!!           if ((kt==558).or.(kt==287)) then
!!             if((gradPhi(j,i)<0.55).and.(rhoSlices2(j,i)>0._pr))skel(j,i)=1
!!           else
!!             if((gradPhi(j,i)<0.68).and.(rhoSlices2(j,i)>0._pr))skel(j,i)=1
!!           endif
!           if((gradPhi(j,i)<0.96).and.(rhoSlices2(j,i)>0._pr))skel2(j,i)=1

!           !serie5_17
!           if ((gradPhi(j,i)<0.55).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

           !serie5_18_angle2
           if ((gradPhi(j,i)<0.62).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
           if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
           if ((kt==60).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
           if ((kt==561).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
           if ((kt==365).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
           if ((kt==162).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
           if ((kt==247).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
           if ((kt==303).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
           if ((kt==304).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
           if ((kt==501).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!           !serie2_19_angle2
!           if ((gradPhi(j,i)<0.62).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==511).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==272).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==461).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==532).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==503).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==505).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!           !serie5_17_angle2
!           if ((gradPhi(j,i)<0.55).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==91).and.(gradPhi(j,i)<0.995).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==92).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==94).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==124).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==125).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==217).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==219).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==366).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==373).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==383).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==384).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==582).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==613).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==694).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==696).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==692).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!           !serie8_P19_1_angle2
!           if ((gradPhi(j,i)<0.55).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==355).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==362).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==565).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!           !serie8_P39_2_angle2
!           if ((gradPhi(j,i)<0.62).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((gradPhi(j,i)<0.95).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==40).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==43).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==49).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==59).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==62).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==599).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!           !serie7_P19_1_angle2
!           if ((gradPhi(j,i)<0.52).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt>200).and.(gradPhi(j,i)<0.55).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((kt==742).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==746).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==63).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==121).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==804).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==382).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!           !serie8_P65_1_angle2
!           if ((gradPhi(j,i)<0.52).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((kt>100).and.(gradPhi(j,i)<0.55).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((kt>650).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==417).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==514).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!           !serie7_P15_2_angle2
!           if ((gradPhi(j,i)<0.62).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==153).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==276).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==326).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!           !serie7_P35_2_angle2
!           if ((gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==452).and.(gradPhi(j,i)<0.62).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!           if ((kt==23).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==24).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==26).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==34).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==179).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==215).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==222).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!           if ((kt==331).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!            !serie9_p3_2_angle2
!            if ((gradPhi(j,i)<0.58).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!            if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt>=333).and.(kt<=342).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==369).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==661).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==749).and.(gradPhi(j,i)<0.995).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==752).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==19).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==92).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==137).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==186).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==244).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==394).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==464).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==483).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==619).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==668).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==686).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==727).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==123).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==124).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==619).and.(gradPhi(j,i)<0.995).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==22).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!            if ((kt==24).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!             !serie7_P47_1_angle2
!             if ((gradPhi(j,i)<0.55).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!             if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==77).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==81).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==699).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==310).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==623).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==673).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==779).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1


!             !serie8_P17_2_angle2
!             if ((gradPhi(j,i)<0.55).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!             if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==483).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!             if ((kt==298).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==428).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==437).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==490).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==518).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==519).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1


!             !serie8_P27_1_angle2
!             if ((gradPhi(j,i)<0.58).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!!             if ((gradPhi(j,i)<0.55).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!!             if ((kt==393).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!!             if ((kt==60).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!!             if ((kt==62).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!             if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt>=11).and.(kt<=50).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!!             if ((kt>=11).and.(kt<=38).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!!             if ((kt==430).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!!             if ((kt==546).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!!             if ((kt==625).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!!             if ((kt==720).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!!             if ((kt==264).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!!             if ((kt==265).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==242).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==363).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==450).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!             !serie10_DMSO_p4P1_1_angle2
!             if ((gradPhi(j,i)<0.58).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!             if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==43).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==50).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==52).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==317).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==420).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==514).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==319).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==562).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!             if ((kt==616).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!              !serie5_12_angle2
!              if ((gradPhi(j,i)<0.52).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!              if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!              if ((kt==335).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!              if ((kt==175).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!              if ((kt==192).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!              if ((kt==288).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!              if ((kt==290).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!              !serie13_DMSO_p1P1_1_angle2
!              if ((gradPhi(j,i)<0.52).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!              if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!              if ((kt==269).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!              if ((kt==328).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!              if ((kt==346).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!              if ((kt==416).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!              if ((kt==507).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!              if ((kt==571).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!              if ((kt==575).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!              if ((kt==646).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!              if ((kt==665).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!              if ((kt==385).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!              if ((kt==615).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!              if ((kt==177).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!              if ((kt==352).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!              if ((kt==675).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!              if ((kt==778).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!              if ((kt==353).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!               !serie13_CPO100_p12P2_2_angle2
!               if ((gradPhi(j,i)<0.55).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!               if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!               if ((kt==553).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!               if ((kt==104).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!               if ((kt==544).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!               if ((kt==590).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!               if ((kt==742).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!               if ((kt==411).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!               if ((kt==452).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!               if ((kt==593).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!               if ((kt==412).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!               if ((kt==452).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!                !serie10_CPO_p12P1_2_angle2
!                if ((gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==148).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==256).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==285).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==395).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==475).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==581).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==706).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==760).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==304).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==611).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==614).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!                !serie10_CPO_p10P2_1_angle2
!                if ((gradPhi(j,i)<0.54).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==36).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==37).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==76).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==105).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==293).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==304).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==457).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==469).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==478).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!                !serie11_P13_1_angle2
!                if ((gradPhi(j,i)<0.52).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==530).and.(gradPhi(j,i)<0.54).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==597).and.(gradPhi(j,i)<0.54).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==550).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==597).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==60).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==75).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==217).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==349).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==351).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==370).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==433).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==663).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!                !serie12_P43_3_angle2
!                if ((gradPhi(j,i)<0.54).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==304).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==127).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==232).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==250).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==261).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==328).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==355).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==402).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==411).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==482).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==738).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==780).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==260).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==577).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==579).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==739).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==743).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!                !serie12_P33_1_angle2
!                if ((gradPhi(j,i)<0.52).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==387).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==542).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==433).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==257).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==446).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==569).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==605).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==618).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==633).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==797).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==841).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==258).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==374).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==452).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==486).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==608).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!                !serie11_P31_1_angle2
!                if ((gradPhi(j,i)<0.52).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==192).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==374).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==506).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==508).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==526).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==328).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==338).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==436).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==482).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!                !serie12_P101_1_angle2
!                if ((gradPhi(j,i)<0.52).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==290).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==318).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==352).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==393).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==417).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==423).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==145).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==395).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==401).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==402).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==403).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==518).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==564).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==589).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==700).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==735).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==766).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==435).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!                !serie12_P81_2_angle2
!                if ((gradPhi(j,i)<0.52).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==145).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==161).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==240).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==248).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==271).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==397).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt>=440).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==440).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==142).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==218).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==371).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==549).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==563).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!                !serie10_CPO_p5P1_1_angle2
!                if ((gradPhi(j,i)<0.54).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==539).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==542).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==44).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==188).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==323).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==328).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==329).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==480).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==498).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==530).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==539).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==604).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==620).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!                !serie10_CPO_p5P2_1_angle2
!                if ((gradPhi(j,i)<0.54).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==449).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==450).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==72).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==151).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==186).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==189).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==250).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==275).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==376).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==457).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==598).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==599).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==601).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==195).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==664).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!                !serie10_CPO_p7P3_1_angle2
!                if ((gradPhi(j,i)<0.54).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==352).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==353).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==553).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==623).and.(gradPhi(j,i)<0.6).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==265).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==290).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==329).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==434).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==438).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==512).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==700).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

!                !serie13_CPO150_p8P1_2_angle2
!                if ((gradPhi(j,i)<0.54).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((kt==296).and.(gradPhi(j,i)<0.56).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
!                if ((gradPhi(j,i)<0.94).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==749).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==141).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==143).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==144).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==145).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==458).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==520).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==523).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1 
!                if ((kt==596).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1
!                if ((kt==726).and.(gradPhi(j,i)<0.98).and.(rhoSlices2(j,i)>0._pr)) skel2(j,i) = 1

        !**
        !! First: Thresholds for extracting the skeleton and its gradient
        !**
        enddo
     enddo

     !**
     !! Second step: we reduce the gradient map
     !**
     skel3 = 0
     do i=2,nx-1
        do j=2,ny-1
          Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
          skel2(i,j-1) + skel2(i+1,j-1)
          !if ((skel2(i,j)==1).and.(skel2(i+1,j)==1).and.(skel2(i,j+1)==1).and.(skel2(i-1,j)==1).and.(skel2(i,j-1)==1)) skel22(i,j) = 1
          if ((skel2(i,j)==1).and.(Nseed(i,j)>5)) skel3(i,j) = 1
          !if ((skel2(i,j)==1).and.(Nseed(i,j)==6)) skel3(i,j) = 1
        enddo
     enddo
     skel2 = skel3

!     l = 1
!     pix1 = 0
!     pix2 = 0
!     skel2 = skel
!     do i=2,N-1
!        do j=2,ny-1
!           Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
!                skel(i+1,j-1)
!           !if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
!           !if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
!           !if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
!           !if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i-1,j+1)==1)) skel2(i,j) = 1
!           !if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i,j+1)==1)) skel2(i,j) = 1
!           !if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j)==1)) skel2(i,j) = 1
!           !if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
!           !if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j+1)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i-1,j)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i-1,j)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i,j-1)==1).and.(skel(i-1,j+1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i,j-1)==1).and.(skel(i,j+1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i-1,j)==1).and.(skel(i+1,j)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i-1,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i-1,j+1)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
!        enddo
!     enddo
!     skel = skel2
!     skel2 = skel
!     do i=2,N-1
!        do j=2,ny-1
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j)==1).and.(skel2(i-1,j+1)==1)) skel2(i-1,j) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j+1)==1).and.(skel2(i,j+1)==1)) skel2(i,j+1) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j+1)==1).and.(skel2(i+1,j+1)==1)) skel2(i,j+1) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j+1)==1).and.(skel2(i+1,j)==1)) skel2(i+1,j) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j)==1).and.(skel2(i+1,j-1)==1)) skel2(i+1,j) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j-1)==1).and.(skel2(i,j-1)==1)) skel2(i,j-1) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j-1)==1).and.(skel2(i-1,j-1)==1)) skel2(i,j-1) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j-1)==1).and.(skel2(i-1,j)==1)) skel2(i-1,j) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)==0)) skel2(i,j) = 0
!        enddo
!     enddo
!     skel = skel2
!!     do while ((pix1==1).or.(pix2==1).or.(l==1))
!!        pix1 = 0
!!        pix2 = 0
!!        l = l + 1
!!        skel2 = skel
!!        do i=2,N-1
!!           do j=2,ny-1
!!              maxv = 0
!!              if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
!!              if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
!!              if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
!!              if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
!!              if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
!!              if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
!!              if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
!!              if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
!!              Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
!!                   skel(i+1,j-1)
!!              if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.&
!!                   ((skel(i+1,j)==0).or.(skel(i-1,j)==0).or.(skel(i,j-1)==0)).and.((skel(i,j-1)==0).or.(skel(i,j+1)==0)&
!!                   !.or.(skel(i,j+1)==0))) then
!!                   .or.(skel(i-1,j)==0))) then
!!                 skel2(i,j) = 0
!!                 pix1 = 1
!!              endif
!!              if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.&
!!                   ((skel(i,j+1)==0).or.(skel(i-1,j)==0).or.(skel(i,j-1)==0)).and.((skel(i+1,j)==0).or.(skel(i,j+1)==0)&
!!                   .or.(skel(i-1,j)==0))) then
!!                 skel2(i,j) = 0
!!                 pix2 = 1
!!              endif
!!           enddo
!!        enddo
!!        skel = skel2
!!        skel2 = skel
!!        do i=2,N-1
!!           do j=2,ny-1
!!              maxv = 0
!!              if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
!!              if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
!!              if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
!!              if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
!!              if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
!!              if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
!!              if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
!!              if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
!!              Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
!!                   skel(i+1,j-1)
!!              if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.&
!!                   ((skel(i,j+1)==0).or.(skel(i+1,j)==0).or.(skel(i,j-1)==0)).and.((skel(i+1,j)==0).or.(skel(i,j-1)==0)&
!!                   .or.(skel(i-1,j)==0))) then
!!                 skel2(i,j) = 0
!!                 pix2 = 1
!!              endif
!!           enddo
!!        enddo
!!        skel = skel2
!!     enddo
!     skel2 = skel
!     do i=2,N-1
!        do j=2,ny-1
!           Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
!                skel(i+1,j-1)
!           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i-1,j+1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i,j+1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
!           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j+1)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
!        enddo
!     enddo
!     skel = skel2
!     skel2 = skel
!     do i=2,N-1
!        do j=2,ny-1
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j)==1).and.(skel2(i-1,j+1)==1)) skel2(i-1,j) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j+1)==1).and.(skel2(i,j+1)==1)) skel2(i,j+1) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j+1)==1).and.(skel2(i+1,j+1)==1)) skel2(i,j+1) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j+1)==1).and.(skel2(i+1,j)==1)) skel2(i+1,j) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j)==1).and.(skel2(i+1,j-1)==1)) skel2(i+1,j) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j-1)==1).and.(skel2(i,j-1)==1)) skel2(i,j-1) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j-1)==1).and.(skel2(i-1,j-1)==1)) skel2(i,j-1) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j-1)==1).and.(skel2(i-1,j)==1)) skel2(i-1,j) = 0
!           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) +&
!                skel2(i,j-1) + skel2(i+1,j-1)
!           if ((skel2(i,j)==1).and.(Nseed(i,j)==0)) skel2(i,j) = 0
!        enddo
!     enddo
!     skel = skel2

     !**
     !! Third step: we complete the level-set map
     !**
!     call ZST(skel)
     skel3 = skel
     do i=2,nx-1
        do j=2,ny-1
           Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
                skel(i+1,j-1)
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j+1)==1)) skel3(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j-1)==1)) skel3(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i+1,j+1)==1)) skel3(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i-1,j+1)==1)) skel3(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i,j+1)==1)) skel3(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j)==1)) skel3(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j-1)==1).and.(skel(i+1,j+1)==1)) skel3(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j+1)==1).and.(skel(i+1,j-1)==1)) skel3(i,j) = 1
        enddo
     enddo

     !**
     !! Fourth step: MANUAL CORRECTIONS of level-set and gradient maps
     !**
      !! FILM
!      !--- film5_18 (bis)
!      if(kt==112)skel3(153,111)=0
!      if(kt==242)skel3(150,95)=0
!      if(kt==289)skel3(158,107)=0
!      if(kt==289)skel3(158,108)=0
!      if(kt==290)skel3(157,108)=0
!      if(kt==453)skel3(155,109)=0

!      !--- film2_19 (bis)
!      if(kt==123)skel3(158,99)=0
!      if(kt==144)skel3(152,93)=0
!      if(kt==193)skel3(142,91)=0
!      if(kt==284)skel3(148,111)=0
!      if(kt==294)skel3(154,109)=0
!      if(kt==296)skel3(154,108)=0
!      if(kt==409)skel3(154,112)=0
!      if(kt==415)skel3(154,111)=0
!      if(kt==417)skel3(155,112)=0
!      if(kt==417)skel3(154,111)=0
!      if(kt==460)skel3(160,97)=0
!      if(kt==463)skel3(156,95)=0
!      if(kt==509)skel3(118,94)=0
!      if(kt==513)skel3(113,93)=0
!      if(kt==514)skel3(111,92)=0
!      if(kt==519)skel3(104,90)=0
!      if(kt==534)skel3(91,92)=0
!      if(kt==537)skel3(67,127)=0
!      if(kt==537)skel3(88,93)=0
!      if(kt==538)skel3(87,94)=0
!      if(kt==539)skel3(85,94)=0
!      if(kt==541)skel3(83,96)=0
!      if(kt==542)skel3(82,97)=0
!      if(kt==543)skel3(79,99)=0
!      if(kt==544)skel3(78,99)=0
!      if(kt==544)skel3(79,99)=0
!      if(kt==545)skel3(77,99)=0
!      if(kt==546)skel3(76,101)=0
!      if(kt==547)skel3(160,108)=0
!     !--- film8_P19_1 (bis)
!if(kt==1)skel3(163,101)=0
!if(kt==71)skel3(161,98)=0
!if(kt==70)skel3(162,99)=0
!if(kt==76)skel3(160,97)=0
!if(kt==334)skel3(152,110)=0
!if(kt==342)skel3(154,108)=0
!if(kt==378)skel3(161,100)=0
!if(kt==402)skel3(158,97)=0
!if(kt==403)skel3(158,96)=0
!if(kt==516)skel3(154,110)=0
!if(kt==521)skel3(153,110)=0
!if(kt==523)skel3(152,110)=0
!if(kt==538)skel3(150,112)=0
!do i=1,size(skel2,1)
!  do j=1,size(skel2,2)
!      if((kt>=740).and.(i>=159).and.(j>=110))skel2(i,j)=0
!      if((kt>=771).and.(i>=153).and.(j>=110))skel2(i,j)=0
!  enddo
!enddo


!!     !--- film5_17
!if(kt==1)skel3(165,101)=0
!if(kt==1)skel3(164,101)=0
!if(kt==137)skel3(162,109)=0
!if(kt==271)skel3(158,91)=0
!if(kt==295)skel3(147,88)=0
!if(kt==621)skel3(161,94)=0
!if(kt==622)skel3(160,92)=0

!!! filme5_18_seuil
!if(kt==105)skel3(152,109)=0
!if(kt==233)skel3(141,85)=0
!if(kt==272)skel3(58,71)=0
!if(kt==299)skel3(152,110)=0
!if(kt==334)skel3(157,106)=0
!if(kt==451)skel3(156,110)=0
!if(kt==536)skel3(156,92)=0

!!! filme5_18_angle2
if(kt==79)skel3(155,109)=0
if(kt==219)skel3(136,85)=0
if(kt==370)skel3(158,95)=0
if(kt==395)skel3(152,93)=0
if(kt==395)skel3(154,93)=0
if(kt==395)skel3(153,92)=0
if(kt==392)skel3(152,92)=0
if(kt==440)skel3(160,109)=0
if(kt==480)skel3(157,107)=0
if(kt==523)skel3(157,95)=0
if(kt==103)skel3(151,110)=0

!!!!! filme2_19_angle2
!if(kt==125)skel3(158,97)=0
!if(kt==125)skel3(157,96)=0
!if(kt==127)skel3(156,95)=0
!if(kt==128)skel3(156,95)=0
!if(kt==125)skel3(156,96)=0
!if(kt==126)skel3(156,96)=0
!if(kt==126)skel3(156,97)=0
!if(kt==140)skel3(153,91)=0
!if(kt==137)skel3(155,92)=0
!if(kt==190)skel3(142,92)=0
!if(kt==190)skel3(141,92)=0
!if(kt==258)skel3(152,118)=0
!if(kt==335)skel3(157,97)=0
!if(kt==335)skel3(157,96)=0
!if(kt==391)skel3(158,113)=0
!if(kt==426)skel3(157,110)=0
!if(kt==431)skel3(158,107)=0
!if(kt==463)skel3(156,97)=0
!if(kt==472)skel3(157,96)=0
!if(kt==500)skel3(128,99)=0
!if(kt==501)skel3(128,99)=0
!if(kt==500)skel3(128,100)=0
!do i=1,size(skel2,1)
!  do j=1,size(skel2,2)
!    if((kt>509).and.(i>=162).and.(j>=110))skel2(i,j)=0
!    if((kt>=393).and.(i>=159).and.(j>=114))skel2(i,j)=0
!    if((kt>=402).and.(i>=155).and.(j>=116))skel2(i,j)=0
!    if((kt>=281).and.(kt<300).and.(i>=147).and.(j>=111))skel2(i,j)=0
!  enddo
!enddo
!if(kt==537)skel3(89,91)=0
!if(kt==538)skel3(87,92)=0
!if(kt==539)skel3(86,93)=0
!if(kt==540)skel3(86,94)=0
!if(kt==541)skel3(83,94)=0
!if(kt==542)skel3(82,95)=0
!if(kt==543)skel3(80,97)=0
!if(kt==544)skel3(79,98)=0
!if(kt==545)skel3(77,99)=0
!if(kt==546)skel3(76,100)=0
!if(kt==552)skel3(83,85)=0
!if(kt==266)skel2(148,116)=0
!if(kt==395)skel2(157,113)=0
!if(kt==276)skel3(146,113)=0
!if(kt==542)skel2(161,112)=0


!!!!--- film5_17 angle2---
!if(kt==1)skel3(165,101)=0
!if(kt==1)skel3(164,101)=0
!if(kt==27)skel3(167,105)=0
!if(kt==271)skel3(157,85)=0
!if(kt==277)skel3(153,84)=0
!if(kt==297)skel3(143,83)=0
!if(kt==341)skel3(144,88)=0
!if(kt==344)skel3(147,89)=0
!if(kt==557)skel3(158,104)=0
!if(kt==632)skel3(154,85)=0

!!!!--- film8_P19_1 angle2---
!if(kt==72)skel3(160,97)=0
!if(kt==266)skel3(145,112)=0
!if(kt==267)skel3(145,112)=0

!!!!---film8_P39_2angle2---
!if(kt==156)skel3(146,88)=0
!if(kt==163)skel3(145,90)=0
!if(kt==266)skel3(157,109)=0
!if(kt==373)skel3(157,95)=0
!if(kt==371)skel3(157,94)=0
!if(kt==372)skel3(157,94)=0
!if(kt==434)skel3(158,107)=0
!if(kt==441)skel3(158,108)=0
!if(kt==468)skel3(157,107)=0
!if(kt==623)skel3(159,109)=0
!if(kt==648)skel3(158,107)=0
!if(kt==659)skel2(163,108)=0


!!! film7_P19_1_angle2
!if(kt==1)skel2(166,105)=0
!if(kt==332)skel3(150,89)=0
!if(kt==364)skel3(142,88)=0
!if(kt==407)skel3(143,90)=0
!if(kt==564)skel3(163,100)=0
!if(kt==566)skel3(162,98)=0
!if(kt==582)skel3(161,95)=0
!if(kt==591)skel3(158,92)=0
!if(kt==761)skel2(163,104)=0
!if(kt==773)skel3(162,98)=0
!if(kt==774)skel3(162,98)=0
!if(kt==781)skel3(160,95)=0
!if(kt==794)skel3(158,94)=0


!!! film8_P65_1_angle2
!if(kt==64)skel3(167,99)=0
!do i=1,size(skel2,1)
!  do j=1,size(skel2,2)
!      if((kt>=59).and.(kt<70).and.(i>=167).and.(j>=100))skel2(i,j)=0
!  enddo
!enddo
!if(kt==117)skel3(62,94)=0
!if(kt==746)skel3(158,94)=0
!if(kt==746)skel3(159,94)=0

!!! film7_P15_2_angle2
!if(kt==316)skel3(159,103)=0
!if(kt==475)skel3(162,97)=0
!if(kt==513)skel3(154,94)=0
!if(kt==511)skel3(155,94)=0
!if(kt==511)skel3(154,94)=0
!if(kt==293)skel3(157,110)=0
!if(kt==412)skel3(167,109)=0
!if(kt==422)skel3(163,108)=0
!do i=1,size(skel2,1)
!  do j=1,size(skel2,2)
!      if((kt>=409).and.(kt<428).and.(i>=163).and.(j>=106))skel2(i,j)=0
!  enddo
!enddo

!!! film7_P35_2_angle2
!if(kt==245)skel3(152,109)=0
!if(kt==245)skel3(151,107)=0
!if(kt==449)skel3(158,105)=0
!if(kt==450)skel3(157,104)=0

!! film9_p3_2_angle2
!if(kt==1)skel3(164,103)=0
!if(kt==1)skel3(162,101)=0
!if(kt==94)skel3(163,97)=0
!if(kt==98)skel3(162,96)=0
!if(kt==115)skel3(160,95)=0
!if(kt==157)skel3(160,97)=0
!if(kt==161)skel3(160,97)=0
!if(kt==251)skel3(145,112)=0
!if(kt==258)skel3(141,112)=0
!if(kt==272)skel3(144,109)=0
!if(kt==272)skel3(145,109)=0
!if(kt==276)skel3(145,109)=0
!if(kt==282)skel3(146,109)=0
!if(kt==302)skel3(157,104)=0
!if(kt==303)skel3(157,103)=0
!if(kt==335)skel3(160,95)=0
!if(kt==332)skel3(158,95)=0
!if(kt==374)skel2(148,89)=0
!if(kt==376)skel3(148,89)=0
!if(kt==377)skel3(149,91)=0
!if(kt==652)skel3(160,104)=0
!if(kt==676)skel3(159,95)=0
!if(kt==724)skel3(151,92)=0
!if(kt==722)skel3(150,92)=0
!if(kt==483)skel3(159,101)=0
!if(kt==402)skel3(159,97)=0
!if(kt==483)skel3(159,105)=0
!if(kt==485)skel3(159,103)=0
!if(kt==19)skel3(163,99)=0
!if(kt==47)skel3(163,101)=0

!!!! film7_P47_1_angle2
!if(kt==1)skel3(158,101)=0
!if(kt==277)skel3(140,110)=0
!if(kt==318)skel3(154,101)=0
!if(kt==516)skel3(156,98)=0
!if(kt==655)skel3(151,109)=0
!if(kt==76)skel3(158,100)=0

!!!! film8_P17_2_angle2
!if(kt==267)skel3(144,106)=0
!if(kt==266)skel3(146,111)=0
!if(kt==476)skel3(156,111)=0
!if(kt==494)skel3(158,106)=0
!do i=1,size(skel2,1)
!  do j=1,size(skel2,2)
!      if((kt==664).and.(i>=155).and.(j>=114))skel2(i,j)=0
!  enddo
!enddo

!!! film8_P27_1_angle2
!!if(kt==1)skel3(160,102)=0
!if(kt==1)skel3(159,101)=0
!if(kt==64)skel3(157,101)=0
!if(kt==65)skel3(157,99)=0
!if(kt==427)skel3(151,110)=0
!if(kt==441)skel3(153,108)=0
!if(kt==504)skel3(153,97)=0
!do i=1,size(skel2,1)
!  do j=1,size(skel2,2)
!      if((kt==418).and.(i>=153).and.(j>=114))skel2(i,j)=0
!  enddo
!enddo
!!if(kt==82)skel3(158,99)=0
!!if(kt==571)skel3(155,108)=0
!!if(kt==579)skel3(154,109)=0
!!if(kt==263)skel2(150,99)=0

!! film10_DMSO_p4P1_1_angle2
!if(kt==97)skel3(156,111)=0
!if(kt==96)skel3(157,112)=0
!if(kt==181)skel3(156,102)=0
!do i=1,size(skel2,1)
!  do j=1,size(skel2,2)
!      if((kt>=356).and.(kt<=396).and.(i>=155).and.(j>=109))skel2(i,j)=0
!      if((kt>=546).and.(kt<=553).and.(i>=157).and.(j>=109))skel2(i,j)=0
!  enddo
!enddo
!if(kt==452)skel3(157,92)=0
!if(kt==457)skel3(155,92)=0
!if(kt==551)skel3(155,110)=0
!if(kt==567)skel3(157,113)=0
!if(kt==574)skel3(155,110)=0
!if(kt==624)skel3(162,96)=0
!if(kt==631)skel3(161,96)=0
!if(kt==347)skel3(162,110)=0
!if(kt==442)skel3(160,94)=0

!! film5_12_angle2
!if(kt==1)skel3(163,100)=0
!if(kt==52)skel3(163,98)=0
!if(kt==61)skel3(163,96)=0
!if(kt==63)skel3(161,95)=0
!if(kt==403)skel3(152,92)=0
!if(kt==403)skel3(153,91)=0
!if(kt==412)skel3(151,92)=0
!if(kt==512)skel3(160,99)=0

!! film13_DMSO_p1P1_1_angle2
!if(kt==187)skel3(160,108)=0
!if(kt==374)skel3(134,77)=0 !!CONTACT ATTENTION
!if((kt>374).and.(kt<385))skel3(133,78)=0
!if((kt>374).and.(kt<385))skel3(131,79)=0
!if((kt>374).and.(kt<385))skel3(132,79)=0
!if((kt>374).and.(kt<385))skel3(133,79)=0
!if(kt==679)skel3(157,107)=0
!if(kt==754)skel3(154,101)=0

!! film13_CPO100_p12P2_2_angle2
!if(kt==121)skel3(162,101)=0
!if(kt==597)skel3(150,112)=0
!if(kt==603)skel3(151,110)=0
!if(kt==629)skel3(158,103)=0
!if(kt==655)skel3(157,95)=0
!if(kt==660)skel3(155,93)=0

!! film10_CPO_p12P1_2_angle2
!if(kt==192)skel3(148,111)=0
!if(kt==191)skel3(148,110)=0
!if(kt==200)skel3(145,110)=0
!if(kt==620)skel3(158,108)=0
!if(kt==644)skel3(73,98)=0
!if(kt==681)skel3(52,106)=0
!if(kt==682)skel3(51,106)=0
!if(kt==259)skel3(153,108)=0
!if(kt==715)skel3(157,96)=0

!! film10_CPO_p10P2_1_angle2
!if(kt==216)skel3(149,112)=0
!if(kt==609)skel3(156,106)=0

!! film11_P13_1_angle2
!if(kt==307)skel3(145,88)=0
!if(kt==380)skel3(159,111)=0
!if(kt==567)skel3(162,107)=0
!if(kt==586)skel3(156,112)=0
!if(kt==596)skel3(157,114)=0
!if(kt==571)skel3(165,111)=0
!if(kt==235)skel3(163,100)=0

!! film12_P43_3_angle2
!if(kt==705)skel3(161,100)=0
!if(kt==708)skel3(161,99)=0

!! film12_P33_1_angle2
!if(kt==147)skel3(161,101)=0
!if(kt==296)skel3(155,109)=0
!if(kt==725)skel3(154,108)=0
!if(kt==747)skel3(59,102)=0
!if(kt==749)skel3(57,102)=0

!! film11_P31_1_angle2
!if(kt==1)skel3(163,101)=0
!if(kt==142)skel3(161,104)=0
!if(kt==152)skel3(159,108)=0
!if(kt==164)skel3(152,109)=0
!if(kt==166)skel3(152,108)=0
!if(kt==166)skel3(153,109)=0
!if(kt==166)skel3(151,107)=0
!if(kt==182)skel3(147,109)=0
!if(kt==356)skel2(161,109)=0
!if(kt==358)skel3(159,108)=0
!if(kt==359)skel3(159,108)=0
!if(kt==504)skel3(159,110)=0

!! film12_P101_1_angle2
!if(kt==416)skel3(157,113)=0
!if(kt==418)skel2(157,114)=0
!if(kt==418)skel3(160,116)=0
!if(kt==419)skel3(157,115)=0
!if(kt==424)skel3(152,113)=0
!if(kt==429)skel3(153,114)=0
!if(kt==430)skel3(155,116)=0
!if(kt==453)skel3(155,107)=0
!if(kt==577)skel3(156,110)=0
!if(kt==604)skel2(159,108)=0
!if(kt==452)skel2(157,109)=0
!if(kt==452)skel3(157,109)=0

!! film12_P81_2_angle2
!if(kt==172)skel3(157,108)=0
!if(kt==321)skel3(143,92)=0
!if(kt==321)skel3(144,92)=0
!if(kt==345)skel3(86,60)=0
!if(kt==390)skel2(160,111)=0
!if(kt==394)skel2(158,112)=0
!if(kt==395)skel3(156,111)=0
!if(kt==396)skel3(155,109)=0
!if(kt==400)skel3(153,109)=0
!if(kt==408)skel3(65,126)=0
!if(kt==411)skel3(66,129)=0
!if(kt==412)skel3(68,129)=0
!if(kt==413)skel3(68,131)=0
!if(kt==414)skel3(69,132)=0
!if(kt==411)skel3(150,108)=0
!if(kt==412)skel3(150,108)=0
!if(kt==419)skel3(150,108)=0
!if(kt==503)skel3(148,93)=0
!if(kt==501)skel3(149,92)=0
!if(kt==562)skel3(156,110)=0
!if(kt==193)skel3(150,114)=0
!if(kt==200)skel3(147,113)=0
!do i=1,size(skel2,1)
!  do j=1,size(skel2,2)
!    if((kt>=566).and.(kt<=574).and.(i>=154).and.(j>=107))skel2(i,j)=0
!    if((kt>=578).and.(kt<=596).and.(i>=153).and.(j>=106))skel2(i,j)=0
!  enddo
!enddo

!! film10_CPO_p5P1_1_angle2
!if(kt==290)skel3(150,90)=0
!if(kt==291)skel3(150,91)=0
!if(kt==293)skel3(149,92)=0
!if(kt==296)skel3(149,92)=0
!if(kt==548)skel3(160,108)=0
!if(kt==623)skel3(160,99)=0

!! film10_CPO_p5P2_1_angle2
!if(kt==87)skel3(154,89)=0
!if(kt==388)skel3(77,109)=0
!if(kt==426)skel3(149,114)=0
!if(kt==427)skel3(149,113)=0
!if(kt==643)skel3(166,111)=0
!if(kt==631)skel3(162,112)=0
!if(kt==634)skel3(163,112)=0
!do i=1,size(skel2,1)
!  do j=1,size(skel2,2)
!    if((kt>=448).and.(kt<=467).and.(i>=147).and.(j>=112))skel2(i,j)=0
!    if((kt==642).and.(i>=162).and.(j>=109))skel2(i,j)=0
!  enddo
!enddo
!if(kt==80)skel2(157,91)=0
!if(kt==80)skel2(156,91)=0
!if(kt==80)skel2(156,92)=0
!if(kt==80)skel2(157,92)=0

!! film10_CPO_p7P3_1_angle2
!if(kt==547)skel3(157,110)=0
!if(kt==561)skel3(153,110)=0
!if(kt==623)skel3(162,100)=0
!if(kt==289)skel2(155,95)=0

!! film13_CPO150_p8P1_2_angle2
!if(kt==245)skel3(157,114)=0
!if(kt==414)skel3(144,90)=0
!if(kt==421)skel3(147,90)=0
!if(kt==628)skel3(154,110)=0
!if(kt==641)skel3(152,110)=0
!do i=1,size(skel2,1)
!  do j=1,size(skel2,2)
!    if((kt>=479).and.(kt<=505).and.(i>=157).and.(j>=110))skel2(i,j)=0
!  enddo
!enddo
!if(kt==248)skel3(155,115)=0
!if(kt==305)skel3(156,109)=0
!if((kt>=342).and.(kt<=346))skel2(153,98)=0
!if((kt>=343).and.(kt<=346))skel2(153,98)=0
!if((kt>=343).and.(kt<=344))skel2(154,98)=0










!      !!--- film2_19 ---
!      if(kt==118)skel3(158,100)=0
!      if(kt==124)skel3(158,98)=0
!      if(kt==128)skel3(157,97)=0
!      if(kt==129)skel3(157,94)=0
!      if(kt==131)skel3(157,97)=0
!      if(kt==183)skel3(139,91)=0
!      if(kt==238)skel3(158,107)=0
!      if(kt==242)skel3(157,107)=0
!      if(kt==243)skel3(157,107)=0
!      if(kt==365)skel3(158,97)=0
!      do i=1,size(skel2,1)
!        do j=1,size(skel2,2)
!            if((kt>=411).and.(kt<=415).and.(i>=155).and.(j>=113))skel2(i,j)=0
!            if((kt>=423).and.(kt<=426).and.(i>=157).and.(j>=107))skel2(i,j)=0
!            if((kt==530).and.(i>=158).and.(j>=110))skel2(i,j)=0
!            if((kt==546).and.(i>=160).and.(j>=106))skel2(i,j)=0
!        enddo
!      enddo
!      if(kt==509)skel3(117,93)=0
!      if(kt==510)skel3(118,94)=0
!      if(kt==524)skel3(160,110)=0
!      if(kt==534)skel3(91,92)=0
!      if(kt==537)skel3(88,93)=0
!      if(kt==538)skel3(86,95)=0
!      if(kt==539)skel3(85,95)=0
!      if(kt==540)skel3(84,96)=0
!      if(kt==541)skel3(83,97)=0
!      if(kt==542)skel3(82,98)=0
!      if(kt==543)skel3(80,100)=0
!      if(kt==544)skel3(79,101)=0
!      if(kt==545)skel3(77,102)=0
!      if(kt==545)skel3(159,106)=0
!      if(kt==546)skel3(77,103)=0
!      if(kt==553)skel3(160,103)=0
!      if(kt==553)skel3(159,103)=0
!     !!--- film5_18 --- 
!     if (kt==74) skel3(158,109) = 0
!     if (kt==80) skel3(155,111) = 0
!     if (kt==84) skel3(57,133) = 0
!     if (kt==103) skel3(150,109) = 0
!     if (kt==103) skel3(151,109) = 0
!     if (kt==205) skel3(136,87) = 0
!     if (kt==206) skel3(136,87) = 0
!     if (kt==207) skel3(137,88) = 0
!     if (kt==208) skel3(137,89) = 0
!     if (kt==208) skel3(138,89) = 0
!     if (kt==209) skel3(138,90) = 0
!     if (kt==209) skel3(139,90) = 0
!     if (kt==210) skel3(138,91) = 0
!     if (kt==210) skel3(139,91) = 0
!     if (kt==211) skel3(138,92) = 0
!     if (kt==211) skel3(139,92) = 0
!     if (kt==288) skel3(157,110) = 0 
!     if (kt==303) skel3(150,112) = 0
!     if (kt==459) skel3(153,110) = 0
!     if (kt==464) skel3(154,110) = 0
!     if (kt==483) skel3(158,106) = 0
!     if (kt==544) skel3(157,93) = 0 
!     if (kt==545) skel3(157,94) = 0
!     if (kt==582) skel3(160,109) = 0
!     if (kt==590) skel3(157,110) = 0
!     if (kt==607) skel3(157,107) = 0
!     if (kt==609) skel3(158,107) = 0
!!!--- film5_18 ---
!if(kt==139)skel3(57,78)=0
!if(kt==153)skel3(156,107)=0
!if(kt==156)skel3(155,105)=0
!if(kt==158)skel3(155,102)=0
!if(kt==166)skel3(151,94)=0
!if(kt==177)skel3(145,90)=0
!if(kt==272)skel3(121,159)=0
!if(kt==275)skel3(120,163)=0
!if(kt==275)skel3(121,162)=0
!if(kt==275)skel3(121,163)=0
!if(kt==378)skel3(72,145)=0
!if(kt==417)skel3(135,62)=0
!if(kt==421)skel3(139,63)=0
!if(kt==420)skel3(139,63)=0
!if(kt==424)skel3(141,64)=0
!if(kt==444)skel3(151,77)=0
!if(kt==452)skel3(151,83)=0
!if(kt==488)skel3(154,82)=0
!if(kt==513)skel3(146,68)=0
!if(kt==545)skel3(141,69)=0
!if(kt==550)skel3(146,70)=0
!if(kt==552)skel3(147,72)=0
!!--- film8_P19_1 ---
!      if(kt==59)skel3(163,100)=0
!      if (kt==60) skel3(163,99)=0
!      if (kt==60) skel3(162,99)=0
!      if (kt==59) skel3(163,100)=0
!      if (kt==69) skel3(161,96)=0
!      if (kt==68) skel3(162,97)=0
!      if (kt==86) skel3(158,95)=0
!      if (kt==106) skel3(151,90)=0
!      if (kt==221) skel3(157,108)=0
!      if (kt==230) skel3(156,109)=0
!      if (kt==248) skel3(150,112)=0
!      if (kt==246) skel3(151,112)=0
!      if (kt==247) skel3(151,112)=0
!      if (kt==267) skel3(145,113)=0
!      if (kt==285) skel3(142,113)=0
!      if (kt==297) skel3(142,112)=0
!      if (kt==305) skel3(144,111)=0
!      if (kt==316) skel3(146,110)=0
!      if (kt==318) skel3(146,109)=0
!      if (kt==322) skel3(147,109)=0
!      if (kt==326) skel3(148,108)=0
!      if (kt==327) skel3(148,107)=0
!      if (kt==328) skel3(148,108)=0
!      if (kt==332) skel3(149,108)=0
!      if (kt==402) skel3(158,96)=0
!      if (kt==411) skel3(154,92)=0
!      if (kt==411) skel3(153,93)=0
!      if (kt==411) skel3(155,93)=0
!      if (kt==513) skel3(155,109)=0
!      if (kt==528) skel2(154,114)=0
!      if (kt==519) skel2(152,110)=0
!      if (kt==526) skel2(151,111)=0
!      if (kt==529) skel2(151,111)=0
!      if (kt==533) skel2(150,111)=0
!      if (kt==537) skel2(150,111)=0
!      if (kt==542) skel2(149,111)=0
!      if (kt==551) skel2(151,110)=0
!      if (kt==556) skel2(151,109)=0
     skel = skel3

     !**
     !! Fifth step: construction of a 1-pixel wide skeleton
     !**
     call filterskel(skel,rhoSlices2)
     tmpbool = 0

     sizeSkel = sum(skel)
     write(*,*) "Longlong 00  : ",long3," ",long2," ",long

     !**
     !! Sixth step: construction of the midline from the 2D-map (skeleton)
     !**
     allocate(midline(sizeSkel,3))
     write(*,*) "okcut"
     l=1
     do i=2,nx-1
        do j=2,ny-1
           if ((skel(i,j)==1).and.(l==1)) then
              Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
                   skel(i+1,j-1)
              !!if ((Nseed(i,j)==1).and.((abs(xskelL-i)<100).and.(abs(yskelL-j)<100))) then 
              !              if ((((kt<121).or.(kt>140)).and.(Nseed(i,j)==1).and.((abs(xskelL-i)<100).and.(abs(yskelL-j)<100)))&
              !                   .or.((Nseed(i,j)==1).and.(kt==1))) then 
              !                 midline(l,1) = i
              !                 midline(l,2) = j
              !                 l = l+1
              !              else if (((Nseed(i,j)==1).and.((abs(xskelL-i)<20).and.(abs(yskelL-j)<20))).or.((Nseed(i,j)==1).and.(kt==1))) then 
              !if (((Nseed(i,j)==1).and.((abs(xskelL-i)<100).and.(abs(yskelL-j)<100))).or.((Nseed(i,j)==1).and.(kt==1))) then 
              if (((Nseed(i,j)==1).and.((abs(xskelL-i)<50).and.(abs(yskelL-j)<50))).or.((Nseed(i,j)==1).and.(kt==1))) then 
              !if (((Nseed(i,j)==1).and.((abs(xskelL-i)<10).and.(abs(yskelL-j)<10))).or.((Nseed(i,j)==1).and.(kt==1))) then 
                 midline(l,1) = i
                 midline(l,2) = j
                 l = l+1
              endif
           endif
        enddo
     enddo
     write(*,*) "okcut"
     !     if (kt==130) then
     !        skel(137,78)=0
     !        midline(1,1)=136
     !        midline(1,2)=77
     !     endif
     midline(1,3) = nint(zslice)
     write(*,*) "CHECK midline : ",midline(1,1)," ",midline(1,2)," ",midline(size(midline,1),1)," ",midline(size(midline,1),2)
     xskelL = midline(1,1)
     yskelL = midline(1,2)
     boolskel=0
     nl = sum(skel)
     do l=2,sum(skel)
        do i=2,nx-1
           do j=2,ny-1
              if ((skel(i,j)==1).and.(boolskel==0)) then
                 Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1)&
                      + skel(i+1,j-1)
                 if ((Nseed(i,j)>1).and.((midline(l-1,1)==i).or.(midline(l-1,1)==i-1).or.(midline(l-1,1)==i+1)).and.&
                      ((midline(l-1,2)==j).or.(midline(l-1,2)==j-1).or.(midline(l-1,2)==j+1)).and.(appartient(midline,i,j,l)&
                      .neqv..true.)) then 
                    midline(l,1) = i
                    midline(l,2) = j
                 endif
                 !                 if ((Nseed(i,j)==1).and.(l==sum(skel))) then
                 if ((Nseed(i,j)==1).and.((midline(l-1,1)==i).or.(midline(l-1,1)==i-1).or.(midline(l-1,1)==i+1)).and.&
                      ((midline(l-1,2)==j).or.(midline(l-1,2)==j-1).or.(midline(l-1,2)==j+1)).and.(appartient(midline,i,j,l)&
                      .neqv..true.)) then 
                    !                 if (Nseed(i,j)==1) then
                    midline(l,1) = i
                    boolskel = 1
                    nl=l
                    midline(l,2) = j
                 endif
              endif
           enddo
        enddo
        midline(l,3) = nint(zslice)
     enddo
     write(*,*) "CHECK midline : ",midline(1,1)," ",midline(1,2)," ",midline(size(midline,1),1)," ",midline(size(midline,1),2)&
          ," ",size(midline,1)," ",sum(skel)," ",nl
     write(*,*) "CHECK midline BIS : ",midline(1,1)," ",midline(1,2)," ",midline(nl,1)," ",midline(nl,2)," ",nl

     if (nl<10) then
             write(*,*) "error midline queue fourche"

             do l=1,nl
               skel(midline(l,1),midline(l,2)) = 0
             enddo

             l=1
             do i=2,nx-1
                do j=2,ny-1
                   if ((skel(i,j)==1).and.(l==1)) then
                      Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) +&
                      skel(i,j-1) + skel(i+1,j-1)
                      !if (((Nseed(i,j)==1).and.((abs(xskelL-i)<100).and.(abs(yskelL-j)<100))).or.((Nseed(i,j)==1).and.(kt==1))) then 
                      if (((Nseed(i,j)==1).and.((abs(xskelL-i)<50).and.(abs(yskelL-j)<50))).or.((Nseed(i,j)==1).and.(kt==1))) then 
                      !if (((Nseed(i,j)==1).and.((abs(xskelL-i)<10).and.(abs(yskelL-j)<10))).or.((Nseed(i,j)==1).and.(kt==1))) then 
                         midline(l,1) = i
                         midline(l,2) = j
                         l = l+1
                      endif
                   endif
                enddo
             enddo
             midline(1,3) = nint(zslice)
             write(*,*) "CHECK midline : ",midline(1,1)," ",midline(1,2)," ",midline(size(midline,1),1)," "&
             ,midline(size(midline,1),2)
             xskelL = midline(1,1)
             yskelL = midline(1,2)
             boolskel=0
             do l=2,sum(skel)
                do i=2,nx-1
                   do j=2,ny-1
                      if ((skel(i,j)==1).and.(boolskel==0)) then
                         Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) +&
                         skel(i,j-1) + skel(i+1,j-1)
                         if ((Nseed(i,j)>1).and.((midline(l-1,1)==i).or.(midline(l-1,1)==i-1).or.(midline(l-1,1)==i+1)).and.&
                              ((midline(l-1,2)==j).or.(midline(l-1,2)==j-1).or.(midline(l-1,2)==j+1)).and.&
                              (appartient(midline,i,j,l).neqv..true.)) then 
                            midline(l,1) = i
                            midline(l,2) = j
                         endif
                         if ((Nseed(i,j)==1).and.((midline(l-1,1)==i).or.(midline(l-1,1)==i-1).or.(midline(l-1,1)==i+1)).and.&
                              ((midline(l-1,2)==j).or.(midline(l-1,2)==j-1).or.(midline(l-1,2)==j+1)).and.&
                              (appartient(midline,i,j,l).neqv..true.)) then 
                            midline(l,1) = i
                            boolskel = 1
                            nl=l
                            midline(l,2) = j
                         endif
                      endif
                   enddo
                enddo
                midline(l,3) = nint(zslice)
             enddo
     endif


















     !**
     !! Generating a 2D-map from the midline (for visualisation purpose)
     !**

!     call filtermidline(midline,rhoSlices2)
!     if (.not.((midline(1,1)==midline(2,1)).or.(midline(1,2)==midline(2,2)))) then
!       do l=1,size(midline,1)-1
!         midline(l,1) = midline(l+1,1)
!         midline(l,2) = midline(l+1,2)
!       enddo
!       nl=nl-1
!     endif
!     skel2 = skel
     skel = 0
     do l=1,nl
!     do l=1,size(midline,1)
       skel(midline(l,1),midline(l,2)) = 1
     enddo

     !**
     !! Seventh step: searching the endpoint of the midline (and cutting the rest) TRACKING STEP
     !**
     if (kt==1) then
             ic = -1
             jc = -1
     endif
     !if (kt==1) ic = midline(nl,1)
     !if (kt==1) jc = midline(nl,2)
     write(*,*) "okcut"
     !skel3 = skel2
     !call cutheadskel(midline,skel,skel2,nl,ic,jc)
     call cutheadskel(midline,skel,skel2,nl,ic,jc,kt)
     !skel2 = skel3
     write(*,*) "okcut"

     !**
     !! Heighth step: filling the midline
     !**
     xskelL = midline(1,1)
     yskelL = midline(1,2)
     boolskel=0
     do l=2,size(midline,1)
        do i=2,nx-1
           do j=2,ny-1
              if ((skel(i,j)==1).and.(boolskel==0)) then
                 Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1)&
                      + skel(i+1,j-1)
                 if ((Nseed(i,j)>1).and.((midline(l-1,1)==i).or.(midline(l-1,1)==i-1).or.(midline(l-1,1)==i+1)).and.&
                      ((midline(l-1,2)==j).or.(midline(l-1,2)==j-1).or.(midline(l-1,2)==j+1)).and.(appartient(midline,i,j,l)&
                      .neqv..true.)) then 
                    midline(l,1) = i
                    midline(l,2) = j
                 endif
                 if ((Nseed(i,j)==1).and.((midline(l-1,1)==i).or.(midline(l-1,1)==i-1).or.(midline(l-1,1)==i+1)).and.&
                      ((midline(l-1,2)==j).or.(midline(l-1,2)==j-1).or.(midline(l-1,2)==j+1)).and.(appartient(midline,i,j,l)&
                      .neqv..true.)) then 
                    midline(l,1) = i
                    boolskel = 1
                    nl=l
                    midline(l,2) = j
                 endif
              endif
           enddo
        enddo
     enddo

     !**
     !! Nineth step: smoothing considerably the midline
     !**
     !!FILM
     !if (kt<39+picNum-1) call filtermidline(midline,rhoSlices2)
     !if (kt<40) then
     if (kt<20) then
             call filtermidline(midline,rhoSlices2)
     !call filtermidline(midline,rhoSlices2)

     !**
     !! Tenth step: deleting the first pixel if horizontal or vertical
     !**
     if (.not.((midline(1,1)==midline(2,1)).or.(midline(1,2)==midline(2,2)))) then
     do l=1,size(midline,1)-1
     midline(l,1) = midline(l+1,1)
     midline(l,2) = midline(l+1,2)
     enddo
     nl=nl-1
     endif
     endif
     write(*,*) "CHECK midline : ",midline(1,1)," ",midline(1,2)," ",midline(size(midline,1),1)," ",midline(size(midline,1),2)&
          ," ",size(midline,1)
     write(*,*) "CHECK midline BIS : ",midline(1,1)," ",midline(1,2)," ",midline(nl,1)," ",midline(nl,2)," ",nl
     !call filtermidline(midline,rhoSlices2)
     !skel2 = skel

     !**
     !! Final 2D-map of the midline
     !**
     skel = 0
     do l=1,nl
       skel(midline(l,1),midline(l,2)) = 1
     enddo


     !!        rr = dx
     !!        x1 = xx(midline(nl-10,1))
     !!        y1 = xx(midline(nl-10,2))
     !!        x2 = xx(midline(nl,1))
     !!        y2 = xx(midline(nl,2))
     !!        sinPhi = (x2-x1)/(sqrt((x1-x2)**2+(y1-y2)**2))
     !!        cosPhi = (y2-y1)/(sqrt((x1-x2)**2+(y1-y2)**2)) 
!!!        do while ((rhoSlices(nint(zslice),nint(x2 + rr*sinPhi),nint(y2 + rr*cosPhi))>0._pr).and.(rr<float(ny)))
     !!        do while ((rhoSlices2(nint(x2 + rr*sinPhi),nint(y2 + rr*cosPhi))>0._pr).and.(rr<float(ny)))
     !!           rr=rr+dx
     !!        enddo
     !!        xc = x2 + (rr-dx)*sinPhi +&
     !!sinPhi*rhoSlices2(nint(x2 + (rr-dx)*sinPhi),nint(y2 + (rr-dx)*cosPhi))*dy&
     !!/abs(rhoSlices2(nint(x2 + rr*sinPhi),nint(y2 + rr*cosPhi)) -&
     !!rhoSlices2(nint(x2 + (rr-dx)*sinPhi),nint(y2 + (rr-dx)*cosPhi)))
     !!        yc = y2 + (rr-dx)*cosPhi +&
     !!cosPhi*rhoSlices2(nint(x2 + (rr-dx)*sinPhi),nint(y2 + (rr-dx)*cosPhi))*dy&
     !!/abs(rhoSlices2(nint(x2 + rr*sinPhi),nint(y2 + rr*cosPhi)) -&
     !!rhoSlices2(nint(x2 + (rr-dx)*sinPhi),nint(y2 + (rr-dx)*cosPhi)))
     !!     write(*,*) "NEZ coord : ",xc," ",yc,"    dist old ",disthead," new ",dist(xc,yc,x2,y2)," ",sinPhi," ",cosPhi," ",x1," ",y1&
     !!," ",x2," ",y2


     !**
     !! Definition of the control points of the midline
     !**
     !allocate(points_control(sum(skel),2))
     !allocate(points_control(size(midline,1),3))
     allocate(points_control(nl,3))
     !do l=1,sizeSkel
     do l=1,nl
        points_control(l,1) = xx(midline(l,1))
        points_control(l,2) = yy(midline(l,2))
        points_control(l,3) = zz(nint(zslice))
     enddo
     !!TESTNEZ
     !!     points_control(nl,1) = xc
     !!     points_control(nl,2) = yc
     !!     points_control(nl,3) = zslice

     !**
     !! Spline approximation of the midline
     !**
     points_courbe(1,1) = points_control(1,1)
     points_courbe(1,2) = points_control(1,2)
     points_courbe(1,3) = points_control(1,3)
     tb = dtb
     l = 1
     long = 0._pr
     do while ((tb<1._pr).and.(l+1<Ns))
        l = l+1
        !        call pointsBezierN(points_control,tb,px,py)
        call pointsBezierN3D(points_control,tb,px,py,pz)
        points_courbe(l,1) = px
        points_courbe(l,2) = py
        points_courbe(l,3) = pz
        long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))**2 + (points_courbe(l,2)-points_courbe(l-1,2))**2 +&
             (points_courbe(l,3)-points_courbe(l-1,3))**2)
        tb = tb+dtb
     enddo
     if (l==Ns-1) then
        l=l+1
        write(*,*) " ET VOILA"
        points_courbe(size(points_courbe_equal,1),1) = points_control(size(points_control,1),1)
        points_courbe(size(points_courbe_equal,1),2) = points_control(size(points_control,1),2)
        points_courbe(size(points_courbe_equal,1),3) = points_control(size(points_control,1),3)
        long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))**2 + (points_courbe(l,2)-points_courbe(l-1,2))**2 +&
             (points_courbe(l,3)-points_courbe(l-1,3))**2)
     endif
     if (.not.(l==Ns)) write(*,*) "WARNING1  p ",l
     long2 = long
     write(*,*) "POINTS_COURBE_EQUAL  L==1  ",points_control(1,1)," ",points_control(1,2)
     write(*,*) "POINTS_COURBE_EQUAL  L==Ns  ",points_control(size(points_control,1),1)," ",points_control(size(points_control,1),2)
!     if (kt==1) long00 = long2

     !**
     !! Tail extrapolation 
     !**
     if (long00>long2) then
     !!x1 = points_courbe(1,1)
     !!y1 = points_courbe(1,2)
     !!x1 = points_courbe(nint(0.02*Ns),1)
     !!y1 = points_courbe(nint(0.02*Ns),2)
     !!x2 = points_courbe(nint(0.03*Ns),1)
     !!y2 = points_courbe(nint(0.03*Ns),2)
     !!x2 = points_courbe(nint(0.04*Ns),1)
     !!y2 = points_courbe(nint(0.04*Ns),2)
     !if (kt>80) then
     !        l=nint(0.1*nl)-1
     !else
     !        l=1
             l=5
     !endif
     x1=0.0
     y1=0.0
     do i=1,l!1!2!nint(0.01*nl)-1 !!0.1
       x1 = x1+points_control(i,1)
       y1 = y1+points_control(i,2)
     enddo
     x1=x1*1.0/l
     y1=y1*1.0/l
     do i=1,1
     !if (kt>80) then
     !x2 = points_control(nint(0.1*nl),1)
     !y2 = points_control(nint(0.1*nl),2)
     !else
     !x2 = points_control(2,1)!nint(0.01*nl),1)
     !y2 = points_control(2,2)!nint(0.01*nl),2)
     x2 = points_control(6,1)!nint(0.01*nl),1)
     y2 = points_control(6,2)!nint(0.01*nl),2)
     !endif
     !rt = abs(long00-long2)
!     rt = abs(long00-long2) + sqrt((points_control(i,1)-points_control(1,1))**2 + (points_control(i,2)-points_control(1,2))**2)
     rt = abs(long00-long2) + sqrt((x1-points_control(1,1))**2 + (y1-points_control(1,2))**2)
     !if (abs(x1-x2)<eepsilon*dx) then
     if (abs(x1-x2)/dx<eepsilon) then
        xt = x1
        !if (rt**2<(y2-y1-rt)**2) yt = y1+rt
        !if (rt**2<(y2-y1+rt)**2) yt = y1-rt
        yt = y1+rt

        if (((y2-y1)*(yt-y1))>0._pr) then
          write(*,*) "JEPASSEICIAUSSI"
          yt = y1-rt
        endif
     else
        xt = rt/(sqrt(1+((y2-y1)**2)/((x2-x1)**2))) + x1
        yt = (y2-y1)/(x2-x1)*(xt-x1) + y1
        !if (.not.(rt**2<(x2-xt)**2+(y2-yt)**2)) then
        !   xt = -rt/(sqrt(1+((y2-y1)**2)/((x2-x1)**2))) + x1
        !   yt = (y2-y1)/(x2-x1)*(xt-x1) + y1
        !endif

        if (((x2-x1)*(xt-x1)+(y2-y1)*(yt-y1))>0._pr) then
          write(*,*) "JEPASSEICI"
          xt = -rt/(sqrt(1+((y2-y1)**2)/((x2-x1)**2))) + x1
          yt = (y2-y1)/(x2-x1)*(xt-x1) + y1
        endif
     endif
     write(*,*) "CHECKCJECKi  ",i,"    ",x1," ",y1," ",xt," ",yt,"  RT  ",rt," ",long00," ",long2&
          ,"  PS  ",(x2-x1)*(xt-x1)+(y2-y1)*(yt-y1)
     if (i==1) points_control(1,1) = 0._pr
     if (i==1) points_control(1,2) = 0._pr
     points_control(1,1) = points_control(1,1)+xt
     points_control(1,2) = points_control(1,2)+yt
     enddo
     !if (kt>80) then
     !points_control(1,1) = points_control(1,1)/(nint(0.1*nl)-1._pr)
     !points_control(1,2) = points_control(1,2)/(nint(0.1*nl)-1._pr)
     !endif
     endif

     write(*,*) "POINTS_COURBE_EQUAL  L==1  ",points_control(1,1)," ",points_control(1,2)
     points_control(1,3) = zz(nint(zslice))
     points_courbe(1,1) = points_control(1,1)
     points_courbe(1,2) = points_control(1,2)
     points_courbe(1,3) = zz(nint(zslice))


     tb = dtb
     l = 1
     long = 0._pr
     do while ((tb<1._pr).and.(l+1<Ns+1))
        l = l+1
        call pointsBezierN3D(points_control,tb,px,py,pz)
        points_courbe(l,1) = px
        points_courbe(l,2) = py
        points_courbe(l,3) = pz
        long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))**2 + (points_courbe(l,2)-points_courbe(l-1,2))**2 +&
             (points_courbe(l,3)-points_courbe(l-1,3))**2)
        tb = tb+dtb
     enddo
     if (l==Ns-1) then
        write(*,*) " ET VOILA"
        l=l+1
        points_courbe(size(points_courbe_equal,1),1) = points_control(size(points_control,1),1)
        points_courbe(size(points_courbe_equal,1),2) = points_control(size(points_control,1),2)
        points_courbe(size(points_courbe_equal,1),3) = points_control(size(points_control,1),3)
        long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))**2 + (points_courbe(l,2)-points_courbe(l-1,2))**2 +&
             (points_courbe(l,3)-points_courbe(l-1,3))**2)
     endif
     if (.not.(l==Ns)) write(*,*) "WARNING2  p ",l
     !    write(*,*) "IMPORTANT  ",x1," ",y1," ",x2," ",y2," ",xt," ",yt," ",dist(x1,y1,xt,yt)," ",dist(x2,y2,xt,yt)," ",delta," ",rt&
     !," ",rt**2
     write(*,*) "IMPORTANT  ",long00," ",long2," ",long,"  RT ",rt," ",sqrt((x1-xt)**2+(y1-yt)**2)&
          ,"  coord ",x1," ",y1," ",x2," ",y2
     long2 = long

     !**
     !! Tail extrapolation 
     !**
     l=1
     do i=1,l!1!2!nint(0.01*nl)-1 !!0.1
     x1 = points_control(i,1)
     y1 = points_control(i,2)
     x2 = points_control(2,1)!nint(0.01*nl),1)
     y2 = points_control(2,2)!nint(0.01*nl),2)
     rt = abs(long00-long2)
     !if (abs(x1-x2)<eepsilon*dx) then
     if (abs(x1-x2)/dx<eepsilon) then
        xt = x1
        !if (rt**2<(y2-y1-rt)**2) yt = y1+rt
        !if (rt**2<(y2-y1+rt)**2) yt = y1-rt
        yt = y1+rt

        if (((y2-y1)*(yt-y1))>0._pr) then
          write(*,*) "JEPASSEICIAUSSI"
          yt = y1-rt
        endif
     else
        xt = rt/(sqrt(1+((y2-y1)**2)/((x2-x1)**2))) + x1
        yt = (y2-y1)/(x2-x1)*(xt-x1) + y1
        !if (.not.(rt**2<(x2-xt)**2+(y2-yt)**2)) then
        !   xt = -rt/(sqrt(1+((y2-y1)**2)/((x2-x1)**2))) + x1
        !   yt = (y2-y1)/(x2-x1)*(xt-x1) + y1
        !endif

        if (((x2-x1)*(xt-x1)+(y2-y1)*(yt-y1))>0._pr) then
          write(*,*) "JEPASSEICI"
          xt = -rt/(sqrt(1+((y2-y1)**2)/((x2-x1)**2))) + x1
          yt = (y2-y1)/(x2-x1)*(xt-x1) + y1
        endif
     endif
     write(*,*) "CHECKCJECKi  ",i,"    ",x1," ",y1," ",xt," ",yt,"  RT  ",rt," ",long00," ",long2&
          ,"  PS  ",(x2-x1)*(xt-x1)+(y2-y1)*(yt-y1)
     if (i==1) points_control(1,1) = 0._pr
     if (i==1) points_control(1,2) = 0._pr
     points_control(1,1) = points_control(1,1)+xt
     points_control(1,2) = points_control(1,2)+yt
     enddo
     points_control(1,3) = zz(nint(zslice))
     points_courbe(1,1) = points_control(1,1)
     points_courbe(1,2) = points_control(1,2)
     points_courbe(1,3) = zz(nint(zslice))

     tb = dtb
     l = 1
     long = 0._pr
     do while ((tb<1._pr).and.(l+1<Ns+1))
        l = l+1
        call pointsBezierN3D(points_control,tb,px,py,pz)
        points_courbe(l,1) = px
        points_courbe(l,2) = py
        points_courbe(l,3) = pz
        long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))**2 + (points_courbe(l,2)-points_courbe(l-1,2))**2 +&
             (points_courbe(l,3)-points_courbe(l-1,3))**2)
        tb = tb+dtb
     enddo
     if (l==Ns-1) then
        l=l+1
        points_courbe(size(points_courbe_equal,1),1) = points_control(size(points_control,1),1)
        points_courbe(size(points_courbe_equal,1),2) = points_control(size(points_control,1),2)
        points_courbe(size(points_courbe_equal,1),3) = points_control(size(points_control,1),3)
        long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))**2 + (points_courbe(l,2)-points_courbe(l-1,2))**2 +&
             (points_courbe(l,3)-points_courbe(l-1,3))**2)
     endif
     if (.not.(l==Ns)) write(*,*) "WARNING2  p ",l
     write(*,*) "IMPORTANTBIS  ",long00," ",long2," ",long,"  RT ",rt," ",sqrt((x1-xt)**2+(y1-yt)**2)&
          ,"  coord ",x1," ",y1," ",x2," ",y2


     !**
     !! Uniform spline approximation 
     !**
     long2 = long
     ds = long/(Ns-1)
     !ds = area*long2/(meshRatio*Ns-1)
     points_courbe_equal =0._pr
     points_courbe_equal(1,1) = points_control(1,1)
     points_courbe_equal(1,2) = points_control(1,2)
     points_courbe_equal(1,3) = points_control(1,3)
     l = 1
     long = 0._pr
     tb = 0._pr
     do while ((tb<1._pr).and.(l+1<Ns)) !+1))
     !do while ((tb<1._pr).and.(l+1<meshRatio*Ns+1)) !+1))
        l = l+1
        nt = 1
        s = 0._pr
        do while ((l-1)*ds-s>0._pr) 
           nt = nt+1
           s = s + sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)-points_courbe(nt-1,2))**2 +&
                (points_courbe(nt,3)-points_courbe(nt-1,3))**2)
        enddo
        tbm = (nt-2)*dtb
        tbp = (nt-1)*dtb
        tb = tbm
        s0 = s
        sinit = s - sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)-points_courbe(nt-1,2))**2 +&
             (points_courbe(nt,3)-points_courbe(nt-1,3))**2)
        s = sinit
        bool = 0
        !do while ((abs((l-1)*ds-s)>eepsilon*dx).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
        do while ((abs((l-1)*ds-s)/dx>eepsilon).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
           tb = (tbm + tbp)*0.5_pr
           !           call pointsBezierN(points_control,tb,px,py)
           call pointsBezierN3D(points_control,tb,px,py,pz)
           s = sinit + sqrt((px-points_courbe(nt-1,1))**2 + (py-points_courbe(nt-1,2))**2 + (pz-points_courbe(nt-1,3))**2)
           if ((l-1)*ds-s>0._pr) then
              tbm = tb
           else
              tbp = tb
           endif
           if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
              bool = 1
           endif
        enddo
        !        call pointsBezierN(points_control,tb,px,py)
        call pointsBezierN3D(points_control,tb,px,py,pz)
        points_courbe_equal(l,1) = px
        points_courbe_equal(l,2) = py
        points_courbe_equal(l,3) = pz
        long = long +&
             sqrt((points_courbe_equal(l,1)-points_courbe_equal(l-1,1))**2 +&
             (points_courbe_equal(l,2)-points_courbe_equal(l-1,2))**2 +&
             (points_courbe_equal(l,3)-points_courbe_equal(l-1,3))**2)
     enddo
!     lp = l
!     !ds = 0.9*long2/(0.8*Ns-1)
!     !ds = 0.9*long2/(Ns-lp)
!     ds = (1._pr-2._pr*area)*long2/((1._pr-meshRatio)*Ns-lp)
!     tp = tb
!     tb = 0._pr !dtb
!     l = lp !1
!     !do while ((tb<1._pr).and.(l+1<Ns+1))
!     do while ((tb<1._pr).and.(l+1<(1._pr-meshRatio)*Ns+1))
!        l = l+1
!        nt = 1
!        s = 0._pr
!        !do while ((lp-1)*0.1*long2/(0.2*Ns-1)+(l-lp-1)*ds-s>0._pr) 
!        do while ((lp-1)*area*long2/(meshRatio*Ns-1)+(l-lp)*ds-s>0._pr) 
!           nt = nt+1
!           s = s + sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)&
!                -points_courbe(nt-1,2))**2 + (points_courbe(nt-1,3)-points_courbe(nt-1,3))**2)
!        enddo
!        tbm = (nt-2)*dtb!-tp+dtb
!        tbp = (nt-1)*dtb!-tp+dtb
!        tb = tbm !(tbm + tbp)*0.5_pr
!        s0 = s
!        sinit = s - sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)&
!             -points_courbe(nt-1,2))**2 + (points_courbe(nt,3)-points_courbe(nt-1,3))**2)
!        s = sinit
!        bool = 0
!        !do while ((abs((lp-1)*0.1*long2/(0.2*Ns-1)+(l-lp-1)*ds-s)>eepsilon).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
!        do while ((abs((lp-1)*area*long2/(meshRatio*Ns-1)+(l-lp)*ds-s)>eepsilon).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
!           tb = (tbm + tbp)*0.5_pr
!           call pointsBezierN3D(points_control,tb,px,py,pz)
!           s = sinit + sqrt((px-points_courbe(nt-1,1))**2 + (py-points_courbe(nt-1,2))**2&
!                + (pz-points_courbe(nt-1,3))**2)
!           !if ((lp-1)*0.1*long2/(0.2*Ns-1)+(l-lp-1)*ds-s>0._pr) then
!           if ((lp-1)*area*long2/(meshRatio*Ns-1)+(l-lp)*ds-s>0._pr) then
!              tbm = tb
!           else
!              tbp = tb
!           endif
!           if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
!              bool = 1
!           endif
!        enddo
!        call pointsBezierN3D(points_control,tb,px,py,pz)
!        points_courbe_equal(l,1) = px
!        points_courbe_equal(l,2) = py
!        points_courbe_equal(l,3) = pz
!        long = long +&
!             sqrt((points_courbe_equal(l,1)-points_courbe_equal(l-1,1))**2 +&
!             (points_courbe_equal(l,2)-points_courbe_equal(l-1,2))**2 +&
!             (points_courbe_equal(l,3)-points_courbe_equal(l-1,3))**2)
!     enddo
!     lpt = l
!     ds = area*long2/(Ns-lpt)
!     tp = tb
!     tb = 0._pr !dtb
!     l = lpt !1
!     do while ((tb<1._pr).and.(l+1<Ns))
!        l = l+1
!        nt = 1
!        s = 0._pr
!        do while ((lp-1)*area*long2/(meshRatio*Ns-1)+(lpt-lp)*(1._pr-2._pr*area)*long2/((1._pr-meshRatio)*Ns-lp)+(l-lpt)*ds-s>0._pr) 
!           nt = nt+1
!           s = s + sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)&
!                -points_courbe(nt-1,2))**2 + (points_courbe(nt-1,3)-points_courbe(nt-1,3))**2)
!        enddo
!        tbm = (nt-2)*dtb!-tp+dtb
!        tbp = (nt-1)*dtb!-tp+dtb
!        tb = tbm !(tbm + tbp)*0.5_pr
!        s0 = s
!        sinit = s - sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)&
!             -points_courbe(nt-1,2))**2 + (points_courbe(nt,3)-points_courbe(nt-1,3))**2)
!        s = sinit
!        bool = 0
!        do while ((abs((lp-1)*area*long2/(meshRatio*Ns-1)+(lpt-lp)*(1._pr-2._pr*area)*long2/((1._pr-meshRatio)*Ns-lp)&
!             +(l-lpt)*ds-s)>eepsilon).and.(bool==0)) !.and.((tbm+tbp)*0.5_pr<1._pr))
!           tb = (tbm + tbp)*0.5_pr
!           call pointsBezierN3D(points_control,tb,px,py,pz)
!           s = sinit + sqrt((px-points_courbe(nt-1,1))**2 + (py-points_courbe(nt-1,2))**2&
!                + (pz-points_courbe(nt-1,3))**2)
!           if ((lp-1)*area*long2/(meshRatio*Ns-1)+(lpt-lp)*(1._pr-2._pr*area)*long2/((1._pr-meshRatio)*Ns-lp)+(l-lpt)*ds-s>0._pr)&
!                then
!              tbm = tb
!           else
!              tbp = tb
!           endif
!           if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
!              bool = 1
!           endif
!        enddo
!        call pointsBezierN3D(points_control,tb,px,py,pz)
!        points_courbe_equal(l,1) = px
!        points_courbe_equal(l,2) = py
!        points_courbe_equal(l,3) = pz
!        long = long +&
!             sqrt((points_courbe_equal(l,1)-points_courbe_equal(l-1,1))**2 +&
!             (points_courbe_equal(l,2)-points_courbe_equal(l-1,2))**2 +&
!             (points_courbe_equal(l,3)-points_courbe_equal(l-1,3))**2)
!     enddo
     if (l==Ns-1) then
        l = l+1
        write(*,*) " ET VOILA j y passe  "
        points_courbe_equal(l,1) = points_control(size(points_control,1),1)
        points_courbe_equal(l,2) = points_control(size(points_control,1),2)
        points_courbe_equal(l,3) = points_control(size(points_control,1),3)
        long = long +&
             sqrt((points_courbe_equal(l,1)-points_courbe_equal(l-1,1))**2 +&
             (points_courbe_equal(l,2)-points_courbe_equal(l-1,2))**2 +&
             (points_courbe_equal(l,3)-points_courbe_equal(l-1,3))**2)
     endif
     if (.not.(l==Ns)) write(*,*) "WARNING  s ",l
     write(*,*) "CHECKPOINT 1 ",long2," ",long," ",l," ",tb," POINTS  ",points_courbe_equal(Ns,1)," ",points_courbe_equal(Ns,2)&
          ," ",points_courbe(Ns,1)," ",points_courbe(Ns,2)," ",points_control(size(points_control,1),1)&
          ," ",points_control(size(points_control,1),2)
     do l=nint(0.04*Ns),1,-1
        x2 = points_courbe_equal(Ns-l-1,1)
        y2 = points_courbe_equal(Ns-l-1,2)
        x1 = points_courbe_equal(Ns-l,1)
        y1 = points_courbe_equal(Ns-l,2)
        !! RTMODIF
        rt = dist(x1,y1,x2,y2)
        !if (abs(x1-x2)<eepsilon*dx) then
        if (abs(x1-x2)/dx<eepsilon) then
           xt = x1
           !if (rt**2<(y2-y1-rt)**2) yt = y1+rt
           !if (rt**2<(y2-y1+rt)**2) yt = y1-rt
           yt = y1+rt

           if (((y2-y1)*(yt-y1))>0._pr) then
             yt = y1-rt
           endif
        else
           xt = rt/(sqrt(1+((y2-y1)**2)/((x2-x1)**2))) + x1
           yt = (y2-y1)/(x2-x1)*(xt-x1) + y1
           !if (.not.(rt**2<(x2-xt)**2+(y2-yt)**2)) then
           !   xt = -rt/(sqrt(1+((y2-y1)**2)/((x2-x1)**2))) + x1
           !   yt = (y2-y1)/(x2-x1)*(xt-x1) + y1
           !endif

           if (((x2-x1)*(xt-x1)+(y2-y1)*(yt-y1))>0._pr) then
             xt = -rt/(sqrt(1+((y2-y1)**2)/((x2-x1)**2))) + x1
             yt = (y2-y1)/(x2-x1)*(xt-x1) + y1
           endif
        endif
        points_courbe_equal(size(points_courbe_equal,1)-l+1,1) = xt
        points_courbe_equal(size(points_courbe_equal,1)-l+1,2) = yt
        points_courbe_equal(size(points_courbe_equal,1)-l+1,3) = zz(nint(zslice))
     enddo

     !call body_masscenter(points_courbe_equal,xg,yg)
     !call body_masscentering(points_courbe_equal,xg,yg,xgref,ygref)
     !call body_masscenter(points_courbe_equal,xg,yg)
     !call body_rotationdefPS(points_courbe_equal_ref,points_courbe_equal,xg,yg,alphadef,dx)

     !**
     !! Head and Tail linear approximations 
     !**
     dti = dsi/ds !distslice(1+Ni,1)/(Ni-1)/dsi
     do l=2,Ni
        tb = - (l-1)*dti
        !        call courbeBezierN(points_courbe_equal,tb,Px,Py)
        call courbeBezierN3D(points_courbe_equal,tb,Px,Py,Pz)
        tail_courbe(l,1) = Px
        tail_courbe(l,2) = Py
        tail_courbe(l,3) = Pz
     enddo
     tail_courbe(1,1) = points_courbe_equal(1,1) !points_control(1,1)
     tail_courbe(1,2) = points_courbe_equal(1,2) !points_control(1,2)
     tail_courbe(1,3) = points_courbe_equal(1,3) !points_control(1,2)
     dtf = dsf/ds !*90) !dist(points_control(size(points_control,1),1),points_control(size(points_control,1),2),points_courbe(size(points_courbe,1)-1,1),points_courbe(size(points_courbe,1)-1,2)) !ds!/(100)!*ds)!*100 !distslice(Ns+Ni,2)/(Nf-1)/dsf !(10*ds)
     do l=2,Nf
        tb = 1._pr + (l-1)*dtf
        !        call courbeBezierN(points_courbe_equal,tb,Px,Py)
        call courbeBezierN3D(points_courbe_equal,tb,Px,Py,Pz)
        head_courbe(l,1) = Px
        head_courbe(l,2) = Py
        head_courbe(l,3) = Pz
     enddo
     head_courbe(1,1) = points_courbe_equal(Ns,1) !points_control(size(points_control,1),1) !points_courbe_equal(Ns,1) !points_control(Ns,1)
     head_courbe(1,2) = points_courbe_equal(Ns,2) !points_control(size(points_control,1),2) !points_courbe_equal(Ns,2) !points_control(Ns,2)
     head_courbe(1,3) = points_courbe_equal(Ns,3) !points_control(size(points_control,1),2) !points_courbe_equal(Ns,2) !points_control(Ns,2)
     long3 = 0._pr
     !do l=1,sum(skel)-1
     do l=1,nl-1
        long3 = long3 +sqrt((xx(midline(l+1,1))-xx(midline(l,1)))**2 + (yy(midline(l+1,2))-yy(midline(l,2)))**2)
     enddo

     !write(*,*) "Longlong : ",long," ",long3," ",sum(skel)," ",sizeSkel
     write(*,*) "Longlong : ",long," ",long3," ",nl," ",sizeSkel,"          "&
     ,dsi," ",dsf," ",ds," ",long," ",(Nf-1)*dsf," ",disthead,"     ",Px-head_courbe(1,1)," "&
     ,points_courbe_equal(Ns,1)-points_courbe_equal(Ns-1,1)




     !**
     !! Shape reconstruction (Lagrangian markers) based on the deformed midline: each transverse slice remains orthogonal to the midline
     !**

     l=1
     indextab = 0
     !     do theta=1,nbtheta
     !        thetatab(theta) = 1
!!!        indextab(theta,thetatab(theta)) = 1
     !        indextab(theta,1) = 1
     !        slice(theta,1,1) = tail_courbe(Ni,1)
     !        slice(theta,1,2) = tail_courbe(Ni,2)
     !        slice(theta,1,3) = tail_courbe(Ni,3)
     !        slice2(theta,1,1) = slice(theta,1,1)
     !        slice2(theta,1,2) = slice(theta,1,2)
     !        slice2(theta,1,3) = slice(theta,1,3)
     !     enddo
     do l=1,Ni-1
        if (l==1) then
           !call intersection(tail_courbe(Ni-l+1,1),tail_courbe(Ni-l+1-1,1),tail_courbe(Ni-l+1,2),tail_courbe(Ni-l+1-1,2)&
           !     ,tail_courbe(Ni-l+1,1),tail_courbe(Ni-l+1,2),distslice(l,1),distslice(l,2)&
           !     ,distslice(Ns+Ni+Nf+l,1),distslice(Ns+Ni+Nf+l,2)&
           !     ,xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
           call intersection(tail_courbe(Ni,1),tail_courbe(Ni-2,1),tail_courbe(Ni,2),tail_courbe(Ni-2,2)&
                ,tail_courbe(Ni-1,1),tail_courbe(Ni-1,2),distslice(2,1),distslice(2,2)&
                ,distslice(Ns+Ni+Nf+2,1),distslice(Ns+Ni+Nf+2,2)&
                ,xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar)
        else
           call intersection(tail_courbe(Ni-l+1+1,1),tail_courbe(Ni-l+1-1,1),tail_courbe(Ni-l+1+1,2),tail_courbe(Ni-l+1-1,2)&
                ,tail_courbe(Ni-l+1,1),tail_courbe(Ni-l+1,2),distslice(l,1),distslice(l,2)&
                ,distslice(Ns+Ni+Nf+l,1),distslice(Ns+Ni+Nf+l,2)&
                ,xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
        endif
        if (l==1) then
           sinPhi = (xTheta(1)-tail_courbe(Ni-1,1))/(&
                sqrt((xTheta(1)-tail_courbe(Ni-1,1))**2+(yTheta(1)-tail_courbe(Ni-1,2))**2))
           cosPhi = (yTheta(1)-tail_courbe(Ni-1,2))/(&
                sqrt((xTheta(1)-tail_courbe(Ni-1,1))**2+(yTheta(1)-tail_courbe(Ni-1,2))**2))
        else
           sinPhi = (xTheta(1)-tail_courbe(Ni-l+1,1))/(&
                sqrt((xTheta(1)-tail_courbe(Ni-l+1,1))**2+(yTheta(1)-tail_courbe(Ni-l+1,2))**2))
           cosPhi = (yTheta(1)-tail_courbe(Ni-l+1,2))/(&
                sqrt((xTheta(1)-tail_courbe(Ni-l+1,1))**2+(yTheta(1)-tail_courbe(Ni-l+1,2))**2)) 
        endif
        do theta=1,nbtheta
           !           slice(theta,l,1) = tail_courbe(Ni-l+1,1) + valDist(l,theta)*cos(valTheta(l,theta))*sinPhi
           !           slice(theta,l,2) = tail_courbe(Ni-l+1,2) + valDist(l,theta)*cos(valTheta(l,theta))*cosPhi
           !           slice(theta,l,3) = zslice + valDist(l,theta)*sin(valTheta(l,theta))
           slice(theta,l,1) = tail_courbe(Ni-l+1,1) + valDist(l,theta)*cosTheta_tab(l,theta)*sinPhi
           slice(theta,l,2) = tail_courbe(Ni-l+1,2) + valDist(l,theta)*cosTheta_tab(l,theta)*cosPhi
           slice(theta,l,3) = zz(nint(zslice)) + valDist(l,theta)*sinTheta_tab(l,theta)
        enddo

        do theta=1,nbtheta
           if (l==1) then
              thetatab(theta) = 1
           else
              thetatab(theta) = thetatab(theta) + 1
           endif
           !!           indextab(theta,thetatab(theta)) = l
           indextab(theta,l) = 1
           !!           indextab(theta,l) = indextab(theta,l) +1
           slice2(theta,thetatab(theta),1) = slice(theta,l,1)
           slice2(theta,thetatab(theta),2) = slice(theta,l,2)
           slice2(theta,thetatab(theta),3) = slice(theta,l,3)
        enddo
!!!        slice(1,l,1) = xTheta(1)
!!!        slice(1,l,2) = yTheta(1)
!!!        if (errorl==1) then
!!!           slice(1,l,1) = slice(1,l-1,1)
!!!           slice(1,l,2) = slice(1,l-1,2)
!!!        endif
!!!        if ((abs(slice(1,l,1)-slice(1,l-1,1))<1.e-2).and.(abs(slice(1,l,2)-slice(1,l-1,2))<1.e-2)) then
!!!           slice(2,l,1) = slice(2,l-1,1)
!!!           slice(2,l,2) = slice(2,l-1,2)
!!!        else
!!!           slice(2,l,1) = -slice(1,l,1) + 2*tail_courbe(Ni-l+1,1)
!!!           slice(2,l,2) = -slice(1,l,2) + 2*tail_courbe(Ni-l+1,2)
!!!        endif
!!!        do theta=1,nbtheta
!!!            slice(theta,l,1) = slice(theta,l-1,1)
!!!            slice(theta,l,2) = slice(theta,l-1,2)
!!!            slice(theta,l,3) = slice(theta,l-1,3)
!!!        enddo
        !        if (det(tail_courbe(Ni-l+1+1,1),tail_courbe(Ni-l+1+1,2),slice2(lpli,1,1),slice2(lpli,1,2),tail_courbe(Ni-l+1,1),tail_courbe(Ni-l+1,2))*det(tail_courbe(Ni-l+1+1,1),tail_courbe(Ni-l+1+1,2),slice2(lpli,1,1),slice2(lpli,1,2),xLeft,yLeft)>= 0._pr) then
!!!        do theta=1,nbtheta
!!!           thetatab(theta) = thetatab(theta) + 1
!!!        enddo
!!!        slice2(1,thetatab(1),1) = xTheta(1)
!!!        slice2(1,thetatab(1),2) = yTheta(1)
!!!        slice2(2,thetatab(2),1) = slice(2,l,1)
!!!        slice2(2,thetatab(2),2) = slice(2,l,2)
!!!        do theta=2,nbtheta
!!!           slice2(theta,thetatab(2),1) = slice(theta,l,1)
!!!           slice2(theta,thetatab(2),2) = slice(theta,l,2)
!!!           slice2(theta,thetatab(2),3) = slice(theta,l,3)
!!!        enddo
        !        endif
!!!        if (errorl==1) then
!!!           write(*,*) "blabla 1",l
!!!           slice2(1,thetatab(1),1) = slice2(1,thetatab(1)-1,1)
!!!           slice2(1,thetatab(1),2) = slice2(1,thetatab(1)-1,2)
!!!           slice2(1,thetatab(1),3) = slice2(1,thetatab(1)-1,3)
!!!        endif
!!!        if (errorr==1) then
!!!           write(*,*) "blabla 2",l
!!!           slice2(2,thetatab(2),1) = slice2(2,thetatab(2)-1,1)
!!!           slice2(2,thetatab(2),2) = slice2(2,thetatab(2)-1,2)
!!!           slice2(2,thetatab(2),3) = slice2(2,thetatab(2)-1,3)
!!!        endif
        if ((l>1).and.((errorl==1).or.(errorr==1))) then
           do theta=1,nbtheta
              slice(theta,l,1) = slice(theta,l-1,1)
              slice(theta,l,2) = slice(theta,l-1,2)
              slice(theta,l,3) = slice(theta,l-1,3)
              slice2(theta,thetatab(theta),1) = slice2(theta,thetatab(theta)-1,1)
              slice2(theta,thetatab(theta),2) = slice2(theta,thetatab(theta)-1,2)
              slice2(theta,thetatab(theta),3) = slice2(theta,thetatab(theta)-1,3)
           enddo
           write(*,*) "error initialsegment : ",theta," ",l," ",errorl," ",errorr
        endif


        !if (errorl==1) write(*,*) "WARNING : ",errorl," ",l
     enddo
     write(*,*) "VOILA VOILA"
     call intersection(tail_courbe(2,1),points_courbe_equal(2,1),tail_courbe(2,2),points_courbe_equal(2,2),points_courbe_equal(1,1)&
          ,points_courbe_equal(1,2),distslice(Ni,1),distslice(Ni+1,2),distslice(Ns+Ni+Nf+Ni+1,1),distslice(Ns+Ni+Nf+Ni+1,2)&
          ,xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
     l = Ni
     sinPhi = (xTheta(1)-points_courbe_equal(1,1))/(&
          sqrt((xTheta(1)-points_courbe_equal(1,1))**2+(yTheta(1)-points_courbe_equal(1,2))**2))
     cosPhi = (yTheta(1)-points_courbe_equal(1,2))/(&
          sqrt((xTheta(1)-points_courbe_equal(1,1))**2+(yTheta(1)-points_courbe_equal(1,2))**2)) 
!!!     slice(1,l,1) = points_courbe_equal(1,1) + valDist(l,1)*cos(valTheta(l,1))*sinPhi
!!!     slice(1,l,2) = points_courbe_equal(1,2) + valDist(l,1)*cos(valTheta(l,1))*cosPhi
!!!     slice(2,l,1) = points_courbe_equal(1,1) + valDist(l,2)*cos(valTheta(l,2))*sinPhi 
!!!     slice(2,l,2) = points_courbe_equal(1,2) + valDist(l,2)*cos(valTheta(l,2))*cosPhi 
     do theta=1,nbtheta
        !        slice(theta,l,1) = points_courbe_equal(1,1) + valDist(l,theta)*cos(valTheta(l,theta))*sinPhi
        !        slice(theta,l,2) = points_courbe_equal(1,2) + valDist(l,theta)*cos(valTheta(l,theta))*cosPhi
        !        slice(theta,l,3) = zslice + valDist(l,theta)*sin(valTheta(l,theta))
        slice(theta,l,1) = points_courbe_equal(1,1) + valDist(l,theta)*cosTheta_tab(l,theta)*sinPhi
        slice(theta,l,2) = points_courbe_equal(1,2) + valDist(l,theta)*cosTheta_tab(l,theta)*cosPhi
        slice(theta,l,3) = zz(nint(zslice)) + valDist(l,theta)*sinTheta_tab(l,theta)
     enddo

!!!     if ((det(tail_courbe(2,1),tail_courbe(2,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),points_courbe_equal(1,1),&
!!!points_courbe_equal(1,2))*det(tail_courbe(2,1),tail_courbe(2,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),slice(1,l,1),&
!!!slice(1,l,2))>= 0._pr).or.(thetatab(1)<Ni)) then
!!!        thetatab(1) = thetatab(1) + 1
!!!        slice2(1,thetatab(1),1) = slice(1,l,1)
!!!        slice2(1,thetatab(1),2) = slice(1,l,2)
!!!     endif
     do theta=1,nbtheta
        if ((det(tail_courbe(2,1),tail_courbe(2,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),&
             points_courbe_equal(1,1),points_courbe_equal(1,2))*det(tail_courbe(2,1),tail_courbe(2,2)&
             ,slice2(theta,thetatab(theta),1),&
             slice2(theta,thetatab(theta),2),slice(theta,l,1),slice(theta,l,2))>= 0._pr).or.(thetatab(theta)<Ni)) then
           thetatab(theta) = thetatab(theta) + 1
           !!           indextab(theta,thetatab(theta)) = l
           indextab(theta,l) = 1
           slice2(theta,thetatab(theta),1) = slice(theta,l,1)
           slice2(theta,thetatab(theta),2) = slice(theta,l,2)
           slice2(theta,thetatab(theta),3) = slice(theta,l,3)
        endif
     enddo
     if (errorl==1) write(*,*) "WARNING : ",errorl," ",l
     do l=2,Ns-1
        sinPhir = sinPhi
        cosPhir = cosPhi
        call intersection(points_courbe_equal(l-1,1),points_courbe_equal(l+1,1),points_courbe_equal(l-1,2),&
             points_courbe_equal(l+1,2),points_courbe_equal(l,1),points_courbe_equal(l,2),distslice(l+Ni,1),distslice(l+Ni,2),&
             distslice(Ns+Ni+Nf+l+Ni,1),distslice(Ns+Ni+Nf+l+Ni,2),xTheta(1),yTheta(1)&
             ,xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
        sinPhi = (xTheta(1)-points_courbe_equal(l,1))/(&
             sqrt((xTheta(1)-points_courbe_equal(l,1))**2+(yTheta(1)-points_courbe_equal(l,2))**2))
        cosPhi = (yTheta(1)-points_courbe_equal(l,2))/(&
             sqrt((xTheta(1)-points_courbe_equal(l,1))**2+(yTheta(1)-points_courbe_equal(l,2))**2)) 
        sinAlpha = sinPhi
        cosAlpha = cosPhi
        sinPhil = sinPhi
        cosPhil = cosPhi
        !!        slice(1,l+Ni-1,1) = points_courbe_equal(l,1) + valDist(l+Ni-1,1)*cos(valTheta(l+Ni-1,1))*sinPhi
        !!        slice(1,l+Ni-1,2) = points_courbe_equal(l,2) + valDist(l+Ni-1,1)*cos(valTheta(l+Ni-1,1))*cosPhi
        !!        slice(1,l+Ni-1,3) = zslice + valDist(l+Ni-1,1)*sin(valTheta(l+Ni-1,1))
        !!        slice(2,l+Ni-1,1) = points_courbe_equal(l,1) + valDist(l+Ni-1,2)*cos(valTheta(l+Ni-1,2))*sinPhi 
        !!        slice(2,l+Ni-1,2) = points_courbe_equal(l,2) + valDist(l+Ni-1,2)*cos(valTheta(l+Ni-1,2))*cosPhi 
        do theta=1,nbtheta
           !           slice(theta,l+Ni-1,1) = points_courbe_equal(l,1) + valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhi
           !           slice(theta,l+Ni-1,2) = points_courbe_equal(l,2) + valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhi
           !           slice(theta,l+Ni-1,3) = zslice + valDist(l+Ni-1,theta)*sin(valTheta(l+Ni-1,theta))
           slice(theta,l+Ni-1,1) = points_courbe_equal(l,1) + valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*sinPhi
           slice(theta,l+Ni-1,2) = points_courbe_equal(l,2) + valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*cosPhi
           slice(theta,l+Ni-1,3) = zz(nint(zslice)) + valDist(l+Ni-1,theta)*sinTheta_tab(l+Ni-1,theta)
        enddo
        !!        if ((det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2)&
        !!,points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)&
        !!,slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),slice(1,l+Ni-1,1),slice(1,l+Ni-1,2))>= 0._pr).or.(thetatab(1)<Ni)) then
        !!           thetatab(1) = thetatab(1) + 1
        !!           slice2(1,thetatab(1),1) = slice(1,l+Ni-1,1)
        !!           slice2(1,thetatab(1),2) = slice(1,l+Ni-1,2)
        !!        endif
        bool = 0
        do theta=1,nbtheta
           !           if (((theta==1).or.(theta==1+nbtheta/2)).and.(.not.((det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)&
           !,slice(theta,l+Ni-1-1,1)&
           !,slice(theta,l+Ni-1-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1)&
           !,points_courbe_equal(l-1,2),slice(theta,l+Ni-1-1,1),slice(theta,l+Ni-1-1,2),slice(theta,l+Ni-1,1)&
           !,slice(theta,l+Ni-1,2))>= 0._pr).or.(thetatab(theta)<Ni)))) then
           if (.not.(det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)&
                                !if (((theta==1).or.(theta==1+nbtheta/2)).and.(.not.(det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)&
                                !,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*sinPhir&
                                !,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*cosPhir&
                                !,points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)&
                                !,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*sinPhir&
                                !,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*cosPhir&
                                !,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhil&
                                !,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhil)>= 0._pr)) then
                ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*sinPhir&
                ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*cosPhir&
                ,points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)&
                ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*sinPhir&
                ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*cosPhir&
                ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*sinPhil&
                ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*cosPhil)>= 0._pr)) then

              !              sinTheta = -det(points_courbe_equal(l,1),points_courbe_equal(l,2),slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2)&
              !,slice(theta,l+Ni-1-1,1),slice(theta,l+Ni-1-1,2))/(norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
              !,slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2))*norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
              !,slice(theta,l+Ni-1-1,1),slice(theta,l+Ni-1-1,2)))
              !              cosTheta = dotProd(points_courbe_equal(l,1),points_courbe_equal(l,2),slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2)&
              !,slice(theta,l+Ni-1-1,1),slice(theta,l+Ni-1-1,2))/(norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
              !,slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2))*norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
              !,slice(theta,l+Ni-1-1,1),slice(theta,l+Ni-1-1,2)))
              !sinTheta = -det(points_courbe_equal(l,1),points_courbe_equal(l,2)&
              !     ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhil&
              !     ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhil&
              !     ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*sinPhir&
              !     ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*cosPhir)&
              !     /(norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
              !     ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhil&
              !     ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhil)&
              !     *norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
              !     ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*sinPhir&
              !     ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*cosPhir))
              !cosTheta = dotProd(points_courbe_equal(l,1),points_courbe_equal(l,2)&
              !     ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhil&
              !     ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhil&
              !     ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*sinPhir&
              !     ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*cosPhir)&
              !     /(norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
              !     ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhil&
              !     ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhil)&
              !     *norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
              !     ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*sinPhir&
              !     ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*cosPhir))
              sinTheta = -det(points_courbe_equal(l,1),points_courbe_equal(l,2)&
                   ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*sinPhil&
                   ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*cosPhil&
                   ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*sinPhir&
                   ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*cosPhir)&
                   /(norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
                   ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*sinPhil&
                   ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*cosPhil)&
                   *norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
                   ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*sinPhir&
                   ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*cosPhir))
              cosTheta = dotProd(points_courbe_equal(l,1),points_courbe_equal(l,2)&
                   ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*sinPhil&
                   ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*cosPhil&
                   ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*sinPhir&
                   ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*cosPhir)&
                   /(norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
                   ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*sinPhil&
                   ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*cosPhil)&
                   *norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
                   ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*sinPhir&
                   ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*cosPhir))
              if (cosTheta*oldC<0._pr) write(*,*) "ATTENTION COS NEGATIF ",l," ",l+Ni-1," ",theta,"     kt ",kt
              !               if (bool==0) then
              !               endif
              if ((abs(sinTheta/cosTheta)<abs(oldS/oldC)).and.(bool==1)) then
                 sinTheta = oldS
                 cosTheta = oldC
              endif
              bool = 1
              oldS = sinTheta
              oldC = cosTheta
              write(*,*) "cccTESTEST  ",theta," ",l," ",l+Ni-1," ",sinTheta," ",cosTheta," ",sinTheta/cosTheta
           endif
        enddo
        do theta=1,nbtheta
           !           if ((det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),slice2(theta,thetatab(theta),1)&
           !,slice2(theta,thetatab(theta),2),points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1)&
           !,points_courbe_equal(l-1,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),slice(theta,l+Ni-1,1)&
           !,slice(theta,l+Ni-1,2))>= 0._pr).or.(thetatab(theta)<Ni)) then
           !              xr = slice2(theta,thetatab(theta),1)
           !              yr = slice2(theta,thetatab(theta),2)
           xr = slice(theta,l+Ni-1,1)
           yr = slice(theta,l+Ni-1,2)
           thetatab(theta) = thetatab(theta) + 1
           !!              indextab(theta,thetatab(theta)) = l+Ni-1
           indextab(theta,l+Ni-1) = 1
           slice2(theta,thetatab(theta),1) = slice(theta,l+Ni-1,1)
           slice2(theta,thetatab(theta),2) = slice(theta,l+Ni-1,2)
           slice2(theta,thetatab(theta),3) = slice(theta,l+Ni-1,3)
           if (bool==1) then
              slice(theta,l+Ni-1,1) = cosTheta*(xr-points_courbe_equal(l,1)) + sinTheta*(yr-points_courbe_equal(l,2)) +&
                   points_courbe_equal(l,1)
              slice(theta,l+Ni-1,2) = -sinTheta*(xr-points_courbe_equal(l,1)) + cosTheta*(yr-points_courbe_equal(l,2)) +&
                   points_courbe_equal(l,2)
              sinPhi = sinAlpha*cosTheta + sinTheta*cosAlpha
              cosPhi = cosAlpha*cosTheta - sinAlpha*sinTheta
           endif
           !           else
           !
           !           if (bool==1) then
           !              xr = slice2(theta,thetatab(theta),1)
           !              yr = slice2(theta,thetatab(theta),2)
           !              thetatab(theta) = thetatab(theta) + 1
           !              indextab(theta,l+Ni-1) = 1
           !              slice2(theta,thetatab(theta),1) = cosTheta*xr + sinTheta*yr
           !              slice2(theta,thetatab(theta),2) = -sinTheta*xr + cosTheta*yr
           !            endif
           !           endif
        enddo

        if (errorl==1) write(*,*) "WARNING11 : ",errorl," ",l
     enddo
     l = Ns
     sinPhir = sinPhi
     cosPhir = cosPhi
     call intersection(points_courbe_equal(l-1,1),head_courbe(1+1,1),points_courbe_equal(Ns-1,2),head_courbe(1+1,2)&
                                !,head_courbe(1,1),head_courbe(1,2),distslice(Ni+Ns,1),distslice(1+Ni+Ns,2),distslice(Ns+Ni+Nf+Ni+Ns,1)&
          ,head_courbe(1,1),head_courbe(1,2),distslice(Ni+Ns,1),distslice(Ni+Ns+1,2),distslice(Ns+Ni+Nf+Ni+Ns,1)&
          ,distslice(Ns+Ni+Nf+1+Ni+Ns,2),xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
     sinPhi = (xTheta(1)-head_courbe(1,1))/(sqrt((xTheta(1)-head_courbe(1,1))**2+(yTheta(1)-head_courbe(1,2))**2))
     cosPhi = (yTheta(1)-head_courbe(1,2))/(sqrt((xTheta(1)-head_courbe(1,1))**2+(yTheta(1)-head_courbe(1,2))**2)) 
     sinAlpha = sinPhi
     cosAlpha = cosPhi
     sinPhil = sinPhi
     cosPhil = cosPhi
!!!     slice(1,l+Ni-1,1) = head_courbe(1,1) + valDist(l+Ni-1,1)*cos(valTheta(l+Ni-1,1))*sinPhi
!!!     slice(1,l+Ni-1,2) = head_courbe(1,2) + valDist(l+Ni-1,1)*cos(valTheta(l+Ni-1,1))*cosPhi
!!!     slice(2,l+Ni-1,1) = head_courbe(1,1) + valDist(l+Ni-1,2)*cos(valTheta(l+Ni-1,2))*sinPhi
!!!     slice(2,l+Ni-1,2) = head_courbe(1,2) + valDist(l+Ni-1,2)*cos(valTheta(l+Ni-1,2))*cosPhi
     do theta=1,nbtheta
        !        slice(theta,l+Ni-1,1) = head_courbe(1,1) + valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhi
        !        slice(theta,l+Ni-1,2) = head_courbe(1,2) + valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhi
        !        slice(theta,l+Ni-1,3) = zslice + valDist(l+Ni-1,theta)*sin(valTheta(l+Ni-1,theta))
        slice(theta,l+Ni-1,1) = head_courbe(1,1) + valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*sinPhi
        slice(theta,l+Ni-1,2) = head_courbe(1,2) + valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*cosPhi
        slice(theta,l+Ni-1,3) = zz(nint(zslice)) + valDist(l+Ni-1,theta)*sinTheta_tab(l+Ni-1,theta)
     enddo

!!!     if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2)&
!!!,points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)&
!!!,slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),slice(1,l+Ni-1,1),slice(1,l+Ni-1,2))>= 0._pr) then
!!!        thetatab(1) = thetatab(1) + 1
!!!        slice2(1,thetatab(1),1) = slice(1,l+Ni-1,1)
!!!        slice2(1,thetatab(1),2) = slice(1,l+Ni-1,2)
!!!     endif
     bool = 0
     do theta=1,nbtheta
        !        if (((theta==1).or.(theta==1+nbtheta/2)).and.(.not.(det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)&
        !,slice(theta,l+Ni-1-1,1)&
        !,slice(theta,l+Ni-1-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1)&
        !,points_courbe_equal(l-1,2),slice(theta,l+Ni-1-1,1),slice(theta,l+Ni-1-1,2),slice(theta,l+Ni-1,1)&
        !,slice(theta,l+Ni-1,2))>= 0._pr))) then
        if (.not.(det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)&
                                !,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*sinPhir&
                                !,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*cosPhir&
                                !,points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)&
                                !,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*sinPhir&
                                !,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*cosPhir&
                                !,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhil&
                                !,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhil)>= 0._pr)) then
             ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*sinPhir&
             ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*cosPhir&
             ,points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)&
             ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*sinPhir&
             ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*cosPhir&
             ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*sinPhil&
             ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*cosPhil)>= 0._pr)) then

           !              sinTheta = -det(points_courbe_equal(l,1),points_courbe_equal(l,2),slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2)&
           !,slice(theta,l+Ni-1-1,1),slice(theta,l+Ni-1-1,2))/(norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
           !,slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2))*norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
           !,slice(theta,l+Ni-1-1,1),slice(theta,l+Ni-1-1,2)))
           !              cosTheta = dotProd(points_courbe_equal(l,1),points_courbe_equal(l,2),slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2)&
           !,slice(theta,l+Ni-1-1,1),slice(theta,l+Ni-1-1,2))/(norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
           !,slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2))*norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
           !,slice(theta,l+Ni-1-1,1),slice(theta,l+Ni-1-1,2)))
           !sinTheta = -det(points_courbe_equal(l,1),points_courbe_equal(l,2)&
           !     ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhil&
           !     ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhil&
           !     ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*sinPhir&
           !     ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*cosPhir)&
           !     /(norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
           !     ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhil&
           !     ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhil)&
           !     *norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
           !     ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*sinPhir&
           !     ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*cosPhir))
           !cosTheta = dotProd(points_courbe_equal(l,1),points_courbe_equal(l,2)&
           !     ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhil&
           !     ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhil&
           !     ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*sinPhir&
           !     ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*cosPhir)&
           !     /(norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
           !     ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhil&
           !     ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhil)&
           !     *norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
           !     ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*sinPhir&
           !     ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cos(valTheta(l+Ni-1-1,theta))*cosPhir))
           sinTheta = -det(points_courbe_equal(l,1),points_courbe_equal(l,2)&
                ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*sinPhil&
                ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*cosPhil&
                ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*sinPhir&
                ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*cosPhir)&
                /(norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
                ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*sinPhil&
                ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*cosPhil)&
                *norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
                ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*sinPhir&
                ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*cosPhir))
           cosTheta = dotProd(points_courbe_equal(l,1),points_courbe_equal(l,2)&
                ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*sinPhil&
                ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*cosPhil&
                ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*sinPhir&
                ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*cosPhir)&
                /(norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
                ,points_courbe_equal(l,1)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*sinPhil&
                ,points_courbe_equal(l,2)+valDist(l+Ni-1,theta)*cosTheta_tab(l+Ni-1,theta)*cosPhil)&
                *norme(points_courbe_equal(l,1),points_courbe_equal(l,2)&
                ,points_courbe_equal(l-1,1)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*sinPhir&
                ,points_courbe_equal(l-1,2)+valDist(l+Ni-1-1,theta)*cosTheta_tab(l+Ni-1-1,theta)*cosPhir))
           if (cosTheta*oldC<0._pr) write(*,*) "ATTENTION COS NEGATIF ",l," ",l+Ni-1," ",theta,"     kt ",kt
           !               if (bool==0) then
           !               endif
           if ((abs(sinTheta/cosTheta)<abs(oldS/oldC)).and.(bool==1)) then
              sinTheta = oldS
              cosTheta = oldC
           endif
           bool = 1
           oldS = sinTheta
           oldC = cosTheta
           write(*,*) "bbbTESTEST  ",theta," ",l," ",l+Ni-1," ",sinTheta," ",cosTheta," ",sinTheta/cosTheta
        endif
     enddo
     !boolPhi = 0
     do theta=1,nbtheta
        !        if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),slice2(theta,thetatab(theta),1)&
        !,slice2(theta,thetatab(theta),2),points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1)&
        !,points_courbe_equal(l-1,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),slice(theta,l+Ni-1,1)&
        !,slice(theta,l+Ni-1,2))>= 0._pr) then
        !              xr = slice2(theta,thetatab(theta),1)
        !              yr = slice2(theta,thetatab(theta),2)
        xr = slice(theta,l+Ni-1,1)
        yr = slice(theta,l+Ni-1,2)
        thetatab(theta) = thetatab(theta) + 1
        !!           indextab(theta,thetatab(theta)) = l+Ni-1
        indextab(theta,l+Ni-1) = 1
        slice2(theta,thetatab(theta),1) = slice(theta,l+Ni-1,1)
        slice2(theta,thetatab(theta),2) = slice(theta,l+Ni-1,2)
        slice2(theta,thetatab(theta),3) = slice(theta,l+Ni-1,3)
        if (bool==1) then
           slice(theta,l+Ni-1,1) = cosTheta*(xr-points_courbe_equal(l,1)) + sinTheta*(yr-points_courbe_equal(l,2)) +&
                points_courbe_equal(l,1)
           slice(theta,l+Ni-1,2) = -sinTheta*(xr-points_courbe_equal(l,1)) + cosTheta*(yr-points_courbe_equal(l,2)) +&
                points_courbe_equal(l,2)
           !              if (boolPhi==0) then
           sinPhi = sinAlpha*cosTheta + sinTheta*cosAlpha
           cosPhi = cosAlpha*cosTheta - sinAlpha*sinTheta
           !              sinPhi = sinPhi*cosTheta + sinTheta*cosPhi
           !              cosPhi = cosPhi*cosTheta - sinPhi*sinTheta
           !                boolPhi = 1
           !                endif
        endif
        !           else
        !
        !           if (bool==1) then
        !              xr = slice2(theta,thetatab(theta),1)
        !              yr = slice2(theta,thetatab(theta),2)
        !              thetatab(theta) = thetatab(theta) + 1
        !              indextab(theta,l+Ni-1) = 1
        !              slice2(theta,thetatab(theta),1) = cosTheta*xr + sinTheta*yr
        !              slice2(theta,thetatab(theta),2) = -sinTheta*xr + cosTheta*yr
        !            endif
        !           endif
     enddo
     if (errorl==1) write(*,*) "WARNING22 : ",errorl," ",l
     do l=2,Nf
        sinPhir = sinPhi
        cosPhir = cosPhi
        if (l==Nf) then
           !call intersection(head_courbe(l-1,1),head_courbe(l,1),head_courbe(l-1,2),head_courbe(l,2),head_courbe(l,1)&
           !     ,head_courbe(l,2),distslice(l+Ni+Ns,1),distslice(l+Ni+Ns,2)&
           !     ,distslice(Ns+Ni+Nf+l+Ni+Ns,1),distslice(Ns+Ni+Nf+l+Ni+Ns,2),xTheta(1)&
           !     ,yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
           call intersection(head_courbe(Nf-2,1),head_courbe(Nf-1,1),head_courbe(Nf-2,2),head_courbe(Nf-1,2),head_courbe(Nf-1,1)&
                ,head_courbe(Nf-1,2),distslice(Nf-1+Ni+Ns,1),distslice(Nf-1+Ni+Ns,2)&
                ,distslice(Ns+Ni+Nf+Nf-1+Ni+Ns,1),distslice(Ns+Ni+Nf+Nf-1+Ni+Ns,2),xTheta(1)&
                ,yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar)
        else
           call intersection(head_courbe(l-1,1),head_courbe(l+1,1),head_courbe(l-1,2),head_courbe(l+1,2),head_courbe(l,1)&
                ,head_courbe(l,2),distslice(l+Ni+Ns,1),distslice(l+Ni+Ns,2)&
                ,distslice(Ns+Ni+Nf+l+Ni+Ns,1),distslice(Ns+Ni+Nf+l+Ni+Ns,2),xTheta(1)&
                ,yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
        endif
        if (l==Nf) then
           sinPhi = (xTheta(1)-head_courbe(Nf-1,1))/(sqrt((xTheta(1)-head_courbe(Nf-1,1))**2+(yTheta(1)-head_courbe(Nf-1,2))**2))
           cosPhi = (yTheta(1)-head_courbe(Nf-1,2))/(sqrt((xTheta(1)-head_courbe(Nf-1,1))**2+(yTheta(1)-head_courbe(Nf-1,2))**2))
        else
           sinPhi = (xTheta(1)-head_courbe(l,1))/(sqrt((xTheta(1)-head_courbe(l,1))**2+(yTheta(1)-head_courbe(l,2))**2))
           cosPhi = (yTheta(1)-head_courbe(l,2))/(sqrt((xTheta(1)-head_courbe(l,1))**2+(yTheta(1)-head_courbe(l,2))**2)) 
        endif
        sinAlpha = sinPhi
        cosAlpha = cosPhi
        sinPhil = sinPhi
        cosPhil = cosPhi
!!!        slice(1,l+Ni-1+Ns-1,1) = head_courbe(l,1) + valDist(l+Ni-1+Ns-1,1)*cos(valTheta(l+Ni-1+Ns-1,1))*sinPhi
!!!        slice(1,l+Ni-1+Ns-1,2) = head_courbe(l,2) + valDist(l+Ni-1+Ns-1,1)*cos(valTheta(l+Ni-1+Ns-1,1))*cosPhi
!!!        slice(2,l+Ni-1+Ns-1,1) = head_courbe(l,1) + valDist(l+Ni-1+Ns-1,2)*cos(valTheta(l+Ni-1+Ns-1,2))*sinPhi
!!!        slice(2,l+Ni-1+Ns-1,2) = head_courbe(l,2) + valDist(l+Ni-1+Ns-1,2)*cos(valTheta(l+Ni-1+Ns-1,2))*cosPhi
        do theta=1,nbtheta
           !           slice(theta,l+Ni-1+Ns-1,1) = head_courbe(l,1) + valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*sinPhi
           !           slice(theta,l+Ni-1+Ns-1,2) = head_courbe(l,2) + valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*cosPhi
           !           slice(theta,l+Ni-1+Ns-1,3) = zslice + valDist(l+Ni-1+Ns-1,theta)*sin(valTheta(l+Ni-1+Ns-1,theta))
           slice(theta,l+Ni-1+Ns-1,1) = head_courbe(l,1) + valDist(l+Ni-1+Ns-1,theta)*cosTheta_tab(l+Ni-1+Ns-1,theta)*sinPhi
           slice(theta,l+Ni-1+Ns-1,2) = head_courbe(l,2) + valDist(l+Ni-1+Ns-1,theta)*cosTheta_tab(l+Ni-1+Ns-1,theta)*cosPhi
           slice(theta,l+Ni-1+Ns-1,3) = zz(nint(zslice)) + valDist(l+Ni-1+Ns-1,theta)*sinTheta_tab(l+Ni-1+Ns-1,theta)
        enddo
!!!        if (det(head_courbe(l-1,1),head_courbe(l-1,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),head_courbe(l,1),&
!!!head_courbe(l,2))*det(head_courbe(l-1,1),head_courbe(l-1,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),slice(1,l+Ni-1+Ns-1,1),&
!!!slice(1,l+Ni-1+Ns-1,2))>= 0._pr) then
!!!           thetatab(1) = thetatab(1) + 1
!!!           slice2(1,thetatab(1),1) = slice(1,l+ni-1+Ns-1,1)
!!!           slice2(1,thetatab(1),2) = slice(1,l+Ni-1+Ns-1,2)
!!!        endif
        bool = 0
        do theta=1,nbtheta
           !           if (.not.(det(head_courbe(l-1,1),head_courbe(l-1,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),&
           !head_courbe(l,1),head_courbe(l,2))*det(head_courbe(l-1,1),head_courbe(l-1,2),slice2(theta,thetatab(theta),1),&
           !slice2(theta,thetatab(theta),2),slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2))>= 0._pr)) then
           !              bool = 1
           !              sinTheta = det(head_courbe(l,1),head_courbe(l,2),slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2)&
           !,slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2))/(norme(head_courbe(l,1),head_courbe(l,2)&
           !,slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2))*norme(head_courbe(l,1),head_courbe(l,2)&
           !,slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2)))
           !cosTheta = dotProd(head_courbe(l,1),head_courbe(l,2),slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2)&
           !,slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2))/(norme(head_courbe(l,1),head_courbe(l,2)&
           !,slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2))*norme(head_courbe(l,1),head_courbe(l,2)&
           !,slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2)))

           !xr = dist3D(head_courbe(l-1,1),head_courbe(l-1,2),head_courbe(l-1,3),slice(theta,l+Ni-1+Ns-1-1,1),slice(theta,l+Ni-1+Ns-1-1,2)&
           !,slice(theta,l+Ni-1+Ns-1-1,3))
           !xl = dist3D(head_courbe(l,1),head_courbe(l,2),head_courbe(l,3),slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2)&
           !,slice(theta,l+Ni-1+Ns-1,3))
           !           if (theta==1+nbtheta/2) write(*,*) "mytheta     ",l,"    ",slice(theta,l+Ni-1+Ns-1,1)," ",slice(theta,l+Ni-1+Ns-1,2)&
           !," ",head_courbe(l,1)+valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*sinPhil," ",head_courbe(l,2)+&
           !valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*cosPhil," ",head_courbe(l,1)+xl*cos(valTheta(l+Ni-1+Ns-1,theta))&
           !*sinPhil," ",head_courbe(l,2)+xl*cos(valTheta(l+Ni-1+Ns-1,theta))*cosPhil,"  dist  ",valDist(l+Ni-1+Ns-1,theta)," ",xl," "&
           !,valTheta(l+Ni-1+Ns-1,theta)
           !           if (theta==1+nbtheta/2) write(*,*) "mytheta     ",l,"    ",slice(theta,l+Ni-1+Ns-1-1,1)," ",slice(theta,l+Ni-1+Ns-1-1,2)&
           !," ",head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*sinPhir," ",head_courbe(l-1,2)+&
           !valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*cosPhir," ",head_courbe(l-1,1)+&
           !xr*cos(valTheta(l+Ni-1+Ns-1-1,theta))*sinPhir," ",head_courbe(l-1,2)+xr*cos(valTheta(l+Ni-1+Ns-1-1,theta))*cosPhir,"  dist  "&
           !,valDist(l+Ni-1+Ns-1-1,theta)," ",xr," ",valTheta(l+Ni-1+Ns-1-1,theta)

           !           if (((theta==1).or.(theta==1+nbtheta/2)).and.(.not.(det(head_courbe(l-1,1),head_courbe(l-1,2)&
           if (.not.(det(head_courbe(l-1,1),head_courbe(l-1,2)&
                                !,head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*sinPhir&
                                !,head_courbe(l-1,2)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*cosPhir&
                                !,head_courbe(l,1),head_courbe(l,2))*det(head_courbe(l-1,1),head_courbe(l-1,2)&
                                !,head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*sinPhir&
                                !,head_courbe(l-1,2)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*cosPhir&
                                !,head_courbe(l,1)+valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*sinPhil&
                                !,head_courbe(l,2)+valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*cosPhil)&
                ,head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cosTheta_tab(l+Ni-1+Ns-1-1,theta)*sinPhir&
                ,head_courbe(l-1,2)+valDist(l+Ni-1+Ns-1-1,theta)*cosTheta_tab(l+Ni-1+Ns-1-1,theta)*cosPhir&
                ,head_courbe(l,1),head_courbe(l,2))*det(head_courbe(l-1,1),head_courbe(l-1,2)&
                ,head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cosTheta_tab(l+Ni-1+Ns-1-1,theta)*sinPhir&
                ,head_courbe(l-1,2)+valDist(l+Ni-1+Ns-1-1,theta)*cosTheta_tab(l+Ni-1+Ns-1-1,theta)*cosPhir&
                ,head_courbe(l,1)+valDist(l+Ni-1+Ns-1,theta)*cosTheta_tab(l+Ni-1+Ns-1,theta)*sinPhil&
                ,head_courbe(l,2)+valDist(l+Ni-1+Ns-1,theta)*cosTheta_tab(l+Ni-1+Ns-1,theta)*cosPhil)&
                >= 0._pr)) then
              !           if (((theta==1).or.(theta==1+nbtheta/2)).and.(.not.(det(head_courbe(l-1,1),head_courbe(l-1,2)&
              !,head_courbe(l-1,1)+xr*cos(valTheta(l+Ni-1+Ns-1-1,theta))*sinPhir,head_courbe(l-1,2)+xr*cos(valTheta(l+Ni-1+Ns-1-1,theta))*cosPhir,&
              !head_courbe(l,1),head_courbe(l,2))*det(head_courbe(l-1,1),head_courbe(l-1,2),head_courbe(l-1,1)+&
              !xr*cos(valTheta(l+Ni-1+Ns-1-1,theta))*sinPhir&
              !,head_courbe(l-1,2)+xr*cos(valTheta(l+Ni-1+Ns-1-1,theta))*cosPhir,head_courbe(l,1)+xl*cos(valTheta(l+Ni-1+Ns-1,theta))*sinPhil&
              !,head_courbe(l,2)+xl*cos(valTheta(l+Ni-1+Ns-1,theta))*cosPhil)&
              !>= 0._pr))) then
              !           if (((theta==1).or.(theta==1+nbtheta/2)).and.(.not.(det(head_courbe(l-1,1),head_courbe(l-1,2)&
              !,slice(theta,l+Ni-1+Ns-1-1,1),slice(theta,l+Ni-1+Ns-1-1,2),&
              !head_courbe(l,1),head_courbe(l,2))*det(head_courbe(l-1,1),head_courbe(l-1,2),slice(theta,l+Ni-1+Ns-1-1,1),&
              !slice(theta,l+Ni-1+Ns-1-1,2),slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2))>= 0._pr))) then
              !sinTheta = -det(head_courbe(l,1),head_courbe(l,2)&
              !     ,head_courbe(l,1)+valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*sinPhil&
              !     ,head_courbe(l,2)+valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*cosPhil&
              !     ,head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*sinPhir&
              !     ,head_courbe(l-1,2)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*cosPhir)&
              !     /(norme(head_courbe(l,1),head_courbe(l,2)&
              !     ,head_courbe(l,1)+valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*sinPhil&
              !     ,head_courbe(l,2)+valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*cosPhil)&
              !     *norme(head_courbe(l,1),head_courbe(l,2)&
              !     ,head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*sinPhir&
              !     ,head_courbe(l-1,2)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*cosPhir))
              !cosTheta = dotProd(head_courbe(l,1),head_courbe(l,2)&
              !     ,head_courbe(l,1)+valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*sinPhil&
              !     ,head_courbe(l,2)+valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*cosPhil&
              !     ,head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*sinPhir&
              !     ,head_courbe(l-1,2)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*cosPhir)&
              !     /(norme(head_courbe(l,1),head_courbe(l,2)&
              !     ,head_courbe(l,1)+valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*sinPhil&
              !     ,head_courbe(l,2)+valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*cosPhil)&
              !     *norme(head_courbe(l,1),head_courbe(l,2)&
              !     ,head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*sinPhir&
              !     ,head_courbe(l-1,2)+valDist(l+Ni-1+Ns-1-1,theta)*cos(valTheta(l+Ni-1+Ns-1-1,theta))*cosPhir))
              sinTheta = -det(head_courbe(l,1),head_courbe(l,2)&
                   ,head_courbe(l,1)+valDist(l+Ni-1+Ns-1,theta)*cosTheta_tab(l+Ni-1+Ns-1,theta)*sinPhil&
                   ,head_courbe(l,2)+valDist(l+Ni-1+Ns-1,theta)*cosTheta_tab(l+Ni-1+Ns-1,theta)*cosPhil&
                   ,head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cosTheta_tab(l+Ni-1+Ns-1-1,theta)*sinPhir&
                   ,head_courbe(l-1,2)+valDist(l+Ni-1+Ns-1-1,theta)*cosTheta_tab(l+Ni-1+Ns-1-1,theta)*cosPhir)&
                   /(norme(head_courbe(l,1),head_courbe(l,2)&
                   ,head_courbe(l,1)+valDist(l+Ni-1+Ns-1,theta)*cosTheta_tab(l+Ni-1+Ns-1,theta)*sinPhil&
                   ,head_courbe(l,2)+valDist(l+Ni-1+Ns-1,theta)*cosTheta_tab(l+Ni-1+Ns-1,theta)*cosPhil)&
                   *norme(head_courbe(l,1),head_courbe(l,2)&
                   ,head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cosTheta_tab(l+Ni-1+Ns-1-1,theta)*sinPhir&
                   ,head_courbe(l-1,2)+valDist(l+Ni-1+Ns-1-1,theta)*cosTheta_tab(l+Ni-1+Ns-1-1,theta)*cosPhir))
              cosTheta = dotProd(head_courbe(l,1),head_courbe(l,2)&
                   ,head_courbe(l,1)+valDist(l+Ni-1+Ns-1,theta)*cosTheta_tab(l+Ni-1+Ns-1,theta)*sinPhil&
                   ,head_courbe(l,2)+valDist(l+Ni-1+Ns-1,theta)*cosTheta_tab(l+Ni-1+Ns-1,theta)*cosPhil&
                   ,head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cosTheta_tab(l+Ni-1+Ns-1-1,theta)*sinPhir&
                   ,head_courbe(l-1,2)+valDist(l+Ni-1+Ns-1-1,theta)*cosTheta_tab(l+Ni-1+Ns-1-1,theta)*cosPhir)&
                   /(norme(head_courbe(l,1),head_courbe(l,2)&
                   ,head_courbe(l,1)+valDist(l+Ni-1+Ns-1,theta)*cosTheta_tab(l+Ni-1+Ns-1,theta)*sinPhil&
                   ,head_courbe(l,2)+valDist(l+Ni-1+Ns-1,theta)*cosTheta_tab(l+Ni-1+Ns-1,theta)*cosPhil)&
                   *norme(head_courbe(l,1),head_courbe(l,2)&
                   ,head_courbe(l-1,1)+valDist(l+Ni-1+Ns-1-1,theta)*cosTheta_tab(l+Ni-1+Ns-1-1,theta)*sinPhir&
                   ,head_courbe(l-1,2)+valDist(l+Ni-1+Ns-1-1,theta)*cosTheta_tab(l+Ni-1+Ns-1-1,theta)*cosPhir))
              if (cosTheta*oldC<0._pr) write(*,*) "ATTENTION COS NEGATIF ",l," ",l+Ni-1+Ns-1," ",theta,"    kt ",kt
              !               if (bool==0) then
              !               endif
              if ((abs(sinTheta/cosTheta)<abs(oldS/oldC)).and.(bool==1)) then
                 sinTheta = oldS
                 cosTheta = oldC
              endif
              bool = 1
              oldS = sinTheta
              oldC = cosTheta

              !              sinTheta = -det(head_courbe(l,1),head_courbe(l,2),slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2)&
              !,slice(theta,l+Ni-1+Ns-1-1,1),slice(theta,l+Ni-1+Ns-1-1,2))/(norme(head_courbe(l,1),head_courbe(l,2)&
              !,slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2))*norme(head_courbe(l,1),head_courbe(l,2)&
              !,slice(theta,l+Ni-1+Ns-1-1,1),slice(theta,l+Ni-1+Ns-1-1,2)))
              !              cosTheta = dotProd(head_courbe(l,1),head_courbe(l,2),slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2)&
              !,slice(theta,l+Ni-1+Ns-1-1,1),slice(theta,l+Ni-1+Ns-1-1,2))/(norme(head_courbe(l,1),head_courbe(l,2)&
              !,slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2))*norme(head_courbe(l,1),head_courbe(l,2)&
              !,slice(theta,l+Ni-1+Ns-1-1,1),slice(theta,l+Ni-1+Ns-1-1,2)))
              write(*,*) "aaaTESTEST  ",theta," ",l," ",l+Ni-1+Ns-1," ",sinTheta," ",cosTheta," ",sinPhir," ",cosPhir&
                   ," ",sinPhil," ",cosPhil," ",sinTheta/cosTheta
              !              write(*,*) "aaaTESTEST  ",theta," ",l," ",l+Ni-1+Ns-1," ",sinTh(theta)," ",cosTh(theta)," ",sinPhir," ",cosPhir&
              !," ",sinPhil," ",cosPhil
           endif
        enddo
        !boolPhi = 0
        do theta=1,nbtheta
           !           if (det(head_courbe(l-1,1),head_courbe(l-1,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),&
           !head_courbe(l,1),head_courbe(l,2))*det(head_courbe(l-1,1),head_courbe(l-1,2),slice2(theta,thetatab(theta),1),&
           !slice2(theta,thetatab(theta),2),slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2))>= 0._pr) then
           !              xr = slice2(theta,thetatab(theta),1)
           !              yr = slice2(theta,thetatab(theta),2)
           xr = slice(theta,l+Ni-1+Ns-1,1)
           yr = slice(theta,l+Ni-1+Ns-1,2)
           !           if (theta==101) write(*,*) "CHECK ouch appartient  ",theta," ",l," ",l+Ni-1+Ns-1," ",slice(theta,l+Ni-1+Ns-1,1)&
           !," ",slice(theta,l+Ni-1+Ns-1,2)," ",slice(theta,l+Ni-1+Ns-1,3)," ",cos(valTheta(l+Ni-1+Ns-1,theta))&
           !," ",sin(valTheta(l+Ni-1+Ns-1,theta))," ",indextheta(l)
           thetatab(theta) = thetatab(theta) + 1
           !!              indextab(theta,thetatab(theta)) = l+Ni-1+Ns-1
           indextab(theta,l+Ni-1+Ns-1) = 1
           slice2(theta,thetatab(theta),1) = slice(theta,l+Ni-1+Ns-1,1)
           slice2(theta,thetatab(theta),2) = slice(theta,l+Ni-1+Ns-1,2)
           slice2(theta,thetatab(theta),3) = slice(theta,l+Ni-1+Ns-1,3)
           !              xr = slice2(theta,thetatab(theta),1)
           !              yr = slice2(theta,thetatab(theta),2)
           if (bool==1) then
              !              write(*,*) "oint : ",slice2(theta,thetatab(theta),1)," ",slice2(theta,thetatab(theta),2)
              !              slice2(theta,thetatab(theta),1) = cosTheta*xr + sinTheta*yr
              !              slice2(theta,thetatab(theta),2) = -sinTheta*xr + cosTheta*yr
              !write(*,*) "AH oui ",theta," ",l," ",l+Ni-1+Ns-1," ",slice(theta,l+Ni-1+Ns-1,1)," ",slice(theta,l+Ni-1+Ns-1,2)
              slice(theta,l+Ni-1+Ns-1,1) = cosTheta*(xr-head_courbe(l,1)) + sinTheta*(yr-head_courbe(l,2)) + head_courbe(l,1)
              slice(theta,l+Ni-1+Ns-1,2) = -sinTheta*(xr-head_courbe(l,1)) + cosTheta*(yr-head_courbe(l,2)) + head_courbe(l,2)
              !write(*,*) "ah oui ",theta," ",l," ",l+Ni-1+Ns-1," ",slice(theta,l+Ni-1+Ns-1,1)," ",slice(theta,l+Ni-1+Ns-1,2)
              !            slice(theta,l+Ni-1+Ns-1,1) = cosTh(theta)*(xr-head_courbe(l,1)) + sinTh(theta)*(yr-head_courbe(l,2)) + head_courbe(l,1)
              !            slice(theta,l+Ni-1+Ns-1,2) = -sinTh(theta)*(xr-head_courbe(l,1)) + cosTh(theta)*(yr-head_courbe(l,2)) + head_courbe(l,2)
              !              write(*,*) "oint : ",slice2(theta,thetatab(theta),1)," ",slice2(theta,thetatab(theta),2)
              !              slice2(theta,thetatab(theta),1) = cosTheta*xr - sinTheta*yr
              !              slice2(theta,thetatab(theta),2) = sinTheta*xr + cosTheta*yr
              !              write(*,*) "oint : ",slice2(theta,thetatab(theta),1)," ",slice2(theta,thetatab(theta),2)
              !if (boolPhi==0) then
              sinPhi = sinAlpha*cosTheta + sinTheta*cosAlpha
              cosPhi = cosAlpha*cosTheta - sinAlpha*sinTheta
              !              sinPhi = sinPhi*cosTheta + sinTheta*cosPhi
              !              cosPhi = cosPhi*cosTheta - sinPhi*sinTheta
              !              sinPhi = sinAlpha*cosTh(theta) + sinTh(theta)*cosAlpha
              !              cosPhi = cosAlpha*cosTh(theta) - sinAlpha*sinTh(theta)
              !  boolPhi = 1
              !  endif
           endif
           !           else
           !
           !           if (bool==1) then
           !              xr = slice2(theta,thetatab(theta),1)
           !              yr = slice2(theta,thetatab(theta),2)
           !!              xr = slice(theta,l+Ni-1+Ns-1,1)
           !!              yr = slice(theta,+Ni-1+Ns-1,2)
           !              thetatab(theta) = thetatab(theta) + 1
           !              indextab(theta,l+Ni-1+Ns-1) = 1
           !              slice2(theta,thetatab(theta),1) = cosTheta*xr + sinTheta*yr
           !              slice2(theta,thetatab(theta),2) = -sinTheta*xr + cosTheta*yr
           !            endif
           !           endif
        enddo
        if (errorl==1) write(*,*) "WARNING33 : ",errorl," ",l
     enddo
     !     l = Nf
     !     do theta=1,nbtheta
     !        thetatab(theta) = thetatab(theta) + 1
!!!        indextab(theta,thetatab(theta)) = Nf+Ni-1+Ns-1
     !        indextab(theta,l+Ni-1+Ns-1) = 1
     !     slice(theta,l+Ni-1+Ns-1,1) = head_courbe(l,1)
     !     slice(theta,l+Ni-1+Ns-1,2) = head_courbe(l,2)
     !     slice(theta,l+Ni-1+Ns-1,3) = head_courbe(l,3)
     !        slice2(theta,thetatab(theta),1) = head_courbe(l,1)
     !        slice2(theta,thetatab(theta),2) = head_courbe(l,2)
     !        slice2(theta,thetatab(theta),3) = head_courbe(l,3)
     !     enddo
     !!   do theta=1,nbtheta
     !!        indextheta(1) = 1
!!!!!        slice(theta,1,1) = tail_courbe(Ni,1)
!!!!!        slice(theta,1,2) = tail_courbe(Ni,2)
!!!!!        slice(theta,1,3) = tail_courbe(Ni,3)
     !!        vect(theta,1,1) = slice(theta,1,1)
     !!        vect(theta,1,2) = slice(theta,1,2)
     !!        vect(theta,1,3) = slice(theta,1,3)
     !!     do l=2,Ni-1
!!!!!        call intersection(tail_courbe(Ni-l+1+1,1),tail_courbe(Ni-l+1-1,1),tail_courbe(Ni-l+1+1,2),tail_courbe(Ni-l+1-1,2)&
!!!!!,tail_courbe(Ni-l+1,1),tail_courbe(Ni-l+1,2),distslice(l,1),distslice(l,2),distslice(Ns+Ni+Nf+l,1),distslice(Ns+Ni+Nf+l,2)&
!!!!!,xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
!!!!!        sinPhi = (xTheta(1)-tail_courbe(Ni-l+1,1))/(&
!!!!!sqrt((xTheta(1)-tail_courbe(Ni-l+1,1))**2+(yTheta(1)-tail_courbe(Ni-l+1,2))**2))
!!!!!        cosPhi = (yTheta(1)-tail_courbe(Ni-l+1,2))/(&
!!!!!sqrt((xTheta(1)-tail_courbe(Ni-l+1,1))**2+(yTheta(1)-tail_courbe(Ni-l+1,2))**2)) 
!!!!!           slice(theta,l,1) = tail_courbe(Ni-l+1,1) + valDist(l,theta)*cos(valTheta(l,theta))*sinPhi
!!!!!           slice(theta,l,2) = tail_courbe(Ni-l+1,2) + valDist(l,theta)*cos(valTheta(l,theta))*cosPhi
!!!!!           slice(theta,l,3) = zslice + valDist(l,theta)*sin(valTheta(l,theta))
     !!
     !!           indextheta(l) = indextheta(l) + 1
     !!           vect(indextheta(l),l,1) = slice(theta,l,1)
     !!           vect(indextheta(l),l,2) = slice(theta,l,2)
     !!           vect(indextheta(l),l,3) = slice(theta,l,3)
     !!        if ((errorl==1).or.(errorr==1)) then
     !!           vect(indextheta(l),l,1) = vect(indextheta(l),l-1,1)
     !!           vect(indextheta(l),l,2) = vect(indextheta(l),l-1,2)
     !!           vect(indextheta(l),l,3) = vect(indextheta(l),l-1,3)
     !!   endif
     !!     enddo
     !!     l = Ni
!!!!!     call intersection(tail_courbe(2,1),points_courbe_equal(2,1),tail_courbe(2,2),points_courbe_equal(2,2),points_courbe_equal(1,1)&
!!!!!,points_courbe_equal(1,2),distslice(Ni,1),distslice(Ni+1,2),distslice(Ns+Ni+Nf+Ni,1),distslice(Ns+Ni+Nf+Ni+1,2),xTheta(1),yTheta(1)&
!!!!!,xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
!!!!!     sinPhi = (xTheta(1)-points_courbe_equal(1,1))/(&
!!!!!sqrt((xTheta(1)-points_courbe_equal(1,1))**2+(yTheta(1)-points_courbe_equal(1,2))**2))
!!!!!     cosPhi = (yTheta(1)-points_courbe_equal(1,2))/(&
!!!!!sqrt((xTheta(1)-points_courbe_equal(1,1))**2+(yTheta(1)-points_courbe_equal(1,2))**2)) 
!!!!!        slice(theta,l,1) = points_courbe_equal(1,1) + valDist(l,theta)*cos(valTheta(l,theta))*sinPhi
!!!!!        slice(theta,l,2) = points_courbe_equal(1,2) + valDist(l,theta)*cos(valTheta(l,theta))*cosPhi
!!!!!        slice(theta,l,3) = zslice + valDist(l,theta)*sin(valTheta(l,theta))
     !!        if ((det(tail_courbe(2,1),tail_courbe(2,2),vect(indextheta(l),l,1),vect(indextheta(l),l,2),&
     !!points_courbe_equal(1,1),points_courbe_equal(1,2))*det(tail_courbe(2,1),tail_courbe(2,2),vect(indextheta(l),l,1),&
     !!vect(indextheta(l),l,2),slice(theta,l,1),slice(theta,l,2))>= 0._pr).or.(l<Ni)) then
     !!           indextheta(l) = indextheta(l) + 1
     !!           vect(indextheta(l),l,1) = slice(theta,l,1)
     !!           vect(indextheta(l),l,2) = slice(theta,l,2)
     !!           vect(indextheta(l),l,3) = slice(theta,l,3)
     !!        endif
     !!     do l=2,Ns-1
!!!!!        call intersection(points_courbe_equal(l-1,1),points_courbe_equal(l+1,1),points_courbe_equal(l-1,2),&
!!!!!points_courbe_equal(l+1,2),points_courbe_equal(l,1),points_courbe_equal(l,2),distslice(l+Ni,1),distslice(l+Ni,2),&
!!!!!distslice(Ns+Ni+Nf+l+Ni,1),distslice(Ns+Ni+Nf+l+Ni,2),xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
!!!!!        sinPhi = (xTheta(1)-points_courbe_equal(l,1))/(&
!!!!!sqrt((xTheta(1)-points_courbe_equal(l,1))**2+(yTheta(1)-points_courbe_equal(l,2))**2))
!!!!!        cosPhi = (yTheta(1)-points_courbe_equal(l,2))/(&
!!!!!sqrt((xTheta(1)-points_courbe_equal(l,1))**2+(yTheta(1)-points_courbe_equal(l,2))**2)) 
!!!!!           slice(theta,l+Ni-1,1) = points_courbe_equal(l,1) + valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhi
!!!!!           slice(theta,l+Ni-1,2) = points_courbe_equal(l,2) + valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhi
!!!!!           slice(theta,l+Ni-1,3) = zslice + valDist(l+Ni-1,theta)*sin(valTheta(l+Ni-1,theta))
     !!           if ((det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),vect(indextheta(l+Ni-1),l+Ni-1,1)&
     !!,vect(indextheta(l+Ni-1),l+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1)&
     !!,points_courbe_equal(l-1,2),vect(indextheta(l+Ni-1),l+Ni-1,1),vect(indextheta(l+Ni-1),l+Ni-1,2),slice(theta,l+Ni-1,1)&
     !!,slice(theta,l+Ni-1,2))>= 0._pr).or.(l<Ni)) then
     !!              indextheta(l+Ni-1) = indextheta(l+Ni-1) + 1
     !!              vect(indextheta(l+Ni-1),l+Ni-1,1) = slice(theta,l+Ni-1,1)
     !!              vect(indextheta(l+Ni-1),l+Ni-1,2) = slice(theta,l+Ni-1,2)
     !!              vect(indextheta(l+Ni-1),l+Ni-1,3) = slice(theta,l+Ni-1,3)
     !!           endif
     !!     enddo
     !!     l = Ns
!!!!!     call intersection(points_courbe_equal(l-1,1),head_courbe(1+1,1),points_courbe_equal(Ns-1,2),head_courbe(1+1,2)&
!!!!!,head_courbe(1,1),head_courbe(1,2),distslice(Ni+Ns,1),distslice(1+Ni+Ns,2),distslice(Ns+Ni+Nf+Ni+Ns,1)&
!!!!!,distslice(Ns+Ni+Nf+1+Ni+Ns,2),xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
!!!!!     sinPhi = (xTheta(1)-head_courbe(1,1))/(sqrt((xTheta(1)-head_courbe(1,1))**2+(yTheta(1)-head_courbe(1,2))**2))
!!!!!     cosPhi = (yTheta(1)-head_courbe(1,2))/(sqrt((xTheta(1)-head_courbe(1,1))**2+(yTheta(1)-head_courbe(1,2))**2)) 
!!!!!        slice(theta,l+Ni-1,1) = head_courbe(1,1) + valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*sinPhi
!!!!!        slice(theta,l+Ni-1,2) = head_courbe(1,2) + valDist(l+Ni-1,theta)*cos(valTheta(l+Ni-1,theta))*cosPhi
!!!!!        slice(theta,l+Ni-1,3) = zslice + valDist(l+Ni-1,theta)*sin(valTheta(l+Ni-1,theta))
     !!        if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),vect(indextheta(l+Ni-1),l+Ni-1,1)&
     !!,vect(indextheta(l+Ni-1),l+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1)&
     !!,points_courbe_equal(l-1,2),vect(indextheta(l+Ni-1),l+Ni-1,1),vect(indextheta(l+Ni-1),l+Ni-1,2),slice(theta,l+Ni-1,1)&
     !!,slice(theta,l+Ni-1,2))>= 0._pr) then
     !!           indextheta(l+Ni-1) = indextheta(l+Ni-1) + 1
     !!           vect(indextheta(l+Ni-1),l+Ni-1,1) = slice(theta,l+Ni-1,1)
     !!           vect(indextheta(l+Ni-1),l+Ni-1,2) = slice(theta,l+Ni-1,2)
     !!           vect(indextheta(l+Ni-1),l+Ni-1,3) = slice(theta,l+Ni-1,3)
     !!        endif
     !!     do l=2,Nf-1
!!!!!        call intersection(head_courbe(l-1,1),head_courbe(l+1,1),head_courbe(l-1,2),head_courbe(l+1,2),head_courbe(l,1)&
!!!!!,head_courbe(l,2),distslice(l+Ni+Ns,1),distslice(l+Ni+Ns,2),distslice(Ns+Ni+Nf+l+Ni+Ns,1),distslice(Ns+Ni+Nf+l+Ni+Ns,2),xTheta(1)&
!!!!!,yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
!!!!!        sinPhi = (xTheta(1)-head_courbe(l,1))/(sqrt((xTheta(1)-head_courbe(l,1))**2+(yTheta(1)-head_courbe(l,2))**2))
!!!!!        cosPhi = (yTheta(1)-head_courbe(l,2))/(sqrt((xTheta(1)-head_courbe(l,1))**2+(yTheta(1)-head_courbe(l,2))**2)) 
!!!!!           slice(theta,l+Ni-1+Ns-1,1) = head_courbe(l,1) + valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*sinPhi
!!!!!           slice(theta,l+Ni-1+Ns-1,2) = head_courbe(l,2) + valDist(l+Ni-1+Ns-1,theta)*cos(valTheta(l+Ni-1+Ns-1,theta))*cosPhi
!!!!!           slice(theta,l+Ni-1+Ns-1,3) = zslice + valDist(l+Ni-1+Ns-1,theta)*sin(valTheta(l+Ni-1+Ns-1,theta))
     !!           if (det(head_courbe(l-1,1),head_courbe(l-1,2),vect(indextheta(l+Ni-1+Ns-1),l+Ni-1+Ns-1,1),vect(indextheta(l+Ni-1+Ns-1)&
     !!,l+Ni-1+Ns-1,2),head_courbe(l,1),head_courbe(l,2))*det(head_courbe(l-1,1),head_courbe(l-1,2),vect(indextheta(l+Ni-1+Ns-1)&
     !!,l+Ni-1+Ns-1,1),vect(indextheta(l+NI-1+Ns-1),l+Ni-1+Ns-1,2),slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2))>= 0._pr) then
     !!              indextheta(l+Ni-1+Ns-1) = indextheta(l+Ni-1+Ns-1) + 1
     !!              vect(indextheta(l+Ni-1+Ns-1),l+Ni-1+Ns-1,1) = slice(theta,l+Ni-1+Ns-1,1)
     !!              vect(indextheta(l+Ni-1+Ns-1),l+Ni-1+Ns-1,2) = slice(theta,l+Ni-1+Ns-1,2)
     !!              vect(indextheta(l+Ni-1+Ns-1),l+Ni-1+Ns-1,3) = slice(theta,l+Ni-1+Ns-1,3)
     !!           endif
     !!     enddo
     !!     l = Nf
     !!        indextheta(l+Ni-1+Ns-1) = indextheta(l+Ni-1+Ns-1) + 1
     !!        vect(indextheta(l+Ni-1+Ns-1),l+Ni-1+Ns-1,1) = head_courbe(l,1)
     !!        vect(indextheta(l+Ni-1+Ns-1),l+Ni-1+Ns-1,2) = head_courbe(l,2)
     !!        vect(indextheta(l+Ni-1+Ns-1),l+Ni-1+Ns-1,3) = head_courbe(l,3)
     !!   enddo
     do l=1,Ni+Ns+Nf-2
        indextheta(l) = 0
        do theta = 1,nbtheta
!!!           if (l==indextab(theta,l)) then
           !           if (theta==101) write(*,*) "CHECK ERROR101 ",theta," ",l," ",l+Ni-1+Ns-1," ",slice(theta,l+Ni-1+Ns-1,1)&
           !," ",slice(theta,l+Ni-1+Ns-1,2)," ",slice(theta,l+Ni-1+Ns-1,3)," ",cos(valTheta(l+Ni-1+Ns-1,theta))&
           !," ",sin(valTheta(l+Ni-1+Ns-1,theta))," ",indextheta(l)
           !!           if (appartientElmt(indextab,theta,l).eqv..true.) then
           if (indextab(theta,l)==1) then
              indextheta(l) = indextheta(l) + 1
!!!              vect(l,indextheta(l)) = theta
!!!              vect(indextheta(l),l,1) = slice2(theta,thetatab(theta),1)
!!!              vect(indextheta(l),l,2) = slice2(theta,thetatab(theta),2)
!!!              vect(indextheta(l),l,3) = slice2(theta,thetatab(theta),3)
              !           if (theta==101) write(*,*) "CHECK ERROR101 appartient  ",theta," ",l," ",l+Ni-1+Ns-1," ",slice(theta,l+Ni-1+Ns-1,1)&
              !," ",slice(theta,l+Ni-1+Ns-1,2)," ",slice(theta,l+Ni-1+Ns-1,3)," ",cos(valTheta(l+Ni-1+Ns-1,theta))&
              !," ",sin(valTheta(l+Ni-1+Ns-1,theta))," ",indextheta(l)
              vect(indextheta(l),l,1) = slice(theta,l,1)
              vect(indextheta(l),l,2) = slice(theta,l,2)
              !              if (vect(101,l,2)>110.0) write(*,*) "WHHHYY  ",theta," ",l," ",indextheta(l)
              vect(indextheta(l),l,3) = slice(theta,l,3)
           endif
        enddo
     enddo

     !     write(*,*) "lpl lpr finaux : ",thetatab(1)," ",thetatab(2)

     deallocate(midline)
     deallocate(points_control)

     !**
     !! OUTPUTS 
     !**

     !! FILM
     !open(unit=85,file='results/skelhead'//str(kt+140-1)//'.txt',status='unknown')
     !open(unit=84,file='results/right'//str(kt+140-1)//'.txt',status='unknown')
     !open(unit=83,file='results/left'//str(kt+140-1)//'.txt',status='unknown')
     !open(unit=82,file='results/skeletteq'//str(kt+140-1)//'.txt',status='unknown')
     !open(unit=81,file='results/skelett'//str(kt+140-1)//'.txt',status='unknown')
     !open(unit=79,file='results/skelett'//str(kt+140-1)//'.vtk',status='unknown')
     !open(unit=85,file='results_secondTEST6/skelhead'//str(kt+picNum-1)//'.txt',status='unknown')
     !open(unit=84,file='results_secondTEST6/right'//str(kt+picNum-1)//'.txt',status='unknown')
     !open(unit=83,file='results_secondTEST6/left'//str(kt+picNum-1)//'.txt',status='unknown')
     !open(unit=82,file='results_secondTEST6/skeletteq'//str(kt+picNum-1)//'.txt',status='unknown')
     !open(unit=81,file='results_secondTEST6/skelett'//str(kt+picNum-1)//'.txt',status='unknown')
     !open(unit=79,file='results_secondtest6/skelett'//str(kt+picnum-1)//'.vtk',status='unknown')
     !open(unit=86,file='results_secondtest6/gradPhi'//str(kt+picnum-1)//'.vtk',status='unknown')
     !open(unit=87,file='results_secondtest6/gradPhiBis'//str(kt+picnum-1)//'.vtk',status='unknown')
     open(unit=85,file='results_test/skelhead'//str(kt+picNum-1)//'.txt',status='unknown')
     open(unit=84,file='results_test/right'//str(kt+picNum-1)//'.txt',status='unknown')
     open(unit=83,file='results_test/left'//str(kt+picNum-1)//'.txt',status='unknown')
     open(unit=82,file='results_test/skeletteq'//str(kt+picNum-1)//'.txt',status='unknown')
     open(unit=81,file='results_test/skelett'//str(kt+picNum-1)//'.txt',status='unknown')
     open(unit=79,file='results_test/skelett'//str(kt+picnum-1)//'.vtk',status='unknown')
     !open(unit=86,file='results_test/gradPhi'//str(kt+picnum-1)//'.vtk',status='unknown')
     !open(unit=87,file='results_test/gradPhiBis'//str(kt+picnum-1)//'.vtk',status='unknown')
     !!open(unit=79,file='results/skelett'//str(kt+picnum-1)//'.vtk',status='unknown')
     write(79,'(1A26)') '# vtk DataFile Version 2.0'
     write(79,'(a)') 'rho'
     write(79,'(a)') 'ASCII'
     write(79,'(a)') 'DATASET STRUCTURED_POINTS'
     write(79,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1
     write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
     write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 1.,1.,1. 
     write(79,'(a,I6)') 'POINT_DATA          ' , nx*ny 
     write(79,'(a)') 'SCALARS values double'
     write(79,'(a)') 'LOOKUP_TABLE default'
     !write(86,'(1A26)') '# vtk DataFile Version 2.0'
     !write(86,'(a)') 'rho'
     !write(86,'(a)') 'ASCII'
     !write(86,'(a)') 'DATASET STRUCTURED_POINTS'
     !write(86,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1
     !write(86,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
     !write(86,'(a,E23.15,E23.15,E23.15)') 'SPACING', 1.,1.,1. 
     !write(86,'(a,I6)') 'POINT_DATA          ' , nx*ny 
     !write(86,'(a)') 'SCALARS values double'
     !write(86,'(a)') 'LOOKUP_TABLE default'
     !write(87,'(1A26)') '# vtk DataFile Version 2.0'
     !write(87,'(a)') 'rho'
     !write(87,'(a)') 'ASCII'
     !write(87,'(a)') 'DATASET STRUCTURED_POINTS'
     !write(87,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1
     !write(87,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
     !write(87,'(a,E23.15,E23.15,E23.15)') 'SPACING', 1.,1.,1. 
     !write(87,'(a,I6)') 'POINT_DATA          ' , nx*ny 
     !write(87,'(a)') 'SCALARS values double'
     !write(87,'(a)') 'LOOKUP_TABLE default'
     do k=1,ny
        do j=1,nx
           write(79,*) rhoSlices2(j,k)*(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k)
           !!write(79,*) skel(j,k)!(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k)
           !!write(86,*) gradPhi(j,k)*(1-skel(j,k))/maxval(gradPhi) + skel(j,k)
           !!write(86,*) gradPhi(j,k)*(1-skel(j,k)) + 0.5_pr*(minval(gradPhi)+maxval(gradPhi))*skel(j,k)
           !write(86,*) gradPhi(j,k)*(1-skel(j,k)) + 2*skel(j,k)
           !write(87,*) gradPhi(j,k)*(1-skel2(j,k)) + 2*skel2(j,k)
        enddo
     enddo
     write(*,*) "CHECK size nbtheta : ",sum(thetatab)," ",sum(indextheta),"  THETA  ",thetatab(nbtheta/2+1)
     !! FILM
     !open(unit=78,file='results/surf'//str(kt+140-1)//'.vtk',status='unknown')
     !open(unit=78,file='results/surf'//str(kt+140-1)//'.vts',status='unknown')
     !open(unit=78,file='results_secondTEST6/surf'//str(kt+picNum-1)//'.vts',status='unknown')
     !open(unit=78,file='results_test/surf'//str(kt+picNum-1)//'.vts',status='unknown')
     !open(unit=92,file='results_secondTEST6/surf'//str(kt+picNum-1)//'.dat',status='unknown')
     !open(unit=92,file='results_secondTEST6/surf/surf'//str(kt+picNum-1)//'.dat',status='unknown')
     open(unit=92,file='results_test/surf/surf'//str(kt+picNum-1)//'.dat',status='unknown')
         write(78,'(a)') "<?xml version=""1.0""?>"
         write(78,'(a)') "<VTKFile type=""StructuredGrid"" version=""0.1"" byte_order=""LittleEndian"" compressor=&
              ""vtkZLibDataCompressor"">"
         write(78,'(a,I3,a,I3,a)') "<StructuredGrid WholeExtent=""0 ",nbtheta," 0 ",Ni-1+Ns+Nf-1-1," 0 0"">"
         write(78,'(a,I3,a,I3,a)') "<Piece Extent=""0 ",nbtheta," 0 ",Ni-1+Ns+Nf-1-1," 0 0"">"
         write(78,'(a)') "<PointData >"
!!!!!     write(78,'(a)') "<PointData Scalars=""Density"">"
!!!!!     write(78,'(a)') "</DataArray>"
         write(78,'(a)') "</PointData>"
         write(78,'(a)') "<CellData>"
         write(78,'(a)') "</CellData>"
         write(78,'(a)') "<Points>"
!!!!!     write(78,'(a)') "<DataArray type=""Float32"" Name=""Density"" format=""ascii"">"
         write(78,'(a)') "<DataArray NumberOfComponents=""3"" type=""Float64"" format=""ascii"" >"  

!!      write(78,'(1A26)') '# vtk DataFile Version 2.0'
!!      write(78,'(a)') 'rho'
!!      write(78,'(a)') 'ASCII'
!!      write(78,'(a)') 'DATASET POLYDATA'
!!!!       write(78,'(a,I6,a)') 'POINTS ', sum(thetatab), ' FLOAT'
!!      write(78,'(a,I6,a)') 'POINTS ', sum(indextheta), ' FLOAT'
           write(78,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,nz
           write(78,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
           write(78,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx, dy, dz
           write(78,'(a,I9)') 'POINT_DATA' , nx*ny*nz
           write(78,'(a)') 'SCALARS values double'
           write(78,'(a)') 'LOOKUP_TABLE default'
!!        do theta=1,nbtheta
!!     do l=1,thetatab(theta)
!!           write(78,*) slice2(theta,l,1)," ",slice2(theta,l,2)," ",slice2(theta,l,3)
!!     enddo
!!        enddo
!!        do theta=1,nbtheta
!!        do l=1,thetatab(theta)
!!        write(*,*) "OHOH th ",
     do l=1,Ni+Ns+Nf-2
        do theta=1,nbtheta !indextheta(l)
!!!        write(78,*) slice2(vect(l,theta),l,1)," ",slice2(vect(l,theta),l,2)," ",slice2(vect(l,theta),l,3)
!!!         if (vect(l,theta)==1) then
!!!                 write(78,*) slice2(theta,l,1)," ",slice2(theta,l,2)," ",slice2(theta,l,3)
           !!     write(78,*) vect(theta,l,1)," ",vect(theta,l,2)," ",vect(theta,l,3)
           !!     if (appartientElmt(indextab,theta,l).eqv..true.) write(78,*) vect(theta,l,1)," ",vect(theta,l,2)," ",vect(theta,l,3)
           !!     if (indextab(theta,l)==1) write(78,*) vect(theta,l,1)," ",vect(theta,l,2)," ",vect(theta,l,3)
     !      if (indextab(theta,l)==1) write(78,*) slice(theta,l,1)," ",slice(theta,l,2)," ",slice(theta,l,3)
           if (indextab(theta,l)==1) write(92,*) slice(theta,l,1)," ",slice(theta,l,2)," ",slice(theta,l,3)
         !   if (slice2(theta,l,1)<1.e-9) write(*,*) "voila : ",theta," ",l," ",indextheta(l)," ",thetatab(theta)&
         !                   if (vect(theta,l,1)<1.e-9) write(*,*) "voila : ",theta," ",l," ",indextheta(l)," ",thetatab(theta)&
           !,"  KT  ",kt
!!!         endif
!!!         write(78,*) slice2(theta,l,1)," ",slice2(theta,l,2)," ",slice2(theta,l,3)
        enddo
     !      write(78,*) slice(1,l,1)," ",slice(1,l,2)," ",slice(1,l,3)
           write(92,*) slice(1,l,1)," ",slice(1,l,2)," ",slice(1,l,3)
     enddo
     !     write(78,'(a)') "</DataArray>"
     !     write(78,'(a)') "</Points>"
     !     write(78,'(a)') "</Piece>"
     !     write(78,'(a)') "</StructuredGrid>"
     !     write(78,'(a)') "</VTKFile>"

!!!!       write(78,'(a,I6,a,I6)') 'LINES ' , nbtheta,' ',nbtheta+sum(thetatab)
!!!!        do theta=1,nbtheta
!!!!        write(78,*) thetatab(theta)," ",(sum(thetatab(1:theta-1))+(l-1),l=1,thetatab(theta))
!!!!        enddo
!!!!!       write(78,'(a,I6,a,I6)') 'POLYGONS ' , Ni+Ns+Nf-2,' ',Ni+Ns+Nf-2+sum(indextheta)
!!!!!        do l=1,Ni+Ns+Nf-2
!!!!!           write(78,*) indextheta(l)," ",(sum(indextheta(1:l-1))+(th-1),th=1,indextheta(l))
!!!!!        enddo
!!!!!!!!       write(78,'(a,I6)') 'POINT_DATA ' , sum(thetatab)
!!!!!       write(78,'(a,I6)') 'POINT_DATA ' , sum(indextheta)
!!!!!       write(78,'(a)') 'SCALARS values float'
!!!!!       write(78,'(a)') 'LOOKUP_TABLE default'
!!!!!!!        do theta=1,nbtheta
!!!!!!!     do l=1,thetatab(theta)
!!!!!!!           write(78,*) l*1.0
!!!!!!!     enddo
!!!!!!!        enddo
!!!!!        do l=1,Ni+Ns+Nf-2
!!!!!     do theta=1,indextheta(l)
!!!!!           write(78,*) l*1.0
!!!!!     enddo
!!!!!        enddo

          do l=1,Ni-1
             write(82,*) tail_courbe(Ni-l+1,1)," ",tail_courbe(Ni-l+1,2)," ",tail_courbe(Ni-l+1,3)
          enddo
!!!!     do l=1,Ns
!!!!        write(81,*) points_courbe(l,1)," ",points_courbe(l,2)
!!!!        write(82,*) points_courbe_equal(l,1)," ",points_courbe_equal(l,2)
!!!!     enddo
          do l=2,Nf
             write(85,*) head_courbe(l,1)," ",head_courbe(l,2)," ",head_courbe(l,3)
          enddo
     
!!!!     do l=1,thetatab(1)
!!!!        write(83,*) slice2(1,l,1)," ",slice2(1,l,2)
!!!!     enddo
!!!!     do l=1,thetatab(2)
!!!!        write(84,*) slice2(2,l,1)," ",slice2(2,l,2)
!!!!     enddo
     !     !write(80,*) kt,"    ",sum(skel)," ",long3," ",long2," ",long
          write(80,*) kt,"    ",nl," ",long3," ",long2," ",long
          do l=1,Ns
             write(81,*) points_courbe(l,1)," ",points_courbe(l,2)," ",points_courbe(l,3)
             write(82,*) points_courbe_equal(l,1)," ",points_courbe_equal(l,2)," ",points_courbe_equal(l,3)
          enddo
          !     do l=1,2 !Nf
          !        write(82,*) head_courbe(l,1)," ",head_courbe(l,2)," ",head_courbe(l,3)
          !     enddo
!!!!        do theta=1,nbtheta
          !!     theta=1
          !!     do l=1,thetatab(theta)
          !!           write(83,*) slice2(theta,l,1)," ",slice2(theta,l,2)," ",slice2(theta,l,3)
          !!     enddo
          !!     !theta=1+nbtheta/2
          !        do theta=1,nbtheta
          !theta=1+nbtheta/2
          do l=1,Ni+Ns+Nf-2 !thetatab(theta)
             !!do theta=1,indextheta(l)
             !!     if (vect(theta,l,1)<1.e-9) write(*,*) "voila : ",theta," ",l," ",indextheta(l)," ",thetatab(theta)&
             !!,"  KT  ",kt
             !!       write(84,*) slice2(theta,l,1)," ",slice2(theta,l,2)," ",slice2(theta,l,3)
             !!if (theta==1+nbtheta/2)  write(84,*) vect(theta,l,1)," ",vect(theta,l,2)," ",vect(theta,l,3)
             !!f (theta==1)            write(83,*) vect(theta,l,1)," ",vect(theta,l,2)," ",vect(theta,l,3)
             !!     if (theta<indextheta(l)+1) write(84,*) vect(theta,l,1)," ",vect(theta,l,2)," ",vect(theta,l,3)
             !!     if (appartientElmt(indextab,1+nbtheta/2,l).eqv..true.) write(84,*) vect(1+nbtheta/2,l,1)," ",vect(1+nbtheta/2,l,2)&
             !!," ",vect(1+nbtheta/2,l,3)
             !!     if (appartientElmt(indextab,1,l).eqv..true.) write(83,*) vect(1,l,1)," ",vect(1,l,2)," ",vect(1,l,3)
             if (indextab(1+nbtheta/2,l)==1) write(84,*) slice(1+nbtheta/2,l,1)," ",slice(1+nbtheta/2,l,2)&
                  ," ",slice(1+nbtheta/2,l,3)
             if (indextab(1,l)==1) write(83,*) slice(1,l,1)," ",slice(1,l,2)," ",slice(1,l,3)
             !!     if (vect(theta,l,2)>110.0) write(*,*) "WHHHYY  ",theta," ",l," ",indextheta(l)
             !!     enddo
          enddo
     close(79)    
     !close(86)    
     !close(87)    
     !close(78)    
     close(92)    
     close(81)
     close(82)
     close(83)
     close(84)
     close(85)
!     write(*,*) "okcut"
!     call cutheadskel(midline,skel,skel2,nl,ic,jc)
!     write(*,*) "okcut"
!
!     deallocate(midline)
  enddo
  close(80)
  write(*,*) "************************************************" 
  deallocate(rhoSlices2)
  !deallocate(rhoSlices)
  !  deallocate(rho0) 
  deallocate(xx,yy)
  !  deallocate(u,v,rho,rhou,rhov) 
  !  deallocate(rhoup,rhovp,rhop)
  !  deallocate(uu,vv,rhouu,rhovv,rhouup,rhovvp)
  !  deallocate(u0,v0)
  deallocate(tmp1,tmp2,tmpbool)
  deallocate(dir1,dir2,dir3,dir4,Nseed,skel,skel2,skel3)
  !deallocate(un,zero)
  deallocate(distslice)
  deallocate(points_courbe,points_courbe_equal,points_courbe_equal_ref)
  deallocate(slice_courbe,slice_courbe_equal,slice_courbemidLarge)
  deallocate(tail_courbe,head_courbe)
  deallocate(slice,slice2,thetatab,xTheta,yTheta,valTheta,valDist,slicetmp,longTh,longTheta,sth,dsth)
  deallocate(longslice,longslicei,longslicef,sslice,dsslice,slicemid)
  deallocate(indextab,indextheta,vect)
  deallocate(gradPhi) !,valTh)
  deallocate(cosTheta_tab,sinTheta_tab)
end program def3D
