module advection
  
  implicit none
  
contains  
  
  subroutine transport(Tps,pm,bool,interp,rho,u,v,rhou,rhov)
    
    use variables
    use doubler

    implicit none
    
    real(pr), dimension(:,:), intent(inout) :: rho, u, v, rhou, rhov
    integer :: i,j,k,m,l,nbreite
    real(pr) :: x,y,tt,g1,g2,g3,x0,y0,g4,g5, du, dv,delta,r1,r2
    real(pr), dimension(:),allocatable :: aa, bb,xxx,yyy
    integer, dimension(:),allocatable :: ip,jp
    real(pr), parameter :: zero = 1.e-6
    integer :: mm, kk, Kiter
    real(pr), intent(in) :: Tps
    integer, intent(in) :: pm, bool, interp
    real(pr) :: time, tt0,divumax,ratio,d11psi,d22psi,d12psi,coefa, maxiU,maxiV
    real(pr), dimension(:), allocatable :: localgridrhop, localgridrhoup, localgridrhovp, localgridrhouup, localgridrhovvp

    allocate(aa(1:4),bb(1:4))
    allocate(ip(1:4),jp(1:4))
    allocate(xxx(1:4),yyy(1:4))
    allocate(localgridrhop(16))
    allocate(localgridrhoup(16))
    allocate(localgridrhovp(16))
    allocate(localgridrhouup(16))
    allocate(localgridrhovvp(16))

  
!    do i=1,nx
!       do j=1,ny 
!          if (rho(i,j).gt.1E-6) then
!          !if (rho(i,j)>1E-1) then
!             u(i,j) = rhou(i,j)/rho(i,j) 
!             v(i,j) = rhov(i,j)/rho(i,j)              
!          else
!             rho(i,j) = 0.
!             rhou(i,j) = 0.
!             rhov(i,j) = 0.
!             rhouu(i,j) = 0.
!             rhovv(i,j) = 0.
!             u(i,j) = 0.
!             v(i,j) = 0.
!          endif
!       enddo
!    enddo
    
    if (pm.eq.1) then
      ! print*, 'calcul dt'
       tt = 1.
       tt0 = 1.
 
       ! autre calcul du pas de temps avec le determinant

       !Tps = 1.
       !do i=3,nx-2
       !   do j=3,ny-2
       !      ! je suppose que sur le bord il ne se passe rien
       !      ! je regarde seulement ou il y a de la masse
       !      ! calcul des derivees secondes de psi = um
       !      if (abs(rho(i,j)).ge.1E-6) then
       !         d11psi = (um(ind(i+1,j))-2.*um(ind(i,j))+ um(ind(i-1,j)))/dx**2
       !         d22psi = (um(ind(i,j+1))-2.*um(ind(i,j))+ um(ind(i,j-1)))/dy**2
       !         d12psi = ( (um(ind(i+1,j+1))-um(ind(i-1,j+1)))/(2.*dx) &
       !              &- (um(ind(i+1,j-1))-um(ind(i-1,j-1)))/(2.*dx))/(2.*dy)
       !         delta = (d11psi - d22psi)**2 + 4.*d12psi**2
       !         coefa = d11psi*d22psi-d12psi**2
       !         r2 = (-(d11psi+d22psi) + sqrt(delta))/(2.*coefa)
       !         r1 = (-(d11psi+d22psi) - sqrt(delta))/(2.*coefa)
       !         if (coefa.le.0.) then  ! alors le det est positif sauf entre ses racines
       !            if (r2.ge.0.) Tps = min(Tps, r2)
       !         else
       !            
       !            if (r1.ge.0.) Tps = min(Tps,r1)
       !         endif
       !       
       !      endif
       !   enddo
       !enddo
       
    endif
!!$
  !  if (pm.eq.-1) print*, 'k, Tps', Tps
    
  dt = Tps 

  !Tps = tfinal !1. 
  !dt = dtIn !Tps
  Kiter = floor(Tps/dt)

    !  on decoupe le pas de temps pour voir

 
      k = 0
      time = 0.
      
!!      do while (time.lt.Tps)
      do k=1,Kiter         
!!         if (bool.eq.1) then
         !calcul pas de temps
!!           dt =  Tps - time
        
           maxiU = -1000
           maxiV = -1000
!           do i = 1,nx-1
!            do j=1,ny-1
!                 if (u(i+1,j).gt.u(i,j)) then  ! dilatation
!                  dt = min(dt,2.*abs( dx/(u(i,j)-u(i+1,j))))
!                endif
!                if (v(i,j+1).gt.v(i,j)) then  ! dilatation
!                  dt = min(dt,2.*abs(dy/(v(i,j)-v(i,j+1))))
!                endif
!                if (abs(u(i,j))>1.e-6) then  ! dilatation
!                !if (u(i+1,j)<u(i,j)) then  ! dilatation
!             !if (rho(i,j)>1E-6) then
!                  maxiU=max(maxiU,abs(u(i,j)-u(i+1,j)))
!                endif
!                if (abs(v(i,j))>1.e-6) then  ! dilatation
!                !if (v(i,j+1)<v(i,j)) then  ! dilatation
!                  maxiV=max(maxiV,abs(v(i,j)-v(i,j+1)))
!                endif
!             !endif
!            enddo
!           enddo
!!           dt = min(min(dt,dx/(0.001+maxiU)),dy/(0.001+maxiV))*0.8_pr
!!           if ((dt.gt.(dx/maxiU)).or.(dt.gt.(dy/maxiV))) write(*,*) 'WARNING dt CFL ',min(dx/maxiU,dy/maxiV)
           !write(*,*) 'CFL ',min(dx/maxiU,dy/maxiV)
!!           if (dt>((dx/maxiU)+(dy/maxiV))) write(*,*) 'WARNING dt CFL'
!!
!           dt = min(dt, Tps - time)     
!        if (dt>0.1) then
!                dt = 0.1
!        endif
!!          endif 
!!           dt = min(dt, Tps - time)
!           time = time + dt
!           k = k+1
!           print*,"dt = ",dt, "time ",time


!!         print*, 'ite', k
         rhop = 0.
         rhoup = 0.
         rhovp = 0.
         rhouup = 0.
         rhovvp = 0.
       
       ! advection particules
       
       do i=1,nx
          do j=1,ny
             
             xp(i,j) = xx(i) + dt*u(i,j)
             yp(i,j) = yy(j) + dt*v(i,j)
          enddo
       enddo
       ! on remaille
       !interp=5 
       do m=1,nx
          do l=1,ny
             
             if (rho(m,l).gt.1E-6) then
             !if (rho(m,l)>1E-1) then
                x0 = xx(1)! 0.5_pr*dx -2._pr
                y0 = yy(1)!0.5_pr*dy  -2._pr
              !  x0=0.5_pr*dx !-2.0
              !  y0=0.5_pr*dy !-2.0
                !x0 = dx 
                !y0 = dy  
                
                g1 = rho(m,l)! + (maxval(rho)-minval(rho))*0.5_pr
                g2 = rho(m,l)*u(m,l)!rhou(m,l)
                g3 = rho(m,l)*v(m,l) !rhov(m,l)
                g4 = rhouu(m,l)
                g5 = rhovv(m,l)
                x = xp(m,l)
                y = yp(m,l)
                
                ! en x
                !ip(2) = int((x-x0)/dx)
                ip(2) = int((x-x0)/dx)
                ip(1) = ip(2) - 1
                ip(3) = ip(2) + 1
                ip(4) = ip(2) + 2   
                
                !xxx(2) = ((x - float(ip(2))*dx-x0)/dx)
                !xxx(2) = ((x - ip(2)*dx-x0)/dx)
                xxx(2) = abs(x/dx - ip(2) - x0/dx)
                xxx(1) = abs(xxx(2)+1)
                xxx(3) = abs(-1+xxx(2))
                xxx(4) = abs(-2+xxx(2))
                
                ip(2)=mod(ip(2)+nx,nx) +1
                ip(1)=mod(ip(1)+nx,nx) +1
                ip(3)=mod(ip(3)+nx,nx) +1
                ip(4)=mod(ip(4)+nx,nx) +1  

                if ((abs(xxx(2))>1.).or.(abs(xxx(3))>1.)) then
                        print*,"nyx ",x," ",x0," ",ip(2)
                        print*,"goooooooooooo xxx ",xxx(2)," ",xxx(3)
                endif
                if ((abs(xxx(1))<1.).or.(abs(xxx(1))>2.).or.(abs(xxx(4))<1.).or.(abs(xxx(4))>2.)) then
                        print*,"toootooo  xxx ",xxx(1),"    ",xxx(4)
                endif
                if ((xxx(1)<0).or.(xxx(2)<0).or.(xxx(3)<0).or.(xxx(4)<0)) then
                        print*,"nononononono xxx"
                endif
                !write(*,*)
                
                if (interp==1) then !Sigma_1(x)
                  aa(1) = 0.
                  aa(2) = 1.-xxx(2)
                  aa(3) = 1.-xxx(3)
                  aa(4) = 0.
                else if (interp==3) then !Sigma_3(x)
                  aa(1) = (1.-xxx(1))*(2.-xxx(1))*(3.-xxx(1))/6
                  aa(2) = (1.-xxx(2)*xxx(2))*(2.-xxx(2))*0.5
                  aa(3) = (1.-xxx(3)*xxx(3))*(2.-xxx(3))*0.5
                  aa(4) = (1.-xxx(4))*(2.-xxx(4))*(3.-xxx(4))/6
                else if (interp==4) then !M_4(x)
                  aa(1) =  (2.-xxx(1))**3/6. 
                  aa(2) = (2.-xxx(2))**3/6.-4.*(1.-xxx(2))**3/6 
                  aa(3) =  (2.-xxx(3))**3/6.-4.*(1.-xxx(3))**3/6 
                  aa(4) =  (2.-xxx(4))**3/6.    
                else if (interp==5) then !M'_4(x)
                  aa(1) = 0.5*((2.-xxx(1))**2)*(1.-xxx(1))
                  aa(2) = 1.-2.5*xxx(2)**2 +  1.5*xxx(2)**3
                  aa(3) = 1.-2.5*xxx(3)**2 +  1.5*xxx(3)**3
                  aa(4) = 0.5*((2.- xxx(4))**2)*(1.- xxx(4))   
                endif
                
                ! en y
                jp(2) = int((y-y0)/dy)
                jp(1) = jp(2) - 1
                jp(3) = jp(2) + 1
                jp(4) = jp(2) + 2   
                
                !yyy(2) = ((y - float(jp(2))*dy-y0)/dy)
                !yyy(2) = ((y - jp(2)*dy-y0)/dy)
                yyy(2) = abs(y/dy - jp(2) - y0/dy)
                yyy(1) = abs(yyy(2)+1)
                yyy(3) = abs(-1+yyy(2))
                yyy(4) = abs(-2+yyy(2))
                
                jp(2)=mod(jp(2)+ny,ny) +1
                jp(1)=mod(jp(1)+ny,ny) +1
                jp(3)=mod(jp(3)+ny,ny) +1
                jp(4)=mod(jp(4)+ny,ny) +1  

               if ((abs(yyy(2))>1).or.(abs(yyy(3))>1)) then
                       print*,"goooooooooooo  yyy ",yyy(2),"    ",yyy(3)
               endif
               if ((abs(yyy(1))<1).or.(abs(yyy(1))>2).or.(abs(yyy(4))<1).or.(abs(yyy(4))>2)) then
                       print*,"toootooo  yyy ",yyy(1),"    ",yyy(4)
               endif
                if ((yyy(1)<0).or.(yyy(2)<0).or.(yyy(3)<0).or.(yyy(4)<0)) then
                        print*,"nononononono yyy"
                endif
                
                if (interp==1) then !Sigma_1(x)
                  bb(1) =0.
                  bb(2) = 1.-yyy(2)
                  bb(3) = 1.-yyy(3)
                  bb(4) =0.
                else if (interp==3) then !Sigma_3(x)
                  bb(1) = (1.-yyy(1))*(2.-yyy(1))*(3.-yyy(1))/6
                  bb(2) = (1.-yyy(2)*yyy(2))*(2.-yyy(2))*0.5
                  bb(3) = (1.-yyy(3)*yyy(3))*(2.-yyy(3))*0.5
                  bb(4) = (1.-yyy(4))*(2.-yyy(4))*(3.-yyy(4))/6
                else if (interp==4) then !M_4(x)
                  bb(1) =  (2.-yyy(1))**3/6. 
                  bb(2) = (2.-yyy(2))**3/6.-4.*(1.-yyy(2))**3/6 
                  bb(3) =  (2.-yyy(3))**3/6.-4.*(1.-yyy(3))**3/6 
                  bb(4) =  (2.-yyy(4))**3/6.    
                else if (interp==5) then !M'_4(x)
                  bb(1) = 0.5*((2.-yyy(1))**2)*(1.-yyy(1))
                  bb(2) = 1.-2.5*yyy(2)**2 +  1.5*yyy(2)**3
                  bb(3) = 1.-2.5*yyy(3)**2 +  1.5*yyy(3)**3
                  bb(4) = 0.5*((2.- yyy(4))**2)*(1.- yyy(4))  
                endif
                
                do i=1,4
                   do j=1,4
                      localgridrhop(4*(i-1)+j) = rhop(ip(i),jp(j))
                      localgridrhoup(4*(i-1)+j) = rhoup(ip(i),jp(j))
                      localgridrhovp(4*(i-1)+j) = rhovp(ip(i),jp(j))
                      localgridrhouup(4*(i-1)+j) = rhouup(ip(i),jp(j))
                      localgridrhovvp(4*(i-1)+j) = rhovvp(ip(i),jp(j))
                   enddo
                enddo
                do i = 1,4
                   do j = 1,4                  

                      rhop(ip(i),jp(j))= rhop(ip(i),jp(j))+g1*aa(i)*bb(j)   
                      rhoup(ip(i),jp(j))= rhoup(ip(i),jp(j))+g2*aa(i)*bb(j) 
                      rhovp(ip(i),jp(j))= rhovp(ip(i),jp(j))+g3*aa(i)*bb(j)   
                      rhouup(ip(i),jp(j))= rhouup(ip(i),jp(j))+g4*aa(i)*bb(j) 
                      rhovvp(ip(i),jp(j))= rhovvp(ip(i),jp(j))+g5*aa(i)*bb(j) 

                      !if ((maxval(localgridrhop)-minval(localgridrhop))>1e-6) then
                      !   if (rhop(ip(i),jp(j))>maxval(localgridrhop)) rhop(ip(i),jp(j)) = maxval(localgridrhop)
                      !   if (rhop(ip(i),jp(j))<minval(localgridrhop)) rhop(ip(i),jp(j)) = minval(localgridrhop)
                      !endif
                    
                      !if ((maxval(localgridrhoup)-minval(localgridrhoup))>1e-6) then
                      !   if (rhoup(ip(i),jp(j))>maxval(localgridrhoup)) rhoup(ip(i),jp(j)) = maxval(localgridrhoup)
                      !   if (rhoup(ip(i),jp(j))<minval(localgridrhoup)) rhoup(ip(i),jp(j)) = minval(localgridrhoup)
                      !endif

                      !if ((maxval(localgridrhovp)-minval(localgridrhovp))>1e-6) then
                      !   if (rhovp(ip(i),jp(j))>maxval(localgridrhovp)) rhovp(ip(i),jp(j)) = maxval(localgridrhovp)
                      !   if (rhovp(ip(i),jp(j))<minval(localgridrhovp)) rhovp(ip(i),jp(j)) = minval(localgridrhovp)
                      !endif

                      !if ((maxval(localgridrhouup)-minval(localgridrhouup))>1e-6) then
                      !   if (rhouup(ip(i),jp(j))>maxval(localgridrhouup)) rhouup(ip(i),jp(j)) = maxval(localgridrhouup)
                      !   if (rhouup(ip(i),jp(j))<minval(localgridrhouup)) rhouup(ip(i),jp(j)) = minval(localgridrhouup)
                      !endif

                      !if ((maxval(localgridrhovvp)-minval(localgridrhovvp))>1e-6) then
                      !   if (rhovvp(ip(i),jp(j))>maxval(localgridrhovvp)) rhovvp(ip(i),jp(j)) = maxval(localgridrhovvp)
                      !   if (rhovvp(ip(i),jp(j))<minval(localgridrhovvp)) rhovvp(ip(i),jp(j)) = minval(localgridrhovvp)
                      !endif
                   enddo
                enddo
                
             endif
             
          enddo
       enddo
       
       rho = rhop! - (maxval(rhop)-minval(rhop))*0.5_pr
       rhou = rhoup
       rhov = rhovp
       rhouu = rhouup
       rhovv = rhovvp
       
       if (bool.eq.1) then 
       do i=1,nx
          do j=1,ny 
             if (rho(i,j).gt.1E-6) then
             !if (rho(i,j)>1E-1) then
                u(i,j) = rhou(i,j)/rho(i,j) 
                v(i,j) = rhov(i,j)/rho(i,j) 
             else
                rho(i,j) = 0.
                rhou(i,j) = 0.
                rhov(i,j) = 0.
                rhouu(i,j) = 0.
                rhovv(i,j) = 0.
                u(i,j) = 0.
                v(i,j) = 0.
             endif
          enddo
       enddo
       endif
       
    enddo

    deallocate(aa,bb,ip,jp)
    deallocate(xxx,yyy)
    deallocate(localgridrhop)
    deallocate(localgridrhoup)
    deallocate(localgridrhovp)
    deallocate(localgridrhouup)
    deallocate(localgridrhovvp)

    
    
  end subroutine transport
  

!! subroutine transportbis(Tps,pm)
!!    
!!    use variables
!!    use doubler
!!
!!    implicit none
!!    
!!    integer :: i,j,k,m,l,nbreite
!!    real(pr) :: x,y,tt,g1,g2,g3,x0,y0,g4,g5, du, dv,delta,r1,r2
!!    real(pr), dimension(:),allocatable :: aa, bb,xxx,yyy
!!    integer, dimension(:),allocatable :: ip,jp
!!    real(pr), parameter :: zero = 1.e-6
!!    integer :: mm, kk 
!!    real(pr), intent(inout) :: Tps
!!    integer, intent(in) :: pm
!!    real(pr) :: time, tt0,divumax,ratio,d11psi,d22psi,d12psi,coefa
!!    
!!    
!!    allocate(aa(1:6),bb(1:6))
!!    allocate(ip(1:6),jp(1:6))
!!    allocate(xxx(1:6),yyy(1:6))
!!
!!  
!!    do i=1,nx
!!       do j=1,ny 
!!          if (rho(i,j).gt.1E-6) then
!!             u(i,j) = rhou(i,j)/rho(i,j) 
!!             v(i,j) = rhov(i,j)/rho(i,j) 
!!          else
!!          !   rho(i,j) = 0.
!!             rhou(i,j) = 0.
!!             rhov(i,j) = 0.
!!             rhouu(i,j) = 0.
!!             rhovv(i,j) = 0.
!!             u(i,j) = 0.
!!             v(i,j) = 0.
!!          endif
!!       enddo
!!    enddo
!!    
!!    if (pm.eq.1) then
!!       print*, 'calcul dt'
!!       tt = 1.
!!       tt0 = 1.
!!     
!!
!!       ! autre calcul du pas de temps avec le determinant
!!
!!       !Tps = 1.
!!       !do i=3,nx-2
!!       !   do j=3,ny-2
!!       !      ! je suppose que sur le bord il ne se passe rien
!!       !      ! je regarde seulement ou il y a de la masse
!!       !      ! calcul des derivees secondes de psi = um
!!       !      if (abs(rho(i,j)).ge.1E-6) then
!!       !         d11psi = (um(ind(i+1,j))-2.*um(ind(i,j))+ um(ind(i-1,j)))/dx**2
!!       !         d22psi = (um(ind(i,j+1))-2.*um(ind(i,j))+ um(ind(i,j-1)))/dy**2
!!       !         d12psi = ( (um(ind(i+1,j+1))-um(ind(i-1,j+1)))/(2.*dx) &
!!       !              &- (um(ind(i+1,j-1))-um(ind(i-1,j-1)))/(2.*dx))/(2.*dy)
!!       !         delta = (d11psi - d22psi)**2 + 4.*d12psi**2
!!       !         coefa = d11psi*d22psi-d12psi**2
!!       !         r2 = (-(d11psi+d22psi) + sqrt(delta))/(2.*coefa)
!!       !         r1 = (-(d11psi+d22psi) - sqrt(delta))/(2.*coefa)
!!       !         if (coefa.le.0.) then  ! alors le det est positif sauf entre ses racines
!!       !            if (r2.ge.0.) Tps = min(Tps, r2)
!!       !         else
!!       !              if (r1.ge.0.) Tps = min(Tps,r1)
!!       !         endif
!!       !      endif
!!       !   enddo
!!       !enddo
!!       
!!    endif
!!
!!    !print*, 'Tps', Tps   
!!!!    dt = Tps
!!
!!    !  on decoupe le pas de temps pour voir
!! 
!!      k = 0
!!      time = 0.
!!
!!      do while (time.lt.Tps)
!!         
!!         !calcul pas de temps
!!!!         dt =  Tps - time
!!        
!!         do i = 2,nx
!!          do j=2,ny
!!              if (u(i+1,j).gt.u(i,j)) then  ! dilatation
!!!!                dt = min(dt,1.*abs( dx/(u(i,j)-u(i+1,j))))
!!              endif
!!                 if (v(i,j+1).gt.v(i,j)) then  ! dilatation
!!!!                dt = min(dt,1.*abs(dy/(v(i,j)-v(i,j+1))))
!!             endif
!!          enddo
!!       enddo
!!
!!!!         dt = min(dt, Tps - time)
!!!!         time = time + dt
!!         k = k+1
!!         print*, 'ite', k
!!         rhop = 0.
!!         rhoup = 0.
!!         rhovp = 0.
!!         rhouup = 0.
!!         rhovvp = 0.
!!       
!!       ! advection particules
!!       
!!       do i=1,nx
!!          do j=1,ny
!!             
!!!!             xp(i,j) = xx(i) +dt*u(i,j)
!!!!             yp(i,j) = yy(j) +dt*v(i,j)
!!          enddo
!!       enddo
!!       
!!       ! on remaille
!!       
!!       do m=1,nx
!!          do l=1,ny
!!             
!!             if (rho(m,l).gt.1E-6) then
!!                x0 = 0.5_pr*dx-2._pr
!!                y0 = 0.5_pr*dy-2._pr
!!                
!!                g1 = rho(m,l)
!!                g2 = rhou(m,l)
!!                g3 = rhov(m,l)
!!                g4 = rhouu(m,l)
!!                g5 = rhovv(m,l)
!!                x = xp(m,l)
!!                y = yp(m,l)
!!                
!!                ! en x
!!                ip(3) = int((x-x0)/dx)
!!                ip(1) = ip(3) - 2
!!                ip(2) = ip(3) - 1
!!                ip(4) = ip(3) + 1
!!                ip(5) = ip(3) + 2  
!!                ip(6) = ip(3) + 3  
!!                
!!                xxx(3) = (x - float(ip(3))*dx-x0)/dx
!!                xxx(1)=xxx(3)+2.
!!                xxx(2)=xxx(3)+1.
!!                xxx(4)=1.-xxx(3)
!!                xxx(5)=2.-xxx(3)
!!                xxx(6)=3.-xxx(3)
!!                
!!                ip(3)=mod(ip(3)+nx,nx) +1
!!                ip(2)=mod(ip(2)+nx,nx) +1
!!                ip(1)=mod(ip(1)+nx,nx) +1
!!                ip(4)=mod(ip(4)+nx,nx) +1
!!                ip(5)=mod(ip(5)+nx,nx) +1  
!!                ip(6)=mod(ip(6)+nx,nx) +1
!!
!!                aa(1) =  (3.-xxx(1))**3*(5.*xxx(1)-8.)*(xxx(1)-2.)/24.
!!                aa(2) = (xxx(2)-1.)*(xxx(2)-2.)*(25.*xxx(2)**3-114.*xxx(2)**2+153.*xxx(2)-48.)/24. 
!!                aa(3) = (1.-xxx(3))*(25.*xxx(3)**4-38.*xxx(3)**3-3.*xxx(3)**2+12.*xxx(3)+12.)/12.
!!                aa(4) = (1.-xxx(4))*(25.*xxx(4)**4-38.*xxx(4)**3-3.*xxx(4)**2+12.*xxx(4)+12.)/12.  
!!                aa(5) = (xxx(5)-1.)*(xxx(5)-2.)*(25.*xxx(5)**3-114.*xxx(5)**2+153.*xxx(5)-48.)/24. 
!!                aa(6) =  (3.-xxx(6))**3*(5.*xxx(6)-8.)*(xxx(6)-2.)/24.
!!             
!!                ! en y
!!                jp(3) = int((y-y0)/dy)
!!                jp(1) = jp(3) - 2
!!                jp(2) = jp(3) - 1
!!                jp(4) = jp(3) + 1
!!                jp(5) = jp(3) + 2 
!!                jp(6) = jp(3) + 3    
!!                
!!                yyy(3) = (y - float(jp(3))*dy-y0)/dy
!!                yyy(1)=yyy(3)+2.
!!                yyy(2)=yyy(3)+1.
!!                yyy(4)=1.-yyy(3)
!!                yyy(5)=2.-yyy(3)
!!                yyy(6)=3.-yyy(3)
!!                
!!                jp(3)=mod(jp(3)+ny,ny) +1
!!                jp(1)=mod(jp(1)+ny,ny) +1
!!                jp(2)=mod(jp(2)+ny,ny) +1
!!                jp(4)=mod(jp(4)+ny,ny) +1
!!                jp(5)=mod(jp(5)+ny,ny) +1  
!!                jp(6)=mod(jp(6)+ny,ny) +1 
!!                
!!                bb(1) =  (3.-yyy(1))**3*(5.*yyy(1)-8.)*(yyy(1)-2.)/24.
!!                bb(2) = (yyy(2)-1.)*(yyy(2)-2.)*(25.*yyy(2)**3-114.*yyy(2)**2+153.*yyy(2)-48.)/24. 
!!                bb(3) = (1.-yyy(3))*(25.*yyy(3)**4-38.*yyy(3)**3-3.*yyy(3)**2+12.*yyy(3)+12.)/12.
!!                bb(4) = (1.-yyy(4))*(25.*yyy(4)**4-38.*yyy(4)**3-3.*yyy(4)**2+12.*yyy(4)+12.)/12.  
!!                bb(5) = (yyy(5)-1.)*(yyy(5)-2.)*(25.*yyy(5)**3-114.*yyy(5)**2+153.*yyy(5)-48.)/24. 
!!                bb(6) =  (3.-yyy(6))**3*(5.*yyy(6)-8.)*(yyy(6)-2.)/24.
!!             
!!                              
!!                do i = 1,6
!!                   do j = 1,6                  
!!                      rhop(ip(i),jp(j))= rhop(ip(i),jp(j))+g1*aa(i)*bb(j)   
!!                      rhoup(ip(i),jp(j))= rhoup(ip(i),jp(j))+g2*aa(i)*bb(j) 
!!                      rhovp(ip(i),jp(j))= rhovp(ip(i),jp(j))+g3*aa(i)*bb(j)   
!!                      rhouup(ip(i),jp(j))= rhouup(ip(i),jp(j))+g4*aa(i)*bb(j) 
!!                      rhovvp(ip(i),jp(j))= rhovvp(ip(i),jp(j))+g5*aa(i)*bb(j) 
!!                   enddo
!!                enddo
!!                
!!             endif
!!             
!!          enddo
!!       enddo
!!       
!!       rho = rhop
!!       rhou = rhoup
!!       rhov = rhovp
!!       rhouu = rhouup
!!       rhovv = rhovvp
!!       
!!       
!!       do i=1,nx
!!          do j=1,ny 
!!             if (rho(i,j).gt.1E-6) then
!!                u(i,j) = rhou(i,j)/rho(i,j) 
!!                v(i,j) = rhov(i,j)/rho(i,j) 
!!             else
!!                !  rho(i,j) = 0.
!!                rhou(i,j) = 0.
!!                rhov(i,j) = 0.
!!                rhouu(i,j) = 0.
!!                rhovv(i,j) = 0.
!!                u(i,j) = 0.
!!                v(i,j) = 0.
!!             endif
!!          enddo
!!       enddo
!!       
!!    enddo
!!
!!    deallocate(aa,bb,ip,jp)
!!    deallocate(xxx,yyy)
!!    
!!  end subroutine transportbis
  

end module advection
