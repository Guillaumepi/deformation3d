program def3D
  use variables
  use advection
  use interpolation
  use AdvectionProblem
  use TimeScheme
  
  implicit none
  integer :: i, j, k, kt, idisplay, iUpdateDist, iter, picNum, l, xLeft, xRight, yLeft, yRight, pix1, pix2, l0, pos, sizeSkel, nl,lp,bool,loopbool,xl,xr,yr,yl,xskelL,yskelL,xskelR,yskelR,xcour 
  real(pr) :: Tps, threshold, surf, maxv, minimL, minimL_m, minimL_p, minimR, first
  real(pr), dimension(:,:), allocatable :: rho, uu, vv, u0, v0, u, v, rhou, rhov 
  real(pr) :: t, tPic, xi, yj, distW, distWb, maxLS, minLS, seuilLS
  real(pr), dimension(:,:), allocatable :: rhoSlices, rhoSlices2,LS
  integer, dimension(:,:), allocatable :: midline,ind
  real(pr), dimension(:,:), allocatable :: tmp, tmp1, tmp2, un, zero,distslice,voisin
  integer, dimension(:,:), allocatable :: dir1, dir2, dir3, dir4, Nseed, skel, skel2, tmpbool, slice
  integer, dimension(:), allocatable :: xLeft_tab, yLeft_tab, xRight_tab, yRight_tab

  N =  200 !180 !400!180
  nx = 200 !180 !400!180
  ny = 200 !180 !400!180
  eepsilon = 1.e-6_pr
  dt = 1
  threshold = 0.001
  dx = 1._pr !179.0/199.0!1!200/nx!200._pr/nx
  dy = 1._pr !179.0/199.0!200/ny!200._pr/ny
  dz = 1.
  Tps = 1.  

  allocate(rhoSlices2(nx,ny))
  allocate(un(N,N),zero(N,N))
  allocate(tmp(N,N),tmp1(N,N),tmp2(N,N),tmpbool(N,N))
  allocate(rhoSlices(nx,ny))
  allocate(xx(1:nx),yy(1:ny)) 
  allocate(xp(1:nx,1:ny),yp(1:nx,1:ny)) 
  allocate(rho0(1:nx,1:ny)) 
  allocate(rhou(1:nx,1:ny),rhov(1:nx,1:ny))  
  allocate(rhouu(1:nx,1:ny),rhovv(1:nx,1:ny)) 
  allocate(rhouup(1:nx,1:ny),rhovvp(1:nx,1:ny)) 
  allocate(rhop(1:nx,1:ny),rhoup(1:nx,1:ny),rhovp(1:nx,1:ny)) 
  allocate(u(1:nx,1:ny),v(1:nx,1:ny)) 
  allocate(rho(1:nx,1:ny))
  allocate(uu(1:nx,1:ny), vv(1:nx,1:ny))
  allocate(u0(1:nx,1:ny), v0(1:nx,1:ny))
  allocate(dir1(N,N),dir2(N,N),dir3(N,N),dir4(N,N),Nseed(N,N),skel(N,N),skel2(N,N))

  un = 1._pr
  zero = 0._pr

    do i=1,nx
      xx(i) = 1+(float(i)-1)*dx
    enddo  
    do j=1,ny
      yy(j) = 1+(float(j)-1)*dy
    enddo

  picNum = 140
    !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/TEST6/IMAGES2/Image_"//str(picNum)//".dat",&
!status='unknown')
    open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/mask200/Image_"//str(picNum)//".txt",&
!    !open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/BB18/rho0/rho"//str(picNum)//".txt",&
!status='unknown')
!    open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/mask400/Image_"//str(picNum)//".txt",&
!status='unknown')
    !open(unit=78,file="/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_ini0.txt",&
status='unknown')
!    open(unit=78,file="/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_ini400.txt",&
!status='unknown')
  do k=1,nx
    do j=1,ny
      read(78,*) rhoSlices2(k,j)
    enddo
  enddo
  close(78)

  rhoSlices = rhoSlices2!*0.0001
  !rhoSlices = nint(rhoSlices)!rhoSlices-(maxval(rhoSlices)+minval(rhoSlices))*0.5_pr
  rhoSlices = rhoSlices!-0.5_pr!(maxval(rhoSlices)+minval(rhoSlices))*0.5_pr
  !rhoSlices = sign(1._pr,rhoSlices)
  rhoSlices2 = rhoSlices!-0.5_pr

  allocate(slice(nx,2))
  slice(:,1) = ny/2
  do k=1,nx
    slice(k,2) = k
  enddo
  pos = nx/2
  yLeft = ny
  xLeft = pos
  yRight = 1
  xRight = pos
    
  open(unit=80,file='skeleton.txt',status='unknown')

  t = 0
  iter = 0
  idisplay = 10
  iUpdateDist = 5
  do kt = 1,615
  !open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesse2bis/vitesse"//str(kt+picNum-1)//".txt", &
!status='OLD')  
!  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesse200/vitesse"//str(kt+picNum-1)//".txt", &
  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesses200/vitesse"//str(kt+picNum-1)//".txt", &
  !open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/BB18/vitesses/vitesse"//str(kt+picNum-1)//"_0.txt", &
!  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesse5/vitesse"//str(kt+picNum-1)//".txt", &
!  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesse"//str(kt+picNum-1)//".txt", &
!  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/OpticalFlow/vitesse200/vitesse"//str(kt+picNum-1)//".txt", &
status='OLD')  
  !open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/OpticalFlow/Dense_Motion/Data/TEST6/im"//str(kt+picNum-1)//".pgm.motion", &
  !open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/OpticalFlow/Dense_Motion/Data/TEST6/im"//str(kt+picNum)//".pgm.motion", &
!status='OLD')  
!  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/LagrangienTO/vitesses/vitesse"//str(kt+picNum-1)//".txt", &
!status='OLD')  
  !open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesse"//str(kt+picNum-1)//".txt", status='OLD')  
  do j=1,nx
    do i=1,ny
      read(10,*) u0(j,i), v0(j,i)
    enddo
  enddo
  close(10) 
  bool = 0
  if ((mod(kt,idisplay)==0).or.(kt==1)) then
    rhoSlices2 = rhoSlices2 - 0.5_pr*(maxval(rhoSlices2) + minval(rhoSlices2))
    loopbool = 1
    do while (bool==0)
    !write(*,*)"LOOPBOOL =====       ",loopbool," ",kt
    if (loopbool==1) then
            write(*,*) "size thoslices2 == ",size(rhoSlices2)
            call updateDistance(rhoSlices2)
            dx = 1._pr
            dy = 1._pr
            first = 1._pr
    else
    if (kt>1) then
            N = floor((nl-1)*1.0/((sizeSkel-1)*1.0)*(nx-1)+1)
            write(*,*) "CHEEECK sizeSkel  :    ",nl," ",sizeSkel," SIZE  ",N," ",nx,"   MY N  ",(nl-1)*1.0/((sizeSkel-1)*1.0)*(nx-1)+1,"  first  ",floor(abs(nx-N)*0.5)
            if (allocated(LS)) deallocate(LS)
            allocate(LS(N,ny))
            !call interp(sizeSkel,1,nl,rhoSlices2,LS)  !!np>=ntot
            first = 1 !floor(abs(nx-N)*0.5)
            !call interp(nx,floor(abs(nx-N)*0.5),N,rhoSlices2,LS)  !!np>=ntot
            call interp(nx,1,N,rhoSlices2,LS)  !!np>=ntot
            deallocate(rhoSlices2,xx,yy)
            deallocate(dir1,dir2,dir3,dir4,Nseed,skel,skel2,tmpbool)
            allocate(rhoSlices2(N,ny),xx(N),yy(ny))
            allocate(dir1(N,ny),dir2(N,ny),dir3(N,ny),dir4(N,ny),Nseed(N,ny),skel(N,ny),skel2(N,ny),tmpbool(N,ny))
            rhoSlices2 = LS
            dx = (nx-1)*1._pr/((N-1)*1._pr)
            dy =  1._pr
            do i=1,N
              xx(i) = first+(float(i)-1)*dx
            enddo  
            do j=1,ny
              yy(j) = 1+(float(j)-1)*dy
            enddo
    endif
    !write(*,*) "CHEEECK sizeSkel  :    ",nl," ",sizeSkel
    endif
  !  if (kt==1) then
  !  do j=2,ny-1
  !    if ((rhoSlices2(pos,j)<0._pr).and.(rhoSlices2(pos,j+1)>0._pr)) yRight = j+1
  !    if ((rhoSlices2(pos,j)>0._pr).and.(rhoSlices2(pos,j+1)<0._pr)) yLeft = j
  !  enddo
  !  endif

    dir1 = 0
    dir2 = 0
    dir3 = 0
    dir4 = 0
    Nseed = 0
    skel = 0
    do i=2,N-1
      do j=2,ny-1
        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j))) dir1(i,j) = 1
        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j-1))) dir2(i,j) = 1
        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j-1))) dir3(i,j) = 1
        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j-1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j+1))) dir4(i,j) = 1
        Nseed(i,j) = dir1(i,j) + dir2(i,j) + dir3(i,j) + dir4(i,j)
        if ((Nseed(i,j)>=2).and.(rhoSlices2(i,j)>0)) then 
                skel(i,j) = 1
        !        write(*,*) " skel y : ",j
        endif
      enddo
    enddo
    !do i=1,N
    !  if (sum(skel(i,:))==0) write(*,*) "pixel manquant :=  ",i
    !   !if (rhoSlices2(103,i) >5) write(*,*) rhoSlices2(103,i)," ",i
    !  enddo
    l = 1
    pix1 = 0
    pix2 = 0
    do while ((pix1==1).or.(pix2==1).or.(l==1))
    pix1 = 0
    pix2 = 0
    l = l + 1
    skel2 = skel
    do i=2,N-1
      do j=2,ny-1
        maxv = 0
        if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
        if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
        if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
        if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
        if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
        if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
        if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
        if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
        Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
        if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i+1,j)==0).or.(skel(i-1,j)==0).or.(skel(i,j-1)==0)).and.((skel(i,j-1)==0).or.(skel(i,j+1)==0).or.(skel(i,j+1)==0))) then
                skel2(i,j) = 0
                pix1 = 1
        endif
      enddo
    enddo
    skel = skel2
    skel2 = skel
    do i=2,N-1
      do j=2,ny-1
        maxv = 0
        if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
        if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
        if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
        if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
        if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
        if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
        if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
        if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
        Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
        if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i,j+1)==0).or.(skel(i-1,j)==0).or.(skel(i,j-1)==0)).and.((skel(i+1,j)==0).or.(skel(i,j+1)==0).or.(skel(i-1,j)==0))) then
                skel2(i,j) = 0
                pix2 = 1
        endif
      enddo
    enddo
    skel = skel2
    enddo
    skel2 = skel
    do i=2,N-1
      do j=2,ny-1
        Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
        !if ((skel(i,j)==0).and.(Nseed(i,j)==2)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i-1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j+1)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
      enddo
    enddo
    skel = skel2
        
!   skel2 = 0
!   do i=2,N-1
!     do j=2,N-1
!       if (skel(i,j) .eq. 1) then
!           skel2(i,j) = 1
!           if (skel(i-1,j).eq.1) then
!                   maxv = maxval((/rhoSlices2(i+1,j-1),rhoSlices2(i+1,j),rhoSlices2(i+1,j+1),rhoSlices2(i,j)/))
!                   if (rhoSlices2(i+1,j-1).eq.maxv) skel2(i+1,j-1) = 1
!                   if (rhoSlices2(i+1,j).eq.maxv) skel2(i+1,j) = 1
!                   if (rhoSlices2(i+1,j+1).eq.maxv) skel2(i+1,j+1) = 1
!                   if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
!           endif
!           if (skel(i+1,j).eq.1) then
!                   maxv = maxval((/rhoSlices2(i-1,j-1),rhoSlices2(i-1,j),rhoSlices2(i-1,j+1),rhoSlices2(i,j)/))
!                   if (rhoSlices2(i-1,j-1).eq.maxv) skel2(i-1,j-1) = 1
!                   if (rhoSlices2(i-1,j).eq.maxv) skel2(i-1,j) = 1
!                   if (rhoSlices2(i-1,j+1).eq.maxv) skel2(i-1,j+1) = 1
!                   if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
!           endif
!           if (skel(i,j+1).eq.1) then
!                   maxv = maxval((/rhoSlices2(i-1,j-1),rhoSlices2(i,j-1),rhoSlices2(i+1,j-1),rhoSlices2(i,j)/))
!                   if (rhoSlices2(i-1,j-1).eq.maxv) skel2(i-1,j-1) = 1
!                   if (rhoSlices2(i,j-1).eq.maxv) skel2(i,j-1) = 1
!                   if (rhoSlices2(i+1,j-1).eq.maxv) skel2(i+1,j-1) = 1
!                   if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
!           endif
!           if (skel(i,j-1).eq.1) then
!                   maxv = maxval((/rhoSlices2(i-1,j+1),rhoSlices2(i,j+1),rhoSlices2(i+1,j+1),rhoSlices2(i,j)/))
!                   if (rhoSlices2(i-1,j+1).eq.maxv) skel2(i-1,j+1) = 1
!                   if (rhoSlices2(i,j+1).eq.maxv) skel2(i,j+1) = 1
!                   if (rhoSlices2(i+1,j+1).eq.maxv) skel2(i+1,j+1) = 1
!                   if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
!           endif
!           if (skel(i-1,j-1).eq.1) then
!                   maxv = maxval((/rhoSlices2(i,j+1),rhoSlices2(i+1,j+1),rhoSlices2(i+1,j),rhoSlices2(i,j)/))
!                   if (rhoSlices2(i,j+1).eq.maxv) skel2(i,j+1) = 1
!                   if (rhoSlices2(i+1,j+1).eq.maxv) skel2(i+1,j+1) = 1
!                   if (rhoSlices2(i+1,j).eq.maxv) skel2(i+1,j) = 1
!                   if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
!           endif
!           if (skel(i+1,j+1).eq.1) then
!                   maxv = maxval((/rhoSlices2(i,j-1),rhoSlices2(i-1,j-1),rhoSlices2(i-1,j),rhoSlices2(i,j)/))
!                   if (rhoSlices2(i,j-1).eq.maxv) skel2(i,j-1) = 1
!                   if (rhoSlices2(i-1,j-1).eq.maxv) skel2(i-1,j-1) = 1
!                   if (rhoSlices2(i-1,j).eq.maxv) skel2(i-1,j) = 1
!                   if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
!           endif
!           if (skel(i-1,j+1).eq.1) then
!                   maxv = maxval((/rhoSlices2(i,j-1),rhoSlices2(i+1,j-1),rhoSlices2(i+1,j),rhoSlices2(i,j)/))
!                   if (rhoSlices2(i,j-1).eq.maxv) skel2(i,j-1) = 1
!                   if (rhoSlices2(i+1,j-1).eq.maxv) skel2(i+1,j-1) = 1
!                   if (rhoSlices2(i+1,j).eq.maxv) skel2(i+1,j) = 1
!                   if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
!           endif
!           if (skel(i+1,j-1).eq.1) then
!                   maxv = maxval((/rhoSlices2(i,j+1),rhoSlices2(i-1,j+1),rhoSlices2(i-1,j),rhoSlices2(i,j)/))
!                   if (rhoSlices2(i,j+1).eq.maxv) skel2(i,j+1) = 1
!                   if (rhoSlices2(i-1,j+1).eq.maxv) skel2(i-1,j+1) = 1
!                   if (rhoSlices2(i-1,j).eq.maxv) skel2(i-1,j) = 1
!                   if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
!           endif
!       endif
!     enddo
!   enddo
!   skel = skel2
    tmpbool = 0

    if (kt==1) nl = sum(skel)
    sizeSkel = sum(skel)
    write(*,*) "SKEL : ",sizeSkel
    loopbool =loopbool + 1 
    !if ((loopbool>2).or.(kt==1)) then !(sizeSkel==nl).or.(kt==1)) then
            bool=1
    !else
            write(*,*) "MY SIZE ",sizeSkel," ",kt," ",nl
    !endif
    enddo

    !if (kt==1) allocate(midline(nl,2),distslice(2*nl,2),xLeft_tab(nl), yLeft_tab(nl), xRight_tab(nl), yRight_tab(nl))
    allocate(midline(sizeSkel,2),distslice(2*sizeSkel,2),xLeft_tab(sizeSkel), yLeft_tab(sizeSkel), xRight_tab(sizeSkel), yRight_tab(sizeSkel))
    !if (kt==1) allocate(midline(nl,2),xLeft_tab(nl), yLeft_tab(nl), xRight_tab(nl), yRight_tab(nl))
    !allocate(distslice(2*nl,2))!,xLeft_tab(nl), yLeft_tab(nl), xRight_tab(nl), yRight_tab(nl))
    write(*,*) "CHECKED"   
    if (kt == 1) then
    l=1
    midline = 0
    do j=1,N
      do k=1,ny
        if ((skel(j,k)==1).and.(l==1)) then
                midline(l,1) = j
                midline(l,2) = k
                if ((j==nx/2).and.(kt==1)) l0 = l
                l = l+1
        endif
      enddo
    enddo
    xskelL = midline(1,1)
    yskelL = midline(1,2)
  !write(*,*) "MIDLINE 11 ",midline(1,1)," ",midline(1,2)," ",yskelL
    else
    do i=2,N-1
      do j=2,ny-1
!        if (skel(j,k)==1) then
!                midline(l,1) = j
!                midline(l,2) = k
!                if ((j==nx/2).and.(kt==1)) l0 = l
!                l = l+1
          if (skel(i,j)==1) then
            Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
                    write(*,*) "midline ",sum(skel)," ",l
            if ((Nseed(i,j)==1).and.((abs(xskelL-i)<100).and.(abs(yskelL-j)<100))) then 
                    midline(1,1) = i
                    midline(1,2) = j
            endif
        endif
      enddo
    enddo
    endif
    do l=2,sum(skel)
    do i=2,N-1
      do j=2,ny-1
!        if (skel(j,k)==1) then
!                midline(l,1) = j
!                midline(l,2) = k
!                if ((j==nx/2).and.(kt==1)) l0 = l
!                l = l+1
          if (skel(i,j)==1) then
            Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
                    write(*,*) "midline ",sum(skel)," ",l," ",Nseed(i,j)," ",midline(l-1,1)
            if ((Nseed(i,j)>1).and.(((midline(l-1,1)==i).and.(midline(l-1,2).ne.j)).or.(midline(l-1,1)==i-1).or.(midline(l-1,1)==i+1))) then 
                    midline(l,1) = i
                    midline(l,2) = j
            endif
        endif
      enddo
    enddo
!  write(*,*) "MIDLINE 11 ",midline(1,1)," ",midline(1,2)," ",yskelL," ll ",l," ",size(midline,1)
    enddo
    write(*,*) "CHECKED L ",l," ",sum(skel)   
    lp=l0!+4
    !l0 = 1
!    if (kt==1) then
!      do k=2,ny-1
!        distslice(k,1) = dist(slice(k,1)*dx,slice(k,2)*dy,midline(l0-1,1)*dx,midline(l0-1,2)*dy)
!        distslice(k,2) = dist(slice(k,1)*dx,slice(k,2)*dy,midline(l0+1,1)*dx,midline(l0+1,2)*dy)
!      enddo
        !distslice(1,1) = dist(xLeft*dx,yLeft*dy,midline(l0-1,1)*dx,midline(l0-1,2)*dy)
        !distslice(1,2) = dist(xLeft*dx,yLeft*dy,midline(l0+1,1)*dx,midline(l0+1,2)*dy)
        !distslice(2,1) = dist(xRight*dx,yRight*dy,midline(l0-1,1)*dx,midline(l0-1,2)*dy)
        !distslice(2,2) = dist(xRight*dx,yRight*dy,midline(l0+1,1)*dx,midline(l0+1,2)*dy)
 !           if (kt==1) then
                    l=1
    do j=2,ny-1
      if ((rhoSlices2(midline(l,1),j)<0._pr).and.(rhoSlices2(midline(l,1),j+1)>0._pr)) yRight =  1+(j-1)*dy - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j+1
      if ((rhoSlices2(midline(l,1),j)>0._pr).and.(rhoSlices2(midline(l,1),j+1)<0._pr)) yLeft =  1+(j-1)*dy - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j
    enddo
            xLeft_tab(l) = midline(l,1)
            xRight_tab(l) = midline(l,1)
            yLeft_tab(l) = yLeft
            yRight_tab(l) = yRight
    write(*,*) "CHECKED"   
          distslice(l,1) = dist(xx(xLeft_tab(l)),yy(yLeft_tab(l)),xx(midline(l,1)),yy(midline(l,2)))
    write(*,*) "CHECKED"   
                    l=sum(skel)
    do j=2,ny-1
      if ((rhoSlices2(midline(l,1),j)<0._pr).and.(rhoSlices2(midline(l,1),j+1)>0._pr)) yRight =  1+(j-1)*dy - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j+1
      if ((rhoSlices2(midline(l,1),j)>0._pr).and.(rhoSlices2(midline(l,1),j+1)<0._pr)) yLeft =  1+(j-1)*dy - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j
    enddo
    write(*,*) "CHECKED"   
            xLeft_tab(l) = midline(l,1)
            xRight_tab(l) = midline(l,1)
            yLeft_tab(l) = yLeft
            yRight_tab(l) = yRight
          distslice(1,2) = dist(xx(xLeft_tab(l)),yy(yLeft_tab(l)),xx(midline(l,1)),yy(midline(l,2)))
    write(*,*) "CHECKED"   
        do l=2,sum(skel)-1
!    if (kt==1) then
    do j=2,ny-1
      if ((rhoSlices2(midline(l,1),j)<0._pr).and.(rhoSlices2(midline(l,1),j+1)>0._pr)) yRight =  1+(j-1)*dy - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j+1
      if ((rhoSlices2(midline(l,1),j)>0._pr).and.(rhoSlices2(midline(l,1),j+1)<0._pr)) yLeft =  1+(j-1)*dy - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j
    enddo
    write(*,*) "CHECKED LL ",l   
!    endif
        !do l=l0,l0+5!sum(skel)-1
        !do l=lp,lp!sum(skel)-1
        !l=l0
            xLeft_tab(l) = midline(l,1)
            xRight_tab(l) = midline(l,1)
            yLeft_tab(l) = yLeft
            yRight_tab(l) = yRight
          distslice(l,1) = dist(xx(xLeft_tab(l)),yy(yLeft_tab(l)),xx(midline(l-1,1)),yy(midline(l-1,2)))
          distslice(l,2) = dist(xx(xLeft_tab(l)),yy(yLeft_tab(l)),xx(midline(l+1,1)),yy(midline(l+1,2)))
          distslice(sizeSkel+l,1) = dist(xx(xRight_tab(l)),yy(yRight_tab(l)),xx(midline(l-1,1)),yy(midline(l-1,2)))
          distslice(sizeSkel+l,2) = dist(xx(xRight_tab(l)),yy(yRight_tab(l)),xx(midline(l+1,1)),yy(midline(l+1,2)))
          !distslice(nl+l,1) = dist(xx(xRight_tab(l)),yy(yRight_tab(l)),xx(midline(l-1,1)),yy(midline(l-1,2)))
          !distslice(nl+l,2) = dist(xx(xRight_tab(l)),yy(yRight_tab(l)),xx(midline(l+1,1)),yy(midline(l+1,2)))
            !if (l==lp) write(*,*) "MYDIST ==            ",midline(l-1,1)," ",midline(l-1,2)," ",midline(l,1)," ",midline(l,2)," ",midline(l+1,1)," ",midline(l+1,2)," ",yLeft," ",yRight," ",dist(xLeft_tab(l)*dx,yLeft_tab(l)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)," ",dist(xLeft_tab(l)*dx,yLeft_tab(l)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)," ",dist(xRight_tab(l)*dx,yRight_tab(l)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)," ",dist(xRight_tab(l)*dx,yRight_tab(l)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)
          !distslice(2*l,1) = dist(xRight_tab(l)*dx,yRight_tab(l)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)
          !distslice(2*l,2) = dist(xRight_tab(l)*dx,yRight_tab(l)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)
        !distslice(l,1) = dist(xLeft*dx,yLeft*dy,midline(l0-1,1)*dx,midline(l0-1,2)*dy)
        !distslice(l,2) = dist(xLeft*dx,yLeft*dy,midline(l0+1,1)*dx,midline(l0+1,2)*dy)
        !distslice(2*l,1) = dist(xRight*dx,yRight*dy,midline(l0-1,1)*dx,midline(l0-1,2)*dy)
        !distslice(2*l,2) = dist(xRight*dx,yRight*dy,midline(l0+1,1)*dx,midline(l0+1,2)*dy)
        !write(*,*)"MAXVAL DIST ==     ",distslice(l,1)," ",distslice(l,2),"  ",distslice(nl+l,1)," ",distslice(nl+l,2),"     ",nl+l

        enddo
    write(*,*) "CHECKED"   
!            endif
!    endif
!  write(*,*) "MIDLINE 11 ",midline(1,1)," ",midline(1,2)," ",yskelL
    l = 1
    xLeft = 0
    yLeft = 0
    xRight = 0
    yRight = 0
    do k=1,N
      do j=1,ny
!        if ((abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))<minimL).and.(det(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy,midline(l,1)*dx,midline(l,2)*dy) <= 0).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))<1._pr)) then
!          xLeft = j
!          yLeft = k
!          minimL = abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))
!        endif
!        if ((abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))<minimR).and.(det(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy,midline(l,1)*dx,midline(l,2)*dy) >= 0).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))<1._pr)) then
!          xRight = j
!          yRight = k
!          minimR = abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))
!        endif
        !if (dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(1,1)*dx,midline(1,2)*dy)<distslice(1,1)) tmpbool(j,k) = 1
        if (abs(dist(xx(j),yy(k),xx(midline(1,1)),yy(midline(1,2)))-distslice(1,1))<eepsilon) then
                xl = j
                yl = k
        endif
      enddo
    enddo
    write(*,*) "CHECKED"   
    do k=1,N
      do j=1,ny
        !if (dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(1,1)*dx,midline(1,2)*dy)<dist(1+(float(xl)-1)*dx,1+(float(yl)-1)*dy,midline(1,1)*dx,midline(1,2)*dy)) tmpbool(j,k) = 1
        !if (dist(xx(j),yy(k),xx(midline(1,1)),yy(midline(1,2)))<dist(xx(xl),yy(yl),xx(midline(1,1)),yy(midline(1,2)))) skel2(j,k) = 1
      enddo
    enddo
!      xLeft_tab(l) = xLeft
!      yLeft_tab(l) = yLeft
!      xRight_tab(l) = xRight
!      yRight_tab(l) = yRight
      l = sum(skel)
      xLeft = 0
      yLeft = 0
      xRight = 0
      yRight = 0
      do k=1,N
        do j=1,ny
!          if ((abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))<minimL).and.(det(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l,1)*dx,midline(l,2)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy) <= 0).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))<1._pr)) then
!            xLeft = j
!            yLeft = k
!            minimL = abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))
!          endif
!          if ((abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))<minimR).and.(det(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l,1)*dx,midline(l,2)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy) >= 0).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))<1._pr)) then
!            xRight = j
!            yRight = k
!            minimR = abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))
!          endif
        !if (dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(sum(skel),1)*dx,midline(sum(skel),2)*dy)<distslice(1,2)) tmpbool(j,k) = 1
        if (abs(dist(xx(j),yy(k),xx(midline(sum(skel),1)),yy(midline(sum(skel),2)))-distslice(1,2))<eepsilon) then
                xr = j
                yr = k
        endif
        enddo
      enddo
!    do k=1,N
!      do j=1,ny
!        if (dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(sum(skel),1)*dx,midline(sum(skel),2)*dy)<dist(1+(float(xr)-1)*dx,1+(float(yr)-1)*dy,midline(sum(skel),1)*dx,midline(sum(skel),2)*dy)) tmpbool(j,k) = 1
!      enddo
!    enddo
!        xLeft_tab(l) = xLeft
!        yLeft_tab(l) = yLeft
!        xRight_tab(l) = xRight
!        yRight_tab(l) = yRight
    if (kt>1) then
    write(*,*) "CHECKED"   
    do l=2,sum(skel)-1
    !do l=l0,l0+5!sum(skel)-1
    !do l=lp,lp!sum(skel)-1
    !l = l0
    xLeft = 0
    yLeft = 0
    xRight = 0
    yRight = 0
    minimL = 1e6
    minimL_m = 1e6
    minimL_p = 1e6
    minimR = 1e6
    do k=1,N
      do j=1,ny
!        if (((abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))<minimL).and.(abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))<minimL)).and.(det(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy) <= 0)) then
        !if (((abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))<minimL).and.(abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))<minimL)).and.(det(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy) <= 0).and.(abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))<0.252_pr).and.(abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))<0.252_pr)) then
!        if (((abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))<minimL).and.(abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))<minimL)).and.(det(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy) <= 0).and.(abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))<1._pr).and.(abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))<1._pr)) then
        !if (((abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,1+(float(midline(l-1,1))-1)*dx,1+(float(midline(l-1,2))-1)*dy)-distslice(l,1))<minimL).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,1+(float(midline(l+1,1))-1)*dx,1+(float(midline(l+1,2))-1)*dy)-distslice(l,2))<minimL)).and.(det(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy) <= 0).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))<1._pr).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))<1._pr)) then
        if (((abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(l,1))<minimL).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(l,2))<minimL)).and.(det(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)),xx(midline(l-1,1)),yy(midline(l-1,2))) <= 0).and.(abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(l,1))<1._pr).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(l,2))<1._pr)) then
        !if (((abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(l,1))<minimL).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(l,2))<minimL)).and.(det(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)),xx(midline(l-1,1)),yy(midline(l-1,2))) <= 0)) then
          xLeft = j
          yLeft = k
          !write(*,*) "LEFT DIST ==          ",dist(1+(j-1)*dx,1+(k-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)," ",dist(1+(j-1)*dx,1+(k-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)
          !if (l==1) write(*,*) "DISTT          ",abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))
          !if (l==1) write(*,*) "WHHYYYYY  ",k
!          minimL = max(abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1)),abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2)))
          !minimL = max(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1)),abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2)))
          minimL = max(abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(l,1)),abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(l,2)))
          !minimL_m = abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))
          !minimL_p = abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))
        endif
!        if (((abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))<minimR).and.(abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))<minimR)).and.(det(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy) >= 0)) then
        !if (((abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))<minimR).and.(abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))<minimR)).and.(det(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy) >= 0).and.(abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))<0.252_pr).and.(abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))<0.252_pr)) then
!        if (((abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))<minimR).and.(abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))<minimR)).and.(det(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy) >= 0).and.(abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))<1._pr).and.(abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))<1._pr)) then
        !if (((abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))<minimR).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))<minimR)).and.(det(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy) >= 0).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))<1._pr).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))<1._pr)) then
        if (((abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(sizeSkel+l,1))<minimR).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(sizeSkel+l,2))<minimR)).and.(det(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)),xx(midline(l-1,1)),yy(midline(l-1,2))) >= 0).and.(abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(sizeSkel+l,1))<1._pr).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(sizeSkel+l,2))<1._pr)) then
        !if (((abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(nl+l,1))<minimR).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(nl+l,2))<minimR)).and.(det(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)),xx(midline(l-1,1)),yy(midline(l-1,2))) >= 0).and.(abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(nl+l,1))<1._pr).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(nl+l,2))<1._pr)) then
        !if (((abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(nl+l,1))<minimR).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(nl+l,2))<minimR)).and.(det(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)),xx(midline(l-1,1)),yy(midline(l-1,2))) >= 0)) then
          xRight = j
          yRight = k
          !write(*,*) "RIGHT DIST ==          ",dist(1+(j-1)*dx,1+(k-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)," ",dist(1+(j-1)*dx,1+(k-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)
!          minimR = max(abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1)),abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2)))
          !minimR = max(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1)),abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2)))
          minimR = max(abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(sizeSkel+l,1)),abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(sizeSkel+l,2)))
          !minimR = max(abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(nl+l,1)),abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(nl+l,2)))
!          minimR = max(abs(dist(j*dx,k*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(2*l,1)),abs(dist(j*dx,k*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(2*l,2)))
        endif
      enddo
    enddo
!      write(*,*)"TEST LEFT        ",abs(dist(xLeft*dx,yLeft*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1)),"        ",abs(dist(xLeft*dx,yLeft*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2)),"  COORD  ",xLeft," ",yLeft
!      write(*,*)"TEST RIGHT        ",abs(dist(xRight*dx,yRight*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1)),"        ",abs(dist(xRight*dx,yRight*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2)),"  COORD  ",xRight," ",yRight
      xLeft_tab(l) = xLeft
      yLeft_tab(l) = yLeft
      xRight_tab(l) = xRight
      yRight_tab(l) = yRight
!    write(*,*) "CHECKED l ",l," ",sizeSkel   
    enddo
    endif
    write(*,*) "CHECKED"   
    do l=2,sum(skel)-1
!    write(*,*)"CHECKED 3",sum(skel)," ",xLeft_tab(l)," ",xRight_tab(l)," LLLL   ",l
!    write(*,*)"CHECKED 4",sum(skel)," ",yLeft_tab(l)," ",yRight_tab(l)," LLLL   ",l
    !do l=l0,l0+5
    !do l=lp,lp
      if ((xRight_tab(l) == 0).or.(yRight_tab(l) == 0).or.(yLeft_tab(l) == 0).or.(xLeft_tab(l) == 0)) then
              write(*,*) "INTROUVABLE : ",l
      else
      tmpbool(xLeft_tab(l),yLeft_tab(l)) = 1
      tmpbool(xRight_tab(l),yRight_tab(l)) = 1
      endif
    enddo
    !tmpbool(xLeft,yLeft) = 1
    !tmpbool(xRight,yRight) = 1
    !write(*,*)"xleft yleft xright yright ==       ",xLeft," ",yLeft," ",xRight," ",yRight


!    tmpbool = 0
    !do k=1,ny
    !  tmpbool(slice(k,1),slice(k,2)) = 1
    !enddo
    !do l=1,size(midline,1)
    !l = l0
    do l=l0,l0+5
!    l=l0
    do k=1,N
      do j=1,ny
!        if ((det(j*dx,k*dy,slice(1,1)*dx,slice(1,2)*dy,midline(1,1)*dx,midline(1,2)*dy) <= 0).and.(det(j*dx,k*dy,midline(1,1)*dx,midline(1,2)*dy,slice(size(slice,1),1)*dx,slice(size(slice,1),2)*dy) <= 0).and.(det(j*dx,k*dy,slice(size(slice,1),1)*dx,slice(size(slice,1),2)*dy,midline(size(midline,1),1)*dx,midline(size(midline,1),2)*dy) <= 0).and.(det(j*dx,k*dy,midline(size(midline,1),1)*dx,midline(size(midline,1),2)*dy,slice(1,1)*dx,slice(1,2)*dy) <= 0)) tmpbool(j,k) = 1
!        if ((det(j*dx,k*dy,xRight*dx,yRight*dy,midline(l-2,1)*dx,midline(l-2,2)*dy) <= 0).and.(det(j*dx,k*dy,midline(l-2,1)*dx,midline(l-2,2)*dy,xLeft*dx,yLeft*dy) <= 0).and.(det(j*dx,k*dy,xLeft*dx,yLeft*dy,midline(l+2,1)*dx,midline(l+2,2)*dy) <= 0).and.(det(j*dx,k*dy,midline(l+2,1)*dx,midline(l+2,2)*dy,xRight*dx,yRight*dy) <= 0)) tmpbool(j,k) = 1
!        if ((det(j*dx,k*dy,xRight_tab(l)*dx,yRight_tab(l)*dy,midline(l-2,1)*dx,midline(l-2,2)*dy) <= 0).and.(det(j*dx,k*dy,midline(l-2,1)*dx,midline(l-2,2)*dy,xLeft_tab(l)*dx,yLeft_tab(l)*dy) <= 0).and.(det(j*dx,k*dy,xLeft_tab(l)*dx,yLeft_tab(l)*dy,midline(l+2,1)*dx,midline(l+2,2)*dy) <= 0).and.(det(j*dx,k*dy,midline(l+2,1)*dx,midline(l+2,2)*dy,xRight_tab(l)*dx,yRight_tab(l)*dy) <= 0)) tmpbool(j,k) = 1
      enddo
    enddo
    enddo
    !tmpbool(midline(1,1),midline(1,2)) = 0
    !skel(midline(1,1),midline(1,2)) = 0
    !tmpbool(midline(size(midline,1),1),midline(size(midline,1),2)) = 0
    tmpbool(midline(1,1),midline(1,2)) = 0
    skel2(midline(1,1),midline(1,2)) = 0
    tmpbool(midline(2,1),midline(2,2)) = 0
    skel2(midline(2,1),midline(2,2)) = 0
    tmpbool(xl,yl) = 0
    skel2(xl,yl) = 0
    !tmpbool(midline(sum(skel),1),midline(sum(skel),2)) = 0
    !skel(midline(sum(skel),1),midline(sum(skel),2)) = 0
    !do l=lp,lp!sum(skel)
    !tmpbool(midline(l,1),midline(l,2)) = 0
    !skel(midline(l,1),midline(l,2)) = 0
    !!tmpbool(midline(l+1,1),midline(l+1,2)) = 0
    !!skel(midline(l+1,1),midline(l+1,2)) = 0
    !enddo

    !deallocate(distslice)!,xLeft_tab,yLeft_tab,xRight_tab,yRight_tab)
    deallocate(midline,distslice,xLeft_tab,yLeft_tab,xRight_tab,yRight_tab)
        
    !open(unit=79,file='results/rhomatlab'//str(kt+140-1)//'.txt',status='unknown')
    !open(unit=79,file='results/rho'//str(kt+140-1)//'.txt',status='unknown')
    !open(unit=79,file='results/skel'//str(kt+140-1)//'.vtk',status='unknown')
    open(unit=79,file='results/skelett'//str(kt+140-1)//'.vtk',status='unknown')
      write(79,'(1A26)') '# vtk DataFile Version 2.0'
      write(79,'(a)') 'rho'
      write(79,'(a)') 'ASCII'
      write(79,'(a)') 'DATASET STRUCTURED_POINTS'
      write(79,'(a,I4,I4,I4)') 'DIMENSIONS', N,ny,1 !200,200,1 !nx,ny,1
      write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
      write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,1. !1.,1.,1.
      !write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 24.7687, 24.7687, 24.0 !179.0/199.0,179.0/199.0,1.0!dx,dy, 1.
      write(79,'(a,I6)') 'POINT_DATA          ' , N*ny !200*200 !nx*ny!nx*ny
      !write(79,'(a)') 'SCALARS density float 1' !SCALARS values double
      write(79,'(a)') 'SCALARS values double'
      write(79,'(a)') 'LOOKUP_TABLE default'
    
    do k=1,ny
      do j=1,N
!        write(79,*) rhoSlices2(j,k)
!        write(79,*) (1-tmpbool(j,k))*(rhoSlices2(j,k)*(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k)) + minval(rhoSlices2)*tmpbool(j,k)
!        write(79,*) rhoSlices2(j,k)*(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k)
         write(79,*) skel2(j,k)
!         write(79,*) tmpbool(j,k)
      enddo
    enddo
    write(80,*) kt,"    ",sum(skel)
    write(*,*)"ktkt ",kt," ",bool
    if (kt>1) then
            N = nx
            dx = 1._pr
            dy = 1._pr
            first = 1._pr
            if (allocated(LS)) deallocate(LS)
            deallocate(rhoSlices2,xx,yy)
            allocate(rhoSlices2(nx,ny),xx(nx),yy(ny))
            deallocate(dir1,dir2,dir3,dir4,Nseed,skel,skel2,tmpbool)
            allocate(dir1(N,ny),dir2(N,ny),dir3(N,ny),dir4(N,ny),Nseed(N,ny),skel(N,ny),skel2(N,ny),tmpbool(N,ny))
            do i=1,N
              xx(i) = first+(float(i)-1)*dx
            enddo  
            do j=1,ny
              yy(j) = 1+(float(j)-1)*dy
            enddo
    endif
  endif

 u = u0*dx
 v = v0*dy
 call SetInitialCondition(rhoSlices(:,:),u,v,rho,rhou,rhov)
 dt = 1._pr !0.9_pr/(eepsilon + maxval(abs(u))/dx + maxval(abs(v))/dy) ! abs() necessaire au cas ou a<0
 do while (t<float(kt))
 !do while (t<1._pr)
      t = t + dt
      write(*,*)"t ",t," ",kt," ",t-kt+1
      iter = iter + 1
      call AdvanceTime(kt, dt, 0, 3, 4, 1, rho, u, v, rhou, rhov)
  enddo

 rhoSlices(:,:) = rho(:,:)!/maxval(rho)!-seuilLS
 rhoSlices2(:,:) = rhoSlices(:,:)

 if (mod(kt,idisplay)==0) then
    close(79)    
    endif
 enddo  
 close(80)

 deallocate(rhoSlices2)
 deallocate(rhoSlices)
 deallocate(rho0) 
 deallocate(xx,yy)
 deallocate(u,v,rho,rhou,rhov) 
 deallocate(rhoup,rhovp,rhop)
 deallocate(uu,vv,rhouu,rhovv,rhouup,rhovvp)
 deallocate(u0,v0)
 deallocate(tmp1,tmp2,tmpbool)
 deallocate(dir1,dir2,dir3,dir4,Nseed,skel)
 deallocate(un,zero)
 deallocate(slice)
! deallocate(midline,distslice,xLeft_tab,yLeft_tab,xRight_tab,yRight_tab)
end program def3D

!   "Convert an integer to string."
character(len=3) function str(k)
    integer, intent(in) :: k
    write (str, '(I3.3)') k
    str = adjustl(str)
end function str
