program def3D
  use variables
  use interpolation
  use AdvectionProblem
  use libBezier

  implicit none
  integer :: i, j, k, kt, idisplay, iUpdateDist, iter, picNum, l, pix1, pix2, l0, pos, sizeSkel, nl,lp,bool,loopbool,xl,xr,yr,yl,xskelL,yskelL,xskelR,yskelR,ii,lph,lpt 
  real(pr) :: Tps, threshold, surf, maxv, minimL, minimL_m, minimL_p, minimR, long, tb, dtb, long2, long3, dti, dtf, dsi, dsf
  real(pr),dimension(:),allocatable :: longslice, longslicei, longslicef
  real(pr), dimension(:,:), allocatable :: rho, uu, vv, u0, v0, u, v, rhou, rhov 
  real(pr) :: t, tPic, xi, yj, distW, distWb, xLeft, yLeft, xRight, yRight
  real(pr), dimension(:,:), allocatable :: rhoSlices, rhoSlices2
  integer, dimension(:,:), allocatable :: midline,midlinebis,ind
  real(pr), dimension(:,:), allocatable :: tmp, tmp1, tmp2, un, zero,distslice,voisin
  integer, dimension(:,:), allocatable :: dir1, dir2, dir3, dir4, Nseed, skel, skel2, tmpbool, skel3
  real(pr) :: px,py,s0,sinit,tbm,tbp,s,ds
  real(pr),dimension(:),allocatable :: sslice,dsslice
  real(pr),dimension(:,:),allocatable :: points_control, points_courbe, points_courbe_equal, tail_courbe, head_courbe
  real(pr),dimension(:,:,:),allocatable :: slicecontrolm, slicecontrolf, slicecontrol,slicemid,slicecontroli
  real(pr),dimension(:,:,:),allocatable :: slice_control, slice_courbe, slice_courbe_equal
  integer :: Ns,Ni,Nf,nt,errorl,errorr,itail,ihead
  real(pr) :: deltal,deltar,disttail,disthead,rhead,rtail,disttaillY,disttailrY,distheadlY,distheadrY
  real(pr),dimension(:,:,:),allocatable :: slice,slice2
  integer,dimension(:),allocatable :: thetatab
  real(pr),dimension(:),allocatable :: xTheta,yTheta
  real(pr),dimension(:,:),allocatable :: valDist,valTheta
  integer :: th,theta,nbtheta,boolskel
  real(pr) :: PI 
  real(pr) :: cosPhi,sinPhi

  PI = acos(-1.0_pr)
  N =  200 
  nx = 200 
  ny = 200 
  eepsilon = 1.e-6_pr
  dt = 1
  threshold = 0.001
  dx = 1._pr 
  dy = 1._pr 
  dz = 1.
  Tps = 1.
  Ns = 1000
  Ni = 10
  Nf = 100
  dtb = 1._pr/(Ns-1)
  nbtheta = 2
  allocate(slicemid(nbtheta,Ns,2))
  allocate(longslice(nbtheta),longslicei(nbtheta),longslicef(nbtheta),sslice(nbtheta),dsslice(nbtheta))
  allocate(thetatab(nbtheta),xTheta(nbtheta),yTheta(nbtheta),valTheta(Ns+Ni+Nf-2,nbtheta),valDist(Ns+Ni+Nf-2,nbTheta))
  allocate(head_courbe(Nf,2),tail_courbe(Ni,2))
  allocate(points_courbe(Ns,2),points_courbe_equal(Ns,2))
  allocate(rhoSlices2(nx,ny))
  allocate(un(N,N),zero(N,N))
  allocate(tmp(N,N),tmp1(N,N),tmp2(N,N),tmpbool(N,N))
  allocate(rhoSlices(nx,ny))
  allocate(xx(1:nx),yy(1:ny)) 
  allocate(xp(1:nx,1:ny),yp(1:nx,1:ny)) 
  allocate(rho0(1:nx,1:ny)) 
  allocate(rhou(1:nx,1:ny),rhov(1:nx,1:ny))  
  allocate(rhouu(1:nx,1:ny),rhovv(1:nx,1:ny)) 
  allocate(rhouup(1:nx,1:ny),rhovvp(1:nx,1:ny)) 
  allocate(rhop(1:nx,1:ny),rhoup(1:nx,1:ny),rhovp(1:nx,1:ny)) 
  allocate(u(1:nx,1:ny),v(1:nx,1:ny)) 
  allocate(rho(1:nx,1:ny))
  allocate(uu(1:nx,1:ny), vv(1:nx,1:ny))
  allocate(u0(1:nx,1:ny), v0(1:nx,1:ny))
  allocate(dir1(N,N),dir2(N,N),dir3(N,N),dir4(N,N),Nseed(N,N),skel(N,N),skel2(N,N),skel3(N,N))
  allocate(distslice(2*(Ns+Ni+Nf),2))
  allocate(slice_courbe(nbtheta,Ns+Ni+Nf-2,2),slice_courbe_equal(nbtheta,Ns+Ni+Nf-2,2))
  allocate(slice(nbtheta,Ns+Ni+Nf-2,2),slice2(nbtheta,Ns+Ni+Nf-2,2))

  un = 1._pr
  zero = 0._pr

  do i=1,nx
     xx(i) = 1+(float(i)-1)*dx
  enddo
  do j=1,ny
     yy(j) = 1+(float(j)-1)*dy
  enddo

  picNum = 140
  pos = nx/2
  yLeft = ny
  xLeft = pos
  yRight = 1
  xRight = pos
  open(unit=78,file="/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_inidroit.txt",status='unknown')
  do k=1,nx
     do j=1,ny
        read(78,*) rhoSlices2(k,j)
     enddo
  enddo
  close(78)
  !rhoSlices2 = (rhoSlices2-minval(rhoSlices2))/(maxval(rhoSlices2)-minval(rhoSlices2))
  rhoSlices2 = rhoSlices2 - 0.5_pr*(maxval(rhoSlices2) + minval(rhoSlices2))
!!  open(unit=79,file='results/sol00.vtk',status='unknown')
!!  write(79,'(1A26)') '# vtk DataFile Version 2.0'
!!  write(79,'(a)') 'rho'
!!  write(79,'(a)') 'ASCII'
!!  write(79,'(a)') 'DATASET STRUCTURED_POINTS'
!!  write(79,'(a,I4,I4,I4)') 'DIMENSIONS', N,ny,1
!!  write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
!!  write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,1. 
!!  write(79,'(a,I6)') 'POINT_DATA          ' , N*ny 
!!  write(79,'(a)') 'SCALARS values double'
!!  write(79,'(a)') 'LOOKUP_TABLE default'
!!
!!  do k=1,ny
!!     do j=1,N
!!        write(79,*) rhoSlices2(j,k)
!!     enddo
!!  enddo
!!  close(79)    
  call updateDistance(rhoSlices2)

  dir1 = 0
  dir2 = 0
  dir3 = 0
  dir4 = 0
  Nseed = 0
  skel = 0
  do i=2,N-1
     do j=2,ny-1
        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j))) dir1(i,j) = 1
        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j-1))) dir2(i,j) = 1
        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j-1))) dir3(i,j) = 1
        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j-1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j+1))) dir4(i,j) = 1
        Nseed(i,j) = dir1(i,j) + dir2(i,j) + dir3(i,j) + dir4(i,j)
        if ((Nseed(i,j)>=2).and.(rhoSlices2(i,j)>0)) skel(i,j) = 1
        if ((i==146).and.(j==109)) skel(i,j) = 0 !write(*,*) "testpoint ",Nseed(i,j)
        if ((i==147).and.(j==109)) skel(i,j) = 0 !write(*,*) "testpoint ",Nseed(i,j)
     enddo
  enddo
  l = 1
  pix1 = 0
  pix2 = 0
  do while ((pix1==1).or.(pix2==1).or.(l==1))
     pix1 = 0
     pix2 = 0
     l = l + 1
     skel2 = skel
     do i=2,N-1
        do j=2,ny-1
           maxv = 0
           if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
           if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
           if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
           if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
           if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
           if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
           if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
           if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
           Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
           if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i+1,j)==0).or.(skel(i-1,j)==0).or.(skel(i,j-1)==0)).and.((skel(i,j-1)==0).or.(skel(i,j+1)==0).or.(skel(i,j+1)==0))) then
              skel2(i,j) = 0
              pix1 = 1
           endif
        enddo
     enddo
     skel = skel2
     skel2 = skel
     do i=2,N-1
        do j=2,ny-1
           maxv = 0
           if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
           if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
           if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
           if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
           if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
           if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
           if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
           if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
           Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
           if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i,j+1)==0).or.(skel(i-1,j)==0).or.(skel(i,j-1)==0)).and.((skel(i+1,j)==0).or.(skel(i,j+1)==0).or.(skel(i-1,j)==0))) then
              skel2(i,j) = 0
              pix2 = 1
           endif
        enddo
     enddo
     skel = skel2
  enddo
  skel2 = skel
  do i=2,N-1
     do j=2,ny-1
        Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i-1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j+1)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
     enddo
  enddo
  skel = skel2
  skel2 = skel
  do i=2,N-1
     do j=2,ny-1
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j)==1).and.(skel2(i-1,j+1)==1)) skel2(i-1,j) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j+1)==1).and.(skel2(i,j+1)==1)) skel2(i,j+1) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j+1)==1).and.(skel2(i+1,j+1)==1)) skel2(i,j+1) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j+1)==1).and.(skel2(i+1,j)==1)) skel2(i+1,j) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j)==1).and.(skel2(i+1,j-1)==1)) skel2(i+1,j) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j-1)==1).and.(skel2(i,j-1)==1)) skel2(i,j-1) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j-1)==1).and.(skel2(i-1,j-1)==1)) skel2(i,j-1) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j-1)==1).and.(skel2(i-1,j)==1)) skel2(i-1,j) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)==0)) skel2(i,j) = 0
     enddo
  enddo
  skel = skel2


  tmpbool = 0
  nl = sum(skel)
  sizeSkel = sum(skel)
  allocate(midlinebis(sizeSkel,2))!,xLeft_tab(sizeSkel), yLeft_tab(sizeSkel), xRight_tab(sizeSkel), yRight_tab(sizeSkel))
  l=1
  midlinebis = 0
  do j=1,N
     do k=1,ny
        if ((skel(j,k)==1).and.(l==1)) then
           midlinebis(l,1) = j
           midlinebis(l,2) = k
           if (j==nx/2) l0 = l
           l = l+1
        endif
     enddo
  enddo
  xskelL = midlinebis(1,1)
  yskelL = midlinebis(1,2)
  lp = 1
  boolskel=0
  do i=2,N-1
     do j=2,ny-1
        if (skel(i,j)==1) then
           Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
           if ((Nseed(i,j)>1).and.((midlinebis(lp,1)==i).or.(midlinebis(lp,1)==i-1).or.(midlinebis(lp,1)==i+1)).and.((midlinebis(lp,2)==j).or.(midlinebis(lp,2)==j-1).or.(midlinebis(lp,2)==j+1)).and.(appartient(midlinebis,i,j,lp+1).neqv..true.)) then 
              lp=lp+1
              midlinebis(lp,1) = i
              midlinebis(lp,2) = j
           endif
        endif
     enddo
  enddo
  nl = lp
  allocate(midline(nl,2))
  midline(1:nl,1) = midlinebis(1:nl,1)
  midline(1:nl,2) = midlinebis(1:nl,2)

  allocate(points_control(nl,2))!,leftcontrolm(nl,2),rightcontrolm(nl,2))
  do l=1,nl
     points_control(l,1) = xx(midline(l,1))
     points_control(l,2) = yy(midline(l,2))
  enddo
  points_courbe(1,1) = points_control(1,1)
  points_courbe(1,2) = points_control(1,2)

  tb = dtb
  l = 1
  long = 0._pr
  do while ((tb<1._pr).and.(l+1<Ns+1))
     l = l+1
     call pointsBezierN(points_control,tb,px,py)
     points_courbe(l,1) = px
     points_courbe(l,2) = py
     long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))**2 + (points_courbe(l,2)-points_courbe(l-1,2))**2)
     tb = tb+dtb
  enddo
  write(*,*) "longueur =  ",(points_control(size(points_control,1),1)-points_control(1,1))," ",long," ",(points_courbe(Ns,1)-points_courbe(1,1))
  long2 = long
  ds = long/(Ns-1)
  points_courbe_equal(1,1) = points_control(1,1)
  points_courbe_equal(1,2) = points_control(1,2)
  l = 1
  long = 0._pr
  tb = 0._pr
  do while ((tb<1._pr).and.(l+1<Ns+1))
     l = l+1
     nt = 1
     s = 0._pr
     do while ((l-1)*ds-s>0._pr) 
        nt = nt+1
        s = s + sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)-points_courbe(nt-1,2))**2)
     enddo
     s0 = s
     tbm = (nt-2)*dtb
     tbp = (nt-1)*dtb
     tb = tbm !(tbm + tbp)*0.5_pr
     sinit = s - sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)-points_courbe(nt-1,2))**2)
     s = sinit
     bool = 0
     do while ((abs((l-1)*ds-s)>eepsilon).and.(bool==0))
        tb = (tbm + tbp)*0.5_pr
        call pointsBezierN(points_control,tb,px,py)
        s = sinit + sqrt((px-points_courbe(nt-1,1))**2 + (py-points_courbe(nt-1,2))**2)
        if ((l-1)*ds-s>0._pr) then
           tbm = tb
        else
           tbp = tb
        endif
        if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
           bool = 1
        endif
     enddo
     call pointsBezierN(points_control,tb,px,py)
     points_courbe_equal(l,1) = px
     points_courbe_equal(l,2) = py
     long = long + sqrt((points_courbe_equal(l,1)-points_courbe_equal(l-1,1))**2 + (points_courbe_equal(l,2)-points_courbe_equal(l-1,2))**2)
  enddo
  long3 = 0._pr
  do l=1,nl-1
     long3 = long3 +sqrt((xx(midline(l+1,1))-xx(midline(l,1)))**2 + (yy(midline(l+1,2))-yy(midline(l,2)))**2)
  enddo
  itail = 0
  do i=2,nx-1
     if ((rhoSlices2(i,midline(1,2))>0._pr).and.(rhoSlices2(i-1,midline(1,2))<0._pr)) then 
        if (itail==0) then
           itail = i
           rtail = xx(i-1) - rhoSlices2(i,midline(1,2))*dx/(rhoSlices2(i-1,midline(1,2)) - rhoSlices2(i,midline(1,2)))
        endif
     endif
     if ((rhoSlices2(i,midline(1,2))>0._pr).and.(rhoSlices2(i+1,midline(1,2))<0._pr)) then
        ihead = i
        rhead = xx(i) - rhoSlices2(i,midline(1,2))*dx/(rhoSlices2(i+1,midline(1,2)) - rhoSlices2(i,midline(1,2)))
     endif
  enddo
  do j=2,ny-1
     if ((rhoSlices2(midline(1,1),ny-j+1)>0._pr).and.(rhoSlices2(midline(1,1),ny-(j+1)+1)<0._pr)) yRight =  xx(ny-j) - rhoSlices2(midline(1,1),ny-j+1)*dy/(rhoSlices2(midline(1,1),ny-(j+1)+1) - rhoSlices2(midline(1,1),ny-j+1)) !j+1
     if ((rhoSlices2(midline(1,1),j)>0._pr).and.(rhoSlices2(midline(1,1),j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(midline(1,1),j)*dy/(rhoSlices2(midline(1,1),j+1) - rhoSlices2(midline(1,1),j)) !j
  enddo
  disttaillY = dist(xx(midline(1,1)),xx(midline(1,2)),xx(midline(1,1)),yLeft)
  disttailrY = dist(xx(midline(1,1)),xx(midline(1,2)),xx(midline(1,1)),yRight)
  disttail = abs(rtail - points_courbe_equal(1,1))
  do j=2,ny-1
     if ((rhoSlices2(midline(nl,1),ny-j+1)>0._pr).and.(rhoSlices2(midline(nl,1),ny-(j+1)+1)<0._pr)) yRight =  xx(ny-j) - rhoSlices2(midline(nl,1),ny-j+1)*dy/(rhoSlices2(midline(nl,1),ny-(j+1)+1) - rhoSlices2(midline(nl,1),ny-j+1)) !j+1
     if ((rhoSlices2(midline(nl,1),j)>0._pr).and.(rhoSlices2(midline(nl,1),j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(midline(nl,1),j)*dy/(rhoSlices2(midline(nl,1),j+1) - rhoSlices2(midline(nl,1),j)) !j
  enddo
  distheadlY = dist(xx(midline(nl,1)),xx(midline(nl,2)),xx(midline(nl,1)),yLeft)
  distheadrY = dist(xx(midline(nl,1)),xx(midline(nl,2)),xx(midline(nl,1)),yRight)
  disthead = abs(rhead-points_control(size(points_control,1),1))
  write(*,*) "DIST : TAIL ",disttail," HEAD ",disthead," NINT ",nint(disttail)," ",floor(disthead)," ",rtail," ",rhead!," nbtheta ",nbtheta
  allocate(slice_control(nbtheta,ihead-itail+1+2,2),slicecontrolm(nbtheta,floor(points_courbe_equal(Ns,1))-ceiling(points_courbe_equal(1,1))+1,2),slicecontrolf(nbtheta,floor(disthead),2),slicecontroli(nbtheta,nint(disttail),2))
  lp = 0
  lph = 0
  lpt = 0
  do l=itail,ihead
     yRight = points_courbe_equal(1,2)
     yLeft = points_courbe_equal(1,2)

     do j=2,ny-1
        if ((rhoSlices2(l,ny-j+1)>0._pr).and.(rhoSlices2(l,ny-(j+1)+1)<0._pr)) yRight =  xx(ny-j) - rhoSlices2(l,ny-j+1)*dy/(rhoSlices2(l,ny-(j+1)+1) - rhoSlices2(l,ny-j+1)) !j+1
        if ((rhoSlices2(l,j)>0._pr).and.(rhoSlices2(l,j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(l,j)*dy/(rhoSlices2(l,j+1) - rhoSlices2(l,j)) !j
     enddo
     


     do theta=1,nbtheta
        slice_control(theta,l-itail+1+1,1) = xx(l)
     enddo
     slice_control(1,l-itail+1+1,2) = yLeft
     slice_control(2,l-itail+1+1,2) = yRight
     write(*,*) "TESTTEST ",itail," ",l," ",points_courbe_equal(1,1)
     if (l<ceiling(points_courbe_equal(1,1))) then
     !if (l<nint(points_courbe_equal(1,1))) then
        if (lpt==0) lpt = l
        do theta=1,nbtheta
           slicecontroli(theta,l-lpt+1,1) = xx(l)
        enddo
        slicecontroli(1,l-lpt+1,2) = yLeft
        slicecontroli(2,l-lpt+1,2) = yRight
     endif
     !if ((l>=ceiling(points_courbe_equal(1,1))).and.(l<=floor(points_courbe_equal(Ns,1)))) then
     if ((l>=nint(points_courbe_equal(1,1))).and.(l<=nint(points_courbe_equal(Ns,1)))) then
        if (lp==0) lp = l
        do theta=1,nbtheta
           slicecontrolm(theta,l-lp+1,1) = xx(l)
        enddo
        slicecontrolm(1,l-lp+1,2) = yLeft
        slicecontrolm(2,l-lp+1,2) = yRight
     endif
     !if (l>floor(points_courbe_equal(Ns,1))) then
     if (l>nint(points_courbe_equal(Ns,1))) then
        if (lph==0) lph = l
        do theta=1,nbtheta
           slicecontrolf(theta,l-lph+1,1) = xx(l)
        enddo
        slicecontrolf(1,l-lph+1,2) = yLeft
        slicecontrolf(2,l-lph+1,2) = yRight
     endif
  enddo

  dtb = 1._pr/(Ni-1)
  tb = dtb
  do theta=1,nbtheta
     slicecontroli(theta,1,1) = rtail
     slicecontroli(theta,1,2) = points_courbe_equal(1,2)
     slice_courbe(theta,1,1) = slicecontroli(theta,1,1)
     slice_courbe(theta,1,2) = slicecontroli(theta,1,2)
  enddo
  l = 1
  longslicei = 0._pr
  do while ((tb<1._pr).and.(l+1<Ni))
     l = l+1
     call pointsBezierN(slicecontroli(1,:,:),tb,px,py)
     slice_courbe(1,l,1) = px
     slice_courbe(1,l,2) = py
     call pointsBezierN(slicecontroli(2,:,:),tb,px,py)
     slice_courbe(2,l,1) = px
     slice_courbe(2,l,2) = py
     do theta=1,nbtheta
        longslicei(theta) = longslicei(theta) + sqrt((slice_courbe(theta,l,1)-slice_courbe(theta,l-1,1))**2 + (slice_courbe(theta,l,2)-slice_courbe(theta,l-1,2))**2)
     enddo
     tb = tb+dtb
  enddo
  dtb = 1._pr/(Ns-1)
  tb = dtb
  slicecontrolm(1,1,1) = points_courbe_equal(1,1)
  slicecontrolm(1,1,2) = points_courbe_equal(1,2) + disttaillY
  slicecontrolm(1,size(slicecontrolm,2),1) = points_courbe_equal(size(points_courbe_equal,1),1)
  slicecontrolm(1,size(slicecontrolm,2),2) = points_courbe_equal(size(points_courbe_equal,1),2) + distheadlY
  slicecontrolm(2,1,1) = points_courbe_equal(1,1)
  slicecontrolm(2,1,2) = points_courbe_equal(1,2) - disttailrY
  slicecontrolm(2,size(slicecontrolm,2),1) = points_courbe_equal(size(points_courbe_equal,1),1)
  slicecontrolm(2,size(slicecontrolm,2),2) = points_courbe_equal(size(points_courbe_equal,1),2) - distheadrY
  l = l+1
  do theta=1,nbtheta
     slice_courbe(theta,l,1) = slicecontrolm(theta,1,1)
     slice_courbe(theta,l,2) = slicecontrolm(theta,1,2)
  enddo
  do theta=1,nbtheta
     longslicei(theta) = longslicei(theta) + sqrt((slice_courbe(theta,l,1)-slice_courbe(theta,l-1,1))**2 + (slice_courbe(theta,l,2)-slice_courbe(theta,l-1,2))**2) 
  enddo
  longslice = 0._pr
  do while ((tb<1._pr).and.(l+1<Ni-1+Ns))
     l = l+1
     call pointsBezierN(slicecontrolm(1,:,:),tb,px,py)
     slice_courbe(1,l,1) = px
     slice_courbe(1,l,2) = py
     call pointsBezierN(slicecontrolm(2,:,:),tb,px,py)
     slice_courbe(2,l,1) = px
     slice_courbe(2,l,2) = py
     do theta=1,nbtheta
        longslice(theta) = longslice(theta) + sqrt((slice_courbe(theta,l,1)-slice_courbe(theta,l-1,1))**2 + (slice_courbe(theta,l,2)-slice_courbe(theta,l-1,2))**2)
     enddo

     tb = tb+dtb
  enddo
  l = l+1
  do theta=1,nbtheta
     slice_courbe(theta,l,1) = slicecontrolm(theta,size(slicecontrolm,2),1)
     slice_courbe(theta,l,2) = slicecontrolm(theta,size(slicecontrolm,2),2)
  enddo

  do theta=1,nbtheta
     longslice(theta) = longslice(theta) + sqrt((slice_courbe(theta,l,1)-slice_courbe(theta,l-1,1))**2 + (slice_courbe(theta,l,2)-slice_courbe(theta,l-1,2))**2)
  enddo
  dtb = 1._pr/(Nf-1)
  tb = dtb
  longslicef = 0._pr
  slicecontrolf(1,1,1) = points_courbe_equal(size(points_courbe_equal,1),1)
  slicecontrolf(1,1,2) = points_courbe_equal(size(points_courbe_equal,1),2) + distheadlY
  slicecontrolf(1,size(slicecontrolf,2),1) = rhead
  slicecontrolf(1,size(slicecontrolf,2),2) = points_courbe_equal(1,2) 
  slicecontrolf(2,1,1) = points_courbe_equal(size(points_courbe_equal,1),1)
  slicecontrolf(2,1,2) = points_courbe_equal(size(points_courbe_equal,1),2) - distheadrY
  slicecontrolf(2,size(slicecontrolf,2),1) = rhead
  slicecontrolf(2,size(slicecontrolf,2),2) = points_courbe_equal(1,2) 
  do while ((tb<1._pr).and.(l+1<Ni-1+Ns+Nf-1)) !size(left_courbe,1)))
     l = l+1
     call pointsBezierN(slicecontrolf(1,:,:),tb,px,py)
     slice_courbe(1,l,1) = px
     slice_courbe(1,l,2) = py
     call pointsBezierN(slicecontrolf(2,:,:),tb,px,py)
     slice_courbe(2,l,1) = px
     slice_courbe(2,l,2) = py
     do theta=1,nbtheta
        longslicef(theta) = longslicef(theta) + sqrt((slice_courbe(theta,l,1)-slice_courbe(theta,l-1,1))**2 + (slice_courbe(theta,l,2)-slice_courbe(theta,l-1,2))**2)
     enddo
     tb = tb+dtb
  enddo
  l = l+1
  do theta=1,nbtheta
     slice_courbe(theta,l,1) = slicecontrolf(theta,size(slicecontrolf,2),1)
     slice_courbe(theta,l,2) = slicecontrolf(theta,size(slicecontrolf,2),2)
     longslicef(theta) = longslicef(theta) + sqrt((slice_courbe(theta,l,1)-slice_courbe(theta,l-1,1))**2 + (slice_courbe(theta,l,2)-slice_courbe(theta,l-1,2))**2)
  enddo
  dtb = 1._pr/(Ns-1)
  do theta=1,nbtheta
     dsslice(theta) = longslice(theta)/(Ns-1)
  enddo

  dtb = 1._pr/(Ns-1)
  do theta=1,nbtheta
     l = 1
     tb = 0._pr
     slice(theta,1:Ni,1) = slice_courbe(theta,1:Ni,1)
     slice(theta,1:Ni,2) = slice_courbe(theta,1:Ni,2)
     slice(theta,Ni-1+Ns:size(slice_courbe_equal,2),1) = slice_courbe(theta,Ni-1+Ns:size(slice_courbe_equal,2),1)
     slice(theta,Ni-1+Ns:size(slice_courbe_equal,2),2) = slice_courbe(theta,Ni-1+Ns:size(slice_courbe_equal,2),2)
     slicemid(theta,1:Ns,1) = slice_courbe(theta,Ni:Ni-1+Ns,1)
     slicemid(theta,1:Ns,2) = slice_courbe(theta,Ni:Ni-1+Ns,2)
     do while ((tb<1._pr).and.(l+1<Ns)) !size(leftmid,1)))
        l = l+1
        nt = 1
        sslice(theta) = 0._pr
        do while ((l-1)*dsslice(theta)-sslice(theta)>0._pr) 
           nt = nt+1
           sslice(theta) = sslice(theta) + sqrt((slicemid(theta,nt,1)-slicemid(theta,nt-1,1))**2 + (slicemid(theta,nt,2)-slicemid(theta,nt-1,2))**2)
        enddo
        tbm = (nt-2)*dtb
        tbp = (nt-1)*dtb
        tb = tbm 
        s0 = sslice(theta)
        sinit = sslice(theta) - sqrt((slicemid(theta,nt,1)-slicemid(theta,nt-1,1))**2 + (slicemid(theta,nt,2)-slicemid(theta,nt-1,2))**2)
        sslice(theta) = sinit
        bool = 0
        do while ((abs((l-1)*dsslice(theta)-sslice(theta))>eepsilon).and.(bool==0))
           tb = (tbm + tbp)*0.5_pr
           call pointsBezierN(slicecontrolm(theta,:,:),tb,px,py)
           sslice(theta) = sinit + sqrt((px-slicemid(theta,nt-1,1))**2 + (py-slicemid(theta,nt-1,2))**2)
           if ((l-1)*dsslice(theta)-sslice(theta)>0._pr) then
              tbm = tb
           else
              tbp = tb
           endif
           if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
              bool = 1
           endif
        enddo
        call pointsBezierN(slicecontrolm(theta,:,:),tb,px,py)
        slice_courbe_equal(theta,l+Ni-1,1) = px
        slice_courbe_equal(theta,l+Ni-1,2) = py
        slice(theta,l+Ni-1,1) = slice_courbe_equal(theta,l+Ni-1,1)
        slice(theta,l+Ni-1,2) = slice_courbe_equal(theta,l+Ni-1,2)
     enddo
  enddo

  distslice(1+Ni,1) = dist(slice(1,1+Ni-1,1),slice(1,1+Ni-1,2),points_courbe_equal(1,1),points_courbe_equal(1,2))
  distslice(1+Ni,2) = dist(slice(1,1+Ni-1,1),slice(1,1+Ni-1,2),points_courbe_equal(2,1),points_courbe_equal(2,2))
  do theta=1,nbtheta
     valDist(1+Ni-1,theta) = dist(slice(theta,1+Ni-1,1),slice(theta,1+Ni-1,2),points_courbe_equal(1,1),points_courbe_equal(1,2))
  enddo
  do l=2,Ns-1
     distslice(l+Ni,1) = dist(slice(1,l+Ni-1,1),slice(1,l+Ni-1,2),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2))
     distslice(l+Ni,2) = dist(slice(1,l+Ni-1,1),slice(1,l+Ni-1,2),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2))
     do theta=1,nbtheta
        valDist(l+Ni-1,theta) = dist(slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2))
     enddo
  enddo
  distslice(Ns+Ni,1) = dist(slice(1,Ns+Ni-1,1),slice(1,Ns+Ni-1,2),points_courbe_equal(Ns-1,1),points_courbe_equal(Ns-1,2))
  distslice(Ns+Ni,2) = dist(slice(1,Ns+Ni-1,1),slice(1,Ns+Ni-1,2),points_courbe_equal(Ns,1),points_courbe_equal(Ns,2))
  do theta=1,nbtheta
     valDist(Ns+Ni-1,theta) = dist(slice(theta,Ns+Ni-1,1),slice(theta,Ns+Ni-1,2),points_courbe_equal(Ns,1),points_courbe_equal(Ns,2))
  enddo

  dsi = disttail/(Ni-1)
  distslice(1,1) = dist(slice(1,1,1),slice(1,1,2),points_courbe_equal(1,1)-(Ni-1)*dsi,points_courbe_equal(1,2))
  distslice(1,2) = dist(slice(1,1,1),slice(1,1,2),points_courbe_equal(1,1)-(Ni-2)*dsi,points_courbe_equal(1,2))
  do theta=1,nbtheta
     valDist(1,theta) = dist(slice(theta,1,1),slice(theta,1,2),points_courbe_equal(1,1)-(Ni-1)*dsi,points_courbe_equal(1,2))
  enddo
  do l=2,Ni-1
     distslice(l,1) = dist(slice(1,l,1),slice(1,l,2),points_courbe_equal(1,1)-(Ni-l+1)*dsi,points_courbe_equal(1,2))
     distslice(l,2) = dist(slice(1,l,1),slice(1,l,2),points_courbe_equal(1,1)-(Ni-l-1)*dsi,points_courbe_equal(1,2))
     do theta=1,nbtheta
        valDist(l,theta) = dist(slice(theta,l,1),slice(theta,l,2),points_courbe_equal(1,1)-(Ni-l)*dsi,points_courbe_equal(1,2))
     enddo
  enddo
  distslice(Ni,1) = dist(slice(1,Ni,1),slice(1,Ni,2),points_courbe_equal(1,1)-dsi,points_courbe_equal(1,2))
  distslice(Ni,2) = dist(slice(1,Ni,1),slice(1,Ni,2),points_courbe_equal(1,1),points_courbe_equal(1,2))
  do theta=1,nbtheta
     valDist(Ni,theta) = dist(slice(theta,Ni,1),slice(theta,Ni,2),points_courbe_equal(1,1),points_courbe_equal(1,2))
  enddo

  dsf = disthead/(Nf-1)
  distslice(1+Ni+Ns,1) = dist(slice(1,1+Ni-1+Ns-1,1),slice(1,1+Ni-1+Ns-1,2),points_courbe_equal(Ns,1),points_courbe_equal(1,2))
  distslice(1+Ni+Ns,2) = dist(slice(1,1+Ni-1+Ns-1,1),slice(1,1+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+dsf,points_courbe_equal(1,2))
  do theta=1,nbtheta
     valDist(1+Ni-1+Ns-1,theta) = dist(slice(theta,1+Ni-1+Ns-1,1),slice(theta,1+Ni-1+Ns-1,2),points_courbe_equal(Ns,1),points_courbe_equal(1,2))
  enddo
  do l=2,Nf-1
     distslice(l+Ni+Ns,1) = dist(slice(1,l+Ni-1+Ns-1,1),slice(1,l+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(l-2)*dsf,points_courbe_equal(1,2))
     distslice(l+Ni+Ns,2) = dist(slice(1,l+Ni-1+Ns-1,1),slice(1,l+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+l*dsf,points_courbe_equal(1,2))
     do theta=1,nbtheta
        valDist(l+Ni-1+Ns-1,theta) = dist(slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(l-1)*dsf,points_courbe_equal(1,2))
     enddo
  enddo
  distslice(Nf+Ni+Ns,1) = dist(slice(1,Nf+Ni-1+Ns-1,1),slice(1,Nf+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(Nf-2)*dsf,points_courbe_equal(1,2))
  distslice(Nf+Ni+Ns,2) = dist(slice(1,Nf+Ni-1+Ns-1,1),slice(1,Nf+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(Nf-1)*dsf,points_courbe_equal(1,2))
  do theta=1,nbtheta
     valDist(Nf+Ni-1+Ns-1,theta) = dist(slice(theta,Nf+Ni-1+Ns-1,1),slice(theta,Nf+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(Nf-1)*dsf,points_courbe_equal(1,2))
  enddo

  do l=1,size(valTheta,1)
     valTheta(l,1) = 0._pr
     valTheta(l,2) = PI  
  enddo


!!  open(unit=84,file='results/right00.txt',status='unknown')
!!  open(unit=83,file='results/left00.txt',status='unknown')
!!  open(unit=79,file='results/skelett00.vtk',status='unknown')
!!  open(unit=81,file='results/skelett00.txt',status='unknown')
!!  open(unit=82,file='results/skeletteq00.txt',status='unknown')
!!  write(79,'(1A26)') '# vtk DataFile Version 2.0'
!!  write(79,'(a)') 'rho'
!!  write(79,'(a)') 'ASCII'
!!  write(79,'(a)') 'DATASET STRUCTURED_POINTS'
!!  write(79,'(a,I4,I4,I4)') 'DIMENSIONS', N,ny,1
!!  write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
!!  write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,1. 
!!  write(79,'(a,I6)') 'POINT_DATA          ' , N*ny 
!!  write(79,'(a)') 'SCALARS values double'
!!  write(79,'(a)') 'LOOKUP_TABLE default'
!!
!!  do k=1,ny
!!     do j=1,N
!!        write(79,*) rhoSlices2(j,k)*(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k)
!!     enddo
!!  enddo
!!  do l=1,Ns
!!     write(81,*) points_courbe(l,1)," ",points_courbe(l,2)
!!     write(82,*) points_courbe_equal(l,1)," ",points_courbe_equal(l,2)
!!  enddo
!!  !do l=1,size(slice,2) !size(left_courbe_equal,1)
!!  !   write(83,*) slice(1,l,1)," ",slice(1,l,2)
!!  !enddo
!!  !do l=1,size(slicecontroli,2) !size(left_courbe_equal,1)
!!  !   write(83,*) slicecontroli(1,l,1)," ",slicecontroli(1,l,2)
!!  !enddo
!!  do l=1,size(slicecontrolm,2) !size(left_courbe_equal,1)
!!     write(83,*) slicecontrolm(1,l,1)," ",slicecontrolm(1,l,2)
!!  enddo
!!  !do l=1,size(slicecontrolf,2) !size(left_courbe_equal,1)
!!  !   write(83,*) slicecontrolf(1,l,1)," ",slicecontrolf(1,l,2)
!!  !enddo
!!  do l=1,size(slice,2) !size(right_courbe_equal,1)
!!     write(84,*) slice(2,l,1)," ",slice(2,l,2)
!!  enddo
!!  write(*,*) " long ==   ",nl," ",long
!!  close(79)    
!!  close(81)
!!  close(82)
!!  close(83)
!!  close(84)
  deallocate(midline,midlinebis)
  deallocate(points_control)
  deallocate(slice_control,slicecontrolm,slicecontrolf,slicecontroli)

!!  open(unit=80,file='skeleton.txt',status='unknown')
!!  write(80,*) kt,"    ",nl," ",long3," ",long2," ",long

  t = 0
  iter = 0
  idisplay = 10
  iUpdateDist = 5
  do kt = 1,615,idisplay
     write(*,*)"time iteration : ",kt
     open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/mask200/Image_"//str(kt+picNum-1)//".txt",status='unknown')
     do k=1,nx
        do j=1,ny
           read(78,*) rhoSlices2(k,j)
        enddo
     enddo
     close(78)
     rhoSlices2 = rhoSlices2 - 0.5_pr*(maxval(rhoSlices2) + minval(rhoSlices2))
     call updateDistance(rhoSlices2)
     dx = 1._pr
     dy = 1._pr

     dir1 = 0
     dir2 = 0
     dir3 = 0
     dir4 = 0
     Nseed = 0
     skel = 0
     do i=2,N-1
        do j=2,ny-1
           if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j))) dir1(i,j) = 1
           if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j-1))) dir2(i,j) = 1
           if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j-1))) dir3(i,j) = 1
           if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j-1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j+1))) dir4(i,j) = 1
           Nseed(i,j) = dir1(i,j) + dir2(i,j) + dir3(i,j) + dir4(i,j)
           if ((Nseed(i,j)>=2).and.(rhoSlices2(i,j)>0)) then 
              skel(i,j) = 1
           endif
        enddo
     enddo
     l = 1
     pix1 = 0
     pix2 = 0
     do while ((pix1==1).or.(pix2==1).or.(l==1))
        pix1 = 0
        pix2 = 0
        l = l + 1
        skel2 = skel
        do i=2,N-1
           do j=2,ny-1
              maxv = 0
              if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
              if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
              if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
              if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
              if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
              if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
              if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
              if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
              Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
              if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i+1,j)==0).or.(skel(i-1,j)==0).or.(skel(i,j-1)==0)).and.((skel(i,j-1)==0).or.(skel(i,j+1)==0).or.(skel(i,j+1)==0))) then
                 skel2(i,j) = 0
                 pix1 = 1
              endif
           enddo
        enddo
        skel = skel2
        skel2 = skel
        do i=2,N-1
           do j=2,ny-1
              maxv = 0
              if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
              if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
              if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
              if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
              if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
              if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
              if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
              if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
              Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
              if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i,j+1)==0).or.(skel(i-1,j)==0).or.(skel(i,j-1)==0)).and.((skel(i+1,j)==0).or.(skel(i,j+1)==0).or.(skel(i-1,j)==0))) then
                 skel2(i,j) = 0
                 pix2 = 1
              endif
           enddo
        enddo
        skel = skel2
     enddo
     skel2 = skel
     do i=2,N-1
        do j=2,ny-1
           Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i-1,j+1)==1)) skel2(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i,j+1)==1)) skel2(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j)==1)) skel2(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
           if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j+1)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
        enddo
     enddo
     skel = skel2
     skel2 = skel
     do i=2,N-1
        do j=2,ny-1
           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j)==1).and.(skel2(i-1,j+1)==1)) skel2(i-1,j) = 0
           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j+1)==1).and.(skel2(i,j+1)==1)) skel2(i,j+1) = 0
           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j+1)==1).and.(skel2(i+1,j+1)==1)) skel2(i,j+1) = 0
           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j+1)==1).and.(skel2(i+1,j)==1)) skel2(i+1,j) = 0
           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j)==1).and.(skel2(i+1,j-1)==1)) skel2(i+1,j) = 0
           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j-1)==1).and.(skel2(i,j-1)==1)) skel2(i,j-1) = 0
           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j-1)==1).and.(skel2(i-1,j-1)==1)) skel2(i,j-1) = 0
           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
           if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j-1)==1).and.(skel2(i-1,j)==1)) skel2(i-1,j) = 0
           Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) + skel2(i+1,j-1)
           if ((skel2(i,j)==1).and.(Nseed(i,j)==0)) skel2(i,j) = 0
        enddo
     enddo
     skel = skel2

     tmpbool = 0

     sizeSkel = sum(skel)

     allocate(midline(sizeSkel,2))
     l=1
     do i=2,N-1
        do j=2,ny-1
           if ((skel(i,j)==1).and.(l==1)) then
              Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
              if ((Nseed(i,j)==1).and.((abs(xskelL-i)<100).and.(abs(yskelL-j)<100))) then 
                 midline(l,1) = i
                 midline(l,2) = j
                 l = l+1
              endif
           endif
        enddo
     enddo
     xskelL = midline(1,1)
     yskelL = midline(1,2)
     do l=2,sum(skel)
        do i=2,N-1
           do j=2,ny-1
              if (skel(i,j)==1) then
                 Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
                 if ((Nseed(i,j)>1).and.((midline(l-1,1)==i).or.(midline(l-1,1)==i-1).or.(midline(l-1,1)==i+1)).and.((midline(l-1,2)==j).or.(midline(l-1,2)==j-1).or.(midline(l-1,2)==j+1)).and.(appartient(midline,i,j,l).neqv..true.)) then 
                    midline(l,1) = i
                    midline(l,2) = j
                 endif
                 if ((Nseed(i,j)==1).and.(l==sum(skel))) then
                    midline(l,1) = i
                    midline(l,2) = j
                 endif
              endif
           enddo
        enddo
     enddo
     allocate(points_control(sum(skel),2))
     do l=1,sum(skel)
        points_control(l,1) = xx(midline(l,1))
        points_control(l,2) = yy(midline(l,2))
     enddo
     points_courbe(1,1) = points_control(1,1)
     points_courbe(1,2) = points_control(1,2)
     tb = dtb
     l = 1
     long = 0._pr
     do while ((tb<1._pr).and.(l+1<Ns+1))
        l = l+1
        call pointsBezierN(points_control,tb,px,py)
        points_courbe(l,1) = px
        points_courbe(l,2) = py
        long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))**2 + (points_courbe(l,2)-points_courbe(l-1,2))**2)
        tb = tb+dtb
     enddo
     long2 = long
     ds = long/(Ns-1)
     points_courbe_equal(1,1) = points_control(1,1)
     points_courbe_equal(1,2) = points_control(1,2)
     l = 1
     long = 0._pr
     tb = 0._pr
     do while ((tb<1._pr).and.(l+1<Ns+1))
        l = l+1
        nt = 1
        s = 0._pr
        do while ((l-1)*ds-s>0._pr) 
           nt = nt+1
           s = s + sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)-points_courbe(nt-1,2))**2)
        enddo
        tbm = (nt-2)*dtb
        tbp = (nt-1)*dtb
        s0 = s
        sinit = s - sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))**2 + (points_courbe(nt,2)-points_courbe(nt-1,2))**2)
        s = sinit
        bool = 0
        do while ((abs((l-1)*ds-s)>eepsilon).and.(bool==0))
           tb = (tbm + tbp)*0.5_pr
           call pointsBezierN(points_control,tb,px,py)
           s = sinit + sqrt((px-points_courbe(nt-1,1))**2 + (py-points_courbe(nt-1,2))**2)
           if ((l-1)*ds-s>0._pr) then
              tbm = tb
           else
              tbp = tb
           endif
           if (abs(tb-(tbm+tbp)*0.5_pr)<eepsilon) then
              bool = 1
           endif
        enddo
        call pointsBezierN(points_control,tb,px,py)
        points_courbe_equal(l,1) = px
        points_courbe_equal(l,2) = py
        long = long + sqrt((points_courbe_equal(l,1)-points_courbe_equal(l-1,1))**2 + (points_courbe_equal(l,2)-points_courbe_equal(l-1,2))**2)
     enddo
     dti = dsi/ds !distslice(1+Ni,1)/(Ni-1)/dsi
     do l=2,Ni
        tb = - (l-1)*dti
        call courbeBezierN(points_courbe_equal,tb,Px,Py)
        tail_courbe(l,1) = Px
        tail_courbe(l,2) = Py
     enddo
     tail_courbe(1,1) = points_courbe_equal(1,1) !points_control(1,1)
     tail_courbe(1,2) = points_courbe_equal(1,2) !points_control(1,2)
     dtf = dsf/ds !dist(points_control(size(points_control,1),1),points_control(size(points_control,1),2),points_courbe(size(points_courbe,1)-1,1),points_courbe(size(points_courbe,1)-1,2)) !ds!/(100)!*ds)!*100 !distslice(Ns+Ni,2)/(Nf-1)/dsf !(10*ds)
     do l=2,Nf
        tb = 1._pr + (l-1)*dtf
        call courbeBezierN(points_courbe_equal,tb,Px,Py)
        head_courbe(l,1) = Px
        head_courbe(l,2) = Py
     enddo
     head_courbe(1,1) = points_courbe_equal(Ns,1) !points_control(size(points_control,1),1) !points_courbe_equal(Ns,1) !points_control(Ns,1)
     head_courbe(1,2) = points_courbe_equal(Ns,2) !points_control(size(points_control,1),2) !points_courbe_equal(Ns,2) !points_control(Ns,2)
     long3 = 0._pr
     do l=1,sum(skel)-1
        long3 = long3 +sqrt((xx(midline(l+1,1))-xx(midline(l,1)))**2 + (yy(midline(l+1,2))-yy(midline(l,2)))**2)
     enddo
     l=1





     do theta=1,nbtheta
        thetatab(theta) = 1
        slice(theta,1,1) = tail_courbe(Ni,1)
        slice(theta,1,2) = tail_courbe(Ni,2)
        slice2(theta,1,1) = slice(theta,1,1)
        slice2(theta,1,2) = slice(theta,1,2)
     enddo
     do l=2,Ni-1
        call intersection(tail_courbe(Ni-l+1+1,1),tail_courbe(Ni-l+1-1,1),tail_courbe(Ni-l+1+1,2),tail_courbe(Ni-l+1-1,2),tail_courbe(Ni-l+1,1),tail_courbe(Ni-l+1,2),distslice(l,1),distslice(l,2),distslice(Ns+Ni+Nf+l,1),distslice(Ns+Ni+Nf+l,2),xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
        slice(1,l,1) = xTheta(1)
        slice(1,l,2) = yTheta(1)
        if (errorl==1) then
           slice(1,l,1) = slice(1,l-1,1)
           slice(1,l,2) = slice(1,l-1,2)
        endif
        if ((abs(slice(1,l,1)-slice(1,l-1,1))<1.e-2).and.(abs(slice(1,l,2)-slice(1,l-1,2))<1.e-2)) then
           slice(2,l,1) = slice(2,l-1,1)
           slice(2,l,2) = slice(2,l-1,2)
        else
           slice(2,l,1) = -slice(1,l,1) + 2*tail_courbe(Ni-l+1,1)
           slice(2,l,2) = -slice(1,l,2) + 2*tail_courbe(Ni-l+1,2)
        endif
        !        if (det(tail_courbe(Ni-l+1+1,1),tail_courbe(Ni-l+1+1,2),slice2(lpli,1,1),slice2(lpli,1,2),tail_courbe(Ni-l+1,1),tail_courbe(Ni-l+1,2))*det(tail_courbe(Ni-l+1+1,1),tail_courbe(Ni-l+1+1,2),slice2(lpli,1,1),slice2(lpli,1,2),xLeft,yLeft)>= 0._pr) then
        do theta=1,nbtheta
           thetatab(theta) = thetatab(theta) + 1
        enddo
        slice2(1,thetatab(1),1) = xTheta(1)
        slice2(1,thetatab(1),2) = yTheta(1)
        slice2(2,thetatab(2),1) = slice(2,l,1)
        slice2(2,thetatab(2),2) = slice(2,l,2)
        !        endif
        if (errorl==1) then
           slice2(1,thetatab(1),1) = slice2(1,thetatab(1)-1,1)
           slice2(1,thetatab(1),2) = slice2(1,thetatab(1)-1,2)
        endif
        if (errorr==1) then
           slice2(2,thetatab(2),1) = slice2(2,thetatab(2)-1,1)
           slice2(2,thetatab(2),2) = slice2(2,thetatab(2)-1,2)
        endif

        !if (errorl==1) write(*,*) "WARNING : ",errorl," ",l
     enddo
     call intersection(tail_courbe(2,1),points_courbe_equal(2,1),tail_courbe(2,2),points_courbe_equal(2,2),points_courbe_equal(1,1),points_courbe_equal(1,2),distslice(Ni,1),distslice(Ni+1,2),distslice(Ns+Ni+Nf+Ni,1),distslice(Ns+Ni+Nf+Ni+1,2),xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
     l = Ni
     sinPhi = (xTheta(1)-points_courbe_equal(1,1))/(sqrt((xTheta(1)-points_courbe_equal(1,1))**2+(yTheta(1)-points_courbe_equal(1,2))**2))
     cosPhi = (yTheta(1)-points_courbe_equal(1,2))/(sqrt((xTheta(1)-points_courbe_equal(1,1))**2+(yTheta(1)-points_courbe_equal(1,2))**2)) 
     slice(1,l,1) = points_courbe_equal(1,1) + valDist(l,1)*cos(valTheta(l,1))*sinPhi
     slice(1,l,2) = points_courbe_equal(1,2) + valDist(l,1)*cos(valTheta(l,1))*cosPhi
     slice(2,l,1) = points_courbe_equal(1,1) + valDist(l,2)*cos(valTheta(l,2))*sinPhi 
     slice(2,l,2) = points_courbe_equal(1,2) + valDist(l,2)*cos(valTheta(l,2))*cosPhi 

     if ((det(tail_courbe(2,1),tail_courbe(2,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),points_courbe_equal(1,1),points_courbe_equal(1,2))*det(tail_courbe(2,1),tail_courbe(2,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),slice(1,l,1),slice(1,l,2))>= 0._pr).or.(thetatab(1)<Ni)) then
        thetatab(1) = thetatab(1) + 1
        slice2(1,thetatab(1),1) = slice(1,l,1)
        slice2(1,thetatab(1),2) = slice(1,l,2)
     endif
     do theta=2,nbtheta
        if ((det(tail_courbe(2,1),tail_courbe(2,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),points_courbe_equal(1,1),points_courbe_equal(1,2))*det(tail_courbe(2,1),tail_courbe(2,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),slice(theta,l,1),slice(theta,l,2))>= 0._pr).or.(thetatab(theta)<Ni)) then
           thetatab(theta) = thetatab(theta) + 1
           slice2(2,thetatab(2),1) = slice(2,l,1)
           slice2(2,thetatab(2),2) = slice(2,l,2)
        endif
     enddo
     if (errorl==1) write(*,*) "WARNING : ",errorl," ",l
     do l=2,Ns-1
        call intersection(points_courbe_equal(l-1,1),points_courbe_equal(l+1,1),points_courbe_equal(l-1,2),points_courbe_equal(l+1,2),points_courbe_equal(l,1),points_courbe_equal(l,2),distslice(l+Ni,1),distslice(l+Ni,2),distslice(Ns+Ni+Nf+l+Ni,1),distslice(Ns+Ni+Nf+l+Ni,2),xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
        sinPhi = (xTheta(1)-points_courbe_equal(l,1))/(sqrt((xTheta(1)-points_courbe_equal(l,1))**2+(yTheta(1)-points_courbe_equal(l,2))**2))
        cosPhi = (yTheta(1)-points_courbe_equal(l,2))/(sqrt((xTheta(1)-points_courbe_equal(l,1))**2+(yTheta(1)-points_courbe_equal(l,2))**2)) 
        slice(1,l+Ni-1,1) = points_courbe_equal(l,1) + valDist(l+Ni-1,1)*cos(valTheta(l+Ni-1,1))*sinPhi
        slice(1,l+Ni-1,2) = points_courbe_equal(l,2) + valDist(l+Ni-1,1)*cos(valTheta(l+Ni-1,1))*cosPhi
        slice(2,l+Ni-1,1) = points_courbe_equal(l,1) + valDist(l+Ni-1,2)*cos(valTheta(l+Ni-1,2))*sinPhi 
        slice(2,l+Ni-1,2) = points_courbe_equal(l,2) + valDist(l+Ni-1,2)*cos(valTheta(l+Ni-1,2))*cosPhi 
        if ((det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),slice(1,l+Ni-1,1),slice(1,l+Ni-1,2))>= 0._pr).or.(thetatab(1)<Ni)) then
           thetatab(1) = thetatab(1) + 1
           slice2(1,thetatab(1),1) = slice(1,l+Ni-1,1)
           slice2(1,thetatab(1),2) = slice(1,l+Ni-1,2)
        endif
        do theta=2,nbtheta
           if ((det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2))>= 0._pr).or.(thetatab(theta)<Ni)) then
              thetatab(theta) = thetatab(theta) + 1
              slice2(theta,thetatab(theta),1) = slice(theta,l+Ni-1,1)
              slice2(theta,thetatab(theta),2) = slice(theta,l+Ni-1,2)
           endif
        enddo
        if (errorl==1) write(*,*) "WARNING : ",errorl," ",l
     enddo
     l = Ns
     call intersection(points_courbe_equal(l-1,1),head_courbe(1+1,1),points_courbe_equal(Ns-1,2),head_courbe(1+1,2),head_courbe(1,1),head_courbe(1,2),distslice(Ni+Ns,1),distslice(1+Ni+Ns,2),distslice(Ns+Ni+Nf+Ni+Ns,1),distslice(Ns+Ni+Nf+1+Ni+Ns,2),xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
     sinPhi = (xTheta(1)-head_courbe(1,1))/(sqrt((xTheta(1)-head_courbe(1,1))**2+(yTheta(1)-head_courbe(1,2))**2))
     cosPhi = (yTheta(1)-head_courbe(1,2))/(sqrt((xTheta(1)-head_courbe(1,1))**2+(yTheta(1)-head_courbe(1,2))**2)) 
     slice(1,l+Ni-1,1) = head_courbe(1,1) + valDist(l+Ni-1,1)*cos(valTheta(l+Ni-1,1))*sinPhi
     slice(1,l+Ni-1,2) = head_courbe(1,2) + valDist(l+Ni-1,1)*cos(valTheta(l+Ni-1,1))*cosPhi
     slice(2,l+Ni-1,1) = head_courbe(1,1) + valDist(l+Ni-1,2)*cos(valTheta(l+Ni-1,2))*sinPhi
     slice(2,l+Ni-1,2) = head_courbe(1,2) + valDist(l+Ni-1,2)*cos(valTheta(l+Ni-1,2))*cosPhi

     if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),slice(1,l+Ni-1,1),slice(1,l+Ni-1,2))>= 0._pr) then
        thetatab(1) = thetatab(1) + 1
        slice2(1,thetatab(1),1) = slice(1,l+Ni-1,1)
        slice2(1,thetatab(1),2) = slice(1,l+Ni-1,2)
     endif
     do theta=2,nbtheta
        if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),points_courbe_equal(l,1),points_courbe_equal(l,2))*det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),slice(theta,l+Ni-1,1),slice(theta,l+Ni-1,2))>= 0._pr) then
           thetatab(theta) = thetatab(theta) + 1
           slice2(theta,thetatab(theta),1) = slice(theta,l+Ni-1,1)
           slice2(theta,thetatab(theta),2) = slice(theta,l+Ni-1,2)
        endif
     enddo
     if (errorl==1) write(*,*) "WARNING : ",errorl," ",l
     do l=2,Nf-1
        call intersection(head_courbe(l-1,1),head_courbe(l+1,1),head_courbe(l-1,2),head_courbe(l+1,2),head_courbe(l,1),head_courbe(l,2),distslice(l+Ni+Ns,1),distslice(l+Ni+Ns,2),distslice(Ns+Ni+Nf+l+Ni+Ns,1),distslice(Ns+Ni+Nf+l+Ni+Ns,2),xTheta(1),yTheta(1),xTheta(2),yTheta(2),errorl,errorr,deltal,deltar) 
        sinPhi = (xTheta(1)-head_courbe(l,1))/(sqrt((xTheta(1)-head_courbe(l,1))**2+(yTheta(1)-head_courbe(l,2))**2))
        cosPhi = (yTheta(1)-head_courbe(l,2))/(sqrt((xTheta(1)-head_courbe(l,1))**2+(yTheta(1)-head_courbe(l,2))**2)) 
        slice(1,l+Ni-1+Ns-1,1) = head_courbe(l,1) + valDist(l+Ni-1+Ns-1,1)*cos(valTheta(l+Ni-1+Ns-1,1))*sinPhi
        slice(1,l+Ni-1+Ns-1,2) = head_courbe(l,2) + valDist(l+Ni-1+Ns-1,1)*cos(valTheta(l+Ni-1+Ns-1,1))*cosPhi
        slice(2,l+Ni-1+Ns-1,1) = head_courbe(l,1) + valDist(l+Ni-1+Ns-1,2)*cos(valTheta(l+Ni-1+Ns-1,2))*sinPhi
        slice(2,l+Ni-1+Ns-1,2) = head_courbe(l,2) + valDist(l+Ni-1+Ns-1,2)*cos(valTheta(l+Ni-1+Ns-1,2))*cosPhi

        if (det(head_courbe(l-1,1),head_courbe(l-1,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),head_courbe(l,1),head_courbe(l,2))*det(head_courbe(l-1,1),head_courbe(l-1,2),slice2(1,thetatab(1),1),slice2(1,thetatab(1),2),slice(1,l+Ni-1+Ns-1,1),slice(1,l+Ni-1+Ns-1,2))>= 0._pr) then
           thetatab(1) = thetatab(1) + 1
           slice2(1,thetatab(1),1) = slice(1,l+ni-1+Ns-1,1)
           slice2(1,thetatab(1),2) = slice(1,l+Ni-1+Ns-1,2)
        endif
        do theta=2,nbtheta
           if (det(head_courbe(l-1,1),head_courbe(l-1,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),head_courbe(l,1),head_courbe(l,2))*det(head_courbe(l-1,1),head_courbe(l-1,2),slice2(theta,thetatab(theta),1),slice2(theta,thetatab(theta),2),slice(theta,l+Ni-1+Ns-1,1),slice(theta,l+Ni-1+Ns-1,2))>= 0._pr) then
              thetatab(theta) = thetatab(theta) + 1
              slice2(theta,thetatab(theta),1) = slice(theta,l+Ni-1+Ns-1,1)
              slice2(theta,thetatab(theta),2) = slice(theta,l+Ni-1+Ns-1,2)
           endif
        enddo
        if (errorl==1) write(*,*) "WARNING : ",errorl," ",l
     enddo
     l = Nf
     slice(1,l+Ni-1+Ns-1,1) = head_courbe(l,1)
     slice(1,l+Ni-1+Ns-1,2) = head_courbe(l,2)
     do theta=1,nbtheta
        thetatab(theta) = thetatab(theta) + 1
        slice2(theta,thetatab(theta),1) = head_courbe(l,1)
        slice2(theta,thetatab(theta),2) = head_courbe(l,2)
     enddo
     write(*,*) "lpl lpr finaux : ",thetatab(1)," ",thetatab(2)

     deallocate(midline)
     deallocate(points_control)

!!     open(unit=85,file='results/skelhead'//str(kt+140-1)//'.txt',status='unknown')
!!     open(unit=84,file='results/right'//str(kt+140-1)//'.txt',status='unknown')
!!     open(unit=83,file='results/left'//str(kt+140-1)//'.txt',status='unknown')
!!     open(unit=82,file='results/skeletteq'//str(kt+140-1)//'.txt',status='unknown')
!!     open(unit=81,file='results/skelett'//str(kt+140-1)//'.txt',status='unknown')
     open(unit=79,file='results/skelet'//str(kt+140-1)//'.vtk',status='unknown')
     write(79,'(1A26)') '# vtk DataFile Version 2.0'
     write(79,'(a)') 'rho'
     write(79,'(a)') 'ASCII'
     write(79,'(a)') 'DATASET STRUCTURED_POINTS'
     write(79,'(a,I4,I4,I4)') 'DIMENSIONS', N,ny,1
     write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
     write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,1. 
     write(79,'(a,I6)') 'POINT_DATA          ' , N*ny 
     write(79,'(a)') 'SCALARS values double'
     write(79,'(a)') 'LOOKUP_TABLE default'

     do k=1,ny
        do j=1,N
           write(79,*) rhoSlices2(j,k)*(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k)
        enddo
     enddo
!!     do l=1,Ni-1
!!        write(82,*) tail_courbe(Ni-l+1,1)," ",tail_courbe(Ni-l+1,2)
!!     enddo
!!     do l=1,Ns
!!        write(81,*) points_courbe(l,1)," ",points_courbe(l,2)
!!        write(82,*) points_courbe_equal(l,1)," ",points_courbe_equal(l,2)
!!     enddo
!!     do l=2,Nf
!!        write(85,*) head_courbe(l,1)," ",head_courbe(l,2)
!!     enddo
!!
!!     do l=1,thetatab(1)
!!        write(83,*) slice2(1,l,1)," ",slice2(1,l,2)
!!     enddo
!!     do l=1,thetatab(2)
!!        write(84,*) slice2(2,l,1)," ",slice2(2,l,2)
!!     enddo
!!     write(80,*) kt,"    ",sum(skel)," ",long3," ",long2," ",long
     close(79)    
!!     close(81)
!!     close(82)
!!     close(83)
!!     close(84)
!!     close(85)

  enddo
!!  close(80)
  write(*,*) "************************************************" 
  deallocate(rhoSlices2)
  deallocate(rhoSlices)
  deallocate(rho0) 
  deallocate(xx,yy)
  deallocate(u,v,rho,rhou,rhov) 
  deallocate(rhoup,rhovp,rhop)
  deallocate(uu,vv,rhouu,rhovv,rhouup,rhovvp)
  deallocate(u0,v0)
  deallocate(tmp1,tmp2,tmpbool)
  deallocate(dir1,dir2,dir3,dir4,Nseed,skel,skel2,skel3)
  deallocate(un,zero)
  deallocate(distslice)
  deallocate(points_courbe,points_courbe_equal)
  deallocate(slice_courbe,slice_courbe_equal)
  deallocate(tail_courbe,head_courbe)
  deallocate(slice,slice2,thetatab,xTheta,yTheta,valTheta)
  deallocate(longslice,longslicei,longslicef,sslice,dsslice,slicemid)
end program def3D
