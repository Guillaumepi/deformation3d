module variables
  use doubler  
  implicit none
 
  integer :: N
  integer :: nx, ny, nz
  real(pr) :: dx, dy, dz, dt 
  real(pr) :: a, bbb !!,LL
  real(pr), dimension(4) :: beta, c, alp
  real(pr) :: eepsilon
  real(pr), dimension(:), allocatable :: xx, yy, zz 

!  real(pr), dimension(:,:), allocatable :: xp,yp,rhouu,rhovv, rhovvp, rhouup
!  real(pr), dimension(:,:), allocatable :: rho0
!  real(pr), dimension(:,:), allocatable :: rhoup, rhovp!,  u,v,rhou,rhov
!  real(pr), dimension(:,:), allocatable :: rhop 
!  !real(pr), dimension(:,:), allocatable :: rho, rhoNext
!  integer :: MPI_PR
  
end module variables
