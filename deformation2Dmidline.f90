program def3D
  use variables
!!  use advection
  use interpolation
  use AdvectionProblem
  use TimeScheme
  use libBezier
  
  implicit none
  integer :: i, j, k, kt, idisplay, iUpdateDist, iter, picNum, l, pix1, pix2, l0, pos, sizeSkel, nl,lp,bool,loopbool,xl,xr,yr,yl&
,xskelL,yskelL,xskelR,yskelR,lpl,lpr,ii,lph,lpt,lpli,lpri 
  real(pr) :: Tps, threshold, surf, maxv, minimL, minimL_m, minimL_p, minimR, long, tb, dtb, long2, long3, longleft, longright&
, dti, dtf, dsi, dsf, longlefti, longleftf, longrighti, longrightf, dshl, dshr, dstl, dstr
  real(pr), dimension(:,:), allocatable :: rho, uu, vv, u0, v0, u, v, rhou, rhov 
  real(pr) :: t, tPic, xi, yj, distW, distWb, maxLS, minLS, seuilLS, xLeft, yLeft, xRight, yRight
  real(pr), dimension(:,:), allocatable :: rhoSlices, rhoSlices2,LS,gradPhi
  integer, dimension(:,:), allocatable :: midline,ind
  real(pr), dimension(:,:), allocatable :: tmp, tmp1, tmp2, un, zero,distslice,voisin
  integer, dimension(:,:), allocatable :: dir1, dir2, dir3, dir4, Nseed, skel, skel2, tmpbool, slice, skel3
  real(pr), dimension(:), allocatable :: xLeft_tab, yLeft_tab, xRight_tab, yRight_tab
  real(pr) :: px,py,s0,sinit,tbm,tbp,s,ds,sright,sleft,dsright,dsleft
  real(pr),dimension(:,:),allocatable :: points_control, points_courbe, points_courbe_equal, tail_courbe, head_courbe, righttail&
, lefttail, righthead, lefthead, leftmid, rightmid, leftcontrolm, rightcontrolm, leftcontrolf, rightcontrolf, leftcontroli&
, rightcontroli
  real(pr),dimension(:,:),allocatable :: left_control, left_courbe, left_courbe_equal
  real(pr),dimension(:,:),allocatable :: right_control, right_courbe, right_courbe_equal
  integer :: Ns,Ni,Nf,nt,errorl,errorr,itail,ihead
  real(pr) :: deltal,deltar,disttail,disthead,rhead,rtail,disttaillY,disttailrY,distheadlY,distheadrY

  N =  200 
  nx = 200 
  ny = 200 
  eepsilon = 1.e-6_pr
  dt = 1
  threshold = 0.001
  dx = 1._pr 
  dy = 1._pr 
  dz = 1.
  Tps = 1.
  Ns = 1000
  Ni = 10
  Nf = 100
  dtb = 1._pr/(Ns-1)
  allocate(head_courbe(Nf,2),tail_courbe(Ni,2),righthead(Nf,2),lefthead(Nf,2),righttail(Ni,2),lefttail(Ni,2),leftmid(Ns,2)&
,rightmid(Ns,2))
  allocate(points_courbe(Ns,2),points_courbe_equal(Ns,2))
  allocate(rhoSlices2(nx,ny),gradPhi(nx,ny))
  allocate(un(N,N),zero(N,N))
  allocate(tmp(N,N),tmp1(N,N),tmp2(N,N),tmpbool(N,N))
  allocate(rhoSlices(nx,ny))
  allocate(xx(1:nx),yy(1:ny)) 
!  allocate(xp(1:nx,1:ny),yp(1:nx,1:ny)) 
!  allocate(rho0(1:nx,1:ny)) 
!  allocate(rhou(1:nx,1:ny),rhov(1:nx,1:ny))  
!  allocate(rhouu(1:nx,1:ny),rhovv(1:nx,1:ny)) 
!  allocate(rhouup(1:nx,1:ny),rhovvp(1:nx,1:ny)) 
!  allocate(rhop(1:nx,1:ny),rhoup(1:nx,1:ny),rhovp(1:nx,1:ny)) 
  allocate(u(1:nx,1:ny),v(1:nx,1:ny)) 
  allocate(rho(1:nx,1:ny))
  allocate(uu(1:nx,1:ny), vv(1:nx,1:ny))
  allocate(u0(1:nx,1:ny), v0(1:nx,1:ny))
  allocate(dir1(N,N),dir2(N,N),dir3(N,N),dir4(N,N),Nseed(N,N),skel(N,N),skel2(N,N),skel3(N,N))

  un = 1._pr
  zero = 0._pr

  do i=1,nx
    xx(i) = 1+(float(i)-1)*dx
  enddo  
  do j=1,ny
    yy(j) = 1+(float(j)-1)*dy
  enddo
 
  picNum = 1 !140
  pos = nx/2
  yLeft = ny
  xLeft = pos
  yRight = 1
  xRight = pos
    open(unit=78,file="/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_inidroit.txt",status='unknown')
    do k=1,nx
      do j=1,ny
          read(78,*) rhoSlices2(k,j)
      enddo
    enddo
    close(78)
    rhoSlices2 = rhoSlices2 - 0.5_pr*(maxval(rhoSlices2) + minval(rhoSlices2))
!!    open(unit=79,file='results/sol00.vtk',status='unknown')
!!      write(79,'(1A26)') '# vtk DataFile Version 2.0'
!!      write(79,'(a)') 'rho'
!!      write(79,'(a)') 'ASCII'
!!      write(79,'(a)') 'DATASET STRUCTURED_POINTS'
!!      write(79,'(a,I4,I4,I4)') 'DIMENSIONS', N,ny,1
!!      write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
!!      write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,1. 
!!      write(79,'(a,I6)') 'POINT_DATA          ' , N*ny 
!!      write(79,'(a)') 'SCALARS values double'
!!      write(79,'(a)') 'LOOKUP_TABLE default'
!!    
!!    do k=1,ny
!!      do j=1,N
!!        write(79,*) rhoSlices2(j,k)
!!      enddo
!!    enddo
!!    close(79)    
    call updateDistance(rhoSlices2,gradPhi)
  
    dir1 = 0
    dir2 = 0
    dir3 = 0
    dir4 = 0
    Nseed = 0
    skel = 0
    do i=2,N-1
      do j=2,ny-1
        !if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j))) dir1(i,j) = 1
        !if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j-1))) dir2(i,j) = 1
        !if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j-1))) dir3(i,j) = 1
        !if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j-1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j+1))) dir4(i,j) = 1
        !Nseed(i,j) = dir1(i,j) + dir2(i,j) + dir3(i,j) + dir4(i,j)
        !if ((Nseed(i,j)>=2).and.(rhoSlices2(i,j)>0)) skel(i,j) = 1
        !if ((i==146).and.(j==109)) skel(i,j) = 0 !write(*,*) "testpoint ",Nseed(i,j)
        !if ((i==147).and.(j==109)) skel(i,j) = 0 !write(*,*) "testpoint ",Nseed(i,j)
        if ((gradPhi(j,i)<0.73).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
      enddo
    enddo
    l = 1
    pix1 = 0
    pix2 = 0
    do while ((pix1==1).or.(pix2==1).or.(l==1))
      pix1 = 0
      pix2 = 0
      l = l + 1
      skel2 = skel
      do i=2,N-1
        do j=2,ny-1
          maxv = 0
          if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
          if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
          if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
          if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
          if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
          if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
          if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
          if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
          Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1)&
 + skel(i+1,j-1)
          if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i+1,j)==0).or.(skel(i-1,j)==0)&
.or.(skel(i,j-1)==0)).and.((skel(i,j-1)==0).or.(skel(i,j+1)==0).or.(skel(i,j+1)==0))) then
                  skel2(i,j) = 0
                  pix1 = 1
          endif
        enddo
      enddo
      skel = skel2
      skel2 = skel
      do i=2,N-1
        do j=2,ny-1
          maxv = 0
          if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
          if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
          if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
          if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
          if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
          if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
          if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
          if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
          Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1)&
+ skel(i+1,j-1)
          if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i,j+1)==0).or.(skel(i-1,j)==0)&
.or.(skel(i,j-1)==0)).and.((skel(i+1,j)==0).or.(skel(i,j+1)==0).or.(skel(i-1,j)==0))) then
                  skel2(i,j) = 0
                  pix2 = 1
          endif
        enddo
      enddo
      skel = skel2
    enddo
    skel2 = skel
    do i=2,N-1
      do j=2,ny-1
        Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
skel(i+1,j-1)
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i-1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j+1)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
      enddo
    enddo
    skel = skel2
    skel2 = skel
    do i=2,N-1
      do j=2,ny-1
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j)==1).and.(skel2(i-1,j+1)==1)) skel2(i-1,j) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j+1)==1).and.(skel2(i,j+1)==1)) skel2(i,j+1) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j+1)==1).and.(skel2(i+1,j+1)==1)) skel2(i,j+1) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j+1)==1).and.(skel2(i+1,j)==1)) skel2(i+1,j) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j)==1).and.(skel2(i+1,j-1)==1)) skel2(i+1,j) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j-1)==1).and.(skel2(i,j-1)==1)) skel2(i,j-1) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j-1)==1).and.(skel2(i-1,j-1)==1)) skel2(i,j-1) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j-1)==1).and.(skel2(i-1,j)==1)) skel2(i-1,j) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)==0)) skel2(i,j) = 0
      enddo
    enddo
    skel = skel2
  
        
    tmpbool = 0
    nl = sum(skel)
    sizeSkel = sum(skel)
    !allocate(midline(sizeSkel,2),distslice(2*sizeSkel,2),xLeft_tab(sizeSkel), yLeft_tab(sizeSkel), xRight_tab(sizeSkel), yRight_tab(sizeSkel))
    allocate(midline(sizeSkel,2),xLeft_tab(sizeSkel), yLeft_tab(sizeSkel), xRight_tab(sizeSkel), yRight_tab(sizeSkel))
    l=1
    midline = 0
    do j=1,N
      do k=1,ny
        if ((skel(j,k)==1).and.(l==1)) then
          midline(l,1) = j
          midline(l,2) = k
          if (j==nx/2) l0 = l
          l = l+1
        endif
      enddo
    enddo
        xskelL = midline(1,1)
        yskelL = midline(1,2)
        do l=2,sum(skel)
        do i=2,N-1
          do j=2,ny-1
!              if (skel(i,j)==1) then
!                Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
!                if ((Nseed(i,j)>1).and.((midline(l-1,1)==i).or.(midline(l-1,1)==i-1).or.(midline(l-1,1)==i+1)).and.((midline(l-1,2)==j).or.(midline(l-1,2)==j-1).or.(midline(l-1,2)==j+1)).and.(appartient(midline,i,j,l).neqv..true.)) then 
!                        midline(l,1) = i
!                        midline(l,2) = j
!                endif
!                if ((Nseed(i,j)==1).and.(l==sum(skel))) then
!                        midline(l,1) = i
!                        midline(l,2) = j
!                endif
!              endif
              if (skel(i,j)==1) then
                Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1)&
 + skel(i+1,j-1)
                !if ((Nseed(i,j)>1).and.(((midline(l-1,1)==i).and.(midline(l-1,2).ne.j)).or.(midline(l-1,1)==i-1).or.(midline(l-1,1)==i+1))) then 
                if ((Nseed(i,j)>1).and.(((midline(l-1,1)==i).and.(midline(l-1,2).ne.j)).or.(midline(l-1,1)==i-1).or.(midline(l-1,1)&
==i+1)).and.((midline(l-1,2)==j-1).or.(midline(l-1,2)==j).or.(midline(l-1,2)==j+1))) then 
                        midline(l,1) = i
                        midline(l,2) = j
                endif
            endif
          enddo
        enddo
        enddo
    !skel3 = 0
    !do l=1,sum(skel)
    !  if (skel(midline(l,1),midline(l,2))==1) skel3(midline(l,1),midline(l,2)) = 1
    !enddo
    !skel = skel3
    
    allocate(distslice(2*(Ns+Ni+Nf),2))
    allocate(left_courbe(Ns+Ni+Nf-2,2),left_courbe_equal(Ns+Ni+Nf-2,2))
    allocate(right_courbe(Ns+Ni+Nf-2,2),right_courbe_equal(Ns+Ni+Nf-2,2))
    allocate(points_control(sum(skel),2))!,leftcontrolm(sum(skel),2),rightcontrolm(sum(skel),2))
    do l=1,sum(skel)
      points_control(l,1) = xx(midline(l,1))
      points_control(l,2) = yy(midline(l,2))
    enddo
    points_courbe(1,1) = points_control(1,1)
    points_courbe(1,2) = points_control(1,2)
    points_courbe(Ns,1) = points_control(size(points_control,1),1)
    points_courbe(Ns,2) = points_control(size(points_control,1),2)

    tb = dtb
    l = 1
    long = 0._pr
    do while ((tb<1._pr).and.(l+1<Ns))
      l = l+1
      call pointsBezierN(points_control,tb,px,py)
      points_courbe(l,1) = px
      points_courbe(l,2) = py
      long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))*(points_courbe(l,1)-points_courbe(l-1,1)) + (points_courbe(l,2)-&
points_courbe(l-1,2))*(points_courbe(l,2)-points_courbe(l-1,2)))
      tb = tb+dtb
    enddo
    long = long + sqrt((points_courbe(Ns,1)-points_courbe(Ns-1,1))*(points_courbe(Ns,1)-points_courbe(Ns-1,1)) +&
(points_courbe(Ns,2)-points_courbe(Ns-1,2))*(points_courbe(Ns,2)-points_courbe(Ns-1,2))) 
    long2 = long
    ds = long/(Ns-1)
        points_courbe_equal(1,1) = points_control(1,1)
        points_courbe_equal(1,2) = points_control(1,2)
        points_courbe_equal(Ns,1) = points_control(size(points_control,1),1)
        points_courbe_equal(Ns,2) = points_control(size(points_control,1),2)
        l = 1
        long = 0._pr
        tb = 0._pr
        do while ((tb<1._pr).and.(l+1<Ns))
          l = l+1
          nt = 1
          s = 0._pr
          do while ((l-1)*ds-s>0._pr) 
            nt = nt+1
            s = s + sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))*(points_courbe(nt,1)-points_courbe(nt-1,1)) +&
(points_courbe(nt,2)-points_courbe(nt-1,2))*(points_courbe(nt,2)-points_courbe(nt-1,2)))
          enddo
          tbm = (nt-2)*dtb
          tbp = (nt-1)*dtb
          s0 = s
          sinit = s - sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))*(points_courbe(nt,1)-points_courbe(nt-1,1))+&
(points_courbe(nt,2)-points_courbe(nt-1,2))*(points_courbe(nt,2)-points_courbe(nt-1,2)))
          s = sinit
          do while (abs((l-1)*ds-s)>eepsilon)
            tb = (tbm + tbp)*0.5_pr
            call pointsBezierN(points_control,tb,px,py)
            s = sinit + sqrt((px-points_courbe(nt-1,1))*(px-points_courbe(nt-1,1))+(py-points_courbe(nt-1,2))*&
(py-points_courbe(nt-1,2)))
            if ((l-1)*ds-s>0._pr) then
              tbm = tb
            else
              tbp = tb
            endif
          enddo
          call pointsBezierN(points_control,tb,px,py)
          points_courbe_equal(l,1) = px
          points_courbe_equal(l,2) = py
          long = long + sqrt((points_courbe_equal(l,1)-points_courbe_equal(l-1,1))*(points_courbe_equal(l,1)-&
points_courbe_equal(l-1,1)) + (points_courbe_equal(l,2)-points_courbe_equal(l-1,2))*(points_courbe_equal(l,2)-&
points_courbe_equal(l-1,2)))
        enddo
        long = long + sqrt((points_courbe_equal(Ns,1)-points_courbe_equal(Ns-1,1))*(points_courbe_equal(Ns,1)-&
points_courbe_equal(Ns-1,1)) + (points_courbe_equal(Ns,2)-points_courbe_equal(Ns-1,2))*(points_courbe_equal(Ns,2)-&
points_courbe_equal(Ns-1,2))) 
        long3 = 0._pr
        do l=1,sum(skel)-1
          long3 = long3 +sqrt((xx(midline(l+1,1))-xx(midline(l,1)))*(xx(midline(l+1,1))-xx(midline(l,1))) + (yy(midline(l+1,2))-&
yy(midline(l,2)))*(yy(midline(l+1,2))-yy(midline(l,2))))
        enddo
!    lp=l0!+4
    do i=2,nx-1
      if ((rhoSlices2(i,midline(1,2))>0._pr).and.(rhoSlices2(i-1,midline(1,2))<0._pr)) then 
              itail = i
              rtail = xx(i-1) - rhoSlices2(i,midline(1,2))*dx/(rhoSlices2(i-1,midline(1,2)) - rhoSlices2(i,midline(1,2)))
      endif
      if ((rhoSlices2(i,midline(1,2))>0._pr).and.(rhoSlices2(i+1,midline(1,2))<0._pr)) then
              ihead = i
              rhead = xx(i) - rhoSlices2(i,midline(1,2))*dx/(rhoSlices2(i+1,midline(1,2)) - rhoSlices2(i,midline(1,2)))
       endif
    enddo
    do j=2,ny-1
      if ((rhoSlices2(midline(1,1),ny-j+1)>0._pr).and.(rhoSlices2(midline(1,1),ny-(j+1)+1)<0._pr)) yRight =  xx(ny-j) -&
rhoSlices2(midline(1,1),ny-j+1)*dy/(rhoSlices2(midline(1,1),ny-(j+1)+1) - rhoSlices2(midline(1,1),ny-j+1)) !j+1
      if ((rhoSlices2(midline(1,1),j)>0._pr).and.(rhoSlices2(midline(1,1),j+1)<0._pr)) yLeft =  xx(j) -&
 rhoSlices2(midline(1,1),j)*dy/(rhoSlices2(midline(1,1),j+1) - rhoSlices2(midline(1,1),j)) !j
      !if ((rhoSlices2(midline(1,1),ny-j+1)>0._pr).and.(rhoSlices2(midline(1,1),ny-(j+1)+1)<0._pr)) yRight =  xx(ny-j+1) + rhoSlices2(midline(1,1),ny-j+1)*dy/(rhoSlices2(midline(1,1),ny-(j+1)+1) - rhoSlices2(midline(1,1),ny-j+1)) !j+1
      !if ((rhoSlices2(midline(1,1),j)>0._pr).and.(rhoSlices2(midline(1,1),j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(midline(1,1),j)*dy/(rhoSlices2(midline(1,1),j+1) - rhoSlices2(midline(1,1),j)) !j
    enddo
    disttaillY = dist(xx(midline(1,1)),xx(midline(1,2)),xx(midline(1,1)),yLeft)
    disttailrY = dist(xx(midline(1,1)),xx(midline(1,2)),xx(midline(1,1)),yRight)
    disttail = abs(rtail - points_courbe_equal(1,1))
    do j=2,ny-1
      if ((rhoSlices2(midline(sum(skel),1),ny-j+1)>0._pr).and.(rhoSlices2(midline(sum(skel),1),ny-(j+1)+1)<0._pr)) yRight =&
xx(ny-j) - rhoSlices2(midline(sum(skel),1),ny-j+1)*dy/&
(rhoSlices2(midline(sum(skel),1),ny-(j+1)+1) -rhoSlices2(midline(sum(skel),1),ny-j+1)) !j+1
      if ((rhoSlices2(midline(sum(skel),1),j)>0._pr).and.(rhoSlices2(midline(sum(skel),1),j+1)<0._pr)) yLeft =  xx(j) -&
rhoSlices2(midline(sum(skel),1),j)*dy/(rhoSlices2(midline(sum(skel),1),j+1) - rhoSlices2(midline(sum(skel),1),j)) !j
      !if ((rhoSlices2(nint(points_courbe_equal(Ns,1)),ny-j+1)>0._pr).and.(rhoSlices2(nint(points_courbe_equal(Ns,1)),ny-(j+1)+1)<0._pr)) yRight =  xx(ny-j+1) + rhoSlices2(nint(points_courbe_equal(Ns,1)),ny-j+1)*dy/(rhoSlices2(nint(points_courbe_equal(Ns,1)),ny-(j+1)+1) - rhoSlices2(nint(points_courbe_equal(Ns,1)),ny-j+1)) !j+1
      !if ((rhoSlices2(nint(points_courbe_equal(Ns,1)),j)>0._pr).and.(rhoSlices2(nint(points_courbe_equal(Ns,1)),j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(nint(points_courbe_equal(Ns,1)),j)*dy/(rhoSlices2(nint(points_courbe_equal(Ns,1)),j+1) - rhoSlices2(nint(points_courbe_equal(Ns,1)),j)) !j
    enddo
    distheadlY = dist(xx(midline(sum(skel),1)),xx(midline(sum(skel),2)),xx(midline(sum(skel),1)),yLeft)
    distheadrY = dist(xx(midline(sum(skel),1)),xx(midline(sum(skel),2)),xx(midline(sum(skel),1)),yRight)
    disthead = abs(rhead-points_courbe_equal(Ns,1))
    write(*,*) "DIST : TAIL ",disttail," HEAD ",disthead
    !disthead = dist(xx(nint(points_courbe_equal(Ns,1))),yLeft,xx(nint(points_courbe_equal(Ns,1))),yRight)
    !allocate(left_control(floor(points_courbe_equal(Ns,1)+disthead)-ceiling(points_courbe_equal(1,1)-disttail)+1,2)) !sum(skel),2)
    !allocate(right_control(floor(points_courbe_equal(Ns,1)+disthead)-ceiling(points_courbe_equal(1,1)-disttail)+1,2)) !sum(skel),2))
    allocate(left_control(ihead-itail+1+2,2),leftcontrolm(floor(points_courbe_equal(Ns,1))-ceiling(points_courbe_equal(1,1))+1,2)&
,leftcontrolf(floor(disthead),2),leftcontroli(nint(disttail),2))
    allocate(right_control(ihead-itail+1+2,2),rightcontrolm(floor(points_courbe_equal(Ns,1))-ceiling(points_courbe_equal(1,1))+1,2)&
,rightcontrolf(floor(disthead),2),rightcontroli(nint(disttail),2))
    !do l=ceiling(points_courbe_equal(1,1)-disttail),floor(points_courbe_equal(Ns,1)+disthead)
    !itail = ceiling(points_courbe_equal(1,1)-disttail)
    !rtail = points_courbe_equal(1,1)-disttail
    !ihead = floor(points_courbe_equal(Ns,1)+disthead)
    !rhead = points_courbe_equal(Ns,1)+disthead
    lp = 0
    lph = 0
    lpt = 0
    do l=itail,ihead
      yRight = points_courbe_equal(1,2)
      yLeft = points_courbe_equal(1,2)
      do j=2,ny-1
        !if ((rhoSlices2(midline(l,1),ny-j+1)<0._pr).and.(rhoSlices2(midline(l,1),ny-(j+1)+1)>0._pr)) then
        if ((rhoSlices2(l,ny-j+1)>0._pr).and.(rhoSlices2(l,ny-(j+1)+1)<0._pr)) yRight =  xx(ny-j) - rhoSlices2(l,ny-j+1)*dy/&
(rhoSlices2(l,ny-(j+1)+1) - rhoSlices2(l,ny-j+1)) !j+1
        if ((rhoSlices2(l,j)>0._pr).and.(rhoSlices2(l,j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(l,j)*dy/(rhoSlices2(l,j+1) -&
rhoSlices2(l,j)) !j
      enddo
      
      !left_control(l-ceiling(points_courbe_equal(1,1)-disttail)+1,1) = xx(l)
      !left_control(l-ceiling(points_courbe_equal(1,1)-disttail)+1,2) = yLeft
      !right_control(l-ceiling(points_courbe_equal(1,1)-disttail)+1,1) = xx(l)
      !right_control(l-ceiling(points_courbe_equal(1,1)-disttail)+1,2) = yRight
      left_control(l-itail+1+1,1) = xx(l)
      left_control(l-itail+1+1,2) = yLeft
      if (l<points_courbe_equal(1,1)) then
        if (lpt==0) lpt = l
        leftcontroli(l-lpt+1,1) = xx(l)
        leftcontroli(l-lpt+1,2) = yLeft
        rightcontroli(l-lpt+1,1) = xx(l)
        rightcontroli(l-lpt+1,2) = yRight
      endif
      if ((l>=points_courbe_equal(1,1)).and.(l<=points_courbe_equal(Ns,1))) then
        if (lp==0) lp = l
        leftcontrolm(l-lp+1,1) = xx(l)
        leftcontrolm(l-lp+1,2) = yLeft
        rightcontrolm(l-lp+1,1) = xx(l)
        rightcontrolm(l-lp+1,2) = yRight
      endif
      if (l>points_courbe_equal(Ns,1)) then
        if (lph==0) lph = l
        leftcontrolf(l-lph+1,1) = xx(l)
        leftcontrolf(l-lph+1,2) = yLeft
        rightcontrolf(l-lph+1,1) = xx(l)
        rightcontrolf(l-lph+1,2) = yRight
      endif
      right_control(l-itail+1+1,1) = xx(l)
      right_control(l-itail+1+1,2) = yRight
    enddo
    !left_control(1,1) = rtail
    !left_control(1,2) = points_courbe_equal(1,2)
    !left_control(size(left_control,1),1) = rhead
    !left_control(size(left_control,1),2) = points_courbe_equal(1,2)
    !right_control(1,1) = rtail
    !right_control(1,2) = points_courbe_equal(1,2)
    !right_control(size(right_control,1),1) = rhead
    !right_control(size(right_control,1),2) = points_courbe_equal(1,2)
!    l=1
!    do j=2,ny-1
!      if ((rhoSlices2(midline(l,1),j)<0._pr).and.(rhoSlices2(midline(l,1),j+1)>0._pr)) yRight =  xx(j) - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j+1
!      if ((rhoSlices2(midline(l,1),j)>0._pr).and.(rhoSlices2(midline(l,1),j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j
!    enddo
!    xLeft_tab(l) = midline(l,1)
!    xRight_tab(l) = midline(l,1)
!    yLeft_tab(l) = yLeft
!    yRight_tab(l) = yRight
!    !distslice(l,1) = dist(xx(xLeft_tab(l)),yy(yLeft_tab(l)),xx(midline(l,1)),yy(midline(l,2)))
!!    distslice(l,1) = dist(xLeft_tab(l),yLeft_tab(l),xx(midline(l,1)),yy(midline(l,2)))
!    l=sum(skel)
!    do j=2,ny-1
!      if ((rhoSlices2(midline(l,1),j)<0._pr).and.(rhoSlices2(midline(l,1),j+1)>0._pr)) yRight =  xx(j) - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j+1
!      if ((rhoSlices2(midline(l,1),j)>0._pr).and.(rhoSlices2(midline(l,1),j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j
!    enddo
!    xLeft_tab(l) = midline(l,1)
!    xRight_tab(l) = midline(l,1)
!    yLeft_tab(l) = yLeft
!    yRight_tab(l) = yRight
!    !distslice(1,2) = dist(xx(xLeft_tab(l)),yy(yLeft_tab(l)),xx(midline(l,1)),yy(midline(l,2)))
!!    distslice(1,2) = dist(xLeft_tab(l),yLeft_tab(l),xx(midline(l,1)),yy(midline(l,2)))
!    do l=2,sum(skel)-1
!      do j=2,ny-1
!        !if ((rhoSlices2(midline(l,1),ny-j+1)<0._pr).and.(rhoSlices2(midline(l,1),ny-(j+1)+1)>0._pr)) then
!        if ((rhoSlices2(midline(l,1),ny-j+1)>0._pr).and.(rhoSlices2(midline(l,1),ny-(j+1)+1)<0._pr)) yRight =  xx(ny-j) - rhoSlices2(midline(l,1),ny-j+1)*dy/(rhoSlices2(midline(l,1),ny-(j+1)+1) - rhoSlices2(midline(l,1),ny-j+1)) !j+1
!        if ((rhoSlices2(midline(l,1),j)>0._pr).and.(rhoSlices2(midline(l,1),j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j
!      enddo
!      xLeft_tab(l) = xx(midline(l,1))
!      xRight_tab(l) = xx(midline(l,1))
!      yLeft_tab(l) = yLeft
!      yRight_tab(l) = yRight
!      !distslice(l,1) = dist(xx(xLeft_tab(l)),yy(yLeft_tab(l)),xx(midline(l-1,1)),yy(midline(l-1,2)))
!      !distslice(l,2) = dist(xx(xLeft_tab(l)),yy(yLeft_tab(l)),xx(midline(l+1,1)),yy(midline(l+1,2)))
!      !distslice(sizeSkel+l,1) = dist(xx(xRight_tab(l)),yy(yRight_tab(l)),xx(midline(l-1,1)),yy(midline(l-1,2)))
!      !distslice(sizeSkel+l,2) = dist(xx(xRight_tab(l)),yy(yRight_tab(l)),xx(midline(l+1,1)),yy(midline(l+1,2)))
! !     distslice(l,1) = dist(xLeft_tab(l),yLeft_tab(l),xx(midline(l-1,1)),yy(midline(l-1,2)))
! !     distslice(l,2) = dist(xLeft_tab(l),yLeft_tab(l),xx(midline(l+1,1)),yy(midline(l+1,2)))
! !     distslice(sizeSkel+l,1) = dist(xRight_tab(l),yRight_tab(l),xx(midline(l-1,1)),yy(midline(l-1,2)))
! !     distslice(sizeSkel+l,2) = dist(xRight_tab(l),yRight_tab(l),xx(midline(l+1,1)),yy(midline(l+1,2)))
!    enddo
!    do l=1,sum(skel)
!      left_control(l,1) = xLeft_tab(l)
!      left_control(l,2) = yLeft_tab(l)
!      right_control(l,1) = xRight_tab(l)
!      right_control(l,2) = yRight_tab(l)
!    enddo
    !left_courbe(1,1) = rtail !left_control(1,1)
    !left_courbe(1,2) = points_courbe_equal(1,2) !left_control(1,2)
    !right_courbe(1,1) = rtail !right_control(1,1)
    !right_courbe(1,2) = points_courbe_equal(1,2) !right_control(1,2)
    !left_courbe(Ni,1) = points_courbe_equal(1,1)
    !left_courbe(Ni,2) = points_courbe_equal(1,2) + disttaillY
    !right_courbe(Ni,1) = points_courbe_equal(1,1)
    !right_courbe(Ni,2) = points_courbe_equal(1,2) - disttailrY
    !left_courbe(Ni-1+Ns,1) = points_courbe_equal(size(points_courbe_equal,1),1)
    !left_courbe(Ni-1+Ns,2) = points_courbe_equal(size(points_courbe_equal,1),1) + distheadlY
    !right_courbe(Ni-1+Ns,1) = points_courbe_equal(size(points_courbe_equal,1),1)
    !right_courbe(Ni-1+Ns,2) = points_courbe_equal(size(points_courbe_equal,1),1) - distheadrY
    !left_courbe(size(left_courbe,1),1) = rhead !left_control(size(left_control,1),1)
    !left_courbe(size(left_courbe,1),2) = points_courbe_equal(1,2) !left_control(size(left_control,1),2)
    !right_courbe(size(right_courbe,1),1) = rhead !right_control(size(right_control,1),1)
    !right_courbe(size(right_courbe,1),2) = points_courbe_equal(1,2) !right_control(size(right_control,1),2)

    dtb = 1._pr/(Ni-1)
    tb = dtb
    leftcontroli(1,1) = rtail
    leftcontroli(1,2) = points_courbe_equal(1,2) 
    !leftcontroli(size(leftcontroli,1),1) = points_courbe_equal(1,1)
    !leftcontroli(size(leftcontroli,1),2) = points_courbe_equal(1,2) + disttaillY
    rightcontroli(1,1) = rtail
    rightcontroli(1,2) = points_courbe_equal(1,2) 
    !rightcontroli(size(rightcontroli,1),1) = points_courbe_equal(1,1)
    !rightcontroli(size(rightcontroli,1),2) = points_courbe_equal(1,2) - disttailrY
    l = 1
    left_courbe(1,1) = leftcontroli(1,1)
    left_courbe(1,2) = leftcontroli(1,2)
    right_courbe(l,1) = rightcontroli(1,1)
    right_courbe(l,2) = rightcontroli(1,2)
    longlefti = 0._pr
    longrighti = 0._pr
    do while ((tb<1._pr).and.(l+1<Ni))
      l = l+1
      call pointsBezierN(leftcontroli,tb,px,py)
      left_courbe(l,1) = px
      left_courbe(l,2) = py
      call pointsBezierN(rightcontroli,tb,px,py)
      right_courbe(l,1) = px
      right_courbe(l,2) = py
      longlefti = longlefti + sqrt((left_courbe(l,1)-left_courbe(l-1,1))*(left_courbe(l,1)-left_courbe(l-1,1)) + (left_courbe(l,2)-&
left_courbe(l-1,2))*(left_courbe(l,2)-left_courbe(l-1,2)))
      longrighti = longrighti + sqrt((right_courbe(l,1)-right_courbe(l-1,1))*(right_courbe(l,1)-right_courbe(l-1,1)) +&
(right_courbe(l,2)-right_courbe(l-1,2))*(right_courbe(l,2)-right_courbe(l-1,2)))
      tb = tb+dtb
    enddo
    dtb = 1._pr/(Ns-1)
    tb = dtb
    leftcontrolm(1,1) = points_courbe_equal(1,1)
    leftcontrolm(1,2) = points_courbe_equal(1,2) + disttaillY
    leftcontrolm(size(leftcontrolm,1),1) = points_courbe_equal(size(points_courbe_equal,1),1)
    leftcontrolm(size(leftcontrolm,1),2) = points_courbe_equal(size(points_courbe_equal,1),2) + distheadlY
    rightcontrolm(1,1) = points_courbe_equal(1,1)
    rightcontrolm(1,2) = points_courbe_equal(1,2) - disttailrY
    rightcontrolm(size(rightcontrolm,1),1) = points_courbe_equal(size(points_courbe_equal,1),1)
    rightcontrolm(size(rightcontrolm,1),2) = points_courbe_equal(size(points_courbe_equal,1),2) - distheadrY
    l = l+1
    left_courbe(l,1) = leftcontrolm(1,1)
    left_courbe(l,2) = leftcontrolm(1,2)
    right_courbe(l,1) = rightcontrolm(1,1)
    right_courbe(l,2) = rightcontrolm(1,2)
    longlefti = longlefti + sqrt((left_courbe(l,1)-left_courbe(l-1,1))*(left_courbe(l,1)-left_courbe(l-1,1)) + (left_courbe(l,2)-&
left_courbe(l-1,2))*(left_courbe(l,2)-left_courbe(l-1,2)))
    longrighti = longrighti + sqrt((right_courbe(l,1)-right_courbe(l-1,1))*(right_courbe(l,1)-right_courbe(l-1,1)) +&
(right_courbe(l,2)-right_courbe(l-1,2))*(right_courbe(l,2)-right_courbe(l-1,2)))
    longleft = 0._pr
    longright = 0._pr
    do while ((tb<1._pr).and.(l+1<Ni-1+Ns))
      l = l+1
      call pointsBezierN(leftcontrolm,tb,px,py)
      left_courbe(l,1) = px
      left_courbe(l,2) = py
      call pointsBezierN(rightcontrolm,tb,px,py)
      right_courbe(l,1) = px
      right_courbe(l,2) = py
      longleft = longleft + sqrt((left_courbe(l,1)-left_courbe(l-1,1))*(left_courbe(l,1)-left_courbe(l-1,1)) + (left_courbe(l,2)-&
left_courbe(l-1,2))*(left_courbe(l,2)-left_courbe(l-1,2)))
      longright = longright + sqrt((right_courbe(l,1)-right_courbe(l-1,1))*(right_courbe(l,1)-right_courbe(l-1,1)) +&
(right_courbe(l,2)-right_courbe(l-1,2))*(right_courbe(l,2)-right_courbe(l-1,2)))
      tb = tb+dtb
    enddo
    l = l+1
    left_courbe(l,1) = leftcontrolm(size(leftcontrolm,1),1)
    left_courbe(l,2) = leftcontrolm(size(leftcontrolm,1),2)
    right_courbe(l,1) = rightcontrolm(size(rightcontrolm,1),1)
    right_courbe(l,2) = rightcontrolm(size(rightcontrolm,1),2)
    longleft = longleft + sqrt((left_courbe(l,1)-left_courbe(l-1,1))*(left_courbe(l,1)-left_courbe(l-1,1)) + (left_courbe(l,2)-&
left_courbe(l-1,2))*(left_courbe(l,2)-left_courbe(l-1,2)))
    longright = longright + sqrt((right_courbe(l,1)-right_courbe(l-1,1))*(right_courbe(l,1)-right_courbe(l-1,1)) +&
(right_courbe(l,2)-right_courbe(l-1,2))*(right_courbe(l,2)-right_courbe(l-1,2)))
    dtb = 1._pr/(Nf-1)
    tb = dtb
    longleftf = 0._pr
    longrightf = 0._pr
    leftcontrolf(1,1) = points_courbe_equal(size(points_courbe_equal,1),1)
    leftcontrolf(1,2) = points_courbe_equal(size(points_courbe_equal,1),2) + distheadlY
    leftcontrolf(size(leftcontrolf,1),1) = rhead
    leftcontrolf(size(leftcontrolf,1),2) = points_courbe_equal(1,2) 
    rightcontrolf(1,1) = points_courbe_equal(size(points_courbe_equal,1),1)
    rightcontrolf(1,2) = points_courbe_equal(size(points_courbe_equal,1),2) - distheadrY
    rightcontrolf(size(rightcontrolf,1),1) = rhead
    rightcontrolf(size(rightcontrolf,1),2) = points_courbe_equal(1,2) 
    do while ((tb<1._pr).and.(l+1<Ni-1+Ns+Nf-1)) !size(left_courbe,1)))
      l = l+1
      call pointsBezierN(leftcontrolf,tb,px,py)
      left_courbe(l,1) = px
      left_courbe(l,2) = py
      call pointsBezierN(rightcontrolf,tb,px,py)
      right_courbe(l,1) = px
      right_courbe(l,2) = py
      longleftf = longleftf + sqrt((left_courbe(l,1)-left_courbe(l-1,1))*(left_courbe(l,1)-left_courbe(l-1,1)) + (left_courbe(l,2)-&
left_courbe(l-1,2))*(left_courbe(l,2)-left_courbe(l-1,2)))
      longrightf = longrightf + sqrt((right_courbe(l,1)-right_courbe(l-1,1))*(right_courbe(l,1)-right_courbe(l-1,1)) +&
(right_courbe(l,2)-right_courbe(l-1,2))*(right_courbe(l,2)-right_courbe(l-1,2)))
      tb = tb+dtb
    enddo
    l = l+1
    left_courbe(l,1) = leftcontrolf(size(leftcontrolf,1),1)
    left_courbe(l,2) = leftcontrolf(size(leftcontrolf,1),2)
    right_courbe(l,1) = rightcontrolf(size(rightcontrolf,1),1)
    right_courbe(l,2) = rightcontrolf(size(rightcontrolf,1),2)
    longleftf = longleftf + sqrt((left_courbe(l,1)-left_courbe(l-1,1))*(left_courbe(l,1)-left_courbe(l-1,1)) + (left_courbe(l,2)-&
left_courbe(l-1,2))*(left_courbe(l,2)-left_courbe(l-1,2)))
    longrightf = longrightf + sqrt((right_courbe(l,1)-right_courbe(l-1,1))*(right_courbe(l,1)-right_courbe(l-1,1)) +&
(right_courbe(l,2)-right_courbe(l-1,2))*(right_courbe(l,2)-right_courbe(l-1,2)))
    dtb = 1._pr/(Ns-1)
    !longleft = longleft + sqrt((left_courbe(Ns,1)-left_courbe(Ns-1,1))*(left_courbe(Ns,1)-left_courbe(Ns-1,1)) + (left_courbe(Ns,2)-left_courbe(Ns-1,2))*(left_courbe(Ns,2)-left_courbe(Ns-1,2))) 
    !longright = longright + sqrt((right_courbe(Ns,1)-right_courbe(Ns-1,1))*(right_courbe(Ns,1)-right_courbe(Ns-1,1)) + (right_courbe(Ns,2)-right_courbe(Ns-1,2))*(right_courbe(Ns,2)-right_courbe(Ns-1,2))) 
    !dsleft = longleft/(size(left_courbe,1)-1)
    !dsright = longright/(size(right_courbe,1)-1)
    dsleft = longleft/(Ns-1)
    dsright = longright/(Ns-1)
    dstl = longlefti/(Ni-1)
    dshl = longleftf/(Nf-1)
    dstr = longrighti/(Ni-1)
    dshr = longrightf/(Nf-1)



    left_courbe_equal(1:Ni,1) = left_courbe(1:Ni,1)
    left_courbe_equal(1:Ni,2) = left_courbe(1:Ni,2) 
    left_courbe_equal(Ni-1+Ns:size(left_courbe_equal,1),1) = left_courbe(Ni-1+Ns:size(left_courbe_equal,1),1)
    left_courbe_equal(Ni-1+Ns:size(left_courbe_equal,1),2) = left_courbe(Ni-1+Ns:size(left_courbe_equal,1),2) 
    dtb = 1._pr/(Ns-1)
    l = 1
    !left_courbe_equal(l+Ni-1,1) = leftcontrolm(1,1)
    !left_courbe_equal(l+Ni-1,2) = leftcontrolm(1,2)
    tb = 0._pr
    leftmid(1:Ns,1) = left_courbe(Ni:Ni-1+Ns,1)
    leftmid(1:Ns,2) = left_courbe(Ni:Ni-1+Ns,2)
    do while ((tb<1._pr).and.(l+1<Ns)) !size(leftmid,1)))
      l = l+1
      nt = 1
      sleft = 0._pr
      do while ((l-1)*dsleft-sleft>0._pr) 
        nt = nt+1
        sleft = sleft + sqrt((leftmid(nt,1)-leftmid(nt-1,1))*(leftmid(nt,1)-leftmid(nt-1,1)) + (leftmid(nt,2)-leftmid(nt-1,2))&
*(leftmid(nt,2)-leftmid(nt-1,2)))
      enddo
      tbm = (nt-2)*dtb
      tbp = (nt-1)*dtb
      s0 = sleft
      write(*,*) "s0 l : ",s0," ",l
      sinit = sleft - sqrt((leftmid(nt,1)-leftmid(nt-1,1))*(leftmid(nt,1)-leftmid(nt-1,1))+(leftmid(nt,2)-leftmid(nt-1,2))&
*(leftmid(nt,2)-leftmid(nt-1,2)))
      sleft = sinit
      do while (abs((l-1)*dsleft-sleft)>eepsilon)
        tb = (tbm + tbp)*0.5_pr
        call pointsBezierN(leftcontrolm,tb,px,py)
        sleft = sinit + sqrt((px-leftmid(nt-1,1))*(px-leftmid(nt-1,1))+(py-leftmid(nt-1,2))*(py-leftmid(nt-1,2)))
        if ((l-1)*dsleft-sleft>0._pr) then
          tbm = tb
        else
          tbp = tb
        endif
      enddo
      call pointsBezierN(leftcontrolm,tb,px,py)
      left_courbe_equal(l+Ni-1,1) = px
      left_courbe_equal(l+Ni-1,2) = py
    enddo

    right_courbe_equal(1:Ni,1) = right_courbe(1:Ni,1)
    right_courbe_equal(1:Ni,2) = right_courbe(1:Ni,2) 
    right_courbe_equal(Ni-1+Ns:size(right_courbe_equal,1),1) = right_courbe(Ni-1+Ns:size(right_courbe_equal,1),1)
    right_courbe_equal(Ni-1+Ns:size(right_courbe_equal,1),2) = right_courbe(Ni-1+Ns:size(right_courbe_equal,1),2) 
    dtb = 1._pr/(Ns-1)
    l = 1
    tb = 0._pr
    leftmid(1:Ns,1) = left_courbe(Ni:Ni-1+Ns,1)
    leftmid(1:Ns,2) = left_courbe(Ni:Ni-1+Ns,2)
    rightmid(1:Ns,1) = right_courbe(Ni:Ni-1+Ns,1)
    rightmid(1:Ns,2) = right_courbe(Ni:Ni-1+Ns,2)
    do while ((tb<1._pr).and.(l+1<size(rightmid,1)))
      l = l+1
      nt = 1
      sright = 0._pr
      do while ((l-1)*dsright-sright>0._pr) 
        nt = nt+1
        sright = sright + sqrt((rightmid(nt,1)-rightmid(nt-1,1))*(rightmid(nt,1)-rightmid(nt-1,1)) + (rightmid(nt,2)-&
rightmid(nt-1,2))*(rightmid(nt,2)-rightmid(nt-1,2)))
      enddo
      tbm = (nt-2)*dtb
      tbp = (nt-1)*dtb
      s0 = sright
      sinit = sright - sqrt((rightmid(nt,1)-rightmid(nt-1,1))*(rightmid(nt,1)-rightmid(nt-1,1))+(rightmid(nt,2)-rightmid(nt-1,2))&
*(rightmid(nt,2)-rightmid(nt-1,2)))
      sright = sinit
      do while (abs((l-1)*dsright-sright)>eepsilon)
        tb = (tbm + tbp)*0.5_pr
        call pointsBezierN(rightcontrolm,tb,px,py)
        sright = sinit + sqrt((px-rightmid(nt-1,1))*(px-rightmid(nt-1,1))+(py-rightmid(nt-1,2))*(py-rightmid(nt-1,2)))
        if ((l-1)*dsright-sright>0._pr) then
          tbm = tb
        else
          tbp = tb
        endif
      enddo
      call pointsBezierN(rightcontrolm,tb,px,py)
      right_courbe_equal(l+Ni-1,1) = px
      right_courbe_equal(l+Ni-1,2) = py
    enddo





    leftcontroli(1,1) = rtail
    leftcontroli(1,2) = points_courbe_equal(1,2) 
    leftcontrolf(size(leftcontrolf,1),1) = rhead
    leftcontrolf(size(leftcontrolf,1),2) = points_courbe_equal(1,2) 


!    distslice(1+Ni,1) = dist(left_courbe_equal(1+Ni-1,1),left_courbe_equal(1+Ni-1,2),points_courbe_equal(1,1),points_courbe_equal(1,&
!2))
!    distslice(1+Ni,2) = dist(left_courbe_equal(1+Ni-1,1),left_courbe_equal(1+Ni-1,2),points_courbe_equal(2,1),points_courbe_equal(2,&
!2))
!    distslice(Ns+Ni+Nf+1+Ni,1) = dist(right_courbe_equal(1+Ni-1,1),right_courbe_equal(1+Ni-1,2),points_courbe_equal(1,1),points_cour&
!be_equal(1,2))
!    distslice(Ns+Ni+Nf+1+Ni,2) = dist(right_courbe_equal(1+Ni-1,1),right_courbe_equal(1+Ni-1,2),points_courbe_equal(2,1),points_cour&
!be_equal(2,2))
!    do l=2,Ns-1
!      distslice(l+Ni,1) = dist(left_courbe_equal(l+Ni-1,1),left_courbe_equal(l+Ni-1,2),points_courbe_equal(l-1,1),points_courbe_equa&
!l(l-1,2))
!      distslice(l+Ni,2) = dist(left_courbe_equal(l+Ni-1,1),left_courbe_equal(l+Ni-1,2),points_courbe_equal(l+1,1),points_courbe_equa&
!l(l+1,2))
!      distslice(Ns+Ni+Nf+l+Ni,1) = dist(right_courbe_equal(l+Ni-1,1),right_courbe_equal(l+Ni-1,2),points_courbe_equal(l-1,1),points_&
!courbe_equal(l-1,2))
!      distslice(Ns+Ni+Nf+l+Ni,2) = dist(right_courbe_equal(l+Ni-1,1),right_courbe_equal(l+Ni-1,2),points_courbe_equal(l+1,1),points_&
!courbe_equal(l+1,2))
!    enddo 
!    distslice(Ns+Ni,1) = dist(left_courbe_equal(Ns+Ni-1,1),left_courbe_equal(Ns+Ni-1,2),points_courbe_equal(Ns-1,1),points_courbe_eq&
!ual(Ns-1,2))
!    distslice(Ns+Ni,2) = dist(left_courbe_equal(Ns+Ni-1,1),left_courbe_equal(Ns+Ni-1,2),points_courbe_equal(Ns,1),points_courbe_equa&
!l(Ns,2))
!    distslice(Ns+Ni+Nf+Ns+Ni,1) = dist(right_courbe_equal(Ns+Ni-1,1),right_courbe_equal(Ns+Ni-1,2),points_courbe_equal(Ns-1,1),point&
!s_courbe_equal(Ns-1,2))
!    distslice(Ns+Ni+Nf+Ns+Ni,2) = dist(right_courbe_equal(Ns+Ni-1,1),right_courbe_equal(Ns+Ni-1,2),points_courbe_equal(Ns,1),points_&
!courbe_equal(Ns,2))
!
!    dsi = disttail/(Ni-1)
!    !distslice(1,1) = dist(left_courbe_equal(1,1),left_courbe_equal(1,2),points_courbe_equal(1,1),points_courbe_equal(1,2))
!    !distslice(1,2) = dist(left_courbe_equal(1,1),left_courbe_equal(1,2),points_courbe_equal(2,1),points_courbe_equal(2,2))
!    distslice(1,1) = dist(left_courbe_equal(1,1),left_courbe_equal(1,2),points_courbe_equal(1,1)-(Ni-1)*dsi,points_courbe_equal(1,2)&
!)
!    distslice(1,2) = dist(left_courbe_equal(1,1),left_courbe_equal(1,2),points_courbe_equal(1,1)-(Ni-2)*dsi,points_courbe_equal(1,2)&
!)
!    distslice(Ns+Ni+Nf+1,1) = dist(right_courbe_equal(1,1),right_courbe_equal(1,2),points_courbe_equal(1,1)-(Ni-1)*dsi,points_courbe&
!_equal(1,2))
!    distslice(Ns+Ni+Nf+1,2) = dist(right_courbe_equal(1,1),right_courbe_equal(1,2),points_courbe_equal(1,1)-(Ni-2)*dsi,points_courbe&
!_equal(1,2))
!    do l=2,Ni-1
!      distslice(l,1) = dist(left_courbe_equal(l,1),left_courbe_equal(l,2),points_courbe_equal(1,1)-(Ni-l+1)*dsi,points_courbe_equal(&
!1,2))
!      distslice(l,2) = dist(left_courbe_equal(l,1),left_courbe_equal(l,2),points_courbe_equal(1,1)-(Ni-l-1)*dsi,points_courbe_equal(&
!1,2))
!      distslice(Ns+Ni+Nf+l,1) = dist(right_courbe_equal(l,1),right_courbe_equal(l,2),points_courbe_equal(1,1)-(Ni-l+1)*dsi,points_co&
!urbe_equal(1,2))
!      distslice(Ns+Ni+Nf+l,2) = dist(right_courbe_equal(l,1),right_courbe_equal(l,2),points_courbe_equal(1,1)-(Ni-l-1)*dsi,points_co&
!urbe_equal(1,2))
!    enddo 
!    distslice(Ni,1) = dist(left_courbe_equal(Ni,1),left_courbe_equal(Ni,2),points_courbe_equal(1,1)-dsi,points_courbe_equal(1,2))
!    distslice(Ni,2) = dist(left_courbe_equal(Ni,1),left_courbe_equal(Ni,2),points_courbe_equal(1,1),points_courbe_equal(1,2))
!    distslice(Ns+Ni+Nf+Ni,1) = dist(right_courbe_equal(Ni,1),right_courbe_equal(Ni,2),points_courbe_equal(1,1)-dsi,points_courbe_equ&
!al(1,2))
!    distslice(Ns+Ni+Nf+Ni,2) = dist(right_courbe_equal(Ni,1),right_courbe_equal(Ni,2),points_courbe_equal(1,1),points_courbe_equal(1&
!,2))
!
!    !dsf = distslice(Ns+Ni,1)/(Nf-1)
!    dsf = disthead/(Nf-1)
!    distslice(1+Ni+Ns,1) = dist(left_courbe_equal(1+Ni-1+Ns-1,1),left_courbe_equal(1+Ni-1+Ns-1,2),points_courbe_equal(Ns,1),points_c&
!ourbe_equal(1,2))
!    distslice(1+Ni+Ns,2) = dist(left_courbe_equal(1+Ni-1+Ns-1,1),left_courbe_equal(1+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+dsf,poin&
!ts_courbe_equal(1,2))
!    distslice(Ns+Ni+Nf+1+Ni+Ns,1) = dist(right_courbe_equal(1+Ni-1+Ns-1,1),right_courbe_equal(1+Ni-1+Ns-1,2),points_courbe_equal(Ns,&
!1),points_courbe_equal(1,2))
!    distslice(Ns+Ni+Nf+1+Ni+Ns,2) = dist(right_courbe_equal(1+Ni-1+Ns-1,1),right_courbe_equal(1+Ni-1+Ns-1,2),points_courbe_equal(Ns,&
!1)+dsf,points_courbe_equal(1,2))
!    do l=2,Nf-1
!      distslice(l+Ni+Ns,1) = dist(left_courbe_equal(l+Ni-1+Ns-1,1),left_courbe_equal(l+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(l-2)*&
!dsf,points_courbe_equal(1,2))
!      distslice(l+Ni+Ns,2) = dist(left_courbe_equal(l+Ni-1+Ns-1,1),left_courbe_equal(l+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+l*dsf,&
!points_courbe_equal(1,2))
!      distslice(Ns+Ni+Nf+l+Ni+Ns,1) = dist(right_courbe_equal(l+Ni-1+Ns-1,1),right_courbe_equal(l+Ni-1+Ns-1,2),points_courbe_equal(N&
!s,1)+(l-2)*dsf,points_courbe_equal(1,2))
!      distslice(Ns+Ni+Nf+l+Ni+Ns,2) = dist(right_courbe_equal(l+Ni-1+Ns-1,1),right_courbe_equal(l+Ni-1+Ns-1,2),points_courbe_equal(N&
!s,1)+l*dsf,points_courbe_equal(1,2))
!    enddo 
!    !distslice(Ns+Ni+Nf,1) = dist(left_courbe_equal(Ns,1),left_courbe_equal(Ns,2),points_courbe_equal(Ns-1,1),points_courbe_equal(Ns&
!-1,2))
!    !distslice(Ns+Ni+Nf,2) = dist(left_courbe_equal(Ns,1),left_courbe_equal(Ns,2),points_courbe_equal(Ns,1),points_courbe_equal(Ns,2&
!))
!    distslice(Nf+Ni+Ns,1) = dist(left_courbe_equal(Nf+Ni-1+Ns-1,1),left_courbe_equal(Nf+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(Nf-2&
!)*dsf,points_courbe_equal(1,2))
!    distslice(Nf+Ni+Ns,2) = dist(left_courbe_equal(Nf+Ni-1+Ns-1,1),left_courbe_equal(Nf+Ni-1+Ns-1,2),points_courbe_equal(Ns,1)+(Nf-1&
!)*dsf,points_courbe_equal(1,2))
!    distslice(Ns+Ni+Nf+Nf+Ni+Ns,1) = dist(right_courbe_equal(Nf+Ni-1+Ns-1,1),right_courbe_equal(Nf+Ni-1+Ns-1,2),points_courbe_equal(&
!Ns,1)+(Nf-2)*dsf,points_courbe_equal(1,2))
!    distslice(Ns+Ni+Nf+Nf+Ni+Ns,2) = dist(right_courbe_equal(Nf+Ni-1+Ns-1,1),right_courbe_equal(Nf+Ni-1+Ns-1,2),points_courbe_equal(&
!Ns,1)+(Nf-1)*dsf,points_courbe_equal(1,2))
!!        l = 1
!!        xLeft = 0
!!        yLeft = 0
!!        xRight = 0
!!        yRight = 0
!!        do k=1,N
!!          do j=1,ny
!!            if (abs(dist(xx(j),yy(k),xx(midline(1,1)),yy(midline(1,2)))-distslice(1,1))<eepsilon) then
!!                    xl = j
!!                    yl = k
!!            endif
!!          enddo
!!        enddo
!!        do k=1,N
!!          do j=1,ny
!!            if (dist(xx(j),yy(k),xx(midline(1,1)),yy(midline(1,2)))<5) skel2(j,k) = 1
!!          enddo
!!        enddo
!!          l = sum(skel)
!!          xLeft = 0
!!          yLeft = 0
!!          xRight = 0
!!          yRight = 0
!!          do k=1,N
!!            do j=1,ny
!!            if (abs(dist(xx(j),yy(k),xx(midline(sum(skel),1)),yy(midline(sum(skel),2)))-distslice(1,2))<eepsilon) then
!!                    xr = j
!!                    yr = k
!!            endif
!!            enddo
!!          enddo
!!        do k=1,N
!!          do j=1,ny
!!            if (dist(xx(j),yy(k),xx(midline(sum(skel),1)),yy(midline(sum(skel),2)))<5) skel2(j,k) = 1
!!          enddo
!!        enddo
!!    do l=1,sum(skel)
!!      if ((xRight_tab(l) == 0).or.(yRight_tab(l) == 0).or.(yLeft_tab(l) == 0).or.(xLeft_tab(l) == 0)) then
!!        write(*,*) "INTROUVABLE : ",l
!!      else
!!        tmpbool(floor(xLeft_tab(l)),floor(yLeft_tab(l))) = 1
!!        tmpbool(ceiling(xRight_tab(l)),ceiling(yRight_tab(l))) = 1
!!      endif
!!    enddo
!!  
!!    tmpbool(midline(1,1),midline(1,2)) = 0
!!    skel2(midline(1,1),midline(1,2)) = 0
!!    tmpbool(midline(sum(skel),1),midline(sum(skel),2)) = 0
!!    skel2(midline(sum(skel),1),midline(sum(skel),2)) = 0

        

!!    open(unit=84,file='results/right00.txt',status='unknown')
!!    open(unit=83,file='results/left00.txt',status='unknown')
    open(unit=79,file='results/skelett00.vtk',status='unknown')
!!    open(unit=81,file='results/skelett00.txt',status='unknown')
!!    open(unit=82,file='results/skeletteq00.txt',status='unknown')
      write(79,'(1A26)') '# vtk DataFile Version 2.0'
      write(79,'(a)') 'rho'
      write(79,'(a)') 'ASCII'
      write(79,'(a)') 'DATASET STRUCTURED_POINTS'
      write(79,'(a,I4,I4,I4)') 'DIMENSIONS', N,ny,1
      write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
      write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,1. 
      write(79,'(a,I6)') 'POINT_DATA          ' , N*ny 
      write(79,'(a)') 'SCALARS values double'
      write(79,'(a)') 'LOOKUP_TABLE default'
    
    do k=1,ny
      do j=1,N
      !        write(79,*) rhoSlices2(j,k)
        !write(79,*) (1-tmpbool(j,k))*(rhoSlices2(j,k)*(1-skel3(j,k)) + minval(rhoSlices2)*skel3(j,k)) + minval(rhoSlices2)*tmpbool(j,k)
        !write(79,*) (1-tmpbool(j,k))*(rhoSlices2(j,k)*(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k)) + minval(rhoSlices2)*tmpbool(j,k)
        write(79,*) rhoSlices2(j,k)*(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k)
        !write(79,*) rhoSlices2(j,k)*(1-skel3(j,k)) + minval(rhoSlices2)*skel3(j,k)
        !write(79,*) rhoSlices2(j,k)*(1-tmpbool(j,k)) + minval(rhoSlices2)*tmpbool(j,k)
      !  write(79,*) rhoSlices2(j,k)*(1-skel2(j,k)) + minval(rhoSlices2)*skel2(j,k)
        !write(79,*) skel(j,k)
      enddo
    enddo
!!    do l=1,Ns
!!      write(81,*) points_courbe(l,1)," ",points_courbe(l,2)
!!      write(82,*) points_courbe_equal(l,1)," ",points_courbe_equal(l,2)
!!    enddo
!!    do l=1,size(left_courbe_equal,1)
!!      write(83,*) left_courbe_equal(l,1)," ",left_courbe_equal(l,2)
!!    enddo
!!    do l=1,size(right_courbe_equal,1)
!!      write(84,*) right_courbe_equal(l,1)," ",right_courbe_equal(l,2)
!!    enddo
!!    !do l=1,Ni-1+Ns-1+Nf !1,size(left_courbe,1)
!!    !  write(83,*) left_courbe(l,1)," ",left_courbe(l,2)
!!    !enddo
!!    !do l=1,Ni-1+Ns-1+Nf !1,size(right_courbe,1)
!!    !  write(84,*) right_courbe(l,1)," ",right_courbe(l,2)
!!    !enddo
!!    !do l=1,size(left_control,1)
!!    !  write(83,*) left_control(l,1)," ",left_control(l,2)
!!    !enddo
!!    !do l=1,size(right_control,1)
!!    !  write(84,*) right_control(l,1)," ",right_control(l,2)
!!    !enddo
!!    write(*,*) " long ==   ",sum(skel)," ",long
    close(79)    
!!    close(81)
!!    close(82)
!!    close(83)
!!    close(84)
    deallocate(midline,xLeft_tab,yLeft_tab,xRight_tab,yRight_tab)
    deallocate(points_control)
    deallocate(left_control,leftcontrolm,leftcontrolf,leftcontroli)
    deallocate(right_control,rightcontrolm,rightcontrolf,rightcontroli)
    !allocate(left_courbe(Ns,2),left_courbe_equal(Ns,2))
    !allocate(right_courbe(Ns,2),right_courbe_equal(Ns,2))
    !allocate(distslice(2*Ns,2))
    
  !open(unit=80,file='skeleton.txt',status='unknown')
  !  write(80,*) kt,"    ",sum(skel)," ",long3," ",long2," ",long
  
  t = 0
  iter = 0
  idisplay = 10
  iUpdateDist = 5
  do kt = 1,566,idisplay !300 250,idisplay !121,140 !615,idisplay !615,idisplay
    write(*,*)"time iteration : ",kt
    !open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/mask200/Image_"//str(kt+picNum-1)//".txt"&
!    open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/mask200_slow/Image_"//str(kt+picNum-1)//".txt"&
!,status='unknown')
    open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/secondTEST6/IMAGES2/Image_"//str(kt+picNum-1)//".dat"&
,status='unknown')
    do k=1,nx
      do j=1,ny
          read(78,*) rhoSlices2(k,j)
      enddo
    enddo
    close(78)
    rhoSlices2 = rhoSlices2 - 0.5_pr*(maxval(rhoSlices2) + minval(rhoSlices2))
    call updateDistance(rhoSlices2,gradPhi)
    dx = 1._pr
    dy = 1._pr
  
    dir1 = 0
    dir2 = 0
    dir3 = 0
    dir4 = 0
    Nseed = 0
    skel = 0
    do i=2,N-1
      do j=2,ny-1
!        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j))) dir1(i,j) = 1
!        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i,j-1))) dir2(i,j) = 1
!        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j+1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j-1))) dir3(i,j) = 1
!        if ((rhoSlices2(i,j)+eepsilon>rhoSlices2(i+1,j-1)).and.(rhoSlices2(i,j)+eepsilon>rhoSlices2(i-1,j+1))) dir4(i,j) = 1
!!        if ((rhoSlices2(i,j)>rhoSlices2(i+1,j)).and.(rhoSlices2(i,j)>rhoSlices2(i-1,j))) dir1(i,j) = 1
!!        if ((rhoSlices2(i,j)>rhoSlices2(i,j+1)).and.(rhoSlices2(i,j)>rhoSlices2(i,j-1))) dir2(i,j) = 1
!!        if ((rhoSlices2(i,j)>rhoSlices2(i+1,j+1)).and.(rhoSlices2(i,j)>rhoSlices2(i-1,j-1))) dir3(i,j) = 1
!!        if ((rhoSlices2(i,j)>rhoSlices2(i+1,j-1)).and.(rhoSlices2(i,j)>rhoSlices2(i-1,j+1))) dir4(i,j) = 1
!        Nseed(i,j) = dir1(i,j) + dir2(i,j) + dir3(i,j) + dir4(i,j)
!        if ((Nseed(i,j)>=2).and.(rhoSlices2(i,j)>0)) then 
!          skel(i,j) = 1
!        !        write(*,*) " skel y : ",j
!        endif
         !if ((gradPhi(j,i)<0.57).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
         !if ((gradPhi(j,i)<0.55).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
         !if ((gradPhi(j,i)<0.7).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
         if ((gradPhi(j,i)<0.66).and.(rhoSlices2(j,i)>0._pr)) skel(j,i) = 1
      enddo
    enddo
    l = 1
    pix1 = 0
    pix2 = 0
    do while ((pix1==1).or.(pix2==1).or.(l==1))
      pix1 = 0
      pix2 = 0
      l = l + 1
      skel2 = skel
      do i=2,N-1
        do j=2,ny-1
          maxv = 0
          if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
          if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
          if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
          if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
          if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
          if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
          if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
          if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
          Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
skel(i+1,j-1)
          if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i+1,j)==0).or.(skel(i-1,j)==0)&
.or.(skel(i,j-1)==0)).and.((skel(i,j-1)==0).or.(skel(i,j+1)==0).or.(skel(i,j+1)==0))) then
                  skel2(i,j) = 0
                  pix1 = 1
          endif
        enddo
      enddo
      skel = skel2
      skel2 = skel
      do i=2,N-1
        do j=2,ny-1
          maxv = 0
          if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
          if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
          if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
          if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
          if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
          if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
          if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
          if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
          Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
skel(i+1,j-1)
          if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i,j+1)==0).or.(skel(i-1,j)==0)&
.or.(skel(i,j-1)==0)).and.((skel(i+1,j)==0).or.(skel(i,j+1)==0).or.(skel(i-1,j)==0))) then
                  skel2(i,j) = 0
                  pix2 = 1
          endif
        enddo
      enddo
      skel = skel2
    enddo
    skel2 = skel
    do i=2,N-1
      do j=2,ny-1
        Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) +&
skel(i+1,j-1)
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i-1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i,j-1)==1).and.(skel(i,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j)==1).and.(skel(i+1,j)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j-1)==1).and.(skel(i+1,j+1)==1)) skel2(i,j) = 1
        if ((skel(i,j)==0).and.(Nseed(i,j)==2).and.(skel(i-1,j+1)==1).and.(skel(i+1,j-1)==1)) skel2(i,j) = 1
      enddo
    enddo
    skel = skel2
    skel2 = skel
    do i=2,N-1
      do j=2,ny-1
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +& 
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j)==1).and.(skel2(i-1,j+1)==1)) skel2(i-1,j) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j+1)==1).and.(skel2(i,j+1)==1)) skel2(i,j+1) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j+1)==1).and.(skel2(i+1,j+1)==1)) skel2(i,j+1) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j+1)==1).and.(skel2(i+1,j)==1)) skel2(i+1,j) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j)==1).and.(skel2(i+1,j-1)==1)) skel2(i+1,j) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i+1,j-1)==1).and.(skel2(i,j-1)==1)) skel2(i,j-1) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i,j-1)==1).and.(skel2(i-1,j-1)==1)) skel2(i,j-1) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)>2).and.(skel2(i-1,j-1)==1).and.(skel2(i-1,j)==1)) skel2(i-1,j) = 0
        Nseed(i,j) = skel2(i+1,j) + skel2(i+1,j+1) + skel2(i,j+1) + skel2(i-1,j+1) + skel2(i-1,j) + skel2(i-1,j-1) + skel2(i,j-1) +&
skel2(i+1,j-1)
        if ((skel2(i,j)==1).and.(Nseed(i,j)==0)) skel2(i,j) = 0
        !if (skel2(i,j)==1) write(*,*) rhoSlices2(i,j)
      enddo
    enddo
    skel = skel2
        
    tmpbool = 0
  
    sizeSkel = sum(skel)
  
!    !allocate(midline(sizeSkel,2),distslice(2*sizeSkel,2),xLeft_tab(sizeSkel), yLeft_tab(sizeSkel), xRight_tab(sizeSkel), yRight_tab(sizeSkel))
!    allocate(midline(sizeSkel,2),xLeft_tab(sizeSkel), yLeft_tab(sizeSkel), xRight_tab(sizeSkel), yRight_tab(sizeSkel))
!    !write(*,*) "CHECKED"   
!        l=1
!        do i=2,N-1
!          do j=2,ny-1
!    !        if (skel(j,k)==1) then
!    !                midline(l,1) = j
!    !                midline(l,2) = k
!    !                if ((j==nx/2).and.(kt==1)) l0 = l
!    !                l = l+1
!              if ((skel(i,j)==1).and.(l==1)) then
!                Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1)+&
!skel(i+1,j-1)
!    !                    write(*,*) "midline ",sum(skel)," ",l
!                if ((Nseed(i,j)==1).and.((abs(xskelL-i)<100).and.(abs(yskelL-j)<100))) then 
!                        midline(l,1) = i
!                        midline(l,2) = j
!                        l = l+1
!                endif
!            endif
!          enddo
!        enddo
!        xskelL = midline(1,1)
!        yskelL = midline(1,2)
!        do l=2,sum(skel)
!        do i=2,N-1
!          do j=2,ny-1
!    !        if (skel(j,k)==1) then
!    !                midline(l,1) = j
!    !                midline(l,2) = k
!    !                if ((j==nx/2).and.(kt==1)) l0 = l
!    !                l = l+1
!              if (skel(i,j)==1) then
!                Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1)&
!+skel(i+1,j-1)
!          !      write(*,*) "midline ",sum(skel)," ",l," ",Nseed(i,j)," ",midline(l-1,1)
!                if ((Nseed(i,j)>1).and.((midline(l-1,1)==i).or.(midline(l-1,1)==i-1).or.(midline(l-1,1)==i+1)).and.((midline(l-1,2)&
!==j).or.(midline(l-1,2)==j-1).or.(midline(l-1,2)==j+1)).and.(appartient(midline,i,j,l).neqv..true.)) then 
!                        midline(l,1) = i
!                        midline(l,2) = j
!          !              write(*,*) "midline ll : ",midline(l,1)," ",midline(l,2),"  LL  ",l," SUM ",sum(skel)," ",size(midline,1)," ",size(midline,2)," ",i," ",j
!                endif
!                if ((Nseed(i,j)==1).and.(l==sum(skel))) then
!                        midline(l,1) = i
!                        midline(l,2) = j
!          !              write(*,*) "midline ll : ",midline(l,1)," ",midline(l,2),"  LL  ",l," SUM ",sum(skel)," ",size(midline,1)," ",size(midline,2)," ",i," ",j
!                endif
!            endif
!          enddo
!        enddo
!    !  write(*,*) "MIDLINE 11 ",midline(1,1)," ",midline(1,2)," ",yskelL," ll ",l," ",size(midline,1)
!        enddo
!    !skel3 = 0
!    !do l=1,sum(skel)
!    !  if (skel(midline(l,1),midline(l,2))==1) skel3(midline(l,1),midline(l,2)) = 1
!    !enddo
!    !skel = skel3
!        allocate(points_control(sum(skel),2))
!        do l=1,sum(skel)
!          points_control(l,1) = xx(midline(l,1))
!          points_control(l,2) = yy(midline(l,2))
!        enddo
!!!        open(unit=81,file='results/control'//str(kt+140-1)//'.txt',status='unknown')
!!!        do l=1,sum(skel)
!!!          !write(81,*) points_control(l,1)," ",points_control(l,2)
!!!        enddo
!!!        close(81)
!        points_courbe(1,1) = points_control(1,1)
!        points_courbe(1,2) = points_control(1,2)
!        points_courbe(Ns,1) = points_control(size(points_control,1),1)
!        points_courbe(Ns,2) = points_control(size(points_control,1),2)
!        tb = dtb
!        l = 1
!        long = 0._pr
!        do while ((tb<1._pr).and.(l+1<Ns))
!          l = l+1
!          call pointsBezierN(points_control,tb,px,py)
!          points_courbe(l,1) = px
!          points_courbe(l,2) = py
!          long = long + sqrt((points_courbe(l,1)-points_courbe(l-1,1))*(points_courbe(l,1)-points_courbe(l-1,1)) +&
!(points_courbe(l,2)-points_courbe(l-1,2))*(points_courbe(l,2)-points_courbe(l-1,2)))
!          tb = tb+dtb
!        enddo
!        long = long + sqrt((points_courbe(Ns,1)-points_courbe(Ns-1,1))*(points_courbe(Ns,1)-points_courbe(Ns-1,1)) +&
!(points_courbe(Ns,2)-points_courbe(Ns-1,2))*(points_courbe(Ns,2)-points_courbe(Ns-1,2))) 
!        long2 = long
!        ds = long/(Ns-1)
!        points_courbe_equal(1,1) = points_control(1,1)
!        points_courbe_equal(1,2) = points_control(1,2)
!        points_courbe_equal(Ns,1) = points_control(size(points_control,1),1)
!        points_courbe_equal(Ns,2) = points_control(size(points_control,1),2)
!        l = 1
!        long = 0._pr
!        tb = 0._pr
!        do while ((tb<1._pr).and.(l<Ns))
!          l = l+1
!          nt = 1
!          s = 0._pr
!          do while ((l-1)*ds-s>0._pr) 
!            nt = nt+1
!            s = s + sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))*(points_courbe(nt,1)-points_courbe(nt-1,1)) +&
!(points_courbe(nt,2)-points_courbe(nt-1,2))*(points_courbe(nt,2)-points_courbe(nt-1,2)))
!          enddo
!          tbm = (nt-2)*dtb
!          tbp = (nt-1)*dtb
!          s0 = s
!          sinit = s - sqrt((points_courbe(nt,1)-points_courbe(nt-1,1))*(points_courbe(nt,1)-points_courbe(nt-1,1))+&
!(points_courbe(nt,2)-points_courbe(nt-1,2))*(points_courbe(nt,2)-points_courbe(nt-1,2)))
!          s = sinit
!          do while (abs((l-1)*ds-s)>eepsilon)
!            tb = (tbm + tbp)*0.5_pr
!            call pointsBezierN(points_control,tb,px,py)
!            s = sinit + sqrt((px-points_courbe(nt-1,1))*(px-points_courbe(nt-1,1))+(py-points_courbe(nt-1,2))*&
!(py-points_courbe(nt-1,2)))
!            if ((l-1)*ds-s>0._pr) then
!              tbm = tb
!            else
!              tbp = tb
!            endif
!          enddo
!          call pointsBezierN(points_control,tb,px,py)
!          if (l<Ns) then
!          points_courbe_equal(l,1) = px
!          points_courbe_equal(l,2) = py
!
!          long = long + sqrt((points_courbe_equal(l,1)-points_courbe_equal(l-1,1))*(points_courbe_equal(l,1)-&
!points_courbe_equal(l-1,1)) + (points_courbe_equal(l,2)-points_courbe_equal(l-1,2))*(points_courbe_equal(l,2)-&
!points_courbe_equal(l-1,2)))
!          endif
!        enddo
!        points_courbe_equal(Ns,1) = points_control(size(points_control,1),1)
!        points_courbe_equal(Ns,2) = points_control(size(points_control,1),2)
!        long = long + sqrt((points_courbe_equal(Ns,1)-points_courbe_equal(Ns-1,1))*(points_courbe_equal(Ns,1)-&
!points_courbe_equal(Ns-1,1)) + (points_courbe_equal(Ns,2)-points_courbe_equal(Ns-1,2))*(points_courbe_equal(Ns,2)-&
!points_courbe_equal(Ns-1,2))) 
!        dti = dsi/ds !distslice(1+Ni,1)/(Ni-1)/dsi
!        !write(*,*) "DTI : ",distslice(1+Ni,1)," ",points_courbe_equal(1,1)," ",points_courbe_equal(1,2)
!        do l=2,Ni
!          tb = - (l-1)*dti
!          write(*,*) "points tail Ni : ",l," ",tb," ",dti," ",dti*(Ni-1)
!          call courbeBezierN(points_courbe_equal,tb,Px,Py)
!          write(*,*) "points tail Ni : ",l," ",tb," ",dti," ",dti*(Ni-1)
!          tail_courbe(l,1) = Px
!          tail_courbe(l,2) = Py
!          write(*,*) "points tail Ni : ",l," ",Px," ",Py
!        enddo
!        tail_courbe(1,1) = points_courbe_equal(1,1) !points_control(1,1)
!        tail_courbe(1,2) = points_courbe_equal(1,2) !points_control(1,2)
!        dtf = dsf/ds!/(100)!*ds)!*100 !distslice(Ns+Ni,2)/(Nf-1)/dsf !(10*ds)
!        !write(*,*) "DTF : ",distslice(Ns+Ni,2)," ",points_courbe_equal(Ns,1)," ",points_courbe_equal(Ns,2)
!        do l=2,Nf
!          tb = 1._pr + (l-1)*dtf
!          call courbeBezierN(points_courbe_equal,tb,Px,Py)
!          head_courbe(l,1) = Px
!          head_courbe(l,2) = Py
!        enddo
!        head_courbe(1,1) = points_courbe_equal(Ns,1) !points_control(Ns,1)
!        head_courbe(1,2) = points_courbe_equal(Ns,2) !points_control(Ns,2)
!        long3 = 0._pr
!        do l=1,sum(skel)-1
!          !if (midline(l,1)==midline(l+1,1)) then
!          !  long = long + dx
!          !else if (midline(l,2)==midline(l+1,2)) then
!          !  long = long + dy
!          !else
!          !  long = long + sqrt(dx*dx + dy*dy)
!          !endif
!          long3 = long3 +sqrt((xx(midline(l+1,1))-xx(midline(l,1)))*(xx(midline(l+1,1))-xx(midline(l,1))) + (yy(midline(l+1,2))-&
!yy(midline(l,2)))*(yy(midline(l+1,2))-yy(midline(l,2))))
!        enddo
!        !write(*,*) "CHECKED L ",l," ",sum(skel)   
!!    lp=l0!+4
!!!    !           if (kt==1) then
!!!    l=1
!!!    do j=2,ny-1
!!!      if ((rhoSlices2(midline(l,1),j)<0._pr).and.(rhoSlices2(midline(l,1),j+1)>0._pr)) yRight =  xx(j) - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j+1
!!!      if ((rhoSlices2(midline(l,1),j)>0._pr).and.(rhoSlices2(midline(l,1),j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j
!!!    enddo
!!!    xLeft_tab(l) = midline(l,1)
!!!    xRight_tab(l) = midline(l,1)
!!!    yLeft_tab(l) = yLeft
!!!    yRight_tab(l) = yRight
!!!    !write(*,*) "CHECKED"   
!!!    !distslice(l,1) = dist(xx(xLeft_tab(l)),yy(yLeft_tab(l)),xx(midline(l,1)),yy(midline(l,2)))
!!!    distslice(l,1) = dist(xLeft_tab(l),yLeft_tab(l),xx(midline(l,1)),yy(midline(l,2)))
!!!    !write(*,*) "CHECKED"   
!!!    l=sum(skel)
!!!    do j=2,ny-1
!!!      if ((rhoSlices2(midline(l,1),j)<0._pr).and.(rhoSlices2(midline(l,1),j+1)>0._pr)) yRight =  xx(j) - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j+1
!!!      if ((rhoSlices2(midline(l,1),j)>0._pr).and.(rhoSlices2(midline(l,1),j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j
!!!    enddo
!!!    !write(*,*) "CHECKED"   
!!!    xLeft_tab(l) = midline(l,1)
!!!    xRight_tab(l) = midline(l,1)
!!!    yLeft_tab(l) = yLeft
!!!    yRight_tab(l) = yRight
!!!    distslice(1,2) = dist(xLeft_tab(l),yLeft_tab(l),xx(midline(l,1)),yy(midline(l,2)))
!!!    !write(*,*) "CHECKED mid : ",midline(1,1)," ",midline(1,2)," ",midline(sum(skel),1)," ",midline(sum(skel),2)   
!!!    do l=2,sum(skel)-1
!!!      do j=2,ny-1
!!!        if ((rhoSlices2(midline(l,1),j)<0._pr).and.(rhoSlices2(midline(l,1),j+1)>0._pr)) yRight =  xx(j) - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j+1
!!!        if ((rhoSlices2(midline(l,1),j)>0._pr).and.(rhoSlices2(midline(l,1),j+1)<0._pr)) yLeft =  xx(j) - rhoSlices2(midline(l,1),j)*dy/(rhoSlices2(midline(l,1),j+1) - rhoSlices2(midline(l,1),j)) !j
!!!      enddo
!!!      !write(*,*) "CHECKED LL ",l   
!!!      !do l=l0,l0+5!sum(skel)-1
!!!      !do l=lp,lp!sum(skel)-1
!!!      !l=l0
!!!      xLeft_tab(l) = midline(l,1)
!!!      xRight_tab(l) = midline(l,1)
!!!      yLeft_tab(l) = yLeft
!!!      yRight_tab(l) = yRight
!!!      !distslice(l,1) = dist(xx(xLeft_tab(l)),yy(yLeft_tab(l)),xx(midline(l-1,1)),yy(midline(l-1,2)))
!!!      !distslice(l,2) = dist(xx(xLeft_tab(l)),yy(yLeft_tab(l)),xx(midline(l+1,1)),yy(midline(l+1,2)))
!!!      !distslice(sizeSkel+l,1) = dist(xx(xRight_tab(l)),yy(yRight_tab(l)),xx(midline(l-1,1)),yy(midline(l-1,2)))
!!!      !distslice(sizeSkel+l,2) = dist(xx(xRight_tab(l)),yy(yRight_tab(l)),xx(midline(l+1,1)),yy(midline(l+1,2)))
!!!      distslice(l,1) = dist(xLeft_tab(l),yLeft_tab(l),xx(midline(l-1,1)),yy(midline(l-1,2)))
!!!      distslice(l,2) = dist(xLeft_tab(l),yLeft_tab(l),xx(midline(l+1,1)),yy(midline(l+1,2)))
!!!      distslice(sizeSkel+l,1) = dist(xRight_tab(l),yRight_tab(l),xx(midline(l-1,1)),yy(midline(l-1,2)))
!!!      distslice(sizeSkel+l,2) = dist(xRight_tab(l),yRight_tab(l),xx(midline(l+1,1)),yy(midline(l+1,2)))
!!!    enddo
!!    !write(*,*) "CHECKED"   
!!    !  write(*,*) "MIDLINE 11 ",midline(1,1)," ",midline(1,2)," ",yskelL
!!    
!!        l = 1
!!        xLeft = 0
!!        yLeft = 0
!!        xRight = 0
!!        yRight = 0
!!        do k=1,N
!!          do j=1,ny
!!    !        if ((abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))<minimL).and.(det(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy,midline(l,1)*dx,midline(l,2)*dy) <= 0).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))<1._pr)) then
!!    !          xLeft = j
!!    !          yLeft = k
!!    !          minimL = abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(l,2))
!!    !        endif
!!    !        if ((abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))<minimR).and.(det(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy,midline(l,1)*dx,midline(l,2)*dy) >= 0).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))<1._pr)) then
!!    !          xRight = j
!!    !          yRight = k
!!    !          minimR = abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l+1,1)*dx,midline(l+1,2)*dy)-distslice(nl+l,2))
!!    !        endif
!!            !if (dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(1,1)*dx,midline(1,2)*dy)<distslice(1,1)) tmpbool(j,k) = 1
!!            if (abs(dist(xx(j),yy(k),xx(midline(1,1)),yy(midline(1,2)))-distslice(1,1))<eepsilon) then
!!                    xl = j
!!                    yl = k
!!            endif
!!          enddo
!!        enddo
!!        !write(*,*) "CHECKED"   
!!        do k=1,N
!!          do j=1,ny
!!            !if (dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(1,1)*dx,midline(1,2)*dy)<dist(1+(float(xl)-1)*dx,1+(float(yl)-1)*dy,midline(1,1)*dx,midline(1,2)*dy)) tmpbool(j,k) = 1
!!            !if (dist(xx(j),yy(k),xx(midline(1,1)),yy(midline(1,2)))<dist(xx(xl),yy(yl),xx(midline(1,1)),yy(midline(1,2)))) skel2(j,k) = 1
!!            if (dist(xx(j),yy(k),xx(midline(1,1)),yy(midline(1,2)))<5) skel2(j,k) = 1
!!          enddo
!!        enddo
!!    !      xLeft_tab(l) = xLeft
!!    !      yLeft_tab(l) = yLeft
!!    !      xRight_tab(l) = xRight
!!    !      yRight_tab(l) = yRight
!!          l = sum(skel)
!!          xLeft = 0
!!          yLeft = 0
!!          xRight = 0
!!          yRight = 0
!!          do k=1,N
!!            do j=1,ny
!!    !          if ((abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))<minimL).and.(det(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l,1)*dx,midline(l,2)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy) <= 0).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))<1._pr)) then
!!    !            xLeft = j
!!    !            yLeft = k
!!    !            minimL = abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))
!!    !          endif
!!    !          if ((abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))<minimR).and.(det(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l,1)*dx,midline(l,2)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy) >= 0).and.(abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))<1._pr)) then
!!    !            xRight = j
!!    !            yRight = k
!!    !            minimR = abs(dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(nl+l,1))
!!    !          endif
!!            !if (dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(sum(skel),1)*dx,midline(sum(skel),2)*dy)<distslice(1,2)) tmpbool(j,k) = 1
!!            if (abs(dist(xx(j),yy(k),xx(midline(sum(skel),1)),yy(midline(sum(skel),2)))-distslice(1,2))<eepsilon) then
!!                    xr = j
!!                    yr = k
!!            endif
!!            enddo
!!          enddo
!!        do k=1,N
!!          do j=1,ny
!!            !if (dist(1+(float(j)-1)*dx,1+(float(k)-1)*dy,midline(sum(skel),1)*dx,midline(sum(skel),2)*dy)<dist(1+(float(xr)-1)*dx,1+(float(yr)-1)*dy,midline(sum(skel),1)*dx,midline(sum(skel),2)*dy)) tmpbool(j,k) = 1
!!            !if (dist(xx(j),yy(k),xx(midline(sum(skel),1)),yy(midline(sum(skel),2)))<dist(xx(xl),yy(yl),xx(midline(sum(skel),1)),yy(midline(sum(skel),2)))) skel2(j,k) = 1
!!!            if (dist(xx(j),yy(k),xx(midline(sum(skel),1)),yy(midline(sum(skel),2)))<5) skel2(j,k) = 1
!!          enddo
!!        enddo
!!    !        xLeft_tab(l) = xLeft
!!    !        yLeft_tab(l) = yLeft
!!    !        xRight_tab(l) = xRight
!!    !        yRight_tab(l) = yRight
!!      write(*,*) "CHECKED"   
!      !do l=2,sum(skel)-1
!!      do l=2,Ns-1
!!        !do l=l0,l0+5!sum(skel)-1
!!        !do l=lp,lp!sum(skel)-1
!!        !l = l0
!!        xLeft = 0
!!        yLeft = 0
!!        xRight = 0
!!        yRight = 0
!!        minimL = 1e6
!!        minimL_m = 1e6
!!        minimL_p = 1e6
!!        minimR = 1e6
!!        do k=1,N
!!          do j=1,ny
!!            !if (((abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(l,1))<minimL).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(l,2))<minimL)).and.(det(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)),xx(midline(l-1,1)),yy(midline(l-1,2))) <= 0).and.(abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(l,1))<1._pr).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(l,2))<1._pr)) then
!!            !if (((abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(l,1))<minimL).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(l,2))<minimL)).and.(det(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)),xx(midline(l-1,1)),yy(midline(l-1,2))) <= 0)) then
!!            if (((abs(dist(xx(j),yy(k),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2))-distslice(l,1))<minimL).and.(abs(dist(xx(j),yy(k),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2))-distslice(l,2))<minimL)).and.(det(xx(j),yy(k),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)) <= 0).and.(abs(dist(xx(j),yy(k),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2))-distslice(l,1))<1._pr).and.(abs(dist(xx(j),yy(k),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2))-distslice(l,2))<1._pr)) then
!!            !if (((abs(dist(xx(j),yy(k),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2))-distslice(l,1))<minimL).and.(abs(dist(xx(j),yy(k),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2))-distslice(l,2))<minimL)).and.(det(xx(j),yy(k),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)) <= 0)) then
!!              xLeft = j
!!              yLeft = k
!!              !write(*,*) "LEFT DIST ==          ",dist(xx(j),xx(k),midline(l-1,1)*dx,midline(l-1,2)*dy)," ",dist(xx(j),xx(k),midline(l+1,1)*dx,midline(l+1,2)*dy)," LL ",l
!!              !if (l==1) write(*,*) "DISTT          ",abs(dist(xx(j),xx(k),midline(l-1,1)*dx,midline(l-1,2)*dy)-distslice(l,1))
!!              !if (l==1) write(*,*) "WHHYYYYY  ",k
!!              !minimL = max(abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(l,1)),abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(l,2)))
!!              minimL = max(abs(dist(xx(j),yy(k),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2))-distslice(l,1)),abs(dist(xx(j),yy(k),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2))-distslice(l,2)))
!!            endif
!!            !if (((abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(nl+l,1))<minimR).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(nl+l,2))<minimR)).and.(det(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)),xx(midline(l-1,1)),yy(midline(l-1,2))) >= 0).and.(abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(nl+l,1))<1._pr).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(nl+l,2))<1._pr)) then
!!            !if (((abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(nl+l,1))<minimR).and.(abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(nl+l,2))<minimR)).and.(det(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)),xx(midline(l-1,1)),yy(midline(l-1,2))) >= 0)) then
!!            if (((abs(dist(xx(j),yy(k),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2))-distslice(Ns+l,1))<minimR).and.(abs(dist(xx(j),yy(k),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2))-distslice(Ns+l,2))<minimR)).and.(det(xx(j),yy(k),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)) >= 0).and.(abs(dist(xx(j),yy(k),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2))-distslice(Ns+l,1))<1._pr).and.(abs(dist(xx(j),yy(k),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2))-distslice(Ns+l,2))<1._pr)) then
!!            !if (((abs(dist(xx(j),yy(k),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2))-distslice(Ns+l,1))<minimR).and.(abs(dist(xx(j),yy(k),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2))-distslice(Ns+l,2))<minimR)).and.(det(xx(j),yy(k),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2)) >= 0)) then
!!              xRight = j
!!              yRight = k
!!              !write(*,*) "RIGHT DIST ==          ",dist(xx(j),xx(k),midline(l-1,1)*dx,midline(l-1,2)*dy)," ",dist(xx(j),xx(k),midline(l+1,1)*dx,midline(l+1,2)*dy)," LL ",l
!!              !minimR = max(abs(dist(xx(j),yy(k),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(nl+l,1)),abs(dist(xx(j),yy(k),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(nl+l,2)))
!!              minimR = max(abs(dist(xx(j),yy(k),points_courbe_equal(l-1,1),points_courbe_equal(l-1,2))-distslice(Ns+l,1)),abs(dist(xx(j),yy(k),points_courbe_equal(l+1,1),points_courbe_equal(l+1,2))-distslice(Ns+l,2)))
!!            endif
!!          enddo
!!        enddo
!!        !write(*,*)"TEST LEFT        ",abs(dist(xx(nint(xLeft)),yy(nint(yLeft)),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(l,1)),"        ",abs(dist(xx(nint(xLeft)),yy(nint(yLeft)),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(l,2)),"  COORD  ",xLeft," ",yLeft," -- ",nint(xLeft)," ",nint(yLeft) 
!!        !write(*,*)"TEST RIGHT        ",abs(dist(xx(nint(xRight)),yy(nint(yRight)),xx(midline(l-1,1)),yy(midline(l-1,2)))-distslice(nl+l,1)),"        ",abs(dist(xx(nint(xRight)),yy(nint(yRight)),xx(midline(l+1,1)),yy(midline(l+1,2)))-distslice(nl+l,2)),"  COORD  ",xRight," ",yRight," -- ",nint(xRight)," ",nint(yRight)
!!        !xLeft_tab(l) = xLeft
!!        !yLeft_tab(l) = yLeft
!!        !xRight_tab(l) = xRight
!!        !yRight_tab(l) = yRight
!!        left_courbe_equal(l,1) = xLeft
!!        left_courbe_equal(l,2) = yLeft
!!        right_courbe_equal(l,1) = xRight
!!        right_courbe_equal(l,2) = yRight
!!        !write(*,*) "CHECKED l ",l," ",sizeSkel   
!!      enddo
!!      call intersection(points_courbe_equal(1,1),points_courbe_equal(2,1),points_courbe_equal(1,2),points_courbe_equal(2,2),points_courbe_equal(1,1),points_courbe_equal(1,2),distslice(1+Ni,1),distslice(1+Ni,2),distslice(Ns+Ni+Nf+1+Ni,1),distslice(Ns+Ni+Nf+1+Ni,2),xLeft,yLeft,xRight,yRight,errorl,errorr,deltal,deltar)
!!!        if ((errorl==1).or.(errorr==1)) write(*,*) "error : ",1," dist        ",distslice(1+Ni,1)," ",distslice(1+Ni,2)," ",distslice(Ns+Nf+Ni+1+Ni,1)," ",distslice(Ns+Nf+Ni+1+Ni,2),"  YY  ",points_courbe_equal(1+1,1)," ",points_courbe_equal(l+1,2)," DELTA ",deltal," ",deltar
!!      left_courbe_equal(1+Ni-1,1) = xLeft
!!      left_courbe_equal(1+Ni-1,2) = yLeft
!!      right_courbe_equal(1+Ni-1,1) = xRight
!!      right_courbe_equal(1+Ni-1,2) = yRight
!      left_courbe_equal = 0._pr
!      right_courbe_equal = 0._pr
!      !write(*,*) "total : ",lpli," ",lpri
!      lpli = 1
!      lpri = 1
!      call intersection(tail_courbe(Ni,1),tail_courbe(Ni-1,1),tail_courbe(Ni,2),tail_courbe(Ni-1,2),tail_courbe(Ni,1)&
!,tail_courbe(Ni,2),distslice(1,1),distslice(1,2),distslice(Ns+Ni+Nf+1,1),distslice(Ns+Ni+Nf+1,2),xLeft,yLeft,xRight,yRight,errorl&
!,errorr,deltal,deltar) 
!      !write(*,*) "ok ? ",tail_courbe(Ni,1)," ",tail_courbe(Ni-1,1)," ",tail_courbe(Ni,2)," ",tail_courbe(Ni-1,2)," ",distslice(1,1)," ",distslice(1,2)," ",distslice(Ns+Ni+Nf+1,1)," ",distslice(Ns+Ni+Nf+1,2)," ",errorl," ",errorr," ",deltal," ",deltar," ",points_courbe_equal(1,1)-(Ni-1)*dsi," ",points_courbe_equal(1,1)-(Ni-2)*dsi," ",xLeft," ",yLeft
!!      call intersection(points_courbe_equal(1,1)-(Ni-1)*dsi,points_courbe_equal(1,1)-(Ni-2)*dsi,tail_courbe(Ni,2),tail_courbe(Ni-1,2),tail_courbe(Ni,1),tail_courbe(Ni,2),distslice(1,1),distslice(1,2),distslice(Ns+Ni+Nf+1,1),distslice(Ns+Ni+Nf+1,2),xLeft,yLeft,xRight,yRight,errorl,errorr,deltal,deltar) 
!!      write(*,*) "ok ? ",tail_courbe(Ni,1)," ",tail_courbe(Ni-1,1)," ",tail_courbe(Ni,2)," ",tail_courbe(Ni-1,2)," ",distslice(1,1)," ",distslice(1,2)," ",distslice(Ns+Ni+Nf+1,1)," ",distslice(Ns+Ni+Nf+1,2)," ",errorl," ",errorr," ",deltal," ",deltar," ",points_courbe_equal(1,1)-(Ni-1)*dsi," ",points_courbe_equal(1,1)-(Ni-2)*dsi
!!      call intersection(points_courbe_equal(1,1)-(Ni-1)*dsi,points_courbe_equal(1,1)-(Ni-2)*dsi,points_courbe_equal(1,2),points_courbe_equal(1,2),tail_courbe(Ni,1),tail_courbe(Ni,2),distslice(1,1),distslice(1,2),distslice(Ns+Ni+Nf+1,1),distslice(Ns+Ni+Nf+1,2),xLeft,yLeft,xRight,yRight,errorl,errorr,deltal,deltar) 
!!      write(*,*) "ok ? ",tail_courbe(Ni,1)," ",tail_courbe(Ni-1,1)," ",tail_courbe(Ni,2)," ",tail_courbe(Ni-1,2)," ",distslice(1,1)," ",distslice(1,2)," ",distslice(Ns+Ni+Nf+1,1)," ",distslice(Ns+Ni+Nf+1,2)," ",errorl," ",errorr," ",deltal," ",deltar," ",points_courbe_equal(1,1)-(Ni-1)*dsi," ",points_courbe_equal(1,1)-(Ni-2)*dsi
!
!      !if (det(tail_courbe(Ni,1),tail_courbe(Ni,2),left_courbe_equal(lpli-1,1),left_courbe_equal(lpli-1,2),tail_courbe(Ni,1),tail_courbe(Ni,2)) * det(tail_courbe(Ni,1),tail_courbe(Ni,2),left_courbe_equal(lpli-1,1),left_courbe_equal(lpli-1,2),xLeft,yLeft) >= 0._pr) then
!      !  lpli = lpli+1
!        left_courbe_equal(lpli,1) = xLeft
!        left_courbe_equal(lpli,2) = yLeft
!      !endif
!      !if (det(tail_courbe(Ni,1),tail_courbe(Ni,2),right_courbe_equal(lpri-1,1),right_courbe_equal(lpri-1,2),tail_courbe(Ni,1),tail_courbe(Ni,2)) * det(tail_courbe(Ni,1),tail_courbe(Ni,2),right_courbe_equal(lpri-1,1),right_courbe_equal(lpri-1,2),xRight,yRight) >= 0._pr) then
!      !  lpri = lpri+1
!        right_courbe_equal(lpri,1) = xRight
!        right_courbe_equal(lpri,2) = yRight
!        !write(*,*) "xLeft ",left_courbe_equal(1,1)," yLeft ",left_courbe_equal(1,2)," xRight ",right_courbe_equal(1,1)," yRight ",right_courbe_equal(1,2)
!        !write(*,*) "xLeft ",left_courbe_equal(Ni,1)," yLeft ",left_courbe_equal(Ni,2)," xRight ",right_courbe_equal(Ni,1)," yRight ",right_courbe_equal(Ni,2)
!      !endif
!      !left_courbe_equal(1,1) = xLeft
!      !left_courbe_equal(1,2) = yLeft
!      !right_courbe_equal(1,1) = xRight
!      !right_courbe_equal(1,2) = yRight
!      !write(*,*) "total : ",lpli," ",lpri
!      do l=2,Ni-1
!        call intersection(tail_courbe(Ni-l+1+1,1),tail_courbe(Ni-l+1-1,1),tail_courbe(Ni-l+1+1,2),tail_courbe(Ni-l+1-1,2)&
!,tail_courbe(Ni-l+1,1),tail_courbe(Ni-l+1,2),distslice(l,1),distslice(l,2),distslice(Ns+Ni+Nf+l,1),distslice(Ns+Ni+Nf+l,2),xLeft&
!,yLeft,xRight,yRight,errorl,errorr,deltal,deltar) 
!!        if (det(tail_courbe(Ni-l+1+1,1),tail_courbe(Ni-l+1+1,2),left_courbe_equal(lpli,1),left_courbe_equal(lpli,2),tail_courbe(Ni-l+1,1),tail_courbe(Ni-l+1,2)) * det(tail_courbe(Ni-l+1+1,1),tail_courbe(Ni-l+1+1,2),left_courbe_equal(lpli,1),left_courbe_equal(lpli,2),xLeft,yLeft) >= 0._pr) then
!          lpli = lpli+1
!          !write(*,*) "INITIAL LEFT : ",l," ",lpli
!          left_courbe_equal(lpli,1) = xLeft
!          left_courbe_equal(lpli,2) = yLeft
!!        endif
!!!        if (det(tail_courbe(Ni-l+1+1,1),tail_courbe(Ni-l+1+1,2),right_courbe_equal(lpri,1),right_courbe_equal(lpri,2),tail_courbe(Ni-l+1,1),tail_courbe(Ni-l+1,2)) * det(tail_courbe(Ni-l+1+1,1),tail_courbe(Ni-l+1+1,2),right_courbe_equal(lpri,1),right_courbe_equal(lpri,2),xRight,yRight) >= 0._pr) then
!          lpri = lpri+1
!          !write(*,*) "INITIAL RIGHT : ",l," ",lpli
!          right_courbe_equal(lpri,1) = xRight
!          right_courbe_equal(lpri,2) = yRight
!!!        endif
!        !left_courbe_equal(l,1) = xLeft
!        !left_courbe_equal(l,2) = yLeft
!        !right_courbe_equal(l,1) = xRight
!        !right_courbe_equal(l,2) = yRight
!        !write(*,*) "initial : ",l," ",tail_courbe(Ni-l+1-1,1)," ",tail_courbe(Ni-l+1+1,1)," ",tail_courbe(Ni-l+1-1,2)," ",tail_courbe(Ni-l+1+1,2)
!        !write(*,*) "initial : ",l," ",distslice(Ni-l+1,1),distslice(Ni-l+1,2)," ",distslice(Ns+Ni+Nf+Ni-l+1,1)," ",distslice(Ns+Ni+Nf+Ni-l+1,2)
!        !write(*,*) "initial : ",l," ",xleft," ",yLeft," ",xRight," ",yRight
!        !write(*,*) "initial : ",l," ",errorl," ",errorr," ",deltal," ",deltar,"  dti  ",dti
!      enddo
!      !write(*,*) "total : ",lpli," ",lpri
!        !write(*,*) "xLeft ",left_courbe_equal(1,1)," yLeft ",left_courbe_equal(1,2)," xRight ",right_courbe_equal(1,1)," yRight ",right_courbe_equal(1,2)
!        !write(*,*) "xLeft ",left_courbe_equal(Ni,1)," yLeft ",left_courbe_equal(Ni,2)," xRight ",right_courbe_equal(Ni,1)," yRight ",right_courbe_equal(Ni,2)
!!        call intersection(tail_courbe(Ni-1,1),points_courbe_equal(2,1),tail_courbe(Ni-1,2),points_courbe_equal(2,1),tail_courbe(Ni,1),tail_courbe(Ni,2),distslice(Ni,1),distslice(Ni,2),distslice(Ns+Ni+Nf+Ni,1),distslice(Ns+Ni+Nf+Ni+1,2),xLeft,yLeft,xRight,yRight,errorl,errorr,deltal,deltar) 
!        call intersection(tail_courbe(Ni-1,1),points_courbe_equal(2,1),tail_courbe(Ni-1,2),points_courbe_equal(2,2)&
!,points_courbe_equal(1,1),points_courbe_equal(1,2),distslice(Ni,1),distslice(Ni+1,2),distslice(Ns+Ni+Nf+Ni,1)&
!,distslice(Ns+Ni+Nf+Ni+1,2),xLeft,yLeft,xRight,yRight,errorl,errorr,deltal,deltar) 
!!        left_courbe_equal(1,1) = xLeft
!!        left_courbe_equal(1,2) = yLeft
!!        right_courbe_equal(1,1) = xRight
!!        right_courbe_equal(1,2) = yRight
!
!
!      !write(*,*) "ok ? ",tail_courbe(Ni-1,1)," ",points_courbe_equal(2,1)," ",tail_courbe(Ni-1,2)," ",points_courbe_equal(2,2)," ",distslice(Ni,1)," ",distslice(Ni+1,1)," ",distslice(Ni,2)," ",distslice(Ni+1,2)," ",distslice(Ns+Ni+Nf+Ni+1,1)," ",distslice(Ns+Ni+Nf+Ni+1,2)," ",errorl," ",errorr," ",deltal," ",deltar," ",xLeft," ",yLeft
!      !write(*,*) "ok ? ",distslice(Ni+1,1)," ",distslice(Ni,2)
!
!
!        !write(*,*) "xLeft1 ",left_courbe_equal(1,1)," yLeft ",left_courbe_equal(1,2)," xRight ",right_courbe_equal(1,1)," yRight ",right_courbe_equal(1,2)
!        lpl = lpli -Ni +1
!        lpr = lpri -Ni +1
!        !write(*,*) "lp initiaux : ",lpl," ",lpr
!        l = Ni
!        !lpl = 1
!        !lpr = 1
!        !if (det(tail_courbe(Ni-1,1),tail_courbe(Ni-1,2),left_courbe_equal(lpl-1+Ni-1,1),left_courbe_equal(lpl-1+Ni-1,2),points_courbe_equal(1,1),points_courbe_equal(1,2)) * det(tail_courbe(Ni-1,1),tail_courbe(Ni-1,2),left_courbe_equal(lpl-1+Ni-1,1),left_courbe_equal(lpl-1+Ni-1,2),xLeft,yLeft) >= 0._pr) then
!        !  lpl = lpl+1!-Ni+1
!          left_courbe_equal(lpl+Ni-1,1) = xLeft
!          left_courbe_equal(lpl+Ni-1,2) = yLeft
!        !endif
!!!        if (det(tail_courbe(Ni-1,1),tail_courbe(Ni-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),points_courbe_equal(1,1),points_courbe_equal(1,2)) * det(tail_courbe(Ni-1,1),tail_courbe(Ni-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),xRight,yRight) >= 0._pr) then
!          !lpr = lpr+1!-Ni+1
!          right_courbe_equal(lpr+Ni-1,1) = xRight
!          right_courbe_equal(lpr+Ni-1,2) = yRight
!!!        endif
!        !write(*,*) "total : ",lpl+Ni-1," ",lpr+Ni-1
!        !write(*,*) "xLeft ",left_courbe_equal(Ni,1)," yLeft ",left_courbe_equal(Ni,2)," xRight ",right_courbe_equal(Ni,1)," yRight ",right_courbe_equal(Ni,2)
!      do l=2,Ns-1
!        call intersection(points_courbe_equal(l-1,1),points_courbe_equal(l+1,1),points_courbe_equal(l-1,2)&
!,points_courbe_equal(l+1,2),points_courbe_equal(l,1),points_courbe_equal(l,2),distslice(l+Ni,1),distslice(l+Ni,2)&
!,distslice(Ns+Ni+Nf+l+Ni,1),distslice(Ns+Ni+Nf+l+Ni,2),xLeft,yLeft,xRight,yRight,errorl,errorr,deltal,deltar) 
!        !call intersection(points_courbe_equal(l-1,1),points_courbe_equal(l+1,1),points_courbe_equal(l-1,2),points_courbe_equal(l+1,2),points_courbe_equal(l,1),points_courbe_equal(l,2),distslice(l,1),distslice(l,2),distslice(l,1),distslice(l,2),xLeft,yLeft,xRight,yRight) 
!        !call intersectionR(points_courbe_equal(l,1),points_courbe_equal(l+1,1),points_courbe_equal(l,2),points_courbe_equal(l+1,2),distslice(l,1),distslice(l,2),distslice(Ns+l,1),distslice(Ns+l,2),xLeft,yLeft,xRight,yRight) 
!        !if ((errorl==1).or.(errorr==1)) write(*,*) "error : ",l," dist        ",distslice(l+Ni,1)," ",distslice(l+Ni,2)," ",distslice(Ns+Nf+Ni+l+Ni,1)," ",distslice(Ns+Nf+Ni+l+Ni,2),"  YY  ",points_courbe_equal(l+1,1)," ",points_courbe_equal(l+1,2)," DELTA ",deltal," ",deltar
!        !write(*,*) "ok ... ",yLeft," ",l," ",points_courbe_equal(l-1,1)," ",points_courbe_equal(l+1,1)," ",points_courbe_equal(l-1,2)," ",points_courbe_equal(l+1,2)," ",distslice(l+Ni,1)," ",distslice(l+Ni,2)," ",deltal," ",deltar," ",errorl
!        !if (l>Ns-20) write(*,*) "aie aie aie ... ",yLeft," ",l," ",points_courbe_equal(l-1,1)," ",points_courbe_equal(l,1)," ",points_courbe_equal(l+1,1)," ",points_courbe_equal(l-1,2)," ",points_courbe_equal(l,2)," ",points_courbe_equal(l+1,2)," ",distslice(l+Ni,1)," ",distslice(l+Ni,2)," ",deltal," ",deltar," ",errorl
!        !if ((errorl==1).or.(errorr==1)) then
!        !  call intersection(points_courbe_equal(l,1),points_courbe_equal(l+1,1),points_courbe_equal(l,2),points_courbe_equal(l+1,2),points_courbe_equal(l,1),points_courbe_equal(l,2),distslice(l,3),distslice(l,2),distslice(Ns+l,3),distslice(Ns+l,2),xLeft,yLeft,xRight,yRight,errorl,errorr,deltal,deltar)
!        !endif
!        !if ((errorl==1).or.(errorr==1)) then
!        !  call intersection(points_courbe_equal(l-1,1),points_courbe_equal(l,1),points_courbe_equal(l-1,2),points_courbe_equal(l,2),points_courbe_equal(l,1),points_courbe_equal(l,2),distslice(l,1),distslice(l,3),distslice(Ns+l,1),distslice(Ns+l,3),xLeft,yLeft,xRight,yRight,errorl,errorr,deltal,deltar)
!        !endif
!        !if ((errorl==1).or.(errorr==1)) write(*,*) "error : ",l," dist        ",distslice(l,1)," ",distslice(l,2)," ",distslice(Ns+l,1)," ",distslice(Ns+l,2),"  YY  ",points_courbe_equal(l+1,1)," ",points_courbe_equal(l+1,2)," DELTA ",deltal," ",deltar
!        !write(*,*) "error : ",l," dist        ",distslice(l,1)," ",distslice(l,2)," ",distslice(Ns+l,1)," ",distslice(Ns+l,2),"  YY  ",points_courbe_equal(l+1,1)," ",points_courbe_equal(l+1,2)," DELTA ",deltal," ",deltar
!        !write(*,*) "error : ",l," dist        ",distslice(l,1)," ",distslice(l,2)," ",distslice(Ns+l,1)," ",distslice(Ns+l,2),"  XX  ",points_courbe_equal(l-1,1)," ",points_courbe_equal(l+1,1),"  YY  ",points_courbe_equal(l-1,2)," ",points_courbe_equal(l+1,2)," DELTA ",deltal," ",deltar
!        !left_courbe_equal(l+Ni-1,1) = xLeft
!        !left_courbe_equal(l+Ni-1,2) = yLeft
!        !right_courbe_equal(l+Ni-1,1) = xRight
!        !right_courbe_equal(l+Ni-1,2) = yRight
!        !if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2)) * det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),xRight,yRight) <= 0)  WRITE(*,*) "WARNING ",l," ",kt+picNum-1
!
!!!        if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),left_courbe_equal(lpl-1+Ni-1,1),left_courbe_equal(lpl-1+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2)) * det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),left_courbe_equal(lpl-1+Ni-1,1),left_courbe_equal(lpl-1+Ni-1,2),xLeft,yLeft) <= 0._pr) then
!          lpl = lpl+1
!          left_courbe_equal(lpl+Ni-1,1) = xLeft
!          left_courbe_equal(lpl+Ni-1,2) = yLeft
!!!        endif
!!!        if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2)) * det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),xRight,yRight) >= 0._pr) then
!          lpr = lpr+1
!          right_courbe_equal(lpr+Ni-1,1) = xRight
!          right_courbe_equal(lpr+Ni-1,2) = yRight
!!!        endif
!      enddo
!        !write(*,*) "total : ",lpl+Ni-1," ",lpr+Ni-1
!      l = Ns
!      call intersection(points_courbe_equal(l-1,1),head_courbe(1+1,1),points_courbe_equal(Ns-1,2),head_courbe(1+1,2)&
!,head_courbe(1,1),head_courbe(1,2),distslice(Ni+Ns-1,1),distslice(1+Ni+Ns-1,2),distslice(Ns+Ni+Nf+Ni+Ns-1,1)&
!,distslice(Ns+Ni+Nf+1+Ni+Ns-1,2),xLeft,yLeft,xRight,yRight,errorl,errorr,deltal,deltar) 
!      !write(*,*) "INFO: ",lp," ",xLeft," ",kt+picNum-1
!
!!!      if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),left_courbe_equal(lpl-1+Ni-1,1),left_courbe_equal(lpl-1+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2)) * det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),left_courbe_equal(lpl-1+Ni-1,1),left_courbe_equal(lpl-1+Ni-1,2),xLeft,yLeft) <= 0._pr) then
!              lpl = lpl+1
!              left_courbe_equal(lpl+Ni-1,1) = xLeft
!              left_courbe_equal(lpl+Ni-1,2) = yLeft
!!!      endif
!      !left_courbe_equal(1+Ni-1+Ns-1,1) = xLeft
!      !left_courbe_equal(1+Ni-1+Ns-1,2) = yLeft
!!!      if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2)) * det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),xRight,yRight) >= 0._pr) then
!        lpr = lpr+1
!        right_courbe_equal(lpr+Ni-1,1) = xRight
!        right_courbe_equal(lpr+Ni-1,2) = yRight
!!!      endif
!        !write(*,*) "total : ",lpl+Ni-1," ",lpr+Ni-1
!      !lplf = 1
!      !lprf = 1
!      do l=2,Nf-1
!        call intersection(head_courbe(l-1,1),head_courbe(l+1,1),head_courbe(l-1,2),head_courbe(l+1,2),head_courbe(l,1)&
!,head_courbe(l,2),distslice(l+Ni+Ns,1),distslice(l+Ni+Ns,2),distslice(Ns+Ni+Nf+l+Ni+Ns,1),distslice(Ns+Ni+Nf+l+Ni+Ns,2),xLeft,yLeft&
!,xRight,yRight,errorl,errorr,deltal,deltar) 
!
!!!        if (det(head_courbe(l-1,1),head_courbe(l-1,2),left_courbe_equal(lpl+Ni-1-1,1),left_courbe_equal(lpl-1+Ni-1,2),head_courbe(l,1),head_courbe(l,2)) * det(head_courbe(l-1,1),head_courbe(l-1,2),left_courbe_equal(lpl-1+Ni-1,1),left_courbe_equal(lpl-1+Ni-1,2),xLeft,yLeft) <= 0._pr) then
!!      if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),left_courbe_equal(lpl-1+Ni-1,1),left_courbe_equal(lpl-1+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2)) * det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),left_courbe_equal(lpl-1+Ni-1,1),left_courbe_equal(lpl-1+Ni-1,2),xLeft,yLeft) >= 0._pr) then
!          lpl = lpl+1
!!          left_courbe_equal(lpl+Ni-1+Ns-1,1) = xLeft
!!          left_courbe_equal(lpl+Ni-1+Ns-1,2) = yLeft
!              left_courbe_equal(lpl+Ni-1,1) = xLeft
!              left_courbe_equal(lpl+Ni-1,2) = yLeft
!!!        endif
!!!        if (det(head_courbe(l-1,1),head_courbe(l-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),head_courbe(l,1),head_courbe(l,2)) * det(head_courbe(l-1,1),head_courbe(l-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),xRight,yRight) >= 0._pr) then
!!      if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2)) * det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),xRight,yRight) >= 0._pr) then
!          lpr = lpr+1
!!          right_courbe_equal(lpr+Ni-1+Ns-1,1) = xRight
!!          right_courbe_equal(lpr+Ni-1+Ns-1,2) = yRight
!        right_courbe_equal(lpr+Ni-1,1) = xRight
!        right_courbe_equal(lpr+Ni-1,2) = yRight
!!!        endif
!        !left_courbe_equal(l+Ni-1+Ns-1,1) = xLeft
!        !left_courbe_equal(l+Ni-1+Ns-1,2) = yLeft
!        !right_courbe_equal(l+Ni-1+Ns-1,1) = xRight
!        !right_courbe_equal(l+Ni-1+Ns-1,2) = yRight
!      enddo
!        !write(*,*) "total : ",lpl+Ni-1," ",lpr+Ni-1
!        !write(*,*) "xLeft ",left_courbe_equal(1,1)," yLeft ",left_courbe_equal(1,2)," xRight ",right_courbe_equal(1,1)," yRight ",right_courbe_equal(1,2)
!        !write(*,*) "xLeft ",left_courbe_equal(Ni,1)," yLeft ",left_courbe_equal(Ni,2)," xRight ",right_courbe_equal(Ni,1)," yRight ",right_courbe_equal(Ni,2)
!!      call intersection(points_courbe_equal(Ns-1,1),points_courbe_equal(Ns,1),points_courbe_equal(Ns-1,2),points_courbe_equal(Ns,2),points_courbe_equal(Ns,1),points_courbe_equal(Ns,2),distslice(Ns+Ni,1),distslice(Ns+Ni,2),distslice(Ns+Ni+Nf+Ns+Ni,1),distslice(Ns+Ni+Nf+Ns+Ni,2),xLeft,yLeft,xRight,yRight,errorl,errorr,deltal,deltar)
!      l = Nf
!      call intersection(head_courbe(l-1,1),head_courbe(l,1),head_courbe(l-1,2),head_courbe(l,2),head_courbe(l,1),head_courbe(l,2)&
!,distslice(l+Ni+Ns,1),distslice(l+Ni+Ns,2),distslice(Ns+Ni+Nf+l+Ni+Ns,1),distslice(Ns+Ni+Nf+l+Ni+Ns,2),xLeft,yLeft,xRight,yRight&
!,errorl,errorr,deltal,deltar) 
!      !left_courbe_equal(Ns+Ni-1,1) = xLeft
!      !left_courbe_equal(Ns+Ni-1,2) = yLeft
!      !right_courbe_equal(Ns+Ni-1,1) = xRight
!      !right_courbe_equal(Ns+Ni-1,2) = yRight
!
!!!      if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),left_courbe_equal(lpl-1+Ni-1,1),left_courbe_equal(lpl-1+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2)) * det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),left_courbe_equal(lpl-1+Ni-1,1),left_courbe_equal(lpl-1+Ni-1,2),xLeft,yLeft) <= 0._pr) then
!          lpl = lpl+1
!              left_courbe_equal(lpl+Ni-1,1) = xLeft
!              left_courbe_equal(lpl+Ni-1,2) = yLeft
!!!      endif
!!!      if (det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),points_courbe_equal(l,1),points_courbe_equal(l,2)) * det(points_courbe_equal(l-1,1),points_courbe_equal(l-1,2),right_courbe_equal(lpr-1+Ni-1,1),right_courbe_equal(lpr-1+Ni-1,2),xRight,yRight) >= 0._pr) then
!          lpr = lpr+1
!        right_courbe_equal(lpr+Ni-1,1) = xRight
!        right_courbe_equal(lpr+Ni-1,2) = yRight
!!!      endif
!       ! write(*,*) "total : ",lpl+Ni-1," ",lpr+Ni-1
!
!        !write(*,*) "xLeft2 ",left_courbe_equal(1,1)," yLeft ",left_courbe_equal(1,2)," xRight ",right_courbe_equal(1,1)," yRight ",right_courbe_equal(1,2)
!!      write(*,*) "CHECKED"   
!!    do l=1,sum(skel)
!!      !do l=l0,l0+5
!!      !do l=lp,lp
!!      if ((ceiling(xRight_tab(l)) == 0).or.(ceiling(yRight_tab(l)) == 0).or.(floor(yLeft_tab(l)) == 0).or.(floor(xLeft_tab(l)) == 0)) then
!!        write(*,*) "INTROUVABLE : ",l
!!      else
!!        write(*,*)"CHECKED 3",sum(skel)," ",xLeft_tab(l)," ",xRight_tab(l)," LLLL   ",l
!!        write(*,*)"CHECKED 4",sum(skel)," ",yLeft_tab(l)," ",yRight_tab(l)," LLLL   ",l
!!        tmpbool(floor(xLeft_tab(l)),floor(yLeft_tab(l))) = 1
!!        tmpbool(ceiling(xRight_tab(l)),ceiling(yRight_tab(l))) = 1
!!      endif
!!    enddo
!!      write(*,*) "CHECKED"   
!  
!!    !tmpbool(midline(1,1),midline(1,2)) = 0
!!    !skel(midline(1,1),midline(1,2)) = 0
!!    !tmpbool(midline(size(midline,1),1),midline(size(midline,1),2)) = 0
!!    tmpbool(midline(1,1),midline(1,2)) = 0
!!    skel2(midline(1,1),midline(1,2)) = 0
!!    !tmpbool(midline(2,1),midline(2,2)) = 0
!!    !skel2(midline(2,1),midline(2,2)) = 0
!!    !tmpbool(xl,yl) = 0
!!    !skel2(xl,yl) = 0
!!    tmpbool(midline(sum(skel),1),midline(sum(skel),2)) = 0
!!    skel2(midline(sum(skel),1),midline(sum(skel),2)) = 0
!!    !do l=lp,lp!sum(skel)
!!    !tmpbool(midline(l,1),midline(l,2)) = 0
!!    !skel(midline(l,1),midline(l,2)) = 0
!!    !!tmpbool(midline(l+1,1),midline(l+1,2)) = 0
!!    !!skel(midline(l+1,1),midline(l+1,2)) = 0
!!    !enddo
!  
!    deallocate(midline,xLeft_tab,yLeft_tab,xRight_tab,yRight_tab)
!    deallocate(points_control)
        
!!    open(unit=85,file='results/skelhead'//str(kt+140-1)//'.txt',status='unknown')
!!    open(unit=84,file='results/right'//str(kt+140-1)//'.txt',status='unknown')
!!    open(unit=83,file='results/left'//str(kt+140-1)//'.txt',status='unknown')
!!    open(unit=82,file='results/skeletteq'//str(kt+140-1)//'.txt',status='unknown')
!!    open(unit=81,file='results/skelett'//str(kt+140-1)//'.txt',status='unknown')
!    open(unit=79,file='results_slow/skelet'//str(kt+140-1)//'.vtk',status='unknown')
    open(unit=79,file='results_secondTEST6/skelet'//str(kt+1-1)//'.vtk',status='unknown')
      write(79,'(1A26)') '# vtk DataFile Version 2.0'
      write(79,'(a)') 'rho'
      write(79,'(a)') 'ASCII'
      write(79,'(a)') 'DATASET STRUCTURED_POINTS'
      write(79,'(a,I4,I4,I4)') 'DIMENSIONS', N,ny,1
      write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
      write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,1. 
      write(79,'(a,I6)') 'POINT_DATA          ' , N*ny 
      write(79,'(a)') 'SCALARS values double'
      write(79,'(a)') 'LOOKUP_TABLE default'
    
    do k=1,ny
      do j=1,N
      !        write(79,*) rhoSlices2(j,k)
        !write(79,*) (1-tmpbool(j,k))*(rhoSlices2(j,k)*(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k)) + minval(rhoSlices2)*tmpbool(j,k)
        !write(79,*) (1-tmpbool(j,k))*(rhoSlices2(j,k)*(1-skel2(j,k)) + minval(rhoSlices2)*skel2(j,k)) + minval(rhoSlices2)*tmpbool(j,k)
        write(79,*) rhoSlices2(j,k)*(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k)
        !write(79,*) rhoSlices2(j,k)*(1-skel2(j,k)) + minval(rhoSlices2)*skel2(j,k)
      !         write(79,*) skel2(j,k)
      enddo
    enddo
    !open(unit=78,file='results_slow/gradPhi'//str(kt+140-1)//'.vtk',status='unknown')
    open(unit=78,file='results_secondTEST6/gradPhi'//str(kt+1-1)//'.vtk',status='unknown')
      write(78,'(1A26)') '# vtk DataFile Version 2.0'
      write(78,'(a)') 'rho'
      write(78,'(a)') 'ASCII'
      write(78,'(a)') 'DATASET STRUCTURED_POINTS'
      write(78,'(a,I4,I4,I4)') 'DIMENSIONS', N,ny,1
      write(78,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
      write(78,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,1. 
      write(78,'(a,I6)') 'POINT_DATA          ' , N*ny 
      write(78,'(a)') 'SCALARS values double'
      write(78,'(a)') 'LOOKUP_TABLE default'
    
    do k=1,ny
      do j=1,N
        write(78,*) gradPhi(j,k) !*(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k)
      enddo
    enddo
!!    do l=1,Ni-1
!!      write(82,*) tail_courbe(Ni-l+1,1)," ",tail_courbe(Ni-l+1,2)
!!    enddo
!!    do l=1,Ns
!!      write(81,*) points_courbe(l,1)," ",points_courbe(l,2)
!!      write(82,*) points_courbe_equal(l,1)," ",points_courbe_equal(l,2)
!!    enddo
!!    do l=2,Nf
!!      write(85,*) head_courbe(l,1)," ",head_courbe(l,2)
!!    enddo
!!
!!    !do l=1,Ni-1
!!    !  write(83,*) left_courbe_equal(Ni-l+1,1)," ",left_courbe_equal(Ni-l+1,2)
!!    !enddo
!!    !do l=1,lpli
!!    !  write(83,*) left_courbe_equal(Ni-l+1,1)," ",left_courbe_equal(Ni-l+1,2)
!!    !enddo
!!    do l=1,lpl
!!      write(83,*) left_courbe_equal(l+Ni-1,1)," ",left_courbe_equal(l+Ni-1,2)
!!    enddo
!!    !do l=1,Nf-1
!!    !  write(83,*) left_courbe_equal(l+Ni-1+Ns-1,1)," ",left_courbe_equal(l+Ni-1+Ns-1,2)
!!    !enddo
!!
!!    !do l=1,Ni-1
!!    !  write(84,*) right_courbe_equal(Ni-l+1,1)," ",right_courbe_equal(Ni-l+1,2)
!!    !enddo
!!    !do l=1,lpri
!!    !  write(84,*) right_courbe_equal(Ni-l+1,1)," ",right_courbe_equal(Ni-l+1,2)
!!    !enddo
!!    do l=1,lpr
!!      write(84,*) right_courbe_equal(l+Ni-1,1)," ",right_courbe_equal(l+Ni-1,2)
!!    enddo
!!    !do l=1,Nf-1
!!    !  write(84,*) right_courbe_equal(l+Ni-1+Ns-1,1)," ",right_courbe_equal(l+Ni-1+Ns-1,2)
!!    !enddo
!    write(80,*) kt,"    ",sum(skel)," ",long3," ",long2," ",long
    close(79)    
    close(78)
!!    close(81)
!!    close(82)
!!    close(83)
!!    close(84)
!!    close(85)
  
  enddo  
!  close(80)
  write(*,*) "************************************************" 
  deallocate(rhoSlices2)
  deallocate(rhoSlices)
!  deallocate(rho0) 
  deallocate(xx,yy)
!  deallocate(u,v,rho,rhou,rhov) 
!  deallocate(rhoup,rhovp,rhop)
!  deallocate(uu,vv,rhouu,rhovv,rhouup,rhovvp)
!  deallocate(u0,v0)
  deallocate(tmp1,tmp2,tmpbool)
  deallocate(dir1,dir2,dir3,dir4,Nseed,skel,skel2,skel3)
  deallocate(un,zero)
  deallocate(distslice)
  deallocate(points_courbe,points_courbe_equal)
  deallocate(left_courbe,left_courbe_equal)
  deallocate(right_courbe,right_courbe_equal)
  deallocate(tail_courbe,head_courbe)
  deallocate(righttail,lefttail,righthead,lefthead,leftmid,rightmid)
  deallocate(gradPhi)
end program def3D

character(len=3) function str(k)
  integer, intent(in) :: k
  write (str, '(I3.3)') k
  str = adjustl(str)
end function str
