module libBezier
  use doubler
  use AdvectionProblem

  implicit none

contains  
  !real(pr) function det(x1, y1, x2, y2, x3, y3) !det(A1A2,A1A3)
  !    implicit none
  !    real(pr) :: x1,y1,x2,y2,x3,y3
  !
  !    det = (x2-x1)*(y3-y1) - (y2-y1)*(x3-x1)
  !end function det

  subroutine intersection(x1,x2,y1,y2,xc,yc,d1l,d2l,d1r,d2r,xl,yl,xr,yr,errorl,errorr,deltal,deltar) !X1:l Xc:l X2:l+1
    implicit none
    real(pr),intent(in) :: x1,x2,y1,y2,d1l,d2l,d1r,d2r
    real(pr),intent(in) :: xc,yc
    real(pr),intent(out) :: xl,yl,xr,yr,deltal,deltar
    integer,intent(out) :: errorl,errorr
    real(pr) :: A,Bl,Br,Cl,Cr,xal,xar,xbl,xbr,yal,yar,ybl,ybr,cstl,cstr

    errorr = 0
    !if (abs(y2-y1)>1.e-3_pr) then
    !if (abs(y2-y1)/dx>eepsilon) then
    if (abs(y2-y1)/dx>10*eepsilon) then
       cstl = ((d1l*d1l - x1*x1 - y1*y1) - (d2l*d2l - x2*x2 - y2*y2))/(2*(y2-y1))
       !     cstr = ((d1r*d1r - x1*x1 - y1*y1) - (d2r*d2r - x2*x2 - y2*y2))/(2*(y2-y1))
       A = 1._pr + (x2-x1)*(x2-x1)/((y2-y1)*(y2-y1))
       Bl = -2*(cstl-y1)*(x2-x1)/(y2-y1) - 2*x1
       !     Br = -2*(cstr-y1)*(x2-x1)/(y2-y1) - 2*x1
       Cl = x1*x1 - d1l*d1l + (cstl-y1)*(cstl-y1)
       !     Cr = x1*x1 - d1r*d1r + (cstr-y1)*(cstr-y1)
       !     write(*,*) "racine 1"
       call racine(A,Bl,Cl,xal,xbl,errorl,deltal)
       !     write(*,*) "racine 1 ",A," ",Bl," ",Cl," delta  ",Bl*Bl-4*A*Cl," ",d1l," ",d2l," ",x1," ",y1," ",x2," ",y2
       !     write(*,*) "racine 2"
       !!     call racine(A,Br,Cr,xar,xbr,errorr,deltar)
       !     write(*,*) "racine 2"
       !yal = cstl - xal*(x2-x1)/(y2-y1)
       !yar = cstr - xar*(x2-x1)/(y2-y1)
       !ybl = cstl - xbl*(x2-x1)/(y2-y1)
       !ybr = cstr - xbr*(x2-x1)/(y2-y1)
       !yal = (((d1l*d1l - x1*x1 - y1*y1)-(d2l*d2l - x2*x2 - y2*y2))*0.5_pr-xal*(x2-x1))/(y2-y1) !cstl - xal*(x2-x1)/(y2-y1)
       yal = cstl - xal*(x2-x1)/(y2-y1)
       yar = cstr - xar*(x2-x1)/(y2-y1)
       !ybl = (((d1l*d1l - x1*x1 - y1*y1)-(d2l*d2l - x2*x2 - y2*y2))*0.5_pr-xbl*(x2-x1))/(y2-y1) !cstl - xbl*(x2-x1)/(y2-y1)
       ybl = cstl - xbl*(x2-x1)/(y2-y1)
       ybr = cstr - xbr*(x2-x1)/(y2-y1)
       if (det(xal,yal,x2,y2,x1,y1) <= 0) then
          xl = xal
          yl = yal
       else
          xl = xbl
          yl = ybl
       endif
       if (det(xar,yar,x2,y2,x1,y1) >= 0) then
          xr = xar
          yr = yar
       else
          xr = xbr
          yr = ybr
       endif
    else
       xl = ((d1l*d1l - x1*x1) - (d2l*d2l - x2*x2))/(2*(x2-x1))
       xr = ((d1r*d1r - x1*x1) - (d2r*d2r - x2*x2))/(2*(x2-x1))
       !     if (d1l*d1l - (xl-x1)*(xl-x1) - (yc-y1)*(yc-y1)<0._pr) then
       !       !yl = yc + sqrt(d2l*d2l - (xl-x2)*(xl-x2) - (yc-y2)*(yc-y2))
       !       !yr = yc - sqrt(d2l*d2l - (xl-x2)*(xl-x2) - (yc-y2)*(yc-y2))
       !!       if (-(d1l*d1l - (xl-x1)*(xl-x1) - (yc-y1)*(yc-y1))
       !       yl = yc + d1l !sqrt(abs(d1l*d1l - (xl-x1)*(xl-x1) - (yc-y1)*(yc-y1)))
       !       yr = yc - d1l !sqrt(abs((d1l*d1l - (xl-x1)*(xl-x1) - (yc-y1)*(yc-y1))))
       !     else
       !       yl = yc + sqrt(d1l*d1l - (xl-x1)*(xl-x1) - (yc-y1)*(yc-y1))
       !       yr = yc - sqrt(d1l*d1l - (xl-x1)*(xl-x1) - (yc-y1)*(yc-y1))
       !     endif
       A = 1._pr
       Bl = -2*y1
       Br = -2*y1
       Cl = (xl-x1)*(xl-x1) + y1*y1 - d1l*d1l 
       Cr = (xr-x1)*(xr-x1) + y1*y1 - d1r*d1r 
       !write(*,*) "racine 3"
       call racine(A,Bl,Cl,yal,ybl,errorl,deltal)
       !     write(*,*) "racine 3"
       !     write(*,*) "racine 4"
       !!     call racine(A,Br,Cr,yar,ybr,errorr,deltar)
       !     write(*,*) "racine 4"
       !write(*,*) "triplets : ",A," ",Bl," ",Cl," ",Br," ",Cr
       if (det(xl,yal,x2,y2,x1,y1) <= 0) then
          yl = yal
       else
          yl = ybl
       endif
       if (det(xr,yar,x2,y2,x1,y1) >= 0) then
          yr = yar
       else
          yr = ybr
       endif
    endif
  end subroutine intersection


  subroutine intersectionR(x1,x2,y1,y2,d1l,d2l,d1r,d2r,xl,yl,xr,yr,errorl,errorr,deltal,deltar) !X1:l X2:l+1
    implicit none
    real(pr),intent(in) :: x1,x2,y1,y2,d1l,d2l,d1r,d2r
    real(pr),intent(out) :: xl,yl,xr,yr,deltal,deltar
    integer,intent(out) :: errorl,errorr
    real(pr) :: A,B,Cl,Cr,xal,xar,xbl,xbr,yal,yar,ybl,ybr,cst

    cst = (x2*x1 + y2*y1 - x2*x2 - x1*x1 - y2*y2 - y1*y1)/(y2-y1)

    A = 1._pr + (x2-x1)*(x2-x1)/((y2-y1)*(y2-y1))
    B = -2*(cst-y1)*(x2-x1)/(y2-y1) - 2*x1
    Cl = x1*x1 - d1l*d1l + (cst-y1)*(cst-y1)
    Cr = x1*x1 - d1r*d1r + (cst-y1)*(cst-y1)
    call racine(A,B,Cl,xal,xbl,errorl,deltal)
    call racine(A,B,Cr,xar,xbr,errorr,deltar)
    !if (abs(y2-y1)>1.e-6_pr) then
    if (abs(y2-y1)/dx>eepsilon) then
       yal = ((x2*x1 + y2*y1 - x2*x2 - x1*x1 - y2*y2 - y1*y1) - xal*(x2-x1))/(y2-y1)
       ybl = ((x2*x1 + y2*y1 - x2*x2 - x1*x1 - y2*y2 - y1*y1) - xbl*(x2-x1))/(y2-y1)
       yar = ((x2*x1 + y2*y1 - x2*x2 - x1*x1 - y2*y2 - y1*y1) - xar*(x2-x1))/(y2-y1)
       ybr = ((x2*x1 + y2*y1 - x2*x2 - x1*x1 - y2*y2 - y1*y1) - xbr*(x2-x1))/(y2-y1)
       if (det(xal,yal,x2,y2,x1,y1) <= 0) then
          xl = xal
          yl = yal
       else
          xl = xbl
          yl = ybl
       endif
       if (det(xar,yar,x2,y2,x1,y1) >= 0) then
          xr = xar
          yr = yar
       else
          xr = xbr
          yr = ybr
       endif
    else
       xl = x1
       yl = y1 + d1l
       xr = x1
       yr = y1 - d1r
    endif
  end subroutine intersectionR


  subroutine racine(a,b,c,x1,x2,error,delta) !ax2 + bx + c = 0 -- supposons delta>0 (intersection 2 cercles)
    implicit none
    real(pr),intent(in) :: a,b,c
    real(pr),intent(out) :: x1,x2
    integer,intent(out) :: error
    real(pr),intent(out) :: delta

    delta = b*b - 4*a*c
    if (delta>0._pr) then
       x1 = (-b-sqrt(abs(delta)))/(2*a)
       x2 = (-b+sqrt(abs(delta)))/(2*a)
       error = 0
    !else if (abs(delta)<1.e-6) then
    !else if (abs(delta)/(dx*dx)<eepsilon) then
    else if (abs(delta)/(dx*dx)<10*eepsilon) then
       x1 = -b/(2*a)
       x2 = x1
       error = 0
    else 
       error = 1
       write(*,*) "error ROOT FINDING : delta<0    ",a," ",b," ",c," ",delta
       !x1 = -b/(2*a)
       !x2 = x1
    endif
  end subroutine racine


  logical function appartient(tab,i,j,lmax)
    implicit none
    integer,dimension(:,:),intent(in) :: tab
    integer,intent(in) :: i,j,lmax
    integer :: l
    logical :: bool

    l = 1
    bool = .false.
    do while ((bool.eqv..false.).and.(l<lmax).and.(l<size(tab,1))) 
       !     write(*,*) "test size ==  ",i," ",j," ",l," ",size(tab,1)," ",size(tab,2)
       if ((tab(l,1)==i).and.(tab(l,2)==j)) bool = .true.
       l = l+1
    enddo

    appartient = bool
  end function appartient

  logical function appartientElmtTh(tab,theta,l)
    implicit none
    integer,dimension(:,:),intent(in) :: tab
    integer,intent(in) :: theta,l
    integer :: k
    logical :: bool

    k = 1
    bool = .false.
    do while ((bool.eqv..false.).and.(k<size(tab,2)+1)) 
       if (tab(theta,k)==l) bool = .true.
       k = k+1
    enddo

    appartientElmtTh = bool
  end function appartientElmtTh

  logical function appartientElmtL(tab,theta,l)
    implicit none
    integer,dimension(:,:),intent(in) :: tab
    integer,intent(in) :: theta,l
    integer :: k
    logical :: bool

    k = 1
    bool = .false.
    do while ((bool.eqv..false.).and.(k<size(tab,1)+1)) 
       if (tab(k,l)==theta) bool = .true.
       k = k+1
    enddo

    appartientElmtL = bool
  end function appartientElmtL



  subroutine initialize4(points_control)
    implicit none
    real(pr),dimension(:,:),intent(inout) :: points_control
    integer :: l

    !points_control(1,1) = 0._pr
    !points_control(1,2) = 0._pr

    !points_control(2,1) = 0.3_pr
    !points_control(2,2) = 1._pr

    !points_control(3,1) = 1._pr
    !points_control(3,2) = 1._pr

    !points_control(4,1) = 1._pr
    !points_control(4,2) = 0._pr
    points_control(1,1) = 0._pr
    points_control(1,2) = 0._pr

    points_control(2,1) = 1._pr
    points_control(2,2) = 1._pr

    points_control(3,1) = 2._pr
    points_control(3,2) = 2._pr

    points_control(4,1) = 4._pr
    points_control(4,2) = 4._pr
  end subroutine initialize4

  subroutine initialize7(points_control)
    implicit none
    real(pr),dimension(:,:),intent(inout) :: points_control
    integer :: l

    points_control(1,1) = 0._pr
    points_control(1,2) = 0._pr

    points_control(2,1) = 0.3_pr
    points_control(2,2) = 1._pr

    points_control(3,1) = 1._pr
    points_control(3,2) = 1._pr

    points_control(4,1) = 1._pr
    points_control(4,2) = 0._pr

    points_control(5,1) = 1._pr
    points_control(5,2) = -1._pr

    points_control(6,1) = 1.5_pr
    points_control(6,2) = -0.5_pr

    points_control(7,1) = 2._pr
    points_control(7,2) = 0._pr

    points_control(:,1) = points_control(:,1) + 1
    points_control(:,2) = points_control(:,2) + 1
  end subroutine initialize7

  !subroutine initializeN(points_control,pointsN)
  !  implicit none
  !  real(pr),dimension(:,:),intent(inout) :: points_control
  !  integer :: l

  !  !points_control(1,1) = 0._pr
  !  !points_control(1,2) = 0._pr
  ! 
  !  !points_control(2,1) = 0.3_pr
  !  !points_control(2,2) = 1._pr

  !  !points_control(3,1) = 1._pr
  !  !points_control(3,2) = 1._pr

  !  !points_control(4,1) = 1._pr
  !  !points_control(4,2) = 0._pr
  !  points_control(1,1) = 0._pr
  !  points_control(1,2) = 0._pr
  ! 
  !  points_control(2,1) = 1._pr
  !  points_control(2,2) = 1._pr

  !  points_control(3,1) = 2._pr
  !  points_control(3,2) = 2._pr

  !  points_control(4,1) = 3._pr
  !  points_control(4,2) = 3._pr
  !end

  subroutine pointBezier3(points_control,t,px,py)
    implicit none
    real(pr),dimension(:,:),intent(in) :: points_control
    real(pr),intent(in) :: t
    real(pr),intent(out) :: px, py
    real(pr) :: x,y
    real(pr) :: Ax,Ay,Bx,By

    x = (1-t)*(1-t)
    y = t*t

    Ax = (1-t)*x*points_control(1,1) + 3*t*x*points_control(2,1)
    Ay = (1-t)*x*points_control(1,2) + 3*t*x*points_control(2,2)

    Bx = 3*y*(1-t)*points_control(3,1) + y*t*points_control(4,1)
    By = 3*y*(1-t)*points_control(3,2) + y*t*points_control(4,2)

    px = Ax + Bx
    py = Ay + By
  end subroutine pointBezier3


  subroutine reduction1D(points_control,t,N,points_sortie)
    implicit none
    real(pr),intent(in) :: t
    integer,intent(in) :: N
    real(pr),dimension(:),intent(in) :: points_control
    real(pr),dimension(:),intent(out) :: points_sortie
    integer :: l

    do l=1,N-1
       points_sortie(l) = (1-t)*points_control(l) + t*points_control(l+1)
    enddo
  end subroutine reduction1D

  subroutine reduction(points_control,t,N,points_sortie)
    implicit none
    real(pr),intent(in) :: t
    integer,intent(in) :: N
    real(pr),dimension(:,:),intent(in) :: points_control
    real(pr),dimension(:,:),intent(out) :: points_sortie
    integer :: l

    do l=1,N-1
       points_sortie(l,1) = (1-t)*points_control(l,1) + t*points_control(l+1,1)
       points_sortie(l,2) = (1-t)*points_control(l,2) + t*points_control(l+1,2)
    enddo
  end subroutine reduction

  subroutine reduction3D(points_control,t,N,points_sortie)
    implicit none
    real(pr),intent(in) :: t
    integer,intent(in) :: N
    real(pr),dimension(:,:),intent(in) :: points_control
    real(pr),dimension(:,:),intent(out) :: points_sortie
    integer :: l

    do l=1,N-1
       points_sortie(l,1) = (1-t)*points_control(l,1) + t*points_control(l+1,1)
       points_sortie(l,2) = (1-t)*points_control(l,2) + t*points_control(l+1,2)
       points_sortie(l,3) = (1-t)*points_control(l,3) + t*points_control(l+1,3)
    enddo
  end subroutine reduction3D


  subroutine pointsBezierN1D(points_control,t,P)
    implicit none
    real(pr),intent(in) :: t
    real(pr),dimension(:),intent(in) :: points_control
    real(pr),intent(out) :: P
    real(pr),dimension(size(points_control)) :: tab
    integer :: N,l

    N = size(points_control)
    do l=1,N
       tab(l) = points_control(l)
    enddo
    do while (N>1)
       call reduction1D(tab,t,N,tab)
       N = N-1
    enddo

    P = tab(1)
  end subroutine pointsBezierN1D

  subroutine pointsBezierN(points_control,t,Px,Py)
    implicit none
    real(pr),intent(in) :: t
    real(pr),dimension(:,:),intent(in) :: points_control
    real(pr),intent(out) :: Px,Py
    real(pr),dimension(size(points_control,1),2) :: tab
    integer :: N,l

    N = size(points_control,1)
    do l=1,N
       tab(l,1) = points_control(l,1)
       tab(l,2) = points_control(l,2)
    enddo
    do while (N>1)
       call reduction(tab,t,N,tab)
       N = N-1
    enddo

    Px = tab(1,1)
    Py = tab(1,2)
  end subroutine pointsBezierN

  subroutine pointsBezierN3D(points_control,t,Px,Py,Pz)
    implicit none
    real(pr),intent(in) :: t
    real(pr),dimension(:,:),intent(in) :: points_control
    real(pr),intent(out) :: Px,Py,Pz
    real(pr),dimension(size(points_control,1),3) :: tab
    integer :: N,l

    N = size(points_control,1)
    do l=1,N
       tab(l,1) = points_control(l,1)
       tab(l,2) = points_control(l,2)
       tab(l,3) = points_control(l,3)
    enddo
    do while (N>1)
       call reduction3D(tab,t,N,tab)
       N = N-1
    enddo

    Px = tab(1,1)
    Py = tab(1,2)
    Pz = tab(1,3)
  end subroutine pointsBezierN3D


  subroutine courbeBezierN(points_courbe,t,Px,Py)
    implicit none
    real(pr),intent(in) :: t
    real(pr),dimension(:,:),intent(in) :: points_courbe
    real(pr),intent(out) :: Px,Py

    if (t<0._pr) then
       Px = (1-t)*points_courbe(1,1) + t*points_courbe(2,1)
       Py = (1-t)*points_courbe(1,2) + t*points_courbe(2,2)
    else if (t>1._pr) then
       !     Px = (1-t)*points_courbe(size(points_courbe,1)-1,1) + t*points_courbe(size(points_courbe,1),1)
       !     Py = (1-t)*points_courbe(size(points_courbe,1)-1,2) + t*points_courbe(size(points_courbe,1),2)
       Px = (1-t)*points_courbe(size(points_courbe,1)-1,1) + t*points_courbe(size(points_courbe,1),1)
       Py = (1-t)*points_courbe(size(points_courbe,1)-1,2) + t*points_courbe(size(points_courbe,1),2)
    else
       write(*,*) "error problem with t-value ",t
    endif
  end subroutine courbeBezierN

  subroutine courbeBezierN3D(points_courbe,t,Px,Py,Pz)
    implicit none
    real(pr),intent(in) :: t
    real(pr),dimension(:,:),intent(in) :: points_courbe
    real(pr),intent(out) :: Px,Py,Pz

    if (t<0._pr) then
       Px = (1-t)*points_courbe(1,1) + t*points_courbe(2,1)
       Py = (1-t)*points_courbe(1,2) + t*points_courbe(2,2)
       Pz = (1-t)*points_courbe(1,3) + t*points_courbe(2,3)
    else if (t>1._pr) then
       Px = (1-t)*points_courbe(size(points_courbe,1)-1,1) + t*points_courbe(size(points_courbe,1),1)
       Py = (1-t)*points_courbe(size(points_courbe,1)-1,2) + t*points_courbe(size(points_courbe,1),2)
       Pz = (1-t)*points_courbe(size(points_courbe,1)-1,3) + t*points_courbe(size(points_courbe,1),3)
       !     Px = (1-t)*points_courbe(size(points_courbe,1)-90,1) + t*points_courbe(size(points_courbe,1),1)
       !     Py = (1-t)*points_courbe(size(points_courbe,1)-90,2) + t*points_courbe(size(points_courbe,1),2)
       !     Pz = (1-t)*points_courbe(size(points_courbe,1)-90,3) + t*points_courbe(size(points_courbe,1),3)
    else
       write(*,*) "error problem with t-value ",t
    endif
  end subroutine courbeBezierN3D


  subroutine poly2(pointA,pointB,pointC,t,tB,outputP)
    implicit none
    real(pr),intent(in) :: pointA,pointB,pointC,t,tB
    real(pr),intent(out) :: outputP
    real(pr) :: a

    a = (pointC-pointA)/(1._pr-tB) - (pointB-pointA)/((1._pr-tB)*tB)

    outputP = (1._pr-t)*pointA + t*pointC - t*(1._pr-t)*a
  end subroutine poly2

  ! subroutine splineN(points_control,t,Px,Py)
  !   implicit none
  !   real(pr),intent(in) :: t
  !   real(pr),dimension(:,:),intent(in) :: points_control
  !   real(pr),intent(out) :: Px,Py
  !   real(pr),dimension(size(points_control,1),2) :: tab
  !   integer :: N,l,d,m,s,h,i,j,k,kk,r
  !   real(pr),dimension(size(points_control,1)+3+1) :: u  !N+d
  !   real(pr),dimension(size(points_control,1),size(points_control,1),size(points_control,2)) :: Pt
  !   real(pr) :: uu,alpha
  !
  !   N = size(points_control,1)
  !!   do l=1,N
  !!     tab(l,1) = points_control(l,1)
  !!     tab(l,2) = points_control(l,2)
  !!   enddo
  !!   do while (N>1)
  !!     call reduction(tab,t,N,tab)
  !!     N = N-1
  !!   enddo
  !!
  !!   Px = tab(1,1)
  !!   Py = tab(1,2)
  !
  !              !N=n+1 is the number of sampling points  
  !    d=3       !d is the approximation order
  !    m=N-1+d+1 !the number of points needed by De Boor algorithm
  !    s=0       !The possible multiplicity order 
  !    h=d-s
  !    !du=1./(Ns-1)
  !    !do i=1,m+1
  !    do i=0,m
  !      !if (i-1<d) u(i) = 0._pr
  !      !if ((i-1>=d).and.(i-1<=(m-d))) u(i) = (i-1-d)/(1._pr*(N-1)-1)
  !      !if (i-1>m-d) u(i) = (N-1-d+1)/(1._pr*(N-1)-1)
  !      !if (i<d) u(i+1) = 0._pr
  !      !if ((i>=d).and.(i<=(m-d))) u(i+1) = (i-d)/(1._pr*(N-1)-1)
  !      !if (i>m-d) u(i+1) = (N-1-d+1)/(1._pr*(N-1)-1)
  !      if (i<d) u(i+1) = 0._pr
  !      if ((i>=d).and.(i<=(m-d))) u(i+1) = (i-d+1)/(1._pr*(N+1-d+1)) ! /(1._pr*(N-1)-1)
  !      if (i>m-d) u(i+1) = (N+1-d+1)/(1._pr*(N+1-d+1)) !(N-d)/(1._pr*(N-1)-1)
  !      !if(i<d){u[i]=0;}
  !      !if(i>=d && i<=(m-d)){u[i]=(i-d)/(1.*n-1);}
  !      !if(i>m-d){u[i]=(n-d+1)/(1.*n-1);}
  !      write(*,*) "u(i)  ",i," ",u(i+1)
  !    enddo
  !    uu=t !0._pr
  !    kk=1
  !
  !    !x(1)=0._pr
  !    !xl=0._pr
  !    !do while (xl<=1)
  !    !do while (uu<t)
  !      write(*,*) "iteration  ",k
  !      do j=1,m
  !        if ((uu>=u(j)).and.(uu<u(j+1))) k=j
  !      enddo
  !      write(*,*) "checl1 ",k
  !
  !      do i=1,N
  !        Pt(i,1,1) = points_control(i,1)
  !        Pt(i,1,2) = points_control(i,2)
  !        !Pt[i][0][1]=P[i][1]
  !        !Pt[i][0][2]=P[i][2]
  !      enddo
  !      write(*,*) "checl2"
  !    
  !      do r=1,h
  !        do i=k-d+r,k-s 
  !          write(*,*) "check I  ",i
  !          alpha = (uu-u(i))/(u(i+d-r+1)-u(i))
  !          if (abs(u(i+d-r+1)-u(i))<1.e-6) write(*,*) "AIEAIEAIE ",u(i+d-r+1)," ",u(i),"  ",i," ",r,"  ",i+d-r+1
  !          Pt(i+1,r+1,1) = (1-alpha)*Pt(i,r,1) + alpha*Pt(i+1,r,1)
  !          Pt(i+1,r+1,2) = (1-alpha)*Pt(i,r,2) + alpha*Pt(i+1,r,2)
  !          if ((r==d).and.(i==k)) write(*,*) "points_control  ",Pt(i+1,r+1,1)," ",Pt(i+1,r+1,2)
  !          !alpha = (uu-u(i))/(u(i+d-r+1)-u(i))
  !          !Pt(i,r,1) = (1-alpha)*Pt(i-1,r-1,1) + alpha*Pt(i,r-1,1)
  !          !Pt(i,r,2) = (1-alpha)*Pt(i-1,r-1,2) + alpha*Pt(i,r-1,2)
  !          !alpha=(uu-u[i])/(u[i+d-r+1]-u[i]);
  !          !Pt[i][r][1]=(1-alpha)*Pt[i-1][r-1][1]+alpha*Pt[i][r-1][1];
  !          !Pt[i][r][2]=(1-alpha)*Pt[i-1][r-1][2]+alpha*Pt[i][r-1][2];
  !        enddo
  !      enddo
  !      write(*,*) "au temps t  ",t,"  ",Pt(k+1,d+1,1)," ",Pt(k+1,d+1,2)&
  !,"    ",points_control(1,1)," ",points_control(1,2)," ",points_control(N,1)," ",points_control(N,2)
  !
  !      !x[kk]=Pt[k-s][d-s][1];
  !      !y[kk]=Pt[k-s][d-s][2];
  !      !xl=x[kk];
  !      !kk++;
  !      Px = Pt(k+1-s,d+1-s,1)
  !      Py = Pt(k+1-s,d+1-s,2)
  !      kk = kk + 1
  !    !  uu=uu+du;
  !    !enddo
  !
  ! end
  subroutine splineN(points_control,t,Px,Py)
    implicit none
    real(pr),intent(in) :: t
    real(pr),dimension(:,:),intent(in) :: points_control
    real(pr),intent(out) :: Px,Py
    integer :: N,i,j,k
    real(pr),dimension(size(points_control,1)) :: u  !N+d
    real(pr),dimension(size(points_control,1)) :: a,c,l,z
    real(pr),dimension(size(points_control,1)-1) :: b,d,mu,h
    real(pr),dimension(size(points_control,1)-1-1) :: alpha

    N = size(points_control,1)
    do i=1,N
       u(i) = (i-1)*1._pr/(1._pr*(N-1)) ! /(1._pr*(N-1)-1)
    enddo

    do i=1,N
       a(i) = points_control(i,2)
    enddo
    do i=1,N-1
       h(i) = u(i+1)-u(i)
    enddo
    do i=2,N-1
       alpha(i) = 3._pr*(a(i+1)-a(i))/h(i) - 3._pr*(a(i)-a(i-1))/(h(i-1))
    enddo
    l(1) = 1._pr
    mu(1) = 0._pr
    z(1) = 0._pr
    do i=2,N-1
       l(i) = 2._pr*(u(i+1)-u(i-1)) - h(i-1)*mu(i-1)
       mu(i) = h(i)/l(i)
       z(i) = (alpha(i) - h(i-1)*z(i-1))/(l(i))
    enddo
    l(N) = 1._pr
    z(N) = 0._pr
    c(N) = 0._pr
    do i=N-1,1,-1
       c(i) = z(i) - mu(i)*c(i+1)
       b(i) = (a(i+1)-a(i))/h(i) - (h(i)*(c(i+1)+2._pr*c(i)))/3._pr
       d(i) = (c(i+1)-c(i))/(3._pr*h(i))
    enddo
    do j=1,N
       if ((t>=u(j)).and.(t<u(j+1))) k=j
    enddo
    Py = a(k) + b(k)*(t-u(k)) + c(k)*(t-u(k))**2 + d(k)*(t-u(k))**3

    do i=1,N
       a(i) = points_control(i,1)
    enddo
    do i=1,N-1
       h(i) = u(i+1)-u(i)
    enddo
    do i=2,N-1
       alpha(i) = 3._pr*(a(i+1)-a(i))/h(i) - 3._pr*(a(i)-a(i-1))/(h(i-1))
    enddo
    l(1) = 1._pr
    mu(1) = 0._pr
    z(1) = 0._pr
    do i=2,N-1
       l(i) = 2._pr*(u(i+1)-u(i-1)) - h(i-1)*mu(i-1)
       mu(i) = h(i)/l(i)
       z(i) = (alpha(i) - h(i-1)*z(i-1))/(l(i))
    enddo
    l(N) = 1._pr
    z(N) = 0._pr
    c(N) = 0._pr
    do i=N-1,1,-1
       c(i) = z(i) - mu(i)*c(i+1)
       b(i) = (a(i+1)-a(i))/h(i) - (h(i)*(c(i+1)+2._pr*c(i)))/3._pr
       d(i) = (c(i+1)-c(i))/(3._pr*h(i))
    enddo
    do j=1,N
       if ((t>=u(j)).and.(t<u(j+1))) k=j
    enddo
    Px = a(k) + b(k)*(t-u(k)) + c(k)*(t-u(k))**2 + d(k)*(t-u(k))**3
  end subroutine splineN


  subroutine splineN3D(points_control,t,Px,Py,Pz)
    implicit none
    real(pr),intent(in) :: t
    real(pr),dimension(:,:),intent(in) :: points_control
    real(pr),intent(out) :: Px,Py,Pz
    integer :: N,i,j,k
    real(pr),dimension(size(points_control,1)) :: u  !N+d
    real(pr),dimension(size(points_control,1)) :: a,c,l,z
    real(pr),dimension(size(points_control,1)-1) :: b,d,mu,h
    real(pr),dimension(size(points_control,1)-1-1) :: alpha

    N = size(points_control,1)
    do i=1,N
       u(i) = (i-1)*1._pr/(1._pr*(N-1)) ! /(1._pr*(N-1)-1)
    enddo

    do i=1,N
       a(i) = points_control(i,2)
    enddo
    do i=1,N-1
       h(i) = u(i+1)-u(i)
    enddo
    do i=2,N-1
       alpha(i) = 3._pr*(a(i+1)-a(i))/h(i) - 3._pr*(a(i)-a(i-1))/(h(i-1))
    enddo
    l(1) = 1._pr
    mu(1) = 0._pr
    z(1) = 0._pr
    do i=2,N-1
       l(i) = 2._pr*(u(i+1)-u(i-1)) - h(i-1)*mu(i-1)
       mu(i) = h(i)/l(i)
       z(i) = (alpha(i) - h(i-1)*z(i-1))/(l(i))
    enddo
    l(N) = 1._pr
    z(N) = 0._pr
    c(N) = 0._pr
    do i=N-1,1,-1
       c(i) = z(i) - mu(i)*c(i+1)
       b(i) = (a(i+1)-a(i))/h(i) - (h(i)*(c(i+1)+2._pr*c(i)))/3._pr
       d(i) = (c(i+1)-c(i))/(3._pr*h(i))
    enddo
    do j=1,N
       if ((t>=u(j)).and.(t<u(j+1))) k=j
    enddo
    Py = a(k) + b(k)*(t-u(k)) + c(k)*(t-u(k))**2 + d(k)*(t-u(k))**3

    do i=1,N
       a(i) = points_control(i,1)
    enddo
    do i=1,N-1
       h(i) = u(i+1)-u(i)
    enddo
    do i=2,N-1
       alpha(i) = 3._pr*(a(i+1)-a(i))/h(i) - 3._pr*(a(i)-a(i-1))/(h(i-1))
    enddo
    l(1) = 1._pr
    mu(1) = 0._pr
    z(1) = 0._pr
    do i=2,N-1
       l(i) = 2._pr*(u(i+1)-u(i-1)) - h(i-1)*mu(i-1)
       mu(i) = h(i)/l(i)
       z(i) = (alpha(i) - h(i-1)*z(i-1))/(l(i))
    enddo
    l(N) = 1._pr
    z(N) = 0._pr
    c(N) = 0._pr
    do i=N-1,1,-1
       c(i) = z(i) - mu(i)*c(i+1)
       b(i) = (a(i+1)-a(i))/h(i) - (h(i)*(c(i+1)+2._pr*c(i)))/3._pr
       d(i) = (c(i+1)-c(i))/(3._pr*h(i))
    enddo
    do j=1,N
       if ((t>=u(j)).and.(t<u(j+1))) k=j
    enddo
    Px = a(k) + b(k)*(t-u(k)) + c(k)*(t-u(k))**2 + d(k)*(t-u(k))**3

    do i=1,N
       a(i) = points_control(i,3)
    enddo
    do i=1,N-1
       h(i) = u(i+1)-u(i)
    enddo
    do i=2,N-1
       alpha(i) = 3._pr*(a(i+1)-a(i))/h(i) - 3._pr*(a(i)-a(i-1))/(h(i-1))
    enddo
    l(1) = 1._pr
    mu(1) = 0._pr
    z(1) = 0._pr
    do i=2,N-1
       l(i) = 2._pr*(u(i+1)-u(i-1)) - h(i-1)*mu(i-1)
       mu(i) = h(i)/l(i)
       z(i) = (alpha(i) - h(i-1)*z(i-1))/(l(i))
    enddo
    l(N) = 1._pr
    z(N) = 0._pr
    c(N) = 0._pr
    do i=N-1,1,-1
       c(i) = z(i) - mu(i)*c(i+1)
       b(i) = (a(i+1)-a(i))/h(i) - (h(i)*(c(i+1)+2._pr*c(i)))/3._pr
       d(i) = (c(i+1)-c(i))/(3._pr*h(i))
    enddo
    do j=1,N
       if ((t>=u(j)).and.(t<u(j+1))) k=j
    enddo
    Pz = a(k) + b(k)*(t-u(k)) + c(k)*(t-u(k))**2 + d(k)*(t-u(k))**3
  end subroutine splineN3D

  subroutine filterskel(skel,rhoSlices)
    implicit none
    real(pr),dimension(:,:),intent(in) :: rhoSlices
    integer,dimension(:,:),intent(inout) :: skel
    integer,dimension(size(skel,1),size(skel,2)) :: skeltmp,bool
    integer,dimension(3,3) :: filt,filt2,filt3,filt4,filt5,filt6,filt7,filt8,filt9,filt1,filt10,filt14
    integer :: i,j,ll

    bool = 0

    filt = -1
    filt(2,2) = 1
    filt(2,3) = 1
    filt(3,1) = 1
    filt(3,2) = 1
    filt(1,2) = 0
    filt(1,3) = 0
    filt(3,3) = 0

    filt2 = -1
    filt2(2,2) = 1
    filt2(2,3) = 1
    filt2(3,1) = 1
    filt2(3,2) = 1
    filt2(1,2) = 0
    filt2(1,3) = 0
    filt2(3,3) = 1

    filt3 = -1
    filt3(2,2) = 1
    filt3(2,3) = 1
    filt3(2,1) = 1
    filt3(3,2) = 1
    filt3(1,2) = 0
    filt3(1,3) = 0
    filt3(3,3) = 1

!    filt4 = -1
!    filt4(2,2) = 1
!    filt4(2,3) = 1
!    filt4(2,1) = 1
!    filt4(1,2) = 1
!    filt4(1,1) = 0
!    filt4(1,3) = 0

    filt4(1,2) = 0
    filt4(1,1) = 0
    filt4(1,3) = 0
    filt4(2,2) = 1
    filt4(2,3) = 0
    filt4(2,1) = 0
    filt4(3,2) = 1
    filt4(3,1) = 1
    filt4(3,3) = 1
    filt14(1,2) = 0
    filt14(1,1) = 0
    filt14(1,3) = 1
    filt14(2,2) = 1
    filt14(2,3) = 1
    filt14(2,1) = 1
    filt14(3,2) = 0
    filt14(3,1) = 0
    filt14(3,3) = 1

    filt5 = -1
    filt5(2,2) = 1
    filt5(2,3) = 1
    filt5(3,1) = 1
    filt5(3,2) = 1
    filt5(1,3) = 1
    filt5(1,2) = 0
    filt5(3,3) = 0

    filt6 = -1
    filt6(2,2) = 1
    filt6(2,3) = 1
    filt6(1,3) = 1
    filt6(1,2) = 0
    filt6(3,2) = 0
    filt6(3,3) = 0

    filt7 = -1
    filt7(2,2) = 1
    filt7(2,3) = 1
    filt7(1,3) = 1
    filt7(1,2) = 1

    filt8 = -1
    filt8(1,1) = 1
    filt8(2,2) = 1
    filt8(2,3) = 1
    filt8(3,2) = 1
    filt8(3,3) = 1
    filt8(1,2) = 0
    filt8(1,3) = 0

    filt9 = -1
    filt9(1,1) = 1
    filt9(2,2) = 1
    filt9(3,3) = 1
    filt9(1,3) = 1
    filt9(1,2) = 0
    filt9(2,3) = 0
    filt9(2,1) = 0
    filt9(3,1) = 0
    filt9(3,2) = 0

    filt1 = -1
    filt1(1,3) = 1
    filt1(2,3) = 1
    filt1(3,1) = 1
    filt1(3,2) = 1
    filt1(2,2) = 0
    filt1(1,2) = 0
    filt1(3,3) = 0

    filt10 = -1
    filt10(2,2) = 1
    filt10(2,3) = 1
    filt10(3,2) = 1
    filt10(1,2) = 0
    filt10(1,3) = 0
    filt10(3,3) = 0
    filt10(1,1) = 1
    filt10(2,1) = 0
    filt10(3,1) = 0


    skeltmp = skel
!    do ll=1,2
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt2(2,2)).and.(skel(i,j+1)==filt2(2,3)).and.(skel(i-1,j-1)==filt2(3,1)).and.(skel(i-1,j)==filt2(3,2))&
        .and.(skel(i-1,j+1)==filt2(3,3)).and.(skel(i+1,j)==filt2(1,2)).and.(skel(i+1,j+1)==filt2(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i-1,j)) then
            skel(i,j) = 0
            bool(i-1,j) = 1
          else
            skel(i-1,j) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i,j+1)<rhoSlices(i-1,j+1)) then
              skel(i,j+1) = 0
              bool(i-1,j+1) = 1
            else
              skel(i-1,j+1) = 0
              bool(i,j+1) = 1
            endif
          if (skel(i+1,j+2)==1) skel(i,j+1) = 1
          if (skel(i-2,j+2)==1) skel(i-1,j+1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt2(2,2)).and.(skel(i,j-1)==filt2(2,3)).and.(skel(i-1,j+1)==filt2(3,1)).and.(skel(i-1,j)==filt2(3,2))&
        .and.(skel(i-1,j-1)==filt2(3,3)).and.(skel(i+1,j)==filt2(1,2)).and.(skel(i+1,j-1)==filt2(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i-1,j)) then
            skel(i,j) = 0
            bool(i-1,j) = 1
          else
            skel(i-1,j) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i,j-1)<rhoSlices(i-1,j-1)) then
              skel(i,j-1) = 0
              bool(i-1,j-1) = 1
            else
              skel(i-1,j-1) = 0
              bool(i,j-1) = 1
            endif
          if (skel(i+1,j-2)==1) skel(i,j-1) = 1
          if (skel(i-2,j-2)==1) skel(i-1,j-1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt2(2,2)).and.(skel(i,j+1)==filt2(2,3)).and.(skel(i+1,j-1)==filt2(3,1)).and.(skel(i+1,j)==filt2(3,2))&
        .and.(skel(i+1,j+1)==filt2(3,3)).and.(skel(i-1,j)==filt2(1,2)).and.(skel(i-1,j+1)==filt2(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i+1,j)) then
            skel(i,j) = 0
            bool(i+1,j) = 1
          else
            skel(i+1,j) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i,j+1)<rhoSlices(i+1,j+1)) then
              skel(i,j+1) = 0
              bool(i+1,j+1) = 1
            else
              skel(i+1,j+1) = 0
              bool(i,j+1) = 1
            endif
          if (skel(i-1,j+2)==1) skel(i,j+1) = 1
          if (skel(i+2,j+2)==1) skel(i+1,j+1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt2(2,2)).and.(skel(i,j-1)==filt2(2,3)).and.(skel(i+1,j+1)==filt2(3,1)).and.(skel(i+1,j)==filt2(3,2))&
        .and.(skel(i+1,j-1)==filt2(3,3)).and.(skel(i-1,j)==filt2(1,2)).and.(skel(i-1,j-1)==filt2(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i+1,j)) then
            skel(i,j) = 0
            bool(i+1,j) = 1
          else
            skel(i+1,j) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i,j-1)<rhoSlices(i+1,j-1)) then
              skel(i,j-1) = 0
              bool(i+1,j-1) = 1
            else
              skel(i+1,j-1) = 0
              bool(i,j-1) = 1
            endif
          if (skel(i-1,j-2)==1) skel(i,j-1) = 1
          if (skel(i+2,j-2)==1) skel(i+1,j-1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2

        if ((skel(i,j)==filt2(2,2)).and.(skel(i+1,j)==filt2(2,3)).and.(skel(i-1,j-1)==filt2(3,1)).and.(skel(i,j-1)==filt2(3,2))&
        .and.(skel(i+1,j-1)==filt2(3,3)).and.(skel(i,j+1)==filt2(1,2)).and.(skel(i+1,j+1)==filt2(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j-1)) then
            skel(i,j) = 0
            bool(i,j-1) = 1
          else
            skel(i,j-1) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i+1,j)<rhoSlices(i+1,j-1)) then
              skel(i+1,j) = 0
              bool(i+1,j-1) = 1
            else
              skel(i+1,j-1) = 0
              bool(i+1,j) = 1
            endif
          if (skel(i+2,j+1)==1) skel(i+1,j) = 1
          if (skel(i+2,j-2)==1) skel(i+1,j-1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt2(2,2)).and.(skel(i-1,j)==filt2(2,3)).and.(skel(i+1,j-1)==filt2(3,1)).and.(skel(i,j-1)==filt2(3,2))&
        .and.(skel(i-1,j-1)==filt2(3,3)).and.(skel(i,j+1)==filt2(1,2)).and.(skel(i-1,j+1)==filt2(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j-1)) then
            skel(i,j) = 0
            bool(i,j-1) = 1
          else
            skel(i,j-1) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i-1,j)<rhoSlices(i-1,j-1)) then
              skel(i-1,j) = 0
              bool(i-1,j-1) = 1
            else
              skel(i-1,j-1) = 0
              bool(i-1,j) = 1
            endif
          if (skel(i-2,j+1)==1) skel(i-1,j) = 1
          if (skel(i-2,j-2)==1) skel(i-1,j-1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt2(2,2)).and.(skel(i+1,j)==filt2(2,3)).and.(skel(i-1,j+1)==filt2(3,1)).and.(skel(i,j-1)==filt2(3,2))&
        .and.(skel(i+1,j+1)==filt2(3,3)).and.(skel(i,j+1)==filt2(1,2)).and.(skel(i+1,j-1)==filt2(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j+1)) then
            skel(i,j) = 0
            bool(i,j+1) = 1
          else
            skel(i,j+1) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i+1,j)<rhoSlices(i+1,j+1)) then
              skel(i+1,j) = 0
              bool(i+1,j+1) = 1
            else
              skel(i+1,j+1) = 0
              bool(i+1,j) = 1
            endif
          if (skel(i+2,j-1)==1) skel(i+1,j) = 1
          if (skel(i+2,j+2)==1) skel(i+1,j+1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt2(2,2)).and.(skel(i-1,j)==filt2(2,3)).and.(skel(i+1,j+1)==filt2(3,1)).and.(skel(i,j-1)==filt2(3,2))&
        .and.(skel(i-1,j+1)==filt2(3,3)).and.(skel(i,j+1)==filt2(1,2)).and.(skel(i-1,j-1)==filt2(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j+1)) then
            skel(i,j) = 0
            bool(i,j+1) = 1
          else
            skel(i,j+1) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i-1,j)<rhoSlices(i-1,j+1)) then
              skel(i-1,j) = 0
              bool(i-1,j+1) = 1
            else
              skel(i-1,j+1) = 0
              bool(i-1,j) = 1
            endif
          if (skel(i-2,j-1)==1) skel(i-1,j) = 1
          if (skel(i-2,j+2)==1) skel(i-1,j+1) = 1
        endif
      enddo
    enddo



    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt3(2,2)).and.(skel(i,j+1)==filt3(2,3)).and.(skel(i,j-1)==filt3(2,1)).and.(skel(i-1,j)==filt3(3,2))&
        .and.(skel(i-1,j+1)==filt3(3,3)).and.(skel(i+1,j)==filt3(1,2)).and.(skel(i+1,j+1)==filt3(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i-1,j)) then
            skel(i,j) = 0
            bool(i-1,j) = 1
          else
            skel(i-1,j) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i,j+1)<rhoSlices(i-1,j+1)) then
              skel(i,j+1) = 0
              bool(i-1,j+1) = 1
            else
              skel(i-1,j+1) = 0
              bool(i,j+1) = 1
            endif
          if (skel(i+1,j+2)==1) skel(i,j+1) = 1
          if (skel(i-2,j+2)==1) skel(i-1,j+1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt3(2,2)).and.(skel(i,j-1)==filt3(2,3)).and.(skel(i,j+1)==filt3(2,1)).and.(skel(i-1,j)==filt3(3,2))&
        .and.(skel(i-1,j-1)==filt3(3,3)).and.(skel(i+1,j)==filt3(1,2)).and.(skel(i+1,j-1)==filt3(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i-1,j)) then
            skel(i,j) = 0
            bool(i-1,j) = 1
          else
            skel(i-1,j) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i,j-1)<rhoSlices(i-1,j-1)) then
              skel(i,j-1) = 0
              bool(i-1,j-1) = 1
            else
              skel(i-1,j-1) = 0
              bool(i,j-1) = 1
            endif
          if (skel(i+1,j-2)==1) skel(i,j-1) = 1
          if (skel(i-2,j-2)==1) skel(i-1,j-1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt3(2,2)).and.(skel(i,j+1)==filt3(2,3)).and.(skel(i,j-1)==filt3(2,1)).and.(skel(i+1,j)==filt3(3,2))&
        .and.(skel(i+1,j+1)==filt3(3,3)).and.(skel(i-1,j)==filt3(1,2)).and.(skel(i-1,j+1)==filt3(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i+1,j)) then
            skel(i,j) = 0
            bool(i+1,j) = 1
          else
            skel(i+1,j) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i,j+1)<rhoSlices(i+1,j+1)) then
              skel(i,j+1) = 0
              bool(i+1,j+1) = 1
            else
              skel(i+1,j+1) = 0
              bool(i,j+1) = 1
            endif
          if (skel(i-1,j+2)==1) skel(i,j+1) = 1
          if (skel(i+2,j+2)==1) skel(i+1,j+1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt3(2,2)).and.(skel(i,j-1)==filt3(2,3)).and.(skel(i,j+1)==filt3(2,1)).and.(skel(i+1,j)==filt3(3,2))&
        .and.(skel(i+1,j-1)==filt3(3,3)).and.(skel(i-1,j)==filt3(1,2)).and.(skel(i-1,j-1)==filt3(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i+1,j)) then
            skel(i,j) = 0
            bool(i+1,j) = 1
          else
            skel(i+1,j) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i,j-1)<rhoSlices(i+1,j-1)) then
              skel(i,j-1) = 0
              bool(i+1,j-1) = 1
            else
              skel(i+1,j-1) = 0
              bool(i,j-1) = 1
            endif
          if (skel(i-1,j-2)==1) skel(i,j-1) = 1
          if (skel(i+2,j-2)==1) skel(i+1,j-1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2

        if ((skel(i,j)==filt3(2,2)).and.(skel(i+1,j)==filt3(2,3)).and.(skel(i-1,j)==filt3(2,1)).and.(skel(i,j-1)==filt3(3,2))&
        .and.(skel(i+1,j-1)==filt3(3,3)).and.(skel(i,j+1)==filt3(1,2)).and.(skel(i+1,j+1)==filt3(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j-1)) then
            skel(i,j) = 0
            bool(i,j-1) = 1
          else
            skel(i,j-1) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i+1,j)<rhoSlices(i+1,j-1)) then
              skel(i+1,j) = 0
              bool(i+1,j-1) = 1
            else
              skel(i+1,j-1) = 0
              bool(i+1,j) = 1
            endif
          if (skel(i+2,j+1)==1) skel(i+1,j) = 1
          if (skel(i+2,j-2)==1) skel(i+1,j-1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt3(2,2)).and.(skel(i-1,j)==filt3(2,3)).and.(skel(i+1,j)==filt3(2,1)).and.(skel(i,j-1)==filt3(3,2))&
        .and.(skel(i-1,j-1)==filt3(3,3)).and.(skel(i,j+1)==filt3(1,2)).and.(skel(i-1,j+1)==filt3(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j-1)) then
            skel(i,j) = 0
            bool(i,j-1) = 1
          else
            skel(i,j-1) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i-1,j)<rhoSlices(i-1,j-1)) then
              skel(i-1,j) = 0
              bool(i-1,j-1) = 1
            else
              skel(i-1,j-1) = 0
              bool(i-1,j) = 1
            endif
          if (skel(i-2,j+1)==1) skel(i-1,j) = 1
          if (skel(i-2,j-2)==1) skel(i-1,j-1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt3(2,2)).and.(skel(i+1,j)==filt3(2,3)).and.(skel(i-1,j)==filt3(2,1)).and.(skel(i,j-1)==filt3(3,2))&
        .and.(skel(i+1,j+1)==filt3(3,3)).and.(skel(i,j+1)==filt3(1,2)).and.(skel(i+1,j-1)==filt3(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j+1)) then
            skel(i,j) = 0
            bool(i,j+1) = 1
          else
            skel(i,j+1) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i+1,j)<rhoSlices(i+1,j+1)) then
              skel(i+1,j) = 0
              bool(i+1,j+1) = 1
            else
              skel(i+1,j+1) = 0
              bool(i+1,j) = 1
            endif
          if (skel(i+2,j-1)==1) skel(i+1,j) = 1
          if (skel(i+2,j+2)==1) skel(i+1,j+1) = 1
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt3(2,2)).and.(skel(i-1,j)==filt3(2,3)).and.(skel(i+1,j)==filt3(2,1)).and.(skel(i,j-1)==filt3(3,2))&
        .and.(skel(i-1,j+1)==filt3(3,3)).and.(skel(i,j+1)==filt3(1,2)).and.(skel(i-1,j-1)==filt3(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j+1)) then
            skel(i,j) = 0
            bool(i,j+1) = 1
          else
            skel(i,j+1) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i-1,j)<rhoSlices(i-1,j+1)) then
              skel(i-1,j) = 0
              bool(i-1,j+1) = 1
            else
              skel(i-1,j+1) = 0
              bool(i-1,j) = 1
            endif
          if (skel(i-2,j-1)==1) skel(i-1,j) = 1
          if (skel(i-2,j+2)==1) skel(i-1,j+1) = 1
        endif
      enddo
    enddo


    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt8(2,2)).and.(skel(i,j+1)==filt8(2,3)).and.(skel(i+1,j-1)==filt8(1,1)).and.(skel(i-1,j)==filt8(3,2))&
        .and.(skel(i-1,j+1)==filt8(3,3)).and.(skel(i+1,j)==filt8(1,2)).and.(skel(i+1,j+1)==filt8(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i-1,j)) then
            skel(i,j) = 0
            bool(i-1,j) = 1
          else
            skel(i-1,j) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i,j+1)<rhoSlices(i-1,j+1)) then
              skel(i,j+1) = 0
              bool(i-1,j+1) = 1
            else
              skel(i-1,j+1) = 0
              bool(i,j+1) = 1
            endif
          if (skel(i+1,j+2)==1) skel(i,j+1) = 1
          if (skel(i-2,j+2)==1) skel(i-1,j+1) = 1
!          if (rhoSlices(i,j+1)<rhoSlices(i-1,j)) then
!            skel(i,j+1) = 0
!          else
!            skel(i-1,j) = 0
!          endif
!           skel(i-1,j+1) = 0
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt8(2,2)).and.(skel(i,j-1)==filt8(2,3)).and.(skel(i+1,j+1)==filt8(1,1)).and.(skel(i-1,j)==filt8(3,2))&
        .and.(skel(i-1,j-1)==filt8(3,3)).and.(skel(i+1,j)==filt8(1,2)).and.(skel(i+1,j-1)==filt8(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i-1,j)) then
            skel(i,j) = 0
            bool(i-1,j) = 1
          else
            skel(i-1,j) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i,j-1)<rhoSlices(i-1,j-1)) then
              skel(i,j-1) = 0
              bool(i-1,j-1) = 1
            else
              skel(i-1,j-1) = 0
              bool(i,j-1) = 1
            endif
          if (skel(i+1,j-2)==1) skel(i,j-1) = 1
          if (skel(i-2,j-2)==1) skel(i-1,j-1) = 1
!          if (rhoSlices(i,j-1)<rhoSlices(i-1,j)) then
!            skel(i,j-1) = 0
!          else
!            skel(i-1,j) = 0
!          endif
!           skel(i-1,j-1) = 0
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt8(2,2)).and.(skel(i,j+1)==filt8(2,3)).and.(skel(i-1,j-1)==filt8(1,1)).and.(skel(i+1,j)==filt8(3,2))&
        .and.(skel(i+1,j+1)==filt8(3,3)).and.(skel(i-1,j)==filt8(1,2)).and.(skel(i-1,j+1)==filt8(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i+1,j)) then
            skel(i,j) = 0
            bool(i+1,j) = 1
          else
            skel(i+1,j) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i,j+1)<rhoSlices(i+1,j+1)) then
              skel(i,j+1) = 0
              bool(i+1,j+1) = 1
            else
              skel(i+1,j+1) = 0
              bool(i,j+1) = 1
            endif
          if (skel(i-1,j+2)==1) skel(i,j+1) = 1
          if (skel(i+2,j+2)==1) skel(i+1,j+1) = 1
!          if (rhoSlices(i,j+1)<rhoSlices(i+1,j)) then
!            skel(i,j+1) = 0
!          else
!            skel(i+1,j) = 0
!          endif
!           skel(i+1,j+1) = 0
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt8(2,2)).and.(skel(i,j-1)==filt8(2,3)).and.(skel(i-1,j+1)==filt8(1,1)).and.(skel(i+1,j)==filt8(3,2))&
        .and.(skel(i+1,j-1)==filt8(3,3)).and.(skel(i-1,j)==filt8(1,2)).and.(skel(i-1,j-1)==filt8(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i+1,j)) then
            skel(i,j) = 0
            bool(i+1,j) = 1
          else
            skel(i+1,j) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i,j-1)<rhoSlices(i+1,j-1)) then
              skel(i,j-1) = 0
              bool(i+1,j-1) = 1
            else
              skel(i+1,j-1) = 0
              bool(i,j-1) = 1
            endif
          if (skel(i-1,j-2)==1) skel(i,j-1) = 1
          if (skel(i+2,j-2)==1) skel(i+1,j-1) = 1
!          if (rhoSlices(i,j-1)<rhoSlices(i+1,j)) then
!            skel(i,j-1) = 0
!          else
!            skel(i+1,j) = 0
!          endif
!           skel(i+1,j-1) = 0
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2

        if ((skel(i,j)==filt8(2,2)).and.(skel(i+1,j)==filt8(2,3)).and.(skel(i-1,j+1)==filt8(1,1)).and.(skel(i,j-1)==filt8(3,2))&
        .and.(skel(i+1,j-1)==filt8(3,3)).and.(skel(i,j+1)==filt8(1,2)).and.(skel(i+1,j+1)==filt8(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j-1)) then
            skel(i,j) = 0
            bool(i,j-1) = 1
          else
            skel(i,j-1) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i+1,j)<rhoSlices(i+1,j-1)) then
              skel(i+1,j) = 0
              bool(i+1,j-1) = 1
            else
              skel(i+1,j-1) = 0
              bool(i+1,j) = 1
            endif
          if (skel(i+2,j+1)==1) skel(i+1,j) = 1
          if (skel(i+2,j-2)==1) skel(i+1,j-1) = 1
!          if (rhoSlices(i+1,j)<rhoSlices(i,j-1)) then
!            skel(i+1,j) = 0
!          else
!            skel(i,j-1) = 0
!          endif
!           skel(i+1,j-1) = 0
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt8(2,2)).and.(skel(i-1,j)==filt8(2,3)).and.(skel(i+1,j+1)==filt8(1,1)).and.(skel(i,j-1)==filt8(3,2))&
        .and.(skel(i-1,j-1)==filt8(3,3)).and.(skel(i,j+1)==filt8(1,2)).and.(skel(i-1,j+1)==filt8(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j-1)) then
            skel(i,j) = 0
            bool(i,j-1) = 1
          else
            skel(i,j-1) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i-1,j)<rhoSlices(i-1,j-1)) then
              skel(i-1,j) = 0
              bool(i-1,j-1) = 1
            else
              skel(i-1,j-1) = 0
              bool(i-1,j) = 1
            endif
          if (skel(i-2,j+1)==1) skel(i-1,j) = 1
          if (skel(i-2,j-2)==1) skel(i-1,j-1) = 1
!          if (rhoSlices(i-1,j)<rhoSlices(i,j-1)) then
!            skel(i-1,j) = 0
!          else
!            skel(i,j-1) = 0
!          endif
!           skel(i-1,j-1) = 0
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt8(2,2)).and.(skel(i+1,j)==filt8(2,3)).and.(skel(i-1,j-1)==filt8(1,1)).and.(skel(i,j+1)==filt8(3,2))&
        .and.(skel(i+1,j+1)==filt8(3,3)).and.(skel(i,j-1)==filt8(1,2)).and.(skel(i+1,j-1)==filt8(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j+1)) then
            skel(i,j) = 0
            bool(i,j+1) = 1
          else
            skel(i,j+1) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i+1,j)<rhoSlices(i+1,j+1)) then
              skel(i+1,j) = 0
              bool(i+1,j+1) = 1
            else
              skel(i+1,j+1) = 0
              bool(i+1,j) = 1
            endif
          if (skel(i+2,j-1)==1) skel(i+1,j) = 1
          if (skel(i+2,j+2)==1) skel(i+1,j+1) = 1
!          if (rhoSlices(i+1,j)<rhoSlices(i,j+1)) then
!            skel(i+1,j) = 0
!          else
!            skel(i,j+1) = 0
!          endif
!           skel(i+1,j+1) = 0
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt8(2,2)).and.(skel(i-1,j)==filt8(2,3)).and.(skel(i+1,j-1)==filt8(1,1)).and.(skel(i,j+1)==filt8(3,2))&
        .and.(skel(i-1,j+1)==filt8(3,3)).and.(skel(i,j-1)==filt8(1,2)).and.(skel(i-1,j-1)==filt8(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j+1)) then
            skel(i,j) = 0
            bool(i,j+1) = 1
          else
            skel(i,j+1) = 0
            bool(i,j) = 1
          endif
            if (rhoSlices(i-1,j)<rhoSlices(i-1,j+1)) then
              skel(i-1,j) = 0
              bool(i-1,j+1) = 1
            else
              skel(i-1,j+1) = 0
              bool(i-1,j) = 1
            endif
          if (skel(i-2,j-1)==1) skel(i-1,j) = 1
          if (skel(i-2,j+2)==1) skel(i-1,j+1) = 1
!          if (rhoSlices(i-1,j)<rhoSlices(i,j+1)) then
!            skel(i-1,j) = 0
!          else
!            skel(i,j+1) = 0
!          endif
!           skel(i-1,j+1) = 0
        endif
      enddo
    enddo


    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt14(2,2)).and.(skel(i,j+1)==filt14(2,3)).and.(skel(i,j-1)==filt14(2,1)).and.(skel(i+1,j)==filt14(1,2))&
        .and.(skel(i-1,j+1)==filt14(3,3)).and.(skel(i-1,j-1)==filt14(3,1)).and.(skel(i-1,j)==filt14(3,2))&
        .and.(skel(i+1,j-1)==filt14(1,1)).and.(skel(i+1,j+1)==filt14(1,3))) then
!            skel(i+1,j+1) = 0
!            skel(i-1,j+1) = 0
            skel(i,j+1) = 0
!            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt14(2,2)).and.(skel(i,j-1)==filt14(2,3)).and.(skel(i,j+1)==filt14(2,1)).and.(skel(i+1,j)==filt14(1,2))&
        .and.(skel(i-1,j-1)==filt14(3,3)).and.(skel(i-1,j+1)==filt14(3,1)).and.(skel(i-1,j)==filt14(3,2))&
        .and.(skel(i+1,j+1)==filt14(1,1)).and.(skel(i+1,j-1)==filt14(1,3))) then
!            skel(i+1,j-1) = 0
!            skel(i-1,j-1) = 0
            skel(i,j-1) = 0
!            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt14(2,2)).and.(skel(i,j+1)==filt14(2,3)).and.(skel(i,j-1)==filt14(2,1)).and.(skel(i-1,j)==filt14(1,2))&
        .and.(skel(i+1,j+1)==filt14(3,3)).and.(skel(i+1,j-1)==filt14(3,1)).and.(skel(i+1,j)==filt14(3,2))&
        .and.(skel(i-1,j-1)==filt14(1,1)).and.(skel(i-1,j+1)==filt14(1,3))) then
!            skel(i-1,j+1) = 0
!            skel(i+1,j+1) = 0
            skel(i,j+1) = 0
!            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt14(2,2)).and.(skel(i,j-1)==filt14(2,3)).and.(skel(i,j+1)==filt14(2,1)).and.(skel(i-1,j)==filt14(1,2))&
        .and.(skel(i+1,j-1)==filt14(3,3)).and.(skel(i+1,j+1)==filt14(3,1)).and.(skel(i+1,j)==filt14(3,2))&
        .and.(skel(i-1,j+1)==filt14(1,1)).and.(skel(i-1,j-1)==filt14(1,3))) then
!            skel(i-1,j-1) = 0
!            skel(i+1,j-1) = 0
            skel(i,j-1) = 0
!            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1

        if ((skel(i,j)==filt14(2,2)).and.(skel(i+1,j)==filt14(2,3)).and.(skel(i-1,j)==filt14(2,1)).and.(skel(i,j+1)==filt14(1,2))&
        .and.(skel(i+1,j-1)==filt14(3,3)).and.(skel(i-1,j-1)==filt14(3,1)).and.(skel(i,j-1)==filt14(3,2))&
        .and.(skel(i-1,j+1)==filt14(1,1)).and.(skel(i+1,j+1)==filt14(1,3))) then
!            skel(i+1,j+1) = 0
!            skel(i+1,j-1) = 0
            skel(i+1,j) = 0
!            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt14(2,2)).and.(skel(i+1,j)==filt14(2,3)).and.(skel(i-1,j)==filt14(2,1)).and.(skel(i,j+1)==filt14(1,2))&
        .and.(skel(i-1,j-1)==filt14(3,3)).and.(skel(i+1,j-1)==filt14(3,1)).and.(skel(i,j-1)==filt14(3,2))&
        .and.(skel(i+1,j+1)==filt14(1,1)).and.(skel(i-1,j+1)==filt14(1,3))) then
!            skel(i-1,j+1) = 0
!            skel(i-1,j-1) = 0
            skel(i-1,j) = 0
!            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt14(2,2)).and.(skel(i+1,j)==filt14(2,3)).and.(skel(i-1,j)==filt14(2,1)).and.(skel(i,j-1)==filt14(1,2))&
        .and.(skel(i+1,j+1)==filt14(3,3)).and.(skel(i-1,j+1)==filt14(3,1)).and.(skel(i,j+1)==filt14(3,2))&
        .and.(skel(i-1,j-1)==filt14(1,1)).and.(skel(i+1,j-1)==filt14(1,3))) then
!            skel(i+1,j-1) = 0
!            skel(i+1,j+1) = 0
            skel(i+1,j) = 0
!            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt14(2,2)).and.(skel(i+1,j)==filt14(2,3)).and.(skel(i-1,j)==filt14(2,1)).and.(skel(i,j-1)==filt14(1,2))&
        .and.(skel(i-1,j+1)==filt14(3,3)).and.(skel(i+1,j+1)==filt14(3,1)).and.(skel(i,j+1)==filt14(3,2))&
        .and.(skel(i+1,j-1)==filt14(1,1)).and.(skel(i-1,j-1)==filt14(1,3))) then
!            skel(i-1,j-1) = 0
!            skel(i-1,j+1) = 0
            skel(i-1,j) = 0
!            skel(i,j) = 0
        endif
      enddo
    enddo


    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt4(2,2)).and.(skel(i,j+1)==filt4(2,3)).and.(skel(i,j-1)==filt4(2,1)).and.(skel(i+1,j)==filt4(1,2))&
        .and.(skel(i-1,j+1)==filt4(3,3)).and.(skel(i-1,j-1)==filt4(3,1)).and.(skel(i-1,j)==filt4(3,2))&
        .and.(skel(i+1,j-1)==filt4(1,1)).and.(skel(i+1,j+1)==filt4(1,3))) then
!            skel(i+1,j) = 0
            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt4(2,2)).and.(skel(i,j-1)==filt4(2,3)).and.(skel(i,j+1)==filt4(2,1)).and.(skel(i+1,j)==filt4(1,2))&
        .and.(skel(i-1,j-1)==filt4(3,3)).and.(skel(i-1,j+1)==filt4(3,1)).and.(skel(i-1,j)==filt4(3,2))&
        .and.(skel(i+1,j+1)==filt4(1,1)).and.(skel(i+1,j-1)==filt4(1,3))) then
            !skel(i+1,j) = 0
            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt4(2,2)).and.(skel(i,j+1)==filt4(2,3)).and.(skel(i,j-1)==filt4(2,1)).and.(skel(i-1,j)==filt4(1,2))&
        .and.(skel(i+1,j+1)==filt4(3,3)).and.(skel(i+1,j-1)==filt4(3,1)).and.(skel(i+1,j)==filt4(3,2))&
        .and.(skel(i-1,j-1)==filt4(1,1)).and.(skel(i-1,j+1)==filt4(1,3))) then
            !skel(i-1,j) = 0
            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt4(2,2)).and.(skel(i,j-1)==filt4(2,3)).and.(skel(i,j+1)==filt4(2,1)).and.(skel(i-1,j)==filt4(1,2))&
        .and.(skel(i+1,j-1)==filt4(3,3)).and.(skel(i+1,j+1)==filt4(3,1)).and.(skel(i+1,j)==filt4(3,2))&
        .and.(skel(i-1,j+1)==filt4(1,1)).and.(skel(i-1,j-1)==filt4(1,3))) then
            !skel(i-1,j) = 0
            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1

        if ((skel(i,j)==filt4(2,2)).and.(skel(i+1,j)==filt4(2,3)).and.(skel(i-1,j)==filt4(2,1)).and.(skel(i,j+1)==filt4(1,2))&
        .and.(skel(i+1,j-1)==filt4(3,3)).and.(skel(i-1,j-1)==filt4(3,1)).and.(skel(i,j-1)==filt4(3,2))&
        .and.(skel(i-1,j+1)==filt4(1,1)).and.(skel(i+1,j+1)==filt4(1,3))) then
            !skel(i,j+1) = 0
            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt4(2,2)).and.(skel(i+1,j)==filt4(2,3)).and.(skel(i-1,j)==filt4(2,1)).and.(skel(i,j+1)==filt4(1,2))&
        .and.(skel(i-1,j-1)==filt4(3,3)).and.(skel(i+1,j-1)==filt4(3,1)).and.(skel(i,j-1)==filt4(3,2))&
        .and.(skel(i+1,j+1)==filt4(1,1)).and.(skel(i-1,j+1)==filt4(1,3))) then
            !skel(i,j+1) = 0
            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt4(2,2)).and.(skel(i+1,j)==filt4(2,3)).and.(skel(i-1,j)==filt4(2,1)).and.(skel(i,j-1)==filt4(1,2))&
        .and.(skel(i+1,j+1)==filt4(3,3)).and.(skel(i-1,j+1)==filt4(3,1)).and.(skel(i,j+1)==filt4(3,2))&
        .and.(skel(i-1,j-1)==filt4(1,1)).and.(skel(i+1,j-1)==filt4(1,3))) then
            !skel(i,j-1) = 0
            skel(i,j) = 0
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt4(2,2)).and.(skel(i+1,j)==filt4(2,3)).and.(skel(i-1,j)==filt4(2,1)).and.(skel(i,j-1)==filt4(1,2))&
        .and.(skel(i-1,j+1)==filt4(3,3)).and.(skel(i+1,j+1)==filt4(3,1)).and.(skel(i,j+1)==filt4(3,2))&
        .and.(skel(i+1,j-1)==filt4(1,1)).and.(skel(i-1,j-1)==filt4(1,3))) then
            !skel(i,j-1) = 0
            skel(i,j) = 0
        endif
      enddo
    enddo


    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt5(2,2)).and.(skel(i,j+1)==filt5(2,3)).and.(skel(i-1,j-1)==filt5(3,1)).and.(skel(i-1,j)==filt5(3,2))&
        .and.(skel(i-1,j+1)==filt5(3,3)).and.(skel(i+1,j)==filt5(1,2)).and.(skel(i+1,j+1)==filt5(1,3))) then
!          if (rhoSlices(i,j)<rhoSlices(i-1,j)) then
!            skel(i,j) = 0
!            bool(i-1,j) = 1
!          else
!            skel(i-1,j) = 0
!            bool(i,j) = 1
!          endif
!          if (rhoSlices(i,j+1)<rhoSlices(i+1,j+1)) then
!            skel(i,j+1) = 0
!            bool(i+1,j+1) = 1
!          else
!            skel(i+1,j+1) = 0
!            bool(i,j+1) = 1
!          endif
!          if (abs(rhoSlices(i,j)-min(rhoSlices(i-1,j),rhoSlices(i,j),rhoSlices(i,j+1)))<1.e-12) then
!            skel(i,j) = 0
!            bool(i-1,j) = 1
!            bool(i,j+1) = 1
!          else if (abs(rhoSlices(i-1,j)-min(rhoSlices(i-1,j),rhoSlices(i,j),rhoSlices(i,j+1)))<1.e-12) then
!            skel(i-1,j) = 0
!            bool(i,j) = 1
!            bool(i,j+1) = 1
!         else
!            skel(i,j+1) = 0
!            bool(i,j) = 1
!            bool(i-1,j) = 1
!          endif
          if (rhoSlices(i-1,j)<rhoSlices(i,j+1)) then
             skel(i-1,j) = 0
             bool(i,j+1) = 1
          else
             skel(i,j+1) = 0
             bool(i-1,j) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt5(2,2)).and.(skel(i,j-1)==filt5(2,3)).and.(skel(i-1,j+1)==filt5(3,1)).and.(skel(i-1,j)==filt5(3,2))&
        .and.(skel(i-1,j-1)==filt5(3,3)).and.(skel(i+1,j)==filt5(1,2)).and.(skel(i+1,j-1)==filt5(1,3))) then
!          if (rhoSlices(i,j)<rhoSlices(i-1,j)) then
!            skel(i,j) = 0
!            bool(i-1,j) = 1
!          else
!            skel(i-1,j) = 0
!            bool(i,j) = 1
!          endif
!          if (rhoSlices(i,j-1)<rhoSlices(i+1,j-1)) then
!            skel(i,j-1) = 0
!            bool(i+1,j-1) = 1
!          else
!            skel(i+1,j-1) = 0
!            bool(i,j-1) = 1
!          endif
!          if (abs(rhoSlices(i,j)-min(rhoSlices(i-1,j),rhoSlices(i,j),rhoSlices(i,j-1)))<1.e-12) then
!            skel(i,j) = 0
!            bool(i+1,j) = 1
!            bool(i,j-1) = 1
!          else if (abs(rhoSlices(i-1,j)-min(rhoSlices(i-1,j),rhoSlices(i,j),rhoSlices(i,j-1)))<1.e-12) then
!            skel(i-1,j) = 0
!            bool(i,j) = 1
!            bool(i,j-1) = 1
!         else
!            skel(i,j-1) = 0
!            bool(i,j) = 1
!            bool(i-1,j) = 1
!          endif
          if (rhoSlices(i-1,j)<rhoSlices(i,j-1)) then
             skel(i-1,j) = 0
             bool(i,j-1) = 1
          else
             skel(i,j-1) = 0
             bool(i-1,j) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt5(2,2)).and.(skel(i,j+1)==filt5(2,3)).and.(skel(i+1,j-1)==filt5(3,1)).and.(skel(i+1,j)==filt5(3,2))&
        .and.(skel(i+1,j+1)==filt5(3,3)).and.(skel(i-1,j)==filt5(1,2)).and.(skel(i-1,j+1)==filt5(1,3))) then
!          if (abs(rhoSlices(i,j)-min(rhoSlices(i+1,j),rhoSlices(i,j),rhoSlices(i,j+1)))<1.e-12) then
!            skel(i,j) = 0
!            bool(i+1,j) = 1
!            bool(i,j+1) = 1
!          else if (abs(rhoSlices(i+1,j)-min(rhoSlices(i+1,j),rhoSlices(i,j),rhoSlices(i,j+1)))<1.e-12) then
!            skel(i+1,j) = 0
!            bool(i,j) = 1
!            bool(i,j+1) = 1
!         else
!            skel(i,j+1) = 0
!            bool(i,j) = 1
!            bool(i+1,j) = 1
!          endif
          if (rhoSlices(i+1,j)<rhoSlices(i,j+1)) then
             skel(i+1,j) = 0
             bool(i,j+1) = 1
          else
             skel(i,j+1) = 0
             bool(i+1,j) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt5(2,2)).and.(skel(i,j-1)==filt5(2,3)).and.(skel(i+1,j+1)==filt5(3,1)).and.(skel(i+1,j)==filt5(3,2))&
        .and.(skel(i+1,j-1)==filt5(3,3)).and.(skel(i-1,j)==filt5(1,2)).and.(skel(i-1,j-1)==filt5(1,3))) then
!          if (abs(rhoSlices(i,j)-min(rhoSlices(i+1,j),rhoSlices(i,j),rhoSlices(i,j-1)))<1.e-12) then
!            skel(i,j) = 0
!            bool(i-1,j) = 1
!            bool(i,j-1) = 1
!          else if (abs(rhoSlices(i+1,j)-min(rhoSlices(i+1,j),rhoSlices(i,j),rhoSlices(i,j-1)))<1.e-12) then
!            skel(i+1,j) = 0
!            bool(i,j) = 1
!            bool(i,j-1) = 1
!         else
!            skel(i,j-1) = 0
!            bool(i,j) = 1
!            bool(i+1,j) = 1
!          endif
          if (rhoSlices(i+1,j)<rhoSlices(i,j-1)) then
             skel(i+1,j) = 0
             bool(i,j-1) = 1
          else
             skel(i,j-1) = 0
             bool(i+1,j) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1

        if ((skel(i,j)==filt5(2,2)).and.(skel(i+1,j)==filt5(2,3)).and.(skel(i-1,j-1)==filt5(3,1)).and.(skel(i,j-1)==filt5(3,2))&
        .and.(skel(i+1,j-1)==filt5(3,3)).and.(skel(i,j+1)==filt5(1,2)).and.(skel(i+1,j+1)==filt5(1,3))) then
!          if (abs(rhoSlices(i,j)-min(rhoSlices(i,j-1),rhoSlices(i,j),rhoSlices(i+1,j)))<1.e-12) then
!            skel(i,j) = 0
!            bool(i,j-1) = 1
!            bool(i+1,j) = 1
!          else if (abs(rhoSlices(i,j-1)-min(rhoSlices(i,j-1),rhoSlices(i,j),rhoSlices(i+1,j)))<1.e-12) then
!            skel(i,j-1) = 0
!            bool(i,j) = 1
!            bool(i+1,j) = 1
!         else
!            skel(i+1,j) = 0
!            bool(i,j) = 1
!            bool(i,j-1) = 1
!          endif
          if (rhoSlices(i,j-1)<rhoSlices(i+1,j)) then
             skel(i,j-1) = 0
             bool(i+1,j) = 1
          else
             skel(i+1,j) = 0
             bool(i,j-1) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt5(2,2)).and.(skel(i-1,j)==filt5(2,3)).and.(skel(i+1,j-1)==filt5(3,1)).and.(skel(i,j-1)==filt5(3,2))&
        .and.(skel(i-1,j-1)==filt5(3,3)).and.(skel(i,j+1)==filt5(1,2)).and.(skel(i-1,j+1)==filt5(1,3))) then
!          if (abs(rhoSlices(i,j)-min(rhoSlices(i,j-1),rhoSlices(i,j),rhoSlices(i-1,j)))<1.e-12) then
!            skel(i,j) = 0
!            bool(i,j-1) = 1
!            bool(i-1,j) = 1
!          else if (abs(rhoSlices(i,j-1)-min(rhoSlices(i,j-1),rhoSlices(i,j),rhoSlices(i-1,j)))<1.e-12) then
!            skel(i,j-1) = 0
!            bool(i,j) = 1
!            bool(i-1,j) = 1
!         else
!            skel(i-1,j) = 0
!            bool(i,j) = 1
!            bool(i,j-1) = 1
!          endif
          if (rhoSlices(i,j-1)<rhoSlices(i-1,j)) then
             skel(i,j-1) = 0
             bool(i-1,j) = 1
          else
             skel(i-1,j) = 0
             bool(i,j-1) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt5(2,2)).and.(skel(i+1,j)==filt5(2,3)).and.(skel(i-1,j+1)==filt5(3,1)).and.(skel(i,j+1)==filt5(3,2))&
        .and.(skel(i+1,j+1)==filt5(3,3)).and.(skel(i,j-1)==filt5(1,2)).and.(skel(i+1,j-1)==filt5(1,3))) then
!          if (abs(rhoSlices(i,j)-min(rhoSlices(i,j+1),rhoSlices(i,j),rhoSlices(i+1,j)))<1.e-12) then
!            skel(i,j) = 0
!            bool(i,j+1) = 1
!            bool(i+1,j) = 1
!          else if (abs(rhoSlices(i,j+1)-min(rhoSlices(i,j+1),rhoSlices(i,j),rhoSlices(i+1,j)))<1.e-12) then
!            skel(i,j+1) = 0
!            bool(i,j) = 1
!            bool(i+1,j) = 1
!         else
!            skel(i+1,j) = 0
!            bool(i,j) = 1
!            bool(i,j+1) = 1
!          endif
          if (rhoSlices(i,j+1)<rhoSlices(i+1,j)) then
             skel(i,j+1) = 0
             bool(i+1,j) = 1
          else
             skel(i+1,j) = 0
             bool(i,j+1) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt5(2,2)).and.(skel(i-1,j)==filt5(2,3)).and.(skel(i+1,j+1)==filt5(3,1)).and.(skel(i,j+1)==filt5(3,2))&
        .and.(skel(i-1,j+1)==filt5(3,3)).and.(skel(i,j-1)==filt5(1,2)).and.(skel(i-1,j-1)==filt5(1,3))) then
!          if (abs(rhoSlices(i,j)-min(rhoSlices(i,j+1),rhoSlices(i,j),rhoSlices(i-1,j)))<1.e-12) then
!            skel(i,j) = 0
!            bool(i,j+1) = 1
!            bool(i-1,j) = 1
!          else if (abs(rhoSlices(i,j+1)-min(rhoSlices(i,j+1),rhoSlices(i,j),rhoSlices(i-1,j)))<1.e-12) then
!            skel(i,j+1) = 0
!            bool(i,j) = 1
!            bool(i-1,j) = 1
!         else
!            skel(i-1,j) = 0
!            bool(i,j) = 1
!            bool(i,j+1) = 1
!          endif
          if (rhoSlices(i,j+1)<rhoSlices(i-1,j)) then
             skel(i,j+1) = 0
             bool(i-1,j) = 1
          else
             skel(i-1,j) = 0
             bool(i,j+1) = 1
          endif
        endif
      enddo
    enddo


    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt(2,2)).and.(skel(i,j+1)==filt(2,3)).and.(skel(i-1,j-1)==filt(3,1)).and.(skel(i-1,j)==filt(3,2))&
        .and.(skel(i-1,j+1)==filt(3,3)).and.(skel(i+1,j)==filt(1,2)).and.(skel(i+1,j+1)==filt(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i-1,j)) then
            skel(i,j) = 0
            bool(i-1,j) = 1
          else
            skel(i-1,j) = 0
            bool(i,j) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt(2,2)).and.(skel(i,j-1)==filt(2,3)).and.(skel(i-1,j+1)==filt(3,1)).and.(skel(i-1,j)==filt(3,2))&
        .and.(skel(i-1,j-1)==filt(3,3)).and.(skel(i+1,j)==filt(1,2)).and.(skel(i+1,j-1)==filt(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i-1,j)) then
            skel(i,j) = 0
            bool(i-1,j) = 1
          else
            skel(i-1,j) = 0
            bool(i,j) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt(2,2)).and.(skel(i,j+1)==filt(2,3)).and.(skel(i+1,j-1)==filt(3,1)).and.(skel(i+1,j)==filt(3,2))&
        .and.(skel(i+1,j+1)==filt(3,3)).and.(skel(i-1,j)==filt(1,2)).and.(skel(i-1,j+1)==filt(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i+1,j)) then
            skel(i,j) = 0
            bool(i+1,j) = 1
          else
            skel(i+1,j) = 0
            bool(i,j) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt(2,2)).and.(skel(i,j-1)==filt(2,3)).and.(skel(i+1,j+1)==filt(3,1)).and.(skel(i+1,j)==filt(3,2))&
        .and.(skel(i+1,j-1)==filt(3,3)).and.(skel(i-1,j)==filt(1,2)).and.(skel(i-1,j-1)==filt(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i+1,j)) then
            skel(i,j) = 0
            bool(i+1,j) = 1
          else
            skel(i+1,j) = 0
            bool(i,j) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1

        if ((skel(i,j)==filt(2,2)).and.(skel(i+1,j)==filt(2,3)).and.(skel(i-1,j-1)==filt(3,1)).and.(skel(i,j-1)==filt(3,2))&
        .and.(skel(i+1,j-1)==filt(3,3)).and.(skel(i,j+1)==filt(1,2)).and.(skel(i+1,j+1)==filt(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j-1)) then
            skel(i,j) = 0
            bool(i,j-1) = 1
          else
            skel(i,j-1) = 0
            bool(i,j) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt(2,2)).and.(skel(i-1,j)==filt(2,3)).and.(skel(i+1,j-1)==filt(3,1)).and.(skel(i,j-1)==filt(3,2))&
        .and.(skel(i-1,j-1)==filt(3,3)).and.(skel(i,j+1)==filt(1,2)).and.(skel(i-1,j+1)==filt(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j-1)) then
            skel(i,j) = 0
            bool(i,j-1) = 1
          else
            skel(i,j-1) = 0
            bool(i,j) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt(2,2)).and.(skel(i+1,j)==filt(2,3)).and.(skel(i-1,j+1)==filt(3,1)).and.(skel(i,j-1)==filt(3,2))&
        .and.(skel(i+1,j+1)==filt(3,3)).and.(skel(i,j+1)==filt(1,2)).and.(skel(i+1,j-1)==filt(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j+1)) then
            skel(i,j) = 0
            bool(i,j+1) = 1
          else
            skel(i,j+1) = 0
            bool(i,j) = 1
          endif
        endif
      enddo
    enddo
    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1
        if ((skel(i,j)==filt(2,2)).and.(skel(i-1,j)==filt(2,3)).and.(skel(i+1,j+1)==filt(3,1)).and.(skel(i,j-1)==filt(3,2))&
        .and.(skel(i-1,j+1)==filt(3,3)).and.(skel(i,j+1)==filt(1,2)).and.(skel(i-1,j-1)==filt(1,3))) then
          if (rhoSlices(i,j)<rhoSlices(i,j+1)) then
            skel(i,j) = 0
            bool(i,j+1) = 1
          else
            skel(i,j+1) = 0
            bool(i,j) = 1
          endif
        endif
      enddo
    enddo


    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt1(2,2)).and.(skel(i,j+1)==filt1(2,3)).and.(skel(i-1,j-1)==filt1(3,1)).and.(skel(i-1,j)==filt1(3,2))&
        .and.((skel(i+1,j+2)+skel(i+2,j+2)+skel(i+2,j+1)+skel(i,j+1)+skel(i+1,j)+skel(i,j)+skel(i,j+2)+skel(i+2,j)==1).or.&
        (skel(i-1,j)+skel(i,j)+skel(i,j-1)+skel(i-2,j-1)+skel(i-1,j-2)+skel(i-2,j-2)+skel(i-2,j)+skel(i,j-2)==1))&
        .and.(skel(i-1,j+1)==filt1(3,3)).and.(skel(i+1,j)==filt1(1,2)).and.(skel(i+1,j+1)==filt1(1,3))) then
          if (skel(i+1,j+2)+skel(i+2,j+2)+skel(i+2,j+1)+skel(i,j+1)+skel(i+1,j)+skel(i,j)+skel(i,j+2)+skel(i+2,j)==1)&
          skel(i+1,j+1) = 0
          if (skel(i-1,j)+skel(i,j)+skel(i,j-1)+skel(i-2,j-1)+skel(i-1,j-2)+skel(i-2,j-2)+skel(i-2,j)+skel(i,j-2)==1)&
          skel(i-1,j-1) = 0
!          if (rhoSlices(i-1,j)<rhoSlices(i,j+1)) then
!             skel(i-1,j) = 0
!             bool(i,j+1) = 1
!          else
!             skel(i,j+1) = 0
!             bool(i-1,j) = 1
!          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt1(2,2)).and.(skel(i,j-1)==filt1(2,3)).and.(skel(i-1,j+1)==filt1(3,1)).and.(skel(i-1,j)==filt1(3,2))&
        .and.((skel(i+1,j-2)+skel(i+2,j-2)+skel(i+2,j-1)+skel(i,j-1)+skel(i+1,j)+skel(i,j)+skel(i,j-2)+skel(i+2,j)==1).or.&
        (skel(i-1,j)+skel(i,j)+skel(i,j+1)+skel(i-2,j+1)+skel(i-1,j+2)+skel(i-2,j+2)+skel(i-2,j)+skel(i,j+2)==1))&
        .and.(skel(i-1,j-1)==filt1(3,3)).and.(skel(i+1,j)==filt1(1,2)).and.(skel(i+1,j-1)==filt1(1,3))) then
          if (skel(i+1,j-2)+skel(i+2,j-2)+skel(i+2,j-1)+skel(i,j-1)+skel(i+1,j)+skel(i,j)+skel(i,j-2)+skel(i+2,j)==1)&
          skel (i+1,j-1) = 0
          if (skel(i-1,j)+skel(i,j)+skel(i,j+1)+skel(i-2,j+1)+skel(i-1,j+2)+skel(i-2,j+2)+skel(i-2,j)+skel(i,j+2)==1)&
          skel(i-1,j+1) = 0
!          if (rhoSlices(i-1,j)<rhoSlices(i,j-1)) then
!             skel(i-1,j) = 0
!             bool(i,j-1) = 1
!          else
!             skel(i,j-1) = 0
!             bool(i-1,j) = 1
!          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt1(2,2)).and.(skel(i,j+1)==filt1(2,3)).and.(skel(i+1,j-1)==filt1(3,1)).and.(skel(i+1,j)==filt1(3,2))&
        .and.((skel(i-1,j+2)+skel(i-2,j+2)+skel(i-2,j+1)+skel(i,j+1)+skel(i-1,j)+skel(i,j)+skel(i,j+2)+skel(i-2,j)==1).or.&
        (skel(i+1,j)+skel(i,j)+skel(i,j-1)+skel(i+2,j-1)+skel(i+1,j-2)+skel(i+2,j-2)+skel(i+2,j)+skel(i,j-2)==1))&
        .and.(skel(i+1,j+1)==filt1(3,3)).and.(skel(i-1,j)==filt1(1,2)).and.(skel(i-1,j+1)==filt1(1,3))) then
          if (skel(i-1,j+2)+skel(i-2,j+2)+skel(i-2,j+1)+skel(i,j+1)+skel(i-1,j)+skel(i,j)+skel(i,j+2)+skel(i-2,j)==1)&
          skel(i-1,j+1) = 0
          if (skel(i+1,j)+skel(i,j)+skel(i,j-1)+skel(i+2,j-1)+skel(i+1,j-2)+skel(i+2,j-2)+skel(i+2,j)+skel(i,j-2)==1)&
          skel(i+1,j-1) = 0
!          if (rhoSlices(i+1,j)<rhoSlices(i,j+1)) then
!             skel(i+1,j) = 0
!             bool(i,j+1) = 1
!          else
!             skel(i,j+1) = 0
!             bool(i+1,j) = 1
!          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt1(2,2)).and.(skel(i,j-1)==filt1(2,3)).and.(skel(i+1,j+1)==filt1(3,1)).and.(skel(i+1,j)==filt1(3,2))&
        .and.((skel(i-1,j-2)+skel(i-2,j-2)+skel(i-2,j-1)+skel(i,j-1)+skel(i-1,j)+skel(i,j)+skel(i,j-2)+skel(i-2,j)==1).or.&
        (skel(i+1,j)+skel(i,j)+skel(i,j+1)+skel(i+2,j+1)+skel(i+1,j+2)+skel(i+2,j+2)+skel(i+2,j)+skel(i,j+2)==1))&
        .and.(skel(i+1,j-1)==filt1(3,3)).and.(skel(i-1,j)==filt1(1,2)).and.(skel(i-1,j-1)==filt1(1,3))) then
          if (skel(i-1,j-2)+skel(i-2,j-2)+skel(i-2,j-1)+skel(i,j-1)+skel(i-1,j)+skel(i,j)+skel(i,j-2)+skel(i-2,j)==1)&
          skel(i-1,j-1) = 0
          if (skel(i+1,j)+skel(i,j)+skel(i,j+1)+skel(i+2,j+1)+skel(i+1,j+2)+skel(i+2,j+2)+skel(i+2,j)+skel(i,j+2)==1)&
          skel(i+1,j+1) = 0
!          if (rhoSlices(i+1,j)<rhoSlices(i,j-1)) then
!             skel(i+1,j) = 0
!             bool(i,j-1) = 1
!          else
!             skel(i,j-1) = 0
!             bool(i+1,j) = 1
!          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2

        if ((skel(i,j)==filt1(2,2)).and.(skel(i+1,j)==filt1(2,3)).and.(skel(i-1,j-1)==filt1(3,1)).and.(skel(i,j-1)==filt1(3,2))&
        .and.((skel(i+2,j+1)+skel(i+2,j+2)+skel(i+1,j+2)+skel(i+1,j)+skel(i,j+1)+skel(i,j)+skel(i+2,j)+skel(i,j+2)==1).or.&
        (skel(i,j-1)+skel(i,j)+skel(i-1,j)+skel(i-1,j-2)+skel(i-2,j-1)+skel(i-2,j-2)+skel(i,j-2)+skel(i-2,j)==1))&
        .and.(skel(i+1,j-1)==filt1(3,3)).and.(skel(i,j+1)==filt1(1,2)).and.(skel(i+1,j+1)==filt1(1,3))) then
          if (skel(i+2,j+1)+skel(i+2,j+2)+skel(i+1,j+2)+skel(i+1,j)+skel(i,j+1)+skel(i,j)+skel(i+2,j)+skel(i,j+2)==1)&
          skel(i+1,j+1) = 0
          if (skel(i,j-1)+skel(i,j)+skel(i-1,j)+skel(i-1,j-2)+skel(i-2,j-1)+skel(i-2,j-2)+skel(i,j-2)+skel(i-2,j)==1)&
          skel(i-1,j-1) = 0
!          if (rhoSlices(i,j-1)<rhoSlices(i+1,j)) then
!             skel(i,j-1) = 0
!             bool(i+1,j) = 1
!          else
!             skel(i+1,j) = 0
!             bool(i,j-1) = 1
!          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt1(2,2)).and.(skel(i-1,j)==filt1(2,3)).and.(skel(i+1,j-1)==filt1(3,1)).and.(skel(i,j-1)==filt1(3,2))&
        .and.((skel(i-2,j+1)+skel(i-2,j+2)+skel(i-1,j+2)+skel(i-1,j)+skel(i,j+1)+skel(i,j)+skel(i-2,j)+skel(i,j+2)==1).or.&
        (skel(i,j-1)+skel(i,j)+skel(i+1,j)+skel(i+1,j-2)+skel(i+2,j-1)+skel(i+2,j-2)+skel(i,j-2)+skel(i+2,j)==1))&
        .and.(skel(i-1,j-1)==filt1(3,3)).and.(skel(i,j+1)==filt1(1,2)).and.(skel(i-1,j+1)==filt1(1,3))) then
          if (skel(i-2,j+1)+skel(i-2,j+2)+skel(i-1,j+2)+skel(i-1,j)+skel(i,j+1)+skel(i,j)+skel(i-2,j)+skel(i,j+2)==1)&
          skel(i-1,j+1) = 0
          if (skel(i,j-1)+skel(i,j)+skel(i+1,j)+skel(i+1,j-2)+skel(i+2,j-1)+skel(i+2,j-2)+skel(i,j-2)+skel(i+2,j)==1)&
          skel(i+1,j-1) = 0
!          if (rhoSlices(i,j-1)<rhoSlices(i-1,j)) then
!             skel(i,j-1) = 0
!             bool(i-1,j) = 1
!          else
!             skel(i-1,j) = 0
!             bool(i,j-1) = 1
!          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt1(2,2)).and.(skel(i+1,j)==filt1(2,3)).and.(skel(i-1,j+1)==filt1(3,1)).and.(skel(i,j+1)==filt1(3,2))&
        .and.((skel(i+2,j-1)+skel(i+2,j-2)+skel(i+1,j-2)+skel(i+1,j)+skel(i,j-1)+skel(i,j)+skel(i+2,j)+skel(i,j-2)==1).or.&
        (skel(i,j+1)+skel(i,j)+skel(i-1,j)+skel(i-1,j+2)+skel(i-2,j+1)+skel(i-2,j+2)+skel(i,j+2)+skel(i-2,j)==1))&
        .and.(skel(i+1,j+1)==filt1(3,3)).and.(skel(i,j-1)==filt1(1,2)).and.(skel(i+1,j-1)==filt1(1,3))) then
          if (skel(i+2,j-1)+skel(i+2,j-2)+skel(i+1,j-2)+skel(i+1,j)+skel(i,j-1)+skel(i,j)+skel(i+2,j)+skel(i,j-2)==1)&
          skel(i+1,j-1) = 0
          if (skel(i,j+1)+skel(i,j)+skel(i-1,j)+skel(i-1,j+2)+skel(i-2,j+1)+skel(i-2,j+2)+skel(i,j+2)+skel(i-2,j)==1)&
          skel(i-1,j+1) = 0
!          if (rhoSlices(i,j+1)<rhoSlices(i+1,j)) then
!             skel(i,j+1) = 0
!             bool(i+1,j) = 1
!          else
!             skel(i+1,j) = 0
!             bool(i,j+1) = 1
!          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt1(2,2)).and.(skel(i-1,j)==filt1(2,3)).and.(skel(i+1,j+1)==filt1(3,1)).and.(skel(i,j+1)==filt1(3,2))&
        .and.((skel(i-2,j-1)+skel(i-2,j-2)+skel(i-1,j-2)+skel(i-1,j)+skel(i,j-1)+skel(i,j)+skel(i-2,j)+skel(i,j-2)==1).or.&
        (skel(i,j+1)+skel(i,j)+skel(i+1,j)+skel(i+1,j+2)+skel(i+2,j+1)+skel(i+2,j+2)+skel(i,j+2)+skel(i+2,j)==1))&
        .and.(skel(i-1,j+1)==filt1(3,3)).and.(skel(i,j-1)==filt1(1,2)).and.(skel(i-1,j-1)==filt1(1,3))) then
          if (skel(i-2,j-1)+skel(i-2,j-2)+skel(i-1,j-2)+skel(i-1,j)+skel(i,j-1)+skel(i,j)+skel(i-2,j)+skel(i,j-2)==1)&
          skel(i-1,j-1) = 0
          if (skel(i,j+1)+skel(i,j)+skel(i+1,j)+skel(i+1,j+2)+skel(i+2,j+1)+skel(i+2,j+2)+skel(i,j+2)+skel(i+2,j)==1)&
          skel(i+1,j+1) = 0
!          if (rhoSlices(i,j+1)<rhoSlices(i-1,j)) then
!             skel(i,j+1) = 0
!             bool(i-1,j) = 1
!          else
!             skel(i-1,j) = 0
!             bool(i,j+1) = 1
!          endif
        endif
      enddo
    enddo

!    do i=3,size(skel,1)-2
!      do j=3,size(skel,2)-2
!        if ((skel(i,j)==filt9(2,2)).and.(skel(i,j+1)==filt9(2,3)).and.(skel(i+1,j-1)==filt9(1,1))&
!!        .and.(skel(i,j-1)==filt9(2,1)).and.(skel(i-1,j-1)==skel(3,1)).and.(skel(i-1,j)==filt9(3,2))&
!        .and.(skel(i-1,j+1)==filt9(3,3)).and.(skel(i+1,j)==filt9(1,2)).and.(skel(i+1,j+1)==filt9(1,3))) then
!          if (skel(i+2,j+2)==0) then
!            skel(i+1,j+1) = 0
!          else
!            skel(i+1,j-1) = 0
!            skel(i-1,j+1) = 0
!          endif
!          if (skel(i+1,j)+skel(i+1,j-2)+skel(i+2,j-1)+skel(i,j-1)+skel(i+2,j)+skel(i+2,j-2)+skel(i,j)+skel(i,j-2)>1)&
!          skel(i+1,j-1) = 1
!          if (skel(i-1,j+2)+skel(i-1,j)+skel(i,j+1)+skel(i-2,j+1)+skel(i,j)+skel(i,j+2)+skel(i-2,j)+skel(i-2,j+2)>1)&
!          skel(i-1,j+1) = 1
!        endif
!      enddo
!    enddo
!    do i=3,size(skel,1)-2
!      do j=3,size(skel,2)-2
!        if ((skel(i,j)==filt9(2,2)).and.(skel(i,j-1)==filt9(2,3)).and.(skel(i+1,j+1)==filt9(1,1))&
!!        .and.(skel(i,j+1)==filt9(2,1)).and.(skel(i-1,j+1)==skel(3,1)).and.(skel(i-1,j)==filt9(3,2))&
!        .and.(skel(i-1,j-1)==filt9(3,3)).and.(skel(i+1,j)==filt9(1,2)).and.(skel(i+1,j-1)==filt9(1,3))) then
!          if (skel(i+2,j-2)==0) then
!            skel(i+1,j-1) = 0
!          else
!            skel(i+1,j+1) = 0
!            skel(i-1,j-1) = 0
!         endif
!          if (skel(i+1,j)+skel(i+1,j+2)+skel(i+2,j+1)+skel(i,j+1)+skel(i+2,j)+skel(i+2,j+2)+skel(i,j)+skel(i,j+2)>1)&
!          skel(i+1,j+1) = 1
!          if (skel(i-1,j-2)+skel(i-1,j)+skel(i,j-1)+skel(i-2,j-1)+skel(i,j)+skel(i,j-2)+skel(i-2,j)+skel(i-2,j-2)>1)&
!          skel(i-1,j-1) = 1
!        endif
!      enddo
!    enddo
!    do i=3,size(skel,1)-2
!      do j=3,size(skel,2)-2
!        if ((skel(i,j)==filt9(2,2)).and.(skel(i,j+1)==filt9(2,3)).and.(skel(i-1,j-1)==filt9(1,1))&
!        .and.(skel(i+1,j+1)==filt9(3,3)).and.(skel(i-1,j)==filt9(1,2)).and.(skel(i-1,j+1)==filt9(1,3))) then
!          if (skel(i-2,j+2)==0) then
!            skel(i-1,j+1) = 0
!          else
!            skel(i-1,j-1) = 0
!            skel(i+1,j+1) = 0
!          endif
!          if (skel(i-1,j)+skel(i-1,j-2)+skel(i-2,j-1)+skel(i,j-1)+skel(i-2,j)+skel(i-2,j-2)+skel(i,j)+skel(i,j-2)>1)&
!          skel(i-1,j-1) = 1
!          if (skel(i+1,j+2)+skel(i+1,j)+skel(i,j+1)+skel(i+2,j+1)+skel(i,j)+skel(i,j+2)+skel(i+2,j)+skel(i+2,j+2)>1)&
!          skel(i+1,j+1) = 1
!        endif
!      enddo
!    enddo
!    do i=3,size(skel,1)-2
!      do j=3,size(skel,2)-2
!        if ((skel(i,j)==filt9(2,2)).and.(skel(i,j-1)==filt9(2,3)).and.(skel(i-1,j+1)==filt9(1,1))&
!        .and.(skel(i+1,j-1)==filt9(3,3)).and.(skel(i-1,j)==filt9(1,2)).and.(skel(i-1,j-1)==filt9(1,3))) then
!          if (skel(i-2,j-2)==0) then
!            skel(i-1,j-1) = 0
!          else
!            skel(i-1,j+1) = 0
!            skel(i+1,j-1) = 0
!          endif
!          if (skel(i-1,j)+skel(i-1,j+2)+skel(i-2,j+1)+skel(i,j+1)+skel(i-2,j)+skel(i-2,j+2)+skel(i,j)+skel(i,j+2)>1)&
!          skel(i-1,j+1) = 1
!          if (skel(i+1,j-2)+skel(i+1,j)+skel(i,j-1)+skel(i+2,j-1)+skel(i,j)+skel(i,j-2)+skel(i+2,j)+skel(i+2,j-2)>1)&
!          skel(i+1,j-1) = 1
!        endif
!      enddo
!    enddo
!    do i=3,size(skel,1)-2
!      do j=3,size(skel,2)-2
!
!        if ((skel(i,j)==filt9(2,2)).and.(skel(i+1,j)==filt9(2,3)).and.(skel(i-1,j+1)==filt9(1,1))&
!!        .and.(skel(i-1,j)==filt9(2,1)).and.(skel(i-1,j-1)==skel(3,1)).and.(skel(i,j-1)==filt9(3,2))&
!        .and.(skel(i+1,j-1)==filt9(3,3)).and.(skel(i,j+1)==filt9(1,2)).and.(skel(i+1,j+1)==filt9(1,3))) then
!          if (skel(i+2,j+2)==0) then
!            skel(i+1,j+1) = 0
!          else
!            skel(i-1,j+1) = 0
!            skel(i+1,j-1) = 0
!          endif
!          if (skel(i,j+1)+skel(i-2,j+1)+skel(i-1,j+2)+skel(i-1,j)+skel(i,j+2)+skel(i-2,j+2)+skel(i,j)+skel(i-2,j)>1)&
!          skel(i-1,j+1) = 1
!          if (skel(i+2,j-1)+skel(i,j-1)+skel(i+1,j)+skel(i+1,j-2)+skel(i,j)+skel(i+2,j)+skel(i,j-2)+skel(i+2,j-2)>1)&
!          skel(i+1,j-1) = 1
!        endif
!      enddo
!    enddo
!    do i=3,size(skel,1)-2
!      do j=3,size(skel,2)-2
!        if ((skel(i,j)==filt9(2,2)).and.(skel(i-1,j)==filt9(2,3)).and.(skel(i+1,j+1)==filt9(1,1))&
!!        .and.(skel(i+1,j)==filt9(2,1)).and.(skel(i+1,j-1)==skel(3,1)).and.(skel(i,j-1)==filt9(3,2))&
!        .and.(skel(i-1,j-1)==filt9(3,3)).and.(skel(i,j+1)==filt9(1,2)).and.(skel(i-1,j+1)==filt9(1,3))) then
!          if (skel(i-2,j+2)==0) then
!            skel(i-1,j+1) = 0
!          else
!            skel(i+1,j+1) = 0
!            skel(i-1,j-1) = 0
!          endif
!          if (skel(i,j+1)+skel(i+2,j+1)+skel(i+1,j+2)+skel(i+1,j)+skel(i,j+2)+skel(i+2,j+2)+skel(i,j)+skel(i+2,j)>1)&
!          skel(i+1,j+1) = 1
!          if (skel(i-2,j-1)+skel(i,j-1)+skel(i-1,j)+skel(i-1,j-2)+skel(i,j)+skel(i-2,j)+skel(i,j-2)+skel(i-2,j-2)>1)&
!          skel(i-1,j-1) = 1
!        endif
!      enddo
!    enddo
!    do i=3,size(skel,1)-2
!      do j=3,size(skel,2)-2
!        if ((skel(i,j)==filt9(2,2)).and.(skel(i+1,j)==filt9(2,3)).and.(skel(i-1,j-1)==filt9(1,1))&
!        .and.(skel(i+1,j+1)==filt9(3,3)).and.(skel(i,j-1)==filt9(1,2)).and.(skel(i+1,j-1)==filt9(1,3))) then
!          if (skel(i+2,j-2)==0) then
!            skel(i+1,j-1) = 0
!          else
!            skel(i-1,j-1) = 0
!            skel(i+1,j+1) = 0
!          endif
!          if (skel(i,j-1)+skel(i-2,j-1)+skel(i-1,j-2)+skel(i-1,j)+skel(i,j-2)+skel(i-2,j-2)+skel(i,j)+skel(i-2,j)>1)&
!          skel(i-1,j-1) = 1
!          if (skel(i+2,j+1)+skel(i,j+1)+skel(i+1,j)+skel(i+1,j+2)+skel(i,j)+skel(i+2,j)+skel(i,j+2)+skel(i+2,j+2)>1)&
!          skel(i+1,j+1) = 1
!        endif
!      enddo
!    enddo
!    do i=3,size(skel,1)-2
!      do j=3,size(skel,2)-2
!        if ((skel(i,j)==filt9(2,2)).and.(skel(i-1,j)==filt9(2,3)).and.(skel(i+1,j-1)==filt9(1,1))&
!        .and.(skel(i-1,j+1)==filt9(3,3)).and.(skel(i,j-1)==filt9(1,2)).and.(skel(i-1,j-1)==filt9(1,3))) then
!          if (skel(i-2,j-2)==0) then
!            skel(i-1,j-1) = 0
!          else
!            skel(i+1,j-1) = 0
!            skel(i-1,j+1) = 0
!          endif
!          if (skel(i,j-1)+skel(i+2,j-1)+skel(i+1,j-2)+skel(i+1,j)+skel(i,j-2)+skel(i+2,j-2)+skel(i,j)+skel(i+2,j)>1)&
!          skel(i+1,j-1) = 1
!          if (skel(i-2,j+1)+skel(i,j+1)+skel(i-1,j)+skel(i-1,j+2)+skel(i,j)+skel(i-2,j)+skel(i,j+2)+skel(i-2,j+2)>1)&
!          skel(i-1,j+1) = 1
!        endif
!      enddo
!    enddo

    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt10(2,2)).and.(skel(i,j+1)==filt10(2,3)).and.(skel(i+1,j-1)==filt10(1,1)).and.(skel(i-1,j)==filt10(3,2))&
        .and.(skel(i-1,j-1)==filt10(3,1)).and.(skel(i,j-1)==filt10(2,1))&
        .and.(skel(i-1,j+1)==filt10(3,3)).and.(skel(i+1,j)==filt10(1,2)).and.(skel(i+1,j+1)==filt10(1,3))) then
          skel(i-1,j+1) = 1  
          skel(i,j+1) = 0
          skel(i-1,j) = 0
          !if (skel(i+1,j+2)==1) then
          if (skel(i+1,j+2)+skel(i,j+2)+skel(i-1,j+2)>0) then
                  skel(i-1,j+1) = 0
                  skel(i,j+1) = 1
          endif
          !if (skel(i-2,j-1)==1) then
          if (skel(i-2,j-1)+skel(i-2,j)+skel(i-2,j+1)>0) then
                  skel(i-1,j+1) = 0
                  skel(i-1,j) = 1
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt10(2,2)).and.(skel(i,j-1)==filt10(2,3)).and.(skel(i+1,j+1)==filt10(1,1)).and.(skel(i-1,j)==filt10(3,2))&
        .and.(skel(i-1,j+1)==filt10(3,1)).and.(skel(i,j+1)==filt10(2,1))&
        .and.(skel(i-1,j-1)==filt10(3,3)).and.(skel(i+1,j)==filt10(1,2)).and.(skel(i+1,j-1)==filt10(1,3))) then
          skel(i-1,j-1) = 1  
          skel(i,j-1) = 0
          skel(i-1,j) = 0
          !if (skel(i+1,j-2)==1) then
          if (skel(i+1,j-2)+skel(i,j-2)+skel(i-1,j-2)>0) then
                  skel(i-1,j-1) = 0
                  skel(i,j-1) = 1
          endif
          !if (skel(i-2,j+1)==1) then
          if (skel(i-2,j+1)+skel(i-2,j)+skel(i-2,j-1)>0) then
                  skel(i-1,j-1) = 0
                  skel(i-1,j) = 1
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt10(2,2)).and.(skel(i,j+1)==filt10(2,3)).and.(skel(i-1,j-1)==filt10(1,1)).and.(skel(i+1,j)==filt10(3,2))&
        .and.(skel(i+1,j-1)==filt10(3,1)).and.(skel(i,j-1)==filt10(2,1))&
        .and.(skel(i+1,j+1)==filt10(3,3)).and.(skel(i-1,j)==filt10(1,2)).and.(skel(i-1,j+1)==filt10(1,3))) then
          skel(i+1,j+1) = 1  
          skel(i,j+1) = 0
          skel(i+1,j) = 0
          !if (skel(i-1,j+2)==1) then
          if (skel(i-1,j+2)+skel(i,j+2)+skel(i+1,j+2)>0) then
                  skel(i+1,j+1) = 0
                  skel(i,j+1) = 1
          endif
          !if (skel(i+2,j-1)==1) then
          if (skel(i+2,j-1)+skel(i+2,j)+skel(i+2,j+1)>0) then
                  skel(i+1,j+1) = 0
                  skel(i+1,j) = 1
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt10(2,2)).and.(skel(i,j-1)==filt10(2,3)).and.(skel(i-1,j+1)==filt10(1,1)).and.(skel(i+1,j)==filt10(3,2))&
        .and.(skel(i+1,j+1)==filt10(3,1)).and.(skel(i,j+1)==filt10(2,1))&
        .and.(skel(i+1,j-1)==filt10(3,3)).and.(skel(i-1,j)==filt10(1,2)).and.(skel(i-1,j-1)==filt10(1,3))) then
          skel(i+1,j-1) = 1  
          skel(i,j-1) = 0
          skel(i+1,j) = 0
          !if (skel(i-1,j-2)==1) then
          if (skel(i-1,j-2)+skel(i,j-2)+skel(i+1,j-2)>0) then
                  skel(i+1,j-1) = 0
                  skel(i,j-1) = 1
          endif
          !if (skel(i+2,j+1)==1) then
          if (skel(i+2,j+1)+skel(i+2,j)+skel(i+2,j-1)>0) then
                  skel(i+1,j-1) = 0
                  skel(i+1,j) = 1
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2

        if ((skel(i,j)==filt10(2,2)).and.(skel(i+1,j)==filt10(2,3)).and.(skel(i-1,j+1)==filt10(1,1)).and.(skel(i,j-1)==filt10(3,2))&
        .and.(skel(i-1,j-1)==filt10(3,1)).and.(skel(i-1,j)==filt10(2,1))&
        .and.(skel(i+1,j-1)==filt10(3,3)).and.(skel(i,j+1)==filt10(1,2)).and.(skel(i+1,j+1)==filt10(1,3))) then
          skel(i+1,j-1) = 1  
          skel(i+1,j) = 0
          skel(i,j-1) = 0
          !if (skel(i+2,j+1)==1) then
          if (skel(i+2,j+1)+skel(i+2,j)+skel(i+2,j-1)>0) then
                  skel(i+1,j-1) = 0
                  skel(i+1,j) = 1
          endif
          !if (skel(i-1,j-2)==1) then
          if (skel(i-1,j-2)+skel(i,j-2)+skel(i+1,j-2)>0) then
                  skel(i+1,j-1) = 0
                  skel(i,j-1) = 1
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt10(2,2)).and.(skel(i-1,j)==filt10(2,3)).and.(skel(i+1,j+1)==filt10(1,1)).and.(skel(i,j-1)==filt10(3,2))&
        .and.(skel(i+1,j-1)==filt10(3,1)).and.(skel(i+1,j)==filt10(2,1))&
        .and.(skel(i-1,j-1)==filt10(3,3)).and.(skel(i,j+1)==filt10(1,2)).and.(skel(i-1,j+1)==filt10(1,3))) then
          skel(i-1,j-1) = 1  
          skel(i-1,j) = 0
          skel(i,j-1) = 0
          !if (skel(i-2,j+1)==1) then
          if (skel(i-2,j+1)+skel(i-2,j)+skel(i-2,j-1)>0) then
                  skel(i-1,j-1) = 0
                  skel(i-1,j) = 1
          endif
          !if (skel(i+1,j-2)==1) then
          if (skel(i+1,j-2)+skel(i,j-2)+skel(i-1,j-2)>0) then
                  skel(i-1,j-1) = 0
                  skel(i,j-1) = 1
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt10(2,2)).and.(skel(i+1,j)==filt10(2,3)).and.(skel(i-1,j-1)==filt10(1,1)).and.(skel(i,j-1)==filt10(3,2))&
        .and.(skel(i-1,j+1)==filt10(3,1)).and.(skel(i-1,j)==filt10(2,1))&
        .and.(skel(i+1,j+1)==filt10(3,3)).and.(skel(i,j+1)==filt10(1,2)).and.(skel(i+1,j-1)==filt10(1,3))) then
          skel(i+1,j+1) = 1  
          skel(i+1,j) = 0
          skel(i,j+1) = 0
          !if (skel(i+2,j-1)==1) then
          if (skel(i+2,j-1)+skel(i+2,j)+skel(i+2,j+1)>0) then
                  skel(i+1,j+1) = 0
                  skel(i+1,j) = 1
          endif
          !if (skel(i-1,j+2)==1) then
          if (skel(i-1,j+2)+skel(i,j+2)+skel(i+1,j+2)>0) then
                  skel(i+1,j+1) = 0
                  skel(i,j+1) = 1
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt10(2,2)).and.(skel(i-1,j)==filt10(2,3)).and.(skel(i+1,j-1)==filt10(1,1)).and.(skel(i,j-1)==filt10(3,2))&
        .and.(skel(i+1,j+1)==filt10(3,1)).and.(skel(i+1,j)==filt10(2,1))&
        .and.(skel(i-1,j+1)==filt10(3,3)).and.(skel(i,j+1)==filt10(1,2)).and.(skel(i-1,j-1)==filt10(1,3))) then
          skel(i-1,j+1) = 1  
          skel(i-1,j) = 0
          skel(i,j+1) = 0
          !if (skel(i-2,j-1)==1) then
          if (skel(i-2,j-1)+skel(i-2,j)+skel(i-2,j+1)>0) then
                  skel(i-1,j+1) = 0
                  skel(i-1,j) = 1
          endif
          !if (skel(i+1,j+2)==1) then
          if (skel(i+1,j+2)+skel(i,j+2)+skel(i-1,j+2)>0) then
                  skel(i-1,j+1) = 0
                  skel(i,j+1) = 1
          endif
        endif
      enddo
    enddo


    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt6(2,2)).and.(skel(i,j+1)==filt6(2,3)).and.(skel(i+1,j+1)==filt6(1,3))&
!        .and.(skel(i+1,j-1)+skel(i,j-1)+skel(i-1,j-1)==1)&
        .and.(skel(i+1,j)==filt6(1,2)).and.(skel(i-1,j)==filt6(3,2)).and.(skel(i-1,j+1)==filt6(3,3))) then
          if ((skel(i+1,j-1)==1).or.(skel(i,j-1)==1)) then
          !if (skel(i+1,j-1)==1) then
          !        skel(i+1,j+1)=0
          !else if (skel(i,j-1)==1) then
          !else
            if (rhoSlices(i,j+1)<rhoSlices(i+1,j+1)) then
              skel(i,j+1) = 0
              bool(i+1,j+1) = 1
            else
              skel(i+1,j+1) = 0
              bool(i,j+1) = 1
            endif
          else
          skel(i,j+1)=0
          endif

          !if ((skel(i-1,j+2)==1).and.(skel(i+2,j)+skel(i+2,j+1)+skel(i+2,j+2)==0)) then
          if (skel(i-1,j+2)==1) then
            skel(i,j+1) = 1
            skel(i+1,j+1) = 0
          !else if ((skel(i+2,j)+skel(i+2,j+1)+skel(i+2,j+2)>0).and.(skel(i-1,j+2)==0)) then
          else if (skel(i+2,j)+skel(i+2,j+1)+skel(i+2,j+2)>0) then
            skel(i+1,j+1) = 1
            skel(i,j+1) = 0
          endif

          if (skel(i+1,j+2)+skel(i+2,j+2)+skel(i+2,j+1)+skel(i,j+1)+skel(i+1,j)+skel(i,j)+skel(i,j+2)+skel(i+2,j)==2) then
            skel(i+1,j+1) = 1
            skel(i,j+1) = 0
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt6(2,2)).and.(skel(i,j-1)==filt6(2,3)).and.(skel(i+1,j-1)==filt6(1,3))&
 !       .and.(skel(i+1,j+1)+skel(i,j+1)+skel(i-1,j+1)==1)&
        .and.(skel(i+1,j)==filt6(1,2)).and.(skel(i-1,j)==filt6(3,2)).and.(skel(i-1,j-1)==filt6(3,3))) then
          if ((skel(i+1,j+1)==1).or.(skel(i,j+1)==1)) then
          !if (skel(i+1,j+1)==1) then
          !        skel(i+1,j-1)=0
          !else if (skel(i,j+1)==1) then
          !else
            if (rhoSlices(i,j-1)<rhoSlices(i+1,j-1)) then
              skel(i,j-1) = 0
              bool(i+1,j-1) = 1
            else
              skel(i+1,j-1) = 0
              bool(i,j-1) = 1
            endif
            else
            skel(i,j-1)=0
            endif

          !if ((skel(i-1,j-2)==1).and.(skel(i+2,j)+skel(i+2,j-1)+skel(i+2,j-2)==0)) then
          if (skel(i-1,j-2)==1) then
            skel(i,j-1) = 1
            skel(i+1,j-1) = 0
          !else if ((skel(i+2,j)+skel(i+2,j-1)+skel(i+2,j-2)>0).and.(skel(i-1,j-2)==0)) then
          else if (skel(i+2,j)+skel(i+2,j-1)+skel(i+2,j-2)>0) then
            skel(i+1,j-1) = 1
            skel(i,j-1) = 0
          endif

          if (skel(i+1,j-2)+skel(i+2,j-2)+skel(i+2,j-1)+skel(i,j-1)+skel(i+1,j)+skel(i,j)+skel(i,j-2)+skel(i+2,j)==2) then
            skel(i+1,j-1) = 1
            skel(i,j-1) = 0
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt6(2,2)).and.(skel(i,j+1)==filt6(2,3)).and.(skel(i-1,j+1)==filt6(1,3))&
  !      .and.(skel(i-1,j-1)+skel(i,j-1)+skel(i+1,j-1)==1)&
        .and.(skel(i-1,j)==filt6(1,2)).and.(skel(i+1,j)==filt6(3,2)).and.(skel(i+1,j+1)==filt6(3,3))) then
          if ((skel(i-1,j-1)==1).or.(skel(i,j-1)==1)) then
          !if (skel(i-1,j-1)==1) then
          !        skel(i-1,j+1)=0
          !else if (skel(i,j-1)==1) then
          !else
            if (rhoSlices(i,j+1)<rhoSlices(i-1,j+1)) then
              skel(i,j+1) = 0
              bool(i-1,j+1) = 1
            else
              skel(i-1,j+1) = 0
              bool(i,j+1) = 1
            endif
            else
          skel(i,j+1)=0
          endif

          !if ((skel(i-2,j-1)==1).and.(skel(i,j+2)+skel(i-1,j+2)+skel(i-2,j+2)==0)) then
          if (skel(i+1,j+2)==1) then
            skel(i,j+1) = 1
            skel(i-1,j+1) = 0
          !else if ((skel(i,j+2)+skel(i-1,j+2)+skel(i-2,j+2)>0).and.(skel(i-2,j-1)==0)) then
          else if (skel(i-2,j)+skel(i-2,j+1)+skel(i-2,j+2)>0) then
            skel(i-1,j+1) = 1
            skel(i,j+1) = 0
          endif

          if (skel(i-1,j+2)+skel(i-2,j+2)+skel(i-2,j+1)+skel(i,j+1)+skel(i-1,j)+skel(i,j)+skel(i,j+2)+skel(i-2,j)==2) then
            skel(i-1,j+1) = 1
            skel(i,j+1) = 0
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt6(2,2)).and.(skel(i,j-1)==filt6(2,3)).and.(skel(i-1,j-1)==filt6(1,3))&
   !     .and.(skel(i-1,j+1)+skel(i,j+1)+skel(i+1,j+1)==1)&
        .and.(skel(i-1,j)==filt6(1,2)).and.(skel(i+1,j)==filt6(3,2)).and.(skel(i+1,j-1)==filt6(3,3))) then
          if ((skel(i-1,j+1)==1).or.(skel(i,j+1)==1)) then
          !if (skel(i-1,j+1)==1) then
          !        skel(i-1,j-1)=0
          !else if (skel(i,j+1)==1) then
          !else
            if (rhoSlices(i,j-1)<rhoSlices(i-1,j-1)) then
              skel(i,j-1) = 0
              bool(i-1,j-1) = 1
            else
              skel(i-1,j-1) = 0
              bool(i,j-1) = 1
            endif
            else
          skel(i,j-1)=0
          endif

          !if ((skel(i-2,j+1)==1).and.(skel(i,j-2)+skel(i-1,j-2)+skel(i-2,j-2)==0)) then
          if (skel(i+1,j-2)==1) then
            skel(i,j-1) = 1
            skel(i-1,j-1) = 0
          !else if ((skel(i,j-2)+skel(i-1,j-2)+skel(i-2,j-2)>0).and.(skel(i-2,j+1)==0)) then
          else if (skel(i-2,j)+skel(i-2,j-1)+skel(i-2,j-2)>0) then
            skel(i-1,j-1) = 1
            skel(i,j-1) = 0
          endif

          if (skel(i-1,j-2)+skel(i-2,j-2)+skel(i-2,j-1)+skel(i,j-1)+skel(i-1,j)+skel(i,j)+skel(i,j-2)+skel(i-2,j)==2) then
            skel(i-1,j-1) = 1
            skel(i,j-1) = 0
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2

        if ((skel(i,j)==filt6(2,2)).and.(skel(i+1,j)==filt6(2,3)).and.(skel(i+1,j+1)==filt6(1,3))&
    !    .and.(skel(i-1,j+1)+skel(i-1,j)+skel(i-1,j-1)==1)&
        .and.(skel(i,j+1)==filt6(1,2)).and.(skel(i,j-1)==filt6(3,2)).and.(skel(i+1,j-1)==filt6(3,3))) then
          if ((skel(i-1,j+1)==1).or.(skel(i-1,j)==1)) then
          !if (skel(i-1,j+1)==1) then
          !        skel(i+1,j+1)=0
          !else if (skel(i-1,j)==1) then
          !else
            if (rhoSlices(i+1,j)<rhoSlices(i+1,j+1)) then
              skel(i+1,j) = 0
              bool(i+1,j+1) = 1
            else
              skel(i+1,j+1) = 0
              bool(i+1,j) = 1
            endif
            else
          skel(i+1,j)=0
          endif

          !if ((skel(i+2,j-1)==1).and.(skel(i,j+2)+skel(i+1,j+2)+skel(i+2,j+2)==0)) then
          if (skel(i+2,j-1)==1) then
            skel(i+1,j) = 1
            skel(i+1,j+1) = 0
          !else if ((skel(i,j+2)+skel(i+1,j+2)+skel(i+2,j+2)>0).and.(skel(i+2,j-1)==0)) then
          else if (skel(i,j+2)+skel(i+1,j+2)+skel(i+2,j+2)>0) then
            skel(i+1,j+1) = 1
            skel(i+1,j) = 0
          endif

          if (skel(i+2,j+1)+skel(i+2,j+2)+skel(i+1,j+2)+skel(i+1,j)+skel(i,j+1)+skel(i,j)+skel(i+2,j)+skel(i,j+2)==2) then
            skel(i+1,j+1) = 1
            skel(i+1,j) = 0
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt6(2,2)).and.(skel(i-1,j)==filt6(2,3)).and.(skel(i-1,j+1)==filt6(1,3))&
     !   .and.(skel(i+1,j+1)+skel(i+1,j)+skel(i+1,j-1)==1)&
        .and.(skel(i,j+1)==filt6(1,2)).and.(skel(i,j-1)==filt6(3,2)).and.(skel(i-1,j-1)==filt6(3,3))) then
          if ((skel(i+1,j+1)==1).or.(skel(i+1,j)==1)) then
          !if (skel(i+1,j+1)==1) then
          !        skel(i-1,j+1)=0
          !else if (skel(i+1,j)==1) then
          !else
            if (rhoSlices(i-1,j)<rhoSlices(i-1,j+1)) then
              skel(i-1,j) = 0
              bool(i-1,j+1) = 1
            else
              skel(i-1,j+1) = 0
              bool(i-1,j) = 1
            endif
           else
          skel(i-1,j)=0
          endif

          !if ((skel(i-2,j-1)==1).and.(skel(i,j+2)+skel(i-1,j+2)+skel(i-2,j+2)==0)) then
          if (skel(i-2,j-1)==1) then
            skel(i-1,j) = 1
            skel(i-1,j+1) = 0
          !else if ((skel(i,j+2)+skel(i-1,j+2)+skel(i-2,j+2)>0).and.(skel(i-2,j-1)==0)) then
          else if (skel(i,j+2)+skel(i-1,j+2)+skel(i-2,j+2)>0) then
            skel(i-1,j+1) = 1
            skel(i-1,j) = 0
          endif

          if (skel(i-2,j+1)+skel(i-2,j+2)+skel(i-1,j+2)+skel(i-1,j)+skel(i,j+1)+skel(i,j)+skel(i-2,j)+skel(i,j+2)==2) then
            skel(i-1,j+1) = 1
            skel(i-1,j) = 0
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt6(2,2)).and.(skel(i+1,j)==filt6(2,3)).and.(skel(i+1,j-1)==filt6(1,3))&
      !  .and.(skel(i-1,j-1)+skel(i-1,j)+skel(i-1,j+1)==1)&
        .and.(skel(i,j+1)==filt6(1,2)).and.(skel(i,j-1)==filt6(3,2)).and.(skel(i+1,j+1)==filt6(3,3))) then
          if ((skel(i-1,j-1)==1).or.(skel(i-1,j)==1)) then
          !if (skel(i-1,j-1)==1) then
          !        skel(i+1,j-1)=0
          !else if (skel(i-1,j)==1) then
          !else
            if (rhoSlices(i+1,j)<rhoSlices(i+1,j-1)) then
              skel(i+1,j) = 0
              bool(i+1,j-1) = 1
            else
              skel(i+1,j-1) = 0
              bool(i+1,j) = 1
            endif
            else
          skel(i+1,j)=0
          endif

          !if ((skel(i-1,j-2)==1).and.(skel(i+2,j)+skel(i+2,j-1)+skel(i+2,j-2)==0)) then
          if (skel(i+2,j+1)==1) then
            skel(i+1,j) = 1
            skel(i+1,j-1) = 0
          !else if ((skel(i+2,j)+skel(i+2,j-1)+skel(i+2,j-2)>0).and.(skel(i-1,j-2)==0)) then
          else if (skel(i,j-2)+skel(i+1,j-2)+skel(i+2,j-2)>0) then
            skel(i+1,j-1) = 1
            skel(i+1,j) = 0
          endif

          if (skel(i+2,j-1)+skel(i+2,j-2)+skel(i+1,j-2)+skel(i+1,j)+skel(i,j-1)+skel(i,j)+skel(i+2,j)+skel(i,j-2)==2) then
            skel(i+1,j-1) = 1
            skel(i+1,j) = 0
          endif
        endif
      enddo
    enddo
    do i=3,size(skel,1)-2
      do j=3,size(skel,2)-2
        if ((skel(i,j)==filt6(2,2)).and.(skel(i-1,j)==filt6(2,3)).and.(skel(i-1,j-1)==filt6(1,3))&
       ! .and.(skel(i+1,j-1)+skel(i+1,j)+skel(i+1,j+1)==1)&
        .and.(skel(i,j+1)==filt6(1,2)).and.(skel(i,j-1)==filt6(3,2)).and.(skel(i-1,j+1)==filt6(3,3))) then
          if ((skel(i+1,j-1)==1).or.(skel(i+1,j)==1)) then
          !if (skel(i+1,j-1)==1) then
          !        skel(i-1,j-1)=0
          !else if (skel(i+1,j)==1) then
          !else        
            if (rhoSlices(i-1,j)<rhoSlices(i-1,j-1)) then
              skel(i-1,j) = 0
              bool(i-1,j-1) = 1
            else
              skel(i-1,j-1) = 0
              bool(i-1,j) = 1
            endif
            else
          skel(i-1,j)=0
          endif

          !if ((skel(i+1,j-2)==1).and.(skel(i-2,j)+skel(i-2,j-1)+skel(i-2,j-2)==0)) then
          if (skel(i-2,j+1)==1) then
            skel(i-1,j) = 1
            skel(i-1,j-1) = 0
          !else if ((skel(i-2,j)+skel(i-2,j-1)+skel(i-2,j-2)>0).and.(skel(i+1,j-2)==0)) then
          else if (skel(i,j-2)+skel(i-1,j-2)+skel(i-2,j-2)>0) then
            skel(i-1,j-1) = 1
            skel(i-1,j) = 0
          endif

          if (skel(i-2,j-1)+skel(i-2,j-2)+skel(i-1,j-2)+skel(i-1,j)+skel(i,j-1)+skel(i,j)+skel(i-2,j)+skel(i,j-2)==2) then
            skel(i-1,j-1) = 1
            skel(i-1,j) = 0
          endif
        endif
!      enddo
!    enddo

!
!!    skel = skel

!        if ((skel(i,j)==filt7(2,2)).and.(skel(i,j+1)==filt7(2,3)).and.(skel(i+1,j)==filt7(1,2))&
!        .and.(skel(i+1,j+1)==filt7(1,3))) then
!          if (abs(rhoSlices(i,j)*rhoSlices(i,j+1)-min(rhoSlices(i,j)*rhoSlices(i,j+1),rhoSlices(i,j)*rhoSlices(i+1,j)&
!          ,rhoSlices(i,j)*rhoSlices(i+1,j+1),rhoSlices(i,j+1)*rhoSlices(i+1,j)&
!          ,rhoSlices(i,j+1)*rhoSlices(i+1,j+1),rhoSlices(i+1,j)*rhoSlices(i+1,j+1)))<1.e-16) then
!            skel(i,j) = 0
!            skel(i,j+1) = 0
!            bool(i+1,j) = 1
!            bool(i+1,j+1) = 1
!          else if (abs(rhoSlices(i,j)*rhoSlices(i+1,j)-min(rhoSlices(i,j)*rhoSlices(i,j+1),rhoSlices(i,j)*rhoSlices(i+1,j)&
!          ,rhoSlices(i,j)*rhoSlices(i+1,j+1),rhoSlices(i,j+1)*rhoSlices(i+1,j)&
!          ,rhoSlices(i,j+1)*rhoSlices(i+1,j+1),rhoSlices(i+1,j)*rhoSlices(i+1,j+1)))<1.e-16) then
!            skel(i,j) = 0
!            skel(i+1,j) = 0
!            bool(i,j+1) = 1
!            bool(i+1,j+1) = 1
!          else if (abs(rhoSlices(i,j)*rhoSlices(i+1,j+1)-min(rhoSlices(i,j)*rhoSlices(i,j+1),rhoSlices(i,j)*rhoSlices(i+1,j)&
!          ,rhoSlices(i,j)*rhoSlices(i+1,j+1),rhoSlices(i,j+1)*rhoSlices(i+1,j)&
!          ,rhoSlices(i,j+1)*rhoSlices(i+1,j+1),rhoSlices(i+1,j)*rhoSlices(i+1,j+1)))<1.e-16) then
!            skel(i,j) = 0
!            skel(i+1,j+1) = 0
!            bool(i,j+1) = 1
!            bool(i+1,j) = 1
!          else if (abs(rhoSlices(i,j+1)*rhoSlices(i+1,j)-min(rhoSlices(i,j)*rhoSlices(i,j+1),rhoSlices(i,j)*rhoSlices(i+1,j)&
!          ,rhoSlices(i,j)*rhoSlices(i+1,j+1),rhoSlices(i,j+1)*rhoSlices(i+1,j)&
!          ,rhoSlices(i,j+1)*rhoSlices(i+1,j+1),rhoSlices(i+1,j)*rhoSlices(i+1,j+1)))<1.e-16) then
!            skel(i,j+1) = 0
!            skel(i+1,j) = 0
!            bool(i,j) = 1
!            bool(i+1,j+1) = 1
!          else if (abs(rhoSlices(i,j+1)*rhoSlices(i+1,j+1)-min(rhoSlices(i,j)*rhoSlices(i,j+1),rhoSlices(i,j)*rhoSlices(i+1,j)&
!          ,rhoSlices(i,j)*rhoSlices(i+1,j+1),rhoSlices(i,j+1)*rhoSlices(i+1,j)&
!          ,rhoSlices(i,j+1)*rhoSlices(i+1,j+1),rhoSlices(i+1,j)*rhoSlices(i+1,j+1)))<1.e-16) then
!            skel(i,j+1) = 0
!            skel(i+1,j+1) = 0
!            bool(i,j) = 1
!            bool(i+1,j) = 1
!          else 
!            skel(i+1,j) = 0
!            skel(i+1,j+1) = 0
!            bool(i,j) = 1
!            bool(i,j+1) = 1
!          endif
!        endif
        !if ((skel(i,j)==filt7(2,2)).and.(skel(i,j-1)==filt7(2,3)).and.(skel(i+1,j)==filt7(1,2))&
        !.and.(skel(i+1,j-1)==filt7(1,3))) then
        !  if (abs(rhoSlices(i,j)*rhoSlices(i,j-1)-min(rhoSlices(i,j)*rhoSlices(i,j-1),rhoSlices(i,j)*rhoSlices(i+1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i+1,j-1),rhoSlices(i,j-1)*rhoSlices(i+1,j)&
        !  ,rhoSlices(i,j-1)*rhoSlices(i+1,j-1),rhoSlices(i+1,j)*rhoSlices(i+1,j-1)))<1.e-16) then
        !    skel(i,j) = 0
        !    skel(i,j-1) = 0
        !    bool(i+1,j) = 1
        !    bool(i+1,j-1) = 1
        !  else if (abs(rhoSlices(i,j)*rhoSlices(i+1,j)-min(rhoSlices(i,j)*rhoSlices(i,j-1),rhoSlices(i,j)*rhoSlices(i+1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i+1,j-1),rhoSlices(i,j-1)*rhoSlices(i+1,j)&
        !  ,rhoSlices(i,j-1)*rhoSlices(i+1,j-1),rhoSlices(i+1,j)*rhoSlices(i+1,j-1)))<1.e-16) then
        !    skel(i,j) = 0
        !    skel(i+1,j) = 0
        !    bool(i,j-1) = 1
        !    bool(i+1,j-1) = 1
        !  else if (abs(rhoSlices(i,j)*rhoSlices(i+1,j-1)-min(rhoSlices(i,j)*rhoSlices(i,j-1),rhoSlices(i,j)*rhoSlices(i+1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i+1,j-1),rhoSlices(i,j-1)*rhoSlices(i+1,j)&
        !  ,rhoSlices(i,j-1)*rhoSlices(i+1,j-1),rhoSlices(i+1,j)*rhoSlices(i+1,j-1)))<1.e-16) then
        !    skel(i,j) = 0
        !    skel(i+1,j-1) = 0
        !    bool(i,j-1) = 1
        !    bool(i+1,j) = 1
        !  else if (abs(rhoSlices(i,j-1)*rhoSlices(i+1,j)-min(rhoSlices(i,j)*rhoSlices(i,j-1),rhoSlices(i,j)*rhoSlices(i+1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i+1,j-1),rhoSlices(i,j-1)*rhoSlices(i+1,j)&
        !  ,rhoSlices(i,j-1)*rhoSlices(i+1,j-1),rhoSlices(i+1,j)*rhoSlices(i+1,j-1)))<1.e-16) then
        !    skel(i,j-1) = 0
        !    skel(i+1,j) = 0
        !    bool(i,j) = 1
        !    bool(i+1,j-1) = 1
        !  else if (abs(rhoSlices(i,j-1)*rhoSlices(i+1,j-1)-min(rhoSlices(i,j)*rhoSlices(i,j-1),rhoSlices(i,j)*rhoSlices(i+1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i+1,j-1),rhoSlices(i,j-1)*rhoSlices(i+1,j)&
        !  ,rhoSlices(i,j-1)*rhoSlices(i+1,j-1),rhoSlices(i+1,j)*rhoSlices(i+1,j-1)))<1.e-16) then
        !    skel(i,j-1) = 0
        !    skel(i+1,j-1) = 0
        !    bool(i,j) = 1
        !    bool(i+1,j) = 1
        !  else 
        !    skel(i+1,j) = 0
        !    skel(i+1,j-1) = 0
        !    bool(i,j) = 1
        !    bool(i,j-1) = 1
        !  endif
        !endif
        !if ((skel(i,j)==filt7(2,2)).and.(skel(i,j-1)==filt7(2,3)).and.(skel(i-1,j)==filt7(1,2))&
        !.and.(skel(i-1,j-1)==filt7(1,3))) then
        !  if (abs(rhoSlices(i,j)*rhoSlices(i,j-1)-min(rhoSlices(i,j)*rhoSlices(i,j-1),rhoSlices(i,j)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i-1,j-1),rhoSlices(i,j-1)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j-1)*rhoSlices(i-1,j-1),rhoSlices(i-1,j)*rhoSlices(i-1,j-1)))<1.e-16) then
        !    skel(i,j) = 0
        !    skel(i,j-1) = 0
        !    bool(i-1,j) = 1
        !    bool(i-1,j-1) = 1
        !  else if (abs(rhoSlices(i,j)*rhoSlices(i-1,j)-min(rhoSlices(i,j)*rhoSlices(i,j-1),rhoSlices(i,j)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i-1,j-1),rhoSlices(i,j-1)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j-1)*rhoSlices(i-1,j-1),rhoSlices(i-1,j)*rhoSlices(i-1,j-1)))<1.e-16) then
        !    skel(i,j) = 0
        !    skel(i-1,j) = 0
        !    bool(i,j-1) = 1
        !    bool(i-1,j-1) = 1
        !  else if (abs(rhoSlices(i,j)*rhoSlices(i-1,j-1)-min(rhoSlices(i,j)*rhoSlices(i,j-1),rhoSlices(i,j)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i-1,j-1),rhoSlices(i,j-1)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j-1)*rhoSlices(i-1,j-1),rhoSlices(i-1,j)*rhoSlices(i-1,j-1)))<1.e-16) then
        !    skel(i,j) = 0
        !    skel(i-1,j-1) = 0
        !    bool(i,j-1) = 1
        !    bool(i-1,j) = 1
        !  else if (abs(rhoSlices(i,j-1)*rhoSlices(i-1,j)-min(rhoSlices(i,j)*rhoSlices(i,j-1),rhoSlices(i,j)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i-1,j-1),rhoSlices(i,j-1)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j-1)*rhoSlices(i-1,j-1),rhoSlices(i-1,j)*rhoSlices(i-1,j-1)))<1.e-16) then
        !    skel(i,j-1) = 0
        !    skel(i-1,j) = 0
        !    bool(i,j) = 1
        !    bool(i-1,j-1) = 1
        !  else if (abs(rhoSlices(i,j-1)*rhoSlices(i-1,j-1)-min(rhoSlices(i,j)*rhoSlices(i,j-1),rhoSlices(i,j)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i-1,j-1),rhoSlices(i,j-1)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j-1)*rhoSlices(i-1,j-1),rhoSlices(i-1,j)*rhoSlices(i-1,j-1)))<1.e-16) then
        !    skel(i,j-1) = 0
        !    skel(i-1,j-1) = 0
        !    bool(i,j) = 1
        !    bool(i-1,j) = 1
        !  else 
        !    skel(i-1,j) = 0
        !    skel(i-1,j-1) = 0
        !    bool(i,j) = 1
        !    bool(i,j-1) = 1
        !  endif
        !endif
        !if ((skel(i,j)==filt7(2,2)).and.(skel(i,j+1)==filt7(2,3)).and.(skel(i-1,j)==filt7(1,2))&
        !.and.(skel(i-1,j+1)==filt7(1,3))) then
        !  if (abs(rhoSlices(i,j)*rhoSlices(i,j+1)-min(rhoSlices(i,j)*rhoSlices(i,j+1),rhoSlices(i,j)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i-1,j+1),rhoSlices(i,j+1)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j+1)*rhoSlices(i-1,j+1),rhoSlices(i-1,j)*rhoSlices(i-1,j+1)))<1.e-16) then
        !    skel(i,j) = 0
        !    skel(i,j+1) = 0
        !    bool(i-1,j) = 1
        !    bool(i-1,j+1) = 1
        !  else if (abs(rhoSlices(i,j)*rhoSlices(i-1,j)-min(rhoSlices(i,j)*rhoSlices(i,j+1),rhoSlices(i,j)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i-1,j+1),rhoSlices(i,j+1)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j+1)*rhoSlices(i-1,j+1),rhoSlices(i-1,j)*rhoSlices(i-1,j+1)))<1.e-16) then
        !    skel(i,j) = 0
        !    skel(i-1,j) = 0
        !    bool(i,j+1) = 1
        !    bool(i-1,j+1) = 1
        !  else if (abs(rhoSlices(i,j)*rhoSlices(i-1,j+1)-min(rhoSlices(i,j)*rhoSlices(i,j+1),rhoSlices(i,j)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i-1,j+1),rhoSlices(i,j+1)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j+1)*rhoSlices(i-1,j+1),rhoSlices(i-1,j)*rhoSlices(i-1,j+1)))<1.e-16) then
        !    skel(i,j) = 0
        !    skel(i-1,j+1) = 0
        !    bool(i,j+1) = 1
        !    bool(i-1,j) = 1
        !  else if (abs(rhoSlices(i,j+1)*rhoSlices(i-1,j)-min(rhoSlices(i,j)*rhoSlices(i,j+1),rhoSlices(i,j)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i-1,j+1),rhoSlices(i,j+1)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j+1)*rhoSlices(i-1,j+1),rhoSlices(i-1,j)*rhoSlices(i-1,j+1)))<1.e-16) then
        !    skel(i,j+1) = 0
        !    skel(i-1,j) = 0
        !    bool(i,j) = 1
        !    bool(i-1,j+1) = 1
        !  else if (abs(rhoSlices(i,j+1)*rhoSlices(i-1,j+1)-min(rhoSlices(i,j)*rhoSlices(i,j+1),rhoSlices(i,j)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j)*rhoSlices(i-1,j+1),rhoSlices(i,j+1)*rhoSlices(i-1,j)&
        !  ,rhoSlices(i,j+1)*rhoSlices(i-1,j+1),rhoSlices(i-1,j)*rhoSlices(i-1,j+1)))<1.e-16) then
        !    skel(i,j+1) = 0
        !    skel(i-1,j+1) = 0
        !    bool(i,j) = 1
        !    bool(i-1,j) = 1
        !  else 
        !    skel(i-1,j) = 0
        !    skel(i-1,j+1) = 0
        !    bool(i,j) = 1
        !    bool(i,j+1) = 1
        !  endif
        !endif

!           if ((skel(i,j)==0).and.(skel(i-1,j)==1).and.(skel(i+1,j+1)==1)) skel(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i-1,j)==1).and.(skel(i+1,j-1)==1)) skel(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i,j-1)==1).and.(skel(i+1,j+1)==1)) skel(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i,j-1)==1).and.(skel(i-1,j+1)==1)) skel(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i,j-1)==1).and.(skel(i,j+1)==1)) skel(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i-1,j)==1).and.(skel(i+1,j)==1)) skel(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i-1,j-1)==1).and.(skel(i+1,j+1)==1)) skel(i,j) = 1
!           if ((skel(i,j)==0).and.(skel(i-1,j+1)==1).and.(skel(i+1,j-1)==1)) skel(i,j) = 1
      enddo
    enddo
!    enddo

    do i=2,size(skel,1)-1
      do j=2,size(skel,2)-1

        !! check
        if ((skel(i,j)==1).and.(skel(i,j+1)==1).and.(skel(i+1,j+1)==1)) write(*,*) "error NAN1"
        if ((skel(i,j)==1).and.(skel(i,j+1)==1).and.(skel(i-1,j+1)==1)) write(*,*) "error NAN2"
        if ((skel(i,j)==1).and.(skel(i,j-1)==1).and.(skel(i+1,j-1)==1)) write(*,*) "error NAN3"
        if ((skel(i,j)==1).and.(skel(i,j-1)==1).and.(skel(i-1,j-1)==1)) write(*,*) "error NAN4"
        if ((skel(i,j)==1).and.(skel(i+1,j)==1).and.(skel(i+1,j+1)==1)) write(*,*) "error NAN5"
        if ((skel(i,j)==1).and.(skel(i+1,j)==1).and.(skel(i+1,j-1)==1)) write(*,*) "error NAN6"
        if ((skel(i,j)==1).and.(skel(i-1,j)==1).and.(skel(i-1,j+1)==1)) write(*,*) "error NAN7"
        if ((skel(i,j)==1).and.(skel(i-1,j)==1).and.(skel(i-1,j-1)==1)) write(*,*) "error NAN8"
      enddo
    enddo

!    skel = skeltmp
  end subroutine filterskel

  subroutine filtermidline(midline,rhoSlices)
    implicit none
    real(pr),dimension(:,:),intent(in) :: rhoSlices
    integer,dimension(:,:),intent(inout) :: midline
    integer,dimension(size(midline,1),size(midline,2)) :: midlinetmp
    integer :: l

    do l=1,size(midline,1)
       midlinetmp(l,1) = midline(size(midline,1)-l+1,1)
       midlinetmp(l,2) = midline(size(midline,1)-l+1,2)
    enddo
    midline = midlinetmp

    midlinetmp = midline
    do l=2,size(midline,1)-1
       !do l=size(midline,1)-1,2,-1
       if ((midline(l+1,1)-midline(l,1)==+2)) then
          midlinetmp(l+1,1) = midline(l,1)+1
       else if ((midline(l+1,1)-midline(l,1)==-2)) then
          midlinetmp(l+1,1) = midline(l,1)-1
       else if ((midline(l+1,2)-midline(l,2)==+2)) then
          midlinetmp(l+1,2) = midline(l,2)+1
       else if ((midline(l+1,2)-midline(l,2)==-2)) then
          midlinetmp(l+1,2) = midline(l,2)-1
       else if (midline(l-1,1)==midline(l,1)) then !2pts aligned
          !if ((midline(l-1,2)+midline(l+1,2)==2*midline(l,2)).and.(abs(midline(l+1,1)-midline(l,1))==1)) then
          !   if (.not.(abs(rhoSlices(midlinetmp(l,1),midlinetmp(l+1,2))-rhoSlices(midline(l+1,1),midline(l+1,2)))&
          !   >0.25*rhoSlices(midline(l+1,1),midline(l+1,2))))          midlinetmp(l+1,1) = midline(l,1)
          
          if ((midline(l-1,2)+midline(l+1,2)==2*midline(l,2)).and.(abs(midline(l+1,1)-midline(l,1))==1)) then
             midlinetmp(l+1,1) = midline(l,1)
        endif

       else if (midline(l-1,2)==midline(l,2)) then !2pts aligned
          if ((midline(l-1,1)+midline(l+1,1)==2*midline(l,1)).and.(abs(midline(l+1,2)-midline(l,2))==1)) then
             !if (.not.(abs(rhoSlices(midlinetmp(l+1,1),midlinetmp(l,2))-rhoSlices(midline(l+1,1),midline(l+1,2)))&
             !>0.25*rhoSlices(midline(l+1,1),midline(l+1,2))))          midlinetmp(l+1,2) = midline(l,2)
             midlinetmp(l+1,2) = midline(l,2)
          endif

       else if ((abs(midline(l-1,1)-midline(l,1))==1).and.(abs(midline(l-1,2)-midline(l,2))==1)) then !2 pts diagonal
          if (midline(l+1,1)==midline(l-1,1)) midlinetmp(l,1) = midline(l-1,1)
          if (midline(l+1,2)==midline(l-1,2)) midlinetmp(l,2) = midline(l-1,2)
          if (midline(l+1,1)==midline(l,1)) then !2pts alignes encore
            ! if (.not.(abs(rhoSlices(midlinetmp(l-1,1),midlinetmp(l,2))-rhoSlices(midline(l,1),midline(l,2)))&
            !>0.25*rhoSlices(midline(l,1),midline(l,2))))               midlinetmp(l,1) = midline(l-1,1)
            midlinetmp(l,1) = midline(l-1,1)
             !if (.not.(abs(rhoSlices(midlinetmp(l-1,1),midlinetmp(l+1,2))-rhoSlices(midline(l+1,1),midline(l+1,2)))&
             !>0.25*rhoSlices(midline(l+1,1),midline(l+1,2))))            midlinetmp(l+1,1) = midline(l-1,1)
             midlinetmp(l+1,1) = midline(l-1,1)
          endif
          if (midline(l+1,2)==midline(l,2)) then !2pts alignes encore
            ! if (.not.(abs(rhoSlices(midlinetmp(l,1),midlinetmp(l-1,2))-rhoSlices(midline(l,1),midline(l,2)))&
            !>0.25*rhoSlices(midline(l,1),midline(l,2))))               midlinetmp(l,2) = midline(l-1,2)
             midlinetmp(l,2) = midline(l-1,2)
             !if (.not.(abs(rhoSlices(midlinetmp(l+1,1),midlinetmp(l-1,2))-rhoSlices(midline(l+1,1),midline(l+1,2)))&
             !>0.25*rhoSlices(midline(l+1,1),midline(l+1,2))))            midlinetmp(l+1,2) = midline(l-1,2)
             midlinetmp(l+1,2) = midline(l-1,2)
          endif
       endif
       midline = midlinetmp
    enddo

    do l=1,size(midline,1)
       midlinetmp(l,1) = midline(size(midline,1)-l+1,1)
       midlinetmp(l,2) = midline(size(midline,1)-l+1,2)
    enddo
    midline = midlinetmp

    !   midline = midline_out
  end subroutine filtermidline

  subroutine findL(midline,ii,jj,ll)
    implicit none
    integer,intent(in) :: ii,jj
    integer,intent(inout) :: ll
    integer,dimension(:,:),intent(in) :: midline

    ll=1
    do while (((.not.(ii==midline(ll,1))).or.(.not.(jj==midline(ll,2)))).and.(ll<size(midline,1)))
      ll = ll+1
    enddo
  end subroutine


  subroutine cutheadskel(midline,skel,skel2,nl,ii,jj,kt)
    implicit none
    integer,intent(in) :: nl,kt
    integer,intent(inout) :: ii,jj
    integer,dimension(:,:),intent(inout) :: skel2
    integer,dimension(:,:),intent(inout) :: skel,midline
    integer,dimension(size(skel2,1),size(skel2,2)) :: skeltmp
    integer,dimension(size(midline,1),size(midline,2)) :: midlinetmp
    integer :: l,bool,ll,lm,lp,bool22,iinew,jjnew
    integer :: ll1,ll2,ll3,ll4,ll5,ll6,ll7,ll8,ll9,i,j,nnz
    integer,dimension(:),allocatable :: tab
    !real(pr),dimension(:),allocatable :: tab
    integer,dimension(9) :: tabll
    !real(pr),dimension(9) :: tabll
    real(pr) :: mintab
    ll1 = 0
    ll2 = 0
    ll3 = 0
    ll4 = 0
    ll5 = 0
    ll6 = 0
    ll7 = 0
    ll8 = 0
    ll9 = 0
    ll = 0

    !if (kt<40) then
    skeltmp = 0
    do i=1,size(skel2,1)
      do j=1,size(skel2,2)
        if (skel2(i,j)==1) then
          skeltmp(i,j) = 1
          skeltmp(i,j+1) = 1
          skeltmp(i,j-1) = 1
          skeltmp(i-1,j) = 1
          skeltmp(i+1,j) = 1
          skeltmp(i+1,j+1) = 1
          skeltmp(i-1,j-1) = 1
          skeltmp(i+1,j-1) = 1
          skeltmp(i-1,j+1) = 1
        endif
      enddo
    enddo
    skel2 = skeltmp
    !endif

    do l=1,size(midline,1)
       midlinetmp(l,1) = midline(size(midline,1)-l+1,1)
       midlinetmp(l,2) = midline(size(midline,1)-l+1,2)
    enddo
    midline = midlinetmp

    skeltmp = 0
!    if ((ii==-1).and.(jj==-1)) then
    if ((ii==-1).and.(jj==-1)) then
      
      skeltmp = skel2
      !do l=size(midline,1)-nl+1,size(midline,1)
      !   skeltmp(midline(l,1),midline(l,2)) = skel2(midline(l,1),midline(l,2))
      !enddo

    else
!    else if (skel(ii-1,jj-1)==1) then
    if (skel(ii-1,jj-1)==1) &
      call findL(midline,ii-1,jj-1,ll1)
!      call findL(midline,ii-1,jj-1,ll)
!      bool = 0
!      lp = ll
!      do while ((bool==0).and.(lp<size(midline,1)))
!        if (skel2(midline(lp,1),midline(lp,2))==1) skeltmp(midline(lp,1),midline(lp,2)) = 1
!        if ((skel2(midline(lp,1),midline(lp,2))==1).and.(skel2(midline(lp+1,1),midline(lp+1,2))==0)) bool = 1   
!        lp=lp+1
!      enddo
!      if ((lp==size(midline,1)).and.(skel2(midline(size(midline,1),1),midline(size(midline,1),2))==1))&
!        skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      !if (lp==size(midline,1)) skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      bool = 0
!      lm = ll
!      do while ((bool==0).and.(lm>size(midline,1)-nl+1))
!        if (skel2(midline(lm,1),midline(lm,2))==1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!        if ((skel2(midline(lm,1),midline(lm,2))==1).and.(skel2(midline(lm-1,1),midline(lm-1,2))==0)) bool = 1   
!        lm=lm-1
!      enddo
!      if ((lm==size(midline,1)-nl+1).and.(skel2(midline(lm,1),midline(lm,2))==1)) skeltmp(midline(lm,1),midline(lm,2)) = 1
!      !if (lm==size(midline,1)-nl+1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      if ((lp<lm).and.(lp==size(midline,1))) then
!!              skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!!      else if ((lm<lp).and.(lm==size(midline,1)-nl+1)) then
!!              skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      endif
!write(*,*) "CASE 1"
!    else if ((skel(ii-1,jj)==1).or.(skel(ii-2,jj)==1)) then
    if ((skel(ii-1,jj)==1).or.(skel(ii-2,jj)==1)) then
            if (skel(ii-1,jj)==1) then
!                    call findL(midline,ii-1,jj,ll)
                    call findL(midline,ii-1,jj,ll2)
            else
!                    call findL(midline,ii-2,jj,ll)
                    call findL(midline,ii-2,jj,ll2)
            endif
    endif
!      bool = 0
!      lp = ll
!      do while ((bool==0).and.(lp<size(midline,1)))
!        if (skel2(midline(lp,1),midline(lp,2))==1) skeltmp(midline(lp,1),midline(lp,2)) = 1
!        if ((skel2(midline(lp,1),midline(lp,2))==1).and.(skel2(midline(lp+1,1),midline(lp+1,2))==0)) bool = 1   
!        lp=lp+1
!      enddo
!      if ((lp==size(midline,1)).and.(skel2(midline(size(midline,1),1),midline(size(midline,1),2))==1))&
!        skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      !if (lp==size(midline,1)) skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      bool = 0
!      lm = ll
!      do while ((bool==0).and.(lm>size(midline,1)-nl+1))
!        if (skel2(midline(lm,1),midline(lm,2))==1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!        if ((skel2(midline(lm,1),midline(lm,2))==1).and.(skel2(midline(lm-1,1),midline(lm-1,2))==0)) bool = 1   
!        lm=lm-1
!      enddo
!      if ((lm==size(midline,1)-nl+1).and.(skel2(midline(lm,1),midline(lm,2))==1)) skeltmp(midline(lm,1),midline(lm,2)) = 1
!      !if (lm==size(midline,1)-nl+1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      if ((lp<lm).and.(lp==size(midline,1))) then
!!              skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!!      else if ((lm<lp).and.(lm==size(midline,1)-nl+1)) then
!!              skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      endif
!write(*,*) "CASE 2"
!    else if (skel(ii-1,jj+1)==1) then
    if (skel(ii-1,jj+1)==1) &
      call findL(midline,ii-1,jj+1,ll3)
!      call findL(midline,ii-1,jj+1,ll)
!      bool = 0
!      lp = ll
!      do while ((bool==0).and.(lp<size(midline,1)))
!        if (skel2(midline(lp,1),midline(lp,2))==1) skeltmp(midline(lp,1),midline(lp,2)) = 1
!        if ((skel2(midline(lp,1),midline(lp,2))==1).and.(skel2(midline(lp+1,1),midline(lp+1,2))==0)) bool = 1   
!        lp=lp+1
!      enddo
!      if ((lp==size(midline,1)).and.(skel2(midline(size(midline,1),1),midline(size(midline,1),2))==1))&
!        skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      !if (lp==size(midline,1)) skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      bool = 0
!      lm = ll
!      do while ((bool==0).and.(lm>size(midline,1)-nl+1))
!        if (skel2(midline(lm,1),midline(lm,2))==1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!        if ((skel2(midline(lm,1),midline(lm,2))==1).and.(skel2(midline(lm-1,1),midline(lm-1,2))==0)) bool = 1   
!        lm=lm-1
!      enddo
!      if ((lm==size(midline,1)-nl+1).and.(skel2(midline(lm,1),midline(lm,2))==1)) skeltmp(midline(lm,1),midline(lm,2)) = 1
!      !if (lm==size(midline,1)-nl+1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      if ((lp<lm).and.(lp==size(midline,1))) then
!!              skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!!      else if ((lm<lp).and.(lm==size(midline,1)-nl+1)) then
!!              skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      endif
!write(*,*) "CASE 3"
!    else if ((skel(ii,jj-1)==1).or.(skel(ii,jj-2)==1)) then
    if ((skel(ii,jj-1)==1).or.(skel(ii,jj-2)==1)) then
            if (skel(ii,jj-1)==1) then
!                    call findL(midline,ii,jj-1,ll)
                    call findL(midline,ii,jj-1,ll4)
            else
!                    call findL(midline,ii,jj-2,ll)
                    call findL(midline,ii,jj-2,ll4)
            endif
    endif
!      bool = 0
!      lp = ll
!      do while ((bool==0).and.(lp<size(midline,1)))
!        if (skel2(midline(lp,1),midline(lp,2))==1) skeltmp(midline(lp,1),midline(lp,2)) = 1
!        if ((skel2(midline(lp,1),midline(lp,2))==1).and.(skel2(midline(lp+1,1),midline(lp+1,2))==0)) bool = 1   
!        lp=lp+1
!      enddo
!      if ((lp==size(midline,1)).and.(skel2(midline(size(midline,1),1),midline(size(midline,1),2))==1))&
!        skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      !if (lp==size(midline,1)) skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      bool = 0
!      lm = ll
!      do while ((bool==0).and.(lm>size(midline,1)-nl+1))
!        if (skel2(midline(lm,1),midline(lm,2))==1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!        if ((skel2(midline(lm,1),midline(lm,2))==1).and.(skel2(midline(lm-1,1),midline(lm-1,2))==0)) bool = 1   
!        lm=lm-1
!      enddo
!      if ((lm==size(midline,1)-nl+1).and.(skel2(midline(lm,1),midline(lm,2))==1)) skeltmp(midline(lm,1),midline(lm,2)) = 1
!      !if (lm==size(midline,1)-nl+1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      if ((lp<lm).and.(lp==size(midline,1))) then
!!              skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!!      else if ((lm<lp).and.(lm==size(midline,1)-nl+1)) then
!!              skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      endif
!write(*,*) "CASE 4"
!    else if (skel(ii,jj)==1) then
    if (skel(ii,jj)==1) &
      call findL(midline,ii,jj,ll5)
!      call findL(midline,ii,jj,ll)
!      bool = 0
!      lp = ll
!      do while ((bool==0).and.(lp<size(midline,1)))
!        if (skel2(midline(lp,1),midline(lp,2))==1) skeltmp(midline(lp,1),midline(lp,2)) = 1
!        if ((skel2(midline(lp,1),midline(lp,2))==1).and.(skel2(midline(lp+1,1),midline(lp+1,2))==0)) bool = 1   
!        lp=lp+1
!      enddo
!      if ((lp==size(midline,1)).and.(skel2(midline(size(midline,1),1),midline(size(midline,1),2))==1))&
!        skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      !if (lp==size(midline,1)) skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      bool = 0
!      lm = ll
!      do while ((bool==0).and.(lm>size(midline,1)-nl+1))
!        if (skel2(midline(lm,1),midline(lm,2))==1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!        if ((skel2(midline(lm,1),midline(lm,2))==1).and.(skel2(midline(lm-1,1),midline(lm-1,2))==0)) bool = 1   
!        lm=lm-1
!      enddo
!      if ((lm==size(midline,1)-nl+1).and.(skel2(midline(lm,1),midline(lm,2))==1)) skeltmp(midline(lm,1),midline(lm,2)) = 1
!      !if (lm==size(midline,1)-nl+1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      if ((lp<lm).and.(lp==size(midline,1))) then
!!              skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!!      else if ((lm<lp).and.(lm==size(midline,1)-nl+1)) then
!!              skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      endif
!write(*,*) "CASE 5"
!    else if ((skel(ii,jj+1)==1).or.(skel(ii,jj+2)==1)) then
    if ((skel(ii,jj+1)==1).or.(skel(ii,jj+2)==1)) then
            if (skel(ii,jj+1)==1) then
!                    call findL(midline,ii,jj+1,ll)
                    call findL(midline,ii,jj+1,ll6)
            else
!                    call findL(midline,ii,jj+2,ll)
                    call findL(midline,ii,jj+2,ll6)
            endif
    endif
!      bool = 0
!      lp = ll
!      do while ((bool==0).and.(lp<size(midline,1)))
!        if (skel2(midline(lp,1),midline(lp,2))==1) skeltmp(midline(lp,1),midline(lp,2)) = 1
!        if ((skel2(midline(lp,1),midline(lp,2))==1).and.(skel2(midline(lp+1,1),midline(lp+1,2))==0)) bool = 1   
!        lp=lp+1
!      enddo
!      if ((lp==size(midline,1)).and.(skel2(midline(size(midline,1),1),midline(size(midline,1),2))==1))&
!        skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      !if (lp==size(midline,1)) skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      bool = 0
!      lm = ll
!      do while ((bool==0).and.(lm>size(midline,1)-nl+1))
!        if (skel2(midline(lm,1),midline(lm,2))==1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!        if ((skel2(midline(lm,1),midline(lm,2))==1).and.(skel2(midline(lm-1,1),midline(lm-1,2))==0)) bool = 1   
!        lm=lm-1
!      enddo
!      if ((lm==size(midline,1)-nl+1).and.(skel2(midline(lm,1),midline(lm,2))==1)) skeltmp(midline(lm,1),midline(lm,2)) = 1
!      !if (lm==size(midline,1)-nl+1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      if ((lp<lm).and.(lp==size(midline,1))) then
!!              skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!!      else if ((lm<lp).and.(lm==size(midline,1)-nl+1)) then
!!              skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      endif
!write(*,*) "CASE 6"
!    else if (skel(ii+1,jj-1)==1) then
    if (skel(ii+1,jj-1)==1) &
      call findL(midline,ii+1,jj-1,ll7)
!      call findL(midline,ii+1,jj-1,ll)
!      bool = 0
!      lp = ll
!      do while ((bool==0).and.(lp<size(midline,1)))
!        if (skel2(midline(lp,1),midline(lp,2))==1) skeltmp(midline(lp,1),midline(lp,2)) = 1
!        if ((skel2(midline(lp,1),midline(lp,2))==1).and.(skel2(midline(lp+1,1),midline(lp+1,2))==0)) bool = 1   
!        lp=lp+1
!      enddo
!      if ((lp==size(midline,1)).and.(skel2(midline(size(midline,1),1),midline(size(midline,1),2))==1))&
!        skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      !if (lp==size(midline,1)) skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      bool = 0
!      lm = ll
!      do while ((bool==0).and.(lm>size(midline,1)-nl+1))
!        if (skel2(midline(lm,1),midline(lm,2))==1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!        if ((skel2(midline(lm,1),midline(lm,2))==1).and.(skel2(midline(lm-1,1),midline(lm-1,2))==0)) bool = 1   
!        lm=lm-1
!      enddo
!      if ((lm==size(midline,1)-nl+1).and.(skel2(midline(lm,1),midline(lm,2))==1)) skeltmp(midline(lm,1),midline(lm,2)) = 1
!      !if (lm==size(midline,1)-nl+1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      if ((lp<lm).and.(lp==size(midline,1))) then
!!              skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!!      else if ((lm<lp).and.(lm==size(midline,1)-nl+1)) then
!!              skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      endif
!write(*,*) "CASE 7"
!    else if ((skel(ii+1,jj)==1).or.(skel(ii+2,jj)==1)) then
    if ((skel(ii+1,jj)==1).or.(skel(ii+2,jj)==1)) then
            if (skel(ii+1,jj)==1) then
!                    call findL(midline,ii+1,jj,ll)
                    call findL(midline,ii+1,jj,ll8)
            else
!                    call findL(midline,ii+2,jj,ll)
                    call findL(midline,ii+2,jj,ll8)
            endif
     endif
!      bool = 0
!      lp = ll
!      do while ((bool==0).and.(lp<size(midline,1)))
!        if (skel2(midline(lp,1),midline(lp,2))==1) skeltmp(midline(lp,1),midline(lp,2)) = 1
!        if ((skel2(midline(lp,1),midline(lp,2))==1).and.(skel2(midline(lp+1,1),midline(lp+1,2))==0)) bool = 1   
!        lp=lp+1
!      enddo
!      if ((lp==size(midline,1)).and.(skel2(midline(size(midline,1),1),midline(size(midline,1),2))==1))&
!        skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      !if (lp==size(midline,1)) skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      bool = 0
!      lm = ll
!      do while ((bool==0).and.(lm>size(midline,1)-nl+1))
!        if (skel2(midline(lm,1),midline(lm,2))==1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!        if ((skel2(midline(lm,1),midline(lm,2))==1).and.(skel2(midline(lm-1,1),midline(lm-1,2))==0)) bool = 1   
!        lm=lm-1
!      enddo
!      if ((lm==size(midline,1)-nl+1).and.(skel2(midline(lm,1),midline(lm,2))==1)) skeltmp(midline(lm,1),midline(lm,2)) = 1
!      !if (lm==size(midline,1)-nl+1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      if ((lp<lm).and.(lp==size(midline,1))) then
!!              skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!!      else if ((lm<lp).and.(lm==size(midline,1)-nl+1)) then
!!              skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      endif
!write(*,*) "CASE 8"
!    else if (skel(ii+1,jj+1)==1) then
    if (skel(ii+1,jj+1)==1) &
      call findL(midline,ii+1,jj+1,ll9)
!      call findL(midline,ii+1,jj+1,ll)
!      bool = 0
!      lp = ll
!      do while ((bool==0).and.(lp<size(midline,1)))
!        if (skel2(midline(lp,1),midline(lp,2))==1) skeltmp(midline(lp,1),midline(lp,2)) = 1
!        if ((skel2(midline(lp,1),midline(lp,2))==1).and.(skel2(midline(lp+1,1),midline(lp+1,2))==0)) bool = 1   
!        lp=lp+1
!      enddo
!      if ((lp==size(midline,1)).and.(skel2(midline(size(midline,1),1),midline(size(midline,1),2))==1))&
!        skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      !if (lp==size(midline,1)) skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!      bool = 0
!      lm = ll
!      do while ((bool==0).and.(lm>size(midline,1)-nl+1))
!        if (skel2(midline(lm,1),midline(lm,2))==1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!        if ((skel2(midline(lm,1),midline(lm,2))==1).and.(skel2(midline(lm-1,1),midline(lm-1,2))==0)) bool = 1   
!        lm=lm-1
!      enddo
!      if ((lm==size(midline,1)-nl+1).and.(skel2(midline(lm,1),midline(lm,2))==1)) skeltmp(midline(lm,1),midline(lm,2)) = 1
!      !if (lm==size(midline,1)-nl+1) skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      if ((lp<lm).and.(lp==size(midline,1))) then
!!              skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
!!      else if ((lm<lp).and.(lm==size(midline,1)-nl+1)) then
!!              skeltmp(midline(lm,1),midline(lm,2)) = 1
!!      endif
!write(*,*) "CASE 9"
!    else
!      write(*,*) "error STOP"
!    endif
      tabll(1) = ll1
      tabll(2) = ll2
      tabll(3) = ll3
      tabll(4) = ll4
      tabll(5) = ll5
      tabll(6) = ll6
      tabll(7) = ll7
      tabll(8) = ll8
      tabll(9) = ll9
      nnz = 9
      if (ll1==0) nnz = nnz-1
      if (ll2==0) nnz = nnz-1
      if (ll3==0) nnz = nnz-1
      if (ll4==0) nnz = nnz-1
      if (ll5==0) nnz = nnz-1
      if (ll6==0) nnz = nnz-1
      if (ll7==0) nnz = nnz-1
      if (ll8==0) nnz = nnz-1
      if (ll9==0) nnz = nnz-1
      if (nnz>0) then
              allocate(tab(nnz))
              mintab = 1E6
              j = 0
              do i=1,9
                if (tabll(i)>0) then
                        j=j+1
                        tab(j) = tabll(i)
                        if (sqrt((ii*1._pr-midline(tabll(i),1))**2+(jj*1._pr-midline(tabll(i),2))**2)<mintab) then
                                mintab = sqrt((ii*1._pr-midline(tabll(i),1))**2+(jj*1._pr-midline(tabll(i),2))**2)
                                ll = tabll(i)
                        endif
                endif
              enddo
              if ((ii==-1).and.(jj==-1)) ll = minval(tab)
              ll = minval(tab)
              if (ll==ll1) write(*,*) "CASE 1 ",mintab
              if (ll==ll2) write(*,*) "CASE 2 ",mintab
              if (ll==ll3) write(*,*) "CASE 3 ",mintab
              if (ll==ll4) write(*,*) "CASE 4 ",mintab
              if (ll==ll5) write(*,*) "CASE 5 ",mintab
              if (ll==ll6) write(*,*) "CASE 6 ",mintab
              if (ll==ll7) write(*,*) "CASE 7 ",mintab
              if (ll==ll8) write(*,*) "CASE 8 ",mintab
              if (ll==ll9) write(*,*) "CASE 9 ",mintab

              bool = 0
              lp = ll
              do while ((bool==0).and.(lp<size(midline,1)))
                !if ((skel2(midline(lp,1),midline(lp,2))==0).and.&
                !((skel2(midline(lp,1)-1,midline(lp,2)-1)==1).or.(skel2(midline(lp,1)-1,midline(lp,2))==1).or.&
                !(skel2(midline(lp,1),midline(lp,2)-1)==1).or.(skel2(midline(lp,1)+1,midline(lp,2))==1).or.&
                !(skel2(midline(lp,1),midline(lp,2)+1)==1).or.(skel2(midline(lp,1)+1,midline(lp,2)+1)==1).or.&
                !(skel2(midline(lp,1)+1,midline(lp,2)-1)==1).or.(skel2(midline(lp,1)-1,midline(lp,2)+1)==1))) then
                !  skeltmp(midline(lp,1),midline(lp,2)) = 1
                !  write(*,*) "p1 ",midline(lp,1)," ",midline(lp,2)
                !endif
               
                !skeltmp(midline(lp,1),midline(lp,2)) = 1
                if (skel2(midline(lp,1),midline(lp,2))==1) skeltmp(midline(lp,1),midline(lp,2)) = 1
                if (skel2(midline(lp,1),midline(lp,2))==1) write(*,*) "ppp1  ",midline(lp,1)," ",midline(lp,2)
                !if ((skel2(midline(lp,1),midline(lp,2))==1).and.(skel2(midline(lp+1,1),midline(lp+1,2))==0)) bool = 1   
                if ((skeltmp(midline(lp,1),midline(lp,2))==1).and.(skel2(midline(lp+1,1),midline(lp+1,2))==0)) bool = 1   
                lp=lp+1
              enddo
              if ((lp==size(midline,1)).and.(skel2(midline(size(midline,1),1),midline(size(midline,1),2))==1))&
                skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
              if ((lp==size(midline,1)).and.(skel2(midline(size(midline,1),1),midline(size(midline,1),2))==1))&
                write(*,*) "P1 ",midline(lp,1)," ",midline(lp,2)
              !if (lp==size(midline,1)) skeltmp(midline(size(midline,1),1),midline(size(midline,1),2)) = 1
              bool = 0
              lm = ll
              do while ((bool==0).and.(lm>size(midline,1)-nl+1))
                write(*,*) "mm0  ",midline(lm,1)," ",midline(lm,2)
                !if ((skel2(midline(lm,1),midline(lm,2))==0).and.&
                !((skel2(midline(lm,1)-1,midline(lm,2)-1)==1).or.(skel2(midline(lm,1)-1,midline(lm,2))==1).or.&
                !(skel2(midline(lm,1),midline(lm,2)-1)==1).or.(skel2(midline(lm,1)+1,midline(lm,2))==1).or.&
                !(skel2(midline(lm,1),midline(lm,2)+1)==1).or.(skel2(midline(lm,1)+1,midline(lm,2)+1)==1).or.&
                !(skel2(midline(lm,1)+1,midline(lm,2)-1)==1).or.(skel2(midline(lm,1)-1,midline(lm,2)+1)==1))) then
                !  skeltmp(midline(lm,1),midline(lm,2)) = 1
                !  write(*,*) "m1 ",midline(lm,1)," ",midline(lm,2)
                !endif
               
                if (skel2(midline(lm,1),midline(lm,2))==1) skeltmp(midline(lm,1),midline(lm,2)) = 1
                if (skel2(midline(lm,1),midline(lm,2))==1) write(*,*) "mm1  ",midline(lm,1)," ",midline(lm,2)
                !if ((skel2(midline(lm,1),midline(lm,2))==1).and.(skel2(midline(lm-1,1),midline(lm-1,2))==0)) bool = 1   
                if ((skeltmp(midline(lm,1),midline(lm,2))==1).and.(skel2(midline(lm-1,1),midline(lm-1,2))==0)) bool = 1   
                lm=lm-1
              enddo
              if ((lm==size(midline,1)-nl+1).and.(skel2(midline(lm,1),midline(lm,2))==1)) skeltmp(midline(lm,1),midline(lm,2)) = 1
              if ((lm==size(midline,1)-nl+1).and.(skel2(midline(lm,1),midline(lm,2))==1)) &
              write(*,*) "M1 ",midline(lm,1)," ",midline(lm,2)
              deallocate(tab)
      else
              write(*,*) "error STOP"
      endif



    endif

    skel2 = skeltmp
    !if(kt==528)skel2(154,114)=0
    !if(kt==519)skel2(152,110)=0
    !if(kt==526)skel2(151,111)=0
    !if(kt==529)skel2(151,111)=0
    !if(kt==533)skel2(150,111)=0
    !if(kt==537)skel2(150,111)=0
    !if(kt==542)skel2(149,111)=0
    !if(kt==551)skel2(151,110)=0
    !if(kt==556)skel2(151,109)=0


    bool = 0
    bool22 = 0
    skeltmp = skel
    l=size(midline,1)-nl+1
    do while ((bool==0).and.(l<size(midline,1)).and.(nl>1))!).or.(skel2(midline(l,1),midline(l,2))==0))
!       write(*,*) "writeL ",l
       write(*,*) "writeL (i,j) ",midline(l,1)," ",midline(l,2)," ",l
!      if ((skel2(midline(l,1),midline(l,2))==0).and.(skel2(midline(l+1,1),midline(l+1,2))==0)) bool = 1
!      if ((l==1).and.(skel2(midline(l,1),midline(l,2))==0).and.(skel2(midline(l+1,1),midline(l+1,2))==1)) 
      if (((skel(midline(l,1),midline(l,2))==1).and.(skel2(midline(l+1,1),midline(l+1,2))==1)).and.&
      !if ((skel(midline(l,1),midline(l,2))==1).and.&
      !(sqrt((ii*1._pr-midline(l+1,1))**2+(jj*1._pr-midline(l+1,2))**2)<=&
      (sqrt((ii*1._pr-midline(l+1,1))**2+(jj*1._pr-midline(l+1,2))**2)<&
      sqrt((ii*1._pr-midline(l,1))**2+(jj*1._pr-midline(l,2))**2))) then
              skeltmp(midline(l,1),midline(l,2)) = 0   
      write(*,*) "AIE 11"
      elseif (((skel(midline(l,1),midline(l,2))==1).and.(skel2(midline(l+1,1),midline(l+1,2))==1)).and.&
      !elseif ((skel(midline(l,1),midline(l,2))==1).and.&
      !(sqrt((ii*1._pr-midline(l+1,1))**2+(jj*1._pr-midline(l+1,2))**2)>&
      (sqrt((ii*1._pr-midline(l+1,1))**2+(jj*1._pr-midline(l+1,2))**2)>=&
      sqrt((ii*1._pr-midline(l,1))**2+(jj*1._pr-midline(l,2))**2))) then
      if (skel2(midline(l,1),midline(l,2))==0) then
              skeltmp(midline(l,1),midline(l,2)) = 0
      write(*,*) "AIE 22"
      else
              iinew = midline(l,1)
              jjnew = midline(l,2)
              bool22 = 1
              bool = 1
      endif
              !skel2(midline(l+1,1),midline(l+1,2)) = 0
      endif
      if ((skel2(midline(l,1),midline(l,2))==1).and.(skel2(midline(l+1,1),midline(l+1,2))==0)) bool = 1   
      !if ((((ii==-1).and.(jj==-1)).or.(skeltmp(midline(l,1),midline(l,2))==1)).and.(skel2(midline(l,1),midline(l,2))==1).and.&
      !(skel2(midline(l+1,1),midline(l+1,2))==0)) bool = 1   
      l=l+1
    enddo
    if (bool==0) write(*,*) "error ouch"

    if (l>size(midline,1)-nl+1) then
      !if ((sqrt((ii*1._pr-midline(l-1,1))**2+(jj*1._pr-midline(l-1,2))**2)<10).or.((ii==-1).and.(jj==-1))) then
      !if ((sqrt((ii*1._pr-midline(l-1,1))**2+(jj*1._pr-midline(l-1,2))**2)<25).or.((ii==-1).and.(jj==-1))) then
      !if ((sqrt((ii*1._pr-midline(l-1,1))**2+(jj*1._pr-midline(l-1,2))**2)<2).or.((ii==-1).and.(jj==-1))) then
      if ((sqrt((ii*1._pr-midline(l-1,1))**2+(jj*1._pr-midline(l-1,2))**2)<2.001).or.((ii==-1).and.(jj==-1))) then
      !ii = midline(l-1,1)
      !jj = midline(l-1,2)
      ii = midline(l,1)
      jj = midline(l,2)
      if (bool22==1) then
              ii = iinew
              jj = jjnew
      endif
      skel = skeltmp
      else
      skel = skeltmp
      write(*,*) "OUCH  ",sqrt((ii*1._pr-midline(l-1,1))**2+(jj*1._pr-midline(l-1,2))**2)
      ii = midline(l,1)
      jj = midline(l,2)
      endif
    else
      !if ((sqrt((ii*1._pr-midline(l,1))**2+(jj*1._pr-midline(l,2))**2)<10).or.((ii==-1).and.(jj==-1))) then
      !if ((sqrt((ii*1._pr-midline(l,1))**2+(jj*1._pr-midline(l,2))**2)<25).or.((ii==-1).and.(jj==-1))) then
      !if ((sqrt((ii*1._pr-midline(l,1))**2+(jj*1._pr-midline(l,2))**2)<2).or.((ii==-1).and.(jj==-1))) then
      if ((sqrt((ii*1._pr-midline(l,1))**2+(jj*1._pr-midline(l,2))**2)<2.001).or.((ii==-1).and.(jj==-1))) then
      ii = midline(l,1)
      jj = midline(l,2)
      skel = skeltmp
      else
      write(*,*) "OUCH22  ",sqrt((ii*1._pr-midline(l,1))**2+(jj*1._pr-midline(l,2))**2)
      endif
    endif
      write(*,*) "Last tracked midline point ",ii," ",jj


    do l=1,size(midline,1)
       midlinetmp(l,1) = midline(size(midline,1)-l+1,1)
       midlinetmp(l,2) = midline(size(midline,1)-l+1,2)
    end do
    midline = midlinetmp

  end subroutine cutheadskel

!!  SUBROUTINE ZST(DOT)	!Attempts to thin out thick lines.
!!    INTEGER DOT(:,:)	!The image in an array, rows down the page.
!!    INTEGER,DIMENSION(SIZE(DOT,1)*SIZE(DOT,2),2) ::  WHACK !Allow a whack for every dot.
!!    INTEGER WHACKCOUNT	!Counts up those to be wiped out.
!!    LOGICAL WHACKED	!Notes if any have been.
!!    INTEGER STEP,I,N,J,M	!Assistants.
!!    INTEGER,DIMENSION(9) :: D9 	!Holds a 3x3 portion.
!!    INTEGER,DIMENSION(3,2) :: HIT1,HIT2
!!    INTEGER ITER
!!
!!    HIT1 = TRANSPOSE(RESHAPE((/2,6,8, 4,2,6/),(/SIZE(HIT1,2),SIZE(HIT1,1)/)))	!Two stages.
!!    HIT2 = TRANSPOSE(RESHAPE((/4,8,6, 2,4,8/),(/SIZE(HIT2,2),SIZE(HIT2,1)/)))	!Each with two hit lists.
!!    N = SIZE(DOT,1)	!Number of rows.
!!    M = SIZE(DOT,2)	!Number of columns.
!!    WHACKED = .FALSE.	!No damage so far.
!!    ITER = 1
!!    DO WHILE (WHACKED.OR.(ITER==1))
!!      WHACKED = .FALSE.	!No damage so far.
!!      DO STEP = 1,2		!Each pass is in two stages.
!!         WHACKCOUNT = 0	!No dots have been selected for whitewashing.
!!         DO I = 2,N - 1	!Scan down the rows.
!!            DO J = 2,M - 1	!And the columns. Interior dots only.
!!               IF (DOT(I,J).NE.0) THEN	!Rule 0: Is the dot black? Eight neighbours are present due to loop control.
!!                  D9(1:3) = DOT(I - 1,J - 1:J + 1)	!Yes. Form a 3x3 mesh.	1 2 3  not  9 2 3
!!                  D9(4:6) = DOT(I    ,J - 1:J + 1)	!As a 1-D array.	4 5 6       8 1 4
!!                  D9(7:9) = DOT(I + 1,J - 1:J + 1)	!For eased access.	7 8 9       7 6 5
!!                  CALL INSPECT(D9,HIT1(1,STEP),HIT2(1,STEP))	!Apply rules one to four, as specified.
!!               END IF			!So much for a black dot.
!!            END DO		!On to the next column.
!!         END DO		!On to the next row.
!!         IF (WHACKCOUNT.GT.0) THEN	!Are any to be wiped out?
!!            DO I = 1,WHACKCOUNT		!Yes!
!!               DOT(WHACK(I,1),WHACK(I,2)) = 0		!One by one.
!!            END DO				!On to the next victim.
!!            WHACKED = .TRUE.			!There has been a change.
!!         END IF			!So much for changes.
!!      END DO		!On to the second stage.
!!      ITER = ITER+1
!!    END DO
!!  CONTAINS
!!
!!
!!  SUBROUTINE INSPECT(BLOB,HIT1,HIT2)	!Inspect a 3x3 piece according to the four levels of tests as specified.
!!    INTEGER BLOB(9)		!The piece. BLOB(5) is DOT(I,J), and is expected to be 1.
!!    INTEGER HIT1(3),HIT2(3)	!Two hit lists.
!!    INTEGER TWIRL(9)		!traces the periphery of the piece.
!!    PARAMETER (TWIRL = (/2,3,6,9,8,7,4,1,2/))	!Cycle around the periphery.
!!    INTEGER B	!A counter.			!Rule:
!!    B = SUM(BLOB) - BLOB(5)			!1: Count the neighbours having one, not zero.
!!    IF (2 <= B .AND. B <= 6) THEN		!   The test. Can't have 2 <= B <= 6, alas.
!!       IF (COUNT(BLOB(TWIRL(1:8)).LT.BLOB(TWIRL(2:9))) .EQ.1) THEN		!2: Counting transitions!   The test of 0 --> positive.
!!          IF (ANY(BLOB(HIT1).EQ.0)) THEN		!3: At least one must be white.
!!             IF (ANY(BLOB(HIT2).EQ.0)) THEN	!4: Of two sets of three.
!!                WHACKCOUNT = WHACKCOUNT + 1			!Another one down!
!!                WHACK(WHACKCOUNT,1) = I
!!                WHACK(WHACKCOUNT,2) = J		!This is the place.
!!             END IF				!Now back out of the nested IF-statements.
!!          END IF				!Since the tests must all be passed
!!       END IF			!Rather than say three out of four.
!!    END IF		!For the given method.
!!  END SUBROUTINE INSPECT!That was weird.
!!  END SUBROUTINE ZST	!But so it goes.

!subroutine body_normal(courbe)
!  implicit none
!  integer,dimension(:,:),intent(inout) :: courbe
!  integer :: k,l
!  real(pr),dimension(3) :: te,ts
!  real(pr) :: normn
!
!  for(k=0;k<obs->ns[ib]-1;k++){
!    for(l=0;l<obs->ne[ib]-1;l++){
!      
!      ts[1]=(obs->geom.x[ib][k+1][l]+obs->geom.x[ib][k+1][l+1])/2.-(obs->geom.x[ib][k][l]+obs->geom.x[ib][k][l+1])/2;
!      ts[2]=(obs->geom.y[ib][k+1][l]+obs->geom.y[ib][k+1][l+1])/2.-(obs->geom.y[ib][k][l]+obs->geom.y[ib][k][l+1])/2;
!      ts[3]=(obs->geom.z[ib][k+1][l]+obs->geom.z[ib][k+1][l+1])/2.-(obs->geom.z[ib][k][l]+obs->geom.z[ib][k][l+1])/2;
!      ts=obs->geom.z[k+1]/2.-obs->geom.z[k]/2;
!      
!      te[1]=(obs->geom.x[ib][k][l+1]+obs->geom.x[ib][k+1][l+1])/2.-(obs->geom.x[ib][k][l]+obs->geom.x[ib][k+1][l])/2.;
!      te[2]=(obs->geom.y[ib][k][l+1]+obs->geom.y[ib][k+1][l+1])/2.-(obs->geom.y[ib][k][l]+obs->geom.y[ib][k+1][l])/2.;
!      te[3]=(obs->geom.z[ib][k][l+1]+obs->geom.z[ib][k+1][l+1])/2.-(obs->geom.z[ib][k][l]+obs->geom.z[ib][k+1][l])/2.;
!      
!      obs->geom.n[1][k][l]=-ts[2]*te[3]+ts[3]*te[2];
!      obs->geom.n[2][k][l]=-ts[3]*te[1]+ts[1]*te[3];
!      obs->geom.n[3][k][l]=-ts[1]*te[2]+ts[2]*te[1];
!      
!      normn=pow(pow(obs->geom.n[1][k][l],2.)+pow(obs->geom.n[2][k][l],2.)+pow(obs->geom.n[3][k][l],2.),0.5);
!      
!      obs->geom.n[1][k][l]=obs->geom.n[1][k][l]/normn;
!      obs->geom.n[2][k][l]=obs->geom.n[2][k][l]/normn;
!      obs->geom.n[3][k][l]=obs->geom.n[3][k][l]/normn;
!    }
!  }
!end subroutine body_normal
!
!subroutine body_rotationdef(courbe)
!  implicit none
!  integer,dimension(:,:),intent(inout) :: courbe
!  integer :: k,l
!  real(pr),dimension(3) :: te,ts
!  real(pr) :: dS,dk,dl,dx,dy,dz
!  real(pr) :: x0,y0,z0,x0m1,y0m1,z0m1,deltax,deltay,deltaz;
!  
!  obs->mouv.Theta_def[ib][1]=0.;
!  obs->mouv.Theta_def[ib][2]=0.;
!  obs->mouv.Theta_def[ib][3]=0.;
!
!  body_normal(obs);
!
!  for(k=0;k<obs->ns[ib]-1;k++){
!    for(l=0;l<obs->ne[ib]-1;l++){
!
!      dx=(obs->geom.x0[ib][k+1][l]+obs->geom.x0[ib][k+1][l+1])/2.-(obs->geom.x0[ib][k][l]+obs->geom.x0[ib][k][l+1])/2.;
!      dy=(obs->geom.y0[ib][k+1][l]+obs->geom.y0[ib][k+1][l+1])/2.-(obs->geom.y0[ib][k][l]+obs->geom.y0[ib][k][l+1])/2.;
!      dz=(obs->geom.z0[ib][k+1][l]+obs->geom.z0[ib][k+1][l+1])/2.-(obs->geom.z0[ib][k][l]+obs->geom.z0[ib][k][l+1])/2.;
!      dk=sqrt(pow(dx,2.)+pow(dy,2.)+pow(dz,2.));
! 
!      dx=(obs->geom.x0[ib][k][l+1]+obs->geom.x0[ib][k+1][l+1])/2.-(obs->geom.x0[ib][k][l]+obs->geom.x0[ib][k+1][l])/2.;
!      dy=(obs->geom.y0[ib][k][l+1]+obs->geom.y0[ib][k+1][l+1])/2.-(obs->geom.y0[ib][k][l]+obs->geom.y0[ib][k+1][l])/2.;
!      dz=(obs->geom.z0[ib][k][l+1]+obs->geom.z0[ib][k+1][l+1])/2.-(obs->geom.z0[ib][k][l]+obs->geom.z0[ib][k+1][l])/2.;
!      dl=sqrt(pow(dx,2.)+pow(dy,2.)+pow(dz,2.));
!  
!      dS=dk*dl;
!
!      x0=(obs->geom.x0[ib][k][l]+obs->geom.x0[ib][k+1][l]+obs->geom.x0[ib][k][l+1]+obs->geom.x0[ib][k+1][l+1])/4.;
!      y0=(obs->geom.y0[ib][k][l]+obs->geom.y0[ib][k+1][l]+obs->geom.y0[ib][k][l+1]+obs->geom.y0[ib][k+1][l+1])/4.;
!      z0=(obs->geom.z0[ib][k][l]+obs->geom.z0[ib][k+1][l]+obs->geom.z0[ib][k][l+1]+obs->geom.z0[ib][k+1][l+1])/4.;
!
!      x0m1=(obs->geom.x0m1[ib][k][l]+obs->geom.x0m1[ib][k+1][l]+obs->geom.x0m1[ib][k][l+1]+obs->geom.x0m1[ib][k+1][l+1])/4.;
!      y0m1=(obs->geom.y0m1[ib][k][l]+obs->geom.y0m1[ib][k+1][l]+obs->geom.y0m1[ib][k][l+1]+obs->geom.y0m1[ib][k+1][l+1])/4.;
!      z0m1=(obs->geom.z0m1[ib][k][l]+obs->geom.z0m1[ib][k+1][l]+obs->geom.z0m1[ib][k][l+1]+obs->geom.z0m1[ib][k+1][l+1])/4.;
!
!      deltax=x0-x0m1;
!      deltay=y0-y0m1;
!      deltaz=z0-z0m1;
!
!      obs->mouv.Theta_def[ib][1]=obs->mouv.Theta_def[ib][1]+(deltay*obs->geom.n[3][k][l]-deltaz*obs->geom.n[2][k][l])*dS;
!      obs->mouv.Theta_def[ib][2]=obs->mouv.Theta_def[ib][2]+(deltaz*obs->geom.n[1][k][l]-deltax*obs->geom.n[3][k][l])*dS;
!      obs->mouv.Theta_def[ib][3]=obs->mouv.Theta_def[ib][3]+(deltax*obs->geom.n[2][k][l]-deltay*obs->geom.n[1][k][l])*dS;
!      obs->mouv.Theta_def=obs->mouv.Theta_def+(deltaz*obs->geom.n[1][k][l]-deltax*obs->geom.n[3][k][l])*dS;
!      
!    }
!  }
!  obs->mouv.Theta_def[ib][1]=obs->mouv.Theta_def[ib][1]/(2*obs->geom.V[ib]);
!  obs->mouv.Theta_def[ib][2]=obs->mouv.Theta_def[ib][2]/(2*obs->geom.V[ib]);
!  obs->mouv.Theta_def[ib][3]=obs->mouv.Theta_def[ib][3]/(2*obs->geom.V[ib]);
!
!  PetscPrintf(PETSC_COMM_WORLD," tx=%g ty=%g tz=%g\n",180/3.14*obs->mouv.Theta_def[ib][1],180/3.14*obs->mouv.Theta_def[ib][2],180/3.14*obs->mouv.Theta_def[ib][3]);
!end subroutine body_rotationdef


subroutine body_rotationdef(courbe,xg,yg,alpha)
  implicit none
  real(pr),dimension(:,:),intent(inout) :: courbe
  real(pr),intent(inout) :: xg,yg
  real(pr),intent(out) :: alpha
  integer :: l,nb
  real(pr) :: dt,eps,dalpha,d,sinm,cosm,PI

  PI=acos(-1._pr)
  alpha=0._pr
  !sinm=0._pr
  eps=1e-12
  dt=0._pr
  nb=0._pr
  !cosm=0._pr
  do l=1,size(courbe,1)
    dalpha=atan((courbe(l,2)-yg)/(courbe(l,1)-xg+eps))
    d=sqrt((courbe(l,2)-yg)**2+(courbe(l,1)-xg+eps)**2)
    if ((abs(courbe(l,2)-yg)<eps).and.(abs(courbe(l,1)-xg)<eps)) then
       dalpha=0
    elseif (abs(abs(dalpha)-pi/2)<0.1*PI/2) then
        dalpha=atan((courbe(l,1)-xg)/(courbe(l,2)-yg+eps))
        d=sqrt((courbe(l,2)-yg+eps)**2+(courbe(l,1)-xg)**2)
        
        if ((courbe(l,1)-xg)*(courbe(l,2)-yg+eps)>0) then
            dalpha = PI/2 - dalpha
        else
            dalpha = -PI/2 - dalpha
        endif
        nb=nb+1
    endif
    dt=dt+d
    !sinm=sinm+(courbe(l,2)-yg)*d
    !cosm=cosm+(courbe(l,1)-xg)*d
    !dalpha=abs(dalpha)
    alpha=alpha+dalpha*d
  enddo
  !sinm=sinm/dt!size(courbe,1)
  !cosm=cosm/dt!size(courbe,1)
  !if (cosm*sinm<0._pr) then
  !      alpha=-alpha
  !endif
  alpha=alpha/dt

  !write(*,*) "ALPHA  ",alpha," ",xg," ",yg," ",nb

end subroutine body_rotationdef

subroutine body_rotationdefPS(courberef,courbe,xg,yg,alphadef,dx)
  implicit none
  real(pr),dimension(:,:),intent(inout) :: courbe,courberef
  real(pr),intent(inout) :: xg,yg,dx
  real(pr),intent(out) :: alphadef
  integer :: l,nb,iter,maxiter
  real(pr) :: alpha,dt,eps,dalpha,d,sinm,cosm,PI
  real(pr) :: PS,PSprev,sgn

  PI=acos(-1._pr)
  alpha=0._pr
  eps=1e-12
  dt=0._pr
  nb=0._pr
  do l=1,size(courbe,1)
    dalpha=atan((courbe(l,2)/dx-yg/dx)/(courbe(l,1)/dx-xg/dx+eps))
    d=sqrt((courbe(l,2)/dx-yg/dx)**2+(courbe(l,1)/dx-xg/dx+eps)**2)
    if ((abs(courbe(l,2)/dx-yg/dx)<eps).and.(abs(courbe(l,1)/dx-xg/dx)<eps)) then
       dalpha=0
    elseif (abs(abs(dalpha)-PI/2)<0.1*PI/2) then
        dalpha=atan((courbe(l,1)/dx-xg/dx)/(courbe(l,2)/dx-yg/dx+eps))
        d=sqrt((courbe(l,2)/dx-yg/dx+eps)**2+(courbe(l,1)/dx-xg/dx)**2)
        
        if ((courbe(l,1)/dx-xg/dx)*(courbe(l,2)/dx-yg/dx+eps)>0) then
            dalpha = PI/2 - dalpha
        else
            dalpha = -PI/2 - dalpha
        endif
        nb=nb+1
    endif
    dt=dt+d
    alpha=alpha+dalpha*d
  enddo
  alpha=alpha/dt
  alphadef=alpha

  PS=0._pr
  !do l=1,size(courbe,1)
  !  !PS = PS + courbe(l,1)/dx*courberef(l,1)/dx + courbe(l,2)/dx*courberef(l,2)/dx
  !  PS = PS + (courbe(l,1)-courberef(l,1))/dx*(courbe(l,1)-courberef(l,1))/dx&
  !  + (courbe(l,2)-courberef(l,2))/dx*(courbe(l,2)-courberef(l,2))/dx
  !enddo
  do l=1,size(courbe,1)-1
    PS = PS + (courbe(l+1,1)-courbe(l,1))/dx*(courberef(l+1,1)-courberef(l,1))/dx&
    + (courbe(l+1,2)-courbe(l,2))/dx*(courberef(l+1,2)-courberef(l,2))/dx
  enddo
  PS = PS/(size(courbe,1)-1)
  write(*,*) "PS11  0 ",PS
  if (PS<0._pr) then
          PS = abs(PS)
          alpha = alpha+PI
  endif
  alpha=10._pr*PI/180
  alphadef=alpha
  !call body_rotating_theta(courbe,xg,yg,-alpha)
  !PS=0._pr
  !!do l=1,size(courbe,1)
  !!  !PS = PS + courbe(l,1)/dx*courberef(l,1)/dx + courbe(l,2)/dx*courberef(l,2)/dx
  !!  PS = PS + (courbe(l,1)-courberef(l,1))/dx*(courbe(l,1)-courberef(l,1))/dx&
  !!  + (courbe(l,2)-courberef(l,2))/dx*(courbe(l,2)-courberef(l,2))/dx
  !!enddo
  !do l=1,size(courbe,1)-1
  !  PS = PS + (courbe(l+1,1)-courbe(l,1))/dx*(courberef(l+1,1)-courberef(l,1))/dx&
  !  + (courbe(l+1,2)-courbe(l,2))/dx*(courberef(l+1,2)-courberef(l,2))/dx
  !enddo
  !PS = PS/(size(courbe,1)-1)
  !write(*,*) "PS33  ",alpha*180/PI," ",PS
  !if (PS<0._pr) then
  !        PS = abs(PS)
  !        alpha = alpha+PI
  !endif

  iter = 0
  PSprev = 0._pr!1E9 !0._pr
  alphadef=0._pr
  maxiter = 100
  !do while(((abs(PSprev-PS)>0.1).or.(iter==0)).and.(iter<=maxiter))
  do while(((abs(PSprev-PS)>eps*PSprev).or.(iter==0)).and.(iter<=maxiter))
  !do iter=1,20

    PSprev = PS
    call body_rotating_theta(courbe,xg,yg,-alpha)
    PS=0._pr
    !do l=1,size(courbe,1)
    !  !PS = PS + courbe(l,1)/dx*courberef(l,1)/dx + courbe(l,2)/dx*courberef(l,2)/dx
    !  PS = PS + (courbe(l,1)-courberef(l,1))/dx*(courbe(l,1)-courberef(l,1))/dx&
    !  + (courbe(l,2)-courberef(l,2))/dx*(courbe(l,2)-courberef(l,2))/dx
    !enddo
    do l=1,size(courbe,1)-1
      PS = PS + (courbe(l+1,1)-courbe(l,1))/dx*(courberef(l+1,1)-courberef(l,1))/dx&
      + (courbe(l+1,2)-courbe(l,2))/dx*(courberef(l+1,2)-courberef(l,2))/dx
    enddo
    PS = PS/(size(courbe,1)-1)
    !if (PS<0._pr) then
    !        PS = abs(PS)
    !        sgn = alpha/abs(alpha)
    !        alpha = -sgn*PI+alpha
    !        write(*,*) "PSnegatif"
    !endif
    alphadef=alphadef+alpha
    if (abs(PSprev-PS)<eps*PSprev) alphadef=alphadef-alpha*0.5_pr
    write(*,*) "ITER  ",iter," ",alpha*180/PI," ",PS," ",PSprev," ",abs(PSprev-PS)," ",alphadef*180/PI
    if (PS > PSprev) then
    !if (abs(PS) > abs(PSprev)) then
      alpha = alpha
    else
      alpha = -alpha*0.5_pr
    endif

    iter = iter+1
  enddo
  !alphadef = modulo(alphadef,2*PI)-PI
    

  write(*,*) "ERREUR  ",modulo(alphadef,2*PI)-PI," ",PS," ",PSprev," ",abs(PSprev-PS)

end subroutine body_rotationdefps

subroutine body_rotating_theta(courbe,xg,yg,ang)
  implicit none
  real(pr),dimension(:,:),intent(inout) :: courbe
  real(pr),intent(inout) :: xg,yg
  real(pr),intent(in) :: ang
  integer :: l,method
  real(pr) :: xy,yy,x0,y0
  !double         R[2][2],Atemp[2][2],Afin[2][2];
 
  method=2
 
!  if (method==1){  
  do l=1,size(courbe,1)
       x0=courbe(l,1)
       y0=courbe(l,2)
       xy=(x0-xg)*cos(ang)+(y0-yg)*sin(ang)+xg
       yy=-(x0-xg)*sin(ang)+(y0-yg)*cos(ang)+yg
       courbe(l,1)=xy
       courbe(l,2)=yy
  enddo
!  }
!   if (method==2){
!     R[0][0]=+cos(ang); R[0][1]=+sin(ang);
!     R[1][0]=-sin(ang); R[1][1]=+cos(ang);
!     Afin[0][0]=+cos(ang); Afin[0][1]=+sin(ang);
!     Afin[1][0]=-sin(ang); Afin[1][1]=+cos(ang);
!
!    body_masscenter(obs);
!    for(k=0;k<obs->geom.no[ib];k++){
!         obs->geom.x[ib][k]=obs->geom.x[ib][k]-obs->geom.xg[ib];
!         obs->geom.y[ib][k]=obs->geom.y[ib][k]-obs->geom.yg[ib];
!     }
!    for(k=0;k<obs->geom.no[ib];k++){
!         obs->geom.x0[ib][k]=Afin[0][0]*obs->geom.x[ib][k]+Afin[0][1]*obs->geom.y[ib][k];
!         obs->geom.y0[ib][k]=Afin[1][0]*obs->geom.x[ib][k]+Afin[1][1]*obs->geom.y[ib][k];
!     }
!    for(k=0;k<obs->geom.no[ib];k++){
!         obs->geom.x[ib][k]=obs->geom.x0[ib][k]+obs->geom.xg[ib];
!         obs->geom.y[ib][k]=obs->geom.y0[ib][k]+obs->geom.yg[ib];
!     }
!   }
end subroutine body_rotating_theta

subroutine body_masscenter(courbe,xg,yg)
  implicit none
  real(pr),dimension(:,:),intent(inout) :: courbe
  real(pr),intent(inout) :: xg,yg
  integer :: l

  xg=0._pr
  yg=0._pr
  do l=1,size(courbe,1)
    xg=xg+courbe(l,1)
    yg=yg+courbe(l,2)
  enddo
  xg=xg/size(courbe,1)
  yg=yg/size(courbe,1)

end subroutine body_masscenter

subroutine body_masscentering(courbe,xg,yg,xgref,ygref)
  implicit none
  real(pr),dimension(:,:),intent(inout) :: courbe
  real(pr),intent(inout) :: xg,yg,xgref,ygref
  integer :: l

  do l=1,size(courbe,1)
    courbe(l,1)=courbe(l,1)+xgref-xg
    courbe(l,2)=courbe(l,2)+ygref-yg
  enddo

end subroutine body_masscentering

subroutine compute_thetadef(courbe,alpha,calpha,salpha,kt)
  implicit none
  real(pr),dimension(:,:),intent(inout) :: courbe
  real(pr),intent(inout) :: alpha
  real(pr),intent(inout) :: calpha,salpha
  integer,intent(in) :: kt
  integer :: l,nb
  real(pr) :: PI,PS1,PS2,PV1,PV2,beta1,beta2,eps,norml,normlm,normlp,dx

  PI=acos(-1._pr)
!  alpha=0.
  nb=0
  eps=1e-12
  dx=0.0256*1e-3

!  do l=2,size(courbe,1)-2
  do l=2,size(courbe,1)-1
    PS1 = (courbe(l+1,1)-courbe(l,1))*(courbe(l-1,1)-courbe(l,1)) + (courbe(l+1,2)-courbe(l,2))*(courbe(l-1,2)-courbe(l,2))
    PV1 = (courbe(l+1,1)-courbe(l,1))*(courbe(l-1,2)-courbe(l,2)) - (courbe(l+1,2)-courbe(l,2))*(courbe(l-1,1)-courbe(l,1))
    norml = sqrt((courbe(l+1,1)-courbe(l,1))*(courbe(l+1,1)-courbe(l,1))&
    + (courbe(l+1,2)-courbe(l,2))*(courbe(l+1,2)-courbe(l,2)))
    normlm = sqrt((courbe(l-1,1)-courbe(l,1))*(courbe(l-1,1)-courbe(l,1))&
    + (courbe(l-1,2)-courbe(l,2))*(courbe(l-1,2)-courbe(l,2)))
!!    normlp = sqrt((courbe(l+1,1)-courbe(l+2,1))*(courbe(l+1,1)-courbe(l+2,1))&
 !   + (courbe(l+1,2)-courbe(l+2,2))*(courbe(l+1,2)-courbe(l+2,2)))
    !PS1 = PS1/dx/dx
    !norml = norml/dx
    !normlm = normlm/dx
    !normlp = normlp/dx
    beta1 = atan(PV1/(PS1+eps))


    !if (abs(PS1)<eps*norml*normlm) then
    if (abs(abs(beta1)-PI/2)<0.1*PI/2) then
        beta1=0
        if (PS1*PV1>0) then
        beta1=PI/2 - atan(PS1/(PV1+eps))
        else
        beta1=-PI/2 - atan(PS1/(PV1+eps))
        endif
        !write(*,*) "PS1NULL ",PS1/norml/normlm
        write(*,*) "TANGENTEpb1 ",abs(abs(beta1)-PI/2)
    endif
    !else
    !    beta1 = atan(PV1/PS1)
    !endif
    !if (abs(abs(beta1)-PI/2)<0.1*PI/2) then
    !        write(*,*) "TANGENTEpb1 ",abs(abs(beta1)-PI/2)
    !endif

    !if (PV1<0) then
    !    beta1 = -acos(PS1/(norml*normlm+eps))
    !    !beta1 = acos(PS1/(norml*normlm))
    !else
    !    beta1 = acos(PS1/(norml*normlm+eps))
    !    !beta1 = -acos(PS1/(norml*normlm))
    !endif

    !if (abs(abs(beta1)-PI/2)<20*PI/180) then
    !        !sgn = beta1/abs(beta1)
    !        !beta1 = beta1 - sgn*PI/2
    !        !if (PV1/PS1<0) then
    !        if (beta1>0) then
    !                beta1 = -PI/2 + beta1
    !        else
    !                beta1 = PI/2 + beta1
    !        endif
    !endif
    !if (abs(abs(beta1)-PI/2)<0.1*PI/2) then
    !        beta1 = atan(PS1/PV1)
    !        if (beta1>0) then
    !                beta1 = PI/2 - beta1
    !        else
    !                beta1 = -PI/2 - beta1
    !        endif
    !endif
    !if (beta1<0.) then
    !        beta1 = beta1 + PI
    !else
    !        beta1 = beta1 - PI
    !endif
    !beta1 = beta1 - PI 
    !if (beta1>0.) beta1 = beta1 - PI

!    PS2 = (courbe(l+2,1)-courbe(l+1,1))*(courbe(l,1)-courbe(l+1,1)) + (courbe(l+2,2)-courbe(l+1,2))*(courbe(l,2)-courbe(l+1,2))
!    PV2 = (courbe(l+2,1)-courbe(l+1,1))*(courbe(l,2)-courbe(l+1,2)) - (courbe(l+2,2)-courbe(l+1,2))*(courbe(l,1)-courbe(l+1,1))
!    !PS2 = PS2/dx/dx
!    beta2 = atan(PV2/(PS2+eps))
!
!    !if (abs(PS2)<eps*norml*normlp) then
!    if (abs(abs(beta2)-PI/2)<0.1*PI/2) then
!        if (PS2*PV2>0) then
!        beta2=PI/2 - atan(PS2/(PV2+eps))
!        else
!        beta2=-PI/2 - atan(PS2/(PV2+eps))
!        endif
!        !write(*,*) "PS2NULL ",PS2/norml/normlp
!        write(*,*) "TANGENTEpb2 ",abs(abs(beta2)-PI/2)
!    endif
    !else
    !    beta2 = atan(PV2/PS2)
    !endif
    !if (abs(abs(beta2)-PI/2)<0.1*PI/2) then
    !        write(*,*) "TANGENTEpb2 ",abs(abs(beta2)-PI/2)
    !endif
    !if (PV2<0) then
    !    beta2 = -acos(PS2/(norml*normlp+eps))
    !    !beta2 = acos(PS2/(norml*normlp))
    !else
    !    beta2 = acos(PS2/(norml*normlp+eps))
    !    !beta2 = -acos(PS2/(norml*normlp))
    !endif
    !if (abs(abs(beta2)-PI/2)<20*PI/180) then
    !        !sgn = beta2/abs(beta2)
    !        !beta2 = beta2 - sgn*PI/2
    !        !if (PV1/PS1<0) then
    !        if (beta2>0) then
    !                beta2 = -PI/2 + beta2
    !        else
    !                beta2 = PI/2 + beta2
    !        endif
    !endif
    !if (abs(abs(beta2)-PI/2)<0.1*PI/2) then
    !        beta2 = atan(PS1/PV1)
    !        if (beta2>0) then
    !                beta2 = PI/2 - beta2
    !        else
    !                beta2 = -PI/2 - beta2
    !        endif
    !endif
    !if (beta2<0.) then
    !        beta2 = beta2 + PI
    !else
    !        beta2 = beta2 - PI
    !endif
    !beta2 = PI - beta2 

    !alpha=alpha + abs(beta2) - abs(beta1)
!    alpha=alpha + beta2 - beta1
!    alpha=alpha - beta2 - beta1

    !alpha=alpha + beta2 - beta1
!    alpha=alpha - beta2 - beta1
    alpha=alpha - beta1
    nb=nb+1
  enddo
!  l = size(courbe,1)-1
!  norml = sqrt((courbe(l-1,1)-courbe(l,1))*(courbe(l-1,1)-courbe(l,1))&
!  + (courbe(l-1,2)-courbe(l,2))*(courbe(l-1,2)-courbe(l,2)))
!  normlm = sqrt((courbe(l-1,1)-courbe(l-2,1))*(courbe(l-1,1)-courbe(l-2,1))&
!  + (courbe(l-1,2)-courbe(l-2,2))*(courbe(l-1,2)-courbe(l-2,2)))
!  normlp = sqrt((courbe(l+1,1)-courbe(l,1))*(courbe(l+1,1)-courbe(l,1))&
!  + (courbe(l+1,2)-courbe(l,2))*(courbe(l+1,2)-courbe(l,2)))
!  PS1 = (courbe(l,1)-courbe(l-1,1))*(courbe(l-2,1)-courbe(l-1,1)) + (courbe(l,2)-courbe(l-1,2))*(courbe(l-2,2)-courbe(l-1,2))
!  PV1 = (courbe(l,1)-courbe(l-1,1))*(courbe(l-2,2)-courbe(l-1,2)) - (courbe(l,2)-courbe(l-1,2))*(courbe(l-2,1)-courbe(l-1,1))
!  !PS1 = (courbe(l,1)-courbe(l-1,1))*(courbe(l-2,1)-courbe(l-1,1)) + (courbe(l-2,2)-courbe(l-1,2))*(courbe(l-2,2)-courbe(l-1,2))
!  !PV1 = (courbe(l,1)-courbe(l-1,1))*(courbe(l-2,2)-courbe(l-1,2)) - (courbe(l,2)-courbe(l-1,2))*(courbe(l-2,1)-courbe(l-1,1))
!  !PS1 = PS1/dx/dx
!  beta1 = atan(PV1/(PS1+eps))
!  
!
!  !norml = norml/dx
!  !normlm = normlm/dx
!  !normlp = normlp/dx
!  !if (abs(PS1)<eps*norml*normlm) then
!  if (abs(abs(beta1)-PI/2)<0.1*PI/2) then
!        if (PS1*PV1>0) then
!        beta1=PI/2-atan(PS1/(PV1+eps))
!        else
!        beta1=-PI/2-atan(PS1/(PV1+eps))
!        endif
!        !write(*,*) "PS1NULL ",PS1/norml/normlm
!        write(*,*) "TANGENTEpb3 ",abs(abs(beta1)-PI/2)
!  endif
!  !else
!  !      beta1 = atan(PV1/PS1)
!  !endif
!  !if (abs(abs(beta1)-PI/2)<0.1*PI/2) then
!  !        write(*,*) "TANGENTEpb3 ",abs(abs(beta1)-PI/2)
!  !endif
!  !if (PV1<0) then
!  !      beta1 = -acos(PS1/(norml*normlm+eps))
!  !      !beta1 = acos(PS1/(norml*normlm))
!  !else
!  !      beta1 = acos(PS1/(norml*normlm+eps))
!  !      !beta1 = -acos(PS1/(norml*normlm))
!  !endif
!
!  !if (abs(abs(beta1)-PI/2)<0.1*PI/2) then
!  !        beta1 = atan(PS1/PV1)
!  !        if (beta1>0) then
!  !                beta1 = PI/2 - beta1
!  !        else
!  !                beta1 = -PI/2 - beta1
!  !        endif
!  !endif
!  !if (beta1<0.) then
!  !        beta1 = beta1 + PI
!  !else
!  !        beta1 = beta1 - PI
!  !endif
!  !beta1 = beta1 - PI 
!
!  PS2 = (courbe(l+1,1)-courbe(l,1))*(courbe(l-1,1)-courbe(l,1)) + (courbe(l+1,2)-courbe(l,2))*(courbe(l-1,2)-courbe(l,2))
!  PV2 = (courbe(l+1,1)-courbe(l,1))*(courbe(l-1,2)-courbe(l,2)) - (courbe(l+1,2)-courbe(l,2))*(courbe(l-1,1)-courbe(l,1))
!  !PS2 = PS2/dx/dx
!  beta2 = atan(PV2/(PS2+eps))
!
!  !if (PV2<0) then
!  !      beta2 = -acos(PS2/(norml*normlp+eps))
!  !      !beta2 = acos(PS2/(norml*normlp))
!  !else
!  !      beta2 = acos(PS2/(norml*normlp+eps))
!  !      !beta2 = -acos(PS2/(norml*normlp))
!  !endif
!  !if (abs(PS2)<eps*norml*normlp) then
!  if (abs(abs(beta2)-PI/2)<0.1*PI/2) then
!        if (PS2*PV2>0) then
!        beta2=PI/2-atan(PS2/(PV2+eps))
!        else
!        beta2=PI/2-atan(PS2/(PV2+eps))
!        endif
!        !write(*,*) "PS2NULL ",PS2/norml/normlp
!        write(*,*) "TANGENTEpb4 ",abs(abs(beta2)-PI/2)
!  endif
!  !else
!  !      beta2 = atan(PV2/PS2)
!  !endif
!  !if (abs(abs(beta2)-PI/2)<0.1*PI/2) then
!  !        write(*,*) "TANGENTEpb4 ",abs(abs(beta2)-PI/2)
!  !endif
!  !if (abs(abs(beta2)-PI/2)<0.1*PI/2) then
!  !        beta2 = atan(PS1/PV1)
!  !        if (beta2>0) then
!  !                beta2 = PI/2 - beta2
!  !        else
!  !                beta2 = -PI/2 - beta2
!  !        endif
!  !endif
!  !if (beta2<0.) then
!  !        beta2 = beta2 + PI
!  !else
!  !        beta2 = beta2 - PI
!  !endif
!  !beta2 = PI - beta2 
!
!!  !alpha=alpha + abs(beta2) - abs(beta1)
!!!  alpha=alpha + beta2 - beta1
!!  alpha=alpha - beta2 - beta1
!!!  alpha=modulo(alpha+3*PI,2*PI)-PI
!
!  !alpha=alpha + beta2 - beta1
!  alpha=alpha - beta2 - beta1
!  alpha=alpha - beta2 - beta1
!  alpha=modulo(alpha+2*PI,4*PI)-2*PI
  !if (abs(abs(alpha)-PI/2)<20*PI/180) then
  !        if (alpha>0) then
  !                alpha = -PI/2 + alpha
  !        else
  !                alpha = PI/2 + alpha
  !        endif
  !endif
  nb=nb+1

  !eps=1e-1
!  if ((salpha*sin(alpha)<0._pr).and.(abs(abs(alpha*180/PI)-180)<5).and.(kt>1)) then
!  !if ((salpha*sin(alpha)<0._pr).and.((abs(sin(alpha))<0.1).or.(abs(salpha)<0.1)).and.(kt>1)) then
!  !if ((salpha*sin(alpha)<0._pr).and.(abs(cos(alpha))>0.8).and.(kt>1)) then
!          alpha = -alpha
!  endif
!  if ((((salpha*sin(alpha)<0._pr).and.(abs(sin(alpha))>0.1).and.(abs(salpha)>0.1)).or.&
!  ((calpha*cos(alpha)<0._pr).and.(abs(cos(alpha))>0.1).and.(abs(calpha)>0.1))).and.(kt>1)) then
!  if ((((salpha*sin(alpha)<0._pr).and.((abs(sin(alpha))>0.5).or.(abs(salpha)>0.5))).or.&
!  if ((((salpha*sin(alpha)<0._pr).and.(abs(sin(alpha))>0.6).and.(abs(salpha)>0.01)).or.&
!  ((calpha*cos(alpha)<0._pr).and.(abs(cos(alpha))>0.6).and.(abs(calpha)>0.01))).and.(kt>1)) then
!!  ((calpha*cos(alpha)<0._pr).and.((abs(cos(alpha))>0.5).or.(abs(calpha)>0.5)))).and.(kt>1)) then
!          if (sin(alpha)>0._pr) then
!                  alpha = alpha - PI !PI/2 - alpha
!          else
!                  alpha = alpha + PI !-PI/2 - alpha
!          endif
!  endif
  !if ((salpha*sin(alpha)<0._pr).and.(abs(sin(alpha))<eps).and.(abs(salpha)<eps).and.(kt>1)) then
  !        alpha = -alpha
  !endif
  !if ((calpha*cos(alpha)<0._pr).and.(abs(cos(alpha))>0.1).and.(kt>1)) then
  !        if (sin(alpha)>0._pr) then
  !                alpha = alpha - PI !PI/2 - alpha
  !        else
  !                alpha = alpha + PI !-PI/2 - alpha
  !        endif
  !endif

!!  write(*,*) "ALPHA  ",alpha*180/PI," ",cos(alpha)," ",sin(alpha)
  calpha = cos(alpha)
  salpha = sin(alpha)
  !alpha=alpha/nb
  !write(*,*) "ALPHAmean  ",alpha*180/PI



end subroutine compute_thetadef

end module libBezier




