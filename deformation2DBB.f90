
!   "Convert an integer to string."
character(len=3) function str(k)
    integer, intent(in) :: k
    write (str, '(I3.3)') k
    str = adjustl(str)
end function str
!character(len=1) function stri1(k)
!    integer, intent(in) :: k
!    write (stri1, '(I1.1)') k
!    stri1 = adjustl(stri1)
!end function stri1
!character(len=2) function stri2(k)
!    integer, intent(in) :: k
!    write (stri2, '(I1.2)') k
!    stri2 = adjustl(stri2)
!end function stri2
!character(len=3) function stri3(k)
!    integer, intent(in) :: k
!    write (stri3, '(I1.3)') k
!    stri3 = adjustl(stri3)
!end function stri3
program def3D
  use variables
  use advection
  use AdvectionProblem
  use TimeScheme
  
  implicit none
  integer :: i, j, k, kt, idisplay, iUpdateDist, iter, picNum, p, l, q, r, s 
  real(pr) :: Tps, threshold, surf 
  real(pr), dimension(:,:), allocatable :: rho, uu, vv, u0, v0, u, v, rhou, rhov 
  real(pr) :: t, tPic, xi, yj, distW, distWb, maxLS, minLS, seuilLS
  real(pr), dimension(:,:), allocatable :: rhoSlices, rhoSlices2
  integer, dimension(:), allocatable :: rcounts, displs
  real(pr), dimension(:,:), allocatable :: tmp, tmp1, tmp2, un, zero

!  N =  200 
!  nx = 200 
!  ny = 200 
  N =  100 
  nx = 100 
  ny = 100 
  eepsilon = 1.e-6_pr
  dt = 1
  threshold = 0.001
  dx = 1._pr 
  dy = 1._pr 
  dz = 1.
  Tps = 1.  

  allocate(rhoSlices2(nx,ny))
  allocate(un(N,N),zero(N,N))
  allocate(tmp(2*N,2*N),tmp1(N,N),tmp2(N,N))
  allocate(rhoSlices(nx,ny))
  allocate(xx(1:nx),yy(1:ny)) 
  allocate(xp(1:nx,1:ny),yp(1:nx,1:ny)) 
  allocate(rho0(1:nx,1:ny)) 
  allocate(rhou(1:nx,1:ny),rhov(1:nx,1:ny))  
  allocate(rhouu(1:nx,1:ny),rhovv(1:nx,1:ny)) 
  allocate(rhouup(1:nx,1:ny),rhovvp(1:nx,1:ny)) 
  allocate(rhop(1:nx,1:ny),rhoup(1:nx,1:ny),rhovp(1:nx,1:ny)) 
  allocate(u(1:nx,1:ny),v(1:nx,1:ny)) 
  allocate(rho(1:nx,1:ny))
  allocate(uu(1:nx,1:ny), vv(1:nx,1:ny))
  allocate(u0(1:nx,1:ny), v0(1:nx,1:ny))

  un = 1._pr
  zero = 0._pr

    do i=1,nx
      xx(i) = 1+(float(i)-1)*dx
      do j=1,ny
        yy(j) = 1+(float(j)-1)*dy
      enddo
    enddo  

  picNum = 140
    open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/BB18/rho0/rho"//str(picNum)//".txt",&
    !open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/mask200/Image_"//str(picNum)//".txt",&
status='unknown')
!    open(unit=78,file="/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_ini0.txt",&
!status='unknown')
  do k=1,2*nx
    read(78,*) tmp(k,:)
  enddo
  do k=1,2*nx
    do j=1,2*ny
    if ((mod(k-1,2)==0).and.(mod(j-1,2)==0)) rhoSlices2(1+(k-1)/2,1+(j-1)/2) = tmp(k,j)
    enddo
  enddo
  close(78)
    open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/BB18/rho0/rho"//str(picNum+1)//".txt",&
status='unknown')
!  do k=1,nx
!    read(78,*) tmp(k,:)
!  enddo
  close(78)
    open(unit=79,file='results/rho0.vtk',status='unknown')
      write(79,'(1A26)') '# vtk DataFile Version 2.0'
      write(79,'(a)') 'rho'
      write(79,'(a)') 'ASCII'
      write(79,'(a)') 'DATASET STRUCTURED_POINTS'
      write(79,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1 !200,200,1 !nx,ny,1
      write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
      write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 1.,1.,1.
      write(79,'(a,I6)') 'POINT_DATA          ' , nx*ny !200*200 !nx*ny!nx*ny
      write(79,'(a)') 'SCALARS density float 1' !SCALARS values double
      write(79,'(a)') 'LOOKUP_TABLE default'
    do k=1,nx
      do j=1,ny
        write(79,*) rhoSlices2(j,k)
      enddo
    enddo
    close(79)    
    open(unit=79,file='results/rho1.vtk',status='unknown')
      write(79,'(1A26)') '# vtk DataFile Version 2.0'
      write(79,'(a)') 'rho'
      write(79,'(a)') 'ASCII'
      write(79,'(a)') 'DATASET STRUCTURED_POINTS'
      write(79,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1 !200,200,1 !nx,ny,1
      write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
      write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 1.,1.,1.
      write(79,'(a,I6)') 'POINT_DATA          ' , nx*ny !200*200 !nx*ny!nx*ny
      write(79,'(a)') 'SCALARS density float 1' !SCALARS values double
      write(79,'(a)') 'LOOKUP_TABLE default'
    do k=1,nx
      do j=1,ny
        write(79,*) tmp(j,k)
      enddo
    enddo
    close(79)    
 !do k=1,nx
 !do j=1,ny
 !if (rhoSlices2(k,j)<0.1_pr) then
 !        rhoSlices2(k,j) = 0._pr
 !endif
 !enddo
 !enddo

  !rhoSlices2 = 1._pr
  rhoSlices = rhoSlices2

  t = 0
  idisplay = 1
  iUpdateDist = 100
  kt = 1
  do kt = 1,1 !40 !615
!    open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/BB18/rho0/rho"//str(kt+picNum-1)//".txt",&
!status='unknown')
!  do k=1,nx
!    read(78,*) rhoSlices2(k,:)
!  enddo
!  close(78)
!  if (mod(kt,idisplay)==0) then
!    open(unit=79,file='results/sol'//str(kt+140-1)//'.vtk',status='unknown')
!      write(79,'(1A26)') '# vtk DataFile Version 2.0'
!      write(79,'(a)') 'rho'
!      write(79,'(a)') 'ASCII'
!      write(79,'(a)') 'DATASET STRUCTURED_POINTS'
!      write(79,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1 !200,200,1 !nx,ny,1
!      write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
!      write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 1.,1.,1.
!      write(79,'(a,I6)') 'POINT_DATA          ' , nx*ny !200*200 !nx*ny!nx*ny
!      write(79,'(a)') 'SCALARS density float 1' !SCALARS values double
!      write(79,'(a)') 'LOOKUP_TABLE default'
!    
!    do k=1,nx
!      do j=1,ny
!        write(79,*) rhoSlices2(j,k)
!      enddo
!    enddo
!  endif
  iter = 0
  !do while (t<float(kt))
  !do while (iter<10)
  !do while (iter<128)
  do while (iter<64)
      write(*,*)"t ",t
      t = t + 1._pr/64 !0.1_pr
      iter = iter + 1
      write(*,*) "vitesses n  ",iter," ",str(iter-1)
!if ((iter-1)<10) open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/BB18/vitesses/vitesse"//str(kt+picNum-1)&
!//"_"//stri1(iter-1)//".txt",status='OLD')  
!if ((iter-1)<100) open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/BB18/vitesses/vitesse"//str(kt+picNum-1)&
!//"_"//stri2(iter-1)//".txt",status='OLD')  
!if ((iter-1)<1000) open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/BB18/vitesses/vitesse"//str(kt+picNum-1)&
!//"_"//stri3(iter-1)//".txt",status='OLD')  
 open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/BB18/vitesses/vitesse"//str(kt+picNum-1)&
//"_"//str(iter-1)//".txt",status='OLD')  
  do j=1,nx
    do i=1,ny
!        read(10,'(F15.6,F15.6)') u0(j,i), v0(j,i)
        read(10,'(F10.6,F10.6)') u0(j,i), v0(j,i)
        !read(10,*) v0(j,i), u0(j,i)
    enddo
  enddo
  close(10) 
 u = u0*dx
 v = v0*dy
! do i=1,nx
!   do j=1,ny
!     if (rhoSlices(i,j).lt.1.e-6_pr) then
!       u(i,j) = 0._pr
!       v(i,j) = 0._pr
! !      rhoSlices(i,j) = 0._pr
!     endif
!   enddo
! enddo     
    !open(unit=79,file='results/sol'//str(iter+140)//'.vtk',status='unknown')
    !open(unit=79,file='results/sol'//str(128*(kt-1)+iter)//'.vtk',status='unknown')
    open(unit=79,file='results/sol'//str(64*(kt-1)+iter)//'.vtk',status='unknown')
      write(79,'(1A26)') '# vtk DataFile Version 2.0'
      write(79,'(a)') 'rho'
      write(79,'(a)') 'ASCII'
      write(79,'(a)') 'DATASET STRUCTURED_POINTS'
      write(79,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1 !200,200,1 !nx,ny,1
      write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
      write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 1.,1.,1.
      write(79,'(a,I6)') 'POINT_DATA          ' , nx*ny !200*200 !nx*ny!nx*ny
      write(79,'(a)') 'SCALARS density float 1' !SCALARS values double
      write(79,'(a)') 'LOOKUP_TABLE default'
    do k=1,nx
      do j=1,ny
        write(79,*) rhoSlices2(j,k)
      enddo
    enddo
    close(79)    
 call SetInitialCondition(rhoSlices(:,:),u,v,rho,rhou,rhov)

 dt = 1._pr !0.9_pr/(eepsilon + maxval(abs(u))/dx + maxval(abs(v))/dy) ! abs() necessaire au cas ou a<0
 call AdvanceTime(kt, dt, 0, 3, 4, 1, rho, u, v, rhou, rhov)

 rhoSlices(:,:) = rho(:,:)!/maxval(rho)!-seuilLS
 rhoSlices2(:,:) = rhoSlices(:,:)

  enddo

! if (mod(kt,idisplay)==0) then
!    close(79)    
!    endif
 enddo  
 
 deallocate(rhoSlices2)
 deallocate(rhoSlices)
 deallocate(rho0) 
 deallocate(xx,yy)
 deallocate(u,v,rho,rhou,rhov) 
 deallocate(rhoup,rhovp,rhop)
 deallocate(uu,vv,rhouu,rhovv,rhouup,rhovvp)
 deallocate(u0,v0)
 deallocate(tmp1,tmp2)
 deallocate(un,zero)
end program def3D

