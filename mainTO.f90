program sph
  
  use variables
  use doubler 
  use init
  use inverser
  use construction
  use util
  use advection
 
  implicit none
  
  integer :: i,j, indx,indy, k, kk,m, keps, kt, cpt
  integer :: bool 
  character(len=50) ::  filepart1, str
  real(pr) :: x,sol,y,r,residuabs, errmax,lun, linf,pi,l2,linf2
  real(pr) :: Tps, alphaa, alph, resu, resi, linfin, epsiloni, threshold
  real(pr), dimension(:), allocatable :: residu,solution, Trespi 
  real(pr), dimension(:,:), allocatable :: gradxphi, gradyphi, uu, vv,u0,v0, nu, u0i, v0i
  character(len=50) ::  filepart2,filepart3,filepart4,filepart5,mystr, filepart6
  real(pr) :: xi, yj, distW, distWb

  CHARACTER(len=3) :: arg
  CHARACTER(len=128) :: arg1,arg2,arg4,arg5
  CALL getarg(1, arg1)
  CALL getarg(2, arg2)
  CALL getarg(3, arg)
  CALL getarg(4, arg4)
  CALL getarg(5, arg5)
  write(*,*)arg1!,arg2,arg4,arg5

  OPEN(10,file='C_IN.DAT',status='OLD')
  READ(10,*)
  read(10,*)nx
  READ(10,*)
  read(10,*)ny
  READ(10,*)
  read(10,*)nite
  read(10,*)
  read(10,*)repsilon 
  read(10,*)
  read(10,*)alph
  read(10,*)
  read(10,*)dt
  read(10,*)
  read(10,*)threshold
  close(10)

!!  dx = 4._pr/float(nx-1)
!!  dy = 4._pr/float(ny-1)
!!  dx = 0.008_pr/float(nx-1)
!!  dy = 0.008_pr/float(ny-1)

  dx = 1.
  dy = 1.
  Tps = 1.  
  ntot = nx*ny 
  eps = 1.D-6
  
  pi = acos(-1.)
  allocate(xx(1:nx))
  allocate(yy(1:ny)) 
 
  allocate(marqueur(1:nx,1:ny)) 
  allocate(phi2(1:nx,1:ny)) 
  allocate(phi2x(1:nx,1:ny),phi2y(1:nx,1:ny) ) 
  allocate(xp(1:nx,1:ny),yp(1:nx,1:ny)) 
  allocate(rho0(1:nx,1:ny),rho1(1:nx,1:ny)) 
  allocate(rho00(1:nx,1:ny),rho11(1:nx,1:ny)) 
  allocate(xinter(1:nx,1:ny),yinter(1:nx,1:ny)) 
  allocate(gradxphi(1:nx,1:ny),gradyphi(1:nx,1:ny)) 
  allocate(Tx(1:2,1:nx,1:ny),Ty(1:2,1:nx,1:ny)) 
  allocate(xix2(1:2,1:nx,1:ny),xiy2(1:2,1:nx,1:ny))
  allocate(ipar(1:13),fpar(1:16)) 
  allocate(rhou(1:nx,1:ny),rhov(1:nx,1:ny))  
  allocate(rhouu(1:nx,1:ny),rhovv(1:nx,1:ny)) 
  allocate(rhouup(1:nx,1:ny),rhovvp(1:nx,1:ny)) 
  allocate(rhop(1:nx,1:ny),rhoup(1:nx,1:ny),rhovp(1:nx,1:ny)) 
  allocate(u(1:nx,1:ny),v(1:nx,1:ny)) 
  allocate(rho(1:nx,1:ny))
  
  allocate(gradrhox(1:nx,1:ny))
  allocate(gradrhoy(1:nx,1:ny))
  allocate(rmr1(1:nx,1:ny))

  allocate(rho0i(1:nx,1:ny),rho1i(1:nx,1:ny))
 

  call initialisation(arg1,arg2,arg4,arg5)
  write(*,*)arg2
  
  taillemat = 8*ntot+10*nbsigne
  
  allocate(residu(ntot+nbsigne))
  allocate(um(1:ntot+nbsigne))
  allocate(b(1:ntot+nbsigne))
  allocate(mat1(1:taillemat))
  allocate(mat2(1:taillemat))
  allocate(mat3(1:taillemat))
  allocate(solution(1:ntot+nbsigne))
  allocate(r0mr1(1:nx,1:ny))  
  allocate(uu(1:nx,1:ny), vv(1:nx,1:ny))
  allocate(u0(1:nx,1:ny), v0(1:nx,1:ny))
  allocate(nu(1:nx,1:ny)) 

  ! premiere approx de la vitesse, hors de la boucle
  
  r0mr1 = rho - rho1
  
  call assemblage
  
  allocate(matt1(1:indice))
  allocate(matt2(1:indice))
  allocate(matt3(1:indice))


!  allocate(Ter(0:nite))
!  allocate(Terl1(0:nite))
  
  matt1 = mat1(1:indice)
  matt2 = mat2(1:indice)
  matt3 = mat3(1:indice)


  ! initial guess for systeme to solve
  
  do i=1,ntot+nbsigne
     um(i) = 1._pr
  enddo
  
  ! inversion systeme lineaire
  
  n = ntot+nbsigne
  nnz = indice
 
  call inverse_gmres
   
  ! calcul gradient de phi

  do i=2,nx-1
     do j=1,ny
        gradxphi(i,j) = (um(ind(i+1,j))- um(ind(i-1,j)))/(2*dx)    
     enddo
  enddo
  
  do i=1,nx
     do j=2,ny-1
        gradyphi(i,j) = (um(ind(i,j+1))- um(ind(i,j-1)))/(2*dy)
     enddo
  enddo
  
  gradxphi(1,:) = 0.0_pr 
  gradxphi(nx,:) = 0.0_pr  
  gradyphi(:,1) = 0.0_pr
  gradyphi(:,ny) = 0.0_pr
  
  !alph = 1.8
  !alph = 0.1
  
  u0 = gradxphi!*alph
  v0 = gradyphi!*alph

 ! u0 = 0.
 ! v0 = 0.


!! print*,maxval(abs(u0))
!! print*,maxval(abs(v0)) 
!! print*,maxval(rho0) 
!! print*,minval(rho0)


 open(unit=11,file='erreureps.txt',status='unknown')
 open(unit=12,file='Tresidu.txt',status='unknown')
 open(unit=13,file='Wasserstein.txt',status='unknown')

 cpt = 0

 resi =  maxval(abs(rho0i-rho1i))
!! print*,resi

! do while (repsilon.ge.0.001_pr) 
!!    print*,'Kespilon================', repsilon,'====================='
  cpt = cpt + 1
  write(*,*) "resi0 ",resi    
  kk = 0
!  do kk = 0,nite
  linf = resi
  bool = 0
  do while ((((linf>resi*threshold).and.(kk<nite)).and.(bool.eq.0)).or.(kk.eq.0))

       u = u0
       v = v0
       rho = rho0
!!       print*, 'kk=======',kk 
       
       do i=1,nx
          do j=1,ny 
             rhou(i,j) = rho(i,j)*u(i,j) 
             rhov(i,j) = rho(i,j)*v(i,j) 
             rhouu(i,j) = rho(i,j)*uu(i,j)
             rhovv(i,j) = rho(i,j)*vv(i,j)
          enddo
       enddo
       call transport(Tps,-1,1)
       
       
       linf2 = 0.
       l2 = 0.
       residuabs = 0.
       do i=1,nx
          do j=1,ny
             residuabs = residuabs+ dx*dy*abs(rho(i,j)-rho1(i,j))
             l2 = l2+ dx*dy*(rho(i,j)-rho1(i,j))**2
             linf2 = max(linf, abs(rho(i,j)-rho1(i,j)))
          enddo
       enddo
       l2= sqrt(l2)
       !!if ((kk>0).and.(maxval(abs(rho-rho1))>linf)) then
       !!        print*,kk+1," DIVERGENCE"
       !!        bool=1
       !!        kk = kk - 1
       !!else
       linf = maxval(abs(rho-rho1))
       !!endif
       print*,kk+1," ",linf
       write(12,*) kk+1, linf

       !!write(11,*)linf, distWb
       !!if (kk==0) then
       !!        resi = linf
       !!        write(*,*) "resi0 ",resi    
       !!endif
       ! on recalcule une correction de vitesse en comparant la densite obtenue � celle qu'on veut obtenir
 
       r0mr1 = rho - rho1
     
     call assemblage
     
     matt1 = mat1(1:indice)
     matt2 = mat2(1:indice)
     matt3 = mat3(1:indice)
     
     ! initial guess for systeme to solve
     
     do i=1,ntot+nbsigne
        um(i) = 1._pr
     enddo
     
     ! inversion systeme lineaire
     
     n = ntot+nbsigne
     nnz = indice
 
     call inverse_gmres
   
     ! calcul de la vitesse
     do i=2,nx-1
        do j=1,ny
           gradxphi(i,j) = (um(ind(i+1,j))- um(ind(i-1,j)))/(2*dx)
        enddo
     enddo
     
     do i=1,nx
        do j=2,ny-1
           gradyphi(i,j) = (um(ind(i,j+1))- um(ind(i,j-1)))/(2*dy)
        enddo
     enddo

     gradxphi(1,:) = 0.0_pr 
     gradxphi(nx,:) = 0.0_pr  
     gradyphi(:,1) = 0.0_pr
     gradyphi(:,ny) = 0.0_pr

     uu = gradxphi
     vv = gradyphi
   
     rhouu = rho*uu
     rhovv = rho*vv
     
     rhou = - rhou
     rhov = -rhov
     ! on repropage la correction de vitesse dans l'autre sens
   
     call transport(Tps,-1,0)
    
    
     ! on ajoute la correction de vitesse sur les particules initiales (dans le support de la densit�)
     do i=1,nx
        do j=1,ny
           if (rho(i,j).gt.1E-6) then
           !if (rho(i,j)>1E-1) then
              uu(i,j) = rhouu(i,j)/rho(i,j)
              vv(i,j) = rhovv(i,j)/rho(i,j)
           else
              uu(i,j) = 0.
              vv(i,j) = 0.
           endif
        enddo
     enddo

     if (bool.eq.0) then
       u0 = u0 + alph*uu
       v0 = v0 + alph*vv
     endif

   
       distW  = 0._pr 
       distWb = 0._pr 
       do i=1,nx
          ! xi = (float(i)-1.0_pr)*dx -2._pr
          xi = xx(i)
          do j=1,ny 
            !  yj = (float(j)-1.0_pr)*dy -2._pr
            yj= yy(j)
          !  if (rho0i(i,j).gt.5E-3) then
              distW = distW + rho0(i,j)*((u0(i,j)-xi)**2+(v0(i,j)-yj)**2) 
              distWb =  distWb + rho0(i,j)*(u0(i,j)**2 + v0(i,j)**2)
          !  endif
          enddo
       enddo 
       distW = distW*dx*dy
       distWb = distWb*dx*dy 
       write(13,*)kk+1, distW, distWb
!!     u0 = Tps*(u0 + alph*uu)
!!     v0 = Tps*(v0 + alph*vv)

    !print*, 'erreur L inf', linf

      
      kk = kk + 1

 enddo ! fin iterations sur kk
 close(11)
 close(12)
 close(13)
!! write(*,*) "nbIter ",kk
!!$end if 

 ! On souhaite calculer la distance de Kantorovich

 distW  = 0._pr 
 distWb = 0._pr 
 do i=1,nx
    ! xi = (float(i)-1.0_pr)*dx -2._pr
    xi = xx(i)
    do j=1,ny 
     !  yj = (float(j)-1.0_pr)*dy -2._pr
       yj= yy(j)
       distW = distW + rho0(i,j)*((u0(i,j)-xi)**2+(v0(i,j)-yj)**2) 
       distWb =  distWb + rho0(i,j)*(u0(i,j)**2 + v0(i,j)**2)
    enddo
 enddo 

 distWb = distWb*dx*dy 
!! print*,'maxu0=',maxval(abs(u0))
!! print*,'maxv0=', maxval(abs(v0))


 ! on fait une derniere ite, ou on propage et on garde le resultat final


 
 rho = rho0i
 u = u0
 v = v0
 
! !!$!u(1,:) = 0.
! !!$!u(nx,:) = 0.
! !!$!v(:,1) = 0.
! !!$!v(:,ny) = 0.

 do i=1,nx
    do j=1,ny 
       rhou(i,j) = rho(i,j)*u0(i,j) 
       rhov(i,j) = rho(i,j)*v0(i,j) 
       rhouu(i,j) = rho(i,j)*uu(i,j)
       rhovv(i,j) = rho(i,j)*vv(i,j)
    enddo
 enddo

 !Tps = real(kt)/3.
 call transport(Tps,-1,0)
 
  linfin = maxval(abs(rho-rho1i))/resi
!!  print*, 'erreur L inf', linfin
  write(*,*) "numImage - nbIter - erreur Linf    ",arg," ",kk," ",linfin
!!  write(11,*)linfin,  distWb


  rho0 = rho0 - repsilon 
  rho1 = rho1 - repsilon 
  !repsilon = repsilon/1.1_pr
  repsilon = 0.95*repsilon
  rho0 = rho0 + repsilon 
  rho1 = rho1 + repsilon 

!!  do i=1,nx
!!    do j=1,ny
!!      if (rho0(i,j).lt.0.003_pr) then
!!        u0(i,j)=0._pr
!!        v0(i,j)=0._pr
!!      endif
!!    enddo
!!  enddo

!enddo ! fin iteration sur keps

 u = sqrt(u0*u0 + v0*v0)
 print*,'maxu0=',maxval(abs(u0))
 print*,'maxv0=', maxval(abs(v0))
 print*,'max=', maxval(u)

!! print*,"cpt==",cpt

!!close(11)
!!!!! ecriture resultat


!! filepart1='data.txt'
!!  open(unit=78,file=filepart1,status='unknown')   
!!  ! sortie pour gnuplot 
!!
!!  do i=1,nx
!!     xi = xx(i)
!!     do j=1,ny
!!        yj = yy(j)
!!        write(78,*) xi, yj, rho0(i,j), rho1(i,j), rho(i,j),rho0i(i,j),rho1i(i,j),u0(i,j),v0(i,j) !um(ind(i,j))
!!     enddo 
!!     write(78,*)
!!  enddo
!!  close(78)
!!
!! open(unit=78,file='datarho0.txt',status='unknown') 
!! do i=1,nx
!!     xi = xx(i) !(float(i)-1)*dx !(float(i)-1.0_pr)*dx -2._pr
!!     do j=1,ny
!!     	yj = yy(j) !(float(j)-1.0_pr)*dy -2._pr
!!     	write(78,*) xi, yj, rho0(i,j)
!!     end do	 
!!     write(78,*)
!! enddo
!! close(78)
!!
!!  open(unit=78,file='datarho1.txt',status='unknown') 
!! do i=1,nx
!!    xi = xx(i)
!!    do j=1,ny
!!        yj = yy(j)
!!        write(78,*) xi, yj, rho1(i,j)
!!    enddo
!!    write(78,*) 
!! enddo
!! close(78)
!!
!!  open(unit=78,file='datau.txt',status='unknown') 
!! do i=1,nx
!!    xi = xx(i)
!!    do j=1,ny
!!      yj = yy(j)
!!      write(78,*) xi, yj, u0(i,j) 
!!    enddo
!!    write(78,*)
!! enddo
!! close(78)
!!
!! open(unit=78,file='datav.txt',status='unknown') 
!! do i=1,nx
!!    xi = xx(i)
!!    do j=1,ny
!!      yj = yy(j)
!!      write(78,*) xi, yj, v0(i,j) 
!!    enddo
!!    write(78,*)
!! enddo
!! close(78)
!!  


 open(unit=78,file='vitesse'//arg//'.txt',status='unknown') 
 do j=1,ny
    do i=1,nx
      !!write(78,'(E23.15,E23.15)') u0(i,j), v0(i,j) 
      write(78,'(F10.6,F10.6)') u0(j,i), v0(j,i) 
    enddo
 enddo
 close(78)

 !enddo ! fin kt 
  

!!  filepart2='um.vtk'
!! filepart3='v.vtk'
!!  filepart4='u.vtk'
!!  !filepart3='exact.vtk'
!!  !filepart4='erreur.vtk'
!!  filepart5 = 'rho.vtk'
!! 
!!  filepart6='graxphi.vtk'
!!  
!!  open(unit=79,file=filepart2,status='unknown',position='append') 
!!  open(unit=80,file=filepart3,status='unknown',position='append') 
!!  open(unit=81,file=filepart4,status='unknown',position='append') 
!!  open(unit=82,file=filepart5,status='unknown',position='append') 
!!  open(unit=83,file=filepart6,status='unknown',position='append') 
!!
!!
!!  write(79,'(1A26)') '# vtk DataFile Version 2.0'
!!  write(79,'(a)') 'fleur um'
!!  write(79,'(a)') 'ASCII'
!!  write(79,'(a)') 'DATASET STRUCTURED_POINTS'
!!  write(79,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1
!!  write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 0.,0.,0.
!!  write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy, 1.
!!  write(79,'(a,I6)') 'POINT_DATA' , nx*ny
!!  write(79,'(a)') 'SCALARS density float 1'
!!  write(79,'(a)') 'LOOKUP_TABLE default'
!!  
!!  write(80,'(1A26)') '# vtk DataFile Version 2.0'
!!  write(80,'(a)') 'fleur u'
!!  write(80,'(a)') 'ASCII'
!!  write(80,'(a)') 'DATASET STRUCTURED_POINTS'
!!  write(80,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1
!!  write(80,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 0.,0.,0.
!!  write(80,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy, 1.
!!  write(80,'(a,I6)') 'POINT_DATA' , nx*ny
!!  write(80,'(a)') 'SCALARS density float 1'
!!  write(80,'(a)') 'LOOKUP_TABLE default'
!!  
!!  write(81,'(1A26)') '# vtk DataFile Version 2.0'
!!  write(81,'(a)') 'fleur v'
!!  write(81,'(a)') 'ASCII'
!!  write(81,'(a)') 'DATASET STRUCTURED_POINTS'
!!  write(81,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1
!!  write(81,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 0.,0.,0.
!!  write(81,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy, 1.
!!  write(81,'(a,I6)') 'POINT_DATA' , nx*ny
!!  write(81,'(a)') 'SCALARS density float 1'
!!  write(81,'(a)') 'LOOKUP_TABLE default'
!!
!!  write(82,'(1A26)') '# vtk DataFile Version 2.0'
!!  write(82,'(a)') 'fleur rho'
!!  write(82,'(a)') 'ASCII'
!!  write(82,'(a)') 'DATASET STRUCTURED_POINTS'
!!  write(82,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1
!!  write(82,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 0.,0.,0.
!!  write(82,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy, 1.
!!  write(82,'(a,I6)') 'POINT_DATA' , nx*ny
!!  write(82,'(a)') 'SCALARS density float 1'
!!  write(82,'(a)') 'LOOKUP_TABLE default'
!!
!!  write(83,'(1A26)') '# vtk DataFile Version 2.0'
!!  write(83,'(a)') 'fleur rho1'
!!  write(83,'(a)') 'ASCII'
!!  write(83,'(a)') 'DATASET STRUCTURED_POINTS'
!!  write(83,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1
!!  write(83,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 0.,0.,0.
!!  write(83,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy, 1.
!!  write(83,'(a,I6)') 'POINT_DATA' , nx*ny
!!  write(83,'(a)') 'SCALARS density float 1'
!!  write(83,'(a)') 'LOOKUP_TABLE default'
!!
!!  residuabs = 0.
!!  !sortie vtk
!!  do j=1,ny
!!     do i=1,nx
!!        residuabs = residuabs+ dx*dy*abs(rho(i,j)-rho1(i,j))
!!       r = sqrt(xx(i)**2+yy(j)**2)
!!        sol = r**2 - 2._pr*r
!!       if (phi2(i,j).le.0._pr) sol =  0._pr
!!       
!!        write(79,999) um(ind(i,j)) 
!!        if (abs(u(i,j)).lt.1E-6) then
!!           write(80,999) 0.
!!        else
!!           write(80,999) vv(i,j)
!!        endif
!!
!!        if (abs(u0(i,j)).lt.1E-6) then
!!           write(81,999) 0.
!!        else
!!           write(81,999) uu(i,j)
!!        endif
!!
!!        if (abs(rho(i,j)).lt.1E-6) then
!!           write(82,999) 0.
!!        else
!!          write(82,999) rho(i,j)! - rho1(i,j)
!!        endif
!!
!!           if (abs(rho1(i,j)).lt.1E-6) then
!!              write(83,999) 0.
!!           else
!!             write(83,999)  rho1(i,j)-rho(i,j) !gradxphi(i,j)  !gradxphi(i,j) 
!!           endif
!!
!!      
!!       
!!     enddo
!!  enddo
!!
!! 
!!  999 FORMAT(5(E23.15))
!!
!!  
!!  close(79)
!!  close(80)
!!  close(81)
!!  close(82)
!!  close(83) 

  
  deallocate(residu)
  deallocate(marqueur)
  deallocate(mat1,mat2,mat3)
  deallocate(matt1,matt2,matt3)
  deallocate(phi2,phi2x,phi2y) 
  deallocate(xinter,yinter)  
  deallocate(xix2,xiy2) 
  deallocate(rho0,rho1) 
  deallocate(rho00,rho11) 
  deallocate(Tx,Ty)
  deallocate(um)
  deallocate(xx,yy)
  deallocate(gradxphi,gradyphi)
  deallocate(b) 
  deallocate(ipar,fpar) 
  deallocate(solution)
  deallocate(u,v,rho,rhou,rhov) 
  deallocate(rhoup,rhovp,rhop)
  deallocate(gradrhox,gradrhoy)
  deallocate(rmr1)
  deallocate(uu,vv,rhouu,rhovv,rhouup,rhovvp)
  deallocate(u0,v0)
  deallocate(nu) 
  deallocate(rho0i,rho1i)

  
end program sph


character(len=20) function str(k)
!   "Convert an integer to string."
    integer, intent(in) :: k
    write (str, *) k
    str = adjustl(str)
end function str




