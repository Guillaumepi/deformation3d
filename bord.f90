module bord
  
  use util

  implicit none
  
contains  

  real(pr) function cond_bord(x,y)
    
    use variables
    use doubler
    
    implicit none
    
    real(pr) :: x,y,sol,r,pi
    
    pi = acos(-1.)
    r = sqrt(x**2+y**2)
 !   cond_bord = r**2-2._pr*r
    cond_bord = exp(r)/exp(1.) - sin(2.*pi*r)/(2.*pi)
    cond_bord = 0._pr
  end function cond_bord
  

  real(pr) function source(i,j)
    
    use variables
    use doubler
    
    implicit none
    
    integer :: i,j
    real(pr) :: r,pi
    pi = acos(-1.)
    r = sqrt(xx(i)**2+yy(j)**2)
    source = 0._pr
   ! if (phi2(i,j).ge.0._pr)   source= 4._pr - 2._pr/r
    ! if (phi2(i,j).ge.0._pr)   source= exp(r)/(exp(1.)*r) + exp(r)/exp(1.) - cos(2.*pi*r)/r + sin(2.*pi*r)*2.*pi
    if (phi2(i,j).ge.0._pr)   source= 4._pr
  end function source

end module bord


