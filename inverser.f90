module inverser
   
  use variables
  use doubler

  implicit none

contains

  SUBROUTINE varsgmres

    IMPLICIT NONE
    
    integer :: m
   ! INTEGER,DIMENSION(:),INTENT(out)::ipar
   ! REAL(pr),DIMENSION(:),INTENT(out)::fpar
   

    ipar = 0
  ipar(2) = 1
  ipar(3) = 1
  ipar(5) = 2000
  ipar(6) = 5000
  kprec = 100
  
  m = ipar(5)
  ipar(4) = ((n+3)*(m+2) + (m+1)*m/2)
   ipar(4) =7*n


  fpar(1) = 1.0D-12
  fpar(2) = 1.0D-12
 
    
    RETURN
    
  END SUBROUTINE varsgmres

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  SUBROUTINE inverse_gmres

    IMPLICIT NONE
    
    integer :: ierr
    real(pr),dimension(:),allocatable:: wk
    integer,dimension(:),allocatable::ju,jlu
    real(pr),dimension(:),allocatable::alu,bb,uu
    real(pr), dimension(:,:), allocatable :: vv
    
    call varsgmres

    CALL precond(alu,jlu,ju)
   
    allocate(bb(n))
    allocate(uu(n))
    bb = b
   
    allocate(vv(n,(50+1)))

    ALLOCATE(wk(ipar(4)))
 
    call pgmres(n,50,bb,um,vv, 0.000000000001_pr,100000,0,matt3,matt2,matt1,alu,jlu,ju,ierr)  

  
    deallocate(vv)
    deallocate(bb)
    deallocate(uu)
   DEALLOCATE(wk)  
   !print*, 'erreur  ',ierr

    DEALLOCATE(alu,jlu,ju)

   
    RETURN
    
  END SUBROUTINE inverse_gmres


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  SUBROUTINE precond(alu,jlu,ju)

 
    IMPLICIT NONE  

    integer :: ierr,i
    integer,dimension(:),allocatable::ju,jlu,w,jw
    real(pr),dimension(:),allocatable::alu,ww
  
    
    allocate(w(n+1))
    CALL coocsr_inplace(n,nnz,1,matt3,matt2,matt1,w)
     DEALLOCATE(w)
  
    
  !  ALLOCATE(alu(kprec*NNZ),jlu(kprec*NNZ),ju(kprec*NNZ))
    ALLOCATE(alu(kprec*NNZ),jlu(kprec*NNZ),ju(n))
    ALLOCATE(ww(nnz+1),jw(2*nnz))

    jlu = 0
    ju = 0
    alu = 0.0D0
  
  !  CALL ILUT(n,matt3,matt2,matt1,n,1.0D-3,alu,jlu,ju,kprec*NNZ,ww,jw,ierr)
    CALL ILUT0(n,matt3,matt2,matt1,n,1.0D-3,alu,jlu,ju,kprec*NNZ,ww,jw,ierr)
 
   
    DEALLOCATE(ww,jw)
    
    if (ierr.ne.0) then
       write(*,*) 'erreur IN PRECOND',ierr
       stop
    endif
   

    RETURN

  END SUBROUTINE precond
 



end module inverser

 
