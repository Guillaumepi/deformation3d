program sph2
  
  use variables
  use doubler 
  use init
  use inverser
  use construction
  use util
  use advection
 
  implicit none
  
  integer :: i,j, indx,indy, k, kk,m, keps, kt, cpt, nbSlices
  
  character(len=50) ::  filepart1, str
  real(pr) :: x,sol,y,r,residuabs, errmax,lun, linf,pi,l2
  real(pr) :: Tps, alphaa, alph, resu, resi, linfin, epsiloni, threshold
  real(pr), dimension(:), allocatable :: residu,solution, Trespi 
  real(pr), dimension(:,:), allocatable :: gradxphi, gradyphi, uu, vv,u0,v0, nu, u0i, v0i
  character(len=50) ::  filepart
  real(pr) :: xi, yj, distW, distWb

  real(pr), dimension(:,:,:), allocatable :: rhoSlices

  CHARACTER(len=128) :: arg1,arg2
  CHARACTER(len=3) :: arg !len=16
  CALL getarg(1, arg1)
  CALL getarg(2, arg2)
  CALL getarg(3, arg)
!  CALL getarg(4, nbSlices)

  OPEN(10,file='C_IN.DAT',status='OLD')
  READ(10,*)
  read(10,*)nx
  READ(10,*)
  read(10,*)ny
  READ(10,*)
  read(10,*)nite
  read(10,*)
  read(10,*)repsilon 
  read(10,*)
  read(10,*)alph
  read(10,*)
  read(10,*)dt
  read(10,*)
  read(10,*)threshold
  read(10,*)
  read(10,*) nbSlices
  close(10)
  
!!  write(*,*) arg1, arg2, arg, nbSlices
!!  dx = 4._pr/float(nx-1)
!!  dy = 4._pr/float(ny-1)

  dx = 1.
  dy = 1.
  dz = 1.
  Tps = 1.  
  ntot = nx*ny 
  eps = 1.D-6
  
  pi = acos(-1.)
  allocate(rhoSlices(nbSlices+1,nx,ny))

  allocate(xx(1:nx))
  allocate(yy(1:ny)) 
 
  allocate(marqueur(1:nx,1:ny)) 
  allocate(phi2(1:nx,1:ny)) 
  allocate(phi2x(1:nx,1:ny),phi2y(1:nx,1:ny) ) 
  allocate(xp(1:nx,1:ny),yp(1:nx,1:ny)) 
  allocate(rho0(1:nx,1:ny),rho1(1:nx,1:ny)) 
  allocate(xinter(1:nx,1:ny),yinter(1:nx,1:ny)) 
  allocate(gradxphi(1:nx,1:ny),gradyphi(1:nx,1:ny)) 
  allocate(Tx(1:2,1:nx,1:ny),Ty(1:2,1:nx,1:ny)) 
  allocate(xix2(1:2,1:nx,1:ny),xiy2(1:2,1:nx,1:ny))
  allocate(ipar(1:13),fpar(1:16)) 
  allocate(rhou(1:nx,1:ny),rhov(1:nx,1:ny))  
  allocate(rhouu(1:nx,1:ny),rhovv(1:nx,1:ny)) 
  allocate(rhouup(1:nx,1:ny),rhovvp(1:nx,1:ny)) 
  allocate(rhop(1:nx,1:ny),rhoup(1:nx,1:ny),rhovp(1:nx,1:ny)) 
  allocate(u(1:nx,1:ny),v(1:nx,1:ny)) 
  allocate(rho(1:nx,1:ny))
  
  allocate(gradrhox(1:nx,1:ny))
  allocate(gradrhoy(1:nx,1:ny))
  allocate(rmr1(1:nx,1:ny))

  allocate(rho0i(1:nx,1:ny),rho1i(1:nx,1:ny))
 

  call initialisation(arg1,arg2)
  
  taillemat = 8*ntot+10*nbsigne
  
  allocate(residu(ntot+nbsigne))
  allocate(um(1:ntot+nbsigne))
  allocate(b(1:ntot+nbsigne))
  allocate(mat1(1:taillemat))
  allocate(mat2(1:taillemat))
  allocate(mat3(1:taillemat))
  allocate(solution(1:ntot+nbsigne))
  allocate(r0mr1(1:nx,1:ny))  
  allocate(uu(1:nx,1:ny), vv(1:nx,1:ny))
  allocate(u0(1:nx,1:ny), v0(1:nx,1:ny))
  allocate(nu(1:nx,1:ny)) 
  
!!  open(unit=78,file='Image_'//arg//'.dat',status='unknown')
!!    do i=1,nx
!!      xi = xx(i)
!!      do j=1,ny
!!        yj = yy(j)
!!        write(78,*) rho0(i,j)
!!      enddo
!!    enddo
!!  close(78)
  write(*,*)"bou"
  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesse"//arg//".txt", status='OLD')  
  do j=1,ny
    do i=1,nx
      read(10,*) u0(j,i), v0(j,i)!'(F10.6,F10.6)'
    enddo
  enddo
  close(10) 
!!  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/"//arg//"/datau.txt", status='OLD')  
!!  do i=1,nx
!!    do j=1,ny
!!      read(10,*) xi, yj, u0(i,j)
!!    enddo
!!  enddo
!!  close(10) 
!!  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/"//arg//"/datav.txt", status='OLD')  
!!  do i=1,nx
!!    do j=1,ny
!!      read(10,*) xi, yj, v0(i,j)
!!    enddo
!!  enddo
!!  close(10) 

 ! On souhaite calculer la distance de Kantorovich
 distW  = 0._pr 
 distWb = 0._pr 
 do i=1,nx
    ! xi = (float(i)-1.0_pr)*dx -2._pr
    xi = xx(i)
    do j=1,ny 
     !  yj = (float(j)-1.0_pr)*dy -2._pr
       yj= yy(j)
       distW = distW + rho0(i,j)*((u0(i,j)-xi)**2+(v0(i,j)-yj)**2) 
       distWb =  distWb + rho0(i,j)*(u0(i,j)**2 + v0(i,j)**2)
    enddo
 enddo 
 distW = distW*dx*dy
 distWb = distWb*dx*dy 

 rhoSlices(1,:,:) = rho0i
 rhoSlices(nbSlices,:,:) = rho1i
  write(*,*)"bou2"


 do kt=1,nbSlices-1
 ! on fait une derniere ite, ou on propage et on garde le resultat final
 rho = rho0i
 u = u0
 v = v0
 do i=1,nx
    do j=1,ny 
       rhou(i,j) = rho(i,j)*u0(i,j) 
       rhov(i,j) = rho(i,j)*v0(i,j) 
       rhouu(i,j) = rho(i,j)*uu(i,j)
       rhovv(i,j) = rho(i,j)*vv(i,j)
    enddo
 enddo

 Tps = real(kt)/(nbSlices-1) !3.
 call transport(Tps,-1,0)
 
 if (kt.eq.(nbSlices-1)) then
   rhoSlices(nbSlices+1,:,:) = rho
 else
   rhoSlices(kt+1,:,:) = rho
 endif

 enddo
 
  linfin = maxval(abs(rho-rho1i))
  print*, 'erreur L inf', linfin
  write(*,*)linfin,  distWb

 print*,'maxu0=',maxval(abs(u0))
 print*,'maxv0=', maxval(abs(v0))

!!!!! ecriture resultat
 !enddo ! fin kt 


  !sortie vtk
!! open(unit=78,file='../rhoSlices.vtk',status='unknown',position='append') 
!!!!  write(78,'(1A26)') '# vtk DataFile Version 2.0'
!!!!  write(78,'(a)') 'rho'
!!!!  write(78,'(a)') 'ASCII'
!!!!  write(78,'(a)') 'DATASET STRUCTURED_POINTS'
!!!!  write(78,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,nbSlices
!!!!  write(78,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 0.,0.,0.
!!!!  write(78,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,dz
!!!!  write(78,'(a,I6)') 'POINT_DATA ' , nx*ny*nbSlices
!!!!  write(78,'(a)') 'SCALARS values double' !SCALARS density float 1'
!!!!  write(78,'(a)') 'LOOKUP_TABLE default'
!!  do kt=1,nbSlices-1,2
!!    rhoSlices(kt,:,:)=rhoSlices(kt,:,:)/maxval(rhoSlices(kt,:,:)) !!rhoSlices(kt,nx/2,ny/2) !maxval(rhoSlices(kt,:,:))
!!  enddo
!!  do kt=1,nbSlices-1,2
!!    do j=1,ny
!!      do i=1,nx
!!        if (abs(rhoSlices(kt,i,j)).lt.1E-6) then
!!          write(78,999) 0.
!!        else
!!          write(78,999)  rhoSlices(kt,i,j) 
!!        endif
!!      enddo
!!    enddo
!!  enddo
!!  999 FORMAT(5(E23.15))
!!  close(78)

    !sortie matlab
    open(unit=79,file='rhoslicematlab.txt',status='unknown')
    do kt=1,nbSlices-1,2
      rhoSlices(kt,:,:)=rhoSlices(kt,:,:)/maxval(rhoSlices(kt,:,:)) !!rhoSlices(kt,nx/2,ny/2) !maxval(rhoSlices(kt,:,:))
    enddo
    do kt=2,nbSlices-1
      do i=1,nx
        xi = xx(i)
        do j=1,ny
          yj = yy(j)
          if (abs(rhoSlices(kt,i,j)).lt.1E-6) then
            write(79,*) 0.
          else
            write(79,*) rho0(i,j)
          endif
        end do
      enddo
    enddo
    close(79)    

  deallocate(rhoSlices)

  deallocate(residu)
  deallocate(marqueur)
  deallocate(mat1,mat2,mat3)
  !deallocate(matt1,matt2,matt3)
  deallocate(phi2,phi2x,phi2y) 
  deallocate(xinter,yinter)  
  deallocate(xix2,xiy2) 
  deallocate(rho0,rho1) 
  deallocate(Tx,Ty)
  deallocate(um)
  deallocate(xx,yy)
  deallocate(gradxphi,gradyphi)
  deallocate(b) 
  deallocate(ipar,fpar) 
  deallocate(solution)
  deallocate(u,v,rho,rhou,rhov) 
  deallocate(rhoup,rhovp,rhop)
  deallocate(gradrhox,gradrhoy)
  deallocate(rmr1)
  deallocate(uu,vv,rhouu,rhovv,rhouup,rhovvp)
  deallocate(u0,v0)
  deallocate(nu) 
  deallocate(rho0i,rho1i)
  
end program sph2


character(len=20) function str(k)
!   "Convert an integer to string."
    integer, intent(in) :: k
    write (str, *) k
    str = adjustl(str)
end function str
