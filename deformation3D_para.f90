program def3D
  use variables
  use advection
  use AdvectionProblem
  use TimeScheme
  
  implicit none

  !include 'mpif.h'

  integer :: me, np, statinfo, k1, kN, myproc, stype, stypesend, styperecv
  real(pr) :: maxtot, mintot, maxtmp, mintmp, maxtmp2, mintmp2,  maxtot2, mintot2
  integer, dimension(MPI_STATUS_SIZE) :: status
  integer :: i, j, k, kt, idisplay, iUpdateDist, iter 
  real(pr) :: Tps, threshold 
  real(pr), dimension(:,:), allocatable :: rho, uu, vv, u0, v0, u, v, rhou, rhov 
  real(pr) :: t, tPic, xi, yj, distW, distWb
  real(pr), dimension(:,:,:), allocatable :: rhoSlices, rhoSlices2
  integer, dimension(:), allocatable :: rcounts, displs
  real(pr), dimension(:,:,:), allocatable :: tmp
  real(pr), dimension(:,:),allocatable ::ValTop1, ValBot1

  call MPI_INIT(statinfo)
  if (statinfo<0) then
    write(*,*) 'Error in mpi_init'
  end if

  call MPI_COMM_RANK(MPI_COMM_WORLD,me,statinfo)
  call MPI_COMM_SIZE(MPI_COMM_WORLD,np,statinfo)
  call MPI_TYPE_CREATE_F90_REAL(15, 3, MPI_PR, statinfo)

  N = 200
  nx = 200
  ny = 200
  nz = 200
  eepsilon = 1.e-6_pr
  dt = 1._pr
  threshold = 0.001
  dx = 24.7687_pr!1._pr
  dy = 24.7687_pr!1._pr
  dz = 24._pr!1._pr
  Tps = 1._pr
  t = 0._pr
  iter = 0
  tPic = 0._pr
  idisplay = 1
  iUpdateDist = 10
  mintot = -0.5_pr
  
  allocate(ValTop1(N,N),ValBot1(N,N))
  if (me == 0) allocate(rhoSlices2(nx,ny,nz))
  allocate(tmp(N,N,N))
  allocate(rcounts(np),displs(np))
  allocate(rhoSlices(nx,ny,nz))
  allocate(xx(1:nx),yy(1:ny)) 
  allocate(xp(1:nx,1:ny),yp(1:nx,1:ny)) 
  allocate(rho0(1:nx,1:ny)) 
  allocate(rhou(1:nx,1:ny),rhov(1:nx,1:ny))  
  allocate(rhouu(1:nx,1:ny),rhovv(1:nx,1:ny)) 
  allocate(rhouup(1:nx,1:ny),rhovvp(1:nx,1:ny)) 
  allocate(rhop(1:nx,1:ny),rhoup(1:nx,1:ny),rhovp(1:nx,1:ny)) 
  allocate(u(1:nx,1:ny),v(1:nx,1:ny)) 
  allocate(rho(1:nx,1:ny))
  allocate(uu(1:nx,1:ny), vv(1:nx,1:ny))
  allocate(u0(1:nx,1:ny), v0(1:nx,1:ny))
  
  do i=1,nx
    xx(i) = float(i)*dx
    do j=1,ny
      yy(j) = float(j)*dy
    enddo
  enddo  

  if (me == 0) then
  !open(unit=78,file='/home/gravel/deformation3D/rhoSlices_filt3.dat',status='unknown')
  open(unit=78,file='./rhoSlices_filt3.dat',status='unknown')
  do k=1,nz
    do j=1,ny
      do i=1,nx
        read(78,*) rhoSlices(i,k,j)
      enddo
    enddo
  enddo
  close(78)
  do k=1,nz
    do i=1,nx
      do j=1,nx
        if (rhoSlices(i,k,j)>=0.1_pr) then
          rhoSlices(i,k,j) = 1._pr
        else 
          rhoSlices(i,k,j) = rhoSlices(i,k,j)!*10
        endif
      enddo
    enddo
  enddo
  rhoSlices = rhoSlices! - 0.5_pr
  !call updateDistance3D(rhoSlices)
  !do k=85,115
  !  call updateDistance(rhoSlices(k,:,:))
  !enddo
  rhoSlices2 = rhoSlices
  endif

  !call charge(ceiling(30.0/np)*np,100-ceiling(30.0/np)*nint(np*0.5)+1,np,me,k1,kN)
  !call charge(ceiling(30.0/np)*np,100-nint(ceiling(30.0/np)*0.5)+1,np,me,k1,kN)
  call charge(N,1,np,me,k1,kN)
  do myproc=0,np-1
    if (myproc == me) write(*,*) "me: ",me," ",k1," ",kN
  enddo
  call MPI_BARRIER(MPI_COMM_WORLD, statinfo)

  !! COMMUNICATION rhoSlices
  call MPI_BCAST(rhoSlices(:,:,:), N*N*N, MPI_PR, 0, MPI_COMM_WORLD, statinfo) 
  !call MPI_BARRIER(MPI_COMM_WORLD, statinfo)

  do kt = 1,20!615
    if (me == 0) then
    write(*,*)"time iteration : ",kt

    !open(10,file="/home/gravel/deformation3D/vitesse2/vitesse"//str(kt+140-1)//".txt", status='OLD')  
    open(10,file="./vitesse2/vitesse"//str(kt+140-1)//".txt", status='OLD')  
    do j=1,ny
      do i=1,nx
        read(10,*) u0(j,i), v0(j,i)
      enddo
    enddo
    close(10) 
    u = u0!*dx!/(179.0/199.0)
    v = v0!*dy!/(179.0/199.0)
    dt = 1._pr !0.9_pr/(eepsilon + maxval(abs(u))/dx + maxval(abs(v))/dy) 
    endif

    !! COMMUNICATION u v dt
    call MPI_BCAST(u, N*N, MPI_PR, 0, MPI_COMM_WORLD, statinfo) 
    call MPI_BCAST(v, N*N, MPI_PR, 0, MPI_COMM_WORLD, statinfo) 
    call MPI_BCAST(dt, 1, MPI_PR, 0, MPI_COMM_WORLD, statinfo) 
    maxtmp = maxval(rhoSlices(k1:kN,:,:))
    mintmp = minval(rhoSlices(k1:kN,:,:))
    call MPI_BARRIER(MPI_COMM_WORLD,statinfo)
    call MPI_ALLREDUCE(maxtmp, maxtot, 1, MPI_PR, MPI_MAX, MPI_COMM_WORLD, statinfo)
    call MPI_ALLREDUCE(mintmp, mintot, 1, MPI_PR, MPI_MIN, MPI_COMM_WORLD, statinfo)

    !u = u0
    !v = v0
    !dt = 0.9_pr/(eepsilon + maxval(abs(u))/dx + maxval(abs(v))/dy) 
    if (me == 0) write(*,*)"dt ",dt
    if ((me == 0).and.(mod(kt,idisplay) == 0)) then
      open(unit=79,file='results/rho'//str(kt+140-1)//'.vtk',status='unknown')
        write(79,'(1A26)') '# vtk DataFile Version 2.0'
        write(79,'(a)') 'rho'
        write(79,'(a)') 'ASCII'
        write(79,'(a)') 'DATASET STRUCTURED_POINTS'
        write(79,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,nz
        write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 0.,0.,0.
        !write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 24.768700, 24.768700, 24.000000
        write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx, dy, dz
        write(79,'(a,I9)') 'POINT_DATA' , nx*ny*nz
        write(79,'(a)') 'SCALARS values double'
        write(79,'(a)') 'LOOKUP_TABLE default'
      
        do k=1,nz
          do j=1,ny
            do i=1,nx
                write(79,*) rhoSlices2(i,k,j)!+0.2_pr
            enddo
          enddo
        enddo
    endif
    if (me == 0) rhoSlices2 = 0._pr
        tPic = t
        do while (tPic<float(kt))
          tPic = tPic + dt
          iter = iter + 1
    !do k=1,nz
    do k=k1,kN
      !! COMMUNICATION rhoSlices(k,:,:) -> rho
      !rho(:,:) = rhoSlices(k,:,:)

      call SetInitialCondition(rhoSlices(k,:,:), u, v, rho, rhou, rhov)
      !u = u0
      !v = v0
      do i=1,nx
        do j=1,ny
          if (rho(i,j).lt.1e-6) then
            u(i,j) = 0._pr 
            v(i,j) = 0._pr
          endif
        enddo
      enddo     
      !rho = rho0

      if (maxval(rho(:,:))>1.e-6) then
          !write(*,*)"tPic       kt        t       k         ",tPic,"         ",kt,"      ",t,"          ",k
          !call AdvanceTime(kt, t, 3, 5, 0, 1, rho, u, v, rhou, rhov)
          call AdvanceTime(kt, t, 0, 3, 4, 1, rho, u, v, rhou, rhov)

          !if (mod(iter,iUpdateDist) == 0) call updateDistance(rho)
      endif

      !! COMMUNICATION rho(:,:) -> rhoSlices(k,:,:)
      rhoSlices(k,:,:) = rho(:,:)
    enddo
    call MPI_BARRIER(MPI_COMM_WORLD, statinfo)
          !if (mod(iter,iUpdateDist) == 0) call updateDistance3D(rhoSlices)
          !if (mod(iter,1) == 0) call updateDistance3D(rhoSlices)
          !if (mod(iter,iUpdateDist) == 0) call updateDistance3D_para(rhoSlices, np, me, k1, kN, statinfo)
        enddo
          !if (mod(kt,iUpdateDist) == 0) call updateDistance3D_para(rhoSlices, np, me, k1, kN, statinfo)
          !if (mod(kt,1) == 0) call updateDistance3D(rhoSlices)

    call MPI_BARRIER(MPI_COMM_WORLD, statinfo)
    maxtmp2 = maxval(rhoSlices(k1:kN,:,:))
    mintmp2 = minval(rhoSlices(k1:kN,:,:))
    call MPI_ALLREDUCE(maxtmp2, maxtot2, 1, MPI_PR, MPI_MAX, MPI_COMM_WORLD, statinfo)
    call MPI_ALLREDUCE(mintmp2, mintot2, 1, MPI_PR, MPI_MIN, MPI_COMM_WORLD, statinfo)
    !write(*,*)"me maxtot mintot maxtot2 mintot2 ",me,"    ",maxtot,"     ",mintot,"    ",maxtot2,"     ",mintot2
    !if ((maxtot2-mintot2).gt.1.e-6)  rhoSlices(:,:,:)=(rhoSlices(:,:,:)-mintot2)/(maxtot2-mintot2)
    !rhoSlices(:,:,:)=(maxtot-mintot)*rhoSlices(:,:,:) + mintot
    t = tPic
    !write(*,*) "tPic me ",tPic," ",me
    call MPI_ALLREDUCE(t, t, 1, MPI_PR, MPI_MAX, MPI_COMM_WORLD, statinfo) 
    !write(*,*)"me before GATHER    ",me,"                 ",(kN-k1+1)*N*N,"           ",maxval(rhoSlices(k1:kN,:,:))

    !if (mod(kt,iUpdateDist) == 0) call updateDistance3D_para(rhoSlices, np, me, k1, kN, statinfo)

    do myproc=0,np-1
      rcounts(myproc+1) = (kN-k1+1)*N*N
    enddo
    !write(*,*)"me ", me, " ", k1, " ", kN
    !rcounts(me+1) = (kN-k1+1)*N*N
    !call MPI_BARRIER(MPI_COMM_WORLD, statinfo)
    !call MPI_ALLGATHER(rcounts, np, MPI_INTEGER, rcounts, np, MPI_INTEGER, MPI_COMM_WORLD, statinfo)
    !write(*,*) "me rcounts ",me, "                 ",rcounts
    call MPI_GATHER((kN-k1+1)*N*N, 1, MPI_INTEGER, rcounts(me+1), 1, MPI_INTEGER, 0, MPI_COMM_WORLD, statinfo)
    call MPI_BCAST(rcounts, np, MPI_INTEGER, 0, MPI_COMM_WORLD, statinfo) 
    !rcounts(me+1) = (kN-k1+1)*N*N
    !call MPI_BARRIER(MPI_COMM_WORLD, statinfo)
    !write(*,*) "me rcounts ",me, "                 ",rcounts

    !displs(1) = (100-ceiling(30.0/np)*nint(np*0.5))*N*N
    displs(1) = 0!(100-ceiling(30.0/np)*nint(np*0.5))*N*N
    do myproc=1,np-1
      displs(myproc+1) = displs(myproc+1-1) + rcounts(myproc+1-1)
    enddo
    !call MPI_BARRIER(MPI_COMM_WORLD, statinfo)
    !write(*,*) "me disp ",me, "                 ",displs
    !displs = 0
    do k=1,N
      do i=1,N
        do j=1,N
          tmp(j,i,k) = rhoSlices(k,i,j)
        enddo
      enddo
    enddo
    rhoSlices = tmp!+0.5_pr
    call MPI_BARRIER(MPI_COMM_WORLD, statinfo)
    !write(*,*)"me GATHER    ",me,"                 ",(kN-k1+1)*N*N,"           ",maxval(rhoSlices(:,:,k1:kN))
    call MPI_GATHERV(rhoSlices(:,:,k1:kN), (kN-k1+1)*N*N, MPI_PR, rhoSlices2, rcounts, displs, MPI_PR, 0, MPI_COMM_WORLD, statinfo)
    !if (me ==0) write(*,*)"AFTER GATHERV  ",maxval(rhoSlices2)

    if (me == 0) then
    do k=1,N
      do i=1,N
        do j=1,N
          tmp(k,i,j) = rhoSlices2(j,i,k)
        enddo
      enddo
    enddo
    rhoSlices2 = tmp!-0.5_pr
    endif
    do k=1,N
      do i=1,N
        do j=1,N
          tmp(k,i,j) = rhoSlices(j,i,k)
        enddo
      enddo
    enddo
    rhoSlices = tmp!-0.5_pr
    call MPI_BARRIER(MPI_COMM_WORLD, statinfo)

    if (me == 0) write(*,*)"t                    ",t

    if ((me == 0).and.(mod(kt,idisplay) == 0)) close(79)    
 enddo  
 
 if (me == 0) deallocate(rhoSlices2)
 deallocate(rhoSlices)
 deallocate(rho0) 
 deallocate(xx,yy)
 deallocate(u,v,rho,rhou,rhov) 
 deallocate(rhoup,rhovp,rhop)
 deallocate(uu,vv,rhouu,rhovv,rhouup,rhovvp)
 deallocate(u0,v0)
 deallocate(displs,rcounts)
 deallocate(tmp)
 deallocate(ValTop1,ValBot1)
 
 call MPI_FINALIZE(statinfo)
end program def3D
