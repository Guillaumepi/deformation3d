program def3D
  use variables
  use advection
  use AdvectionProblem
  use TimeScheme
  
  implicit none
  integer :: i, j, k, kt, idisplay, iUpdateDist, iter, picNum, p, l, q, r, s, pix1, pix2 
  real(pr) :: Tps, threshold, surf, maxv
  real(pr), dimension(:,:), allocatable :: rho, uu, vv, u0, v0, u, v, rhou, rhov 
  real(pr) :: t, tPic, xi, yj, distW, distWb, maxLS, minLS, seuilLS
  real(pr), dimension(:,:), allocatable :: rhoSlices, rhoSlices2
  integer, dimension(:), allocatable :: rcounts, displs
  real(pr), dimension(:,:), allocatable :: tmp, tmp1, tmp2, un, zero, tmpbool
  integer, dimension(:,:), allocatable :: dir1, dir2, dir3, dir4, Nseed, skel, skel2

  N =  200 !180 !400!180
  nx = 200 !180 !400!180
  ny = 200 !180 !400!180
  eepsilon = 1.e-6_pr
  dt = 1
  threshold = 0.001
  dx = 1._pr !179.0/199.0!1!200/nx!200._pr/nx
  dy = 1._pr !179.0/199.0!200/ny!200._pr/ny
  dz = 1.
  Tps = 1.  

  allocate(rhoSlices2(nx,ny))
  allocate(un(N,N),zero(N,N))
  allocate(tmp(N,N),tmp1(N,N),tmp2(N,N),tmpbool(N,N))
  allocate(rhoSlices(nx,ny))
  allocate(xx(1:nx),yy(1:ny)) 
  allocate(xp(1:nx,1:ny),yp(1:nx,1:ny)) 
  allocate(rho0(1:nx,1:ny)) 
  allocate(rhou(1:nx,1:ny),rhov(1:nx,1:ny))  
  allocate(rhouu(1:nx,1:ny),rhovv(1:nx,1:ny)) 
  allocate(rhouup(1:nx,1:ny),rhovvp(1:nx,1:ny)) 
  allocate(rhop(1:nx,1:ny),rhoup(1:nx,1:ny),rhovp(1:nx,1:ny)) 
  allocate(u(1:nx,1:ny),v(1:nx,1:ny)) 
  allocate(rho(1:nx,1:ny))
  allocate(uu(1:nx,1:ny), vv(1:nx,1:ny))
  allocate(u0(1:nx,1:ny), v0(1:nx,1:ny))
  allocate(dir1(N,N),dir2(N,N),dir3(N,N),dir4(N,N),Nseed(N,N),skel(N,N),skel2(N,N))

  un = 1._pr
  zero = 0._pr

    do i=1,nx
      xx(i) = 1+(float(i)-1)*dx
      do j=1,ny
        yy(j) = 1+(float(j)-1)*dy
      enddo
    enddo  

  picNum = 140
    !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/TEST6/IMAGES2/Image_"//str(picNum)//".dat",&
!status='unknown')
    open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/mask200/Image_"//str(picNum)//".txt",&
!    !open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/BB18/rho0/rho"//str(picNum)//".txt",&
!status='unknown')
!    open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/mask400/Image_"//str(picNum)//".txt",&
!status='unknown')
    !open(unit=78,file="/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_ini0.txt",&
status='unknown')
!    open(unit=78,file="/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_ini400.txt",&
!status='unknown')
  do k=1,nx
    do j=1,ny
    !read(78,*) rhoSlices2(k,:)
    read(78,*) rhoSlices2(k,j)
    !if ((mod(j,2)==0).and.(mod(k,2)==0)) then 
    !        read(78,*) rhoSlices2(k/2,j/2)
    !else
    !        read(78,*)
    !endif
    enddo
  enddo
  close(78)
 !rhoSlices2 = 10*rhoSlices2
 !do k=1,nx
 !do j=1,ny
 !if (rhoSlices2(k,j)<0.1_pr) then
 !        rhoSlices2(k,j) = 0._pr
 !endif
 !!if (k<ny/2) rhoSlices2(k,j) = 0._pr
 !!if (k<ny/2-50) rhoSlices2(k,j) = 0._pr
 !enddo
 !enddo

 tmpbool = 0
 tmpbool(ny/2,:) = 1

! do p=1,2
!   tmp = rhoSlices2
!   surf = 0
!   do i=2,N-1
!     do j=2,N-1
!       surf
!
!
















  !rhoSlices2=1._pr
  !y recule
  !do k=6,nx
  !  do j=1,ny
  !      rhoSlices(k,j)=rhoSlices2(k-5,j)
  !  enddo
  !enddo
  !y avance
  !do k=1,nx-5
  !  do j=1,ny
  !      rhoSlices(k,j)=rhoSlices2(k+5,j)
  !  enddo
  !enddo
  !x recule
  !do k=1,nx
  !  do j=6,ny
  !      rhoSlices(k,j)=rhoSlices2(k,j-5)
  !  enddo
  !enddo
  !rhoSlices2=rhoSlices
  !!x avance
  !do k=1,nx
  !  do j=1,ny-5
  !      rhoSlices(k,j)=rhoSlices2(k,j+5)
  !  enddo
  !enddo
  rhoSlices = rhoSlices2!*0.0001
  !rhoSlices = nint(rhoSlices)!rhoSlices-(maxval(rhoSlices)+minval(rhoSlices))*0.5_pr
  rhoSlices = rhoSlices!-0.5_pr!(maxval(rhoSlices)+minval(rhoSlices))*0.5_pr
  !rhoSlices = sign(1._pr,rhoSlices)
  rhoSlices2 = rhoSlices!-0.5_pr

  open(unit=80,file='skeleton.txt',status='unknown')

  t = 0
  iter = 0
  idisplay = 10
  iUpdateDist = 5
  do kt = 1,615
  !write(*,*)"time iteration : ",kt
!    open(unit=78,file="/Users/guillaume_ravel/Documents/optimaltransport/BB18/rho1/rho"//str(kt+picNum-1)//".txt",&
!status='unknown')
!  do k=1,nx
!    read(78,*) rhoSlices2(k,:)
!  enddo
!  close(78)
  !  !open(unit=78,file="/Users/guillaume_ravel/Documents/poisson_centering/TEST6/IMAGES2/Image_"//str(kt+picNum-1)//".dat",status='unknown')
  !do k=1,nx
  !  do j=1,ny
  !      read(78,*) rhoSlices2(k,j)
  !  enddo
  !enddo
  !close(78)
  !rhoSlices=rhoSlices2
  !rhoSlices = rhoSlices-0.5_pr
  !open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesse2bis/vitesse"//str(kt+picNum-1)//".txt", &
!status='OLD')  
!  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesse200/vitesse"//str(kt+picNum-1)//".txt", &
  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesses200/vitesse"//str(kt+picNum-1)//".txt", &
  !open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/BB18/vitesses/vitesse"//str(kt+picNum-1)//"_0.txt", &
!  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesse5/vitesse"//str(kt+picNum-1)//".txt", &
!  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesse"//str(kt+picNum-1)//".txt", &
!  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/OpticalFlow/vitesse200/vitesse"//str(kt+picNum-1)//".txt", &
status='OLD')  
  !open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/OpticalFlow/Dense_Motion/Data/TEST6/im"//str(kt+picNum-1)//".pgm.motion", &
  !open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/OpticalFlow/Dense_Motion/Data/TEST6/im"//str(kt+picNum)//".pgm.motion", &
!status='OLD')  
!  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/LagrangienTO/vitesses/vitesse"//str(kt+picNum-1)//".txt", &
!status='OLD')  
  !open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesse"//str(kt+picNum-1)//".txt", status='OLD')  
  do j=1,nx
    do i=1,ny
        !read(10,'(F15.6,F15.6)') u0(j,i), v0(j,i)
        read(10,*) u0(j,i), v0(j,i)
        !if ((mod(j,2)==0).and.(mod(i,2)==0)) then 
        !        read(10,*) u0(j/2,i/2), v0(j/2,i/2)
        !else
        !        read(10,*)
        !endif
    enddo
  enddo
  close(10) 
 ! do j=1,nx
 ! do i=1,ny
 ! u(i,j)=u0(j,i)
 ! v(i,j)=v0(j,i)
 ! enddo
 ! enddo
  !do j=1,10
  !do i=1,ny
  !rhoSlices2(i,j)=0._pr
  !rhoSlices2(j,i)=0._pr
  !enddo
  !enddo
  !do j=nx-10,nx
  !do i=1,ny
  !rhoSlices2(i,j)=0._pr
  !rhoSlices2(j,i)=0._pr
  !enddo
  !enddo
 ! u0=u
 ! v0=v
  u0(:,:) = u0(:,:)!/dx!24.768700/(1.0/15000)
  v0(:,:) = v0(:,:)!/dy!24.0/(1.0/15000)
  !write(*,*)"velocity : ",24.0/(1.0/15000)
  !write(*,*) "max velocity ", maxval(abs(u0))/dx + maxval(abs(v0))/dy ! abs() necessaire au cas ou a<0
  if (mod(kt,idisplay)==0) then
    rhoSlices2 = rhoSlices2 - 0.5_pr*(maxval(rhoSlices2) + minval(rhoSlices2))
    call updateDistance(rhoSlices2)
    dir1 = 0
    dir2 = 0
    dir3 = 0
    dir4 = 0
    Nseed = 0
    skel = 0
    do i=2,N-1
      do j=2,N-1
        if ((rhoSlices2(i,j)>rhoSlices2(i+1,j)).and.(rhoSlices2(i,j)>rhoSlices2(i-1,j))) dir1(i,j) = 1
        if ((rhoSlices2(i,j)>rhoSlices2(i,j+1)).and.(rhoSlices2(i,j)>rhoSlices2(i,j-1))) dir2(i,j) = 1
        if ((rhoSlices2(i,j)>rhoSlices2(i+1,j+1)).and.(rhoSlices2(i,j)>rhoSlices2(i-1,j-1))) dir3(i,j) = 1
        if ((rhoSlices2(i,j)>rhoSlices2(i+1,j-1)).and.(rhoSlices2(i,j)>rhoSlices2(i-1,j+1))) dir4(i,j) = 1
        Nseed(i,j) = dir1(i,j) + dir2(i,j) + dir3(i,j) + dir4(i,j)
        if ((Nseed(i,j)>=2).and.(rhoSlices2(i,j)>0)) skel(i,j) = 1
      enddo
    enddo
    l = 1
    pix1 = 0
    pix2 = 0
    do while ((pix1==1).or.(pix2==1).or.(l==1))
    pix1 = 0
    pix2 = 0
    l = l + 1
    skel2 = skel
    do i=2,N-1
      do j=2,N-1
        maxv = 0
        if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
        if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
        if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
        if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
        if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
        if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
        if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
        if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
        Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
        if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i+1,j)==0).or.(skel(i-1,j)==0).or.(skel(i,j+1)==0)).and.((skel(i,j-1)==0).or.(skel(i-1,j)==0).or.(skel(i,j+1)==0))) skel2(i,j) = 0
      enddo
    enddo
    if (abs(sum(skel)-sum(skel2))>0) pix1 = 1
    skel = skel2
    skel2 = skel
    do i=2,N-1
      do j=2,N-1
        maxv = 0
        if ((skel(i+1,j)==0).and.(skel(i+1,j+1)==1)) maxv = maxv + 1
        if ((skel(i+1,j+1)==0).and.(skel(i,j+1)==1)) maxv = maxv + 1
        if ((skel(i,j+1)==0).and.(skel(i-1,j+1)==1)) maxv = maxv + 1
        if ((skel(i-1,j+1)==0).and.(skel(i-1,j)==1)) maxv = maxv + 1
        if ((skel(i-1,j)==0).and.(skel(i-1,j-1)==1)) maxv = maxv + 1
        if ((skel(i-1,j-1)==0).and.(skel(i,j-1)==1)) maxv = maxv + 1
        if ((skel(i,j-1)==0).and.(skel(i+1,j-1)==1)) maxv = maxv + 1
        if ((skel(i+1,j-1)==0).and.(skel(i+1,j)==1)) maxv = maxv + 1
        Nseed(i,j) = skel(i+1,j) + skel(i+1,j+1) + skel(i,j+1) + skel(i-1,j+1) + skel(i-1,j) + skel(i-1,j-1) + skel(i,j-1) + skel(i+1,j-1)
        if ((skel(i,j).eq.1).and.(maxv.eq.1).and.(Nseed(i,j)>=2).and.(Nseed(i,j)<=6).and.((skel(i+1,j)==0).or.(skel(i-1,j)==0).or.(skel(i,j-1)==0)).and.((skel(i+1,j)==0).or.(skel(i,j+1)==0).or.(skel(i,j-1)==0))) skel2(i,j) = 0
      enddo
    enddo
    if (abs(sum(skel)-sum(skel2))>0) pix2 = 1
    skel = skel2
    enddo
    skel2 = 0
    do i=2,N-1
      do j=2,N-1
        if (skel(i,j) .eq. 1) then
            skel2(i,j) = 1
            if (skel(i-1,j).eq.1) then
                    maxv = maxval((/rhoSlices2(i+1,j-1),rhoSlices2(i+1,j),rhoSlices2(i+1,j+1),rhoSlices2(i,j)/))
                    if (rhoSlices2(i+1,j-1).eq.maxv) skel2(i+1,j-1) = 1
                    if (rhoSlices2(i+1,j).eq.maxv) skel2(i+1,j) = 1
                    if (rhoSlices2(i+1,j+1).eq.maxv) skel2(i+1,j+1) = 1
                    if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
            endif
            if (skel(i+1,j).eq.1) then
                    maxv = maxval((/rhoSlices2(i-1,j-1),rhoSlices2(i-1,j),rhoSlices2(i-1,j+1),rhoSlices2(i,j)/))
                    if (rhoSlices2(i-1,j-1).eq.maxv) skel2(i-1,j-1) = 1
                    if (rhoSlices2(i-1,j).eq.maxv) skel2(i-1,j) = 1
                    if (rhoSlices2(i-1,j+1).eq.maxv) skel2(i-1,j+1) = 1
                    if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
            endif
            if (skel(i,j+1).eq.1) then
                    maxv = maxval((/rhoSlices2(i-1,j-1),rhoSlices2(i,j-1),rhoSlices2(i+1,j-1),rhoSlices2(i,j)/))
                    if (rhoSlices2(i-1,j-1).eq.maxv) skel2(i-1,j-1) = 1
                    if (rhoSlices2(i,j-1).eq.maxv) skel2(i,j-1) = 1
                    if (rhoSlices2(i+1,j-1).eq.maxv) skel2(i+1,j-1) = 1
                    if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
            endif
            if (skel(i,j-1).eq.1) then
                    maxv = maxval((/rhoSlices2(i-1,j+1),rhoSlices2(i,j+1),rhoSlices2(i+1,j+1),rhoSlices2(i,j)/))
                    if (rhoSlices2(i-1,j+1).eq.maxv) skel2(i-1,j+1) = 1
                    if (rhoSlices2(i,j+1).eq.maxv) skel2(i,j+1) = 1
                    if (rhoSlices2(i+1,j+1).eq.maxv) skel2(i+1,j+1) = 1
                    if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
            endif
            if (skel(i-1,j-1).eq.1) then
                    maxv = maxval((/rhoSlices2(i,j+1),rhoSlices2(i+1,j+1),rhoSlices2(i+1,j),rhoSlices2(i,j)/))
                    if (rhoSlices2(i,j+1).eq.maxv) skel2(i,j+1) = 1
                    if (rhoSlices2(i+1,j+1).eq.maxv) skel2(i+1,j+1) = 1
                    if (rhoSlices2(i+1,j).eq.maxv) skel2(i+1,j) = 1
                    if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
            endif
            if (skel(i+1,j+1).eq.1) then
                    maxv = maxval((/rhoSlices2(i,j-1),rhoSlices2(i-1,j-1),rhoSlices2(i-1,j),rhoSlices2(i,j)/))
                    if (rhoSlices2(i,j-1).eq.maxv) skel2(i,j-1) = 1
                    if (rhoSlices2(i-1,j-1).eq.maxv) skel2(i-1,j-1) = 1
                    if (rhoSlices2(i-1,j).eq.maxv) skel2(i-1,j) = 1
                    if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
            endif
            if (skel(i-1,j+1).eq.1) then
                    maxv = maxval((/rhoSlices2(i,j-1),rhoSlices2(i+1,j-1),rhoSlices2(i+1,j),rhoSlices2(i,j)/))
                    if (rhoSlices2(i,j-1).eq.maxv) skel2(i,j-1) = 1
                    if (rhoSlices2(i+1,j-1).eq.maxv) skel2(i+1,j-1) = 1
                    if (rhoSlices2(i+1,j).eq.maxv) skel2(i+1,j) = 1
                    if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
            endif
            if (skel(i+1,j-1).eq.1) then
                    maxv = maxval((/rhoSlices2(i,j+1),rhoSlices2(i-1,j+1),rhoSlices2(i-1,j),rhoSlices2(i,j)/))
                    if (rhoSlices2(i,j+1).eq.maxv) skel2(i,j+1) = 1
                    if (rhoSlices2(i-1,j+1).eq.maxv) skel2(i-1,j+1) = 1
                    if (rhoSlices2(i-1,j).eq.maxv) skel2(i-1,j) = 1
                    if (rhoSlices2(i,j).eq.maxv) skel2(i,j) = 1
            endif
        endif
      enddo
    enddo
    skel = skel2
!    skel2 = 0
!    do i=2,N-1
!      do j=2,N-1
!        if (skel(i,j) .eq. 1) then
!            if (skel(i-1,j).eq.1) then
!                    maxv = maxval((/rhoSlices2(i+1,j-1),rhoSlices2(i+1,j),rhoSlices2(i+1,j+1)/))
!                    if (rhoSlices2(i+1,j-1).eq.maxv) skel2(i+1,j-1) = 1
!                    if (rhoSlices2(i+1,j).eq.maxv) skel2(i+1,j) = 1
!                    if (rhoSlices2(i+1,j+1).eq.maxv) skel2(i+1,j+1) = 1
!                    skel2(i,j) = 1
!            endif
!            if (skel(i+1,j).eq.1) then
!                    maxv = maxval((/rhoSlices2(i-1,j-1),rhoSlices2(i-1,j),rhoSlices2(i-1,j+1)/))
!                    if (rhoSlices2(i-1,j-1).eq.maxv) skel2(i-1,j-1) = 1
!                    if (rhoSlices2(i-1,j).eq.maxv) skel2(i-1,j) = 1
!                    if (rhoSlices2(i-1,j+1).eq.maxv) skel2(i-1,j+1) = 1
!                    skel2(i,j) = 1
!            endif
!            if (skel(i,j+1).eq.1) then
!                    maxv = maxval((/rhoSlices2(i-1,j-1),rhoSlices2(i,j-1),rhoSlices2(i+1,j-1)/))
!                    if (rhoSlices2(i-1,j-1).eq.maxv) skel2(i-1,j-1) = 1
!                    if (rhoSlices2(i,j-1).eq.maxv) skel2(i,j-1) = 1
!                    if (rhoSlices2(i+1,j-1).eq.maxv) skel2(i+1,j-1) = 1
!                    skel2(i,j) = 1
!            endif
!            if (skel(i,j-1).eq.1) then
!                    maxv = maxval((/rhoSlices2(i-1,j+1),rhoSlices2(i,j+1),rhoSlices2(i+1,j+1)/))
!                    if (rhoSlices2(i-1,j+1).eq.maxv) skel2(i-1,j+1) = 1
!                    if (rhoSlices2(i,j+1).eq.maxv) skel2(i,j+1) = 1
!                    if (rhoSlices2(i+1,j+1).eq.maxv) skel2(i+1,j+1) = 1
!                    skel2(i,j) = 1
!            endif
!            if (skel(i-1,j-1).eq.1) then
!                    maxv = maxval((/rhoSlices2(i,j+1),rhoSlices2(i+1,j+1),rhoSlices2(i+1,j)/))
!                    if (rhoSlices2(i,j+1).eq.maxv) skel2(i,j+1) = 1
!                    if (rhoSlices2(i+1,j+1).eq.maxv) skel2(i+1,j+1) = 1
!                    if (rhoSlices2(i+1,j).eq.maxv) skel2(i+1,j) = 1
!                    skel2(i,j) = 1
!            endif
!            if (skel(i+1,j+1).eq.1) then
!                    maxv = maxval((/rhoSlices2(i,j-1),rhoSlices2(i-1,j-1),rhoSlices2(i-1,j)/))
!                    if (rhoSlices2(i,j-1).eq.maxv) skel2(i,j-1) = 1
!                    if (rhoSlices2(i-1,j-1).eq.maxv) skel2(i-1,j-1) = 1
!                    if (rhoSlices2(i-1,j).eq.maxv) skel2(i-1,j) = 1
!                    skel2(i,j) = 1
!            endif
!            if (skel(i-1,j+1).eq.1) then
!                    maxv = maxval((/rhoSlices2(i,j-1),rhoSlices2(i+1,j-1),rhoSlices2(i+1,j)/))
!                    if (rhoSlices2(i,j-1).eq.maxv) skel2(i,j-1) = 1
!                    if (rhoSlices2(i+1,j-1).eq.maxv) skel2(i+1,j-1) = 1
!                    if (rhoSlices2(i+1,j).eq.maxv) skel2(i+1,j) = 1
!                    skel2(i,j) = 1
!            endif
!            if (skel(i+1,j-1).eq.1) then
!                    maxv = maxval((/rhoSlices2(i,j+1),rhoSlices2(i-1,j+1),rhoSlices2(i-1,j)/))
!                    if (rhoSlices2(i,j+1).eq.maxv) skel2(i,j+1) = 1
!                    if (rhoSlices2(i-1,j+1).eq.maxv) skel2(i-1,j+1) = 1
!                    if (rhoSlices2(i-1,j).eq.maxv) skel2(i-1,j) = 1
!                    skel2(i,j) = 1
!            endif
!        endif
!      enddo
!    enddo
!    skel = skel2

    
        
    !open(unit=79,file='results/rhomatlab'//str(kt+140-1)//'.txt',status='unknown')
    !open(unit=79,file='results/rho'//str(kt+140-1)//'.txt',status='unknown')
    !open(unit=79,file='results/skel'//str(kt+140-1)//'.vtk',status='unknown')
    open(unit=79,file='results/sol'//str(kt+140-1)//'.vtk',status='unknown')
      write(79,'(1A26)') '# vtk DataFile Version 2.0'
      write(79,'(a)') 'rho'
      write(79,'(a)') 'ASCII'
      write(79,'(a)') 'DATASET STRUCTURED_POINTS'
      write(79,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1 !200,200,1 !nx,ny,1
      write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 1.,1.,1.
      write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 1.,1.,1.
      !write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 24.7687, 24.7687, 24.0 !179.0/199.0,179.0/199.0,1.0!dx,dy, 1.
      write(79,'(a,I6)') 'POINT_DATA          ' , nx*ny !200*200 !nx*ny!nx*ny
      write(79,'(a)') 'SCALARS density float 1' !SCALARS values double
      write(79,'(a)') 'LOOKUP_TABLE default'
    
 do k=1,nx
      do j=1,ny
          !if (rhoSlices2(j,k).lt.(0.9*maxval(rhoSlices2))) then !1e-6) then
          !  write(79,*) 0._pr
          !else
          !  write(79,*) 1._pr
     ! !if ((mod(k,2)==0).and.(mod(j,2)==0)) write(79,*) rhoSlices2(j,k)
      !write(79,*) rhoSlices2(j,k)
      write(79,*) (1-tmpbool(j,k))*(rhoSlices2(j,k)*(1-skel(j,k)) + minval(rhoSlices2)*skel(j,k))
      !write(79,*) skel(j,k)
!          endif
          !write(79,*) rho0(j,k)
      enddo
      enddo
      write(80,*) kt,"    ",sum(skel)
      endif

 !do j=1,ny
 !do i=1,nx
 !  !if (rho(i,j).lt.1.e-6) then
 !  if (rho(i,j).lt.0._pr) rho(i,j)=0._pr
 !  enddo
 !  enddo
 !rho0 = rhoSlices(:,:)
 !rho = rhoSlices(:,:)
 !if ((maxval(rho(:,:))-minval(rho(:,:))).gt.1.e-6) rho(:,:) = (rho(:,:)-minval(rho(:,:)))/(maxval(rho(:,:))-minval(rho(:,:)))   
 !  if (maxval(rho(:,:)).gt.1.e-6) rho(:,:) = rho(:,:)/maxval(rho(:,:))   
 !summ = 0  
 !do j=1,ny
 !do i=1,nx
 !        summ=summ+rho(i,j)
 !enddo
 !enddo
 !  if (summ.gt.1.e-6) rho(:,:) = rho(:,:)/summ
 !rho(:,:) = rho(:,:) + 1.e-6
 !  if (maxval(rho(:,:)).gt.1.e-6) rho(:,:) = rho(:,:)/maxval(rho(:,:))   
 !write(*,*)maxval(rho(:,:))
 !maxLS = maxval(rhoSlices)
 !minLS = minval(rhoSlices)
 !seuilLS = (maxLS-minLS)*0.5_pr!/1.005_pr
 u = u0*dx
 v = v0*dy
 !do i=1,nx
 !  do j=1,ny
 !    if (rhoSlices(i,j).lt.1.e-6_pr) then
 !    !if (rhoSlices(i,j).lt.0.5_pr) then
 !      u(i,j) = 0._pr
 !      v(i,j) = 0._pr
 !!      rhoSlices(i,j) = 0._pr
 !    endif
 !  enddo
 !enddo     
  !call computeFunction2D(1._pr, u, 0._pr, tmp1, 5, 1, un, zero)
  !call computeFunction2D(1._pr, v, 0._pr, tmp2, 5, 1, zero, un)
  !!call computeFunction2D(1._pr, u, 0._pr, tmp1, 1, 1, un, zero)
  !!call computeFunction2D(1._pr, v, 0._pr, tmp2, 1, 1, zero, un)
  !write(*,*)"DIVERGENCE kt ",maxval(abs(tmp1+tmp2)),"                ",kt
 !if (kt==1) rho0i = rhoSlices(:,:)
 !rho = rho + 0.5_pr!seuilLS
 call SetInitialCondition(rhoSlices(:,:),u,v,rho,rhou,rhov)
 !rho = rho - 0.5_pr!seuilLS
 !if (mod(kt,idisplay)==0) then
 ! write(*,*)"kt u v ",kt," ",maxval(abs(u))," ",maxval(abs(v))
 ! write(*,*)"kt dt ",kt," ",1._pr/(eepsilon + maxval(abs(u))/dx + maxval(abs(v))/dy) ! abs() necessaire au cas ou a<0
 !endif

 !do j=1,ny
 !   do i=1,nx 
 !      !u0(i,j)=0
 !      !v0(i,j)=0
 !      rhou(i,j) = rho(i,j)*u(i,j) 
 !      rhov(i,j) = rho(i,j)*v(i,j) 
 !      rhouu(i,j) = rho(i,j)*u(i,j)
 !      rhovv(i,j) = rho(i,j)*v(i,j)
 !   enddo
 !enddo
  !if (kt==1) call updateDistance(rho)
  !call Norm22D(rho,linf)
  !write(*,*)"norm rho ", maxval(rho)," ",norm2(rho)," ",linf
  dt = 1._pr !0.9_pr/(eepsilon + maxval(abs(u))/dx + maxval(abs(v))/dy) ! abs() necessaire au cas ou a<0
  !write(*,*)"dt = ",dt
  !t = 0._pr
  do while (t<float(kt))
  !do while (t<1._pr)
      t = t + dt
      write(*,*)"t ",t," ",kt," ",t-kt+1
      iter = iter + 1
      u = u0
      v = v0
      !if (maxval(rho)>0._pr) call AdvanceTime(kt, t, 3, 5, 0, 1, rho, u, v)
      !call AdvanceTime(kt, t-kt+1, 0, 3, 4, 1, un, u, v, rhou, rhov)
      !dt = 0.9_pr/(eepsilon + maxval(abs(u))/dx + maxval(abs(v))/dy) ! abs() necessaire au cas ou a<0
      !call AdvanceTime(kt, t, 3, 5, 0, 1, rho, u, v, rhou, rhov)
      !call AdvanceTime(kt, t, 1, 1, 0, 1, rho, u, v, rhou, rhov)
      !write(*,*)"dt =      ",dt,"        maxU =            ",maxval(abs(u))/dx + maxval(abs(v))/dy
      call AdvanceTime(kt, dt, 0, 3, 4, 1, rho, u, v, rhou, rhov)
      !call AdvanceTime(kt, t, 0, 3, 5, 1, rho, u, v, rhou, rhov)
      !dt = 0.9_pr/(eepsilon + maxval(abs(u))/dx + maxval(abs(v))/dy) ! abs() necessaire au cas ou a<0
      !dt = 0.9_pr/(eepsilon + maxval(abs(u))/dx + maxval(abs(v))/dy) ! abs() necessaire au cas ou a<0

      !if (mod(iter,iUpdateDist)==0) call updateDistance(rho)
  enddo

  !if (mod(kt,iUpdateDist)==0) call updateDistance(rho)

 !Tps=1.
 !call transport(Tps,-1,0,3)
 !call AdvanceParticle(kt,kt*dt,5)
 !call AdvanceRK3(kt, kt*dt, 5, 1)
 !call AdvanceRK4(kt, kt*dt, 5, 1)
 !write(*,*) "maxval ",k," == ",maxval(rho(:,:))
 !if ((maxval(rho(:,:))-minval(rho(:,:))).gt.1.e-6)  rho(:,:)=(rho(:,:)-minval(rho(:,:)))/(maxval(rho(:,:))-minval(rho(:,:)))
 !rho(:,:)=(rho(:,:)-minval(rho(:,:)))/(maxval(rho(:,:))-minval(rho(:,:)))
 !rho(:,:)=rho(:,:)/maxval(rho(:,:))
 !if ((maxval(rho(:,:))-minval(rho(:,:))).gt.1.e-6) rho(:,:)=(rho(:,:)-minval(rho(:,:)))*(maxval(rhoSlices(:,:))-&
 !minval(rhoSlices(:,:)))/(maxval(rho(:,:))-minval(rho(:,:))) + minval(rhoSlices(:,:))
 !where (rho>maxval(rhoSlices)) rho = maxval(rhoSlices) 
 !!if (mod(kt,100)==0) then
 !seuilLS = (maxval(rho)-minval(rho))/1.005_pr
 !do j=1,ny
 !  do i=1,nx
 !    if (rho(i,j) > 0._pr) then !1.e-6_pr) then
 !      rho(i,j) = 1._pr
 !    else
 !      rho(i,j) = 0._pr
 !    endif
 !  enddo
 !enddo
 !endif
 rhoSlices(:,:) = rho(:,:)!/maxval(rho)!-seuilLS
 !do k=1,nx
 !do j=1,ny
 !if (rhoSlices(k,j)<1._pr*maxval(rho)) then
 !        rhoSlices(k,j) = 0._pr
 !else
 !!        rhoSlices(k,j) = 0._pr
 !endif
 !enddo
 !enddo
 !where (rhoSlices<0.5_pr) rhoSlices = 0._pr 
 !do k=1,nx
 !do j=1,ny
 !if (rhoSlices(k,j)<1.e-1) then
 !        rhoSlices(k,j) = 0._pr
 !else
 !        rhoSlices(k,j) = 1._pr
 !endif
 !!if (k<ny/2) rhoSlices2(k,j) = 0._pr
 !!if (k<ny/2-50) rhoSlices2(k,j) = 0._pr
 !enddo
 !enddo
 rhoSlices2(:,:) = rhoSlices(:,:)
!  if (mod(kt,idisplay)==0) then
!    !open(unit=79,file='results/rhomatlab'//str(kt+140-1)//'.txt',status='unknown')
!    !open(unit=79,file='results/rho'//str(kt+140-1)//'.txt',status='unknown')
!    open(unit=79,file='results/sol'//str(kt+picNum-1)//'.vtk',status='unknown')
!      write(79,'(1A26)') '# vtk DataFile Version 2.0'
!      write(79,'(a)') 'rho'
!      write(79,'(a)') 'ASCII'
!      write(79,'(a)') 'DATASET STRUCTURED_POINTS'
!      write(79,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,1
!      write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 0.,0.,0.
!      write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy, 1.
!      write(79,'(a,I6)') 'POINT_DATA' , nx*ny
!      write(79,'(a)') 'SCALARS density float 1' !SCALARS values double
!      write(79,'(a)') 'LOOKUP_TABLE default'
!    
! do k=1,nx
!      do j=1,ny
!          if (abs(rhoSlices(j,k)).lt.1e-6) then
!            write(79,*) 0._pr
!          else
!            write(79,*) rhoSlices(j,k)
!          endif
!      enddo
!      enddo
!      endif


 if (mod(kt,idisplay)==0) then
    close(79)    
    endif
 enddo  
 close(80)


!!!!! ecriture resultat
  !sortie vtk
!! open(unit=78,file='../rhoSlices.vtk',status='unknown',position='append') 
!!!!  write(78,'(1A26)') '# vtk DataFile Version 2.0'
!!!!  write(78,'(a)') 'rho'
!!!!  write(78,'(a)') 'ASCII'
!!!!  write(78,'(a)') 'DATASET STRUCTURED_POINTS'
!!!!  write(78,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,nbSlices
!!!!  write(78,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 0.,0.,0.
!!!!  write(78,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,dz
!!!!  write(78,'(a,I6)') 'POINT_DATA ' , nx*ny*nbSlices
!!!!  write(78,'(a)') 'SCALARS values double' !SCALARS density float 1'
!!!!  write(78,'(a)') 'LOOKUP_TABLE default'
!!  do kt=1,nbSlices-1,2
!!    rhoSlices(kt,:,:)=rhoSlices(kt,:,:)/maxval(rhoSlices(kt,:,:)) !!rhoSlices(kt,nx/2,ny/2) !maxval(rhoSlices(kt,:,:))
!!  enddo
!!  do kt=1,nbSlices-1,2
!!    do j=1,ny
!!      do i=1,nx
!!        if (abs(rhoSlices(kt,i,j)).lt.1E-6) then
!!          write(78,999) 0.
!!        else
!!          write(78,999)  rhoSlices(kt,i,j) 
!!        endif
!!      enddo
!!    enddo
!!  enddo
!!  999 FORMAT(5(E23.15))
!!  close(78)


 deallocate(rhoSlices2)
 deallocate(rhoSlices)
 deallocate(rho0) 
 deallocate(xx,yy)
 deallocate(u,v,rho,rhou,rhov) 
 deallocate(rhoup,rhovp,rhop)
 deallocate(uu,vv,rhouu,rhovv,rhouup,rhovvp)
 deallocate(u0,v0)
 deallocate(tmp1,tmp2,tmpbool)
 deallocate(dir1,dir2,dir3,dir4,Nseed,skel)
 deallocate(un,zero)
end program def3D

!   "Convert an integer to string."
character(len=3) function str(k)
    integer, intent(in) :: k
    write (str, '(I3.3)') k
    str = adjustl(str)
end function str
