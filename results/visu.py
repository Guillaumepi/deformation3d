import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
		                               AutoMinorLocator)
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import cmocean
ocean = cmocean.cm.dense
viridis = cm.get_cmap('viridis', 12)
inferno = cm.get_cmap('inferno', 12)
plasma = cm.get_cmap('plasma', 12)

plt.rc('font', family='sans-serif')
width=7 #double column
#width=4.5 #middle column
#width=3.42 #single column
height=width/1.618
params = {
        'mathtext.rm' : 'Arial',
        'axes.labelsize': 7,
        'font.size': 7,
        'legend.fontsize': 7,
        'xtick.labelsize': 7,
        'ytick.labelsize': 7,
        'text.usetex': False,
        'figure.figsize': [width, height]
}
plt.rcParams.update(params)
fig = plt.figure(figsize=(width,height),constrained_layout=False)
#fig = plt.figure()
printStepScatter=10
printStepShape=80
firstShape=1
lastShape=580
twoD=0
nbtot=math.floor((lastShape-firstShape+1)/printStepShape)+1

skel = np.loadtxt('skeletteq00.txt', delimiter='     ', unpack=True)
if (twoD==1):
    skel = np.loadtxt('skeletteq2D00.txt', delimiter='     ', unpack=True)
skelX = [1000*skel[0,i] for i in range(0,skel.shape[1])]
skelY = [1000*skel[1,i] for i in range(0,skel.shape[1])]

cskel = np.loadtxt('skelett00.txt', delimiter='     ', unpack=True)
if (twoD==1):
    cskel = np.loadtxt('skelett2D00.txt', delimiter='     ', unpack=True)
cskelX = [1000*cskel[0,i] for i in range(0,cskel.shape[1],printStepScatter)]
cskelY = [1000*cskel[1,i] for i in range(0,cskel.shape[1],printStepScatter)]

left = np.loadtxt('left00.txt', delimiter='     ', unpack=True)
if (twoD==1):
    left = np.loadtxt('left2D00.txt', delimiter='     ', unpack=True)
leftX = [1000*left[0,i] for i in range(0,left.shape[1])]
leftY = [1000*left[1,i] for i in range(0,left.shape[1])]

right = np.loadtxt('right00.txt', delimiter='     ', unpack=True)
if (twoD==1):
    right = np.loadtxt('right2D00.txt', delimiter='     ', unpack=True)
rightX = [1000*right[0,i] for i in range(0,right.shape[1])]
rightY = [1000*right[1,i] for i in range(0,right.shape[1])]

cleft = np.loadtxt('controlleft00.txt', delimiter='     ', unpack=True)
if (twoD==1):
    cleft = np.loadtxt('controlleft2D00.txt', delimiter='     ', unpack=True)
cleftX = [1000*cleft[0,i] for i in range(0,cleft.shape[1],printStepScatter)]
cleftY = [1000*cleft[1,i] for i in range(0,cleft.shape[1],printStepScatter)]

cright = np.loadtxt('controlright00.txt', delimiter='     ', unpack=True)
if (twoD==1):
    cright = np.loadtxt('controlright2D00.txt', delimiter='     ', unpack=True)
crightX = [1000*cright[0,i] for i in range(0,cright.shape[1],printStepScatter)]
crightY = [1000*cright[1,i] for i in range(0,cright.shape[1],printStepScatter)]

ccskel = np.loadtxt('skeletteq00.txt', delimiter='     ', unpack=True)
if (twoD==1):
    ccskel = np.loadtxt('skeletteq2D00.txt', delimiter='     ', unpack=True)
ccskelX = [1000*ccskel[0,i] for i in range(0,ccskel.shape[1],printStepScatter)]
ccskelY = [1000*ccskel[1,i] for i in range(0,ccskel.shape[1],printStepScatter)]

ccleft = np.loadtxt('left00.txt', delimiter='     ', unpack=True)
if (twoD==1):
    ccleft = np.loadtxt('left2D00.txt', delimiter='     ', unpack=True)
ccleftX = [1000*ccleft[0,i] for i in range(0,ccleft.shape[1],printStepScatter)]
ccleftY = [1000*ccleft[1,i] for i in range(0,ccleft.shape[1],printStepScatter)]

ccright = np.loadtxt('right00.txt', delimiter='     ', unpack=True)
if (twoD==1):
    ccright = np.loadtxt('right2D00.txt', delimiter='     ', unpack=True)
ccrightX = [1000*ccright[0,i] for i in range(0,ccright.shape[1],printStepScatter)]
ccrightY = [1000*ccright[1,i] for i in range(0,ccright.shape[1],printStepScatter)]

plt.plot(skelX,skelY,c='C0',label='skelett')
plt.plot(leftX,leftY,c='C1',label='left')
plt.plot(rightX,rightY,c='C2',label='right')
#plt.scatter(cskelX,cskelY,c='C0',marker='+')
#plt.scatter(cleftX,cleftY,c='C1',marker='+')
#plt.scatter(crightX,crightY,c='C2',marker='+')
plt.scatter(ccskelX,ccskelY,c='C0',marker='.')
plt.scatter(ccleftX,ccleftY,c='C1',marker='.')
plt.scatter(ccrightX,ccrightY,c='C2',marker='.')

#plt.grid()
plt.xlabel('scale (mm)')
#plt.ylabel('y (mm)')
#plt.legend(loc='upper right')
#plt.title('skeleton00')
plt.axis('equal')
axes=plt.axes()
#plt.axis('off')
axes.set_yticks([])
axes.spines['left'].set_visible(False)
axes.spines['top'].set_visible(False)
axes.spines['right'].set_visible(False)
#axes.yaxis.set_ticks_position('right')
axes.xaxis.set_ticks_position('bottom')
#plt.tight_layout()
plt.show()
#plt.savefig("./splinescontour.pdf", format='pdf', dpi=300)

left = np.loadtxt('coupe00.txt', delimiter='     ', unpack=True)
leftX = [0.3+1000*left[0,i] for i in range(0,left.shape[1])]
leftY = [1000*left[1,i] for i in range(0,left.shape[1])]

right = np.loadtxt('coupeLarge00.txt', delimiter='     ', unpack=True)
rightX = [0.5+1000*right[0,i] for i in range(0,right.shape[1])]
rightY = [1000*right[1,i] for i in range(0,right.shape[1])]

fig = plt.figure(figsize=(width,height),constrained_layout=False)
plt.plot(leftX,leftY,c='C1',label='left')
plt.plot(rightX,rightY,c='C0',label='right')
plt.scatter(leftX,leftY,c='C1',label='left',marker='.')
plt.scatter(rightX,rightY,c='C0',label='right',marker='.')
plt.ylabel('scale (mm)')
plt.axis('square')
axes=plt.axes()
#plt.axis('off')
axes.set_xticks([])
axes.spines['right'].set_visible(False)
axes.spines['top'].set_visible(False)
axes.spines['bottom'].set_visible(False)
#axes.yaxis.set_ticks_position('right')
#axes.yaxis.set_label_position("right")
#axes.xaxis.set_ticks_position('bottom')
#plt.tight_layout()
plt.show()
#plt.savefig("./coupe.pdf", format='pdf', dpi=300)

fig = plt.figure(figsize=(width,height),constrained_layout=False)
nb=0
for k in range(firstShape,lastShape,printStepShape):
    kk="{0:0=3d}".format(k)
    print("iteration: ",kk)
    skelhead = np.loadtxt('skelhead'+str(kk)+'.txt', delimiter='     ', unpack=True)
    if (twoD==1):
        skelhead = np.loadtxt('skelhead2D'+str(kk)+'.txt', delimiter='     ', unpack=True)
    skelheadX = [1000*skelhead[0,i] for i in range(0,skelhead.shape[1])]
    skelheadY = [1000*skelhead[1,i] for i in range(0,skelhead.shape[1])]

    skel = np.loadtxt('skeletteq'+str(kk)+'.txt', delimiter='     ', unpack=True)
    if (twoD==1):
        skel = np.loadtxt('skeletteq2D'+str(kk)+'.txt', delimiter='     ', unpack=True)
    skelX = [1000*skel[0,i] for i in range(0,skel.shape[1])]
    skelY = [1000*skel[1,i] for i in range(0,skel.shape[1])]

    left = np.loadtxt('left'+str(kk)+'.txt', delimiter='     ', unpack=True)
    if (twoD==1):
        left = np.loadtxt('left2D'+str(kk)+'.txt', delimiter='     ', unpack=True)
    leftX = [1000*left[0,i] for i in range(0,left.shape[1])]
    leftY = [1000*left[1,i] for i in range(0,left.shape[1])]

    right = np.loadtxt('right'+str(kk)+'.txt', delimiter='     ', unpack=True)
    if (twoD==1):
        right = np.loadtxt('right2D'+str(kk)+'.txt', delimiter='     ', unpack=True)
    rightX = [1000*right[0,i] for i in range(0,right.shape[1])]
    rightY = [1000*right[1,i] for i in range(0,right.shape[1])]

    #plt.plot(skelX,skelY,c='C'+str(nb),label='shape'+str(k))
    #plt.plot(leftX,leftY,c='C'+str(nb))
    #plt.plot(rightX,rightY,c='C'+str(nb))
    skelheadXrot=[-skelheadY[i] for i in range(0,skelhead.shape[1])]
    skelheadYrot=[skelheadX[i] for i in range(0,skelhead.shape[1])]
    skelXrot=[-skelY[i] for i in range(0,skel.shape[1])]
    skelYrot=[skelX[i] for i in range(0,skel.shape[1])]
    leftXrot=[-leftY[i] for i in range(0,left.shape[1])]
    leftYrot=[leftX[i] for i in range(0,left.shape[1])]
    rightXrot=[-rightY[i] for i in range(0,right.shape[1])]
    rightYrot=[rightX[i] for i in range(0,right.shape[1])]

    #plt.plot(skelXrot,skelYrot,c='C'+str(nb),label='t= '+str(int(k/10))+' ms',zorder=nb)
    #plt.plot(skelheadXrot,skelheadYrot,c='C'+str(nb),zorder=nb)
    #plt.scatter(skelheadXrot[0],skelheadYrot[0],c='C'+str(nb),marker='+',zorder=nb)
    plt.plot(skelXrot,skelYrot,c=plasma(nb/nbtot*0.8),label='t= '+str(int(k/10))+' ms',zorder=nb)
    plt.plot(skelheadXrot,skelheadYrot,c=plasma(nb/nbtot*0.8),zorder=nb)
    plt.scatter(skelheadXrot[0],skelheadYrot[0],c=plasma(nb/nbtot*0.8),marker='+',zorder=nb)
    plt.plot(leftXrot,leftYrot,c='C'+str(nb),zorder=nb)
    plt.plot(rightXrot,rightYrot,c='C'+str(nb),zorder=nb)
    nb=nb+1

#plt.xlabel('x (mm)')
#plt.ylabel('scale (mm)')
#plt.title('skel')
plt.axis('square')
axes=plt.axes()
#plt.axis('off')
print(axes.get_xlim())
print(axes.get_ylim())
axes.set_xticks([])
#plt.axis('equal')
plt.legend(loc="upper right")

axes.spines['left'].set_visible(False)
axes.spines['top'].set_visible(False)
axes.spines['bottom'].set_visible(False)
axes.yaxis.set_ticks_position('right')
plt.ylabel('scale (mm)')
axes.yaxis.set_label_position("right")
#axes.xaxis.set_ticks_position('bottom')

#plt.legend(mylabels,loc="upper right")
#plt.axis('square')
#plt.legend(#[ls, lexp],     # The line objects FIG
#	labels=['mylabel'],#line_labels,   # The labels for each line
#	loc="upper right"   # Position of legend
#	#borderaxespad=0.1,    # Small spacing around legend box
#	#title="Legend Title"  # Title for the legend
#)
#fig.tight_layout()
#plt.tight_layout()
plt.show()
#plt.savefig("./midlines3Dsplineshead.pdf", format='pdf', dpi=300)




#subplt[1,1].yaxis.set_ticks_position('both')
#subplt[1,1].xaxis.set_minor_locator(AutoMinorLocator(5))
#subplt[1,1].yaxis.set_minor_locator(AutoMinorLocator(5))
#subplt[1,1].grid(True)
#fig.legend([ls, lexp],     # The line objects
#	labels=line_labels,   # The labels for each line
#	loc="upper right"   # Position of legend
#	#borderaxespad=0.1,    # Small spacing around legend box
#	#title="Legend Title"  # Title for the legend
#)
#fig.suptitle('Kinetic Analysis')

