program advectionMain
  use variables
  use advection
  use AdvectionProblem
  use TimeScheme
  
  implicit none
  
  real(pr) :: tfinal, cfl, t, err1, err2, err, ordre, z, xt, yt, zt, Tps
  integer :: nbIter, i, j, k, nt, advectionSchemeChoice, interpChoice, timeSchemeChoice, functionChoice, BCChoice
  real(pr), dimension(:,:), allocatable :: Uexacte, Udiff, rho, rhoNext, u, v, ue, ve, rhou, rhov, rhoNext1, rhoNext2, un, zero

  advectionSchemeChoice = 3      !! 1 -Upwind    -- 2 -LW       -- 3 -Remeshing Particle Method                 -- 5 -WENO5
  interpChoice = 5               !! 1 -sigma1                   -- 3 -sigma3                    -- 4 -M4        -- 5 -M'4
  functionChoice = 2             !! 1 -creneau   -- 2 -gaussian 
  timeSchemeChoice = 0           !! 1 -EulerExp                 -- 3 -RK3                       -- 4 -RK4                       (0 -Otherwise)
  BCChoice = 1                   !! 1 -Periodic  -- 2 -dirichletH

          write(*,*)" ***************************************************** "
  if (functionChoice == 1) then
          write(*,*)" * InitialFunction     Creneau                       * "
  else if (functionChoice == 2) then
          write(*,*)" * InitialFunction     Gaussian                      * "
  else
          write(*,*)"/!\ NO INITIAL FUNCTION                             /!\"
  endif
  if (BCChoice == 1) then
          write(*,*)" * BoundaryConditions  Periodic                      * "
  else if (BCChoice == 2) then
          write(*,*)" * BoundaryCondition   DirichletH                    * "
  else
          BCChoice = 1
          write(*,*)"/!\ NO BOUNDARY CONDITIONS : Periodic by default    /!\"
  endif
  if ((timeSchemeChoice == 0).and.(advectionSchemeChoice == 2)) then
          write(*,*)" * AdvectionScheme     Lax-WendroffAdvection         * "
  else if ((timeSchemeChoice == 0).and.(advectionSchemeChoice ==  3).and.(interpChoice == 1)) then
          write(*,*)" * AdvectionScheme     Reshing Particle Method       * "
          write(*,*)" * InterpKernel        sigma1                        * "
  else if ((timeSchemeChoice == 0).and.(advectionSchemeChoice ==  3).and.(interpChoice == 3)) then
          write(*,*)" * AdvectionScheme     Reshing Particle Method       * "
          write(*,*)" * InterpKernel        sigma3                        * "
  else if ((timeSchemeChoice == 0).and.(advectionSchemeChoice ==  3).and.(interpChoice == 4)) then
          write(*,*)" * AdvectionScheme     Reshing Particle Method       * "
          write(*,*)" * InterpKernel        M4                            * "
  else if ((timeSchemeChoice == 0).and.(advectionSchemeChoice ==  3).and.(interpChoice == 5)) then
          write(*,*)" * AdvectionScheme     Reshing Particle Method       * "
          write(*,*)" * InterpKernel        M'4                           * "
  else if ((timeSchemeChoice == 0).and.(advectionSchemeChoice == 5)) then
          write(*,*)" * AdvectionScheme     WENO-LF-5                     * "
  else if ((timeSchemeChoice == 1).and.(advectionSchemeChoice) == 1) then
          write(*,*)" * TimeScheme          ExplicitEulerIterator         * "
          write(*,*)" * AdvectionScheme     Upwind                        * "
  else if ((timeSchemeChoice == 1).and.(advectionSchemeChoice) == 5) then
          write(*,*)" * TimeScheme          ExplicitEulerIterator         * "
          write(*,*)" * AdvectionScheme     WENO5                         * "
  else if ((timeSchemeChoice == 3).and.(advectionSchemeChoice == 1)) then
          write(*,*)" * TimeScheme          RK3Iterator                   * "
          write(*,*)" * AdvectionScheme     Upwind                        * "
  else if ((timeSchemeChoice == 3).and.(advectionSchemeChoice == 5)) then
          write(*,*)" * TimeScheme          RK3Iterator                   * "
          write(*,*)" * AdvectionScheme     WENO5                         * "
  else if ((timeSchemeChoice == 4).and.(advectionSchemeChoice == 1)) then
          write(*,*)" * TimeScheme          RK4Iterator                   * "
          write(*,*)" * AdvectionScheme     Upwind                        * "
  else if ((timeSchemeChoice == 4).and.(advectionSchemeChoice == 5)) then
          write(*,*)" * TimeScheme          RK4Iterator                   * "
          write(*,*)" * AdvectionScheme     WENO5                         * "
  else
          write(*,*)"/!\ ALERT AdvectionSchemeChoice OR TimeSchemeChoice /!\"
  endif
          write(*,*)" ***************************************************** "
          write(*,*)" "

  !! initialisation des constantes du problème 
  LL = 200.0 !10
  a = 1 !-0.1
  bbb = 0._pr!-0.1
  tfinal = 50
  cfl = 0.9
  eepsilon = 1.e-6_pr

  N = 200
  nx = N
  ny = N
  !! calcul de dx, dt, nb_iter
  !dx = 2*LL/(N-1)
  !dy = 2*LL/(N-1)
  dx = LL/N
  dy = LL/N
  !dt = cfl*dx/abs(a) ! abs() necessaire au cas ou a<0
  !dt = cfl*0.5/(sqrt((a*a)/(dx*dx) + (bbb*bbb)/(dy*dy)))!/100
  dt = 1._pr !cfl/(abs(a)/dx + abs(bbb)/dy) ! abs() necessaire au cas ou a<0
  !dt=dt/10000
  nbIter = floor(tfinal/dt)
  write(*,*)dx,dy,dt,nbIter

  !! remise a niveau du dt pour tomber pile sur tfinal
  dt = tfinal/nbIter


  !! Uexacte sol exacte, U0 sol initiale
  !! Udiff difference de la sol approchee et de la sol exacte
  allocate(Uexacte(N,N),Udiff(N,N))
  !allocate(rhoX(N),rhoY(N),rhoNextX(N),rhoNextY(N))
  allocate(rhoNext1(N,N),rhoNext2(N,N),un(N,N),zero(N,N))
  allocate(xx(N),yy(N))
  allocate(u(N,N),v(N,N),ue(N,N),ve(N,N)) 
  allocate(rho(N,N),rhoNext(N,N))

  !allocate(marqueur(1:nx,1:ny)) 
  !allocate(phi2(1:nx,1:ny)) 
  !allocate(phi2x(1:nx,1:ny),phi2y(1:nx,1:ny) ) 
  allocate(xp(1:nx,1:ny),yp(1:nx,1:ny)) 
  allocate(rho0(1:nx,1:ny))!,rho1(1:nx,1:ny)) 
  !allocate(xinter(1:nx,1:ny),yinter(1:nx,1:ny)) 
  !allocate(Tx(1:2,1:nx,1:ny),Ty(1:2,1:nx,1:ny)) 
  !allocate(xix2(1:2,1:nx,1:ny),xiy2(1:2,1:nx,1:ny))
  !allocate(ipar(1:13),fpar(1:16)) 
  allocate(rhou(1:nx,1:ny),rhov(1:nx,1:ny))  
  allocate(rhouu(1:nx,1:ny),rhovv(1:nx,1:ny)) 
  allocate(rhouup(1:nx,1:ny),rhovvp(1:nx,1:ny)) 
  allocate(rhop(1:nx,1:ny),rhoup(1:nx,1:ny),rhovp(1:nx,1:ny)) 
  !allocate(gradrhox(1:nx,1:ny))
  !allocate(gradrhoy(1:nx,1:ny))
  !allocate(rmr1(1:nx,1:ny))
  !allocate(rho0i(1:nx,1:ny),rho1i(1:nx,1:ny))
  !allocate(um(1:ntot+nbsigne))
  !allocate(b(1:ntot+nbsigne))
  !allocate(mat1(1:taillemat))
  !allocate(mat2(1:taillemat))
  !allocate(mat3(1:taillemat))
  !allocate(r0mr1(1:nx,1:ny))  

  un = 1._pr
  zero = 0._pr


  do i=1,N
    xx(i) = -LL*0.5 + (i-1)*dx
    !xx(i) = i*dx
    yy(i) = -LL*0.5 + (i-1)*dy
    !yy(i) = i*dy
  enddo

  !! initialisation de la solution initiale
  do i=1,N
    do j=1,N
      call initialFunction(xx(i),xx(j),z,functionChoice,BCChoice)
      !call gaussian(xx(i),xx(j),z)
      !call creneau(xx(i),xx(j),z)
      if (xx(i)<-50.0) z = 0._pr
      rho0(i,j) = z !exp(-(xx(i)+5)*(xx(i)+5))  
    enddo
  enddo

  u = 0._pr !a
  do i=1,N
    do j=1,N
      if ((xx(i)<-40).and.(xx(i)>-60).and.(xx(j)<-40).and.(xx(j)>-60)) u(i,j) = a
    enddo
  enddo
  u = a
  v = bbb
  do i=1,N
    do j=1,N
      !u(i,j) = -0.001*xx(j)!/(sqrt(xx(j)*xx(j)+xx(i)*xx(i)))
      !v(i,j) = 0.001*xx(i)!/(sqrt(xx(j)*xx(j)+xx(i)*xx(i)))
      if (rho0(i,j).lt.1.e-1) then
    !          u(i,j) = 0._pr
    !          v(i,j) = 0._pr
              rho0(i,j) = 0._pr
      endif
    enddo
  enddo
  ue=u
  ve=v
  write(*,*)" u v ",maxval(abs(u))," ",maxval(abs(v))
  dt = 1._pr !cfl/(maxval(abs(u))/dx + maxval(abs(v))/dy) ! abs() necessaire au cas ou a<0
  write(*,*)" dt ",dt
  nbIter = floor(tfinal/dt)
  write(*,*)"nbIter ",floor(tfinal/dt)
  write(*,*)"N ",N
  dt = tfinal/nbIter

  !! initialisation de la solution approchee
  call SetInitialCondition(rho0, u, v, rho, rhou, rhov)

  open(unit=78,file='Uinitial.dat',status='unknown')
  do i=1,N
    do j=1,N
        write(78,'(F12.6,F12.6,F12.6)') xx(i), xx(j), rho0(i,j)
    enddo
    write(78,*) " "
  enddo
  close(78)

  !! premiere boucle en temps (N points discretises)
  do nt = 1,nbIter
      t = (nt-1)*dt
      !u = a
      u = 0._pr !a
      do i=1,N
        do j=1,N
          xt = -50.0 + a*(t+dt)!u(i,j)*(t+dt)
          yt = -50.0 + v(i,j)*(t+dt)
          !if ((xx(i)<xt+10.0).and.(xx(i)>xt-10.0).and.(xx(j)<yt+10.0).and.(xx(j)>yt-10.0)) u(i,j) = a
          if ((xx(i)<xt+20.0).and.(xx(i)>xt-20.0)) u(i,j) = a
          !if ((xx(i)<-35.0+a*(t+dt)).and.(xx(i)>-65+a*(t+dt))) u(i,j)=a
        enddo
      enddo
      !u = a
      v = bbb
      !do i=1,N
      !  do j=1,N
      !    if (rho(i,j).lt.1.e-6) then
      !            u(i,j) = 0._pr
      !            v(i,j) = 0._pr
      !            rho(i,j) = 0._pr
      !    endif
      !  enddo
      !enddo
      !write(*,*) "mnormmax ",maxval(rho(:,:))," ",nt
      !! calcul de la solution exacte au temps t
      Uexacte = 0._pr
      do i=1,N
        do j=1,N
          xt = xx(i) - u(i,j)*(t+dt)
          yt = xx(j) - v(i,j)*(t+dt)
          if ((u(i,j)>0).or.(v(i,j)>0)) then
            call initialFunction(xt,yt,zt,functionChoice,BCChoice)
            Uexacte(i,j) = zt
          endif
          !call gaussian(xt,yt,zt)
          !call creneau(xt,yt,zt)

          if (Uexacte(i,j).lt.1.e-6) then
                  Uexacte(i,j) = 0._pr
          endif
          !if (Uexacte(i,j).lt.1.e-3) then
          !        ue(i,j)=0
          !        ve(i,j)=0
          !endif
        enddo
      enddo

      !! calcul de la sol approchee au temps t
      call AdvanceTime(nt, t, timeSchemeChoice, advectionSchemeChoice, interpChoice, BCChoice, rho, u, v, rhou, rhov)
      !call AdvanceEulerExp(nt, t, advectionSchemeChoice)
      !call AdvanceRK3(nt, t, advectionSchemeChoice)
      !call AdvanceRK4(nt, t, advectionSchemeChoice)
      !call AdvanceLW(nt, t)
      !call AdvanceParticle(nt, t, interpChoice)

  enddo
  !! ecriture des deux sol (approchee et exacte) pour visualisation gnuplot
  !if (N1 == Nmax) then
      open(unit=78,file='U.dat',status='unknown')
      do i=1,N
        do j=1,N
            write(78,'(F12.6,F12.6,F12.6)') xx(i), xx(j), rho(i,j)
        enddo
        write(78,*) " "
      enddo
      close(78)
      open(unit=78,file='Uexacte.dat',status='unknown')
      do i=1,N
        do j=1,N
            write(78,'(F12.6,F12.6,F12.6)') xx(i), xx(j), Uexacte(i,j)
        enddo
        write(78,*) " "
      enddo
      close(78)
   !endif
     
  !! premier calcul de l erreur au temps final (N points discretises) en norme 2
  do i=1,N
    do j=1,N
      Udiff(i,j) = Uexacte(i,j) - rho(i,j)
    enddo
  enddo

  call Norm22D(Udiff, err1)
  call Norm22D(Uexacte, err)
  err1 = err1/err
  !write(*,*) "Erreur = ",err1," ",err," ",log(err1)/log(N*1._pr)
  write(*,*) "Erreur au temps final = ",err1
  !if (N1 == Nmax) write(*,*) "Erreur au temps final = ",err1
  
  !! boucle pour calculer U avec deux fois plus de points pour calculer l'ordre
  !! mise a jour de N et des parametres dependants de N
  N = N*2
  !dx = 2.0*LL/(N-1)
  !dy = 2.0*LL/(N-1)
  dx = LL/N
  dy = LL/N
  !dt = cfl*dx/abs(a) 
  !dt = cfl*0.5/(sqrt((a*a)/(dx*dx) + (bbb*bbb)/(dy*dy)))!/100
  !dt = cfl/(abs(a)/dx + abs(bbb)/dy) ! abs() necessaire au cas ou a<0
  !dt = dt/2
  !dt = cfl/(abs(a)/dx + abs(bbb)/dy)*2

  !! reallocation des vecteurs U0, Udiff, Uexacte et U
  deallocate(Udiff,Uexacte,rho, rhoNext, u, v, xx, yy, rho0)
  deallocate(xp,yp,rhou,rhov,rhouu,rhovv,rhouup,rhovvp,rhop,rhoup,rhovp)!,rho0i,rho1i)
  allocate(Udiff(N,N), Uexacte(N,N), rho(N,N), rhoNext(N,N))
  allocate(xx(N),yy(N))
  allocate(u(N,N),v(N,N)) 
  nx = N
  ny = N
  allocate(xp(1:nx,1:ny),yp(1:nx,1:ny)) 
  allocate(rho0(1:nx,1:ny))!,rho1(1:nx,1:ny)) 
  allocate(rhou(1:nx,1:ny),rhov(1:nx,1:ny))  
  allocate(rhouu(1:nx,1:ny),rhovv(1:nx,1:ny)) 
  allocate(rhouup(1:nx,1:ny),rhovvp(1:nx,1:ny)) 
  allocate(rhop(1:nx,1:ny),rhoup(1:nx,1:ny),rhovp(1:nx,1:ny)) 
  !allocate(rho0i(1:nx,1:ny),rho1i(1:nx,1:ny))

  do i=1,N
    xx(i) = -LL*0.5 + (i-1)*dx
    !xx(i) = (i-1)*dx
    yy(i) = -LL*0.5 + (i-1)*dy
    !yy(i) = (i-1)*dy
  enddo
  !! reinitialisation de la solution exacte
  do i=1,N
    do j=1,N
      call initialFunction(xx(i),xx(j),z,functionChoice,BCChoice)
      !call gaussian(xx(i),xx(j),z)
      !call creneau(xx(i),xx(j),z)
      rho0(i,j) = z 
    enddo
  enddo

  u = a
  v = bbb
  do i=1,N
    do j=1,N
      !u(i,j) = -0.1*xx(j)/(sqrt(xx(j)*xx(j)+xx(i)*xx(i)))
      !v(i,j) = 0.1*xx(i)/(sqrt(xx(j)*xx(j)+xx(i)*xx(i)))
      !if (rho0(i,j).lt.1.e-1) then
      !        u(i,j) = 0._pr
      !        v(i,j) = 0._pr
      !        rho0(i,j) = 0._pr
      !endif
    enddo
  enddo
  write(*,*)" u v ",maxval(abs(u))," ",maxval(abs(v))
  dt = 1._pr !cfl/(maxval(abs(u))/dx + maxval(abs(v))/dy) ! abs() necessaire au cas ou a<0
  nbIter = floor(tfinal/dt)
  dt = tfinal/nbIter
  write(*,*)" dt ",dt
  write(*,*)"nbIter ",floor(tfinal/dt)
  write(*,*)"N ",N

  !! reinitialisation de la solution approchee
  call SetInitialCondition(rho0, u, v, rho, rhou, rhov)

  !! deuxieme boucle en temps (2*N points discretises)
  do nt = 1,nbIter
      t = (nt-1)*dt
        
      !! calcul de la solution exacte au temps t
      Uexacte = 0._pr
      do i=1,N
        do j=1,N
          !call TranslateX(xx(i) - u(i,j)*(t+dt),xt)
          !call TranslateX(xx(j) - v(i,j)*(t+dt),yt)
          xt = xx(i) - u(i,j)*(t+dt)
          yt = xx(j) - v(i,j)*(t+dt)
          !if ((xt<xx(1)).or.(xt>xx(N))) xt = -1
          !if ((yt<xx(1)).or.(yt>xx(N))) yt = -1
          if ((u(i,j)>0).or.(v(i,j)>0)) then
            call initialFunction(xt,yt,zt,functionChoice,BCChoice)
            Uexacte(i,j) = zt
          endif
          !call gaussian(xt,yt,zt)
          !call creneau(xt,yt,zt)

          if (Uexacte(i,j).lt.1.e-6) then
                  Uexacte(i,j) = 0._pr
          endif
        enddo
      enddo
      
      !! calcul de la solution approchee au temps t
      call AdvanceTime(nt, t, timeSchemeChoice, advectionSchemeChoice, interpChoice, BCChoice, rho, u, v, rhou, rhov)
      !call AdvanceEulerExp(nt, t, advectionSchemeChoice)
      !call AdvanceRK3(nt, t, advectionSchemeChoice)
      !call AdvanceRK4(nt, t, advectionSchemeChoice)
      !call AdvanceLW(nt, t)
      !call AdvanceParticle(nt, t, interpChoice)
  enddo
 
  !! deuxieme calcul de lerreur au temps final (2*N points discretises) en norme 2
  do i=1,N
    do j=1,N
      Udiff(i,j) = Uexacte(i,j) - rho(i,j)
    enddo
  enddo

  call Norm22D(Udiff,err2)
  call Norm22D(Uexacte,err)
  err2 = err2 / err
  !write(*,*) "Erreur = ",log(err2)/log(N*1._pr)
  !if (N1 == Nmax) write(*,*) "Erreur au temps final  = ", err2
  write(*,*) "Erreur au temps final  = ", err2

  !! ecriture des deux sol (approchee et exacte) pour visualisation gnuplot
  !if (N1 == Nmax) then
      open(unit=78,file='U2.dat',status='unknown')
      do i=1,N
        do j=1,N
            write(78,'(F12.6,F12.6,F12.6)') xx(i), xx(j), rho(i,j)
        enddo
        write(78,*) " "
      enddo
      close(78)

  !! calcul de l ordre pour une discretisation en espace N
  !ordre = (log(err1) - log(err2)) / log(2._pr)
  ordre = abs(log(err1) - log(err2)) / log(2._pr)
  !if (N1 == Nmax) write(*,*) "Ordre du schema en espace : ", ordre
  write(*,*) "Ordre du schema en espace : ", ordre

  deallocate(Uexacte, Udiff)
  deallocate(xx, yy)
  deallocate(u, v, ue, ve) 
  !deallocate(rhoX,rhoY,rhoNextX,rhoNextY)
  deallocate(rho, rhoNext)

  !deallocate(marqueur)
  !deallocate(mat1,mat2,mat3)
  !deallocate(phi2,phi2x,phi2y) 
  !deallocate(xinter,yinter)  
  !deallocate(xix2,xiy2) 
  deallocate(rho0)!,rho1) 
  !deallocate(Tx,Ty)
  !deallocate(um)
  !deallocate(b) 
  !deallocate(ipar,fpar) 
  deallocate(rhou,rhov) 
  deallocate(rhoup,rhovp,rhop)
  !deallocate(gradrhox,gradrhoy)
  !deallocate(rmr1)
  deallocate(rhouu,rhovv,rhouup,rhovvp)
  deallocate(rhoNext1,rhoNext2,un,zero)
  !deallocate(rho0i,rho1i)

  write(*,*) "********************"
end

