module util
  
  use variables
  use doubler

  implicit none
  
contains  
  
  integer function ind (i, j) 
    
    integer :: i,j   
    
    ind = (i-1)*ny+j +nbsigne 
    
    return 
  end function ind
  
 subroutine formule(alph, bet, gamm, aa,bb,cc)
   
   implicit none
 
   real(pr) :: alph, bet, gamm, aa,bb,cc
   
   cc= 1._pr/((gamm-alph)*(1._pr - (gamm-alph)/(bet-alph)))
   bb = -cc*(gamm-alph)**2/(bet-alph)**2
   aa= -bb-cc      
  
 end subroutine formule

end module util


