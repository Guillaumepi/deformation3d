program mainBezier
  use doubler
  use libBezier
  
  implicit none
  
  integer :: i,j,k,l!,ll
  integer :: N,bool
  real(pr) :: dt,t,px,py,long,ds,s,sinit,tm,tp,s0,eepsilon
  real(pr),dimension(:,:),allocatable :: points_control, points_courbe, points_courbe_equal

  N = 54
  dt = 1._pr/(N-1)
  eepsilon = 1.e-6_pr

  !allocate(points_control(4,2))
  allocate(points_control(7,2))
  allocate(points_courbe(N,2))
  allocate(points_courbe_equal(N,2))

  !call initialize4(points_control)
  call initialize7(points_control)
  open(unit=78,file='bezierpoints.dat',status='unknown')
  do l=1,size(points_control,1)
     write(78,'(F12.6,F12.6)') points_control(l,1),points_control(l,2)
  enddo
  close(78)

  write(*,*) "initialisation : ",points_courbe(1,1)," ",points_courbe(1,2)," ",points_courbe(N,1)," ",points_courbe(N,2)
  points_courbe(1,1) = points_control(1,1)
  points_courbe(1,2) = points_control(1,2)
  points_courbe(N,1) = points_control(size(points_control,1),1)
  points_courbe(N,2) = points_control(size(points_control,1),2)
  write(*,*) "initialisation : ",points_courbe(1,1)," ",points_courbe(1,2)," ",points_courbe(N,1)," ",points_courbe(N,2)

  open(unit=79,file='bezierdist.dat',status='unknown')
  write(79,'(F12.6,F12.6)') 0._pr,0._pr

  t = dt
  k = 1
  long = 0._pr
  do while ((t<1._pr).and.(k+1<N))
    k = k+1
    !call pointBezier3(points_control,t,px,py)
    !call initialize4(points_control)
    !call initialize7(points_control)

    !!call pointsBezierN(points_control,t,px,py)
    call splineN(points_control,t,px,py)
    
    points_courbe(k,1) = px
    points_courbe(k,2) = py
    write(*,*) "points t ==    ",sqrt((points_courbe(k,1)-points_courbe(k-1,1))*(points_courbe(k,1)-points_courbe(k-1,1)) +&
(points_courbe(k,2)-points_courbe(k-1,2))*(points_courbe(k,2)-points_courbe(k-1,2)))," ",t," ",px," ",py," ",k

    long = long + sqrt((points_courbe(k,1)-points_courbe(k-1,1))*(points_courbe(k,1)-points_courbe(k-1,1)) +&
(points_courbe(k,2)-points_courbe(k-1,2))*(points_courbe(k,2)-points_courbe(k-1,2))) 
    write(79,'(F12.6,F12.6)') t,long
    !write(*,*) "t ==  ",t
    !write(*,*) "t = ",t
    t = t+dt
  enddo
    long = long + sqrt((points_courbe(N,1)-points_courbe(N-1,1))*(points_courbe(N,1)-points_courbe(N-1,1)) +&
(points_courbe(N,2)-points_courbe(N-1,2))*(points_courbe(N,2)-points_courbe(N-1,2))) 
    write(79,'(F12.6,F12.6)') t,long
  close(79)
  open(unit=78,file='beziercourbe.dat',status='unknown')
  !write(78,'(F12.6,F12.6)') px,py
  do l=1,N
     write(78,'(F12.6,F12.6)') points_courbe(l,1),points_courbe(l,2)
     !write(*,*) "long = ",long
  enddo
  close(78)
  ds = long/(N-1)
  write(*,*) "ds == ",ds," ",long," ",N-1

  !call initialize4(points_control)
  write(*,*) "initialisation : ",points_courbe(1,1)," ",points_courbe(1,2)," ",points_courbe(N,1)," ",points_courbe(N,2)
  points_courbe_equal(1,1) = points_control(1,1)
  points_courbe_equal(1,2) = points_control(1,2)
  points_courbe_equal(N,1) = points_control(size(points_control,1),1)
  points_courbe_equal(N,2) = points_control(size(points_control,1),2)
  write(*,*) "initialisation : ",points_courbe_equal(1,1)," ",points_courbe_equal(1,2)," ",points_courbe_equal(N,1)&
," ",points_courbe_equal(N,2)
  
  open(unit=79,file='bezierdistequal.dat',status='unknown')
  write(79,'(F12.6,F12.6)') 0._pr,0._pr
  k = 1
  long = 0._pr
  t = 0._pr
  !do while ((t<1._pr).and.(k<N))
  do while ((t<1._pr).and.(k+1<N))
    k = k+1
!    l = 1
!    s = 0._pr
!    do while ((k-1)*ds-s>0._pr) 
!      l = l+1
!      !s = s + sqrt((points_courbe(l+1,1)-points_courbe(l,1))*(points_courbe(l+1,1)-points_courbe(l,1)) + (points_courbe(l+1,2)-points_courbe(l,2))*(points_courbe(l+1,2)-points_courbe(l,2)))
!      !write(*,*) "sqrt ==      ",sqrt((points_courbe(l,1)-points_courbe(l-1,1))*(points_courbe(l,1)-points_courbe(l-1,1))+(points_courbe(l,2)-points_courbe(l-1,2))*(points_courbe(l,2)-points_courbe(l-1,2)))
!
!      s = s + sqrt((points_courbe(l,1)-points_courbe(l-1,1))*(points_courbe(l,1)-points_courbe(l-1,1)) + (points_courbe(l,2)-points_courbe(l-1,2))*(points_courbe(l,2)-points_courbe(l-1,2)))
!    enddo
!    t = (l-2)*dt !((l-2)*dt + (l-1)*dt)*0.5_pr
!    call pointsBezierN(points_control,t,px,py)
!    !write(*,*) "my sqrt ==    ",sqrt((points_courbe(l,1)-points_courbe(l-1,1))*(points_courbe(l,1)-points_courbe(l-1,1)) + (points_courbe(l,2)-points_courbe(l-1,2))*(points_courbe(l,2)-points_courbe(l-1,2)))
!    !write(*,*) "tt :: ",t," ",s," ",s-sqrt((points_courbe(l+1,1)-points_courbe(l,1))*(points_courbe(l+1,1)-points_courbe(l,1)) + (points_courbe(l+1,2)-points_courbe(l,2))*(points_courbe(l+1,2)-points_courbe(l,2)))," ",s-sqrt((points_courbe(l+1,1)-points_courbe(l,1))*(points_courbe(l+1,1)-points_courbe(l,1)) + (points_courbe(l+1,2)-points_courbe(l,2))*(points_courbe(l+1,2)-points_courbe(l,2)))+sqrt((px-points_courbe(l,1))*(px-points_courbe(l,1)) + (py-points_courbe(l,2))*(py-points_courbe(l,2)))," k*ds  ",(k-1)*ds
!    write(*,*) "tt :: ",t," ",s," ",s-sqrt((points_courbe(l,1)-points_courbe(l-1,1))*(points_courbe(l,1)-points_courbe(l-1,1)) + (points_courbe(l,2)-points_courbe(l-1,2))*(points_courbe(l,2)-points_courbe(l-1,2)))," ",s-sqrt((points_courbe(l,1)-points_courbe(l-1,1))*(points_courbe(l,1)-points_courbe(l-1,1)) + (points_courbe(l,2)-points_courbe(l-1,2))*(points_courbe(l,2)-points_courbe(l-1,2)))+sqrt((px-points_courbe(l-1,1))*(px-points_courbe(l-1,1)) + (py-points_courbe(l-1,2))*(py-points_courbe(l-1,2)))," k*ds  ",(k-1)*ds

    l = 1
    s = 0._pr
    do while ((k-1)*ds-s>0._pr) 
      l = l+1
      s = s + sqrt((points_courbe(l,1)-points_courbe(l-1,1))*(points_courbe(l,1)-points_courbe(l-1,1)) +&
(points_courbe(l,2)-points_courbe(l-1,2))*(points_courbe(l,2)-points_courbe(l-1,2)))
    enddo
    tm = (l-2)*dt
    tp = (l-1)*dt
    s0 = s
    sinit = s - sqrt((points_courbe(l,1)-points_courbe(l-1,1))*(points_courbe(l,1)-points_courbe(l-1,1)) +&
(points_courbe(l,2)-points_courbe(l-1,2))*(points_courbe(l,2)-points_courbe(l-1,2)))
    s = sinit
    do while (abs((k-1)*ds-s)>eepsilon)
      t = (tm + tp)*0.5_pr
      !call pointsBezierN(points_control,t,px,py)
      call splineN(points_control,t,px,py)
      s = sinit + sqrt((px-points_courbe(l-1,1))*(px-points_courbe(l-1,1))+(py-points_courbe(l-1,2))*(py-points_courbe(l-1,2)))
      if ((k-1)*ds-s>0._pr) then
        tm = t
      else
        tp = t
      endif
    enddo
    !write(*,*) "tt :: ",t," ",s," ",s-sqrt((points_courbe(l+1,1)-points_courbe(l,1))*(points_courbe(l+1,1)-points_courbe(l,1)) + (points_courbe(l+1,2)-points_courbe(l,2))*(points_courbe(l+1,2)-points_courbe(l,2)))," ",s-sqrt((points_courbe(l+1,1)-points_courbe(l,1))*(points_courbe(l+1,1)-points_courbe(l,1)) + (points_courbe(l+1,2)-points_courbe(l,2))*(points_courbe(l+1,2)-points_courbe(l,2)))+sqrt((px-points_courbe(l,1))*(px-points_courbe(l,1)) + (py-points_courbe(l,2))*(py-points_courbe(l,2)))," k*ds  ",(k-1)*ds
    !write(*,*) "tt :: ",t," ",s," ",s-sqrt((points_courbe(l,1)-points_courbe(l-1,1))*(points_courbe(l,1)-points_courbe(l-1,1)) + (points_courbe(l,2)-points_courbe(l-1,2))*(points_courbe(l,2)-points_courbe(l-1,2)))," ",s-sqrt((points_courbe(l,1)-points_courbe(l-1,1))*(points_courbe(l,1)-points_courbe(l-1,1)) + (points_courbe(l,2)-points_courbe(l-1,2))*(points_courbe(l,2)-points_courbe(l-1,2)))+sqrt((px-points_courbe(l-1,1))*(px-points_courbe(l-1,1)) + (py-points_courbe(l-1,2))*(py-points_courbe(l-1,2)))," k*ds  ",(k-1)*ds
    !write(*,*) "tt :: ",t," ",s0," ",sinit," ",s," k*ds  ",(k-1)*ds

    !call pointsBezierN(points_control,t,px,py)
    call splineN(points_control,t,px,py)
    !s = sinit + sqrt((px-points_courbe(l-1,1))*(px-points_courbe(l-1,1))+(py-points_courbe(l-1,2))*(py-points_courbe(l-1,2)))

    write(*,*) "KK = ",k," ",t!,"      ",(k-1)*ds-s
    points_courbe_equal(k,1) = px
    points_courbe_equal(k,2) = py
    write(*,*) "points S ==    ",sqrt((points_courbe_equal(k,1)-points_courbe_equal(k-1,1))*(points_courbe_equal(k,1)-&
points_courbe_equal(k-1,1)) + (points_courbe_equal(k,2)-points_courbe_equal(k-1,2))*(points_courbe_equal(k,2)-&
points_courbe_equal(k-1,2))),"    ",t," ",k
    
    long = long + sqrt((points_courbe_equal(k,1)-points_courbe_equal(k-1,1))*(points_courbe_equal(k,1)-points_courbe_equal(k-1,1))+&
 (points_courbe_equal(k,2)-points_courbe_equal(k-1,2))*(points_courbe_equal(k,2)-points_courbe_equal(k-1,2)))
    write(79,'(F12.6,F12.6)') t,long
  enddo
  long = long + sqrt((points_courbe(N,1)-points_courbe(N-1,1))*(points_courbe(N,1)-points_courbe(N-1,1)) +&
(points_courbe(N,2)-points_courbe(N-1,2))*(points_courbe(N,2)-points_courbe(N-1,2))) 
  write(79,'(F12.6,F12.6)') t,long
  close(79)
    
  write(*,*) "initialisation : ",points_courbe_equal(1,1)," ",points_courbe_equal(1,2)," ",points_courbe_equal(N,1)&
," ",points_courbe_equal(N,2)
  open(unit=78,file='beziercourbeequal.dat',status='unknown')
  do l=1,N
     write(78,'(F12.6,F12.6)') points_courbe_equal(l,1),points_courbe_equal(l,2)
  enddo
  close(78)

  deallocate(points_control,points_courbe,points_courbe_equal)
  write(*,*) "********************"
end

