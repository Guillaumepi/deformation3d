deformationmidline2D --DOC

Key word to switch the test case: FILM
Key-words in log: ERROR/WARNING/etc

picNum: first image
long00: total length of the initial pre-built shape
longexp: zebrafish length observed experimentally
longratio: ratio between observed and initial length

IMPORTANT: most of variables about the midline are divided in 3 (tail (initial) + mid + head (final))

variables:
mesh size meshratio/area/sigma/sigma2 (uniform meshes: meshRatio=area)
number of control points: nx (length) / ny/nz
spatial steps: dx (length) / dy/dz
number of Lagrangian markers: (Ni+Ns+Nf-2) transverse slices x 2 (Ntheta)

arrays:
cosTheta_tab/sinTheta_tab: angular coordinates of each Lagrangian marker
slicemid: x,y-coordinates of each Lagrangian marker (Ns x2)
longslice/longslicei/longslicef: length of each Ntheta (2) (mid/initial/final)
sslice/dsslice/stheta/dstheta: intermediate curvilinear variables
thetatab/indextheta/indextab: angles and indices of each Ntheta
xTheta/yTheta: intermdiate coordinates when searching angle Theta
valTheta: angle from each Lagrnagian marker to the associated midline marker
valDist: distance from each Lagrnagian marker to the associated midline marker
longTheta: curvilinear length of each transverse slice
head_courbe/tail_courbe: x,y-coordinates of each Lagrnagian markers (Nix2 - Nfx2)
points_control: control points of the midline
points_courbe: x,y-coordinates after points_control and before points_courbe_equal
points_courbe_equal: x,y-coordinates after points_courbe
points_courbe_equal_ref: splince of reference for computing the bend amplitude
rhoSlices: 2D-map of the zebrafish silhouette
gradPhi: 2D-map of the gradient of the zebrafish level-set 
rhoSlices3D/rhoSlices3D2: 3D-map of the zebrafish silhouette
xx/yy: coordinates
skel/skel2/skel3/skeltmp: 2D-map of the zebrafish skeleton
--> skel = le squelette, çàd la midline brute
--> skel2 = seuil sur le gradient du level set qui permet de récupérer le point central de la tête à partir duquel couper la midline
		--> correction manuelle pour les images dans laquelle un seuil de 0.94 permet pas de chopper quoi que ce soit!

distslice: distance from each Lagrnagian marker to the associated midline marker (left and right values for each transverse slcice)
slice_courbe: combination of points_courbe and head and tail
slice_courbe_equal: after slice_courbe
slicecontrolm/slicecontroli/slicecontrolf/slicecontrol: control arrays for the contour/surface of Lagrangian markers

midlinebis: over-estimation of the midline (x,y-coordinates x size of skeleton (overestimated))
nl: size of the midline
midline: horizontal midline 

rtail/rhead: real abscissa (tail/head)
itail/ihead: corresponding integer
disttail: length between tail tip and midline
disthead: length between head snout and midline

kt: number of the experimental image
Nseed: sum of all neighbours of one pixel
ic,jc: center of the head (endpoint of the midline) at previous time step
