
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
		                               AutoMinorLocator)

plt.rc('font', family='sans-serif')
width=7 #double column
#width=4.5 #middle column
#width=3.42 #single column
height=width/1.618
params = {
        'mathtext.rm' : 'Arial',
        'axes.labelsize': 7,
        'font.size': 7,
        'legend.fontsize': 7,
        'xtick.labelsize': 7,
        'ytick.labelsize': 7,
        'text.usetex': False,
        'figure.figsize': [width, height]
}
plt.rcParams.update(params)
fig = plt.figure(figsize=(width,height),constrained_layout=False)

tfinal=58

CoMdiscrete = np.loadtxt('MassCenter.dat', unpack=True)
xg = [-0.0256*(CoMdiscrete[0,i]-CoMdiscrete[0,0]) for i in range(0,CoMdiscrete.shape[1])]
yg = [0.0256*(CoMdiscrete[1,i]-CoMdiscrete[1,0]) for i in range(0,CoMdiscrete.shape[1])]

CoMsmooth = np.loadtxt('deplacements_exp.dat', unpack=True)
xg_smooth = [1000*(CoMsmooth[1,i]-CoMsmooth[1,0]) for i in range(0,CoMsmooth.shape[1])]
yg_smooth = [1000*(CoMsmooth[2,i]-CoMsmooth[2,0]) for i in range(0,CoMsmooth.shape[1])]

plt.plot(yg,xg,c='black')
plt.plot(xg_smooth,yg_smooth,c='red')
plt.xlabel('X-axis (mm)')
plt.ylabel('Y-axis (mm)')
plt.axis('square')
axes=plt.axes()
axes.yaxis.set_ticks_position('both')
axes.xaxis.set_ticks_position('both')
#plt.tight_layout()
plt.show()
#plt.savefig("./trajectory.pdf", format='pdf', dpi=300)

fig = plt.figure(figsize=(width,height),constrained_layout=False)

Angle_discrete = np.loadtxt('../theta_exp.dat', unpack=True)
time = [1000*Angle_discrete[0,i] for i in range(0,Angle_discrete.shape[1]) if(1000*Angle_discrete[0,i]<tfinal)]
angle = [Angle_discrete[1,i] for i in range(0,Angle_discrete.shape[1]) if(1000*Angle_discrete[0,i]<tfinal)]

Angle_smooth = np.loadtxt('theta_exp.dat', unpack=True)
time_smooth = [1000*Angle_smooth[0,i] for i in range(0,Angle_smooth.shape[1]) if(1000*Angle_smooth[0,i]<tfinal)]
angle_smooth = [Angle_smooth[1,i] for i in range(0,Angle_smooth.shape[1]) if(1000*Angle_smooth[0,i]<tfinal)]

plt.plot(time,angle,c='black')
plt.plot(time_smooth,angle_smooth,c='red')
plt.xlabel('number of images')
plt.ylabel(r'$\theta$ (deg°)')
axes=plt.axes()
axes.yaxis.set_ticks_position('both')
#plt.legend(loc='upper right')
#plt.tight_layout()
#plt.show()
plt.savefig("./anglesmoothplus.pdf", format='pdf', dpi=300)

fig = plt.figure(figsize=(width,height),constrained_layout=False)

deltaAngle_original = np.loadtxt('Angle2.dat', unpack=True)
angle_original = [deltaAngle_original[i] for i in range(0,deltaAngle_original.shape[0]) if(1000*Angle_smooth[0,i]<tfinal)]

deltaAngle_seuil = np.loadtxt('Angle3.dat', unpack=True)
angle_seuil = [deltaAngle_seuil[i] for i in range(0,deltaAngle_seuil.shape[0]) if(1000*Angle_smooth[0,i]<tfinal)]

plt.plot(time_smooth,angle_original,c='black')
plt.plot(time_smooth,angle_seuil,c='red')
plt.xlabel('time (ms)')
plt.ylabel(r'$\theta_\epsilon$ (deg°)')
axes=plt.axes()
axes.yaxis.set_ticks_position('both')
#plt.legend(loc='upper right')
#plt.tight_layout()
#plt.show()
plt.savefig("./angleepssmooth.pdf", format='pdf', dpi=300)


##plt.xlabel('x (mm)')
##plt.ylabel('y (mm)')
##plt.title('skel')
#plt.axis('square')
#axes=plt.axes()
##plt.axis('off')
#axes.set_xticks([])
##plt.axis('equal')
#plt.legend(loc="upper right")
#
#axes.spines['left'].set_visible(False)
#axes.spines['top'].set_visible(False)
#axes.spines['bottom'].set_visible(False)
#axes.yaxis.set_ticks_position('right')
##axes.xaxis.set_ticks_position('bottom')
#
##plt.legend(mylabels,loc="upper right")
##plt.axis('square')
##plt.legend(#[ls, lexp],     # The line objects FIG
##	labels=['mylabel'],#line_labels,   # The labels for each line
##	loc="upper right"   # Position of legend
##	#borderaxespad=0.1,    # Small spacing around legend box
##	#title="Legend Title"  # Title for the legend
##)
##fig.tight_layout()
##plt.tight_layout()
#plt.show()
#



#subplt[1,1].yaxis.set_ticks_position('both')
#subplt[1,1].xaxis.set_minor_locator(AutoMinorLocator(5))
#subplt[1,1].yaxis.set_minor_locator(AutoMinorLocator(5))
#subplt[1,1].grid(True)
#fig.legend([ls, lexp],     # The line objects
#	labels=line_labels,   # The labels for each line
#	loc="upper right"   # Position of legend
#	#borderaxespad=0.1,    # Small spacing around legend box
#	#title="Legend Title"  # Title for the legend
#)
#fig.suptitle('Kinetic Analysis')

