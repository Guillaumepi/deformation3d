
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from scipy.interpolate import UnivariateSpline
#from numpy import csaps
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
		                               AutoMinorLocator)

tfinal = 0.058
#tfinal = 0.0641
#tfinal = 0.0621
times_event = [0, 0.01, 0.02, 0.03, 0.04, 0.05]
times_event = [0, 10, 20, 30, 40, 50] 
times_event_final = [times_event[i] for i in range(0,len(times_event)) if times_event[i] < tfinal*1000]
times_event = times_event_final

#exp = np.loadtxt('../deplacements_exp.dat', delimiter=' ', unpack=True)
exp = np.loadtxt('deplacements_exp.dat', delimiter=' ', unpack=True)

texp_dep = [1000*exp[0,i] for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]
xexp_dep = [1000*exp[1,i] for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]
yexp_dep = [1000*exp[2,i] for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]

#exp = np.loadtxt('../theta_exp.dat', delimiter=' ', unpack=True)
#exp = np.loadtxt('myFISHserie5_18/theta_exp.dat', delimiter=' ', unpack=True)
exp = np.loadtxt('../theta_exp.dat', delimiter=' ', unpack=True)
#simu = np.loadtxt('/tmp/sshfs/Results/theta.dat', delimiter=' ', unpack=True)
#expA = np.loadtxt('myFISHserie5_18/Angle.dat', delimiter=' ', unpack=True)
expA = np.loadtxt('./Angle.dat', delimiter=' ', unpack=True)
expAmod = np.loadtxt('Angle.dat', delimiter=' ', unpack=True)
#expS = np.loadtxt('myFISHserie5_18/IMAGES3/Angle_smooth.dat', delimiter=' ', unpack=True)
expSG = np.loadtxt('Angle_smooth.dat', delimiter=' ', unpack=True)
expAG = np.loadtxt('Angle.dat', delimiter=' ', unpack=True)
expA2 = np.loadtxt('Angle2.dat', delimiter=' ', unpack=True)
#expA2 = np.loadtxt('myFISHserie5_18/IMAGES3/Angle2.dat', delimiter=' ', unpack=True)

#ts_theta = [1000*simu[0,i] for i in range(0,simu.shape[1]) if simu[0,i] < tfinal]
#ths_theta = [simu[2,i] for i in range(0,simu.shape[1]) if simu[0,i] < tfinal]

texp_theta = [1000*exp[0,i] for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]
texpA2_theta = [1000*exp[0,i] for i in range(0,exp.shape[1]) if ((exp[0,i] < tfinal)and((exp[0,i]<0.02)or(exp[0,i]>0.025)))]
#texpA2_theta = [1000*exp[0,i] for i in range(0,exp.shape[1]) if ((exp[0,i] < tfinal)and((exp[0,i]<0.018)or(exp[0,i]>0.021)))]
thexp_theta = [exp[1,i] for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]
thexpA_theta = [expA[i]-expA[0] for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]
thexpAmod_theta = [expAmod[i]-expAmod[0] for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]
#thexpS_theta = [expS[i]-expS[0] for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]
#thexpSG_theta = [expSG[i]-expSG[0] for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]
#thexpAG_theta = [expAG[i]-expAG[0] for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]
thexpA2_theta = [expA2[i]-expA2[0] for i in range(0,exp.shape[1]) if ((exp[0,i] < tfinal)and((exp[0,i]<0.02)or(exp[0,i]>0.025)))]
#thexpA2_theta = [expA2[i]-expA2[0] for i in range(0,exp.shape[1]) if ((exp[0,i] < tfinal)and((exp[0,i]<0.018)or(exp[0,i]>0.021)))]
#thexpA2o_theta = [expA2[i]-expA2[0] for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]
thexpA2o_theta = [expA2[i] for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]
#thexpA2_theta = [expA2[i]-expA2[0] for i in range(0,exp.shape[1]) if (exp[0,i] < tfinal)]



spl=UnivariateSpline(texpA2_theta,thexpA2_theta)
#spl=UnivariateSpline(texp_theta,thexpA2_theta)
##spl.set_smoothing_factor(30)
##spl.set_smoothing_factor(50)
spl.set_smoothing_factor(60)
#spl.set_smoothing_factor(74)
#spl.set_smoothing_factor(77)
#thsmooth=savgol_filter(thexpA2_theta,81,6)
#thsmooth=savgol_filter(thexpA2_theta,81,6)
thexpAmod_theta=np.cumsum(spl(texp_theta)+expA2[0])
thexpAmodo_theta=np.cumsum(thexpA2o_theta)

#f= open("Angle3.dat","w")
#for i in range(0,len(texp_theta)):
#  f.write("%lf\r\n" % (spl(texp_theta[i])+expA2[0])) 
#f.close 
#f= open("theta_exp.dat","w")
##f= open("Angle.dat","w")
#for i in range(0,len(texp_theta)):
#  f.write("%lf %lf\r\n" % (i*1.0/10000, thexpAmod_theta[i]))#spl(texp_theta[i])+expA2[0])) 
#  #f.write("%lf\r\n" % (thexpAmodo_theta[i])) 
#f.close 

#thexpAmod_theta = thexpA_theta+spl(texp_theta) #[expA[i]-expA[0]+spl(texp_theta[i]) for i in range(0,exp.shape[1]) if exp[0,i] < tfinal]

fig,=plt.plot(texp_theta,thexpA_theta, label='EXP --2D')
#fig,=plt.plot(texpA2_theta,thexpA2_theta, label='EXP --2D')
#plt.plot(texp_theta,thexpA2o_theta, label='EXP --2D')
#plt.plot(texp_theta,thexpA_theta, label='EXP --2D')
#plt.plot(texp_theta,thexpS_theta, label='EXP --2D')
#plt.plot(texp_theta,thexpSG_theta, label='EXP --2D')
#plt.plot(texp_theta,thexpAG_theta, label='EXP --2D')

plt.plot(texp_theta,thexpAmod_theta, label='3')
plt.plot(texp_theta,thexpAmodo_theta, label='EXP --2D')
#plt.plot(ts_theta,ths_theta, label='simu')
plt.plot(texp_theta,thexp_theta, label='exp')
#plt.plot(texp_theta,spl(texp_theta), label='EXP --2D')
#plt.scatter(texpevent_dep,thexpevent_theta,marker='X',edgecolors='Black',linewidths=0.5)
plt.xlabel('t (ms)')
plt.ylabel('theta (deg°)')
plt.grid(True)
plt.title('Rotation')
plt.legend(loc="upper right")

plt.show()

#fig,=plt.plot(texp_theta,thexpA_theta, label='EXP --2D')
fig,=plt.plot(texpA2_theta,thexpA2_theta, label='1')
plt.plot(texp_theta,thexpA2o_theta, label='2')
#plt.plot(texp_theta,thexpA_theta, label='EXP --2D')
#plt.plot(texp_theta,thexpS_theta, label='EP --2D')
#plt.plot(texp_theta,thexpSG_theta, label='EXP --2D')
#plt.plot(texp_theta,thexpAG_theta, label='EXP --2D')

#plt.plot(texp_theta,thexpAmod_theta, label='EXP --2D')
#plt.plot(texp_theta,thexpAmodo_theta, label='EXP --2D')
plt.plot(texp_theta,spl(texp_theta)+expA2[0], label='3')
#plt.scatter(texpevent_dep,thexpevent_theta,marker='X',edgecolors='Black',linewidths=0.5)
plt.xlabel('t (ms)')
plt.ylabel('theta (deg°)')
plt.grid(True)
plt.title('Rotation')
plt.legend(loc="upper right")
plt.show()
