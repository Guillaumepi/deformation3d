# compilateur
COMPILE=gfortran -g -O3 -fcheck=all -Wall# -O3 #-g -O0 #-O3 #-g #-O3
#COMPILE=mpif90

# edition de liens
deformation3D_para : deformation3D_para.o TimeScheme.o AdvectionProblem.o advection.o  variables.o 
	$(COMPILE) -o  deformation3D_para deformation3D_para.o AdvectionProblem.o TimeScheme.o advection.o variables.o $(LIBS)

deformation3D : deformation3D.o TimeScheme.o AdvectionProblem.o advection.o  variables.o 
	$(COMPILE) -o  deformation3D deformation3D.o AdvectionProblem.o TimeScheme.o advection.o variables.o $(LIBS)

deformation2D : deformation2D.o TimeScheme.o AdvectionProblem.o advection.o  variables.o 
	$(COMPILE) -o  deformation2D deformation2D.o AdvectionProblem.o TimeScheme.o advection.o variables.o $(LIBS)

deformation2Dmidline : deformation2Dmidline.o libBezier.o TimeScheme.o AdvectionProblem.o interpol.o variables.o 
	$(COMPILE) -o  deformation2Dmidline deformation2Dmidline.o libBezier.o AdvectionProblem.o TimeScheme.o interpol.o variables.o $(LIBS)

deformationmidline : deformationmidline.o libBezier.o TimeScheme.o AdvectionProblem.o interpol.o advection.o  variables.o 
	$(COMPILE) -o  deformationmidline deformationmidline.o libBezier.o AdvectionProblem.o TimeScheme.o interpol.o advection.o variables.o $(LIBS)

deformationmidline3D : deformationmidline3D.o libBezier.o TimeScheme.o AdvectionProblem.o interpol.o variables.o 
	$(COMPILE) -o  deformationmidline3D deformationmidline3D.o libBezier.o AdvectionProblem.o TimeScheme.o interpol.o variables.o $(LIBS)

deformationmidline3DTheo : deformationmidline3DTheo.o libBezier.o TimeScheme.o AdvectionProblem.o interpol.o variables.o midline2D_modules_theo.o
	$(COMPILE) -o  deformationmidline3DTheo deformationmidline3DTheo.o libBezier.o AdvectionProblem.o TimeScheme.o interpol.o variables.o midline2D_modules_theo.o $(LIBS)

deformationmidline2D : deformationmidline2D.o libBezier.o TimeScheme.o AdvectionProblem.o interpol.o variables.o 
	$(COMPILE) -o  deformationmidline2D deformationmidline2D.o libBezier.o AdvectionProblem.o TimeScheme.o interpol.o variables.o $(LIBS)

deformationmidline2DTheo : deformationmidline2DTheo.o libBezier.o TimeScheme.o AdvectionProblem.o interpol.o variables.o midline2D_modules_theo.o
	$(COMPILE) -o  deformationmidline2DTheo deformationmidline2DTheo.o libBezier.o AdvectionProblem.o TimeScheme.o interpol.o variables.o midline2D_modules_theo.o $(LIBS)

deformationmidline3D_v2 : deformationmidline3D_v2.o libBezier.o TimeScheme.o AdvectionProblem.o interpol.o variables.o 
	$(COMPILE) -o  deformationmidline3D_v2 deformationmidline3D_v2.o libBezier.o AdvectionProblem.o TimeScheme.o interpol.o variables.o $(LIBS)

testlib : testlib.o libBezier.o TimeScheme.o AdvectionProblem.o interpol.o advection.o  variables.o 
	$(COMPILE) -o  testlib testlib.o libBezier.o AdvectionProblem.o TimeScheme.o interpol.o advection.o variables.o $(LIBS)

deformation2DWENO : deformation2DWENO.o TimeScheme.o AdvectionProblem.o advection.o  variables.o 
	$(COMPILE) -o  deformation2DWENO deformation2DWENO.o AdvectionProblem.o TimeScheme.o advection.o variables.o $(LIBS)

deformation2DBB : deformation2DBB.o TimeScheme.o AdvectionProblem.o advection.o  variables.o 
	$(COMPILE) -o  deformation2DBB deformation2DBB.o AdvectionProblem.o TimeScheme.o advection.o variables.o $(LIBS)

advectionMain : advectionMain.o TimeScheme.o AdvectionProblem.o advection.o  variables.o doubler.o 
	$(COMPILE) -o  advectionMain advectionMain.o TimeScheme.o AdvectionProblem.o advection.o variables.o doubler.o $(LIBS)

mainBezier : mainBezier.o libBezier.o doubler.o 
	$(COMPILE) -o  mainBezier mainBezier.o libBezier.o doubler.o $(LIBS)

# compilation 
deformation3D_para.o : deformation3D_para.f90 TimeScheme.mod AdvectionProblem.mod advection.mod variables.mod 
	$(COMPILE) -c  deformation3D_para.f90

deformation3D.o : deformation3D.f90 TimeScheme.mod AdvectionProblem.mod advection.mod variables.mod 
	$(COMPILE) -c  deformation3D.f90

deformation2D.o : deformation2D.f90 TimeScheme.mod AdvectionProblem.mod advection.mod variables.mod 
	$(COMPILE) -c  deformation2D.f90

deformation2Dmidline.o : deformation2Dmidline.f90 libBezier.mod TimeScheme.mod AdvectionProblem.mod interpol.mod variables.mod 
	$(COMPILE) -c  deformation2Dmidline.f90

deformationmidline.o : deformationmidline.f90 libBezier.mod TimeScheme.mod AdvectionProblem.mod interpol.mod advection.mod variables.mod 
	$(COMPILE) -c  deformationmidline.f90

deformationmidline3D.o : deformationmidline3D.f90 libBezier.mod TimeScheme.mod AdvectionProblem.mod interpol.mod variables.mod 
	$(COMPILE) -c  deformationmidline3D.f90

deformationmidline3DTheo.o : deformationmidline3DTheo.f90 libBezier.mod TimeScheme.mod AdvectionProblem.mod interpol.mod variables.mod midline2D_modules_theo.mod
	$(COMPILE) -c  deformationmidline3DTheo.f90

deformationmidline2D.o : deformationmidline2D.f90 libBezier.mod TimeScheme.mod AdvectionProblem.mod interpol.mod variables.mod 
	$(COMPILE) -c  deformationmidline2D.f90

deformationmidline2DTheo.o : deformationmidline2DTheo.f90 libBezierTheo.mod TimeScheme.mod AdvectionProblem.mod interpol.mod variables.mod midline2D_modules_theo.mod
	$(COMPILE) -c  deformationmidline2DTheo.f90

deformationmidline3D_v2.o : deformationmidline3D_v2.f90 libBezier.mod TimeScheme.mod AdvectionProblem.mod interpol.mod variables.mod 
	$(COMPILE) -c  deformationmidline3D_v2.f90

testlib.o : testlib.f90 libBezier.mod TimeScheme.mod AdvectionProblem.mod interpol.mod advection.mod variables.mod 
	$(COMPILE) -c  testlib.f90

deformation2DWENO.o : deformation2DWENO.f90 TimeScheme.mod AdvectionProblem.mod advection.mod variables.mod 
	$(COMPILE) -c  deformation2DWENO.f90

deformation2DBB.o : deformation2DBB.f90 TimeScheme.mod AdvectionProblem.mod advection.mod variables.mod 
	$(COMPILE) -c  deformation2DBB.f90

advectionMain.o : advectionMain.f90 TimeScheme.mod AdvectionProblem.mod advection.mod variables.mod 
	$(COMPILE) -c  advectionMain.f90

mainBezier.o : mainBezier.f90 libBezier.mod doubler.mod 
	$(COMPILE) -c  mainBezier.f90

AdvectionProblem.o AdvectionProblem.mod : AdvectionProblem.f90 interpol.mod variables.mod doubler.mod 
#AdvectionProblem.o AdvectionProblem.mod : AdvectionProblem.f90 advection.mod variables.mod doubler.mod 
	$(COMPILE) -c  AdvectionProblem.f90

TimeScheme.o TimeScheme.mod : TimeScheme.f90 AdvectionProblem.mod  
	$(COMPILE) -c  TimeScheme.f90

advection.o advection.mod: advection.f90  variables.mod  doubler.mod 
	$(COMPILE) -c   advection.f90 

interpol.o interpol.mod: interpol.f90 doubler.mod
	        $(COMPILE) -c   interpol.f90

libBezier.o libBezier.mod: libBezier.f90 AdvectionProblem.mod doubler.mod
	        $(COMPILE) -c   libBezier.f90

libBezierTheo.o libBezierTheo.mod: libBezierTheo.f90 AdvectionProblem.mod doubler.mod
	        $(COMPILE) -c   libBezierTheo.f90

midline2D_modules_theo.o midline2D_modules_theo.mod: midline2D_modules_theo.f90 AdvectionProblem.mod doubler.mod
	        $(COMPILE) -c   midline2D_modules_theo.f90

util.o util.mod: util.f90   variables.mod  doubler.mod 
	$(COMPILE) -c   util.f90 

variables.o variables.mod: variables.f90 doubler.mod
	$(COMPILE) -c  variables.f90 

doubler.o doubler.mod: doubler.f90
	$(COMPILE) -c   doubler.f90 

#  destruction des fichiers objets
clean :
	rm -f *.o *.mod u *.vtk deformation3D_para deformation3D deformation2D advectionMain

