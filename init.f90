module init  
  
  implicit none
  
contains  
  
  subroutine initialisation(arg1,arg2,arg4,arg5)
    
    use variables
    use doubler
    
    implicit none
    
    integer :: i,j, compteur,l,m,p
    real(pr) :: x,y,r1,r2,r3,r4,r, phix_inter, errmax, phimin, som0, som1,xi,yj, maxva, mid0, mid1, somm0, somm1
    real(pr), dimension(:,:), allocatable :: Trand 
    character(len=128),intent(in) :: arg1,arg2,arg4,arg5
!!    character(len=32),intent(in) :: arg1,arg2

    allocate(Trand(nx,ny)) 

    errmax = 0._pr
    dd0 = -0.1_pr
    dd1 = 0.1_pr
    epsilon = 0.001_pr
   ! epsilon = 0.2
    epsilon = repsilon !0.002_pr


    call random_number(Trand) 

    !Trand = Trand*0.1


  open(10,file=arg4, status='OLD')  
  do j = 1,ny
    do i = 1,nx
        read(10,*) rho00(j,i)
    enddo
  enddo
  close(10) 

  open(10,file=arg5, status='OLD')  
  do j = 1,ny
    do i = 1,nx
        read(10,*) rho11(j,i)
    enddo
  enddo
  close(10) 
!!  open(10,file='rho0.txt', status='OLD')  
!!  open(10,file='../EulerienTO/zebra/zebra5.dat', status='OLD')  
  open(10,file=arg1, status='OLD')  
!!  open(10,file="/Users/guillaume_ravel/Documents/mypoisson_centering/TEST/IMAGES2/Image_098.dat", status='OLD')  
!!  do i = 1,nx
!!!!     read(10,*)rho0(i,:) 
!!     do j = 1,ny
!!        read(10,*) rho0(i,j)
!!     enddo
!!  enddo
!!  close(10) 
  do j = 1,ny
    do i = 1,nx
!!  do j = 1+ny/4,3*ny/4
!!    do i = 1+nx/4,3*nx/4
        read(10,*) rho0(j,i)
    enddo
  enddo
  close(10) 

!!  open(12,file='rho1.txt', status = 'OLD')
!!  open(12,file='../EulerienTO/zebra/zebra6.dat', status='OLD')  
  open(12,file=arg2, status = 'OLD')
!!  open(12,file="/Users/guillaume_ravel/Documents/mypoisson_centering/TEST/IMAGES2/Image_100.dat", status='OLD')  
!!  do i = 1,nx
!!!!     read(12,*)rho1(i,:)
!!    do j = 1,ny
!!        read(12,*) rho1(i,j)
!!    enddo 
!!  enddo
!!  close(12)
  do j = 1,ny
    do i = 1,nx
        read(12,*) rho1(j,i)
    enddo 
  enddo
  close(12)

!!$  print*,'val max=',maxval(abs(rho0)) 
!!$  print*,'val max =',maxval(abs(rho1))
  
!!$ som0 = 0._pr 
!!$      som1 = 0._pr 
!!$      do i=1,nx
!!$         do j=1,ny 
!!$            som0 = som0 + rho0(i,j) ; 
!!$            som1 = som1 + rho1(i,j) ; 
!!$         enddo
!!$      enddo 
!!$      
!!$      print*,'som0=',som0 
!!$  print*,'som1 =',som1

    do i=1,nx
       xx(i) = (float(i)-1.0_pr)*dx  -2._pr
       !xx(i) = (float(i)-0.5_pr)*dx
       xx(i) = float(i)*dx 
       do j=1,ny
          yy(j) = (float(j)-1.0_pr)*dy -2._pr
          !yy(j) = (float(j)-0.5_pr)*dy 
          yy(j) = float(j)*dy
          
          r = sqrt((xx(i))**2 + (yy(j))**2)
          x = xx(i)
          y = yy(j)
          xp(i,j) = xx(i)
          yp(i,j) = yy(j)
      
          
      ! rho0(i,j) = 15._pr*exp(-10*((x-0.)**2 + (y-0.)**2)) + epsilon
      ! rho1(i,j) = 15._pr*exp(-10*((x+ 0.05)**2 + (y+ 0.)**2)) + epsilon
       
      
       !rho0i(i,j) = 15._pr*exp(-10*((x-0.)**2 + (y-0.)**2))   +  0.01
       !rho1i(i,j) = 15._pr*exp(-10*((x + 0.1)**2 + (y + 0.1)**2))   + 0.01

!!!!!!!!!!!!!!!!!!!!!
      
         ! Cas test 1: 
      !   rho0(i,j) = 3.97887*exp(-12.5*((x/float(nx)-0.5)**2 + ((y/float(ny)-0.5)+0.1)**2))
      !   rho0(i,j) = 3.97887*exp(-100*((x/float(nx)-0.5)**2 + ((y/float(ny)-0.5)+0.1)**2))
       !!  rho1(i,j) = 4.59441*exp(-16.66666*(x**2 + x*(y-0.1) + (y-0.1)**2))
!!         rho1(i,j) = 4.59441*exp(-16.66666*(x**2 - x*(y-0.1) + (y-0.1)**2)) 

         ! cas test 2: 

        !  rho0(i,j) =  1.98944*exp(-12.5*(-0.2 + x)**2 - 12.5*(0.1 + y)**2) & 
        !      & + 1.98944*exp(-12.5*(0.2 + x)**2 - 12.5*(0.1 + y)**2) 
        ! rho1(i,j) =  3.97887*exp(-12.5*x**2 -12.5*y**2) 


         ! cas test 3:  
!!$         rho0(i,j) =  1.98944*(exp(-12.5_pr*( (x-0.5)**2+(y+0.3)**2)) + exp(-12.5_pr*((x+0.5)**2+(y+0.3)**2))   )
!!$        rho1(i,j) = 3.9788*exp(-12.5*((x)**2 + (y)**2))
!!$
!!$        rho0i(i,j) = rho0(i,j) + 0.01
!!$        rho1i(i,j) = rho1(i,j) + 0.01

        ! cas test 5: 

       !  rho0(i,j) = exp(-10._pr*((x-0.2)**2 + (y-0.48)**2)) + exp(-10._pr*((x-0.8)**2 + (y-0.52)**2)) 
       ! rho1(j,i) =  rho0(i,j) 


          phi2(i,j) = 1._pr
              
       enddo
      
    enddo



!!  rho1 = rho0 
!!  do i=1,nx-1
!!    do j=1,ny
!!        rho1(i,j) = rho0(i+1,j)
!!    enddo
!!  enddo
  !!mid0 = abs(maxval(rho0)-minval(rho0))*0.1
!!  mid1 = abs(maxval(rho1)-minval(rho1))*0.12
!!  do i=1,nx
!!    do j=1,ny
!!      !if (rho00(i,j).lt.epsilon) rho0(i,j)=0._pr
!!      !!if (rho00(i,j)==1) rho0(i,j)=1._pr
!!    !!  if (rho0(i,j)>mid0) rho0(i,j)=maxval(rho0)
!!      !if (rho11(i,j).lt.epsilon) rho1(i,j)=0._pr
!!      !!if (rho11(i,j)==1) rho1(i,j)=1._pr
!!      if (rho1(i,j)>mid1) then
!!              rho1(i,j)=maxval(rho1)
!!      else
!!              rho1(i,j)=0
!!      endif
!!    enddo
!!  enddo
!!  rho0 = rho0/maxval(rho0)
!!  rho1 = rho1/maxval(rho1)
!!  do i=1,nx
!!    do j=1,ny
!!      !if (rho00(i,j).lt.epsilon) rho0(i,j)=0._pr
!!      if (rho00(i,j).lt.1) rho0(i,j)=0._pr
!!      if (rho00(i,j)==1) rho0(i,j)=1._pr
!!      !if (rho11(i,j).lt.epsilon) rho1(i,j)=0._pr
!!    enddo
!!  enddo
!!
!!  do p=1,1
!!    do i=2,nx-1
!!      do j=2,ny-1
!!        if (rho0(i,j)<1) then
!!          somm0=0
!!          do l=i-1,i+1
!!            do m=j-1,j+1
!!              somm0=somm0+rho0(l,m)
!!            enddo
!!          enddo  
!!          rho0(i,j)=somm0/9
!!        endif
!!        if (rho1(i,j)<1) then
!!          somm1=0
!!          do l=i-1,i+1
!!            do m=j-1,j+1
!!              somm1=somm1+rho1(l,m)
!!            enddo
!!          enddo  
!!          rho1(i,j)=somm1/9
!!        endif
!!      enddo
!!    enddo
!!  enddo
!!  rho0 = rho0/maxval(rho0)
!!  rho1 = rho1/maxval(rho1)
  !epsilon=1e-8
  do i=1,nx
    do j=1,ny
      !if (rho00(i,j).lt.epsilon) rho0(i,j)=0._pr
      if (rho0(i,j).lt.epsilon) then 
              rho0(i,j)=0._pr
      else
 !             rho0(i,j)=1
      endif
      !if (rho00(i,j)==1) rho0(i,j)=1._pr
      !if (rho11(i,j).lt.epsilon) rho1(i,j)=0._pr
      if (rho1(i,j).lt.epsilon) then 
              rho1(i,j)=0._pr
      else
  !            rho1(i,j)=1
      endif
      !if (rho11(i,j)==1) rho1(i,j)=1._pr
    enddo
  enddo
  rho0 = rho0/maxval(rho0)
  rho1 = rho1/maxval(rho1)





  !rho0 = rho0 + epsilon !+ Trand 
  !rho1 = rho1 + epsilon ! + Trand 
  print*,"maxval0 ",maxval(rho0)
  print*,"maxval1 ",maxval(rho1)

  som0 = 0._pr 
  som1 = 0._pr 
  somm0 = 0._pr
  somm1 = 0._pr
  do i=1,nx
     do j=1,ny 
        som0 = som0 + rho0(i,j)  
        som1 = som1 + rho1(i,j) 
        if (rho0(i,j)==1) somm0=somm0+1
        if (rho1(i,j)==1) somm1=somm1+1
     enddo
  enddo 
 print*,"maxval0 ",maxval(rho0)
 print*,"maxval1 ",maxval(rho1)
 rho0(:,:)=rho0(:,:)/som0
 rho1(:,:)=rho1(:,:)/som1
 print*,'som0=',som0 
 print*,'som1 =',som1
 print*,'somm0=',somm0 
 print*,'somm1 =',somm1
    
!!
!!!!    rho1i = 1000*rho1 + epsilon
!!!!    rho0i = 1000*rho0 + epsilon
!!
  rho0 = 1000*rho0 + epsilon !+ Trand 
  rho1 = 1000*rho1 + epsilon ! + Trand 
  maxva = (maxval(rho0) + maxval(rho1))*0.5
  !!rho0 = rho0/maxva + epsilon !+ Trand 
  !!rho1 = rho1/maxva + epsilon ! + Trand 
  rho0i=rho0
  rho1i=rho1
  print*,"maxval0 ",maxval(rho0)
  print*,"maxval1 ",maxval(rho1)

    open(unit=78,file='rho0.txt',status='unknown')
    open(unit=79,file='rho0matlab.txt',status='unknown')
    open(unit=80,file='rho1matlab.txt',status='unknown')
    do i=1,nx
!!    	xi = (float(i)-1.0_pr)*dx-2
        xi = xx(i)
    	do j=1,ny
!!		yj = (float(j)-1.0_pr)*dy-2
                yj = yy(j)
		write(78,*) xi, yj, rho0(i,j), rho1(i,j)
		write(79,*) rho0(i,j)
		write(80,*) rho1(i,j)
	end do
        write(78,*)      
    enddo
    close(78)    
    close(79)    
    close(80)    

    rho = rho0

    deallocate(Trand) 

  end subroutine initialisation

end module init
