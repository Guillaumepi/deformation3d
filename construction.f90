module construction 
  
  implicit none
  
contains  
  
  subroutine assemblage 
    
    use variables
    use doubler
    use bord
   
    use util

    implicit none
    
    integer :: i,j,indice1,indice2, indx, indy
    real(pr) :: y,r,x,denom,distx,disty, fix, fiy,sol
    real(pr) :: mx,mn,mxx, nnx , nny, nnxmax, nnymax,f,jump, alph
    real(pr) :: alphax1, betax1, gammax1, ax1,bx1,cx1, denomx, denomy
    real(pr) :: alphax2, betax2, gammax2, ax2,bx2,cx2, epsi
    real(pr) :: alphay1, betay1, gammay1, ay1,by1,cy1
    real(pr) :: alphay2, betay2, gammay2, ay2,by2,cy2, romx, ropx, romy, ropy
   
    epsi = 0.
    indice = 1
  
    ! init second membre

    do i=1,ntot+nbsigne
       b(i) = 0._pr
    enddo
    
    ! remplissage matrice de l'operateur elliptique
    
    ! termes source laplacien 

    do j=1,ny
       do i=1,nx
          b(ind(i,j)) =  b(ind(i,j))  + r0mr1(i,j) 
       enddo
    enddo
    

    ! calcul laplacien 
    do j=1,ny
       do i=1,nx
          
          ! if (phi2(i,j).ge.0._pr) then
!!$          if ((i.eq.10).and.(j.eq.ny/2)) then
!!$             
!!$             mat1(indice) = ind(i,j)
!!$             mat2(indice) = ind(i,j)
!!$             mat3(indice) = 1._pr
!!$             indice = indice +1             
!!$             
!!$          else
             
             ! i,j
             mat1(indice) = ind(i,j)
             mat2(indice) = ind(i,j)
             
             if (i.eq.nx) then
                ropx = ((rho(i,j)+rho(i,j))/2._pr + (rho1(i,j)+rho1(i,j))/2._pr)/2._pr  + epsi
             else
                ropx = ((rho(i,j)+rho(i+1,j))/2._pr + (rho1(i,j)+rho1(i+1,j))/2._pr)/2._pr   + epsi
                !  ropx = ((rho(i,j)+rho(i+1,j))/2._pr + (rho(i,j)+rho(i+1,j))/2._pr)/2._pr 
             endif
             
             if (i.eq.1) then
                romx = ((rho(i,j)+rho(i,j))/2._pr + (rho1(i,j)+rho1(i,j))/2._pr)/2._pr  + epsi
             else
                romx = ((rho(i,j)+rho(i-1,j))/2._pr + (rho1(i,j)+rho1(i-1,j))/2._pr)/2._pr  + epsi
                !    romx = ((rho(i,j)+rho(i-1,j))/2._pr + (rho(i,j)+rho(i-1,j))/2._pr)/2._pr 
             endif
             
             if (j.eq.ny) then
                ropy = ((rho(i,j)+rho(i,j))/2._pr + (rho1(i,j)  +rho1(i,j))/2._pr)/2._pr + epsi
             else
                ropy = ((rho(i,j)+rho(i,j+1))/2._pr + (rho1(i,j)  +rho1(i,j+1))/2._pr)/2._pr + epsi
                !   ropy = ((rho(i,j)+rho(i,j+1))/2._pr + (rho(i,j)  +rho(i,j+1))/2._pr)/2._pr
             endif
             
             if (j.eq.1) then
                romy = ((rho(i,j)+rho(i,j))/2._pr + (rho1(i,j) + rho1(i,j))/2._pr)/2._pr + epsi
             else
                romy = ((rho(i,j)+rho(i,j-1))/2._pr + (rho1(i,j) + rho1(i,j-1))/2._pr)/2._pr + epsi
                !   romy = ((rho(i,j)+rho(i,j-1))/2._pr + (rho(i,j) + rho(i,j-1))/2._pr)/2._pr
             endif
             
             
!!$             if (abs(rho(i,j)).le.1E-6) then
!!$                ropy = 1.
!!$                romy = 1.
!!$                ropx = 1.
!!$                romx = 1.
!!$                b(ind(i,j)) = 0.
!!$                
!!$             endif
!!$
             
!!$             if ((abs(ropx).le.1E-6).and.(abs(romx).le.1E-6)) then
!!$             
!!$                ropx = 1.
!!$                romx = 1.
!!$                b(ind(i,j)) = 0.
!!$
!!$             else
!!$                if ((abs(ropy).le.1E-6).and.(abs(romy).le.1E-6)) then
!!$                   
!!$                   ropy = 1.
!!$                   romy = 1.
!!$                   b(ind(i,j)) = 0.
!!$                endif
!!$             
!!$          endif
             
             
             mat3(indice) =  ( -1._pr*ropx/(dx**2)) -1._pr*romx/(dx**2)  
             
             mat3(indice) = mat3(indice) + ( -1._pr*ropy/(dy**2))   -1._pr*romy/(dy**2 )
             
             
             if ((i-1.lt.1))  mat3(indice) = mat3(indice) +  romx/dx**2

! cette partie je l'ai commenté. Il me semble que ca signifie que je ne fais plus de neumann mais du dirichlet. 
!!$             
             !    if ((i-1.lt.1).and.(j.ne.ny/2))  mat3(indice) = mat3(indice) +  romx/dx**2
             if (i+1.gt.nx) mat3(indice) = mat3(indice) + ropx/dx**2
             
             
             if (j-1.lt.1)  mat3(indice) = mat3(indice)+ romy/dy**2
             if (j+1.gt.ny)  mat3(indice) = mat3(indice)+ ropy/dy**2
             
             indice = indice +1         
             
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ! i-1,j            
             if (i-1.lt.1) then  
                
             else
                ! si i-1 est dans le domaine        
                mat1(indice) = ind(i,j)                                 
                mat2(indice) = ind(i-1,j) 
                mat3(indice) = romx/dx**2
                indice = indice +1
             endif
             
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! !! i+1,j
             if (i+1.gt.nx) then  
                
             else
                ! si i+1 est dans le domaine
                mat1(indice) = ind(i,j)                           
                mat2(indice) = ind(i+1,j) 
                mat3(indice) = ropx/dx**2 
                indice = indice +1
             endif
             
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! !!!i,j-1
             if (j-1.lt.1) then  
                
             else
                ! si j-1 est dans le domaine
                mat1(indice) = ind(i,j)
                mat2(indice) =  ind(i,j-1)
                mat3(indice) =   romy/dy**2 
                indice = indice +1  
             endif
             
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! !!!!i,j+1
             if (j+1.gt.ny) then  
                
             else
                ! si j+1 est dans le domaine
                mat1(indice) = ind(i,j)
                mat2(indice) = ind(i,j+1) 
                mat3(indice) = ropy/dy**2 
                indice = indice +1  
             endif
             
             
             ! endif
             ! endif
             
             
             
       !   endif
          
       enddo
    enddo
    
    indice1= indice
    
    
    
    indice = indice -1
    
  end subroutine assemblage
  
end module construction


