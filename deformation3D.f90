program def3D
  use variables
  use advection
  use AdvectionProblem
  use TimeScheme
  
  implicit none
  integer :: i, j, k, kt, idisplay, iUpdateDist, iter 
  real(pr) :: Tps, threshold 
  real(pr), dimension(:,:), allocatable :: rho, uu, vv, u0, v0, u, v, rhou, rhov 
  real(pr) :: t, tPic, xi, yj, distW, distWb
  real(pr), dimension(:,:,:), allocatable :: rhoSlices, rhoSlices2
  real(pr), dimension(:,:,:), allocatable :: tmp

  N = 200
  nx = 200
  ny = 200
  nz = 200
  eepsilon = 1.e-6_pr
  dt = 1._pr
  threshold = 0.001
  dx = 1._pr !24.7687_pr!1.
  dy = 1._pr !24.7687_pr!1.
  dz = 1._pr !24._pr!1.
  Tps = 1.  
  allocate(rhoSlices2(nx,ny,nz))
  allocate(tmp(N,N,N))
  allocate(rhoSlices(nx,ny,nz))
  allocate(xx(1:nx),yy(1:ny)) 
  allocate(xp(1:nx,1:ny),yp(1:nx,1:ny)) 
  allocate(rho0(1:nx,1:ny)) 
  allocate(rhou(1:nx,1:ny),rhov(1:nx,1:ny))  
  allocate(rhouu(1:nx,1:ny),rhovv(1:nx,1:ny)) 
  allocate(rhouup(1:nx,1:ny),rhovvp(1:nx,1:ny)) 
  allocate(rhop(1:nx,1:ny),rhoup(1:nx,1:ny),rhovp(1:nx,1:ny)) 
  allocate(u(1:nx,1:ny),v(1:nx,1:ny)) 
  allocate(rho(1:nx,1:ny))
  allocate(uu(1:nx,1:ny), vv(1:nx,1:ny))
  allocate(u0(1:nx,1:ny), v0(1:nx,1:ny))
  
    do i=1,nx
      xx(i) = float(i)*dx
      do j=1,ny
        yy(j) = float(j)*dy
      enddo
    enddo  


  !open(unit=78,file='/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_filt2.dat',status='unknown')
  open(unit=78,file='/Users/guillaume_ravel/Desktop/WorkSpaceMatlab/rhoSlices_filt3.dat',status='unknown')
  do k=1,nz
    do j=1,ny
      do i=1,nx
        read(78,*) rhoSlices(i,k,j)
      enddo
    enddo
  enddo
  close(78)
  !where (rhoSlices>=0.2) rhoSlices=0.5
  !where (rhoSlices<0.2) rhoSlices=-0.5
  rhoSlices = 10*rhoSlices !- 0.5_pr
 do k=1,nz
 do i=1,nx
 do j=1,nx
 if (rhoSlices(i,k,j)>1._pr) then
 rhoSlices(i,k,j) = 1._pr
 !else
 !rhoSlices(i,k,j) = 0.
 endif
 enddo
 enddo
 enddo
 rhoSlices2 = rhoSlices

  t = 0
  tPic = 0
  iter = 0
  idisplay = 1
  iUpdateDist = 10
  !call updateDistance3D(rhoSlices)
  do kt = 1,2!249
  write(*,*)"time iteration : ",kt
  !open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesse2/vitesse"//str(kt+140-1)//".txt", status='OLD')  
  open(10,file="/Users/guillaume_ravel/Documents/optimaltransport/EulerienTO/vitesses200/vitesse"//str(kt+140-1)//".txt", status='OLD')  
  do j=1,ny
    do i=1,nx
      read(10,'(F15.6,F15.6)') u0(j,i), v0(j,i)
      !read(10,*) u0(j,i), v0(j,i)
    enddo
  enddo
  close(10) 

  !u0(:,:) = u0(:,:)*24.768700/(1.0/15000)
  !v0(:,:) = v0(:,:)*24.0/(1.0/15000)
  !write(*,*)"velocity : ",24.0/(1.0/15000)

  if (mod(kt,idisplay)==0) then
    !open(unit=79,file='results/rhomatlab'//str(kt+140-1)//'.txt',status='unknown')
    !open(unit=79,file='results/rho'//str(kt+140-1)//'.txt',status='unknown')
    open(unit=79,file='results/rho'//str(kt+140-1)//'.vtk',status='unknown')
      write(79,'(1A26)') '# vtk DataFile Version 2.0'
      write(79,'(a)') 'rho'
      write(79,'(a)') 'ASCII'
      write(79,'(a)') 'DATASET STRUCTURED_POINTS'
      write(79,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,nz
      write(79,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 0.,0.,0.
      !write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', 24.768700, 24.768700, 24.000000
      write(79,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx, dy, dz
      write(79,'(a,I9)') 'POINT_DATA' , nx*ny*nz
      write(79,'(a)') 'SCALARS values double'
      write(79,'(a)') 'LOOKUP_TABLE default'
    
      do k=1,nz
        do j=1,ny
          do i=1,nx
            !if (abs(rhoSlices(i,k,j)).lt.1e-6) then
            !  write(79,*) 0._pr
            !else
              write(79,*) rhoSlices2(i,k,j)
            !endif
          enddo
        enddo
      enddo
    endif

 u = u0!*dx
 v = v0!*dy
 dt = 1._pr!0.9_pr/(eepsilon + maxval(abs(u))/dx + maxval(abs(v))/dy) ! abs() necessaire au cas ou a<0
 write(*,*)"dt ",dt

 do k=1,nz
 !do i=1,nx
 !do j=1,nx
 !rho(i,j)=0.
 !if (rhoSlices(k,i,j)>=(maxval(rhoSlices)-minval(rhoSlices))*0.2) then
 !rho(i,j) = rhoSlices(k,i,j)
 !rho(:,:) = rhoSlices(k,:,:)
 !rho0(:,:) = rhoSlices(k,:,:)
 !endif
 !enddo
 !enddo
   !rhoMax(k) = maxval(rho(:,:))
   !rhoMin(k) = minval(rho(:,:))
! do i=1,nx
! do j=1,nx
!   if (rho(i,j)>=(rhoMax(k)-rhoMin(k))*0.2) then
!           rho(i,j)=1
!   else
!           rho(i,j)=0
!   endif
!   enddo
!   enddo

   !if (maxval(rho(:,:)).gt.1.e-6) rho(:,:) = rho(:,:)/maxval(rho(:,:))   
   !if ((maxval(rho(:,:))-minval(rho(:,:))).gt.1.e-6) rho(:,:) = (rho(:,:)-minval(rho(:,:)))/(maxval(rho(:,:))-minval(rho(:,:)))   

 !do j=1,ny
 !do i=1,nx
 !  if (rho(i,j).lt.1.e-6) then
 !          rho(i,j)=0
 !  endif
 !  enddo
 !  enddo
 !  if (maxval(rho(:,:)).gt.1.e-6) rho(:,:) = rho(:,:)/maxval(rho(:,:))   
 !summ = 0  
 !do j=1,ny
 !do i=1,nx
 !        summ=summ+rho(i,j)
 !enddo
 !enddo
 !  if (summ.gt.1.e-6) rho(:,:) = rho(:,:)/summ
 !rho(:,:) = rho(:,:) + 1.e-6
 !  if (maxval(rho(:,:)).gt.1.e-6) rho(:,:) = rho(:,:)/maxval(rho(:,:))   
 !write(*,*)maxval(rho(:,:))
 u = u0
 v = v0
 if (maxval(rho(:,:))>1.e-6) then
 do i=1,nx
   do j=1,ny
     if (rho(i,j).lt.1e-6) then
       u(i,j) = 0._pr 
       v(i,j) = 0._pr
       rho(i,j) = 0._pr
     endif
   enddo
 enddo     
 endif
 !if (kt==1) rho0i = rhoSlices(kt,:,:)
 call SetInitialCondition(rhoSlices(k,:,:),u,v,rho,rhou,rhov)
 !if (mod(kt,idisplay)==0) then
 ! write(*,*)"kt u v ",kt," ",maxval(abs(u))," ",maxval(abs(v))
 ! write(*,*)"kt dt ",kt," ",1._pr/(maxval(abs(u))/dx + maxval(abs(v))/dy) ! abs() necessaire au cas ou a<0
 !endif

 !do j=1,ny
 !   do i=1,nx 
 !      !u0(i,j)=0
 !      !v0(i,j)=0
 !      rhou(i,j) = rho(i,j)*u(i,j) 
 !      rhov(i,j) = rho(i,j)*v(i,j) 
 !      rhouu(i,j) = rho(i,j)*u(i,j)
 !      rhovv(i,j) = rho(i,j)*v(i,j)
 !   enddo
 !enddo

 if (maxval(rho(:,:))>1.e-6_pr) then
 write(*,*)" k                                                        ",k,"        ", maxval(rho(:,:))
 tPic = t
  do while (tPic<float(kt))
      tPic = tPic + dt
      iter = iter + 1
      !write(*,*)"tPic       kt        t        ",tPic,"         ",kt,"      ",t
      !call AdvanceTime(kt, t, 3, 5, 0, 1, rho, u, v, rhou, rhov)
      call AdvanceTime(kt, t, 0, 3, 4, 1, rho, u, v, rhou, rhov)
      !call AdvanceTime(kt, t, 0, 3, 5, 1, rho, u, v, rhou, rhov)

      !if (mod(iter,iUpdateDist)==0) call updateDistance(rho)
      !if (mod(iter,iUpdateDist)==0) then
        !rhoSlices(k,:,:) = rho(:,:)
        !call updateDistance3D(rhoSlices)
      !endif
 enddo
 endif

 !Tps=1._pr
 !call transport(Tps,-1,0,5)
 !call AdvanceParticle(kt,kt*dt,3)
 !write(*,*) "maxval ",k," == ",maxval(rho(:,:))
 rhoSlices(k,:,:) = rho(:,:)
 !do i=1,nx
 !do j=1,nx
 !if (rhoSlices(i,k,j)>0.1) then
 !rhoSlices(k,i,j) = 1._pr
 !endif
 !enddo
 !enddo
 !if ((maxval(rho(:,:))-minval(rho(:,:))).gt.1.e-6) then
 !        !rhoSlices(k,:,:) = (rhoMax(k)-rhoMin(k)) / (maxval(rho(:,:))-minval(rho(:,:))) * (rho(:,:)-minval(rho(:,:))) + rhoMin(k)
 !        rhoSlices(k,:,:)=(rho(:,:)-minval(rho(:,:)))/(maxval(rho(:,:))-minval(rho(:,:)))
 !else
 !        rhoSlices(k,:,:)=0_pr
 !endif
 
 !if (maxval(rho(:,:)).gt.1.e-6)  rhoSlices(k,:,:)=rho(:,:)/maxval(rho(:,:))
!if ((maxval(rho(:,:))-minval(rho(:,:))).gt.1.e-6)  rhoSlices(k,:,:)=(rho(:,:)-minval(rho(:,:)))/(maxval(rho(:,:))-minval(rho(:,:)))



 enddo

!if ((maxval(rhoSlices(:,:,:))-minval(rhoSlices(:,:,:))).gt.1.e-6)  rhoSlices2(:,:,:)=(rhoSlices(:,:,:)-minval(rhoSlices(:,:,:)))/(maxval(rhoSlices(:,:,:))-minval(rhoSlices(:,:,:)))
 rhoSlices2 = rhoSlices
 !where (rhoSlices2>1._pr) rhoSlices2=1._pr
! do j=1,ny
! do i=1,nx
!   if (rhoSlices(i,j,k)>1._pr) then
!     rhoSlices(i,j,k) = 1_pr
!   !else
!   !  rhoSlices(i,j,k) = 0_pr
!   !endif
!enddo
!enddo
!if (maxval(rhoSlices(:,:,:)).gt.1.e-6)  rhoSlices(:,:,:)=rhoSlices(:,:,:)/(maxval(rhoSlices(:,:,:)))
 t = tPic
 !if (mod(kt,iUpdateDist)==0) call updateDistance3D(rhoSlices)
 write(*,*)"t                    ",t
!   write(*,*) "maxrho ",sum(rhoMax)
!   write(*,*) "minrho ",sum(rhoMin)
 if (mod(kt,idisplay)==0) then
    close(79)    
    endif
 enddo  
 
!!!!! ecriture resultat
  !sortie vtk
!! open(unit=78,file='../rhoSlices.vtk',status='unknown',position='append') 
!!!!  write(78,'(1A26)') '# vtk DataFile Version 2.0'
!!!!  write(78,'(a)') 'rho'
!!!!  write(78,'(a)') 'ASCII'
!!!!  write(78,'(a)') 'DATASET STRUCTURED_POINTS'
!!!!  write(78,'(a,I4,I4,I4)') 'DIMENSIONS', nx,ny,nbSlices
!!!!  write(78,'(a,E23.15,E23.15,E23.15)') 'ORIGIN', 0.,0.,0.
!!!!  write(78,'(a,E23.15,E23.15,E23.15)') 'SPACING', dx,dy,dz
!!!!  write(78,'(a,I6)') 'POINT_DATA ' , nx*ny*nbSlices
!!!!  write(78,'(a)') 'SCALARS values double' !SCALARS density float 1'
!!!!  write(78,'(a)') 'LOOKUP_TABLE default'
!!  do kt=1,nbSlices-1,2
!!    rhoSlices(kt,:,:)=rhoSlices(kt,:,:)/maxval(rhoSlices(kt,:,:)) !!rhoSlices(kt,nx/2,ny/2) !maxval(rhoSlices(kt,:,:))
!!  enddo
!!  do kt=1,nbSlices-1,2
!!    do j=1,ny
!!      do i=1,nx
!!        if (abs(rhoSlices(kt,i,j)).lt.1E-6) then
!!          write(78,999) 0.
!!        else
!!          write(78,999)  rhoSlices(kt,i,j) 
!!        endif
!!      enddo
!!    enddo
!!  enddo
!!  999 FORMAT(5(E23.15))
!!  close(78)

 deallocate(rhoSlices2)
 deallocate(rhoSlices)
 deallocate(rho0) 
 deallocate(xx,yy)
 deallocate(u,v,rho,rhou,rhov) 
 deallocate(rhoup,rhovp,rhop)
 deallocate(uu,vv,rhouu,rhovv,rhouup,rhovvp)
 deallocate(u0,v0)
 deallocate(tmp)
 
end program def3D

!   "Convert an integer to string."
character(len=3) function str(k)
    integer, intent(in) :: k
    write (str, '(I3.3)') k
    str = adjustl(str)
end function str
